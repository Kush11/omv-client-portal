import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { PageNotFoundComponent } from './shared/page-not-found/page-not-found.component';
import { AuthCallbackComponent } from './auth-callback/auth-callback.component';
import { AuthGuard } from './core/guards/auth-guard.service';
import { AuthService } from './core/services/business/auth.service';
import { UnAuthorizedComponent } from './unauthorized/unauthorized.component';
import { AuthorizationCheckComponent } from './authorization-check/authorization-check.component';
import { SelectiveStrategy } from './selective-strategy.service';
import { ReportIssuesComponent } from './report-issues/report-issues.component';
import { ClearLocalStorageComponent } from './clear-local-storage/clear-local-storage.component';
import { LoginComponent } from './login/login.component';


const routes: Routes = [
  {
    path: '',
    redirectTo: 'dashboard',
    pathMatch: 'full'
  },
  {
    path: 'unauthorize',
    component: UnAuthorizedComponent
  },
  {
    path: 'authorize-check',
    component: AuthorizationCheckComponent
  },
  {
    path: 'implicit/callback',
    component: AuthCallbackComponent
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'dashboard',
    component: DashboardComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'page-not-found',
    component: PageNotFoundComponent
  },
  {
    path: 'report-issues',
    component: ReportIssuesComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'media',
    loadChildren: './media/media.module#MediaModule',
    data: { preload: false }
  },
  {
    path: 'work-planning',
    loadChildren: './work-planning/work-planning.module#WorkPlanningModule',
    data: { preload: false }
  },
  {
    path: 'live-stream',
    loadChildren: './live-stream/live-stream.module#LiveStreamModule',
    data: { preload: false }
  },
  {
    path: 'collaborations',
    loadChildren: './collaborations/collaborations.module#CollaborationsModule',
    data: { preload: false }
  },
  {
    path: 'admin',
    loadChildren: './admin/admin.module#AdminModule',
    data: { preload: false }
  },
  {
    path: 'clear',
    component: ClearLocalStorageComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: SelectiveStrategy, useHash: false }),
  ],
  exports: [RouterModule],
  providers: [AuthGuard, AuthService]
})
export class AppRoutingModule { }
