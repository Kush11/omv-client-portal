import { Component, OnInit, OnDestroy, AfterViewInit, ViewChild } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { Breadcrumb } from 'src/app/core/models/breadcrumb';
import { LiveStreamState } from '../state/live-stream/live-stream.state';
import { AppState } from 'src/app/state/app.state';
import { LiveStream } from 'src/app/core/models/entity/live-stream';
import { BaseComponent } from 'src/app/shared/base/base.component';
import { ClearLiveStreamPlaylist, SetBreadcrumbs, GetLiveStreamPlaylist, AddLiveStreamFavorite } from '../state/live-stream/live-stream.action';
import { ActivatedRoute, Router } from '@angular/router';
import { SubSink } from 'subsink/dist/subsink';
import { ModalComponent } from 'src/app/shared/modal/modal.component';
import { FieldConfiguration } from 'src/app/shared/dynamic-components/field-setting';
import { FormGroup } from '@angular/forms';
declare let jwplayer: any;

@Component({
  selector: 'app-live-stream-viewer',
  templateUrl: './live-stream-viewer.component.html',
  styleUrls: ['./live-stream-viewer.component.css']
})
export class LiveStreamViewerComponent extends BaseComponent implements OnInit, OnDestroy, AfterViewInit {

  @Select(LiveStreamState.getBreadcrumbs) breadcrumbs$: Observable<Breadcrumb[]>;
  @Select(AppState.getPreviousRoute) previousRoute$: Observable<Breadcrumb[]>;
  @Select(LiveStreamState.getPlaylist) playlist$: Observable<LiveStream[]>;
  @Select(LiveStreamState.getCurrentLiveStream) currentLiveStream$: Observable<LiveStream>;
  @Select(AppState.getJwtlicenceKey) jwtLincenceKey$: Observable<string>;


  @ViewChild('saveToFavoritesModal') saveToFavoritesModal: ModalComponent;

  private subs = new SubSink();
  liveStreams: LiveStream[] = [];
  vidObj: any;
  testvideo: LiveStream;
  showVideos: boolean;
  id: any;
  currentStream: LiveStream;
  mainplayer: any;
  allPlayers: any[] = [];
  streamUrl: string;
  playerTimer: any;
  timer: number = 5000;



  favoriteName: FieldConfiguration[] = [
    { type: 'input', label: 'Name', inputType: 'text', name: 'name', required: true, validationMessage: 'Please enter a name.' }
  ];
  livestreamIds: any[] = [];
  jwtLincenceKey: string;

  constructor(protected store: Store, private route: ActivatedRoute, private router: Router) {
    super(store);
    this.hideLeftNav();
    this.setPageTitle("Live Stream Viewer");
    this.store.dispatch(new ClearLiveStreamPlaylist());
  }

  ngOnInit() {
    
    const crumbs = [
      { link: '/live-stream/currently-streaming', name: 'Live Stream' },
      { link: '/live-stream/currently-streaming', name: 'Currently Streaming' },
      { link: '', name: 'Selected' }
    ];
    this.store.dispatch(new SetBreadcrumbs(crumbs));

    this.subs.add(
      this.route.queryParams
        .subscribe(params => {
          let items = params.items;
          this.livestreamIds = items.split(',');
          this.store.dispatch(new GetLiveStreamPlaylist(this.livestreamIds));
        }),
      this.playlist$
        .subscribe(streams => {
          if (streams) {
            this.liveStreams = streams;
          }
        }),
        this.jwtLincenceKey$.subscribe(key => this.jwtLincenceKey = key)
  
    );
    jwplayer.key = this.jwtLincenceKey;
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
    this.store.dispatch(new ClearLiveStreamPlaylist());
    clearTimeout(this.playerTimer)
  }

  ngAfterViewInit() {
    this.setVideoControls();
  }

  switchBreadCrumbs(breadcrumbLink: string) {
    this.router.navigate([breadcrumbLink]);
  }


  reload(streamUrl) {
    jwplayer().load(streamUrl);
    jwplayer().play();
  }

  jumpLive() {
    jwplayer().seek(-jwplayer().getDuration())
  }

  private setVideoControls() {
    this.subs.sink =
      this.playlist$
        .subscribe(streams => {
          if (streams) {
            if (this.liveStreams.length > 0) {
              console.log('streams', streams)
              this.liveStreams.forEach(video => {
                const id = `player_${video.id}`;
                jwplayer(id).setup({
                  sources: [{
                    file: video.url
                  }],
                  responsive: true,
                  width: '100%',
                  autostart: true,
                  aspectratio: "16:9",

                  rtmp: {
                    bufferlength: 5
                  },
                  fallback: true,

                });

                //reconnect every 5 seconds

                jwplayer(id).onIdle(() => {
                  this.playerTimer = setTimeout(jwplayer(id).play(), this.timer);
                });

                jwplayer(id).onError((e) => {
                  console.log("onError caught and resetting!!!", e);
                  this.reload(video.url);
                });
              });
            }
          }
        });
  }
  showSaveToFavoritesModal() {
    this.saveToFavoritesModal.reset();
    this.saveToFavoritesModal.show();
  }
  saveToFavorites(form: FormGroup) {
    if (form.valid) {
      if (form.dirty) {
        const name = form.value.name;
        this.store.dispatch(new AddLiveStreamFavorite(null, name, this.livestreamIds));
      }
    }
  }
}