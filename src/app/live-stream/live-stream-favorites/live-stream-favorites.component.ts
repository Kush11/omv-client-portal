import { Component, OnInit, OnDestroy } from '@angular/core';
import { SubSink } from 'subsink/dist/subsink';
import { Store } from '@ngxs/store';
import { ActivatedRoute } from '@angular/router';
import { BaseComponent } from 'src/app/shared/base/base.component';
import { ViewType } from 'src/app/core/enum/view-type';

@Component({
  selector: 'app-live-stream-favorites',
  templateUrl: './live-stream-favorites.component.html',
  styleUrls: ['./live-stream-favorites.component.css']
})
export class LiveStreamFavoritesComponent extends BaseComponent implements OnInit, OnDestroy {

  private subs = new SubSink();
  viewType: string;

  constructor(protected store: Store, private route: ActivatedRoute) {
    super(store);
  }

  ngOnInit() {
    this.route.queryParams.subscribe(
      params => {
        this.viewType = params['view'] ? params['view'] : ViewType.TILE;
      }
    );
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

}
