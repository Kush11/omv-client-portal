import { Component, OnInit } from '@angular/core';
import { SubSink } from 'subsink/dist/subsink';
import { ActivatedRoute, Router } from '@angular/router';
import { Store, Select } from '@ngxs/store';
import { GetLiveStreamFavorites, ClearSelectedLiveStreams, RemoveLiveStreamFavorite, AddSelectedLiveStreams, RemoveSelectedLiveStreams } from '../../state/live-stream/live-stream.action';
import { LiveStreamState } from '../../state/live-stream/live-stream.state';
import { Observable } from 'rxjs';
import { LiveStream } from 'src/app/core/models/entity/live-stream';
import { GridColumn } from 'src/app/core/models/grid.column';
import { Tag } from 'src/app/core/models/entity/tag';
declare var require: any;


@Component({
  selector: 'app-live-stream-favorites-listview',
  templateUrl: './live-stream-favorites-listview.component.html',
  styleUrls: ['./live-stream-favorites-listview.component.css']
})
export class LiveStreamFavoritesListviewComponent implements OnInit {

  @Select(LiveStreamState.getLiveStreamFavorites) favoriteLiveStream$: Observable<LiveStream[]>;
  @Select(LiveStreamState.getTotalStream) totalStream$: Observable<number>;
  @Select(LiveStreamState.isFilterCleared) isFilterCleared$: Observable<boolean>;
  @Select(LiveStreamState.isLiveStreamSelectionCleared) isLiveStreamSelectionCleared$: Observable<boolean>;
  @Select(LiveStreamState.getFavoriteActionSuccess) favoriteActionSuccess$: Observable<boolean>;
  @Select(LiveStreamState.getSelectedItems) selectedItems$: Observable<LiveStream[]>;

  private subs = new SubSink();
  currentPage = 1;
  pageSize = 12;
  totalStream: number;
  pageCount = 5;
  selectedIds: any[];
  liveStream: any[];
  columns: GridColumn[] = [
    { type: 'checkbox', headerText: 'Select All', width: 70, field: '' },
    { type: 'favorite', field: 'isFavorite', width: 60, action: this.removeFavoriteEvent.bind(this) },
    { headerText: 'Name', field: 'name', useAsLink: true, action: this.viewStream.bind(this) }
  ];

  constructor(private store: Store, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.subs.add(
      this.route.queryParams
        .subscribe(params => {
          const { pageNumber } = params;
          this.currentPage = pageNumber || 1;
          this.store.dispatch(new GetLiveStreamFavorites(this.currentPage, this.pageSize));
        }),
      this.isFilterCleared$
        .subscribe(isFilterCleared => {
          if (isFilterCleared) {
            this.store.dispatch(new GetLiveStreamFavorites(this.currentPage, this.pageSize));
          }
        }),
      this.isLiveStreamSelectionCleared$
        .subscribe(isCleared => {
          if (isCleared) {
            this.store.dispatch(new GetLiveStreamFavorites(this.currentPage, this.pageSize));
          }
        }),
      this.favoriteActionSuccess$
        .subscribe(success => {
          if (success) {
            this.store.dispatch(new GetLiveStreamFavorites(this.currentPage, this.pageSize));
          }
        }),
      this.selectedItems$
        .subscribe(items => this.selectedIds = items.map(i => i.id))
    );
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
    this.store.dispatch(new ClearSelectedLiveStreams());
  }

  rowSelectedEvent(streams: LiveStream[]) {
    this.store.dispatch(new AddSelectedLiveStreams(streams));
  }

  rowDeselectedEvent(streams: LiveStream[]) {
    this.store.dispatch(new RemoveSelectedLiveStreams(streams));
  }

  removeFavoriteEvent(item: LiveStream) {
    this.store.dispatch(new RemoveLiveStreamFavorite(item.favoriteId));
  }

  viewStream(stream: LiveStream) {
    this.router.navigate(['/live-stream/viewer'], { queryParams: { items: stream.id.toString() } });
  }

  private buildFilterTags(data: string): Tag[] {
    return JSON.parse(data) as Tag[];
  }
}
