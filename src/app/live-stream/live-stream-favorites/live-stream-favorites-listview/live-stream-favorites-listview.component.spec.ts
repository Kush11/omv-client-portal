import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LiveStreamFavoritesListviewComponent } from './live-stream-favorites-listview.component';

describe('LiveStreamFavoritesListviewComponent', () => {
  let component: LiveStreamFavoritesListviewComponent;
  let fixture: ComponentFixture<LiveStreamFavoritesListviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LiveStreamFavoritesListviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LiveStreamFavoritesListviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
