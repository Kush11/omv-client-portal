import { Component, OnInit, OnDestroy } from '@angular/core';
import { SubSink } from 'subsink/dist/subsink';
import { Select, Store } from '@ngxs/store';
import { LiveStreamState } from '../../state/live-stream/live-stream.state';
import { Observable } from 'rxjs/internal/Observable';
import { LiveStream } from 'src/app/core/models/entity/live-stream';
import { Router, ActivatedRoute } from '@angular/router';
import { GetLiveStreamFavorites, ClearSelectedLiveStreams, AddSelectedLiveStream, RemoveSelectedLiveStream, RemoveLiveStreamFavorite } from '../../state/live-stream/live-stream.action';
import { Tag } from 'src/app/core/models/entity/tag';

@Component({
  selector: 'app-live-stream-favorites-tileview',
  templateUrl: './live-stream-favorites-tileview.component.html',
  styleUrls: ['./live-stream-favorites-tileview.component.css']
})
export class LiveStreamFavoritesTileviewComponent implements OnInit, OnDestroy {

  private subs = new SubSink();
  currentPage: number;
  pageSize = 8;
  totalStream: number;
  pageCount = 5;
  selectedItems: LiveStream[];

  @Select(LiveStreamState.getLiveStreamFavorites) favoriteLiveStream$: Observable<LiveStream[]>;
  @Select(LiveStreamState.getTotalStream) totalStream$: Observable<number>;
  @Select(LiveStreamState.isFilterCleared) isFilterCleared$: Observable<boolean>;
  @Select(LiveStreamState.isLiveStreamSelectionCleared) isLiveStreamSelectionCleared$: Observable<boolean>;
  @Select(LiveStreamState.getFavoriteActionSuccess) favoriteActionSuccess$: Observable<boolean>;
  @Select(LiveStreamState.getSelectedItems) selectedItems$: Observable<LiveStream[]>;

  constructor(private store: Store, private router: Router, private route: ActivatedRoute) { }

  ngOnInit() {
    this.subs.add(
      this.route.queryParams
        .subscribe(params => {
          const { pageNumber } = params;
          this.currentPage = pageNumber || 1;
          this.store.dispatch(new GetLiveStreamFavorites(this.currentPage, this.pageSize));
        }),
      this.totalStream$
        .subscribe(totalStream => {
          this.totalStream = totalStream;
        }),
      this.isFilterCleared$
        .subscribe(isFilterCleared => {
          if (isFilterCleared) {
            this.store.dispatch(new GetLiveStreamFavorites(this.currentPage, this.pageSize));
          }
        }),
      this.isLiveStreamSelectionCleared$
        .subscribe(isCleared => {
          if (isCleared) {
            this.store.dispatch(new GetLiveStreamFavorites(this.currentPage, this.pageSize));
          }
        }),
      this.favoriteActionSuccess$
        .subscribe(success => {
          if (success) {
            this.store.dispatch(new GetLiveStreamFavorites(this.currentPage, this.pageSize));
          }
        }),
      this.selectedItems$
        .subscribe(items => this.selectedItems = items)
    );
  }


  ngOnDestroy() {
    this.store.dispatch(new ClearSelectedLiveStreams());
    this.subs.unsubscribe();
  }

  toggleCheckBoxEvent(item: any) {
    if (!this.selectedItems.includes(item)) {
      this.store.dispatch(new AddSelectedLiveStream(item));
    } else {
      this.store.dispatch(new RemoveSelectedLiveStream(item));
    }
  }

  removeFavoriteEvent(item: LiveStream) {
    this.store.dispatch(new RemoveLiveStreamFavorite(item.favoriteId));
  }

  viewStream(stream: LiveStream) {
    if(!stream.isMultiple){
    this.router.navigate(['/live-stream/viewer'], { queryParams: { items: stream.id.toString() } });
    }else{

      this.router.navigate(['/live-stream/viewer'], { queryParams: { items: stream.favouriteIds.toString() } });
    }
  }

}
