import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LiveStreamFavoritesTileviewComponent } from './live-stream-favorites-tileview.component';

describe('LiveStreamFavoritesTileviewComponent', () => {
  let component: LiveStreamFavoritesTileviewComponent;
  let fixture: ComponentFixture<LiveStreamFavoritesTileviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LiveStreamFavoritesTileviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LiveStreamFavoritesTileviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
