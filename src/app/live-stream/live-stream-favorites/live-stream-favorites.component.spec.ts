import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LiveStreamFavoritesComponent } from './live-stream-favorites.component';

describe('LiveStreamFavoritesComponent', () => {
  let component: LiveStreamFavoritesComponent;
  let fixture: ComponentFixture<LiveStreamFavoritesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LiveStreamFavoritesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LiveStreamFavoritesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
