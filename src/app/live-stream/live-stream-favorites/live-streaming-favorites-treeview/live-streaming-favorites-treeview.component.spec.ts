import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LiveStreamingFavoritesTreeviewComponent } from './live-streaming-favorites-treeview.component';

describe('LiveStreamingFavoritesTreeviewComponent', () => {
  let component: LiveStreamingFavoritesTreeviewComponent;
  let fixture: ComponentFixture<LiveStreamingFavoritesTreeviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LiveStreamingFavoritesTreeviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LiveStreamingFavoritesTreeviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
