import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LiveStreamComponent } from './live-stream.component';
import { LiveStreamFavoritesComponent } from './live-stream-favorites/live-stream-favorites.component';
import { CurrentlyStreamingComponent } from './currently-streaming/currently-streaming.component';
import { LiveStreamViewerComponent } from './live-stream-viewer/live-stream-viewer.component';
import { AuthGuard } from '../core/guards/auth-guard.service';

const routes: Routes = [
  {
    path: '',
    component: LiveStreamComponent,
    children: [
      { path: '', redirectTo: 'currently-streaming', pathMatch: 'full' },
      { path: 'currently-streaming', component: CurrentlyStreamingComponent, canActivate: [AuthGuard] },
      { path: 'favorites', component: LiveStreamFavoritesComponent, canActivate: [AuthGuard] }
    ],

  },
  {
    path: ':id/viewer',
    component: LiveStreamViewerComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'viewer',
    component: LiveStreamViewerComponent,
    canActivate: [AuthGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LiveStreamRoutingModule { }
