import { Breadcrumb } from 'src/app/core/models/breadcrumb';
import { State, Selector, StateContext, Action } from '@ngxs/store';
import {
  UpdateBreadcrumb, GetLiveStreams, ApplyFilters, GetLiveStream, AddSelectedLiveStream, RemoveSelectedLiveStream,
  SetSelectedLiveStreamItems, ClearSelectedLiveStreams, AddLiveStreamFavorite, ResetFavoriteActions, GetLiveStreamFavorites,
  RemoveLiveStreamFavorite, ClearLiveStreamPlaylist, AddSelectedLiveStreams, RemoveSelectedLiveStreams, ShowFilters, HideFilters,
  AddFilterTag, RemoveFilterTag, ClearFilterTags, SetSavedFilterAction, SetBreadcrumbs, GetLiveStreamPlaylist, GetLiveStreamFilters
} from './live-stream.action';
import { LiveStreamService } from './../../../core/services/business/live-stream/live-stream.service'
import { tap } from 'rxjs/operators';
import { LiveStream } from 'src/app/core/models/entity/live-stream';
import { HideSpinner, ShowErrorMessage, ShowSpinner, ShowWarningMessage, ShowSuccessMessage } from 'src/app/state/app.actions';
import { Lookup } from 'src/app/core/models/entity/lookup';
import { Tag } from 'src/app/core/models/entity/tag';
import { LookupService } from 'src/app/core/services/business/lookup/lookup.service';
import { FieldsService } from 'src/app/core/services/business/fields/fields.service';
import { SortType, SortDirection } from 'src/app/core/enum/sort-enum';

export class LiveStreamStateModel {
  // breadcrumbs
  breadcrumbs: Breadcrumb[];
  totalStreams: number;
  favorites: any[];
  filterFields: [];
  favoriteActionSuccess: boolean;

  filterAssets: Lookup[];
  filterGroups: Lookup[];
  filterTags: Tag[];
  showFilterMenu: boolean;
  isFilterApplied: boolean;
  isSavedFilterApplied: boolean;
  isFilterCleared: boolean;
  liveStreams: LiveStream[];
  isLiveStreamSelectionCleared: boolean;
  selectedItems: LiveStream[];
  currentLiveStream: LiveStream;
  playlist: LiveStream[];
}

@State<LiveStreamStateModel>({
  name: 'liveStream',
  defaults: {
    // breadcrumbs
    breadcrumbs: [],
    totalStreams: 0,
    favorites: [],

    //filter
    filterAssets: [],
    filterGroups: [],
    filterFields: [],
    filterTags: [],
    favoriteActionSuccess: false,
    showFilterMenu: false,
    isFilterApplied: false,
    isSavedFilterApplied: false,
    isFilterCleared: false,

    //live Streams
    liveStreams: [],
    isLiveStreamSelectionCleared: false,
    selectedItems: [],
    currentLiveStream: null,
    playlist: []
  }
})

export class LiveStreamState {

  //#region S E L E C T O R S

  @Selector()
  static getBreadcrumbs(state: LiveStreamStateModel) {
    return state.breadcrumbs;
  }

  @Selector()
  static getTotalStream(state: LiveStreamStateModel) {
    return state.totalStreams;
  }

  @Selector()
  static getLiveStreams(state: LiveStreamStateModel) {
    return state.liveStreams;
  }

  @Selector()
  static getSelectedItems(state: LiveStreamStateModel) {
    return state.selectedItems;
  }

  @Selector()
  static getPlaylist(state: LiveStreamStateModel) {
    return state.playlist;
  }

  @Selector()
  static getCurrentLiveStream(state: LiveStreamStateModel) {
    return state.currentLiveStream
  }

  // favorites 

  @Selector()
  static getLiveStreamFavorites(state: LiveStreamStateModel) {
    return state.favorites;
  }

  @Selector()
  static getFavoriteActionSuccess(state: LiveStreamStateModel) {
    return state.favoriteActionSuccess;
  }

  @Selector()
  static isLiveStreamSelectionCleared(state: LiveStreamStateModel) {
    return state.isLiveStreamSelectionCleared
  }

  // filters

  @Selector()
  static getFilterFields(state: LiveStreamStateModel) {
    return state.filterFields;
  }

  @Selector()
  static getFilterAssets(state: LiveStreamStateModel) {
    return state.filterAssets;
  }

  @Selector()
  static getFilterGroups(state: LiveStreamStateModel) {
    return state.filterGroups;
  }

  @Selector()
  static getFilterTags(state: LiveStreamStateModel) {
    return state.filterTags;
  }

  @Selector()
  static showFilterMenu(state: LiveStreamStateModel) {
    return state.showFilterMenu;
  }

  @Selector()
  static isFilterApplied(state: LiveStreamStateModel) {
    return state.isFilterApplied;
  }

  @Selector()
  static isFilterCleared(state: LiveStreamStateModel) {
    return state.isFilterCleared;
  }

  static isSavedFilterApplied(state: LiveStreamStateModel) {
    return state.isSavedFilterApplied;
  }

  //#endregion

  constructor(private liveStreamService: LiveStreamService, private lookupService: LookupService, private fieldsService: FieldsService) { }

  //#region A C T I O N S

  //#region Breadcrumb

  @Action(SetBreadcrumbs)
  setBreadcrumbs(ctx: StateContext<LiveStreamStateModel>, { breadcrumbs }: SetBreadcrumbs) {
    const state = ctx.getState();
    ctx.setState({
      ...state,
      breadcrumbs: breadcrumbs
    });
  }

  @Action(UpdateBreadcrumb)
  updateBreadcrumb(ctx: StateContext<LiveStreamStateModel>, { breadcrumb }: UpdateBreadcrumb) {
    const state = ctx.getState();
    let breadcrumbs = state.breadcrumbs.filter(x => !x.isFinal);
    breadcrumbs.push(breadcrumb);
    ctx.setState({
      ...state,
      breadcrumbs: breadcrumbs
    });
  }

  //#endregion

  @Action(GetLiveStreams)
  getLiveStreams(ctx: StateContext<LiveStreamStateModel>, { asset, group, pageNumber, pageSize }: GetLiveStreams) {
    const state = ctx.getState();
    const isFilterApplied = state.isFilterApplied;
    if (isFilterApplied) {
      let tags = [...state.filterTags];
      ctx.dispatch(new ApplyFilters(tags, pageNumber, pageSize));
    } else {
      ctx.dispatch(new ShowSpinner());
      return this.liveStreamService.getLiveStreams(asset, group, pageNumber, pageSize)
        .pipe(
          tap(response => {
            if (!response) return;
            let liveStream = response.data;
            const selectedItemIds = state.selectedItems.map(x => x.id);
            liveStream.forEach(m => {
              if (selectedItemIds.includes(m.id)) m.isChecked = true;
            });
            ctx.setState({
              ...state,
              liveStreams: liveStream,
              totalStreams: liveStream.length,
            });
            ctx.dispatch(new HideSpinner());
          }, error => ctx.dispatch(new ShowErrorMessage(error)))
        );
    }
  }

  @Action(GetLiveStreamPlaylist)
  getPlaylist(ctx: StateContext<LiveStreamStateModel>, { cameraIds }: GetLiveStreamPlaylist) {
    return this.liveStreamService.getPlaylist(cameraIds)
      .pipe(
        tap(response => {
          const state = ctx.getState();
          ctx.setState({
            ...state,
            playlist: response
          });
          ctx.dispatch(new HideSpinner());
        }, error => ctx.dispatch(new ShowErrorMessage(error)))
      );
  }

  @Action(ClearLiveStreamPlaylist)
  clearPlaylist(ctx: StateContext<LiveStreamStateModel>) {
    const state = ctx.getState();
    ctx.setState({
      ...state,
      playlist: []
    });
  }

  //#region Live Stream Item

  @Action(GetLiveStream)
  getLiveStream(ctx: StateContext<LiveStreamStateModel>, { id }: GetLiveStream) {
    return this.liveStreamService.getLiveStream(id)
      .pipe(
        tap(item => {
          const state = ctx.getState();
          ctx.setState({
            ...state,
            currentLiveStream: item
          });
        }, error => ctx.dispatch(new ShowErrorMessage(error)))
      );
  }

  @Action(AddSelectedLiveStream)
  addSelectedItem({ getState, setState }: StateContext<LiveStreamStateModel>, { item }: AddSelectedLiveStream) {
    const state = getState();
    let items = [...state.selectedItems];
    if (items.includes(item)) return;
    items.push(item);
    setState({
      ...state,
      selectedItems: items,
      isLiveStreamSelectionCleared: false
    });
  }

  @Action(AddSelectedLiveStreams)
  addSelectedItems({ getState, setState }: StateContext<LiveStreamStateModel>, { items }: AddSelectedLiveStreams) {
    const state = getState();
    let stateItems = [...state.selectedItems];
    items.forEach(item => {
      const stateItemsIds = stateItems.map(x => x.id);
      stateItems = !stateItemsIds.includes(item.id) ? [...stateItems, item] : stateItems;
    });
    setState({
      ...state,
      selectedItems: stateItems,
      isLiveStreamSelectionCleared: false
    });
  }

  @Action(RemoveSelectedLiveStream)
  removeSelectedLiveStreamItem({ getState, setState }: StateContext<LiveStreamStateModel>, { item }: RemoveSelectedLiveStream) {
    const state = getState();
    let items = [...state.selectedItems];
    console.log('LiveStreamState items before: ', items);
    items = items.filter(x => x.id !== item.id);
    console.log('LiveStreamState items removed: ', items);
    setState({
      ...state,
      selectedItems: items
    });
  }

  @Action(RemoveSelectedLiveStreams)
  removeSelectedLiveStreamItems({ getState, setState }: StateContext<LiveStreamStateModel>, { items }: RemoveSelectedLiveStreams) {
    const state = getState();
    let stateItems = [...state.selectedItems];
    console.log('RemoveSelectedLiveStreams items before: ', stateItems);
    items.forEach(item => {
      const stateItemsIds = stateItems.map(x => x.id);
      stateItems = stateItemsIds.includes(item.id) ? stateItems.filter(x => x.id !== item.id) : stateItems;
    });
    console.log('RemoveSelectedLiveStreams items removed: ', stateItems);
    setState({
      ...state,
      selectedItems: stateItems
    });
  }

  @Action(SetSelectedLiveStreamItems)
  setSelectedLiveStreamItems({ getState, setState }: StateContext<LiveStreamStateModel>, { items }: SetSelectedLiveStreamItems) {
    const state = getState();
    setState({
      ...state,
      selectedItems: items,
      isLiveStreamSelectionCleared: false
    });
  }

  @Action(ClearSelectedLiveStreams)
  clearSelectedLiveStreamItems({ getState, setState }: StateContext<LiveStreamStateModel>) {
    const state = getState();
    setState({
      ...state,
      selectedItems: [],
      isLiveStreamSelectionCleared: true,
    });
  }
  //#endregion

  //#region Livestream Favorites

  @Action(GetLiveStreamFavorites)
  getLiveStreamFavorites(ctx: StateContext<LiveStreamStateModel>, { pageNumber, pageSize }: GetLiveStreamFavorites) {
    return this.liveStreamService.getLiveStreamFavorites(pageNumber, pageSize).pipe(
      tap(response => {
        if (!response) return;
        let liveStream = response.data;
        const state = ctx.getState();
        ctx.setState({
          ...state,
          favorites: liveStream,
          totalStreams: liveStream.length
        });
        ctx.dispatch(new HideSpinner());
      }, err => {
        ctx.dispatch(new ShowErrorMessage(err));
      })
    );
  }

  @Action(AddLiveStreamFavorite)
  addLiveStreamFavorite(ctx: StateContext<LiveStreamStateModel>, { id, name, livestreamIds  }: AddLiveStreamFavorite) {
    ctx.dispatch(new ShowSpinner());
    return this.liveStreamService.addLiveStreamFavorite(id, name, livestreamIds).pipe(
      tap(() => {
        const state = ctx.getState();
        ctx.setState({
          ...state,
          favoriteActionSuccess: true
        });
        ctx.dispatch(new HideSpinner());
        ctx.dispatch(new ShowSuccessMessage('Favorite livestream was successfully saved.'));
        ctx.dispatch(new ResetFavoriteActions());
      }, error => {
        ctx.dispatch(new ShowErrorMessage(error));
      })
    );
  }

  @Action(RemoveLiveStreamFavorite)
  removeLiveStreamFavorite(ctx: StateContext<LiveStreamStateModel>, { id }: RemoveLiveStreamFavorite) {
    ctx.dispatch(new ShowSpinner());
    return this.liveStreamService.removeLiveStreamFavorite(id).pipe(
      tap(() => {
        const state = ctx.getState();
        ctx.setState({
          ...state,
          favoriteActionSuccess: true
        });
        ctx.dispatch(new HideSpinner());
        ctx.dispatch(new ResetFavoriteActions());
      }, error => ctx.dispatch(new ShowErrorMessage(error)))
    );
  }


  @Action(ResetFavoriteActions)
  resetFavoriteActions(ctx: StateContext<LiveStreamStateModel>) {
    const state = ctx.getState();
    ctx.setState({
      ...state,
      favoriteActionSuccess: false
    });
  }
  //#endregion

  //#region FILTERS

  @Action(GetLiveStreamFilters)
  getStreamingArchiveAssets({ getState, setState }: StateContext<LiveStreamStateModel>) {
    return this.lookupService.getLiveStreamAssets().toPromise()
      .then(assets => {
        this.lookupService.getLiveStreamGroups().toPromise()
          .then(groups => {
            const state = getState();
            setState({
              ...state,
              filterAssets: assets,
              filterGroups: groups
            });
          });
      });
  }

  @Action(ShowFilters)
  showFilters({ getState, setState }: StateContext<LiveStreamStateModel>) {
    const state = getState();
    setState({
      ...state,
      showFilterMenu: true
    });
  }

  @Action(HideFilters)
  hideFilters({ getState, setState }: StateContext<LiveStreamStateModel>) {
    const state = getState();
    setState({
      ...state,
      showFilterMenu: false
    });
  }

  @Action(AddFilterTag)
  addFilterTag({ getState, setState }: StateContext<LiveStreamStateModel>, { tag }: AddFilterTag) {
    const state = getState();
    let tags = [...state.filterTags];
    const tagNames = tags.map(t => t.name);
    if (tagNames.includes(tag.name)) {
      tags = tags.filter(t => t.name !== tag.name);
    }
    tags = [...tags, tag];
    tags = this.fieldsService.sort(tags, 'order', SortType.Number, SortDirection.Ascending);
    setState({
      ...state,
      filterTags: tags,
      isFilterCleared: false
    });
  }

  @Action(RemoveFilterTag)
  removeFilterTag({ getState, setState }: StateContext<LiveStreamStateModel>, { tag }: RemoveFilterTag) {
    const state = getState();
    let tags = state.filterTags;
    tags = tags.filter(x => (x.name !== tag.name || x.value !== tag.value));
    if (tags.length > 0) {
      setState({
        ...state,
        filterTags: tags
      });
    } else {
      setState({
        ...state,
        isFilterApplied: false,
        filterTags: [],
        isFilterCleared: true
      });
    }
  }

  @Action(ClearFilterTags)
  clearFilterTags({ getState, setState }: StateContext<LiveStreamStateModel>) {
    const state = getState();
    setState({
      ...state,
      isFilterApplied: false,
      filterTags: [],
      isFilterCleared: true
    });
  }

  @Action(ApplyFilters)
  applyFilters(ctx: StateContext<LiveStreamStateModel>, { tags, pageNumber, pageSize }: ApplyFilters) {
    ctx.dispatch(new ShowSpinner());
    return this.liveStreamService.applyFilters(tags, pageNumber, pageSize)
      .pipe(
        tap(response => {
          if (!response) return;
          const state = ctx.getState();
          let liveStream = response.data;
          const selectedItemIds = state.selectedItems.map(x => x.id);
          liveStream.forEach(m => {
            if (selectedItemIds.includes(m.id)) m.isChecked = true;
          });
          ctx.setState({
            ...state,
            liveStreams: liveStream,
            totalStreams: liveStream.length
          });
          ctx.dispatch(new HideSpinner());
          ctx.dispatch(new HideFilters());
        }, error => ctx.dispatch(new ShowErrorMessage(error)))
      );
  }

  @Action(SetSavedFilterAction)
  setSavedFilterAction(ctx: StateContext<LiveStreamStateModel>, { isSavedFilterApplied }: SetSavedFilterAction) {
    const state = ctx.getState();
    ctx.setState({
      ...state,
      isSavedFilterApplied: isSavedFilterApplied
    });
  }

  //#endregion

}