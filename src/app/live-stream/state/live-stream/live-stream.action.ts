import { Breadcrumb } from 'src/app/core/models/breadcrumb';
import { Tag } from 'src/app/core/models/entity/tag';
import { LiveStream } from 'src/app/core/models/entity/live-stream';

//#region BreadCrumb

export class SetBreadcrumbs {
  static readonly type = '[LiveStream] SetBreadcrumbs';

  constructor(public breadcrumbs: Breadcrumb[]) { }
}

export class UpdateBreadcrumb {
  static readonly type = '[LiveStream] UpdateBreadcrumb';

  constructor(public breadcrumb: Breadcrumb) { }
}

//#endregion


//#region Filters

export class GetLiveStreamFilters {
  static readonly type = '[Live Stream] GetLiveStreamFilters';
}

export class GetLiveStreamFilterFields {
  static readonly type = '[Live Stream] GetFilterFields';
}

export class AddFilterTag {
  static readonly type = '[Live Stream] AddFilterTag';

  constructor(public tag: Tag) { }
}

export class RemoveFilterTag {
  static readonly type = '[Live Stream] RemoveFilterTag';

  constructor(public tag: Tag) { }
}

export class ClearFilterTags {
  static readonly type = '[Live Stream] ClearFilterTags';
}

export class ShowFilters {
  static readonly type = '[Live Stream] ShowFilters';
}

export class HideFilters {
  static readonly type = '[Live Stream] HideFilters';
}

export class ApplyFilters {
  static readonly type = '[Live Stream] ApplyFilters';

  constructor(public tags: Tag[], public pageNumber?: number, public pageSize?: number) { }
}

export class SetSavedFilterAction {
  static readonly type = '[Live Stream] SetSavedFilterAction';

  constructor(public isSavedFilterApplied: boolean) { }
}

//#endregion

export class AddSelectedLiveStream {
  static readonly type = '[Live Stream] AddSelectedLiveStream';

  constructor(public item: LiveStream) { }
}

export class AddSelectedLiveStreams {
  static readonly type = '[Live Stream] AddSelectedLiveStreams';

  constructor(public items: LiveStream[]) { }
}

export class RemoveSelectedLiveStream {
  static readonly type = '[Live Stream] RemoveSelectedLiveStream';

  constructor(public item: LiveStream) { }
}

export class RemoveSelectedLiveStreams {
  static readonly type = '[Live Stream] RemoveSelectedLiveStreams';

  constructor(public items: LiveStream[]) { }
}

export class SetSelectedLiveStreamItems {
  static readonly type = '[Live Stream] SetSelectedLiveStreamItems';

  constructor(public items: LiveStream[]) { }
}

export class ClearSelectedLiveStreams {
  static readonly type = '[Live Stream] ClearSelectedLiveStreams';
}

export class GetLiveStreams {
  static readonly type = '[Live Stream] GetLiveStreams';

  constructor(public asset?: string, public group?: string, public pageNumber?: number, public pageSize?: number) { }
}

export class GetLiveStreamPlaylist {
  static readonly type = '[Live Stream] GetLiveStreamPlaylist';

  constructor(public cameraIds: number[]) { }
}

export class ClearLiveStreamPlaylist {
  static readonly type = '[Live Stream] ClearLiveStreamPlaylist';
}

export class GetLiveStream {
  static readonly type = '[Live Stream] GetLiveStream';

  constructor(public id: any) { }
}

export class ClearCurrentLiveStreamItem {
  static readonly type = '[Live Stream] ClearCurrentLiveStreamItem';
}

//#region LiveStream Item 

export class SetCurrentLiveStreamItemId {
  static readonly type = '[Live Stream] SetCurrentLiveStreamItemId';

  constructor(public id: any) { }
}

//#endregion


//#region Favorites

export class GetLiveStreamFavorites {
  static readonly type = '[Live Stream] GetLiveStreamFavorites';

  constructor(public pageNumber?: number, public pageSize?: number) { }
}

export class ToggleLiveStreamFavorite {
  static readonly type = '[Live Stream] ToggleFavorite';

  constructor(public id: number, public payload: LiveStream) { }
}

export class AddLiveStreamFavorite {
  static readonly type = '[Live Stream] AddLiveStreamFavorite';

  constructor(public id: any, public name?: string, public livestreamIds?: number[]) { }
}

export class AddLiveStreamFavorites {
  static readonly type = '[LiveStream] AddLiveStreamFavorites';

  constructor(public id: any, public name?: string, public livestreamIds?: number[]) { }
}

export class RemoveLiveStreamFavorite {
  static readonly type = '[Live Stream] RemoveLiveStreamFavorite';

  constructor(public id: number) { }
}

export class ResetFavoriteActions {
  static readonly type = '[Live Stream] ResetFavoriteActions';
}

//#endregion