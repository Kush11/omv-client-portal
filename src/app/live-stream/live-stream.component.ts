import { Component, OnInit, OnDestroy } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { Breadcrumb } from '../core/models/breadcrumb';
import { BaseComponent } from '../shared/base/base.component';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { LiveStreamState } from './state/live-stream/live-stream.state';
import { SetBreadcrumbs, ShowFilters, HideFilters, GetLiveStreamFilters, AddFilterTag, RemoveFilterTag, ApplyFilters, ClearFilterTags } from './state/live-stream/live-stream.action';
import { Tab } from '../core/models/tab';
import { filter } from 'rxjs/operators';
import { LiveStream } from '../core/models/entity/live-stream';
import { SetPreviousRoute } from '../state/app.actions';
import { AppState } from '../state/app.state';
import { FieldConfiguration, FieldType } from '../shared/dynamic-components/field-setting';
import { SubSink } from 'subsink/dist/subsink';
import { Lookup } from '../core/models/entity/lookup';
import { FieldsService } from '../core/services/business/fields/fields.service';
import { Tag } from '../core/models/entity/tag';

const ALL_LINK = '/live-stream/currently-streaming';
const FAVORITES_LINK = '/live-stream/favorites';

@Component({
  selector: 'app-live-stream',
  templateUrl: './live-stream.component.html',
  styleUrls: ['./live-stream.component.css']
})
export class LiveStreamComponent extends BaseComponent implements OnInit, OnDestroy {

  @Select(LiveStreamState.getBreadcrumbs) breadcrumbs$: Observable<Breadcrumb[]>;
  @Select(AppState.getPreviousRoute) previousRoute$: Observable<Breadcrumb[]>;
  @Select(LiveStreamState.getTotalStream) totalStreams$: Observable<number>;
  @Select(LiveStreamState.showFilterMenu) showFilters$: Observable<boolean>;
  @Select(LiveStreamState.getSelectedItems) selectedItems$: Observable<LiveStream[]>;
  @Select(LiveStreamState.showFilterMenu) showFilterMenu$: Observable<boolean>;
  @Select(LiveStreamState.getFilterTags) filterTags$: Observable<Tag[]>;
  @Select(LiveStreamState.getFilterAssets) filterAssets$: Observable<Lookup[]>;
  @Select(LiveStreamState.getFilterGroups) filterGroups$: Observable<Lookup[]>;

  private subs = new SubSink();
  tabs: Tab[] = [
    { link: '/live-stream/currently-streaming', query: 'tile', name: 'Currently Streaming', isActive: true },
    { link: '/live-stream/favorites', query: 'tile', name: 'Favorites' }
  ];
  toolbarItems = [
    { text: 'View', disabled: true, cssClass: '' },
  ];
  filterFields: FieldConfiguration[] = [
    { type: FieldType.Select, name: 'asset', label: 'Asset', options: [], cssClass: 'col-md-3', placeholder: `Please select`, order: 0 },
    { type: FieldType.Select, name: 'group', label: 'Group', options: [], cssClass: 'col-md-3', placeholder: `Please select`, order: 1 },
    { type: FieldType.Input, name: 'query', label: 'Search', value: '', cssClass: 'col-md-3', placeholder: `Type your search`, order: 2 }
  ];
  showTreeView: boolean;
  showMapView: boolean;
  activeView = 'tile';
  selectedItems: LiveStream[];
  showFilterMenu: any;
  filterTags: Tag[];

  constructor(protected store: Store, private router: Router, private route: ActivatedRoute, private fieldsService: FieldsService) {
    super(store);
    this.hideLeftNav();
  }

  ngOnInit() {
    if (!this.tenantPermission.isPermittedLiveStream) {
      this.router.navigate([`/unauthorize`]);
    }
    this.setViewsByUrl(this.router.url);
    this.setBreadCrumbByUrl(this.router.url);
    this.store.dispatch(new GetLiveStreamFilters());

    this.subs.add(
      this.route.queryParams
        .subscribe(params => {
          const view = params.view;
          this.activeView = view ? view : 'tile';
        }),
      this.router.events
        .pipe(
          filter(event => event instanceof NavigationEnd))
        .subscribe((event: NavigationEnd) => {
          this.setViewsByUrl(event.url);
          this.setBreadCrumbByUrl(event.url);
        }),
      this.filterTags$
        .subscribe(tags => this.filterTags = tags),
      this.selectedItems$
        .subscribe(items => {
          this.selectedItems = items;
          this.setToobarProperties();
        }),
      this.showFilterMenu$
        .subscribe(show => this.showFilterMenu = show),
      this.filterAssets$
        .subscribe(assets => {
          this.filterFields[0].options = this.fieldsService.convertLookupsToListItems(assets);
        }),
      this.filterGroups$
        .subscribe(groups => {
          this.filterFields[1].options = this.fieldsService.convertLookupsToListItems(groups);
        })
    );
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
    this.store.dispatch(new SetPreviousRoute('live-stream/currently-streaming'));
  }

  setViewsByUrl(url: string) {
    url = url.split('?')[0];
    if (url === '/live-stream/favorites') {
      this.showTreeView = false;
    } else {
      this.showTreeView = true;
    }
    this.setBreadCrumbByUrl(url);
  }

  setBreadCrumbByUrl(url: string) {
    url = url.split('?')[0];
    let crumbs: Breadcrumb[];
    switch (url) {
      case ALL_LINK:
        crumbs = [
          { link: '/live-stream', name: 'Live Stream' },
          { link: 'currently-streaming', name: 'Current Streaming' }
        ];
        this.store.dispatch(new SetBreadcrumbs(crumbs));
        break;
      case FAVORITES_LINK:
        crumbs = [
          { link: '/live-stream/favorites', name: 'Live Stream' },
          { link: '/live-stream/favorites', name: 'Favorites' }
        ];
        this.store.dispatch(new SetBreadcrumbs(crumbs));
        break;
    }
  }

  switchBreadCrumbs(breadcrumbLink: string) {
    this.router.navigate([breadcrumbLink]);
  }

  switchTabs(tabLink: string) {
    this.router.navigate([tabLink], { queryParams: { view: 'tile' } });
  }

  navigateToView(view: string) {
    const url = this.router.url.split('?')[0];
    this.router.navigate([url], { queryParams: { view: view } });
  }

  toolbarEvent(index: number) {
    switch (index) {
      case 0:
        this.addToPlayBoard();
        break;
      case 1:
        this.collaborate();
        break;
      default:
        break;
    }
  }

  //#region Filters

  addTag(tag: Tag) {
    this.store.dispatch(new AddFilterTag(tag));
  }

  removeTag(tag: Tag) {
    this.store.dispatch(new RemoveFilterTag(tag));
  }

  applyFilters(tags: Tag[]) {
    this.store.dispatch(new ApplyFilters(tags));
  }

  clearFilters() {
    this.store.dispatch(new ClearFilterTags());
    this.store.dispatch(new HideFilters());
  }

  toggleFilterMenu() {
    if (this.showFilterMenu) this.store.dispatch(new HideFilters());
    else this.store.dispatch(new ShowFilters());
  }

  //#endregion

  addToPlayBoard() {
    const param = this.selectedItems.map(x => x.id);
    this.router.navigate(['/live-stream/viewer'], { queryParams: { items: param.toString() } });
  }

  collaborate() {
    console.log("Method not Implemented")
  }

  private setToobarProperties() {
    this.toolbarItems.map((item, index) => {
      switch (index) {
        case 0:
          item.disabled = this.selectedItems.length === 0;
          break;
        case 1:
          item.disabled = true; //this.selectedItems.length === 0;
          break;
        default:
          break;
      }
    });
  }
}
