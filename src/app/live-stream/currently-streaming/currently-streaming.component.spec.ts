import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CurrentlyStreamingComponent } from './currently-streaming.component';

describe('CurrentlyStreamingComponent', () => {
  let component: CurrentlyStreamingComponent;
  let fixture: ComponentFixture<CurrentlyStreamingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CurrentlyStreamingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CurrentlyStreamingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
