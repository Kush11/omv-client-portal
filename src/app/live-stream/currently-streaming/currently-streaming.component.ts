import { Component, OnInit, OnDestroy } from '@angular/core';
import { Store } from '@ngxs/store';
import { ActivatedRoute } from '@angular/router';
import { BaseComponent } from 'src/app/shared/base/base.component';
import { ViewType } from 'src/app/core/enum/view-type';
import { SetPreviousRoute } from 'src/app/state/app.actions';

@Component({
  selector: 'app-currently-streaming',
  templateUrl: './currently-streaming.component.html',
  styleUrls: ['./currently-streaming.component.css']
})
export class CurrentlyStreamingComponent extends BaseComponent implements OnInit, OnDestroy {

  viewType: string;

  constructor(protected store: Store, private route: ActivatedRoute) {
    super(store);
    this.hideSpinner();
    this.setPageTitle("Currently Streaming");
  }

  ngOnInit() {
    this.subsArray.sink =
      this.route.queryParams
        .subscribe(
          params => {
            this.viewType = params['view'] ? params['view'] : ViewType.TILE;
          }
        );
  }

  ngOnDestroy() {
    this.subsArray.unsubscribe();
    this.store.dispatch(new SetPreviousRoute("currently-streaming"))
  }
}
