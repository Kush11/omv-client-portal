import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CurrentlyStreamingListviewComponent } from './currently-streaming-listview.component';

describe('CurrentlyStreamingListviewComponent', () => {
  let component: CurrentlyStreamingListviewComponent;
  let fixture: ComponentFixture<CurrentlyStreamingListviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CurrentlyStreamingListviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CurrentlyStreamingListviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
