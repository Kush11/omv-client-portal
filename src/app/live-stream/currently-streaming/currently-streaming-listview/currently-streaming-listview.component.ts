import { Component, OnInit, OnDestroy } from '@angular/core';
import { SubSink } from 'subsink/dist/subsink';
import { LiveStreamState } from '../../state/live-stream/live-stream.state';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { GridColumn } from 'src/app/core/models/grid.column';
import { LiveStream } from 'src/app/core/models/entity/live-stream';
import { AddLiveStreamFavorite, RemoveLiveStreamFavorite, GetLiveStreams, AddSelectedLiveStreams, RemoveSelectedLiveStreams, ClearSelectedLiveStreams } from '../../state/live-stream/live-stream.action';
import { Router, ActivatedRoute } from '@angular/router';
import { BaseComponent } from 'src/app/shared/base/base.component';

@Component({
  selector: 'app-currently-streaming-listview',
  templateUrl: './currently-streaming-listview.component.html',
  styleUrls: ['./currently-streaming-listview.component.css']
})
export class CurrentlyStreamingListviewComponent extends BaseComponent implements OnInit, OnDestroy {

  @Select(LiveStreamState.getLiveStreams) liveStream$: Observable<LiveStream[]>;
  @Select(LiveStreamState.getTotalStream) totalStreams$: Observable<number>;
  @Select(LiveStreamState.isFilterCleared) isFilterCleared$: Observable<boolean>;
  @Select(LiveStreamState.isLiveStreamSelectionCleared) isLiveStreamSelectionCleared$: Observable<boolean>;
  @Select(LiveStreamState.getSelectedItems) selectedItems$: Observable<LiveStream[]>;

  private subs = new SubSink();
  liveStream: any[];
  currentPage: number;
  pageSize = 8;
  totalStreams: number;
  pageCount = 5;
  selectedItems: any[];
  isPageChangeInvoked: boolean;
  selectedIds: any;
  columns: GridColumn[] = [
    { type: "checkbox", headerText: "Select All", width: 70, field: "" },
    { headerText: '', type: 'favorite', field: "isFavorite", width: 60, action: this.toggleFavoriteEvent.bind(this) },
    { headerText: "Name", field: "name", useAsLink: true, action: this.viewStream.bind(this) }
  ];

  constructor(protected store: Store, private route: ActivatedRoute, private router: Router) {
    super(store);
  }

  ngOnInit() {
    this.subs.add(
      this.route.queryParams
        .subscribe(params => {
          const { pageNumber } = params;
          this.currentPage = pageNumber || 1;
          this.currentPage = Number(this.currentPage);
          this.store.dispatch(new GetLiveStreams(null, null, this.currentPage, this.pageSize));
        }),
      this.isFilterCleared$
        .subscribe(isFilterCleared => {
          if (isFilterCleared) this.store.dispatch(new GetLiveStreams(null, null, this.currentPage, this.pageSize));
        }),
      this.isLiveStreamSelectionCleared$
        .subscribe(isCleared => {
          if (isCleared) this.store.dispatch(new GetLiveStreams(null, null, this.currentPage, this.pageSize));
        }),
      this.selectedItems$
        .subscribe(items => this.selectedIds = items.map(i => i.id))
    );
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
    this.store.dispatch(new ClearSelectedLiveStreams());
  }

  rowSelectedEvent(streams: LiveStream[]) {
    this.store.dispatch(new AddSelectedLiveStreams(streams));
  }

  rowDeselectedEvent(streams: LiveStream[]) {
    this.store.dispatch(new RemoveSelectedLiveStreams(streams));
  }

  // toggleFavoriteEvent(item: LiveStream) {
  //   if (!item.isFavorite) {
  //     this.store.dispatch(new AddLiveStreamFavorite(item.favoriteId));
  //   } else {
  //     this.store.dispatch(new RemoveLiveStreamFavorite(item.favoriteId));
  //   }
  // }

  toggleFavoriteEvent(item: LiveStream) {
    const { id, favoriteId } = item;
    if (!favoriteId) {
      this.store.dispatch(new AddLiveStreamFavorite(id)).toPromise()
        .then(() => {
          this.store.dispatch(new GetLiveStreams(null, null, this.currentPage, this.pageSize));
        });
    } else {
      this.store.dispatch(new RemoveLiveStreamFavorite(favoriteId)).toPromise()
        .then(() => {
          this.store.dispatch(new GetLiveStreams(null, null, this.currentPage, this.pageSize));
        });
    }
  }

  viewStream(stream: LiveStream) {
    this.router.navigate(['/live-stream/viewer'], { queryParams: { items: stream.id.toString() } });
  }
}
