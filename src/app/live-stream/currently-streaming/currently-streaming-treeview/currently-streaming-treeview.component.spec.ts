import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CurrentlyStreamingTreeviewComponent } from './currently-streaming-treeview.component';

describe('CurrentlyStreamingTreeviewComponent', () => {
  let component: CurrentlyStreamingTreeviewComponent;
  let fixture: ComponentFixture<CurrentlyStreamingTreeviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CurrentlyStreamingTreeviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CurrentlyStreamingTreeviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
