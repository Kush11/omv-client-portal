import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { LiveStreamState } from '../../state/live-stream/live-stream.state';
import { Observable } from 'rxjs';
import { Select, Store } from '@ngxs/store';
import { SubSink } from 'subsink/dist/subsink';
import { TileViewComponent } from 'src/app/shared/tile-view/tile-view.component';
import { LiveStream } from 'src/app/core/models/entity/live-stream';
import { RemoveSelectedLiveStream, AddSelectedLiveStream, GetLiveStreams, AddLiveStreamFavorite, RemoveLiveStreamFavorite, ClearSelectedLiveStreams } from '../../state/live-stream/live-stream.action';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-currently-streaming-tileview',
  templateUrl: './currently-streaming-tileview.component.html',
  styleUrls: ['./currently-streaming-tileview.component.css']
})
export class CurrentlyStreamingTileviewComponent implements OnInit, OnDestroy {

  constructor(protected store: Store, private router: Router, private route: ActivatedRoute) { }

  private subs = new SubSink();
  liveStream: LiveStream[];
  currentPage: number;
  pageSize = 8;
  totalStreams: number;
  pageCount = 5;
  selectedItems: LiveStream[];
  isPageChangeInvoked: boolean;
  selectedIds: number[];

  @Select(LiveStreamState.showFilterMenu) showFilters$: Observable<boolean>;
  @Select(LiveStreamState.getLiveStreams) liveStream$: Observable<any[]>;
  @Select(LiveStreamState.getTotalStream) totalStreams$: Observable<number>;
  @Select(LiveStreamState.isFilterCleared) isFilterCleared$: Observable<boolean>;
  @Select(LiveStreamState.isLiveStreamSelectionCleared) isLiveStreamSelectionCleared$: Observable<boolean>;
  @Select(LiveStreamState.getSelectedItems) selectedItems$: Observable<any[]>;

  @ViewChild('tileView') tileView: TileViewComponent;

  ngOnInit() {
    this.subs.add(
      this.route.queryParams
        .subscribe(params => {
          const { pageNumber } = params;
          this.currentPage = pageNumber || 1;
          console.log('CurrentlyStreamingTileviewComponent queryParams: GetLiveStreams');
          this.store.dispatch(new GetLiveStreams(null, null, this.currentPage, this.pageSize));
        }),
      this.totalStreams$
        .subscribe(total => {
          this.totalStreams = total;
        }),
      this.isFilterCleared$
        .subscribe(isFilterCleared => {
          console.log('CurrentlyStreamingTileviewComponent isFilterCleared$: GetLiveStreams ', isFilterCleared);
          if (isFilterCleared) this.store.dispatch(new GetLiveStreams(null, null, this.currentPage, this.pageSize));
        }),
      this.selectedItems$
        .subscribe(items => this.selectedItems = items)
    );
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
    this.store.dispatch(new ClearSelectedLiveStreams());
  }

  toggleFavoriteEvent(item: LiveStream) {
    const { id, favoriteId } = item;
    if (!favoriteId) {
      this.store.dispatch(new AddLiveStreamFavorite(id)).toPromise()
        .then(() => {
          this.store.dispatch(new GetLiveStreams(null, null, this.currentPage, this.pageSize));
        });
    } else {
      this.store.dispatch(new RemoveLiveStreamFavorite(favoriteId)).toPromise()
        .then(() => {
          this.store.dispatch(new GetLiveStreams(null, null, this.currentPage, this.pageSize));
        });
    }
  }


  toggleCheckBoxEvent(item: LiveStream) {
    const selectedIds = this.selectedItems.map(x => x.id);
    if (!selectedIds.includes(item.id)) {
      this.store.dispatch(new AddSelectedLiveStream(item));
    } else {
      this.store.dispatch(new RemoveSelectedLiveStream(item));
    }
  }

  viewStream(stream: LiveStream) {
    this.router.navigate(['/live-stream/viewer'], { queryParams: { items: stream.id.toString() } });
  }
}
