import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CurrentlyStreamingTileviewComponent } from './currently-streaming-tileview.component';

describe('CurrentlyStreamingTileviewComponent', () => {
  let component: CurrentlyStreamingTileviewComponent;
  let fixture: ComponentFixture<CurrentlyStreamingTileviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CurrentlyStreamingTileviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CurrentlyStreamingTileviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
