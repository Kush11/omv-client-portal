import { NgModule, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { LiveStreamRoutingModule } from './live-stream.routing.module';
import { LiveStreamComponent } from './live-stream.component';
import { SharedModule } from '../shared/shared.module';
import { NgxsModule } from '@ngxs/store';
import { LiveStreamState } from './state/live-stream/live-stream.state';
import { CurrentlyStreamingComponent } from './currently-streaming/currently-streaming.component';
import { CurrentlyStreamingListviewComponent } from './currently-streaming/currently-streaming-listview/currently-streaming-listview.component';
import { CurrentlyStreamingTileviewComponent } from './currently-streaming/currently-streaming-tileview/currently-streaming-tileview.component';
import { CurrentlyStreamingTreeviewComponent } from './currently-streaming/currently-streaming-treeview/currently-streaming-treeview.component';
import { LiveStreamFavoritesComponent } from './live-stream-favorites/live-stream-favorites.component';
import { LiveStreamingFavoritesTreeviewComponent } from './live-stream-favorites/live-streaming-favorites-treeview/live-streaming-favorites-treeview.component';
import { LiveStreamFavoritesListviewComponent } from './live-stream-favorites/live-stream-favorites-listview/live-stream-favorites-listview.component';
import { LiveStreamFavoritesTileviewComponent } from './live-stream-favorites/live-stream-favorites-tileview/live-stream-favorites-tileview.component';
import { environment } from 'src/environments/environment';
import { LiveStreamService } from '../core/services/business/live-stream/live-stream.service';
import { LiveStreamDataService } from '../core/services/data/live-stream/live-stream-data.service';
import { LiveStreamMockDataService } from '../core/services/data/live-stream/live-stream-mock.service';
import { LiveStreamWebDataService } from '../core/services/data/live-stream/live-stream-web.service';
import { LiveStreamViewerComponent } from './live-stream-viewer/live-stream-viewer.component';
import { LookupDataService } from '../core/services/data/lookup/lookup.data.service';
import { LookupMockDataService } from '../core/services/data/lookup/lookup.mock.data.service';
import { LookupWebDataService } from '../core/services/data/lookup/lookup.web.data.service';
import { LookupService } from '../core/services/business/lookup/lookup.service';

@NgModule({
  declarations: [LiveStreamComponent, CurrentlyStreamingComponent, CurrentlyStreamingListviewComponent, CurrentlyStreamingTileviewComponent, CurrentlyStreamingTreeviewComponent, LiveStreamFavoritesComponent, LiveStreamingFavoritesTreeviewComponent, LiveStreamFavoritesListviewComponent, LiveStreamFavoritesTileviewComponent, LiveStreamViewerComponent],
  imports: [
    SharedModule,
    LiveStreamRoutingModule,
    NgxsModule.forFeature([LiveStreamState])
  ],
  providers: [
    LiveStreamService,
    { provide: LiveStreamDataService, useClass: environment.useMocks ? LiveStreamMockDataService : LiveStreamWebDataService },
    LookupService,
    { provide: LookupDataService, useClass: environment.useMocks ? LookupMockDataService : LookupWebDataService }
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA]
})
export class LiveStreamModule { }
