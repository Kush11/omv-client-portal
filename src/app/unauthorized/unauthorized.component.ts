import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '../shared/base/base.component';
import { Store } from '@ngxs/store';
import { Router } from '@angular/router';

@Component({
  selector: 'app-unauthorized',
  templateUrl: './unauthorized.component.html',
  styleUrls: ['./unauthorized.component.css']
})
export class UnAuthorizedComponent extends BaseComponent implements OnInit {

  constructor(protected store: Store, private router: Router) {
    super(store);
  }

  ngOnInit() {
    switch (true) {
      case this.tenantPermission.isPermittedDashboard:
        this.router.navigate([`/dashboard`]);
        break;
      case this.tenantPermission.isPermittedMedia:
        this.router.navigate([`/media`]);
        break;
      case this.tenantPermission.isPermittedLiveStream:
        this.router.navigate([`/live-stream`]);
        break;
      case this.tenantPermission.isPermitedWorkPlanning:
        this.router.navigate([`/work-planning`]);
        break;
      case this.tenantPermission.isPermittedCollaborations:
        this.router.navigate([`/collaborations`]);
        break;
      case this.userPermission.canViewAdminPortal:
        this.router.navigate([`/admin`]);
        break;
      default:
        break;
    }

  }

}
