import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { AuthGuard } from '../core/guards/auth-guard.service';
import { WorkItemGroupsComponent } from './work-item-groups/work-item-groups.component';
import { WorksListComponent } from './works-list/works-list.component';
import { WorkEditComponent } from './work-edit/work-edit.component';
import { WorkEditFormsComponent } from './work-edit/work-edit-forms/work-edit-forms.component';
import { WorkEditFindingsComponent } from './work-edit/work-edit-findings/work-edit-findings.component';
import { WorkEditViewerComponent } from './work-edit/work-edit-viewer/work-edit-viewer.component';

const routes: Routes = [
  {
    path: ':parentName',
    component: WorkItemGroupsComponent,
    canActivate: [AuthGuard],
  },
  {
    path: ':parentName/:assetId/works',
    component: WorksListComponent,
    canActivate: [AuthGuard]
  },
  {
    path: ':parentName/:assetId/:workType/:workId',
    component: WorkEditComponent,
    canActivate: [AuthGuard],
    children: [
      { path: '', redirectTo: 'form', pathMatch: 'full' },
      { path: 'form', component: WorkEditFormsComponent, canActivate: [AuthGuard] },
      { path: 'findings', component: WorkEditFindingsComponent, canActivate: [AuthGuard] }
    ]
  },
  {
    path: ':parentName/:assetId/:workType/:workId/:documentId/viewer',
    component: WorkEditViewerComponent,
    canActivate: [AuthGuard]
  },
];


@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class WorkPlanningRoutingModule { }
