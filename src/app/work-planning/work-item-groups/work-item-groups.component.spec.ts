import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkItemGroupsComponent } from './work-item-groups.component';

describe('WorkItemGroupsComponent', () => {
  let component: WorkItemGroupsComponent;
  let fixture: ComponentFixture<WorkItemGroupsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WorkItemGroupsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkItemGroupsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
