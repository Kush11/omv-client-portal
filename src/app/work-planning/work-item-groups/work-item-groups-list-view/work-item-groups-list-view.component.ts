import { Component, OnInit, Input } from '@angular/core';
import { Store, Select } from '@ngxs/store';
import { GridColumn } from 'src/app/core/models/grid.column';
import { WorkPlanningState } from '../../state/work-planning.state';
import { Observable } from 'rxjs/internal/Observable';
import { WorkPlanningWorkItemParent } from 'src/app/core/models/work-planning';
import { GetWorkItemListGroup, SetCurrentWorkAssetName } from '../../state/work-planning.actions';
import { Router } from '@angular/router';

@Component({
  selector: 'app-work-item-groups-list-view',
  templateUrl: './work-item-groups-list-view.component.html',
  styleUrls: ['./work-item-groups-list-view.component.css']
})
export class WorkItemGroupsListViewComponent implements OnInit {

  @Select(WorkPlanningState.getWorkItemListGroup) workItem$: Observable<WorkPlanningWorkItemParent[]>;

  @Input() itemName: string;

  currentPage = 1;
  pageSize = 8;
  pageCount: number;

  columns: GridColumn[] = [
    { headerText: 'Group Name', field: 'itemName' },
    { headerText: 'Cycle 1', field: 'cycle0' },
    { headerText: 'Cycle 2', field: 'cycle1' },
    { headerText: 'Cycle 3', field: 'cycle2' },
    { headerText: 'Cycle 4', field: 'cycle3' },
  ];


  constructor(
    protected store: Store,
    private router: Router
  ) { }

  ngOnInit() {

  }

  editGridEvent(data: WorkPlanningWorkItemParent) {
    this.store.dispatch(new SetCurrentWorkAssetName(data.itemName));
    this.router.navigate([`work-planning/${this.itemName}/${data.itemId}/works`]);
  }

}
