import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkItemGroupsListViewComponent } from './work-item-groups-list-view.component';

describe('WorkItemGroupsListViewComponent', () => {
  let component: WorkItemGroupsListViewComponent;
  let fixture: ComponentFixture<WorkItemGroupsListViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WorkItemGroupsListViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkItemGroupsListViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
