import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkItemGroupsBarchartViewComponent } from './work-item-groups-barchart-view.component';

describe('WorkItemGroupsBarchartViewComponent', () => {
  let component: WorkItemGroupsBarchartViewComponent;
  let fixture: ComponentFixture<WorkItemGroupsBarchartViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WorkItemGroupsBarchartViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkItemGroupsBarchartViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
