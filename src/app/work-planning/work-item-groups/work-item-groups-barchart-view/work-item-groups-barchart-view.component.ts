import { GetWorkItemTileGroup } from './../../state/work-planning.actions';
import { Component, OnInit, ViewChild } from '@angular/core';
import {} from '@syncfusion/ej2-angular-charts';
import { Select, Store } from '@ngxs/store';
import { WorkPlanningState } from '../../state/work-planning.state';
import { Observable } from 'rxjs/internal/Observable';
import { WorkPlanningWorkItemParent } from 'src/app/core/models/work-planning';
import { SubSink } from 'subsink/dist/subsink';

@Component({
  selector: 'app-work-item-groups-barchart-view',
  templateUrl: './work-item-groups-barchart-view.component.html',
  styleUrls: ['./work-item-groups-barchart-view.component.css']
})
export class WorkItemGroupsBarchartViewComponent implements OnInit {

  @Select(WorkPlanningState.getWorkItemTileGroup) workItems$: Observable<WorkPlanningWorkItemParent[]>;

 


  constructor(protected store: Store) {}

  ngOnInit() {
  }
}
