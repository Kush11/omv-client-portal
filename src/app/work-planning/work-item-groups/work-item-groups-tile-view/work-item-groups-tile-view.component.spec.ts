import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkItemGroupsTileViewComponent } from './work-item-groups-tile-view.component';

describe('WorkItemGroupsTileViewComponent', () => {
  let component: WorkItemGroupsTileViewComponent;
  let fixture: ComponentFixture<WorkItemGroupsTileViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WorkItemGroupsTileViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkItemGroupsTileViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
