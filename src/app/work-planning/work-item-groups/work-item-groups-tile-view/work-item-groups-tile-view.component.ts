import { SubSink } from 'subsink/dist/subsink';
import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { Store, Select } from '@ngxs/store';
import { BaseComponent } from 'src/app/shared/base/base.component';
import { Observable } from 'rxjs/internal/Observable';
import { WorkPlanningState } from '../../state/work-planning.state';
import { WorkPlanningWorkItemParent } from 'src/app/core/models/work-planning';
import { GetWorkItemTileGroup, SetCurrentWorkAssetName } from '../../state/work-planning.actions';
import { Router } from '@angular/router';


@Component({
  selector: 'app-work-item-groups-tile-view',
  templateUrl: './work-item-groups-tile-view.component.html',
  styleUrls: ['./work-item-groups-tile-view.component.css']
})
export class WorkItemGroupsTileViewComponent implements OnInit, OnDestroy {

  @Select(WorkPlanningState.getWorkItemTileGroup) workItems$: Observable<WorkPlanningWorkItemParent[]>;

  @Input() itemName: string;

  private subs = new SubSink();
  currentPage = 1;
  pageSize = 8;
  pageCount: number;

  constructor(protected store: Store, private router: Router) { }

  ngOnInit() {
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

  navigate(data: WorkPlanningWorkItemParent) {
  }

  setItemName(data: WorkPlanningWorkItemParent) {
    this.store.dispatch(new SetCurrentWorkAssetName(data.itemName));
  }
}
