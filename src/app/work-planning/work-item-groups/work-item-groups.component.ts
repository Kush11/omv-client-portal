import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ViewType } from 'src/app/core/enum/view-type';
import { BaseComponent } from 'src/app/shared/base/base.component';
import { Store } from '@ngxs/store';
import { SubSink } from 'subsink/dist/subsink';
import { GetWorkItemListGroup, GetWorkItemTileGroup } from '../state/work-planning.actions';
import { ChangedEventArgs } from '@syncfusion/ej2-angular-calendars';

@Component({
  selector: 'app-work-item-groups',
  templateUrl: './work-item-groups.component.html',
  styleUrls: ['./work-item-groups.component.css']
})
export class WorkItemGroupsComponent extends BaseComponent implements OnInit {

  private subs = new SubSink();
  viewType: string;
  name: string;
  parentName: string;
  dateValue = new Date();
  showTable: boolean;

  constructor(protected store: Store, protected router: Router, private route: ActivatedRoute) {
    super(store);
    this.hideLeftNav();
  }

  ngOnInit() {
    if (!this.tenantPermission.isPermitedWorkPlanning) {
      this.router.navigate([`/unauthorize`]);
    }
    this.subs.add(
      this.route.queryParams.subscribe(
        params => {
          this.viewType = params['view'] ? params['view'] :  ViewType.TILE;
          console.log('view type', this.viewType);
        }
      ),
      this.route.paramMap
        .subscribe(params => {
          const parentName = params.get('parentName');
          this.parentName = decodeURIComponent(parentName);
          this.store.dispatch(new GetWorkItemListGroup(this.parentName));
          this.store.dispatch(new GetWorkItemTileGroup(this.parentName));
        })
    );
  }

  onDateFilterChanged(args: ChangedEventArgs) {
    const { value } = args;
    this.dateValue = value || new Date();
  }
}
