import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { BaseComponent } from 'src/app/shared/base/base.component';
import { Select, Store } from '@ngxs/store';
import { WorkPlanningState } from '../state/work-planning.state';
import { Breadcrumb } from 'src/app/core/models/breadcrumb';
import { SubSink } from 'subsink/dist/subsink';
import { ToolBar } from 'src/app/core/models/toolbar-action';
import { GridColumn } from 'src/app/core/models/grid.column';
import { Work } from 'src/app/core/models/entity/work';
import { Router, ActivatedRoute } from '@angular/router';
import { SetPreviousRoute, ShowSuccessMessage, ShowErrorMessage } from 'src/app/state/app.actions';
import { ClearCurrentWorkAssetName, SetCurrentWorkAssetName, GetWorks, SetBreadcrumbs, GetWorkAsset, ClearWorks, SetWorkIds, DownloadWorkReport } from '../state/work-planning.actions';
import { Observable } from 'rxjs/internal/Observable';
import { WorkPlanningService } from 'src/app/core/services/business/work-planning/work-planning.service';
import { ListComponent } from 'src/app/shared/list/list.component';
import { WorkReportType } from 'src/app/core/enum/work-report-type';

@Component({
  selector: 'app-works-list',
  templateUrl: './works-list.component.html',
  styleUrls: ['./works-list.component.css']
})
export class WorksListComponent extends BaseComponent implements OnInit, OnDestroy {

  @Select(WorkPlanningState.getBreadcrumbs) breadcrumbs$: Observable<Breadcrumb[]>;
  @Select(WorkPlanningState.getTotalAssets) totalAssets$: Observable<number>;
  @Select(WorkPlanningState.getCurrentWorkAssetName) currentAssetName$: Observable<string>;
  @Select(WorkPlanningState.getReturnedAssets) returnedAssets$: Observable<number>;
  @Select(WorkPlanningState.getWorks) works$: Observable<Work[]>;
  @Select(WorkPlanningState.getTotalWorks) totalWorks$: Observable<number>;

  @ViewChild('list') list: ListComponent;

  private subs = new SubSink();
  assetId: number;
  showCurrentInspection: boolean;
  showFutureInspection: boolean;
  showCompletedInspection: boolean;
  lastBreadcrumb: Breadcrumb;
  filterTags: any[] = [];
  currentAssetName: string = '';
  pageCount: number;
  pageSize: 25;
  totalCount: number = 0;
  pageTotal: number;
  currentPage: number;
  returnedAssets: number;
  showFilterMenu: boolean = false;
  toolbarItems: ToolBar[] = [
    { text: 'Inspection Export', cssClass: 'xls-icon-qa inspection-export-qa', icon: './assets/images/xlsicon.svg', action: this.exportInspection.bind(this) },
    { text: 'Inspection Checklist', cssClass: 'xls-icon-qa inspection-checklist-qa', icon: './assets/images/xlsicon.svg', action: this.checkList.bind(this) },
    { text: 'Action List', cssClass: 'action-list-qa', toggle: true, action: this.toggleActionList.bind(this) },
  ];
  columns: GridColumn[] = [
    { headerText: 'Inspection Item', field: 'name', cssClass: 'item-field-color', useAsLink: true },
    { headerText: 'Type', field: 'processCode', filterType: 'checkbox', width: '150' },
    { headerText: 'Findings', field: 'findingsText', filterType: 'checkbox', width: '150' },
    { headerText: 'Current Step', field: 'currentStep', filterType: 'checkbox' },
    { headerText: 'Cycle Name', field: 'cycleName', filterType: 'checkbox' },
    { headerText: 'Last Updated', field: 'modifiedOn', type: 'date', format: 'MM/dd/yyyy', ignoreFiltering: true, width: '200' },
    { headerText: 'Complete By', field: 'completeBy', type: 'date', format: 'MM/dd/yyyy', width: '200' }
  ];
  currentInspections = [];
  works: Work[] = [];
  parentName: string;
  currentDate = new Date();
  actionList: boolean;
  pageSizes: number[];
  currentCycle: string;

  constructor(public store: Store, private router: Router, private route: ActivatedRoute,
    private workPlanningService: WorkPlanningService) {
    super(store);
    this.hideLeftNav();
    this.setPageTitle('Works');
  }

  ngOnInit() {
    this.subs.add(
      this.route.paramMap
        .subscribe(params => {
          this.assetId = Number(params.get('assetId'));
          this.parentName = params.get('parentName');
          const cycle = this.route.snapshot.queryParamMap.get('cycle');
          if (cycle) {
            this.currentCycle = cycle;
            this.store.dispatch(new GetWorks(this.assetId, new Date(), this.actionList, cycle));
          } else {
            this.currentCycle = null;
            this.store.dispatch(new GetWorks(this.assetId, new Date()));
          }
          this.store.dispatch(new GetWorkAsset(this.assetId));
        }),
      this.currentAssetName$
        .subscribe(currentAssetName => {
          this.currentAssetName = currentAssetName;
          this.setBreadCrumbs();
        }),
      this.works$
        .subscribe(works => {
          if (works) {
            this.works = works;
            this.pageSizes = works.length <= 100 ? [25, 50, 100] : [25, 50, 100, works.length];
          }
        })
    );
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
    this.store.dispatch(new SetPreviousRoute(this.router.url));
    this.store.dispatch(new ClearCurrentWorkAssetName());
    // this.store.dispatch(new ClearWorks());
  }

  setBreadCrumbs() {
    let crumbs: Breadcrumb[] = [
      { link: this.router.url, name: 'Work Planning' },
      { link: `/work-planning/${this.parentName}`, name: this.parentName || 'Unknown' },
      { link: this.router.url, name: this.currentAssetName }
    ];
    this.store.dispatch(new SetBreadcrumbs(crumbs));
  }

  switchBreadCrumbs(breadcrumbLink: string) {
    this.router.navigate([breadcrumbLink]);
  }

  onDateChanged(date: Date) {
    this.currentDate = date;
    if (this.assetId) this.store.dispatch(new GetWorks(this.assetId, date, this.actionList, this.currentCycle));
  }

  exportInspection() {
    this.store.dispatch(new DownloadWorkReport(this.parentName, this.assetId, this.currentAssetName, WorkReportType.Export, this.currentDate, this.currentCycle));
  }

  checkList() {
    this.store.dispatch(new DownloadWorkReport(this.parentName, this.assetId, this.currentAssetName, WorkReportType.Checklist, this.currentDate, this.currentCycle));
  }

  toggleActionList() {
    this.actionList = !this.actionList;
    this.store.dispatch(new GetWorks(this.assetId, this.currentDate, this.actionList));
  }

  edit(data: Work) {
    this.store.dispatch(new SetCurrentWorkAssetName(data.assetName));
  }

  advance(work: Work) {
    this.performSignOff(work.id);
  }

  performSignOff(workId: number) {
    this.workPlanningService.signOff(workId)
      .then(() => {
        this.store.dispatch(new GetWorks(this.assetId, this.currentDate));
        this.store.dispatch(new ShowSuccessMessage('Operation was successful.'));
        this.hideSpinner();
      }, error => this.store.dispatch(new ShowErrorMessage(error)));
  }


}