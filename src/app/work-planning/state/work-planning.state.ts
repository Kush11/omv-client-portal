import { State, Selector, Action, StateContext } from '@ngxs/store';
import {
  GetWorkPlanningParentItems, SetBreadcrumbs, UpdateBreadcrumb, GetWorks, ClearWorks,
  SetCurrentWorkItemId, GetWorkItemTileGroup, GetWorkItemListGroup, SetCurrentWorkParentItem,
  ClearCurrentWorkParentItem, SetCurrentWorkId, ClearCurrentWorkId, SetCurrentWorkAssetName, ClearCurrentWorkAssetName,
  ClearBreadcrumbs, GetWorkAsset, CreateWorkFile, GetWorkFiles, ClearWorkFile, DownloadFinding, GetCurrentWorkFile, ClearCurrentWorkFile, AddSelectedWorkFile, AddSelectedWorkFiles, RemoveSelectedWorkFile, RemoveSelectedWorkFiles, SetSelectedWorkFile, ClearSelectedMediaItems, DownloadWorkFiles, SetCurrentWork, ClearCurrentWork, SetCurrentWorkParent, SetWorkFormStatus, PerformSignOff, DownloadWorkReport
} from './work-planning.actions';
import { WorkPlanningService } from 'src/app/core/services/business/work-planning/work-planning.service';
import { tap, map } from 'rxjs/operators';
import { WorkPlanningParentItem, WorkPlanningWorkItemParent } from 'src/app/core/models/work-planning';
import { Breadcrumb } from 'src/app/core/models/breadcrumb';
import { ShowSpinner, ShowErrorMessage, HideSpinner, ShowSuccessMessage, ShowDownloadsMessage, SetDownloadStatus } from 'src/app/state/app.actions';
import { MetadataListItemsService } from 'src/app/core/services/business/metadata-list-items/metadata-list-items.service';
import { WorkFile, WorkFilesGroup } from 'src/app/core/models/entity/work-item';
import { MediaService } from 'src/app/core/services/business/media/media.service';
import { MediaItem } from 'src/app/core/models/entity/media';
import { Work } from 'src/app/core/models/entity/work';
import { DateService } from 'src/app/core/services/business/dates/date.service';

export class WorkPlanningStateModel {
  breadcrumbs: Breadcrumb[];
  currentWorkItemId: number;
  parentItems: WorkPlanningParentItem[];
  parentItem: string;
  works: Work[];
  workIds: number[];
  totalWorks: number;
  workItemTileGroups: WorkPlanningWorkItemParent[];
  workItemListGroups: WorkPlanningWorkItemParent[];

  //Assets
  currentAssetName: string;
  totalAssets: number;
  returnedAssets: number;

  //Forms & Findings
  currentWork: Work;
  currentWorkId: number;
  currentWorkParent: string;
  workFormStatus: boolean;
  initiateSignOff: boolean;

  uploadComplete: boolean;
  workFileGroup: WorkFilesGroup[];
  currentWorkFile: MediaItem;
  selectedItems: WorkFile[];
  isMediaSelectionCleared: boolean;
  cycleName: string;
}

@State<WorkPlanningStateModel>({
  name: 'workPlanning',
  defaults: {
    breadcrumbs: [],
    currentWorkItemId: null,
    parentItems: [],
    workItemTileGroups: [],
    workItemListGroups: [],
    works: [],
    workIds: [],
    totalWorks: 0,

    parentItem: '',

    //Assets
    currentAssetName: '',

    //
    currentWork: null,
    currentWorkId: null,
    currentWorkParent: null,
    workFormStatus: false,
    initiateSignOff: false,
    totalAssets: null,
    returnedAssets: null,

    uploadComplete: false,
    workFileGroup: [],
    currentWorkFile: null,
    selectedItems: [],
    isMediaSelectionCleared: false,
    cycleName: '',
  }
})

export class WorkPlanningState {

  //#region S E L E C T O R S

  //#region Breadcrumbs

  @Selector()
  static getBreadcrumbs(state: WorkPlanningStateModel) {
    return state.breadcrumbs;
  }

  //#endregion

  @Selector()
  static getCurrentWorkItemId(state: WorkPlanningStateModel) {
    return state.currentWorkItemId;
  }

  @Selector()
  static getParentItems(state: WorkPlanningStateModel) {
    return state.parentItems;
  }

  @Selector()
  static getWorks(state: WorkPlanningStateModel) {
    return state.works;
  }

  @Selector()
  static getWorkIds(state: WorkPlanningStateModel) {
    return state.workIds;
  }

  @Selector()
  static getTotalWorks(state: WorkPlanningStateModel) {
    return state.totalWorks;
  }

  //#region Work Planning Groups

  @Selector()
  static getWorkItemListGroup(state: WorkPlanningStateModel) {
    return state.workItemListGroups;
  }

  @Selector()
  static getWorkItemTileGroup(state: WorkPlanningStateModel) {
    return state.workItemTileGroups;
  }

  static getParentItem(state: WorkPlanningStateModel) {
    return state.parentItem;
  }

  //#endregion

  //#region Work Planning Assets

  @Selector()
  static getCurrentWorkAssetName(state: WorkPlanningStateModel) {
    return state.currentAssetName;
  }
  @Selector()
  static setCurrentWorkAssetName(state: WorkPlanningStateModel) {
    return state.currentAssetName;
  }

  @Selector()
  static getTotalAssets(state: WorkPlanningStateModel) {
    return state.totalAssets;
  }

  @Selector()
  static getReturnedAssets(state: WorkPlanningStateModel) {
    return state.returnedAssets;
  }

  //#endregion

  //#region Work Forms and Findings

  @Selector()
  static getCurrentWork(state: WorkPlanningStateModel) {
    return state.currentWork;
  }

  @Selector()
  static getCurrentWorkId(state: WorkPlanningStateModel) {
    return state.currentWorkId;
  }

  @Selector()
  static getCurrentWorkParent(state: WorkPlanningStateModel) {
    return state.currentWorkParent;
  }

  @Selector()
  static getWorkFormStatus(state: WorkPlanningStateModel) {
    return state.workFormStatus;
  }

  @Selector()
  static getSignOffStatus(state: WorkPlanningStateModel) {
    return state.initiateSignOff;
  }

  //#endregion

  @Selector()
  static getWorkFile(state: WorkPlanningStateModel) {
    return state.workFileGroup;
  }

  @Selector()
  static getCurrentWorkFile(state: WorkPlanningStateModel) {
    return state.currentWorkFile;
  }

  @Selector()
  static getSelectedItems(state: WorkPlanningStateModel) {
    return state.selectedItems;
  }

  @Selector()
  static isMediaSelectionCleared(state: WorkPlanningStateModel) {
    return state.isMediaSelectionCleared;
  }
  //#endregion

  constructor(private workPlanningService: WorkPlanningService, private metadataListItemsService: MetadataListItemsService, private mediaService: MediaService,
    private dateService: DateService) { }

  //#region A C T I O N S

  //#region Work Planning Breadcrumbs

  @Action(SetBreadcrumbs)
  setBreadcrumbs(ctx: StateContext<WorkPlanningStateModel>, { breadcrumbs }: SetBreadcrumbs) {
    const state = ctx.getState();
    ctx.setState({
      ...state,
      breadcrumbs: breadcrumbs
    });
  }

  @Action(UpdateBreadcrumb)
  updateBreadcrumb(ctx: StateContext<WorkPlanningStateModel>, { breadcrumb }: UpdateBreadcrumb) {
    const state = ctx.getState();
    const breadcrumbs = state.breadcrumbs.filter(x => !x.isFinal);
    breadcrumbs.push(breadcrumb);
    ctx.setState({
      ...state,
      breadcrumbs: breadcrumbs
    });
  }

  @Action(ClearBreadcrumbs)
  clearBreadcrumbs(ctx: StateContext<WorkPlanningStateModel>) {
    const state = ctx.getState();
    ctx.setState({
      ...state,
      breadcrumbs: []
    });
  }

  //#endregion

  //#region Work Planning Groups

  @Action(SetCurrentWorkParentItem)
  setCurrentWorkParentItem(ctx: StateContext<WorkPlanningStateModel>, { name }: SetCurrentWorkParentItem) {
    const state = ctx.getState();
    ctx.setState({
      ...state,
      parentItem: name
    });
  }

  @Action(ClearCurrentWorkParentItem)
  clearCurrentWorkParentItem(ctx: StateContext<WorkPlanningStateModel>) {
    const state = ctx.getState();
    ctx.setState({
      ...state,
      parentItem: null
    });
  }

  //#endregion

  //#region Work Planning Assets

  @Action(GetWorkAsset)
  getWorkAsset(ctx: StateContext<WorkPlanningStateModel>, { assetId }: GetWorkAsset) {
    return this.metadataListItemsService.getMetadataListItem(assetId)
      .pipe(
        tap(item => {
          if (item) ctx.dispatch(new SetCurrentWorkAssetName(item.description));
        }, err => ctx.dispatch(new ShowErrorMessage(err)))
      );
  }

  @Action(SetCurrentWorkAssetName)
  setCurrentWorkAssetName(ctx: StateContext<WorkPlanningStateModel>, { assetName }: SetCurrentWorkAssetName) {
    const state = ctx.getState();
    ctx.setState({
      ...state,
      currentAssetName: assetName
    });
  }

  @Action(ClearCurrentWorkAssetName)
  clearCurrentWorkAssetName(ctx: StateContext<WorkPlanningStateModel>) {
    const state = ctx.getState();
    ctx.setState({
      ...state,
      currentAssetName: null
    });
  }

  //#endregion

  //#region Work Forms & Findings

  @Action(SetCurrentWork)
  setCurrentWork(ctx: StateContext<WorkPlanningStateModel>, { work }: SetCurrentWork) {
    const state = ctx.getState();
    ctx.setState({
      ...state,
      currentWork: work
    });
  }

  @Action(ClearCurrentWork)
  clearCurrentWork(ctx: StateContext<WorkPlanningStateModel>) {
    const state = ctx.getState();
    ctx.setState({
      ...state,
      currentWork: null
    });
  }

  @Action(SetCurrentWorkId)
  setCurrentWorkId(ctx: StateContext<WorkPlanningStateModel>, { workId: itemNumber }: SetCurrentWorkId) {
    const state = ctx.getState();
    ctx.setState({
      ...state,
      currentWorkId: itemNumber
    });
  }

  @Action(ClearCurrentWorkId)
  clearCurrentWorkId(ctx: StateContext<WorkPlanningStateModel>) {
    const state = ctx.getState();
    ctx.setState({
      ...state,
      currentWorkId: null
    });
  }

  @Action(SetCurrentWorkParent)
  setCurrentWorkParent(ctx: StateContext<WorkPlanningStateModel>, { workParent }: SetCurrentWorkParent) {
    const state = ctx.getState();
    ctx.setState({
      ...state,
      currentWorkParent: workParent
    });
  }

  @Action(DownloadFinding)
  downloadFinding(ctx: StateContext<WorkPlanningStateModel>, { findingId, name }: DownloadFinding) {
    ctx.dispatch(new ShowSpinner());
    return this.workPlanningService.downloadFinding(findingId, name)
      .pipe(
        tap(() => {
          ctx.dispatch(new HideSpinner());
        }, (err) => ctx.dispatch(new ShowErrorMessage(err)))
      );
  }

  //#endregion

  //#region WorkItemParent

  @Action(GetWorkPlanningParentItems)
  getParentItems(ctx: StateContext<WorkPlanningStateModel>) {
    return this.workPlanningService.getParentItems().toPromise()
      .then(items => {
        const state = ctx.getState();
        ctx.setState({
          ...state,
          parentItems: items
        });
      });
  }

  @Action(SetCurrentWorkItemId)
  getWorkItemId(ctx: StateContext<WorkPlanningStateModel>, { id }: SetCurrentWorkItemId) {
    const state = ctx.getState();
    return ctx.setState({
      ...state,
      currentWorkItemId: id,
    });
  }

  @Action(GetWorkItemTileGroup)
  getWorkItemGroup(ctx: StateContext<WorkPlanningStateModel>, { name }: GetWorkItemTileGroup) {
    ctx.dispatch(new ShowSpinner());
    return this.workPlanningService.getWorkItemGroup(name)
      .pipe(
        tap(response => {
          console.log('response', response);
          const state = ctx.getState();
          ctx.setState({
            ...state,
            workItemTileGroups: response
          });
          ctx.dispatch(new HideSpinner());
        })
      );
  }

  //#endregion

  //#region Works

  @Action(GetWorks)
  getWorks(ctx: StateContext<WorkPlanningStateModel>, { assetId, dateFilter, actionList, cycle }: GetWorks) {
    ctx.dispatch(new ShowSpinner());
    return this.workPlanningService.getWorks(assetId, dateFilter, actionList, cycle)
      .pipe(
        tap(works => {
          const state = ctx.getState();
          const ids = works.map(x => x.id);
          ctx.setState({
            ...state,
            works: works,
            workIds: ids
          });
          ctx.dispatch(new HideSpinner());
        }, (err) => ctx.dispatch(new ShowErrorMessage(err)))
      );
  }

  @Action(GetWorkItemListGroup)
  getWorkItemListGroup(ctx: StateContext<WorkPlanningStateModel>, { name }: GetWorkItemListGroup) {
    ctx.dispatch(new ShowSpinner());
    return this.workPlanningService.getWorkItemGroup(name)
      .pipe(
        tap(response => {
          const state = ctx.getState();
          ctx.setState({
            ...state,
            workItemListGroups: response
          });
          ctx.dispatch(new HideSpinner());
        }, (err) => ctx.dispatch(new ShowErrorMessage(err)))
      );
  }

  @Action(ClearWorks)
  clearCurrentInspections(ctx: StateContext<WorkPlanningStateModel>) {
    const state = ctx.getState();
    ctx.setState({
      ...state,
      works: [],
      totalAssets: 0
    });
  }

  @Action(DownloadWorkReport)
  downloadWorkReport(ctx: StateContext<WorkPlanningStateModel>, { workParent, assetId, assetName, queryType, dateFilter, cycle }: DownloadWorkReport) {
    ctx.dispatch(new ShowSpinner());
    const reportName = cycle ?
      `${workParent}-${assetName}-${cycle}-${this.dateService.formatToDateOnly(dateFilter)}-${queryType}.csv` :
      `${workParent}-${assetName}-${this.dateService.formatToDateOnly(dateFilter)}-${queryType}.csv`;
    return this.workPlanningService.downloadReport(assetId, queryType, reportName, dateFilter, cycle)
      .pipe(
        tap(() => {
          ctx.dispatch(new HideSpinner());
        }, (err) => ctx.dispatch(new ShowErrorMessage(err)))
      );
  }

  //#endregion

  @Action(CreateWorkFile)
  createWorkFile(ctx: StateContext<WorkPlanningStateModel>, { payload }: CreateWorkFile) {
    return this.workPlanningService.createWorkFile(payload)
      .pipe(
        map(() => {
          const state = ctx.getState();
          ctx.dispatch(new GetWorkFiles(payload.workId));
          ctx.dispatch(new ShowSuccessMessage(`Media Item successfully uploaded!`));
          ctx.setState({
            ...state,
            uploadComplete: true
          });
          ctx.dispatch(new HideSpinner());
        }, error => ctx.dispatch(new ShowErrorMessage(error)))
      );
  }

  @Action(GetWorkFiles)
  getWorkFile(ctx: StateContext<WorkPlanningStateModel>, { workId }: GetWorkFiles) {
    ctx.dispatch(new ShowSpinner());
    return this.workPlanningService.getWorkFile(workId)
      .pipe(
        tap(response => {
          const state = ctx.getState();
          const selectedItemIds = state.selectedItems.map(x => x.id);
          response.filter(function (value) {
            let workFiles: WorkFile[] = value.workFile;
            workFiles.forEach(workFile => {
              if (selectedItemIds.includes(workFile.id)) workFile.isChecked = true;
            });
          });
          ctx.setState({
            ...state,
            workFileGroup: response,
          });
          ctx.dispatch(new HideSpinner());
        }, (err) => ctx.dispatch(new ShowErrorMessage(err)))
      );
  }

  @Action(ClearWorkFile)
  clearWorkFile(ctx: StateContext<WorkPlanningStateModel>) {
    const state = ctx.getState();
    ctx.setState({
      ...state,
      workFileGroup: []
    });
  }

  @Action(GetCurrentWorkFile)
  getCurrentWorkFile(ctx: StateContext<WorkPlanningStateModel>, { id }: GetCurrentWorkFile) {
    return this.mediaService.getMediaItem(id).pipe(
      tap(item => {
        const state = ctx.getState();
        ctx.setState({
          ...state,
          currentWorkFile: item
        });
      }, error => ctx.dispatch(new ShowErrorMessage(error)))
    );
  }

  @Action(ClearCurrentWorkFile)
  clearCurrentWorkFile(ctx: StateContext<WorkPlanningStateModel>) {
    const state = ctx.getState();
    ctx.setState({
      ...state,
      currentWorkFile: null
    });
  }

  @Action(SetWorkFormStatus)
  setWorkFormStatus(ctx: StateContext<WorkPlanningStateModel>, { isValid }: SetWorkFormStatus) {
    const state = ctx.getState();
    ctx.setState({
      ...state,
      workFormStatus: isValid
    });
  }

  @Action(PerformSignOff)
  performSignOff(ctx: StateContext<WorkPlanningStateModel>) {
    const state = ctx.getState();
    ctx.setState({
      ...state,
      initiateSignOff: true
    });
    ctx.setState({ ...state, initiateSignOff: false });
  }


  //#region selection

  @Action(AddSelectedWorkFile)
  addSelectedItem({ getState, setState }: StateContext<WorkPlanningStateModel>, { item }: AddSelectedWorkFile) {
    const state = getState();
    let items = [...state.selectedItems];
    console.log('MediaState items before: ', items);
    if (items.includes(item)) return;
    items.push(item);
    console.log('MediaState items after: ', items);
    setState({
      ...state,
      selectedItems: items,
      isMediaSelectionCleared: false
    });
  }

  @Action(AddSelectedWorkFiles)
  addSelectedItems({ getState, setState }: StateContext<WorkPlanningStateModel>, { items }: AddSelectedWorkFiles) {
    const state = getState();
    let stateItems = [...state.selectedItems];
    console.log('AddSelectedWorkFiles items before: ', stateItems);
    items.forEach(item => {
      const stateItemsIds = stateItems.map(x => x.id);
      stateItems = !stateItemsIds.includes(item.id) ? [...stateItems, item] : stateItems;
    });
    console.log('AddSelectedWorkFiles items after: ', stateItems);
    setState({
      ...state,
      selectedItems: stateItems,
      isMediaSelectionCleared: false,
    });
  }

  @Action(RemoveSelectedWorkFile)
  removeSelectedWorkFile({ getState, setState }: StateContext<WorkPlanningStateModel>, { item }: RemoveSelectedWorkFile) {
    const state = getState();
    let items = [...state.selectedItems];
    console.log('MediaState items before: ', items);
    items = items.filter(x => x.id !== item.id);
    console.log('MediaState items removed: ', items);
    setState({
      ...state,
      selectedItems: items
    });
  }

  // @Action(RemoveSelectedWorkFiles)
  // removeSelectedWorkFiles({ getState, setState }: StateContext<WorkPlanningModel>, { items }: RemoveSelectedWorkFiles) {
  //   const state = getState();
  //   let stateItems = [...state.selectedItems];
  //   console.log('RemoveSelectedWorkFiles items before: ', stateItems);
  //   items.forEach(item => {
  //     const stateItemsIds = stateItems.map(x => x.id);
  //     stateItems = stateItemsIds.includes(item.id) ? stateItems.filter(x => x.id !== item.id) : stateItems;
  //   });
  //   console.log('RemoveSelectedWorkFiles items removed: ', stateItems);
  //   setState({
  //     ...state,
  //     selectedItems: stateItems
  //   });
  // }

  @Action(SetSelectedWorkFile)
  setSelectedWorkFile({ getState, setState }: StateContext<WorkPlanningStateModel>, { items }: SetSelectedWorkFile) {
    const state = getState();
    setState({
      ...state,
      selectedItems: items,
      isMediaSelectionCleared: false
    });
  }

  @Action(ClearSelectedMediaItems)
  clearSelectedMediaItems({ getState, setState }: StateContext<WorkPlanningStateModel>) {
    const state = getState();
    setState({
      ...state,
      selectedItems: [],
      isMediaSelectionCleared: true
    });
  }

  //#endregion

  @Action(DownloadWorkFiles)
  downloadItems(ctx: StateContext<WorkPlanningStateModel>, { items }: DownloadWorkFiles) {
    ctx.dispatch(new ShowSpinner());

    ctx.dispatch(new ShowDownloadsMessage(items.length));
    let complete = 0;
    let incomplete = items.length;
    items.forEach((item, index) => {
      this.mediaService.download(item.name, item.url)
        .then(() => {
          complete++; incomplete--;
          ctx.dispatch(new SetDownloadStatus(incomplete, complete));
          if (index === (items.length - 1)) ctx.dispatch(new ClearSelectedMediaItems());
        }, error => ctx.dispatch(new ShowErrorMessage(error)));
    });
  }

  //#endregion A C T I O N S

}