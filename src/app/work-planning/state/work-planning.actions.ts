import { Breadcrumb } from 'src/app/core/models/breadcrumb';
import { WorkFile } from 'src/app/core/models/entity/work-item';
import { MediaItem } from 'src/app/core/models/entity/media';
import { Work } from 'src/app/core/models/entity/work';
import { WorkReportType } from 'src/app/core/enum/work-report-type';

export class GetWorkPlanningParentItems {
  static readonly type = '[Media] GetWorkPlanningParentItems';
}

//#region Breadcrumb

export class SetBreadcrumbs {
  static readonly type = '[Work Planning] SetBreadcrumbs';

  constructor(public breadcrumbs: Breadcrumb[]) { }
}

export class UpdateBreadcrumb {
  static readonly type = '[Work Planning] UpdateBreadcrumb';

  constructor(public breadcrumb: Breadcrumb) { }
}

export class ClearBreadcrumbs {
  static readonly type = '[Work Planning] ClearBreadcrumbs';
}

//#endregion

//#region Work Planning Groups

export class SetCurrentWorkParentItem {
  static readonly type = '[Work Planning] SetCurrentWorkParentItem';

  constructor(public name: string) { }
}

export class ClearCurrentWorkParentItem {
  static readonly type = '[Work Planning] ClearCurrentWorkParentItem';
}

//#endregion

//#region Work Planning Item

export class SetCurrentWorkItemId {
  static readonly type = '[Work Planning] SetCurrentWorkItemId';

  constructor(public id: number) { }
}

export class GetWorks {
  static readonly type = '[Work Planning] GetWorks';

  constructor(public assetId: number, public dateFilter: Date, public actionList?: boolean, public cycle?: string) { }
}

export class SetWorkIds {
  static readonly type = '[Work Planning] SetWorkIds';

  constructor(public workIds: number[]) { }
}

export class ClearWorks {
  static readonly type = '[Work Planning] ClearWorks';
}

export class GetWorkItemTileGroup {
  static readonly type = '[Work Planning] GetWorkItemTileGroup';

  constructor(public name: string) { }
}

export class GetWorkItemListGroup {
  static readonly type = '[Work Planning] GetWorkItemListGroup';

  constructor(public name: string) { }
}

export class DownloadWorkReport {
  static readonly type = '[Work Planning] DownloadWorkReport';

  constructor(public workParent: string, public assetId: number, public assetName: string, public queryType: WorkReportType, public dateFilter?: Date, public cycle?: string) { }
}

//#endregion

//#region Work Planning Assets

export class GetWorkAsset {
  static readonly type = '[Work Planning] GetWorkAsset';

  constructor(public assetId: number) { }
}

export class SetCurrentWorkAssetName {
  static readonly type = '[Work Planning] SetCurrentWorkAssetName';

  constructor(public assetName: string) { }
}

export class ClearCurrentWorkAssetName {
  static readonly type = '[Work Planning] ClearCurrentWorkAssetName';
}

//#endregion

//#region Work Forms & Findings

export class SetCurrentWork {
  static readonly type = '[Work Planning] SetCurrentWork';

  constructor(public work: Work) { }
}

export class ClearCurrentWork {
  static readonly type = '[Work Planning] ClearCurrentWork';
}

export class SetCurrentWorkId {
  static readonly type = '[Work Planning] SetCurrentWorkId';

  constructor(public workId: any) { }
}

export class ClearCurrentWorkId {
  static readonly type = '[Work Planning] ClearCurrentWorkId';
}

export class DownloadFinding {
  static readonly type = '[Work Planning] DownloadFinding';

  constructor(public findingId: number, public name: string) { }
}

export class SetCurrentWorkParent {
  static readonly type = '[Work Planning] SetCurrentWorkParent';

  constructor(public workParent: string) { }
}

export class SetWorkFormStatus {
  static readonly type = '[Work Planning] SetWorkFormStatus';

  constructor(public isValid: boolean) { }
}

export class PerformSignOff {
  static readonly type = '[Work Planning] PerformSignOff';
}

//#endregion

//#region Work Files

export class CreateWorkFile {
  static readonly type = '[Work Planning] CreateWorkFile';

  constructor(public payload: WorkFile) { }
}

export class GetWorkFiles {
  static readonly type = '[Work Planning] GetWorkFile';

  constructor(public workId: number) { }
}

export class ClearWorkFile {
  static readonly type = '[Work Planning] ClearWorkFile';

}
export class GetCurrentWorkFile {
  static readonly type = '[Work Planning] GetCurrentWorkFile';

  constructor(public id: any) { }
}

export class ClearCurrentWorkFile {
  static readonly type = '[Work Planning] ClearCurrentWorkFile';

}

//#endregion

export class AddSelectedWorkFile {
  static readonly type = '[Work Planning] AddSelectedWorkFile';

  constructor(public item: WorkFile) { }
}

export class AddSelectedWorkFiles {
  static readonly type = '[MedWork Planningia] AddSelectedWorkFiles';

  constructor(public items: WorkFile[]) { }
}

export class RemoveSelectedWorkFile {
  static readonly type = '[Work Planning] RemoveSelectedWorkFiles';

  constructor(public item: WorkFile) { }
}

export class RemoveSelectedWorkFiles {
  static readonly type = '[Work Planning] RemoveSelectedWorkFiles';

  constructor(public items: WorkFile[]) { }
}

export class SetSelectedWorkFile {
  static readonly type = '[Work Planning] SetSelectedWorkFiles';

  constructor(public items: WorkFile[]) { }
}

export class ClearSelectedMediaItems {
  static readonly type = '[Work Planning] ClearSelectedWorkFiles';
}

export class DownloadWorkFiles {
  static readonly type = '[Work Planning] DownloadWorkFiles';

  constructor(public items: MediaItem[]) { }
}