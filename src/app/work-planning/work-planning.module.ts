import { NgModule, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WorkItemGroupsListViewComponent } from './work-item-groups/work-item-groups-list-view/work-item-groups-list-view.component';
import { WorkItemGroupsTileViewComponent } from './work-item-groups/work-item-groups-tile-view/work-item-groups-tile-view.component';
import { WorkPlanningRoutingModule } from './work-planning.routing.module';
import { NgxsModule } from '@ngxs/store';
import { WorkPlanningState } from './state/work-planning.state';
import { WorkPlanningService } from '../core/services/business/work-planning/work-planning.service';
import { WorkPlanningDataService } from '../core/services/data/work-planning/work-planning.data.service';
import { environment } from 'src/environments/environment';
import { WorkPlanningWebDataService } from '../core/services/data/work-planning/work-planning.web.data.service';
import { WorkPlanningMockDataService } from '../core/services/data/work-planning/work-planning.mock.data.service';
import { SharedModule } from '../shared/shared.module';
import { WorkItemGroupsComponent } from './work-item-groups/work-item-groups.component';
import { MetadataListItemsService } from '../core/services/business/metadata-list-items/metadata-list-items.service';
import { MetadataListItemsDataService } from '../core/services/data/metadata-list-items/metadata-list-items.data.service';
import { MetadataListItemsMockDataService } from '../core/services/data/metadata-list-items/metadata-list-items.mock.data.service';
import { MetadataListItemsWebDataService } from '../core/services/data/metadata-list-items/metadata-list-items.web.data.service';
import { WorksListComponent } from './works-list/works-list.component';
import { DatePickerModule } from '@syncfusion/ej2-angular-calendars';
import { WorkEditComponent } from './work-edit/work-edit.component';
import { WorkEditFormsComponent } from './work-edit/work-edit-forms/work-edit-forms.component';
import { WorkEditFindingsComponent } from './work-edit/work-edit-findings/work-edit-findings.component';
import { WorkEditViewerComponent } from './work-edit/work-edit-viewer/work-edit-viewer.component';
import { MediaService } from '../core/services/business/media/media.service';
import { MediaDataService } from '../core/services/data/media/media.data.service';
import { MediaWebDataService } from '../core/services/data/media/media.web.data.service';
import { MediaMockDataService } from '../core/services/data/media/media.mock.data.service';
import { WorkItemGroupsBarchartViewComponent } from './work-item-groups/work-item-groups-barchart-view/work-item-groups-barchart-view.component';
import { ChartAllModule, CategoryService, LegendService, TooltipService, DataLabelService, LineSeriesService, BarSeriesService, StackingBarSeriesService } from '@syncfusion/ej2-angular-charts';

@NgModule({
  declarations: [
    WorkItemGroupsListViewComponent,
    WorkItemGroupsTileViewComponent,
    WorkItemGroupsComponent,
    WorksListComponent,
    WorkEditComponent,
    WorkEditFormsComponent,
    WorkEditFindingsComponent,
    WorkEditViewerComponent,
    WorkItemGroupsBarchartViewComponent
  ],
  imports: [
    SharedModule,
    ChartAllModule,
    CommonModule,
    SharedModule,
    WorkPlanningRoutingModule,
    DatePickerModule,
    NgxsModule.forFeature([WorkPlanningState])
  ],
  providers: [
    WorkPlanningService,
    CategoryService, LegendService, TooltipService, DataLabelService, LineSeriesService, BarSeriesService, StackingBarSeriesService,
    { provide: WorkPlanningDataService, useClass: environment.useMocks ? WorkPlanningMockDataService : WorkPlanningWebDataService },
    MetadataListItemsService,
    { provide: MetadataListItemsDataService, useClass: environment.useMocks ? MetadataListItemsMockDataService : MetadataListItemsWebDataService },
    MediaService,
    {provide: MediaDataService, useClass: environment.useMocks ? MediaMockDataService : MediaWebDataService }
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA]
})
export class WorkPlanningModule { }
