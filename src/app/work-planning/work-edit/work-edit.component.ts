import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Tab } from 'src/app/core/models/tab';
import { Router, ActivatedRoute } from '@angular/router';
import { SubSink } from 'subsink/dist/subsink';
import { ToolBar } from 'src/app/core/models/toolbar-action';
import { Select, Store } from '@ngxs/store';
import { WorkPlanningState } from '../state/work-planning.state';
import { Observable } from 'rxjs';
import { BaseComponent } from 'src/app/shared/base/base.component';
import { SetCurrentWorkId, SetBreadcrumbs, ClearBreadcrumbs, GetWorkAsset, GetWorkFiles, ClearWorkFile, AddSelectedWorkFile, RemoveSelectedWorkFile, ClearSelectedMediaItems, DownloadWorkFiles, SetCurrentWorkParent, PerformSignOff } from '../state/work-planning.actions';
import { Breadcrumb } from 'src/app/core/models/breadcrumb';
import { WorkFile } from 'src/app/core/models/entity/work-item';
import { SetPreviousRoute, ShowErrorMessage, GetAzureSASToken } from 'src/app/state/app.actions';
import { ModalComponent } from 'src/app/shared/modal/modal.component';
import { AppState } from 'src/app/state/app.state';
import { Work } from 'src/app/core/models/entity/work';
import { Status } from 'src/app/core/enum/status.enum';

@Component({
  selector: 'app-work-edit',
  templateUrl: './work-edit.component.html',
  styleUrls: ['./work-edit.component.css']
})
export class WorkEditComponent extends BaseComponent implements OnInit, OnDestroy {

  @Select(WorkPlanningState.getBreadcrumbs) breadcrumbs$: Observable<Breadcrumb[]>;
  @Select(WorkPlanningState.getCurrentWorkAssetName) currentAssetName$: Observable<string>;
  @Select(WorkPlanningState.getWorkFile) workFile$: Observable<WorkFile[]>;
  @Select(WorkPlanningState.getSelectedItems) selectedItems$: Observable<WorkFile[]>;
  @Select(WorkPlanningState.isMediaSelectionCleared) isMediaSelectionCleared$: Observable<boolean>;
  @Select(AppState.getMediaReadSASToken) sasToken$: Observable<string>;
  @Select(WorkPlanningState.getCurrentWork) currentWork$: Observable<Work>;
  @Select(WorkPlanningState.getWorkFormStatus) currentFormStatus$: Observable<string>;
  @Select(WorkPlanningState.getWorkIds) workIds$: Observable<number[]>;

  @ViewChild('confirmModal') confirmModal: ModalComponent;

  private subs = new SubSink();
  isForms = true;
  currentPage = 1;
  pageSize = 8;
  pageCount: number;
  workItemName: string;

  toolbarItems: ToolBar[] = [
    { text: 'Download', disabled: true, cssClass: 'download-all-qa' },
    { text: 'Delete', disabled: true }
  ];
  tabs: Tab[] = [
    { link: 0, name: 'Information', isActive: true },
    { link: 1, name: 'Findings' }
  ];
  currentStep: string;
  parentName: string;
  assetId: number;
  workType: string;
  workId: number;
  tiles: any[];
  isAllowPagination: boolean;
  workFileLength: number;
  selectedItems: any;
  deleteMessage: string;
  selectedItemsId: any;
  isFormValid: boolean;
  navigationMode: boolean;
  currentIndex = 1;
  workIds: number[];
  totalWorks = 1;

  constructor(protected store: Store, private router: Router, private activatedRoute: ActivatedRoute) {
    super(store);
    this.hideLeftNav();
  }

  ngOnInit() {
    this.store.dispatch(new GetAzureSASToken());
    this.subs.add(
      this.activatedRoute.paramMap
        .subscribe(params => {
          if (!this.navigationMode) {
            this.parentName = params.get('parentName');
            this.assetId = Number(params.get('assetId'));
            this.workType = params.get('workType');
            this.workId = Number(params.get('workId'));
            this.store.dispatch(new GetWorkFiles(Number(this.workId)));
            this.store.dispatch(new GetWorkAsset(this.assetId));
            this.setTabs(this.router.url);
            this.store.dispatch(new SetCurrentWorkParent(this.parentName));
            if (this.workId) this.store.dispatch(new SetCurrentWorkId(this.workId));
          } else {
            this.navigationMode = false;
          }
        }),
      this.currentAssetName$
        .subscribe(assetName => {
          const crumbs: Breadcrumb[] = [
            { link: this.router.url, name: 'Work Planning' },
            { link: `work-planning/${this.parentName}`, name: this.parentName },
            { link: `work-planning/${this.parentName}/${this.assetId}/works`, name: assetName || 'Unknown' },
            { name: "Details" }
          ];
          this.store.dispatch(new SetBreadcrumbs(crumbs));
        }),
      this.workFile$
        .subscribe(workFile => {
          this.workFileLength = workFile.length;
          if (workFile.length > 0) {
            this.isAllowPagination = true;
          } else {
            this.isAllowPagination = false;
          }
        }),
      this.isMediaSelectionCleared$
        .subscribe(isCleared => {
          if (isCleared) this.store.dispatch(new GetWorkFiles(Number(this.workId)));
        }),
      this.selectedItems$
        .subscribe(items => {
          this.selectedItems = items;
          this.setToolbarProperties();
        }),
      this.workIds$
        .subscribe(workIds => {
          if (workIds) {
            this.workIds = workIds;
            this.totalWorks = workIds.length;
            this.currentIndex = workIds.indexOf(Number(this.workId));
          }
        })
    );
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
    this.store.dispatch(new ClearBreadcrumbs());
    this.store.dispatch(new ClearWorkFile());
    this.store.dispatch(new ClearSelectedMediaItems());
  }

  switchBreadCrumbs(breadcrumbLink: string) {
    this.router.navigate([breadcrumbLink]);
  }

  switchTabs(link: any) {
    const currentUrl = this.router.url;
    this.setTabs(currentUrl, link);
  }

  setTabs(currentUrl: string, type?: number) {
    const lastSegment = currentUrl.substring(currentUrl.lastIndexOf("/") + 1, currentUrl.length);
    if (!type) {
      if (lastSegment === 'findings') {
        this.tabs[0].isActive = false;
        this.tabs[1].isActive = true;
      } else {
        this.tabs[0].isActive = true;
        this.tabs[1].isActive = false;
      }
    }
    if (type === 0) {
      this.router.navigate([`work-planning/${this.parentName}/${this.assetId}/${this.workType}/${this.workId}/form`]);
      this.tabs[0].isActive = true;
      this.tabs[1].isActive = false;
    }
    else if (type === 1) {
      this.router.navigate([`work-planning/${this.parentName}/${this.assetId}/${this.workType}/${this.workId}/findings`]);
      this.tabs[0].isActive = false;
      this.tabs[1].isActive = true;
    }
  }

  getCurrentPageType() {
    const currentUrl = this.router.url;
    return currentUrl.substring(currentUrl.lastIndexOf("/") + 1, currentUrl.length);
  }

  signOff() {
    this.store.dispatch(new PerformSignOff());
  }

  previous() {
    if (this.currentIndex > 0) {
      this.workId = this.workIds[this.currentIndex - 1];
      const pageType = this.getCurrentPageType();
      this.navigationMode = true;
      this.router.navigate([`work-planning/${this.parentName}/${this.assetId}/${this.workType}/${this.workId}/${pageType}`]);
      this.currentIndex = this.workIds.indexOf(Number(this.workId));
      this.store.dispatch(new GetWorkFiles(Number(this.workId)));
      if (this.workId) this.store.dispatch(new SetCurrentWorkId(this.workId));
    }
  }

  next() {
    if (this.currentIndex < (this.totalWorks - 1)) {
      this.workId = this.workIds[this.currentIndex + 1];
      const pageType = this.getCurrentPageType();
      this.navigationMode = true;
      this.router.navigate([`work-planning/${this.parentName}/${this.assetId}/${this.workType}/${this.workId}/${pageType}`]);
      this.currentIndex = this.workIds.indexOf(Number(this.workId));
      this.store.dispatch(new GetWorkFiles(Number(this.workId)));
      if (this.workId) this.store.dispatch(new SetCurrentWorkId(this.workId));
    }
  }

  openViewer(data: WorkFile) {
    this.store.dispatch(new SetPreviousRoute(this.router.url));
    if (this.workId) {
      this.router.navigate([`work-planning/${this.parentName}/${this.assetId}/${this.workType}/${this.workId}/${data.id}/viewer`]);
    } else {
      this.store.dispatch(new ShowErrorMessage("Unable to get the current media item."))
    }
  }

  toggleCheckBoxEvent(data: any) {
    const selectedIds = this.selectedItems.map(x => x.id);
    if (!selectedIds.includes(data.id)) {
      this.store.dispatch(new AddSelectedWorkFile(data));
    } else {
      this.store.dispatch(new RemoveSelectedWorkFile(data));
    }
  }

  toolbarClickEvent(index: number) {
    switch (index) {
      case 0:
        this.downloadSelectedItems();
        break;
      case 1:
        this.showConfirmDialog();
        break;
      default:
        break;
    }

  }

  showConfirmDialog() {
    this.deleteMessage = `Are you sure you want to delete?`;
    this.confirmModal.show()
  }

  private setToolbarProperties() {
    this.toolbarItems.map((item) => {
      item.disabled = this.selectedItems.length === 0;
    });
  }

  downloadSelectedItems() {
    const filesToDownload = [...this.selectedItems];
    this.store.dispatch(new DownloadWorkFiles(filesToDownload));
  }

  deleteAttachments() {
    const filesToDownload = [...this.selectedItems];
    this.store.dispatch(new ClearSelectedMediaItems());
    console.log('initiate delete', filesToDownload);
  }
}
