import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkEditViewerComponent } from './work-edit-viewer.component';

describe('WorkEditViewerComponent', () => {
  let component: WorkEditViewerComponent;
  let fixture: ComponentFixture<WorkEditViewerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WorkEditViewerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkEditViewerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
