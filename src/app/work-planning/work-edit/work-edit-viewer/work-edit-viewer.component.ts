import { Component, OnInit, OnDestroy } from '@angular/core';
import { WorkPlanningState } from '../../state/work-planning.state';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { WorkFile } from 'src/app/core/models/entity/work-item';
import { SubSink } from 'subsink/dist/subsink';
import { MediaType } from 'src/app/core/enum/media-type';
import { Router, ActivatedRoute } from '@angular/router';
import { GetCurrentWorkFile, ClearCurrentWorkFile } from '../../state/work-planning.actions';
import { MediaItem } from 'src/app/core/models/entity/media';
import { AppState } from 'src/app/state/app.state';
import { Breadcrumb } from 'src/app/core/models/breadcrumb';
import { DownloadMediaItem } from 'src/app/media/state/media/media.action';

@Component({
  selector: 'app-work-edit-viewer',
  templateUrl: './work-edit-viewer.component.html',
  styleUrls: ['./work-edit-viewer.component.css']
})
export class WorkEditViewerComponent implements OnInit, OnDestroy {

  @Select(WorkPlanningState.getCurrentWorkFile) currentWorkFile$: Observable<MediaItem>;
  @Select(WorkPlanningState.getWorks) works$: Observable<MediaItem>;
  @Select(WorkPlanningState.getCurrentWorkId) currentWorkId$: Observable<number>;
  @Select(AppState.getPreviousRoute) previousRoute$: Observable<Breadcrumb[]>;
  @Select(AppState.getMediaReadSASToken) sasToken$: Observable<string>;

  private subs = new SubSink();
  currentWorkFiles: WorkFile;
  url: string;
  name: string;
  type: string;
  works: any;
  currentWork: any;
  currentProcess: string;
  currentStep: string;
  currentFieldName: string;
  isWorkPlanning: boolean = true;
  sasToken: string;

  constructor(private router: Router, private route: ActivatedRoute, private store: Store) { }

  ngOnInit() {
    const currentWorkId = this.route.snapshot.paramMap.get('documentId');
    this.store.dispatch(new GetCurrentWorkFile(currentWorkId));

    this.subs.add(
      this.currentWorkFile$
        .subscribe(workFile => {
          if (workFile) {
            this.getMediaType(workFile.type.toLowerCase());
            this.url = workFile.url;
            this.name = workFile.name;
            this.currentFieldName = workFile.fieldName;
          }
        }),
      this.sasToken$
        .subscribe(sasToken => this.sasToken = sasToken),
      this.works$
        .subscribe(work => this.works = work),
      this.currentWorkId$
        .subscribe(workId => {
          this.currentWork = this.works.find(work => work.id === Number(workId));
          if (this.currentWork) {
            this.currentProcess = this.currentWork.processCode;
            this.currentStep = this.currentWork.currentStep;
          }
        }),
    )
  }

  getMediaType(type: string) {
    const docFormats = ['doc', 'docx', 'xls', 'xlsx', 'ppt', 'csv', 'log'];
    const imageFormats = ["jpeg", "jpg", "bmp", "gif", "svg", "png"];
    const videoFormats = ['mp4', 'mov', 'avi', 'mpg', 'mkv'];
    if (docFormats.includes(type)) {
      this.type = MediaType.Document
    } else if (imageFormats.includes(type)) {
      this.type = MediaType.Image;
    } else if (videoFormats.includes(type)) {
      this.type = MediaType.Video;
    } else if (type === 'pdf') {
      this.type = MediaType.Pdf;
    }
  }

  ngOnDestroy() {
    this.store.dispatch(new ClearCurrentWorkFile());
    this.subs.unsubscribe();
  }

  onBack(route: string) {
    this.router.navigate([route]);
  }

  download(item: any) {
    if (item) {
      this.url = item;
      this.name = this.name;
    }
    this.store.dispatch(new DownloadMediaItem(this.name, this.url));
  }
}
