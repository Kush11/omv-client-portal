import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkEditFindingsComponent } from './work-edit-findings.component';

describe('WorkEditFindingsComponent', () => {
  let component: WorkEditFindingsComponent;
  let fixture: ComponentFixture<WorkEditFindingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WorkEditFindingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkEditFindingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
