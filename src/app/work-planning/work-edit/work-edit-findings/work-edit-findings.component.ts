import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Store, Select } from '@ngxs/store';
import { SubSink } from 'subsink/dist/subsink';
import { Work } from 'src/app/core/models/entity/work';
import { FieldConfiguration } from 'src/app/shared/dynamic-components/field-setting';
import { MenuItem } from 'src/app/core/models/entity/menu-item';
import { WorkEditFindingsService } from './work-edit-findings.service';
import { WorkPlanningState } from '../../state/work-planning.state';
import { DynamicFormComponent } from 'src/app/shared/dynamic-components/components/dynamic-form.component';
import { ModalComponent } from 'src/app/shared/modal/modal.component';
import { Observable } from 'rxjs/internal/Observable';
import { BaseComponent } from 'src/app/shared/base/base.component';
import { WorkFinding } from 'src/app/core/models/entity/work-finding';
import { WorkPlanningService } from 'src/app/core/services/business/work-planning/work-planning.service';
import { ShowErrorMessage, ShowSuccessMessage } from 'src/app/state/app.actions';
import { SideListMenuComponent } from 'src/app/shared/side-list-menu/side-list-menu.component';
import { FormBuilder } from '@angular/forms';
import { DownloadFinding, ClearCurrentWork, SetCurrentWork } from '../../state/work-planning.actions';
import { WorkItemUploadService } from '../work-item-upload.service';
import { AppState } from 'src/app/state/app.state';
import { WorkFileType } from 'src/app/core/models/entity/work-item';

@Component({
  selector: 'app-work-edit-findings',
  templateUrl: './work-edit-findings.component.html',
  styleUrls: ['./work-edit-findings.component.css']
})
export class WorkEditFindingsComponent extends BaseComponent implements OnInit, OnDestroy {

  @Select(WorkPlanningState.getCurrentWorkId) currentWorkId$: Observable<number>;
  @Select(AppState.getAzureContainer) container$: Observable<string>;
  @Select(AppState.getAzureStorageAccount) storageAccount$: Observable<string>;
  @Select(AppState.getWorkPlanningWriteSASToken) SASToken$: Observable<string>;

  @ViewChild("dynamicForm") dynamicForm: DynamicFormComponent;
  @ViewChild("uploadModal") uploadModal: ModalComponent;
  @ViewChild('findingModal') findingModal: ModalComponent;
  @ViewChild("sideMenu") sideMenu: SideListMenuComponent;

  private subs = new SubSink();
  workTypeTitle: string;
  processCodes: MenuItem[] = [];
  fields: FieldConfiguration[] = [];
  workName: string;
  workId: number;
  canAdvance: boolean;
  advanceButtonText: string;
  currentStepName: string;
  currentField: FieldConfiguration;
  uploadModalTitle: string;
  works: Work[];
  currentProcess: Work;
  processes: MenuItem[] = [];
  workTitle: string;
  currentFinding: WorkFinding;
  findingControls: FieldConfiguration[] = [];
  presetUpdatedFinding: boolean;
  presetCreatedFinding: boolean;
  presetSelectedFinding: boolean;
  storageAccount: any;
  sasToken: any;
  containerName: any;

  constructor(protected store: Store, private fb: FormBuilder, private service: WorkEditFindingsService, private workPlanningService: WorkPlanningService,
    private uploadService: WorkItemUploadService) {
    super(store);
    this.setPageTitle("Work Findings");
  }

  ngOnInit() {
    this.workTitle = "Findings";
    this.subs.add(
      this.currentWorkId$
        .subscribe(workId => {
          this.workId = workId;
          this.getFindings();
        }),
      this.container$
        .subscribe(container => this.containerName = container),
      this.storageAccount$
        .subscribe(storageAccount => this.storageAccount = storageAccount),
      this.SASToken$
        .subscribe(SASToken => this.sasToken = SASToken),
    );
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
    this.store.dispatch(new ClearCurrentWork());
  }

  createFinding() {
    this.showSpinner();
    this.findingControls = [];
    this.service.getFindingFields(this.currentProcess.id)
      .then(fields => {
        this.findingControls = fields;
        this.hideSpinner();
        this.findingModal.reset();
        this.findingModal.show();
      }, err => this.store.dispatch(new ShowErrorMessage(err)));
  }

  saveFinding(dynamicForm: DynamicFormComponent) {
    if (dynamicForm.valid && dynamicForm.dirty) {
      this.showSpinner();
      this.currentProcess.findingData = JSON.stringify(dynamicForm.value);
      this.workPlanningService.createFinding(this.currentProcess.id, this.currentProcess)
        .then(() => {
          this.hideSpinner();
          this.findingModal.reset();
          this.fields = [];
          this.presetCreatedFinding = true;
          this.getFindings();
          this.store.dispatch(new ShowSuccessMessage("Finding was saved successfully."));
        }, err => this.store.dispatch(new ShowErrorMessage(err)));
    }
  }

  updateFinding() {
    const metadata = this.dynamicForm ? JSON.stringify(this.dynamicForm.value) : '{}';
    const { id } = this.currentFinding;
    this.showSpinner();
    this.workPlanningService.updateFinding(id, metadata)
      .then(() => {
        this.hideSpinner();
        this.dynamicForm.reset();
        this.fields = [];
        this.presetUpdatedFinding = true;
        this.getFindings();
        this.store.dispatch(new ShowSuccessMessage("Finding was updated successfully."));
      }, error => this.store.dispatch(new ShowErrorMessage(error)));
  }

  upload(file: File) {
    let currentProcess = this.processes.find(x => x.text === this.currentProcess.processCode);
    let currentSubMenutem = currentProcess.subMenuItems.find(y => y.isSelected === true);
    let processes = {
      processCode: this.currentProcess.processCode,
      findingName: currentSubMenutem.text,
      metadataField: this.currentField.label,
      processName: this.currentProcess.name
    };

    let metadata = JSON.stringify(processes);
    this.showSpinner();
    this.uploadService.uploadFile(file, 'media', this.workId, this.storageAccount, this.sasToken, this.currentField.label, metadata, WorkFileType.Finding);
  }

  disregardChanges() {
    this.fields = [];
    this.presetSelectedFinding = true;
    this.setPageDetails();
  }

  onProcessSelected(item: MenuItem) {
    this.currentProcess = this.works.find(p => p.processId === item.id);
    this.store.dispatch(new SetCurrentWork(this.currentProcess));
    this.currentFinding = this.currentProcess.findings[0];
    this.dynamicForm.removeAllControls();
    this.setFindingDetails();
  }

  onFindingSelected(item?: MenuItem) {
    this.currentProcess = this.works.find(x => x.processId === item.parentId);
    this.store.dispatch(new SetCurrentWork(this.currentProcess));
    this.currentFinding = this.currentProcess.findings.find(x => x.id === item.id);
    this.dynamicForm.removeAllControls();
    this.setFindingDetails();
  }

  downloadFinding(item: MenuItem) {
    const name = `${this.currentProcess.name}_${item.text}`;
    this.store.dispatch(new DownloadFinding(item.id, name));
  }

  setFindingDetails() {
    if (!this.currentFinding) {
      this.fields = [];
    } else {
      this.fields = this.service.generateFields(this.currentFinding.fields);
    }
  }

  private getFindings() {
    this.showSpinner();
    this.service.getFindings(this.workId)
      .then(works => {
        this.works = works;
        this.hideSpinner();
        this.setPageDetails();
      }, err => this.store.dispatch(new ShowErrorMessage(err)));
  }

  private setPageDetails() {
    if (this.presetUpdatedFinding) {
      this.presetUpdatedFinding = false;
      const findings = this.works.find(w => w.id === this.currentProcess.id).findings;
      this.currentFinding = findings.find(f => f.id === this.currentFinding.id);
      this.setFindingDetails();
    } else if (this.presetCreatedFinding) {
      this.presetCreatedFinding = false;
      const findings = this.works.find(w => w.id === this.currentProcess.id).findings;
      this.currentFinding = findings[findings.length - 1];
      this.setFindingDetails();
      this.buildSideMenu();
      this.setActiveMenu();
    } else if (this.presetSelectedFinding) {
      this.presetSelectedFinding = false;
      this.dynamicForm.removeAllControls();
      this.setFindingDetails();
      this.buildSideMenu();
      this.setActiveMenu();
    } else {
      this.currentProcess = this.works[0];
      this.store.dispatch(new SetCurrentWork(this.currentProcess));
      this.currentFinding = this.currentProcess.findings[0];
      this.setFindingDetails();
      this.buildSideMenu();
      this.setActiveMenu();
    }
  }

  private setActiveMenu() {
    this.processes.forEach(p => {
      if (p.id === this.currentProcess.processId) {
        p.isSelected = true;
        p.subMenuItems.forEach(s => {
          if (s.id === this.currentFinding.id) s.isSelected = true;
        });
      }
    });
  }

  private buildSideMenu() {
    this.processes = [];
    this.works.forEach((work, i) => {
      const process: MenuItem = { id: work.processId, text: work.processCode };
      let findings: MenuItem[] = [];
      work.findings.forEach((_finding, index) => {
        const finding: MenuItem = { id: _finding.id, text: `Finding (${work.processCode}) - ${index + 1}`, parentId: work.processId };
        findings = [...findings, finding];
      });
      process.subMenuItems = findings;
      this.processes = [...this.processes, process];
    });
  }

  //#region Upload 

  showUploadModal(config: FieldConfiguration) {
    this.currentField = config;
    this.uploadModalTitle = config.label;
    this.uploadModal.show();
  }

  //#endregion
}
