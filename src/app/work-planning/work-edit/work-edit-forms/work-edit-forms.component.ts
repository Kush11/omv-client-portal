import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { MenuItem } from 'src/app/core/models/entity/menu-item';
import { Store, Select } from '@ngxs/store';
import { ShowSpinner, HideSpinner, ShowErrorMessage, ShowSuccessMessage, GetAzureUploadConfiguration, GetAzureSASToken } from 'src/app/state/app.actions';
import { FieldConfiguration } from 'src/app/shared/dynamic-components/field-setting';
import { Work } from 'src/app/core/models/entity/work';
import { SubSink } from 'subsink/dist/subsink';
import { BaseComponent } from 'src/app/shared/base/base.component';
import { WorkPlanningState } from '../../state/work-planning.state';
import { Observable } from 'rxjs/internal/Observable';
import { DynamicFormComponent } from 'src/app/shared/dynamic-components/components/dynamic-form.component';
import { ModalComponent } from 'src/app/shared/modal/modal.component';
import { WorkItemUploadService } from '../work-item-upload.service';
import { AppState } from 'src/app/state/app.state';
import { WorkEditFormsService } from './work-edit-forms.service';
import { WorkPlanningService } from 'src/app/core/services/business/work-planning/work-planning.service';
import { SideListMenuComponent } from 'src/app/shared/side-list-menu/side-list-menu.component';
import { SASTokenType } from 'src/app/core/enum/azure-sas-token';
import { SetCurrentWork, ClearCurrentWork, SetWorkFormStatus } from '../../state/work-planning.actions';
import { WorkFileType } from 'src/app/core/models/entity/work-item';
import { Status } from 'src/app/core/enum/status.enum';

@Component({
  selector: 'app-work-edit-forms',
  templateUrl: './work-edit-forms.component.html',
  styleUrls: ['./work-edit-forms.component.css']
})
export class WorkEditFormsComponent extends BaseComponent implements OnInit, OnDestroy {

  @Select(WorkPlanningState.getCurrentWorkId) currentWorkId$: Observable<number>;
  @Select(AppState.getAzureContainer) container$: Observable<string>;
  @Select(AppState.getAzureStorageAccount) storageAccount$: Observable<string>;
  @Select(AppState.getMediaWriteSASToken) azureWriteSASToken$: Observable<string>;
  @Select(WorkPlanningState.getCurrentWorkParent) currentWorkParent$: Observable<string>;
  @Select(WorkPlanningState.getSignOffStatus) signOffStatus$: Observable<boolean>;

  @ViewChild("dynamicForm") dynamicForm: DynamicFormComponent;
  @ViewChild("uploadModal") uploadModal: ModalComponent;
  @ViewChild("sideMenu") sideMenu: SideListMenuComponent;

  private subs = new SubSink();
  workTitle: string;
  works: Work[];
  processes: MenuItem[] = [];
  fields: FieldConfiguration[] = [];
  workName: string;
  workId: number;
  currentProcess: Work;
  canAdvance: boolean;
  advanceButtonText: string;
  currentStepName: string;
  currentField: FieldConfiguration;
  uploadModalTitle: string;
  containerName: any;
  storageAccount: any;
  sasToken: any;
  persistSelection: boolean;
  currentWorkParent: string;

  constructor(protected store: Store, private service: WorkEditFormsService, private workPlanningService: WorkPlanningService,
    private uploadService: WorkItemUploadService) {
    super(store);
    this.setPageTitle("Work Form");
  }

  ngOnInit() {
    this.store.dispatch(new ShowSpinner());
    this.workTitle = "Process Types";

    this.store.dispatch(new GetAzureUploadConfiguration());
    this.store.dispatch(new GetAzureSASToken(SASTokenType.Write));

    this.subs.add(
      this.currentWorkId$
        .subscribe(workId => {
          this.workId = workId;
          this.getForms();
        }),
      this.container$
        .subscribe(container => this.containerName = container),
      this.storageAccount$
        .subscribe(storageAccount => this.storageAccount = storageAccount),
      this.azureWriteSASToken$
        .subscribe(SASToken => this.sasToken = SASToken),
      this.currentWorkParent$
        .subscribe(currentWorkParent => this.currentWorkParent = currentWorkParent),
      this.signOffStatus$
        .subscribe(isSignedOff => {
          if (isSignedOff) {
            this.signOff();
          }
        })
    );
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
    this.store.dispatch(new ClearCurrentWork());
  }

  signOff() {
    if (this.dynamicForm.dirty) {
      if (this.dynamicForm.valid) {
        this.showSpinner();
        const metadata = this.dynamicForm ? JSON.stringify(this.dynamicForm.value) : '{}';
        const { id } = this.currentProcess;
        if (this.dynamicForm.dirty && this.dynamicForm.valid) {
          this.workPlanningService.updateForm(id, metadata).toPromise()
            .then(() => {
              this.persistSelection = true;
              this.store.dispatch(new ShowSuccessMessage("Form was saved successfully."));
              this.performSignOff(id);
            }, error => this.store.dispatch(new ShowErrorMessage(error)));
        } else if (this.dynamicForm.valid && this.dynamicForm.pristine) {
          this.performSignOff(id);
        }
      } else {
        this.dynamicForm.controls.forEach(config => {
          if (config.required) {
            this.dynamicForm.markAsTouched(config.name);
          }
        });
      }
    } else {
      this.dynamicForm.controls.forEach(config => {
        if (config.required) {
          this.dynamicForm.markAsTouched(config.name);
        }
      });
    }
  }

  performSignOff(workId: number) {
    this.workPlanningService.signOff(workId)
      .then(() => {
        this.store.dispatch(new ShowSuccessMessage("Operation was successful."));
        this.persistSelection = true;
        this.hideSpinner();
        this.getForms();
      }, error => this.store.dispatch(new ShowErrorMessage(error)));
  }

  onSelect(item: MenuItem) {
    this.currentProcess = this.works.find(w => w.processId === item.id);
    this.store.dispatch(new SetCurrentWork(this.currentProcess));
    this.setProcessDetails();
  }

  updateMetadata() {
    const metadata = this.dynamicForm ? JSON.stringify(this.dynamicForm.value) : '{}';
    const { id } = this.currentProcess;
    this.showSpinner();
    this.workPlanningService.updateForm(id, metadata).toPromise()
      .then(() => {
        this.persistSelection = true;
        this.getForms();
        this.store.dispatch(new ShowSuccessMessage("Form was saved successfully."));
      }, error => this.store.dispatch(new ShowErrorMessage(error)));
  }

  disregardChanges() {
    this.fields = [];
    this.persistSelection = true;
    this.setPageDetails();
  }

  showUploadModal(config: FieldConfiguration) {
    this.currentField = config;
    this.uploadModalTitle = config.label;
    this.uploadModal.show();
  }

  upload(file: File) {
    let meta: any = {};
    this.currentProcess.metadata.forEach(data => {
      meta[data.name] = data.value;
    });
    meta[this.currentField.name] = this.currentField.value;
    meta.processCode = this.currentProcess.processCode;
    meta.asset = this.currentProcess.assetName;
    meta.workParent = this.currentWorkParent;

    let metadata = JSON.stringify(meta);
    this.showSpinner();
    this.uploadService.uploadFile(file, 'media', this.workId, this.storageAccount, this.sasToken, this.currentField.label, metadata, WorkFileType.Form);
  }

  setProcessDetails() {
    if (this.currentProcess) {
      this.dynamicForm.removeAllControls();
      this.fields = this.service.generateProcessForms(this.currentProcess.forms);
    }
  }

  private getForms() {
    this.service.getWorks(this.workId)
      .then(works => {
        this.works = works;
        this.hideSpinner();
        this.setPageDetails();
      }, err => this.store.dispatch(new ShowErrorMessage(err)));
  }

  private setPageDetails() {
    if (this.persistSelection) {
      this.persistSelection = false;
      this.currentProcess = this.works.find(w => w.processId === this.currentProcess.processId);
      this.store.dispatch(new SetCurrentWork(this.currentProcess));
      this.dynamicForm.removeAllControls();
      this.setProcessDetails();
      this.buildSideMenu();
      this.setActiveMenu();
    } else {
      this.currentProcess = this.works[0];
      this.store.dispatch(new SetCurrentWork(this.currentProcess));
      this.setProcessDetails();
      this.buildSideMenu();
      this.setActiveMenu();
    }
    this.dynamicForm.markFormAsUnTouchedAndPristine();
  }

  private setActiveMenu() {
    this.processes.forEach(p => p.isSelected = p.id === this.currentProcess.processId);
  }

  private buildSideMenu() {
    this.processes = [];
    this.works.forEach(work => {
      const process: MenuItem = { text: work.processCode, id: work.processId };
      this.processes = [...this.processes, process];
    });
  }
}
