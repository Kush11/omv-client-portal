import { Injectable } from '@angular/core';
import { WorkPlanningService } from 'src/app/core/services/business/work-planning/work-planning.service';
import { FieldType, FieldConfiguration } from 'src/app/shared/dynamic-components/field-setting';
import { MetadataField } from 'src/app/core/models/entity/metadata-field';
import { ValidatorFn, Validators } from '@angular/forms';
import { MetadataFieldType } from 'src/app/core/enum/metadataFieldType';
import { Work } from 'src/app/core/models/entity/work';
import { DateService } from 'src/app/core/services/business/dates/date.service';

@Injectable({
  providedIn: 'root'
})
export class WorkEditFormsService {

  works: Work[];
  currentForms: FieldConfiguration[];

  constructor(private workPlanningService: WorkPlanningService, private dateService: DateService) { }

  async getWorks(workId: number): Promise<Work[]> {
    this.works = await this.workPlanningService.getForms(workId).toPromise();
    return this.works;
  }

  generateProcessForms(fields: MetadataField[]): FieldConfiguration[] {
    let forms: FieldConfiguration[] = [];
    fields.forEach(field => {
      forms = [
        ...forms,
        this.buildFormField(field)
      ];
    });
    return forms;
  }

  buildFormField(field: MetadataField): FieldConfiguration {
    let config: FieldConfiguration;
    if (!field.canEdit) {
      config = this.buildLabel(field);
    } else {
      switch (field.type) {
        case MetadataFieldType.Text:
          config = this.buildTextBox(field);
          break;
        case MetadataFieldType.Select:
          config = this.buildDropdown(field);
          break;
        case MetadataFieldType.Date:
          config = this.buildDate(field);
          break;
        case MetadataFieldType.Nested:
          config = this.buildNestedDropdown(field);
          break;
      }
    }
    return config;
  }

  private buildLabel(item: MetadataField): FieldConfiguration {
    return {
      type: FieldType.Label,
      name: item.label,
      label: item.label,
      value: item.type === MetadataFieldType.Date ? (item.value ? this.dateService.formatToFullDate(new Date(item.value)) : 'N/A') : item.value || 'N/A',
      order: item.order
    };
  }

  private buildTextBox(item: MetadataField): FieldConfiguration {
    return {
      type: FieldType.Input,
      data: item,
      id: item.id,
      label: item.label || item.name,
      inputType: "text",
      name: item.name,
      order: item.order,
      allowDocumentUpload: item.canUpload,
      placeholder: item.label || item.name,
      required: item.isRequired,
      value: item.value || '',
      validations: this.getValidations(item)
    };
  }

  private buildDropdown(item: MetadataField): FieldConfiguration {
    return {
      type: FieldType.Select,
      data: item,
      id: item.id,
      label: item.label || item.name,
      name: item.name,
      order: item.order,
      options: item.options,
      placeholder: 'Select',
      required: item.isRequired,
      value: item.value || '',
      allowDocumentUpload: item.canUpload,
      validations: this.getValidations(item)
    };
  }

  private buildNestedDropdown(item: MetadataField): FieldConfiguration {
    return {
      type: FieldType.Select,
      data: item,
      id: item.id,
      parentId: item.parentId,
      label: item.label || item.name,
      name: item.name,
      order: item.order,
      options: item.options,
      placeholder: 'Select',
      value: item.value || '',
      required: item.isRequired,
      isNested: true,
      allowDocumentUpload: item.canUpload,
      isEnabled: false
    };
  }

  private buildDate(item: MetadataField): FieldConfiguration {
    return {
      type: FieldType.Date,
      data: item,
      id: item.id,
      label: item.label || item.name,
      name: item.name,
      order: item.order,
      required: item.isRequired,
      ignoreMaxDate: true,
      value: item.value ? new Date(item.value) : null,
      allowDocumentUpload: item.canUpload,
      validations: this.getValidations(item)
    };
  }

  private getValidations(item: MetadataField): ValidatorFn[] {
    let validations: ValidatorFn[] = [];
    if (item.isRequired) {
      let requiredValidation: any = {
        name: 'required',
        validator: Validators.required,
        message: `${item.label || item.name} is required`
      };
      validations.push(requiredValidation);
    }
    return validations;
  }
}