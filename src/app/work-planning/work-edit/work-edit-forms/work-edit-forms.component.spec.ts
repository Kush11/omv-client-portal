import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkEditFormsComponent } from './work-edit-forms.component';

describe('WorkEditFormsComponent', () => {
  let component: WorkEditFormsComponent;
  let fixture: ComponentFixture<WorkEditFormsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WorkEditFormsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkEditFormsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
