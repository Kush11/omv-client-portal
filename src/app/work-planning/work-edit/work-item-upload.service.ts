import { Injectable } from '@angular/core';
import { BlobService, UploadParams } from 'angular-azure-blob-service';
import { WorkFile, WorkFileType } from 'src/app/core/models/entity/work-item';
import { Store } from '@ngxs/store';
import { CreateWorkFile } from '../state/work-planning.actions';
import { ShowErrorMessage } from 'src/app/state/app.actions';

@Injectable({
  providedIn: 'root'
})
export class WorkItemUploadService {
  config: any;
  percent: any;

  constructor(private blob: BlobService, private store: Store) { }

  uploadFile(file: File, containerName: string, workId: number, storageAccount: string, sasToken: string, fieldName: string, metadata: string, type: number) {
    const Config: UploadParams = {
      sas: sasToken,
      storageAccount: storageAccount,
      containerName: containerName
    };
    let splitByLastDot = function (text) {
      const index = text.lastIndexOf('.');
      return [text.slice(0, index), text.slice(index + 1)]
    }

    if (file !== null) {
      const baseUrl = this.blob.generateBlobUrl(Config, `work-planning/${file.name}`);
      const customBlockSize = this.getBlockSize(file.size);
      this.config = {
        baseUrl: baseUrl,
        sasToken: Config.sas,
        blockSize: customBlockSize,
        file: file,
        complete: () => {
          let thumbnail = `https://${Config.storageAccount}.blob.core.windows.net/thumbs/${file.name}`;

          let item = new WorkFile();
          item.size = file.size;
          item.name = file.name;
          item.contentType = file.type;
          item.url = this.config.baseUrl;
          item.type = splitByLastDot(file.name).pop().toUpperCase();
          item.requester = 1;
          item.thumbnail = thumbnail;
          item.metadata = metadata;
          item.fileType = type;
          item.workId = workId;

          this.store.dispatch(new CreateWorkFile(item));
        },
        error: (err: { statusText: any; }) => {
          this.store.dispatch(new ShowErrorMessage(err.statusText));
        },
        progress: (percent: any) => {
          this.percent = percent;
        }
      };
      this.blob.upload(this.config);
    }
  }

  private getBlockSize(filesize: number) {
    let customBlockSize = 0;
    const size10Gb = 1024 * 1024 * 1024 * 10;
    const size500Mb = 1024 * 1024 * 500;
    const size32Mb = 1024 * 1024 * 32;
    const size5mb = 1024 * 1024 * 5;
    if (filesize < size5mb) {
      customBlockSize = (1024 * 32);
    } else if (filesize < size32Mb) {
      customBlockSize = (1024 * 512);
    } else if (filesize < size500Mb) {
      customBlockSize = (1024 * 1024 * 4);
    } else if (filesize < size10Gb) {
      customBlockSize = (1024 * 1024 * 10);
    } else {
      customBlockSize = (1024 * 1024 * 100);
    }
    return customBlockSize;
  }
}
