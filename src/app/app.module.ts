import { ChartAllModule, StackingColumnSeriesService, CategoryService, LegendService, TooltipService, DataLabelService, LineSeriesService,
   BarSeriesService, StackingBarSeriesService } from '@syncfusion/ej2-angular-charts';
import { AppState } from './state/app.state';
import { ReportIssueState } from '../../src/app/state/tickets/tickets.state';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';

import { AppRoutingModule } from './app.routing.module';
import { AppComponent } from './app.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { HttpModule } from '@angular/http';
import { SettingsService } from './core/services/data/appsettings/appsettings.service';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgxsModule } from '@ngxs/store';
import { NgxsStoragePluginModule } from '@ngxs/storage-plugin';
import { NgxsReduxDevtoolsPluginModule } from '@ngxs/devtools-plugin';
import { NgxsLoggerPluginModule } from '@ngxs/logger-plugin';
import { SharedModule } from './shared/shared.module';
import { AuthCallbackComponent } from './auth-callback/auth-callback.component';
import { BlobModule } from 'angular-azure-blob-service';
import { ToastModule } from '@syncfusion/ej2-angular-notifications';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { HttpInterceptorService } from './core/services/httpinterceptor.service';
import { environment } from 'src/environments/environment';
import { UsersDataService } from './core/services/data/users/users.data.service';
import { UsersMockDataService } from './core/services/data/users/users.mock.data.service';
import { UsersWebDataService } from './core/services/data/users/users.web.data.service';
import { UnAuthorizedComponent } from './unauthorized/unauthorized.component';
import { AuthorizationCheckComponent } from './authorization-check/authorization-check.component';
import { OktaDataService } from './core/services/data/okta/okta.service';
import { OktaWebDataService } from './core/services/data/okta/okta.web.service';
import { CustomersDataService } from './core/services/data/customers/customers.data.service';
import { CustomersMockDataService } from './core/services/data/customers/customers.mock.data.service';
import { CustomersWebDataService } from './core/services/data/customers/customers.web.data.service';
import { ReportIssuesComponent } from './report-issues/report-issues.component';
import { DropDownListModule } from '@syncfusion/ej2-angular-dropdowns';
import { CheckBoxModule, ButtonModule } from '@syncfusion/ej2-angular-buttons';
import { RadioButtonModule } from '@syncfusion/ej2-angular-buttons';
import { TimePickerModule } from '@syncfusion/ej2-angular-calendars';
import { TicketsDataService } from './core/services/data/tickets/tickets.data.service';
import { TicketsMockDataService } from './core/services/data/tickets/tickets.mock.service';
import { TicketWebDataService } from './core/services/data/tickets/tickets.web.data.service';
import { WorkPlanningDataService } from './core/services/data/work-planning/work-planning.data.service';
import { WorkPlanningWebDataService } from './core/services/data/work-planning/work-planning.web.data.service';
import { WorkPlanningMockDataService } from './core/services/data/work-planning/work-planning.mock.data.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ClearLocalStorageComponent } from './clear-local-storage/clear-local-storage.component';
import { AuthGuard } from './core/guards/auth-guard.service';
import { AuthService } from './core/services/business/auth.service';
import { LoginComponent } from './login/login.component';
import { DialogModule } from '@syncfusion/ej2-angular-popups';
import {  PageService, ToolbarService } from '@syncfusion/ej2-angular-grids';
import { PageService as TreeGridPageService, ContextMenuService, SortService as TreeGridSortService, TreeGridModule } from '@syncfusion/ej2-angular-treegrid';
import {
  PageService as GridPageService, SearchService as GridSearchService, GridModule, SortService as GridSortService,
  PagerModule, DetailRowService, ExcelExportService, ToolbarService as GridToolbarService
} from '@syncfusion/ej2-angular-grids';


@NgModule({
  declarations: [
    AppComponent,
    AuthCallbackComponent,
    LoginComponent,
    DashboardComponent,
    UnAuthorizedComponent,
    AuthorizationCheckComponent,
    ReportIssuesComponent,
    ClearLocalStorageComponent,
  ],
  imports: [
    ChartAllModule,
    DialogModule,
    ButtonModule,
    AppRoutingModule,
    BrowserModule,
    BrowserAnimationsModule,
    HttpModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    DropDownListModule,
    CheckBoxModule,
    RadioButtonModule,
    TimePickerModule,
    SharedModule,
    ToastModule,
    PagerModule,
    GridModule,
    // TreeGridModule,
    NgxsModule.forRoot([AppState, ReportIssueState], { developmentMode: !environment.production }),
    NgxsStoragePluginModule.forRoot({}),
    NgxsReduxDevtoolsPluginModule.forRoot(),
    NgxsLoggerPluginModule.forRoot(),
    BlobModule.forRoot()
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: HttpInterceptorService, multi: true },
    { provide: CustomersDataService, useClass: environment.useMocks ? CustomersMockDataService : CustomersWebDataService },
    { provide: UsersDataService, useClass: environment.useMocks ? UsersMockDataService : UsersWebDataService },
    { provide: OktaDataService, useClass: environment.useMocks ? OktaWebDataService : OktaWebDataService },
    { provide: TicketsDataService, useClass: environment.useMocks ? TicketsMockDataService : TicketWebDataService },
    { provide: WorkPlanningDataService, useClass: environment.useMocks ? WorkPlanningMockDataService : WorkPlanningWebDataService },
    SettingsService,
    AuthGuard,
    AuthService,
    PageService,
    ToolbarService,
    StackingColumnSeriesService,
    TreeGridPageService,
    TreeGridSortService,
    ContextMenuService,
    GridPageService,
    GridSortService,
    GridToolbarService,
    GridSearchService,
    CategoryService, LegendService, TooltipService, DataLabelService, LineSeriesService, BarSeriesService, StackingBarSeriesService
  ],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA]
})
export class AppModule { }
