import { ConfigurationSetting } from './../core/enum/configuration-setting';

import {
  ShowConfirmationBox,
  ClearNotification,
  Confirmation,
  messageType,
  ClearConfirmation,
  SetPageTitle,
  ShowLeftNav,
  LogOut,
  GetLoggedInUser,
  DeviceWidth,
  ShowSpinner,
  HideSpinner,
  RemoveLoggedInUser,
  GetAzureUploadConfiguration,
  ShowSuccessMessage,
  ShowErrorMessage,
  SetPreviousRoute,
  SetBreadcrumbs,
  UpdateBreadcrumb,
  ClearBreadcrumbs,
  GetWorkPlanningParentItems,
  ShowDownloadsMessage,
  SetDownloadStatus,
  ShowDownloadMessage,
  HideToast,
  ShowWarningMessage,
  HideFullScreen,
  GetAzureSASToken,
  ShowFullScreen,
  GetUserDashboardWidgets,
  UpdateUserDashboardWidgets,
  GetAllWidgets,
  SaveCompanyLogo,
  GetMediaWriteSASToken,
  ShowInfoMessage,
  CloseToast,
} from './app.actions';
import { State, Selector, Action, StateContext } from '@ngxs/store';
import { AuthService } from '../core/services/business/auth.service';
import { Permission } from '../core/enum/permission';
import { tap } from 'rxjs/operators';
import { Toast, ToastType, ToastAction } from '../core/enum/toast';
import { UsersDataService } from '../core/services/data/users/users.data.service';
import { User } from '../core/models/entity/user';
import { CustomersDataService } from '../core/services/data/customers/customers.data.service';

import { Breadcrumb } from '../core/models/breadcrumb';
import { WorkPlanningService } from '../core/services/business/work-planning/work-planning.service';
import { WorkPlanningParentItem } from '../core/models/work-planning';
import { Widget } from '../core/models/entity/widget';
import { CustomersService } from '../core/services/business/customers/customers.service';
import { UsersService } from '../core/services/business/users/users.service';
import { SASTokenType, SASTokenModule } from '../core/enum/azure-sas-token';

export class AppStateModel {
  breadcrumbs: Breadcrumb[];
  previousRoute: string;
  showLeftNav: boolean;
  setPageTitle: string;
  currentUser: User;
  currentUserId: number;
  permissions: Permission[];
  currentUserPermissions: User;
  message: string;
  messageType: messageType;
  confirmationBox: boolean;
  confirmation: boolean;
  showFullScreen: boolean;

  toastMessage?: Toast;
  downloadMessage: string;
  error: string;
  deviceWidth: number;
  gridData: any[];
  showSpinner: boolean;

  isAuthorized: boolean;

  coreWriteSASToken: string;
  mediaReadSASToken: string;
  mediaWriteSASToken: string;
  workPlanningReadSASToken: string;
  workPlanningWriteSASToken: string;
  azureContainer: string;
  azureStorageAccount: string;
  companyLogo: string;

  workPlanningParentItems: WorkPlanningParentItem[];

  // Downloads
  totalDownloads: number;
  downloadStatus: any;

  // bulkuploader
  bulkUploaderMacContainerUrl: string;
  bulkUploaderWindowsContainerUrl: string;

  // dashboard
  sections: any[];
  allWidgets: Widget[];

  //jw Player
  JwtLicenseKey: string;
}

@State<AppStateModel>({
  name: 'app',
  defaults: {
    breadcrumbs: [],
    previousRoute: '',
    showLeftNav: false,
    setPageTitle: 'OMV Client Portal',
    currentUser: null,
    currentUserId: 1,
    permissions: [],
    currentUserPermissions: null,
    message: null,
    messageType: messageType.success,
    confirmationBox: false,
    confirmation: false,
    showFullScreen: false,

    toastMessage: null,
    downloadMessage: null,
    error: '',
    deviceWidth: window.innerWidth,
    gridData: [],
    showSpinner: false,

    // isUserAuthenticated: null,
    isAuthorized: false,

    azureContainer: '',
    coreWriteSASToken: '',
    mediaReadSASToken: '',
    mediaWriteSASToken: '',
    workPlanningReadSASToken: '',
    workPlanningWriteSASToken: '',
    azureStorageAccount: '',
    companyLogo: '',

    workPlanningParentItems: [],

    // Downloads
    totalDownloads: null,
    downloadStatus: null,

    // Bulkuploader
    bulkUploaderMacContainerUrl: '',
    bulkUploaderWindowsContainerUrl: '',

    // Dashboard
    sections: null,
    allWidgets: null,

    JwtLicenseKey:'',
  }
})
export class AppState {

  //#region SELECTORS

  @Selector()
  static getPreviousRoute(state: AppStateModel) {
    return state.previousRoute;
  }

  @Selector()
  static getLeftNavVisibility(state: AppStateModel) {
    return state.showLeftNav;
  }

  @Selector()
  static getSpinnerVisibility(state: AppStateModel) {
    return state.showSpinner;
  }

  @Selector()
  static getPageTitle(state: AppStateModel) {
    return state.setPageTitle;
  }

  @Selector()
  static getLoggedInUser(state: AppStateModel) {
    return state.currentUser;
  }

  @Selector()
  static getCurrentUserId(state: AppStateModel) {
    return state.currentUserId;
  }

  @Selector()
  static getUserPermissions(state: AppStateModel) {
    return state.permissions;
  }

  @Selector()
  static getToastMessage(state: AppStateModel) {
    return state.toastMessage;
  }


  //#region Doshaboard

  @Selector()
  static getUserDashboardWidgets(state: AppStateModel) {
    return state.sections;
  }

  @Selector()
  static getAllWidgets(state: AppStateModel) {
    return state.allWidgets;
  }

  //#region Download Items

  @Selector()
  static getDownloadMessage(state: AppStateModel) {
    return state.downloadMessage;
  }

  @Selector()
  static getTotalDownloads(state: AppStateModel) {
    return state.totalDownloads;
  }

  @Selector()
  static getDownloadStatus(state: AppStateModel) {
    return state.downloadStatus;
  }

  //#endregion

  @Selector()
  static setNotification(state: AppStateModel) {
    return { message: state.message, messageType: state.messageType };
  }

  @Selector()
  static showConfirmationBox(state: AppStateModel) {
    return state.confirmationBox;
  }

  @Selector()
  static confirmation(state: AppStateModel) {
    return state.confirmation;
  }

  @Selector()
  static getErrorMessage(state: AppStateModel) {
    return state.error;
  }

  @Selector()
  static setDeviceWidth(state: AppStateModel) {
    return state.deviceWidth;
  }

  @Selector()
  static getIsAuthorized(state: AppStateModel) {
    return state.isAuthorized;
  }

  @Selector()
  static getCoreWriteSASToken(state: AppStateModel) {
    return state.coreWriteSASToken;
  }

  @Selector()
  static getMediaReadSASToken(state: AppStateModel) {
    return state.mediaReadSASToken;
  }

  @Selector()
  static getMediaWriteSASToken(state: AppStateModel) {
    return state.mediaWriteSASToken;
  }

  @Selector()
  static getWorkPlanningReadSASToken(state: AppStateModel) {
    return state.workPlanningReadSASToken;
  }

  @Selector()
  static getWorkPlanningWriteSASToken(state: AppStateModel) {
    return state.workPlanningWriteSASToken;
  }

  @Selector()
  static getAzureContainer(state: AppStateModel) {
    return state.azureContainer;
  }

  @Selector()
  static getAzureStorageAccount(state: AppStateModel) {
    return state.azureStorageAccount;
  }

  @Selector()
  static getBulkUploaderMacContainerUrl(state: AppStateModel) {
    return state.bulkUploaderMacContainerUrl;
  }

  @Selector()
  static getBulkUploaderWindowsContainerUrl(state: AppStateModel) {
    return state.bulkUploaderWindowsContainerUrl;
  }

  @Selector()
  static getCompanyLogo(state: AppStateModel) {
    return state.companyLogo;
  }

  //#endregion

  @Selector()
  static getBreadcrumbs(state: AppStateModel) {
    return state.breadcrumbs;
  }

  @Selector()
  static getWorkPlanningParentItems(state: AppStateModel) {
    return state.workPlanningParentItems;
  }

  @Selector()
  static getFullScreenMode(state: AppStateModel) {
    return state.showFullScreen;
  }

  @Selector()
  static getJwtlicenceKey(state: AppStateModel) {
    return state.JwtLicenseKey;
  }

  constructor(private auth: AuthService, private usersService: UsersService, private usersDataService: UsersDataService,
    private customersDataService: CustomersDataService, private customersService: CustomersService,
    private workPlanningService: WorkPlanningService) { }

  //#region DASHBOARD
  @Action(GetUserDashboardWidgets)
  getUserDashboardWidgets(ctx: StateContext<AppStateModel>) {
    ctx.dispatch(new ShowSpinner());
    return this.usersService.getUserDashboardWidgets()
      .pipe(
        tap(sections => {
          const state = ctx.getState();
          ctx.setState({
            ...state,
            sections
          });
          ctx.dispatch(new HideSpinner());
        }, err => ctx.dispatch(new ShowErrorMessage(err)))
      );
  }

  @Action(UpdateUserDashboardWidgets)
  updateUserDashboardWidgets(ctx: StateContext<AppStateModel>, { panels }: UpdateUserDashboardWidgets) {
    ctx.dispatch(new ShowSpinner());
    return this.usersService.updateUserDashboardWigets(panels)
      .pipe(
        tap(sections => {
          ctx.dispatch(new ShowSuccessMessage('Widget(s) Updated Successfully!'));
          const state = ctx.getState();
          ctx.setState({
            ...state,
            sections
          });
          ctx.dispatch(new HideSpinner());
        }, err => ctx.dispatch(new ShowErrorMessage(err)))
      );
  }

  @Action(GetAllWidgets)
  getWidgets(ctx: StateContext<AppStateModel>) {
    return this.usersService.getAllWidgets()
      .pipe(
        tap(widgets => {
          const state = ctx.getState();
          ctx.setState({
            ...state,
            allWidgets: widgets
          });
        })
      );
  }

  //#endregion

  //#region ACTIONS

  @Action(SetPreviousRoute)
  setPreviousRoute(ctx: StateContext<AppStateModel>, { route }: SetPreviousRoute) {
    const state = ctx.getState();
    ctx.setState({
      ...state,
      previousRoute: route
    });
  }

  @Action(ShowLeftNav)
  setLeftNavToggle({ getState, setState }: StateContext<AppStateModel>, { payload }: ShowLeftNav) {
    const state = getState();
    setState({
      ...state,
      showLeftNav: payload
    });
  }

  @Action(ShowSpinner)
  showSpinner({ getState, setState }: StateContext<AppStateModel>) {
    const state = getState();
    setState({
      ...state,
      showSpinner: true
    });
  }

  @Action(HideSpinner)
  hideSpinner({ getState, setState }: StateContext<AppStateModel>) {
    const state = getState();
    setState({
      ...state,
      showSpinner: false
    });
  }

  @Action(SetPageTitle)
  setPageTitle({ getState, setState }: StateContext<AppStateModel>, { payload }: SetPageTitle) {
    const state = getState();
    setState({
      ...state,
      setPageTitle: payload
    });
  }

  @Action(GetLoggedInUser)
  getLoggedinUser(ctx: StateContext<AppStateModel>) {
    return this.usersDataService.getLoggedInUser()
      .pipe(
        tap(user => {
          const state = ctx.getState();
          ctx.setState({
            ...state,
            currentUser: user,
            isAuthorized: true,
          });
          ctx.dispatch(new GetAzureSASToken());
          ctx.dispatch(new GetAzureUploadConfiguration());
        }, err => {
          const state = ctx.getState();
          ctx.setState({
            ...state,
            currentUser: null,
            isAuthorized: false
          });
          if (err.status === 404) {
            this.auth.logout();
          }
        })
      );
  }

  @Action(RemoveLoggedInUser)
  removeLoggedInUser({ getState, setState }: StateContext<AppStateModel>) {
    const state = getState();
    setState({
      ...state,
      currentUser: null
    });
  }

  @Action(LogOut)
  async logOut({ getState, setState }: StateContext<AppStateModel>) {
    await this.auth.logout()
      .then(() => {
        const state = getState();
        setState({
          ...state,
          currentUser: null,
          isAuthorized: false
        });
      });
  }

  @Action(SaveCompanyLogo)
  saveCompanyLogo({ getState, setState }: StateContext<AppStateModel>, { logo }: SaveCompanyLogo) {
    const state = getState();
    setState({
      ...state,
      companyLogo: logo
    });
  }

  //#region Azure Config

  @Action(GetAzureSASToken)
  getReadAzureSASToken(ctx: StateContext<AppStateModel>, { type, module }: GetAzureSASToken) {
    return this.customersService.getAzureSASToken(type, module)
      .then(sasToken => {
        const state = ctx.getState();
        if ((!type || type === SASTokenType.Read) && (!module || module === SASTokenModule.Media)) {
          ctx.setState({
            ...state,
            mediaReadSASToken: sasToken
          });
        } else if (type === SASTokenType.Write && (!module || module === SASTokenModule.Media)) {
          ctx.setState({
            ...state,
            mediaWriteSASToken: sasToken
          });
        } else if (type === SASTokenType.Read && module === SASTokenModule.WorkPlanning) {
          ctx.setState({
            ...state,
            workPlanningReadSASToken: sasToken
          });
        } else if (type === SASTokenType.Write && module === SASTokenModule.WorkPlanning) {
          ctx.setState({
            ...state,
            workPlanningWriteSASToken: sasToken
          });
        } else if (type === SASTokenType.Write && module === SASTokenModule.Core) {
          ctx.setState({
            ...state,
            coreWriteSASToken: sasToken
          });
        }
      }, error => ctx.dispatch(new ShowErrorMessage(error)));
  }
  @Action(GetMediaWriteSASToken)
  getMediaWriteSASToken(ctx: StateContext<AppStateModel>) {
    return this.customersDataService.getWriteSASToken()
      .pipe(
        tap(response => {
          const state = ctx.getState();
          ctx.setState({
            ...state,
            mediaWriteSASToken: response
          });
        }, error => ctx.dispatch(new ShowErrorMessage(error)))
      );
  }


  @Action(GetAzureUploadConfiguration)
  getAzureUploadConfiguration(ctx: StateContext<AppStateModel>) {
    this.customersService.getSettings()
      .then(configurations => {
        const state = ctx.getState();
        const azureContainer = configurations.find(ac => ac.settingKey == ConfigurationSetting.MediaContainer);
        const azureStorageAccount = configurations.find(ac => ac.settingKey == ConfigurationSetting.StorageAccount);
        const bulkUploaderMacContainerUrl = configurations.find(ac => ac.settingKey == ConfigurationSetting.BulkUploaderMacContainerUrl);
        const bulkUploaderWindowsContainerUrl = configurations.find(ac => ac.settingKey == ConfigurationSetting.BulkUploaderWindowsContainerUrl);
        const JwtLicenseKey = configurations.find(ac => ac.settingKey == ConfigurationSetting.JWPlayerKey);
       
        ctx.setState({
          ...state,
          azureContainer: azureContainer.value,
          azureStorageAccount: azureStorageAccount.value,
          bulkUploaderMacContainerUrl: bulkUploaderMacContainerUrl.value,
          bulkUploaderWindowsContainerUrl: bulkUploaderWindowsContainerUrl.value,
          JwtLicenseKey: JwtLicenseKey.value,
        });
      }, err => ctx.dispatch(new ShowErrorMessage(err)));
  }

  //#endregion

  @Action(ShowFullScreen)
  showFullScreen({ getState, setState }: StateContext<AppStateModel>) {
    const state = getState();
    setState({
      ...state,
      showFullScreen: true
    })
  }

  @Action(HideFullScreen)
  hideFullScreen({ getState, setState }: StateContext<AppStateModel>) {
    const state = getState();
    setState({
      ...state,
      showFullScreen: false
    })
  }

  @Action(ClearNotification)
  clearNotification({ getState, setState }: StateContext<AppStateModel>) {
    const state = getState();
    setState({
      ...state,
      message: null,
      messageType: messageType.success
    })
  }

  @Action(ShowConfirmationBox)
  showConfirmationBox({ getState, setState }: StateContext<AppStateModel>, { show }: ShowConfirmationBox) {
    const state = getState();

    setState({
      ...state,
      confirmationBox: show,
    });

  }

  @Action(ClearConfirmation)
  clearConfirmation({ getState, setState }: StateContext<AppStateModel>) {
    const state = getState();

    setState({
      ...state,
      confirmation: false,
    });
  }

  @Action(Confirmation)
  confirmation(ctx: StateContext<AppStateModel>) {
    const state = ctx.getState();
    ctx.setState({
      ...state,
      confirmation: true
    });
    setTimeout(() => {
      ctx.dispatch(new ClearConfirmation)
    }, 100)
  }

  @Action(ShowSuccessMessage)
  showSuccessMessage(ctx: StateContext<AppStateModel>, { message }: ShowSuccessMessage) {
    const state = ctx.getState();
    const toast: Toast = { message: message, type: ToastType.Success };
    ctx.setState({
      ...state,
      toastMessage: toast
    });
    ctx.setState({ ...state, toastMessage: null });
  }

  @Action(ShowInfoMessage)
  showInfoMessage(ctx: StateContext<AppStateModel>, { message, timeOut, icon }: ShowInfoMessage) {
    const state = ctx.getState();
    const toast: Toast = { message: message, type: ToastType.Info, timeOut: timeOut, iconType: icon };
    ctx.setState({
      ...state,
      toastMessage: toast
    });
    ctx.setState({ ...state, toastMessage: null });
  }

  @Action(ShowWarningMessage)
  showWarningMessage(ctx: StateContext<AppStateModel>, { message, title, timeOut }: ShowWarningMessage) {
    const state = ctx.getState();
    const toast: Toast = { message: message, type: ToastType.Warning, title: title, timeOut: timeOut };
    ctx.setState({
      ...state,
      toastMessage: toast
    });
    ctx.setState({ ...state, toastMessage: null });
  }

  @Action(ShowErrorMessage)
  showErrorMessage(ctx: StateContext<AppStateModel>, { payload }: ShowErrorMessage) {
    ctx.dispatch(new HideSpinner());
    const state = ctx.getState();
    if (!payload) return;
    let message = '';
    if (typeof payload === 'string') message = payload;
    else {
      message = payload.error ? (payload.error.message ? payload.error.message : payload.error) : '';
      if (payload.status == 401 || payload.errorCode === 'login_required') {
        const route = window.location.pathname;
        this.saveReturnUrl(route);
        this.auth.logout();
        return;
      }
    }
    const toast: Toast = { message: message, type: ToastType.Error };
    ctx.setState({
      ...state,
      toastMessage: toast
    });
    ctx.setState({ ...state, toastMessage: null });
  }

  private saveReturnUrl(route: string) {
    const ignored_routes = ['/startup', '/authorize-check', '/implicit/callback', '/login'];
    if (ignored_routes.includes(route) || route.includes('/implicit/callback')) { return; }
    const key = 'omv-client-return-url';
    if (localStorage.getItem(key)) { localStorage.removeItem(key); }
    localStorage.setItem(key, route);
  }

  @Action(CloseToast)
  closeToast(ctx: StateContext<AppStateModel>) {
    const state = ctx.getState();
    const toast: Toast = { message: 'Close Toast', type: ToastType.Close };
    ctx.setState({
      ...state,
      toastMessage: toast
    });
    ctx.setState({ ...state, toastMessage: null });
  }

  //#region Download Items

  @Action(ShowDownloadsMessage)
  showDownloadsMessage(ctx: StateContext<AppStateModel>, { totalDownloads }: ShowDownloadsMessage) {
    const state = ctx.getState();
    ctx.setState({
      ...state,
      totalDownloads: totalDownloads
    });
    ctx.setState({ ...state, totalDownloads: null });
  }


  @Action(ShowDownloadMessage)
  showDownloadMessage(ctx: StateContext<AppStateModel>, { message }: ShowDownloadMessage) {
    const state = ctx.getState();
    const toast: Toast = { message: message, type: ToastType.Info, title: 'Download Information!', timeOut: 0 };
    ctx.setState({
      ...state,
      toastMessage: toast
    });
  }

  @Action(HideToast)
  hideToast(ctx: StateContext<AppStateModel>) {
    const state = ctx.getState();
    const toast: Toast = { message: '', action: ToastAction.close };
    ctx.setState({
      ...state,
      toastMessage: toast
    })
  }

  @Action(SetDownloadStatus)
  setDownloadStatus(ctx: StateContext<AppStateModel>, { inCompleteDownloads, completeDownloads }: SetDownloadStatus) {
    const state = ctx.getState();
    ctx.setState({
      ...state,
      downloadStatus: { inComplete: inCompleteDownloads, complete: completeDownloads }
    });
    ctx.setState({ ...state, downloadStatus: null });
  }

  //#endregion

  @Action(DeviceWidth)
  deviceWidth({ getState, setState }: StateContext<AppStateModel>, { deviceWidth }: DeviceWidth) {
    const state = getState();
    setState({
      ...state,
      deviceWidth: deviceWidth
    });
  }

  //#region Breadcrumb

  @Action(SetBreadcrumbs)
  setBreadcrumbs(ctx: StateContext<AppStateModel>, { breadcrumbs }: SetBreadcrumbs) {
    const state = ctx.getState();
    ctx.setState({
      ...state,
      breadcrumbs: breadcrumbs
    });
  }

  @Action(UpdateBreadcrumb)
  updateBreadcrumb(ctx: StateContext<AppStateModel>, { breadcrumb }: UpdateBreadcrumb) {
    const state = ctx.getState();
    let breadcrumbs = state.breadcrumbs.filter(x => !x.isFinal);
    breadcrumbs = [
      ...breadcrumbs,
      breadcrumb
    ];
    ctx.setState({
      ...state,
      breadcrumbs: breadcrumbs
    });
  }

  @Action(ClearBreadcrumbs)
  clearBreadcrumbs(ctx: StateContext<AppStateModel>) {
    const state = ctx.getState();
    ctx.setState({
      ...state,
      breadcrumbs: []
    });
  }

  //#endregion

  //#region Work Planning

  @Action(GetWorkPlanningParentItems)
  getWorkPlanningParentItems(ctx: StateContext<AppStateModel>) {
    return this.workPlanningService.getParentItems().toPromise()
      .then(items => {
        const state = ctx.getState();
        ctx.setState({
          ...state,
          workPlanningParentItems: items
        });
      });
  }

  //#endregion

  //#endregion
}
