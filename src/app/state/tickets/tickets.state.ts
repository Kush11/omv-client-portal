import { Ticket } from 'src/app/core/models/entity/tickets';
import { State, Selector, Action, StateContext } from '@ngxs/store';
import { CreateReportIssue } from './tickets.actions';
import { ShowSuccessMessage, HideSpinner, ShowErrorMessage } from '../app.actions';
import { tap } from 'rxjs/operators';
import { TicketsService } from 'src/app/core/services/business/tickets/tickets.service';

export class ReportIssueStateModel {
    reportIssues: Ticket[];
    reportIssue: Ticket;
    currentReportIssueId: number
}

@State<ReportIssueStateModel>({
    name: 'report_issue',
    defaults: {
        reportIssues: [],
        currentReportIssueId: null,
        reportIssue: null
    }
})

export class ReportIssueState {

    @Selector()
    static getReportIssues(state: ReportIssueStateModel) {
        return state.reportIssues;
    }
    @Selector()
    static getReportIssue(state: ReportIssueStateModel) {
        return state.reportIssue;
    }

    @Selector()
    static getcurrentReportIssueId(state: ReportIssueStateModel) {
        return state.currentReportIssueId;
    }

    constructor(private TicketsService: TicketsService) { }

    @Action(CreateReportIssue)
    createReportIssue(ctx: StateContext<ReportIssueStateModel>, { payload, files }: CreateReportIssue) {
        return this.TicketsService.createReportIssue(payload, files)
            .pipe(
                tap(response => {
                    const state = ctx.getState();
                    ctx.dispatch(new ShowSuccessMessage('Report Issue was successfully sent.'));
                    ctx.dispatch(new HideSpinner());
                }, (err) => {
                    ctx.dispatch(new HideSpinner());
                })
            );
    }

}
