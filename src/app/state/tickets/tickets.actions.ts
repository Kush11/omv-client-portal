import { Ticket } from 'src/app/core/models/entity/tickets';
import { Document } from 'src/app/core/models/entity/document';

export class CreateReportIssue {
    static readonly type = '[App] CreateReportIssue';

    constructor(public payload: Ticket, public files?: Document[]) { }
}