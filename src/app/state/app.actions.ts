import { UserWidget } from 'src/app/core/models/entity/widget';
import { User } from 'src/app/core/models/entity/user';
import { ToastType, ToastIcon } from '../core/enum/toast';
import { Breadcrumb } from '../core/models/breadcrumb';
import { SASTokenType, SASTokenModule } from '../core/enum/azure-sas-token';
export class ShowLeftNav {
  static readonly type = '[App] ShowLeftNav';

  constructor(public payload: boolean) { }
}

export enum messageType {
  success = "success",
  warning = "warning",
  error = "error",
}

export class SetPreviousRoute {
  static readonly type = '[App] SetPreviousRoute';

  constructor(public route: string) { }
}

export class SetPageTitle {
  static readonly type = '[App] SetPageTitle';

  constructor(public payload: string) { }
}

export class ShowSpinner {
  static readonly type = '[App] ShowSpinner';
}

export class HideSpinner {
  static readonly type = '[App] HideSpinner';
}

export class SaveCompanyLogo {
  static readonly type = '[App] SaveCompanyLogo';

  constructor(public logo: string) { }
}

export class GetAzureUploadConfiguration {
  static readonly type = '[App] GetAzureUploadConfiguration';
}
export class GetMediaWriteSASToken {
  static readonly type = '[App] GetMediaWriteSASToken';
}

export class GetAzureSASToken {
  static readonly type = '[App] GetAzureSASToken';

  constructor(public type = SASTokenType.Read, public module = SASTokenModule.Media) { }
}

export class GetLoggedInUser {
  static readonly type = '[App] GetLoggedInUser';
}

export class RemoveLoggedInUser {
  static readonly type = '[App] RemoveLoggedInUser';
}

export class AuthenticateUser {
  static readonly type = '[App] AuthenticateUser';
}

export class LogOut {
  static readonly type = '[App] LogOut';
}

export class GetUserPermissions {
  static readonly type = '[App] GetUserPermissions';

  constructor(public userId: number) { }
}

export class ClearNotification {
  static readonly type = '[Notification] ClearNotification';
}

export class SetNotification {

  static readonly type = '[Notification] SetNotification';

  constructor(public message: string | null, public messageType?: messageType) { }
}

export class Confirmation {
  static readonly type = '[Confirmation] Confirmation';
}

export class ClearConfirmation {
  static readonly type = '[ClearConfirmation] Confirmation';
}

export class ShowConfirmationBox {
  static readonly type = '[ShowConfirmationBox] showConfirmationBox';

  constructor(public show: boolean) { }
}

export class ShowSuccessMessage {
  static readonly type = '[App] ShowSuccessMessage';

  constructor(public message: string) { }
}

export class ShowInfoMessage {
  static readonly type = '[App] ShowInfoMessage';

  constructor(public message: string, public timeOut = 5000, public icon?: ToastIcon) { }
}

export class ShowWarningMessage {
  static readonly type = '[App] ShowWarningMessage';

  constructor(public message: string, public title = 'Warning!', public timeOut = 5000) { }
}

export class ShowErrorMessage {
  static readonly type = '[App] ShowErrorMessage';

  constructor(public payload: any) { }
}

export class CloseToast {
  static readonly type = '[App] CloseToast';
}

export class ShowFullScreen {
  static readonly type = '[App] ShowFullScreen';
}

export class HideFullScreen {
  static readonly type = '[App] HideFullScreen';
}


//#region Dashboard Section
export class GetUserDashboardWidgets {
  static readonly type = '[App] GetUserDashboardWidgets';
}


export class UpdateUserDashboardWidgets {
  static readonly type = '[App] UpdateUserDashboardWidgets';

  constructor(public panels: UserWidget[]) { }
}

export class GetAllWidgets {
  static readonly type = '[App] GetAllWidgets';
}

//#endregion

//#region Download Items

export class ShowDownloadsMessage {
  static readonly type = '[App] ShowDownloadsMessage';

  constructor(public totalDownloads: number) { }
}

export class ShowDownloadMessage {
  static readonly type = '[App] ShowDownloadMessage';

  constructor(public message: string) { }
}

export class HideToast {
  static readonly type = '[App] HideToast';
}

export class SetDownloadStatus {
  static readonly type = '[App] SetDownloadStatus';

  constructor(public inCompleteDownloads: number, public completeDownloads: number) { }
}

//#endregion

export class DeviceWidth {
  static readonly type = '[App] DeviceWidth';

  constructor(public deviceWidth: number) { }
}

//#region BreadCrumb

export class SetBreadcrumbs {
  static readonly type = '[App] SetBreadcrumbs';

  constructor(public breadcrumbs: Breadcrumb[]) { }
}

export class UpdateBreadcrumb {
  static readonly type = '[App] UpdateBreadcrumb';

  constructor(public breadcrumb: Breadcrumb) { }
}

export class ClearBreadcrumbs {
  static readonly type = '[App] ClearBreadcrumbs';
}

//#endregion

//#region WorkPlanning

export class GetWorkPlanningParentItems {
  static readonly type = '[Media] GetWorkPlanningParentItems';
}

//#endregion
