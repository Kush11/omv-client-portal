import { AppState } from './state/app.state';
import { AfterViewInit, Component, ViewChild } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { Title } from "@angular/platform-browser";
import { DeviceWidth } from "./state/app.actions";
import { Toast, ToastType, ToastIcon } from './core/enum/toast';
import { createSpinner, hideSpinner, showSpinner } from '@syncfusion/ej2-angular-popups';
import { User } from './core/models/entity/user';
import { SubSink } from 'subsink/dist/subsink';
import { ToastComponent, ToastAnimationSettingsModel } from '@syncfusion/ej2-angular-notifications';
import { Observable } from 'rxjs/internal/Observable';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements AfterViewInit {

  private subs = new SubSink();
  displayWidth: number;
  browser = window.navigator.userAgent;
  isAuthenticated: boolean;
  isAuthorized: boolean;
  showLeftNav: boolean;
  currentUser: User;
  downloadMessage: string;
  inCompleteDownloads: number;
  completeDownloads: number;
  totalDownloads: number;
  downloadStatus: any;

  @ViewChild('toast') toastObj: ToastComponent;
  @ViewChild('downloadToast') downloadToast: ToastComponent;
  toastPosition = { X: 'Right', Y: 'Top' };
  downloadToastPosition = { X: 'Right', Y: 'Bottom' };
  toastAnimation: ToastAnimationSettingsModel;

  @Select(AppState.getSpinnerVisibility) showSpinner$: Observable<boolean>;
  @Select(AppState.getLeftNavVisibility) showLeftNav$: Observable<boolean>;
  @Select(AppState.getPageTitle) currentPageTitle$: Observable<string>;
  @Select(AppState.getToastMessage) toastMessage$: Observable<Toast>;
  @Select(AppState.setDeviceWidth) deviceWidth$: Observable<number>;
  @Select(AppState.getLoggedInUser) currentUser$: Observable<User>;
  @Select(AppState.getIsAuthorized) isAuthorized$: Observable<boolean>;
  @Select(AppState.getTotalDownloads) totalDownloads$: Observable<number>;
  @Select(AppState.getDownloadStatus) downloadStatus$: Observable<any>;
  @Select(AppState.getFullScreenMode) showFullscreen$: Observable<boolean>;

  constructor(private title: Title, private store: Store) {
    window.onresize = () => {
      this.store.dispatch(new DeviceWidth(window.innerWidth))
    };
  }

  async ngOnInit() {
    this.toastAnimation = {
      show: { effect: 'FadeIn', duration: 1200, easing: 'linear' },
      hide: { effect: 'FadeOut', duration: 1200, easing: 'linear' }
    };

    this.subs.add(
      this.showSpinner$
        .subscribe(showSpinner => {
          if (showSpinner) this.showSpinner();
          else this.hideSpinner();
        }),
      this.toastMessage$
        .subscribe(toastResponse => {
          if (!toastResponse) return;
          let toast: any;
          if (typeof (toastResponse.action) != 'undefined') {
            toast = { title: 'Information!', content: toastResponse.message, cssClass: 'e-toast-info', icon: 'e-info toast-icons' };
            this.toastObj.hide();
          }
          if (!toastResponse.message) return;
          if (typeof toastResponse.message !== 'string') return;
          switch (toastResponse.type) {
            case ToastType.Success:
              toast = { title: 'Success!', content: toastResponse.message, cssClass: 'e-toast-success', icon: 'e-success toast-icons' };
              this.toastObj.timeOut = 5000;
              this.toastObj.show(toast);
              break;
            case ToastType.Warning:
              if (toastResponse.title)
                toast = { title: toastResponse.title, content: toastResponse.message, cssClass: 'e-toast-warning', icon: 'e-warning toast-icons' };
              else
                toast = { title: '', content: toastResponse.message, cssClass: 'e-toast-warning', icon: 'e-warning toast-icons' };
              this.toastObj.timeOut = toastResponse.timeOut;
              this.toastObj.show(toast);
              break;
            case ToastType.Error:
              toast = { title: 'Error!', content: toastResponse.message, cssClass: 'e-toast-danger', icon: 'e-danger toast-icons' };
              this.toastObj.timeOut = 0;
              this.toastObj.show(toast);
              break;
            case ToastType.Info:
              if (toastResponse.title)
                toast = { title: toastResponse.title, content: toastResponse.message, cssClass: 'e-toast-info', icon: 'e-info toast-icons' };
              else {
                toast = { title: '', content: toastResponse.message, cssClass: 'e-toast-info', icon: toastResponse.iconType === ToastIcon.Record ? 'e-info toast-icons' : 'e-info toast-icons' };
              }
              this.toastObj.timeOut = toastResponse.timeOut;
              this.toastObj.show(toast);
              break;
            case ToastType.Close:
              this.toastObj.hide('All');
              break;
          }
        }),
      this.totalDownloads$
        .subscribe(total => {
          if (total) {
            this.downloadStatus = { inComplete: total, complete: 0 };
            this.completeDownloads = 0;
            this.inCompleteDownloads = total;
            // const template = { template: message };
            this.downloadToast.show();
          }
        }),
      this.downloadStatus$
        .subscribe(status => {
          if (status) {
            this.completeDownloads = status.complete;
            this.inCompleteDownloads = status.inComplete;
            if (status.inComplete === 0) {
              console.log("AppComponent time to hide toast: ", status);
              setTimeout(() => {
                this.downloadToast.hide();
              }, 3000);
            }
          }
        }),
      this.deviceWidth$
        .subscribe(width => this.displayWidth = width),
      this.currentPageTitle$
        .subscribe((res) => res === 'Oceaneering Media Vault' ? this.title.setTitle(res) : this.title.setTitle(res + ' | Oceaneering Media Vault'))
    );
  }

  ngAfterViewInit(): void {

  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

  showSpinner() {
    createSpinner({
      // Specify the target for the spinner to show
      target: document.getElementById('spinnerContainer')
    });
    showSpinner(document.getElementById('spinnerContainer'));
    // setSpinner({ type: 'Bootstrap' });
  }

  hideSpinner() {
    createSpinner({
      // Specify the target for the spinner to show
      target: document.getElementById('spinnerContainer')
    });
    hideSpinner(document.getElementById('spinnerContainer'));
  }

}
