import { MediaDirectoriesComponent } from './media-directories/media-directories.component';
import { MediaItemHistoryComponent } from './media-item/media-item-history/media-item-history.component';
import { MediaItemRelatedFilesComponent } from './media-item/media-item-related-files/media-item-related-files.component';
import { MediaItemDetailsComponent } from './media-item/media-item-details/media-item-details.component';
import { MediaItemComponent } from './media-item/media-item.component';
import { MediaUploadComponent } from './media-upload/media-upload.component';
import { MediaFavoritesComponent } from './media-favorites/media-favorites.component';
import { MediaComponent } from './media.component';
import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { MediaStreamingArchiveComponent } from './media-streaming-archive/media-streaming-archive.component';
import { MediaAllComponent } from './media-all/media-all.component';
import { MediaPlaylistComponent } from './media-playlist/media-playlist.component';
import { MediaViewerComponent } from './media-viewer/media-viewer.component';
import { MediaSyncModeComponent } from './media-sync-mode/media-sync-mode.component';
import { MediaTelemetryComponent } from './media-telemetry/media-telemetry.component';
import { AuthGuard } from '../core/guards/auth-guard.service';

const mediaRoutes: Routes = [
  {
    path: '',
    component: MediaComponent,
    children: [
      { path: '', redirectTo: 'all', pathMatch: 'full' },
      { path: 'all', component: MediaAllComponent, canActivate: [AuthGuard] },
      { path: 'favorites', component: MediaFavoritesComponent, canActivate: [AuthGuard] },
      { path: 'streaming-archive', component: MediaStreamingArchiveComponent, canActivate: [AuthGuard] },
      { path: 'directories', component: MediaDirectoriesComponent, canActivate: [AuthGuard] }

    ],
  },
  {
    path: 'playlist',
    component: MediaPlaylistComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'sync-mode',
    component: MediaSyncModeComponent,
    canActivate: [AuthGuard]
  },
  {
    path: ':id/telemetry',
    component: MediaTelemetryComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'upload',
    component: MediaUploadComponent,
    canActivate: [AuthGuard]
  },
  {
    path: ':id',
    component: MediaItemComponent,
    canActivate: [AuthGuard],
    children: [
      { path: '', redirectTo: 'details', pathMatch: 'full' },
      { path: 'details', component: MediaItemDetailsComponent, canActivate: [AuthGuard] },
      { path: 'related-items', component: MediaItemRelatedFilesComponent, canActivate: [AuthGuard] },
      { path: 'history', component: MediaItemHistoryComponent, canActivate: [AuthGuard] },
    ]
  },
  {
    path: ':id/viewer',
    component: MediaViewerComponent, 
    canActivate: [AuthGuard]
  },
];

@NgModule({
  imports: [
    RouterModule.forChild(mediaRoutes)
  ],
  exports: [RouterModule]
})
export class MediaRoutingModule { }
