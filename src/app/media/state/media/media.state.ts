import { MediaUploadService } from './../../media-upload/media-upload.service';
import {
  GetHistory, GetFavorites, GetDirectoryMetadata, SetCurrentMediaItemId, GetMediaItem, UpdateMediaItem, CreateMediaItem,
  ClearMediaItemMetadata, ResetUploadStatus, GetTreeViewMedia, ClearDirectoryMetadata, AddFilterTag,
  RemoveFilterTag, ClearFilterTags, ShowFilters, HideFilters, AddFavorite, RemoveFavorite, ResetFavoriteActions, AddSelectedMediaItem,
  RemoveSelectedMediaItem, ClearSelectedMediaItems, SetSavedFilterAction,
  GetRelatedFiles, AddRelatedFiles, DeleteRelatedFiles, SetBreadcrumbs, UpdateBreadcrumb,
  ClearCurrentMediaItem, GetMapViewMedia, SetPlaylist, ClearPlaylist, SetSelectedMediaItems, SetCurrentVideoTime,
  ClearCurrentVideoTime, GetTelemetryItems, ClearTelemetryItems, DownloadMediaItems, DownloadMediaItem, ClearTelemetryEvent, GetDailyVideos,
  AddSelectedMediaItems, RemoveSelectedMediaItems, GetStreamingArchiveAssets, GetStreamingArchiveGroups, GetHourlyVideos, SetArchiveMode,
  ClearArchiveMode, ClearDailyVideos, GetAvailableArchiveDates, AddFilterTags, ResetAllFilters, ResetFilter,
  RefetchMedia, SetFilterTags, UpdateMetadata, GetDirectories, DeleteMediaItem, SetMapCoordinates, ShowIsFetching, ClearHistory,
  HideIsFetching, ShowIsFilterResult, HideIsFilterResult, GetMapCoordinates, GetMedia, SaveExpandedDirectories, SaveSelectedMedia, ShowNewFolder
} from './media.action';
import { tap, map } from 'rxjs/operators';
import { MediaService } from '../../../core/services/business/media/media.service';
import { MediaItem, Media } from '../../../core/models/entity/media';
import { Action, State, StateContext, Selector, Store } from '@ngxs/store';
import { MediaTreeGrid } from 'src/app/core/models/media-tree-grid';
import { FieldConfiguration } from 'src/app/shared/dynamic-components/field-setting';
import {
  HideSpinner, ShowErrorMessage, ShowSpinner, ShowSuccessMessage, ShowDownloadsMessage,
  SetDownloadStatus, ShowDownloadMessage, HideToast
} from 'src/app/state/app.actions';
import { Tag } from 'src/app/core/models/entity/tag';
import { RelatedFiles } from 'src/app/core/models/entity/related-files';
import { DirectoryService } from 'src/app/core/services/business/directory/directory.service';
import { Breadcrumb } from 'src/app/core/models/breadcrumb';
import { MediaDataService } from 'src/app/core/services/data/media/media.data.service';
import { Telemetry } from 'src/app/core/models/entity/telemetry';
import { LookupService } from 'src/app/core/services/business/lookup/lookup.service';
import { Lookup } from 'src/app/core/models/entity/lookup';
import { StreamingArchive } from 'src/app/core/models/entity/streaming-archive';
import { FieldsService } from 'src/app/core/services/business/fields/fields.service';
import { SortDirection, SortType } from 'src/app/core/enum/sort-enum';
import { ArchiveMode } from 'src/app/core/enum/archive-mode';
import { Filter, FilterType } from 'src/app/core/models/entity/filter';
import { CustomersDataService } from 'src/app/core/services/data/customers/customers.data.service';
import { AppState } from 'src/app/state/app.state';
import { MetadataDataType } from 'src/app/core/enum/metadata-data-type';
import { Search_MapFilterDTO } from 'src/app/core/dtos/input/documents/Document_SearchInputDTO';
import { Directory } from 'src/app/core/models/entity/directory';

export class MediaStateModel {
  // breadcrumbs
  breadcrumbs: Breadcrumb[];
  azureReadSASToken: string;
  azureWriteSASToken: string;
  media: MediaItem[];
  treeviewMedia: MediaItem[];
  currentMediaItemId: any;
  currentMediaItem: MediaItem;
  relatedItems: MediaItem[]; // related items
  metadata: FieldConfiguration[];
  itemFields: FieldConfiguration[];
  directories: any[];
  historyItems: any[];
  totalMedia: number;
  mediaTreeData: MediaTreeGrid[];
  currentItemMetadata: any[];
  directoryMetadata: any[];
  documents: any[];
  uploadComplete: boolean;
  pageSize: number;
  mapCoordinates: Search_MapFilterDTO[];
  expandedDirectories: Directory[];
  selectedMedia: any;

  // filters
  filters: Filter[];
  filterTags: Tag[];
  showFilterMenu: boolean;
  isFilterApplied: boolean;
  isFilterCleared: boolean;
  isSavedFilterApplied: boolean;
  archiveFilterTags: Tag[];
  showArchiveFilterMenu: boolean;
  isArchiveFilterApplied: boolean;
  isArchiveFilterCleared: boolean;
  isSavedArchiveFilterApplied: boolean;
  streamingArchiveAssets: Lookup[];
  streamingArchiveGroups: Lookup[];
  searchQuery: string;
  isMapView: boolean;
  isFetching: boolean;
  isFilterResult: boolean;
  isNewFolder: boolean;

  // relatedFiles
  relatedFiles: RelatedFiles[];

  // favorites
  favoriteActionSuccess: boolean;
  favorites: MediaItem[];

  // selected items
  selectedItems: MediaItem[];
  isMediaSelectionCleared: boolean;

  // selected video items
  playlist: MediaItem[];
  selectedVideos: MediaItem[];
  selectedVideoItems: MediaItem[];
  isMediaSlectedVideoItemsCleared: boolean;

  // telementary Items
  telemetryItems: Telemetry;
  selectedTelemetryFeeds: MediaItem[];
  selectedTelementaryEvents: {};
  currentVideoTime: number;

  // Streaming Archive
  isStreamingArchive: boolean;
  dailyVideos: StreamingArchive;
  hourlyVideos: StreamingArchive;
  streamingArchiveMode: ArchiveMode;
  streamingArchiveDays: string[];

  //paging
  isItermPerPageSelected: boolean;
}

@State<MediaStateModel>({
  name: 'media',
  defaults: {
    // breadcrumbs
    breadcrumbs: [],
    expandedDirectories: [],
    azureReadSASToken: '',
    azureWriteSASToken: '',
    media: [],
    treeviewMedia: [],
    currentMediaItemId: null,
    currentMediaItem: null,
    relatedItems: [],
    isNewFolder: false,
    directories: [],
    favorites: [],
    historyItems: [],
    totalMedia: 0,
    mediaTreeData: [],
    metadata: [],
    currentItemMetadata: [],
    itemFields: [],
    documents: [],
    directoryMetadata: [],
    uploadComplete: false,
    pageSize: null,
    selectedMedia: null,
    mapCoordinates: [
      { latitude: 90, longitude: -180 },
      { latitude: -90, longitude: -180 },
      { latitude: -90, longitude: 180 },
      { latitude: 90, longitude: 180 }
    ],

    // filters state
    filters: [],
    filterTags: [],
    showFilterMenu: false,
    isFilterApplied: false,
    isFilterCleared: false,
    isSavedFilterApplied: false,
    archiveFilterTags: [],
    showArchiveFilterMenu: false,
    isArchiveFilterApplied: false,
    isArchiveFilterCleared: false,
    isSavedArchiveFilterApplied: false,
    streamingArchiveAssets: [],
    streamingArchiveGroups: [],
    searchQuery: '',
    isMapView: false,
    isFetching: false,
    isFilterResult: false,

    // relatedFiles
    relatedFiles: [],

    // favorites
    favoriteActionSuccess: false,

    // selected items
    selectedItems: [],
    isMediaSelectionCleared: false,

    // selected video items
    playlist: [],
    selectedVideos: [],
    selectedVideoItems: [],
    isMediaSlectedVideoItemsCleared: false,

    // tementary items
    telemetryItems: null,
    selectedTelemetryFeeds: [],
    selectedTelementaryEvents: null,
    currentVideoTime: 0,

    // streaming archives
    isStreamingArchive: false,
    dailyVideos: null,
    hourlyVideos: null,
    streamingArchiveMode: null,
    streamingArchiveDays: [],

    //paging
    isItermPerPageSelected: false

  }
})

export class MediaState {

  //#region S E L E C T O R S

  @Selector()
  static getAzureReadSASToken(state: MediaStateModel) {
    return state.azureReadSASToken;
  }

  @Selector()
  static getAzureWriteSASToken(state: MediaStateModel) {
    return state.azureWriteSASToken;
  }

  @Selector()
  static getBreadcrumbs(state: MediaStateModel) {
    return state.breadcrumbs;
  }

  @Selector()
  static getCurrentItemId(state: MediaStateModel) {
    return state.currentMediaItemId;
  }

  @Selector()
  static getHistory(state: MediaStateModel) {
    return state.historyItems;
  }

  @Selector()
  static getMedia(state: MediaStateModel) {
    return state.media;
  }

  @Selector()
  static getTreeViewMedia(state: MediaStateModel) {
    return state.treeviewMedia;
  }
  @Selector()
  static getMapViewMedia(state: MediaStateModel) {
    return state.treeviewMedia;
  }

  @Selector()
  static getCurrentMediaItem(state: MediaStateModel) {
    return state.currentMediaItem;
  }

  @Selector()
  static getUploadCompleteStatus(state: MediaStateModel) {
    return state.uploadComplete;
  }

  @Selector()
  static getTotalMedia(state: MediaStateModel) {
    return state.totalMedia;
  }

  @Selector()
  static getIsFetchingVisibility(state: MediaStateModel) {
    return state.isFetching;
  }

  @Selector()
  static getIsNewFolder(state: MediaStateModel) {
    return state.isNewFolder;
  }

  @Selector()
  static getFilterResultVisibility(state: MediaStateModel) {
    return state.isFilterResult;
  }

  @Selector()
  static getCurrentMediaItemId(state: MediaStateModel) {
    return state.currentMediaItemId;
  }

  @Selector()
  static getMetaData(state: MediaStateModel) {
    return state.metadata;
  }

  @Selector()
  static getMediaTreeData(state: MediaStateModel) {
    return state.mediaTreeData;
  }

  @Selector()
  static getCurrentItemMetadata(state: MediaStateModel) {
    return state.currentItemMetadata.sort(x => x.order);
  }

  @Selector()
  static getItemFields(state: MediaStateModel) {
    return state.itemFields.sort(x => x.order);
  }

  @Selector()
  static getDirectories(state: MediaStateModel) {
    return state.directories;
  }

  @Selector()
  static getExpandedDirectories(state: MediaStateModel) {
    return state.expandedDirectories;
  }

  @Selector()
  static getSelectedMedia(state: MediaStateModel) {
    return state.selectedMedia;
  }

  @Selector()
  static getDirectoryMetadata(state: MediaStateModel) {
    return state.directoryMetadata;
  }

  @Selector()
  static getRelatedItems(state: MediaStateModel) {
    return state.relatedItems;
  }


  @Selector()
  static GetMapCoordinates(state: MediaStateModel) {
    return state.mapCoordinates;
  }


  // favorites

  @Selector()
  static getFavorites(state: MediaStateModel) {
    return state.favorites;
  }

  @Selector()
  static getFavoriteActionSuccess(state: MediaStateModel) {
    return state.favoriteActionSuccess;
  }

  //#region FILTERS

  @Selector()
  static getFilterTags(state: MediaStateModel) {
    return state.filterTags;
  }

  @Selector()
  static showFilterMenu(state: MediaStateModel) {
    return state.showFilterMenu;
  }

  @Selector()
  static isFilterApplied(state: MediaStateModel) {
    return state.isFilterApplied;
  }

  @Selector()
  static isFilterCleared(state: MediaStateModel) {
    return state.isFilterCleared;
  }

  @Selector()
  static getStreamingArchiveAssets(state: MediaStateModel) {
    return state.streamingArchiveAssets;
  }

  @Selector()
  static getStreamingArchiveGroups(state: MediaStateModel) {
    return state.streamingArchiveGroups;
  }

  @Selector()
  static getFilters(state: MediaStateModel) {
    return state.filters;
  }
  @Selector()
  static getSearchQuery(state: MediaStateModel) {
    return state.searchQuery;
  }

  //#region Streaming Archive Filters

  @Selector()
  static getArchiveFilterTags(state: MediaStateModel) {
    return state.archiveFilterTags;
  }

  @Selector()
  static showArchiveFilterMenu(state: MediaStateModel) {
    return state.showArchiveFilterMenu;
  }

  @Selector()
  static isArchiveFilterApplied(state: MediaStateModel) {
    return state.isArchiveFilterApplied;
  }

  @Selector()
  static isArchiveFilterCleared(state: MediaStateModel) {
    return state.isArchiveFilterCleared;
  }

  @Selector()
  static isSavedArchiveFilterApplied(state: MediaStateModel) {
    return state.isSavedArchiveFilterApplied;
  }

  //#endregion

  //#endregion

  @Selector()
  static getRelatedFiles(state: MediaStateModel) {
    return state.relatedFiles;
  }

  static isSavedFilterApplied(state: MediaStateModel) {
    return state.isSavedFilterApplied;
  }

  //#region Selected Items

  @Selector()
  static getSelectedItems(state: MediaStateModel) {
    return state.selectedItems;
  }

  @Selector()
  static isMediaSelectionCleared(state: MediaStateModel) {
    return state.isMediaSelectionCleared;
  }

  //#endregion

  //#region Selected  Video Items

  @Selector()
  static getSelectedVideos(state: MediaStateModel) {
    const videoFormats = ['MP4', 'MOV', 'AVI', 'MPG', 'MKV'];
    return state.selectedItems.filter(items => videoFormats.includes(items.type));
  }

  @Selector()
  static getSelectedVideoItems(state: MediaStateModel) {
    return state.selectedVideoItems;
  }

  @Selector()
  static getPlaylist(state: MediaStateModel) {
    return state.playlist;
  }

  @Selector()
  static isMediaSlectedVideoItemsCleared(state: MediaStateModel) {
    return state.isMediaSlectedVideoItemsCleared;
  }
  //#endregion

  //#region telementary Items

  @Selector()
  static getSelectedTelemetryFeeds(state: MediaStateModel) {
    return state.selectedTelemetryFeeds;
  }

  @Selector()
  static getSelectedTelemtary(state: MediaStateModel) {
    return state.selectedTelementaryEvents;
  }

  @Selector()
  static getCurrentVideoTime(state: MediaStateModel) {
    return state.currentVideoTime;
  }
  //#endregion

  //#region Streaming Archive

  @Selector()
  static isStreamingArchive(state: MediaStateModel) {
    return state.isStreamingArchive;
  }

  @Selector()
  static getStreamingArchiveMode(state: MediaStateModel) {
    return state.streamingArchiveMode;
  }

  @Selector()
  static getDailyVideos(state: MediaStateModel) {
    return state.dailyVideos;
  }

  @Selector()
  static getHourlyVideos(state: MediaStateModel) {
    return state.hourlyVideos;
  }

  @Selector()
  static getStreamingArchiveDays(state: MediaStateModel) {
    return state.streamingArchiveDays;
  }

  //#endregion

  //#endregion

  constructor(private store: Store, private mediaService: MediaService, private mediaDataService: MediaDataService, private mediaUploadService: MediaUploadService,
    private foldersService: DirectoryService,
    private lookupService: LookupService, private fieldsService: FieldsService) { }

  //#region A C T I O N S

  //#region Breadcrumb

  @Action(SetBreadcrumbs)
  setBreadcrumbs(ctx: StateContext<MediaStateModel>, { breadcrumbs }: SetBreadcrumbs) {
    const state = ctx.getState();
    ctx.setState({
      ...state,
      breadcrumbs
    });
  }

  @Action(UpdateBreadcrumb)
  updateBreadcrumb(ctx: StateContext<MediaStateModel>, { breadcrumb }: UpdateBreadcrumb) {
    const state = ctx.getState();
    const breadcrumbs = state.breadcrumbs.filter(x => !x.isFinal);
    breadcrumbs.push(breadcrumb);
    ctx.setState({
      ...state,
      breadcrumbs
    });
  }

  //#endregion

  @Action(ShowIsFetching)
  showIsFetching({ getState, setState }: StateContext<MediaStateModel>) {
    const state = getState();
    setState({
      ...state,
      isFetching: true
    });
  }

  @Action(ShowNewFolder)
  showNewFolder({ getState, setState }: StateContext<MediaStateModel>) {
    const state = getState();
    setState({
      ...state,
      isNewFolder: true
    });
  }

  @Action(HideIsFetching)
  hideIsFetching({ getState, setState }: StateContext<MediaStateModel>) {
    const state = getState();
    setState({
      ...state,
      isFetching: false
    });
  }

  @Action(ShowIsFilterResult)
  showIsFilterResult({ getState, setState }: StateContext<MediaStateModel>) {
    const state = getState();
    setState({
      ...state,
      isFilterResult: true
    });
  }

  @Action(HideIsFilterResult)
  hideIsFilterResult({ getState, setState }: StateContext<MediaStateModel>) {
    const state = getState();
    setState({
      ...state,
      isFilterResult: false
    });
  }

  @Action(GetMedia)
  getMedia(ctx: StateContext<MediaStateModel>, { filters, pageNumber, pageSize }: GetMedia) {
    ctx.dispatch(new HideIsFilterResult());
    ctx.dispatch(new ShowIsFetching());
    const state = ctx.getState();
    if (!state.isMapView) {
      ctx.dispatch(new ShowSpinner());
    }
    const queryFilterName = 'queryFilter';
    const queryFilter = filters.find(t => t.name === queryFilterName);
    filters = filters.filter(t => t.name != queryFilterName);
    const query = queryFilter ? queryFilter.value : '';
    const currentPageSize = pageSize ? pageSize : state.pageSize;
    return this.mediaService.getMedia(query, filters, pageNumber, currentPageSize, state.isMapView, state.mapCoordinates)
      .pipe(
        tap((response: Media) => {
          if (!response) { return; }
          const media = response.data;
          const filters = response.filters;
          const state = ctx.getState();
          const filterTags = state.filterTags;
          const selectedItemIds = state.selectedItems.map(x => x.documentId);
          const sasToken = this.store.selectSnapshot(AppState.getMediaReadSASToken);
          media.forEach(m => {
            m.thumbnail += sasToken;
            m.url += sasToken;
            if (selectedItemIds.includes(m.documentId)) { m.isChecked = true; }
          });
          filters.forEach(filter => {
            if (filter.searchType === FilterType.checkbox) {
              if (filter.options) {
                filter.options.forEach(option => {
                  if (typeof option.label === 'string') {
                    option.label = option.label.toString().trim();
                  }
                  const tag = filterTags.find(t => t.name === filter.name && t.value === option.label);
                  if (tag) {
                    option.isChecked = true;
                  }
                });
              }
            } else if (filter.searchType === FilterType.date) {
              const tag = filterTags.find(t => t.name === filter.name);
              if (tag) {
                filter.value = tag.rangeValue;
              }
            } else if (filter.searchType === FilterType.range) {
              if (filter.options) {
                if (filter.dataType === MetadataDataType.Date) {
                  const min = filter.options[0] ? new Date(filter.options[0].label).getTime() : new Date(null).getTime();
                  const max = filter.options[1] ? new Date(filter.options[1].label).getTime() : new Date().getTime();
                  filter.min = min < max ? min : max;
                  filter.max = min < max ? max : min;
                  filter.values = [min, max];
                  filter.value = `${min} - ${max}`;
                } else {
                  const min = filter.options[0] ? filter.options[0].label : 0;
                  const max = filter.options[1] ? filter.options[1].label : 100;
                  filter.min = min < max ? min : max;
                  filter.max = min < max ? max : min;
                  filter.values = [min, max];
                  filter.value = `${min} - ${max}`;
                }
              }
            }
          });
          ctx.setState({
            ...state,
            media: media,
            filters: filters,
            totalMedia: response.pagination.total
          });
          ctx.dispatch(new HideSpinner());
        }, error => {
          ctx.setState({
            ...state,
            media: [],
            treeviewMedia: [],
            filters: [],
            totalMedia: 0
          });
          ctx.dispatch(new ShowErrorMessage(error));
        })
      );
  }

  @Action(RefetchMedia)
  refetchMedia(ctx: StateContext<MediaStateModel>, { pageNumber, pageSize, isMapView, mapCoordinates }: RefetchMedia) {
    const state = ctx.getState();
    const filterTags = [...state.filterTags];
    ctx.setState({
      ...state,
      pageSize,
      isMapView
    });
    ctx.dispatch(new GetMedia(filterTags, pageNumber, pageSize, isMapView, mapCoordinates));
  }

  @Action(GetDirectories)
  getFolders(ctx: StateContext<MediaStateModel>) {
    return this.foldersService.getFolders()
      .pipe(
        tap(directories => {
          const state = ctx.getState();
          ctx.setState({
            ...state,
            directories: directories
          });
        }, error => ctx.dispatch(new ShowErrorMessage(error)))
      );
  }

  @Action(SaveExpandedDirectories)
  saveExpandedDirectories(ctx: StateContext<MediaStateModel>, { directories }: SaveExpandedDirectories) {
    const state = ctx.getState();
    ctx.setState({
      ...state,
      expandedDirectories: directories
    });
  }

  @Action(SaveSelectedMedia)
  SaveSelectedMedia(ctx: StateContext<MediaStateModel>, { selectedMediaId }: SaveSelectedMedia) {
    const state = ctx.getState();
    ctx.setState({
      ...state,
      selectedMedia: selectedMediaId
    });
  }

  //#region Media Item

  @Action(SetMapCoordinates)
  setMapCoordinates({ getState, setState }: StateContext<MediaStateModel>, { mapCoordinates }: SetMapCoordinates) {
    const state = getState();
    return setState({
      ...state,
      mapCoordinates
    });
  }

  @Action(GetMediaItem)
  getMediaItem(ctx: StateContext<MediaStateModel>, { id }: GetMediaItem) {
    return this.mediaService.getMediaItem(id).pipe(
      tap(item => {
        const state = ctx.getState();
        const sasToken = this.store.selectSnapshot(AppState.getMediaReadSASToken);
        item.thumbnails.forEach(thumbnail => {
          thumbnail.src += sasToken;
        });
        item.thumbnail += sasToken;
        item.url += sasToken;
        ctx.setState({
          ...state,
          currentMediaItem: item
        });
      }, error => ctx.dispatch(new ShowErrorMessage(error)))
    );
  }

  @Action(ClearCurrentMediaItem)
  clearCurrentMediaItem(ctx: StateContext<MediaStateModel>) {
    const state = ctx.getState();
    ctx.setState({
      ...state,
      currentMediaItem: null
    });
  }

  @Action(ClearMediaItemMetadata)
  clearItemMetadata(ctx: StateContext<MediaStateModel>) {
    const state = ctx.getState();
    ctx.setState({
      ...state,
      currentItemMetadata: [],
      itemFields: []
    });
  }

  @Action(CreateMediaItem)
  createItem(ctx: StateContext<MediaStateModel>, { payload }: CreateMediaItem) {
    return this.mediaService.createMediaItem(payload)
      .pipe(
        tap(() => {
          const state = ctx.getState();
          if (payload.documentTypeCode === 'MP4' || payload.documentTypeCode === 'MOV' || payload.documentTypeCode === 'AVI'
            || payload.documentTypeCode === 'MKV') {
            this.store.dispatch(
              new ShowSuccessMessage('Your media item has been submitted for offline processing and will be available shortly.')
            );
          } else {
            ctx.dispatch(new ShowSuccessMessage(`Media Item successfully uploaded!`));
          }
          ctx.setState({
            ...state,
            uploadComplete: true
          });
          ctx.dispatch(new HideSpinner());
        }, error => ctx.dispatch(new ShowErrorMessage(error)))
      );
  }

  @Action(UpdateMediaItem)
  updateItem(ctx: StateContext<MediaStateModel>, { id, payload }: UpdateMediaItem) {
    ctx.dispatch(new ShowSpinner());
    return this.mediaService.updateMediaItem(id, payload).pipe(
      tap(() => {
        ctx.dispatch(new HideSpinner());
        ctx.dispatch(new ShowSuccessMessage(`Details updated successfully.`));
      }, error => ctx.dispatch(new ShowErrorMessage(error)))
    );
  }

  @Action(DeleteMediaItem)
  DeleteMediaItem(ctx: StateContext<MediaStateModel>, { id }: DeleteMediaItem) {
    ctx.dispatch(new ShowSpinner());
    return this.mediaService.deleteMediaItem(id)
      .pipe(
        tap(() => {
          ctx.dispatch(new HideSpinner());
          ctx.dispatch(new ShowSuccessMessage(`Media Item was deleted successfully.`));
        }, error => ctx.dispatch(new ShowErrorMessage(error)))
      );
  }

  @Action(UpdateMetadata)
  updateMetadata(ctx: StateContext<MediaStateModel>, { id, metadata }: UpdateMetadata) {
    ctx.dispatch(new ShowSpinner());
    return this.mediaService.updateMetadata(id, metadata)
      .pipe(
        tap(() => {
          ctx.dispatch(new HideSpinner());
          ctx.dispatch(new ShowSuccessMessage(`Metadata was updated successfully.`));
        }, error => {
          console.log("error: ", error);
          ctx.dispatch(new ShowErrorMessage(error))
        })
      );
  }

  @Action(SetCurrentMediaItemId)
  setCurrentMediaItemId({ getState, setState }: StateContext<MediaStateModel>, { id }: SetCurrentMediaItemId) {
    const state = getState();
    setState({
      ...state,
      currentMediaItemId: id
    });
  }

  @Action(GetRelatedFiles)
  getRelatedFiles(ctx: StateContext<MediaStateModel>, { id }: GetRelatedFiles) {
    ctx.dispatch(new ShowSpinner());
    return this.mediaService.getRelatedItems(id)
      .pipe(
        tap(items => {
          const state = ctx.getState();
          ctx.setState({
            ...state,
            relatedItems: items
          });
          ctx.dispatch(new HideSpinner());
        }, error => ctx.dispatch(new ShowErrorMessage(error)))
      );
  }

  @Action(AddRelatedFiles)
  addRelatedFiles(ctx: StateContext<MediaStateModel>, { documentId, relatedDocumentId }: AddRelatedFiles) {
    ctx.dispatch(new ShowSpinner());
    return this.mediaService.addRelatedItems(documentId, relatedDocumentId).pipe(
      tap(() => {
        ctx.dispatch(new GetRelatedFiles(documentId));
      }, error => ctx.dispatch(new ShowErrorMessage(error)))
    );
  }

  @Action(DeleteRelatedFiles)
  deleteRelatedFiles(ctx: StateContext<MediaStateModel>, { documentId, relatedDocumentId }: DeleteRelatedFiles) {
    ctx.dispatch(new ShowSpinner());
    return this.mediaService.deleteRelatedItems(documentId, relatedDocumentId).pipe(
      tap(() => {
        ctx.dispatch(new GetRelatedFiles(documentId));
      }, error => ctx.dispatch(new ShowErrorMessage(error)))
    );
  }

  @Action(DownloadMediaItems)
  downloadItems(ctx: StateContext<MediaStateModel>, { items }: DownloadMediaItems) {
    ctx.dispatch(new ShowSpinner());

    ctx.dispatch(new ShowDownloadsMessage(items.length));
    let complete = 0;
    let incomplete = items.length;
    items.forEach((item, index) => {
      this.mediaService.download(item.name, item.url)
        .then(() => {
          complete++; incomplete--;
          ctx.dispatch(new SetDownloadStatus(incomplete, complete));
          if (index === (items.length - 1)) { ctx.dispatch(new ClearSelectedMediaItems()); ctx.dispatch(new HideSpinner()); }
        }, error => ctx.dispatch(new ShowErrorMessage(error)));
    });
  }

  @Action(DownloadMediaItem)
  downloadItem(ctx: StateContext<MediaStateModel>, { name, url }: DownloadMediaItem) {
    ctx.dispatch(new ShowDownloadMessage('Item is Downloading'));
    this.mediaService.download(name, url)
      .then(() => {
        ctx.dispatch(new HideToast());
      }, error => ctx.dispatch([new HideToast(), new ShowErrorMessage(error)]));
  }

  //#endregion

  @Action(ResetUploadStatus)
  resetUploadStatus(ctx: StateContext<MediaStateModel>) {
    const state = ctx.getState();
    ctx.setState({
      ...state,
      uploadComplete: false
    });
  }

  @Action(ClearDirectoryMetadata)
  clearDirectoryMetadata(ctx: StateContext<MediaStateModel>) {
    const state = ctx.getState();
    ctx.setState({
      ...state,
      directoryMetadata: []
    });
  }

  //#region Favorites

  @Action(GetFavorites)
  getFavorites(ctx: StateContext<MediaStateModel>, { pageNumber, pageSize, query }: GetFavorites) {
    ctx.dispatch(new ShowSpinner());
    return this.mediaService.getFavorites(pageNumber, pageSize, query).pipe(
      tap(response => {
        if (!response) { return; }
        const media = response.data;
        const state = ctx.getState();
        const sasToken = this.store.selectSnapshot(AppState.getMediaReadSASToken);
        media.forEach(m => {
          m.thumbnail += sasToken;
          m.url += sasToken;
        });
        ctx.setState({
          ...state,
          favorites: media,
          totalMedia: response.pagination.total
        });
        ctx.dispatch(new HideSpinner());
      }, err => {
        ctx.dispatch(new ShowErrorMessage(err));
      })
    );
  }

  @Action(AddFavorite)
  addFavorite(ctx: StateContext<MediaStateModel>, { id, name, tags }: AddFavorite) {
    ctx.dispatch(new ShowSpinner());
    return this.mediaService.addFavorite(id, name, tags).pipe(
      tap(() => {
        const state = ctx.getState();
        ctx.setState({
          ...state,
          favoriteActionSuccess: true
        });
        ctx.dispatch(new HideSpinner());
        ctx.dispatch(new ShowSuccessMessage('Favorite media was successfully saved.'));
        ctx.dispatch(new ResetFavoriteActions());
      }, error => {
        ctx.dispatch(new ShowErrorMessage(error));
      })
    );
  }

  @Action(RemoveFavorite)
  removeFavorite(ctx: StateContext<MediaStateModel>, { favoriteItemId }: RemoveFavorite) {
    ctx.dispatch(new ShowSpinner());
    return this.mediaService.removeFavorite(favoriteItemId).pipe(
      tap(() => {
        const state = ctx.getState();
        ctx.setState({
          ...state,
          favoriteActionSuccess: true
        });
        ctx.dispatch(new HideSpinner());
        ctx.dispatch(new ResetFavoriteActions());
      }, error => ctx.dispatch(new ShowErrorMessage(error)))
    );
  }

  @Action(ResetFavoriteActions)
  resetFavoriteActions(ctx: StateContext<MediaStateModel>) {
    const state = ctx.getState();
    ctx.setState({
      ...state,
      favoriteActionSuccess: false
    });
  }

  //#endregion

  @Action(GetHistory)
  getHistory(ctx: StateContext<MediaStateModel>, { id, pageNumber, pageSize }: GetHistory) {
    ctx.dispatch(new ShowSpinner());
    return this.mediaService.getHistory(id, pageNumber, pageSize)
      .pipe(
        tap(media => {
          const state = ctx.getState();
          ctx.setState({
            ...state,
            historyItems: media.data
          });
          ctx.dispatch(new HideSpinner());
        }, error => ctx.dispatch(new ShowErrorMessage(error)))
      );
  }


  @Action(ClearHistory)
  clearHistory(ctx: StateContext<MediaStateModel>, ) {
    ctx.dispatch(new ShowSpinner());
    const state = ctx.getState();
    ctx.setState({
      ...state,
      historyItems: []
    });

  }

  @Action(GetDirectoryMetadata)
  async getDirectoryMetadata(ctx: StateContext<MediaStateModel>, { id }: GetDirectoryMetadata) {
    await this.mediaUploadService.getFolderMetadataFields(id).then(metadata => {
      const state = ctx.getState();
      ctx.setState({
        ...state,
        directoryMetadata: metadata
      });
    }, error => ctx.dispatch(new ShowErrorMessage(error)));
  }

  //#region FILTERS

  @Action(ShowFilters)
  showFilters({ getState, setState }: StateContext<MediaStateModel>, { isStreamingArchive }: ShowFilters) {
    const state = getState();
    if (isStreamingArchive) {
      setState({
        ...state,
        showArchiveFilterMenu: true
      });
    } else {
      setState({
        ...state,
        showFilterMenu: true
      });
    }
  }

  @Action(HideFilters)
  hideFilters({ getState, setState }: StateContext<MediaStateModel>, { isStreamingArchive }: HideFilters) {
    const state = getState();
    if (isStreamingArchive) {
      setState({
        ...state,
        showArchiveFilterMenu: false
      });
    } else {
      setState({
        ...state,
        showFilterMenu: false
      });
    }
  }

  @Action(AddFilterTag)
  addFilterTag(ctx: StateContext<MediaStateModel>, { tag, isStreamingArchive, isFavoriteSearch, currentPage }: AddFilterTag) {
    if (isStreamingArchive) {
      const state = ctx.getState();
      let tags = state.archiveFilterTags;
      const tagNames = tags.map(t => t.name);
      if (tagNames.includes(tag.name)) {
        tags = tags.filter(x => x.name !== tag.name);
      }
      tags = [...tags, tag];
      tags = this.fieldsService.sort(tags, 'order', SortType.Number, SortDirection.Ascending);
      ctx.setState({
        ...state,
        archiveFilterTags: tags,
        isArchiveFilterCleared: false
      });

      const assetFilterName = 'asset'; const groupFilterName = 'group';
      if (tag.name === assetFilterName) {
        const state = ctx.getState();
        const groupFilter = state.archiveFilterTags.find(tag => tag.name === groupFilterName);
        if (groupFilter) {
          ctx.dispatch(new GetAvailableArchiveDates(tag.value, groupFilter.value)).toPromise()
            .then(() => {
              const state = ctx.getState();
              const dateTag = state.archiveFilterTags.find(tag => tag.name === 'date');
              const dates = state.streamingArchiveDays;
              if (dateTag) {
                if (!dates.includes(dateTag.value)) {
                  ctx.dispatch(new RemoveFilterTag(dateTag, true));
                }
              }
            });
        }
      } else if (tag.name === groupFilterName) {
        const state = ctx.getState();
        const assetFilter = state.archiveFilterTags.find(tag => tag.name === assetFilterName);
        if (assetFilter) {
          ctx.dispatch(new GetAvailableArchiveDates(assetFilter.value, tag.value)).toPromise()
            .then(() => {
              const state = ctx.getState();
              const dateTag = state.archiveFilterTags.find(tag => tag.name === 'date');
              const dates = state.streamingArchiveDays;
              if (dateTag) {
                if (!dates.includes(dateTag.value)) {
                  ctx.dispatch(new RemoveFilterTag(dateTag, true));
                }
              }
            });
        }
      }
    } else {
      const state = ctx.getState();
      let tags = state.filterTags;
      const queryFilterName = 'queryFilter';
      if (tag.name === queryFilterName) {
        tags = tags.filter(t => t.name != queryFilterName);
        tags.unshift(tag);
      }
      ctx.setState({
        ...state,
        filterTags: tags
      });
      if (isFavoriteSearch) {
        const queryFilter = tags.find(t => t.name === queryFilterName);
        const query = queryFilter ? queryFilter.value : '';
        ctx.dispatch(new GetFavorites(currentPage, 8, query));
      } else {
        ctx.dispatch(new GetMedia(tags, 1));
      }
    }
  }

  @Action(AddFilterTags)
  addFilterTags(ctx: StateContext<MediaStateModel>, { filterName, tags }: AddFilterTags) {
    const state = ctx.getState();
    let filterTags = [...state.filterTags];
    filterTags = filterTags.filter(f => f.name != filterName);
    filterTags = [...filterTags, ...tags];
    ctx.setState({
      ...state,
      filterTags: filterTags
    });
    ctx.dispatch(new GetMedia(filterTags, 1));
  }

  @Action(SetFilterTags)
  applySavedFilters(ctx: StateContext<MediaStateModel>, { tags }: SetFilterTags) {
    const state = ctx.getState();
    return ctx.setState({
      ...state,
      filterTags: tags
    });
  }

  @Action(RemoveFilterTag)
  removeFilterTag(ctx: StateContext<MediaStateModel>, { tag, isStreamingArchive, isFavoriteSearch }: RemoveFilterTag) {
    const state = ctx.getState();
    if (isStreamingArchive) {
      let tags = state.archiveFilterTags;
      tags = tags.filter(t => (t.name !== tag.name || t.value !== tag.value));
      ctx.setState({
        ...state,
        archiveFilterTags: tags
      });
    } else {
      let filterTags = [...state.filterTags];
      let filters = [...state.filters];
      const queryFilterName = 'queryFilter';
      if (tag.name === queryFilterName) {
        filterTags = filterTags.filter(t => t.name != queryFilterName);
      } else {
        filterTags = filterTags.filter(x => (x.name !== tag.name || x.value !== tag.value));
        filters.forEach(filter => {
          if (filter.options) {
            filter.options.forEach(option => {
              if (filter.label === tag.label && option.label === tag.value) {
                option = { ...option, isChecked: false };
              }
            });
          }
        });
      }
      ctx.setState({
        ...state,
        filterTags: filterTags,
        filters: filters
      });

      if (isFavoriteSearch) {
        ctx.dispatch(new GetFavorites(1, 8));
      } else {
        ctx.dispatch(new GetMedia(filterTags, 1));
      }
    }
  }

  @Action(ResetAllFilters)
  resetAllFilters(ctx: StateContext<MediaStateModel>) {
    const state = ctx.getState();
    const filters = [...state.filters];
    filters.forEach(filter => {
      if (filter.options) {
        filter.options.forEach(option => {
          option = {
            ...option,
            isChecked: false
          };
        });
      }
    });
    ctx.setState({
      ...state,
      filterTags: [],
      filters: filters
    });
    ctx.dispatch(new GetMedia([], 1));
  }

  @Action(ResetFilter)
  resetFilter(ctx: StateContext<MediaStateModel>, { filter }: ResetFilter) {
    const state = ctx.getState();
    const filters = [...state.filters];
    let filterTags = [...state.filterTags];
    filters.forEach(f => {
      if (f.name === filter.name) {
        filterTags = filterTags.filter(t => t.name !== f.name);
      }
    });
    ctx.setState({
      ...state,
      filterTags: filterTags,
      filters: filters
    });
    ctx.dispatch(new GetMedia(filterTags, 1));
  }

  @Action(ClearFilterTags)
  clearFilterTags({ getState, setState }: StateContext<MediaStateModel>, { isStreamingArchive }: ClearFilterTags) {
    const state = getState();
    if (isStreamingArchive) {
      setState({
        ...state,
        isArchiveFilterApplied: false,
        archiveFilterTags: [],
        isArchiveFilterCleared: true
      });
    } else {
      const newState = Object.assign({}, state);
      newState.filters.forEach(field => {
        field.options.forEach(option => {
          option = { ...option, isChecked: false };
        });
      });
      setState({
        ...state,
        isFilterApplied: false,
        filterTags: [],
        isFilterCleared: true,
        filters: newState.filters
      });
    }
  }

  @Action(SetSavedFilterAction)
  setSavedFilterAction(ctx: StateContext<MediaStateModel>, { isSavedFilterApplied }: SetSavedFilterAction) {
    const state = ctx.getState();
    ctx.setState({
      ...state,
      isSavedFilterApplied
    });
  }

  //#endregion

  //#region Selected Files

  @Action(SetPlaylist)
  setPlaylist(ctx: StateContext<MediaStateModel>, { payload }: SetPlaylist) {
    ctx.dispatch(new ShowSpinner());
    return this.mediaDataService.getPlaylist(payload)
      .pipe(
        tap(playlist => {
          const state = ctx.getState();
          const sasToken = this.store.selectSnapshot(AppState.getMediaReadSASToken);
          playlist.forEach(p => {
            p.thumbnail += sasToken;
            p.url += sasToken;
          });
          ctx.setState({
            ...state,
            playlist
          });
          ctx.dispatch(new HideSpinner());
        }, err => ctx.dispatch(new ShowErrorMessage(err)))
      );

  }

  @Action(ClearPlaylist)
  clearPlaylist({ getState, setState }: StateContext<MediaStateModel>) {
    const state = getState();
    setState({
      ...state,
      playlist: []
    });
  }

  @Action(AddSelectedMediaItem)
  addSelectedItem({ getState, setState }: StateContext<MediaStateModel>, { item }: AddSelectedMediaItem) {
    const state = getState();
    const items = [...state.selectedItems];
    console.log('MediaState item before: ', items);
    if (items.includes(item)) { return; }
    items.push(item);
    console.log('MediaState item after: ', items);
    setState({
      ...state,
      selectedItems: items,
      isMediaSelectionCleared: false
    });
  }

  @Action(AddSelectedMediaItems)
  addSelectedItems({ getState, setState }: StateContext<MediaStateModel>, { items }: AddSelectedMediaItems) {
    const state = getState();
    let stateItems = [...state.selectedItems];
    console.log('AddSelectedMediaItems items before: ', stateItems);
    items.forEach(item => {
      const stateItemsIds = stateItems.map(x => x.documentId);
      stateItems = !stateItemsIds.includes(item.documentId) ? [...stateItems, item] : stateItems;
    });
    console.log('AddSelectedMediaItems items after: ', stateItems);
    setState({
      ...state,
      selectedItems: stateItems,
      isMediaSelectionCleared: false
    });
  }

  @Action(RemoveSelectedMediaItem)
  removeSelectedMediaItem({ getState, setState }: StateContext<MediaStateModel>, { item }: RemoveSelectedMediaItem) {
    const state = getState();
    let items = [...state.selectedItems];
    console.log('MediaState item before: ', items);
    items = items.filter(x => x.documentId !== item.documentId);
    console.log('MediaState item removed: ', items);
    setState({
      ...state,
      selectedItems: items
    });
  }

  @Action(RemoveSelectedMediaItems)
  removeSelectedMediaItems({ getState, setState }: StateContext<MediaStateModel>, { items }: RemoveSelectedMediaItems) {
    const state = getState();
    let stateItems = [...state.selectedItems];
    console.log('RemoveSelectedMediaItems items before: ', stateItems);
    items.forEach(item => {
      const stateItemsIds = stateItems.map(x => x.documentId);
      stateItems = stateItemsIds.includes(item.documentId) ? stateItems.filter(x => x.documentId !== item.documentId) : stateItems;
    });
    console.log('RemoveSelectedMediaItems items removed: ', stateItems);
    setState({
      ...state,
      selectedItems: stateItems
    });
  }

  @Action(SetSelectedMediaItems)
  setSelectedMediaItems({ getState, setState }: StateContext<MediaStateModel>, { items }: SetSelectedMediaItems) {
    const state = getState();
    setState({
      ...state,
      selectedItems: items,
      isMediaSelectionCleared: false
    });
  }

  @Action(ClearSelectedMediaItems)
  clearSelectedMediaItems({ getState, setState }: StateContext<MediaStateModel>) {
    const state = getState();
    setState({
      ...state,
      selectedItems: [],
      isMediaSelectionCleared: true
    });
  }

  //#endregion

  //#region Telemetry

  @Action(GetTelemetryItems)
  getTelemetry({ getState, setState }: StateContext<MediaStateModel>, { id }: GetTelemetryItems) {
    return this.mediaDataService.getTelemetry(id)
      .pipe(
        tap(data => {
          const state = getState();
          setState({
            ...state,
            telemetryItems: data,
            selectedTelemetryFeeds: data.feeds,
            // telementaryEvents: data.events || [],
          });
        })
      );
  }

  @Action(SetCurrentVideoTime)
  setCurrentVideoTime({ getState, setState }: StateContext<MediaStateModel>, { time }: SetCurrentVideoTime) {
    const state = getState();
    setState({
      ...state,
      currentVideoTime: time,

    });
  }

  @Action(ClearCurrentVideoTime)
  clearCurrentVideoTime({ getState, setState }: StateContext<MediaStateModel>) {
    const state = getState();
    setState({
      ...state,
      currentVideoTime: 0

    });
  }

  @Action(ClearTelemetryItems)
  clearTelemetryEvents({ getState, setState }: StateContext<MediaStateModel>) {
    const state = getState();
    setState({
      ...state,
      selectedTelemetryFeeds: [],

    });
  }



  @Action(ClearTelemetryEvent)
  clearTelemetryEvent({ getState, setState }: StateContext<MediaStateModel>) {
    const state = getState();
    setState({
      ...state,
      // telementaryEvents: [],

    });
  }
  //#endregion

  //#region Streaming Archives

  @Action(SetArchiveMode)
  setArchiveMode({ getState, setState }: StateContext<MediaStateModel>, { mode }: SetArchiveMode) {
    const state = getState();
    setState({
      ...state,
      streamingArchiveMode: mode
    });
  }

  @Action(ClearArchiveMode)
  clearArchiveMode({ getState, setState }: StateContext<MediaStateModel>) {
    const state = getState();
    setState({
      ...state,
      streamingArchiveMode: null
    });
  }

  @Action(GetDailyVideos)
  getDailyVideos(ctx: StateContext<MediaStateModel>, { asset, group, date }: GetDailyVideos) {
    ctx.dispatch(new ShowSpinner());
    return this.mediaService.getArchiveVideos(asset, group, date)
      .pipe(
        tap(videos => {
          const state = ctx.getState();
          ctx.setState({
            ...state,
            dailyVideos: videos
          });
          ctx.dispatch(new HideSpinner());
        }, error => ctx.dispatch(new ShowErrorMessage(error)))
      );
  }

  @Action(ClearDailyVideos)
  clearDailyVideos({ getState, setState }: StateContext<MediaStateModel>) {
    const state = getState();
    setState({
      ...state,
      dailyVideos: null
    });
  }

  @Action(GetHourlyVideos)
  getHourlyVideos({ getState, setState }: StateContext<MediaStateModel>, { asset, group, date, time }: GetHourlyVideos) {
    return this.mediaService.getArchiveVideos(asset, group, date, time)
      .pipe(
        tap(videos => {
          const state = getState();
          setState({
            ...state,
            hourlyVideos: videos
          });
        })
      );
  }

  @Action(GetStreamingArchiveAssets)
  getStreamingArchiveAssets(ctx: StateContext<MediaStateModel>) {
    return this.lookupService.getLiveStreamAssets()
      .pipe(
        tap(assets => {
          const state = ctx.getState();
          ctx.setState({
            ...state,
            streamingArchiveAssets: assets
          });
        }, error => ctx.dispatch(new ShowErrorMessage(error)))
      );
  }

  @Action(GetStreamingArchiveGroups)
  getStreamingArchiveGroups(ctx: StateContext<MediaStateModel>) {
    return this.lookupService.getLiveStreamGroups()
      .pipe(
        tap(data => {
          const state = ctx.getState();
          ctx.setState({
            ...state,
            streamingArchiveGroups: data
          });
        }, error => ctx.dispatch(new ShowErrorMessage(error)))
      );
  }

  @Action(GetAvailableArchiveDates)
  getAvailableArchiveDates(ctx: StateContext<MediaStateModel>, { asset, group }: GetAvailableArchiveDates) {
    if (!asset && !group) {
      const _state = ctx.getState();
      const tags = _state.archiveFilterTags;
      const assetTag = tags.find(tag => tag.name === 'asset');
      const groupTag = tags.find(tag => tag.name === 'group');
      if (!assetTag || !groupTag) {
        ctx.setState({
          ..._state,
          streamingArchiveDays: []
        });
        return;
      }
      asset = assetTag.value;
      group = groupTag.value;
    }
    ctx.dispatch(new ShowSpinner());
    return this.mediaService.getAvailableArchiveDays(asset, group)
      .pipe(
        tap(dates => {
          const state = ctx.getState();
          ctx.setState({
            ...state,
            streamingArchiveDays: dates
          });
          ctx.dispatch(new HideSpinner());
        }, error => ctx.dispatch(new ShowErrorMessage(error)))
      );
  }

  //#endregion

  //#endregion
}

