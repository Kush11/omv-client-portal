import { MediaItem, Media } from 'src/app/core/models/entity/media';
import { Tag } from 'src/app/core/models/entity/tag';
import { Breadcrumb } from 'src/app/core/models/breadcrumb';
import { ArchiveMode } from 'src/app/core/enum/archive-mode';
import { FieldConfiguration } from 'src/app/shared/dynamic-components/field-setting';
import { ListItem } from 'src/app/core/models/entity/list-item';
import { query } from '@angular/animations';
import { Filter } from 'src/app/core/models/entity/filter';
import { Search_MapFilterDTO } from 'src/app/core/dtos/input/documents/Document_SearchInputDTO';
import { Directory } from 'src/app/core/models/entity/directory';

//#region BreadCrumb

export class SetBreadcrumbs {
  static readonly type = '[Media] SetBreadcrumbs';

  constructor(public breadcrumbs: Breadcrumb[]) { }
}

export class UpdateBreadcrumb {
  static readonly type = '[Media] UpdateBreadcrumb';

  constructor(public breadcrumb: Breadcrumb) { }
}

//#endregion

//#region MEDIA

export class GetMedia {
  static readonly type = '[Media] GetMedia';

  constructor(public filters?: Tag[], public pageNumber?: number, public pageSize?: number, public isMapView?: boolean, public mapCoordinates?: Search_MapFilterDTO[]) { }
}

export class ShowIsFetching {
  static readonly type = '[App] ShowIsFetching';
}

export class ShowNewFolder {
  static readonly type = '[App] ShowIsFetching';
}

export class HideIsFetching {
  static readonly type = '[App] HideIsFetching';
}

export class ShowIsFilterResult {
  static readonly type = '[App] ShowIsFilterResult';
}

export class HideIsFilterResult {
  static readonly type = '[App] HideIsFilterResult';
}

export class RefetchMedia {
  static readonly type = '[Media] RefetchMedia';

  constructor(public pageNumber?: number, public pageSize?: number, public isMapView?: boolean, public mapCoordinates?: Search_MapFilterDTO[]) { }
}

export class SetMapCoordinates {
  static readonly type = '[Media] SetMapCoordinates';

  constructor(public mapCoordinates: Search_MapFilterDTO[]) { }

}

export class GetMapCoordinates {
  static readonly type = '[Media] GetMapCoordinates';


}

export class GetTreeViewMedia {
  static readonly type = '[Media] GetTreeViewMedia';

  constructor(public pageNumber?: number, public pageSize?: number) { }
}

export class GetMapViewMedia {
  static readonly type = '[Media] GetMapViewMedia';

  constructor(public pageNumber?: number, public pageSize?: number) { }
}

export class ClearDirectoryMetadata {
  static readonly type = '[Media] ClearDirectoryMetadata';
}

export class ResetUploadStatus {
  static readonly type = '[Media] ResetUploadStatus';
}

export class GetDirectories {
  static readonly type = '[Media] GetDirectories';
}

export class SaveExpandedDirectories {
  static readonly type = '[Media] SaveExpandedDirectories';

  constructor(public directories: Directory[]) { }
}

export class SaveSelectedMedia {
  static readonly type = '[Media] SaveSelectedMedia';

  constructor(public selectedMediaId: any) { }
}

//#endregion

//#region Media Item 

export class GetMediaItemDetails {
  static readonly type = '[Media] GetMediaItemDetails';

  constructor(public id: any) { }
}

export class SetCurrentMediaItemId {
  static readonly type = '[Media] SetCurrentMediaItemId';

  constructor(public id: any) { }
}

export class GetMediaItem {
  static readonly type = '[Media] GetMediaItem';

  constructor(public id: any) { }
}

export class ClearCurrentMediaItem {
  static readonly type = '[Media] ClearCurrentMediaItem';
}

export class ClearMediaItemMetadata {
  static readonly type = '[Media] ClearMediaItemMetadata';
}

export class GetRelatedFiles {
  static readonly type = '[Media] GetRelatedFiles';

  constructor(public id: string) { }
}

export class AddRelatedFiles {
  static readonly type = '[Media] AddRelatedFiles';

  constructor(public documentId: string, public relatedDocumentId: string[]) { }
}

export class DeleteRelatedFiles {
  static readonly type = '[Media] DeleteRelatedFiles';

  constructor(public documentId: string, public relatedDocumentId: string[]) { }
}

export class DeleteMediaItem {
  static readonly type = '[Media] deleteMediaItem';

  constructor(public id: any) { }
}

export class UpdateMediaItem {
  static readonly type = '[Media] UpdateMediaItem';

  constructor(public id: any, public payload: MediaItem) { }
}

export class CreateMediaItem {
  static readonly type = '[Media] CreateMediaItem';

  constructor(public payload: MediaItem) { }
}

export class DownloadMediaItem {
  static readonly type = '[Media] DownloadMediaItem';

  constructor(public name: string, public url: string) { }
}

export class DownloadMediaItems {
  static readonly type = '[Media] DownloadMediaItems';

  constructor(public items: MediaItem[]) { }
}

export class UpdateMetadata {
  static readonly type = '[Media] UpdateMetadata';

  constructor(public id: any, public metadata: string) { }
}

//#endregion

//#region Favorites

export class GetFavorites {
  static readonly type = '[Media] GetFavorites';

  constructor(public pageNumber?: number, public pageSize?: number, public query?: string) { }
}

export class ToggleFavorite {
  static readonly type = '[Media] ToggleFavorite';

  constructor(public id: number, public payload: MediaItem) { }
}

export class AddFavorite {
  static readonly type = '[Media] AddFavorite';

  constructor(public id?: any, public name?: string, public tags?: Tag[]) { }
}

export class RemoveFavorite {
  static readonly type = '[Media] RemoveFavorite';

  constructor(public favoriteItemId: number) { }
}

export class ResetFavoriteActions {
  static readonly type = '[Media] ResetFavoriteActions';
}

//#endregion

export class GetHistory {
  static readonly type = '[Media] GetHistory';

  constructor(public id: any, public pageNumber?: number, public pageSize?: number) { }
}

export class ClearHistory {
  static readonly type = '[Media] ClearHistory';
}
export class GetMediaTreeData {
  static readonly type = '[Media] GetMediaTreeData';

  constructor() { }
}

export class GetDirectoryMetadata {
  static readonly type = '[Media] GetDirectoryMetadata';

  constructor(public id: number) { }
}

export class SetPlaylist {
  static readonly type = '[Media] SetPlaylist';

  constructor(public payload?: string[]) { }
}

export class ClearPlaylist {
  static readonly type = '[Media] ClearPlaylist';
}

//#region Filters

export class SetFilterQuery {
  static readonly type = '[Media] SetFilterQuery';

  constructor(public query: string) { }
}

export class AddFilterTag {
  static readonly type = '[Media] AddFilterTag';

  constructor(public tag: Tag, public isStreamingArchive?: boolean, public isFavoriteSearch?: boolean, public currentPage?: number) { }
}

export class AddFilterTags {
  static readonly type = '[Media] AddFilterTags';

  constructor(public filterName: string, public tags: Tag[]) { }
}

export class SetFilterTags {
  static readonly type = '[Media] SetFilterTags';

  constructor(public tags: Tag[]) { }
}

export class RemoveFilterTag {
  static readonly type = '[Media] RemoveFilterTag';

  constructor(public tag: Tag, public isStreamingArchive?: boolean, public isFavoriteSearch?: boolean) { }
}

export class ResetAllFilters {
  static readonly type = '[Media] ResetAllFilters';
}

export class ResetFilter {
  static readonly type = '[Media] ResetFilter';

  constructor(public filter: Filter) { }
}

export class ClearFilterTags {
  static readonly type = '[Media] ClearFilterTags';

  constructor(public isStreamingArchive?: boolean) { }
}

export class ShowFilters {
  static readonly type = '[Media] ShowFilters';

  constructor(public isStreamingArchive?: boolean) { }
}

export class HideFilters {
  static readonly type = '[Media] HideFilters';

  constructor(public isStreamingArchive?: boolean) { }
}

export class ApplyFilters {
  static readonly type = '[Media] ApplyFilters';

  constructor(public tags?: Tag[], public query?: string, public pageNumber?: number, public pageSize?: number, public isStreamingArchive?: boolean) { }
}

export class SetSavedFilterAction {
  static readonly type = '[Media] SetSavedFilterAction';

  constructor(public isSavedFilterApplied: boolean, public isStreamingArchive?: boolean) { }
}

export class ToggleFilterCheckbox {
  static readonly type = '[Media] ToggleFilterCheckbox';

  constructor(public option: ListItem) { }
}

export class RemoveFilterGroupTag {
  static readonly type = '[Media] RemoveFilterGroupTag';

  constructor(public field: FieldConfiguration) { }
}

export class SetSearchQuery {
  static readonly type = '[Media] SetSearchQuery';

  constructor(public query: string) { }
}

//#endregion

export class AddSelectedMediaItem {
  static readonly type = '[Media] AddSelectedMediaItem';

  constructor(public item: MediaItem) { }
}

export class AddSelectedMediaItems {
  static readonly type = '[Media] AddSelectedMediaItems';

  constructor(public items: MediaItem[]) { }
}

export class RemoveSelectedMediaItem {
  static readonly type = '[Media] RemoveSelectedMediaItem';

  constructor(public item: MediaItem) { }
}

export class RemoveSelectedMediaItems {
  static readonly type = '[Media] RemoveSelectedMediaItems';

  constructor(public items: MediaItem[]) { }
}

export class SetSelectedMediaItems {
  static readonly type = '[Media] SetSelectedMediaItems';

  constructor(public items: MediaItem[]) { }
}

export class ClearSelectedMediaItems {
  static readonly type = '[Media] ClearSelectedMediaItems';
}

//#region Telemetry Items

export class GetTelemetryEvent {
  static readonly type = '[Media] GetTelemetryEvent';

  constructor(public id: number) { }
}

export class GetTelemetryItems {
  static readonly type = '[Media] GetTelemetryItems';
  constructor(public id: number) { }

}
export class GetTelemetrySurve {
  static readonly type = '[Media] GetTelemetrySurve';

  constructor(public id: number) { }
}

export class SetCurrentVideoTime {
  static readonly type = '[Media] SetCurrentVideoTime';

  constructor(public time: number) { }
}

export class SetArchiveTooBar {
  static readonly type = '[Media] SetArchiveTooBar';

  constructor(public status: boolean) { }
}

export class SetSeLetedTelemetryItem {
  static readonly type = '[Media] SetSeLetedTelemetryItem';

  constructor(public feeds: MediaItem) { }
}

export class GetCurrentVideoTime {
  static readonly type = '[Media]  GetCurrentVideoTime';

  constructor(public time: number) { }
}
export class ClearCurrentVideoTime {
  static readonly type = '[Media]  ClearCurrentVideoTime';


}

export class ClearTelemetryItems {
  static readonly type = '[Media] ClearTelemetryItems';
}

export class ClearTelemetryEvent {
  static readonly type = '[Media] ClearTelemetryEvent';
}
//#endregion

//#region STREAMING ARCHIVE

export class SetArchiveMode {
  static readonly type = '[Media] SetArchiveMode';

  constructor(public mode: ArchiveMode) { }
}

export class ClearArchiveMode {
  static readonly type = '[Media] ClearArchiveMode';
}

export class GetDailyVideos {
  static readonly type = '[Media] GetDailyVideos';

  constructor(public asset: string, public group: string, public date: string) { }
}

export class GetHourlyVideos {
  static readonly type = '[Media] GetHourlyVideos';

  constructor(public asset: string, public group: string, public date: string, public time: string) { }
}

export class ClearDailyVideos {
  static readonly type = '[Media] ClearDailyVideos';
}

export class GetStreamingArchiveAssets {
  static readonly type = '[Media] GetStreamingArchiveAssets';
}

export class GetStreamingArchiveGroups {
  static readonly type = '[Media] GetStreamingArchiveGroups';
}

export class GetAvailableArchiveDates {
  static readonly type = '[Media] GetAvailableArchiveDates';

  constructor(public asset?: string, public group?: string) { }
}

//#endregion
