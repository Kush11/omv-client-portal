import { Component, OnInit, OnDestroy } from '@angular/core';
import { Store, Select } from '@ngxs/store';
import { Router, ActivatedRoute } from '@angular/router';
import { MediaState } from '../state/media/media.state';
import { AppState } from 'src/app/state/app.state';
import { Observable } from 'rxjs/internal/Observable';
import { Breadcrumb } from 'src/app/core/models/breadcrumb';
import { SubSink } from 'subsink/dist/subsink';
import { MediaItem } from 'src/app/core/models/entity/media';
import { MediaType } from 'src/app/core/enum/media-type';
import { GetMediaItem, ClearCurrentMediaItem, DownloadMediaItem } from '../state/media/media.action';
import { BaseComponent } from 'src/app/shared/base/base.component';
import { MediaService } from 'src/app/core/services/business/media/media.service';
import { TranscriptSegment } from 'src/app/core/models/entity/transcript-segment';

@Component({
  selector: 'app-media-viewer',
  templateUrl: './media-viewer.component.html',
  styleUrls: ['./media-viewer.component.css']
})
export class MediaViewerComponent extends BaseComponent implements OnInit, OnDestroy {

  @Select(MediaState.getCurrentMediaItem) mediaItem$: Observable<MediaItem>;
  @Select(AppState.getPreviousRoute) previousRoute$: Observable<Breadcrumb[]>;

  private subs = new SubSink();

  type: string;
  url: string;
  name: string;
  thumbnails: any;
  transcript: TranscriptSegment[];
  containerUrl: string;
  sasToken: string;
  thumbnailUrl: string = ''

  constructor(protected store: Store, private router: Router, private route: ActivatedRoute, private mediaService: MediaService) {
    super(store);
    this.hideLeftNav();
  }

  ngOnInit() {
    const mediaItemId = this.route.snapshot.paramMap.get('id');

    this.store.dispatch(new GetMediaItem(mediaItemId));
    this.mediaService.getVideoTranscript(mediaItemId)
      .then(segments => {
        this.transcript = segments;
      }, err => this.showErrorMessage(err));
    this.subs.add(
      this.mediaItem$
        .subscribe(item => {
          if (item) {
            this.getMediaType(item.type.toLowerCase());
            this.url = item.url;
            this.name = item.name;
            this.thumbnails = item.thumbnailObject;
            this.containerUrl = item.thumbnailContainer;
            this.thumbnailUrl = item.thumbnail;
          } else {
            this.type = this.name = this.url = '';
          }
        })
    );

    this.sasToken = this.store.selectSnapshot(AppState.getMediaReadSASToken);
  }

  ngOnDestroy() {
    // this.type = this.name = this.url = '';
    this.store.dispatch(new ClearCurrentMediaItem());
    this.subs.unsubscribe();
  }

  onBack(route: string) {
    this.router.navigate([route]);
  }

  getMediaType(type: string) {
    const docFormats = ['doc', 'docx', 'xls', 'xlsx', 'ppt', 'log'];
    const imageFormats = ['jpeg', 'jpg', 'bmp', 'gif', 'svg', 'png'];
    const videoFormats = ['mp4', 'mov', 'avi', 'mpg', 'mkv'];
    if (docFormats.includes(type)) {
      this.type = MediaType.Document;
    } else if (imageFormats.includes(type)) {
      this.type = MediaType.Image;
    } else if (videoFormats.includes(type)) {
      this.type = MediaType.Video;
    } else if (type === 'pdf') {
      this.type = MediaType.Pdf;
    } else {
      this.type = MediaType.Others;
    }
  }

  download(item: any) {
    if (item) {
      this.url = item;
      this.name = this.name;
    }
    this.store.dispatch(new DownloadMediaItem(this.name, this.url));
  }
}
