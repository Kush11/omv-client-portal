import { Component, OnInit, OnDestroy } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { Select, Store } from '@ngxs/store';
import { MediaState } from '../../state/media/media.state';
import { StreamingArchive } from 'src/app/core/models/entity/streaming-archive';
import { SubSink } from 'subsink/dist/subsink';
import { Router, ActivatedRoute } from '@angular/router';
import { GetDailyVideos, ClearDailyVideos } from '../../state/media/media.action';
import { BaseComponent } from 'src/app/shared/base/base.component';
import { DateService } from 'src/app/core/services/business/dates/date.service';

@Component({
  selector: 'app-daily-video-schedule',
  templateUrl: './daily-video-schedule.component.html',
  styleUrls: ['./daily-video-schedule.component.css']
})
export class DailyVideoScheduleComponent extends BaseComponent implements OnInit, OnDestroy {

  @Select(MediaState.getDailyVideos) dailySchedules$: Observable<StreamingArchive>;

  private subs = new SubSink();
  dailyTimes: { text: string, value: number, meridiem: string }[] = [
    { text: '12', value: 12, meridiem: "am" },
    { text: '01', value: 1, meridiem: "am" },
    { text: '02', value: 2, meridiem: "am" },
    { text: '03', value: 3, meridiem: "am" },
    { text: '04', value: 4, meridiem: "am" },
    { text: '05', value: 5, meridiem: "am" },
    { text: '06', value: 6, meridiem: "am" },
    { text: '07', value: 7, meridiem: "am" },
    { text: '08', value: 8, meridiem: "am" },
    { text: '09', value: 9, meridiem: "am" },
    { text: '10', value: 10, meridiem: "am" },
    { text: '11', value: 11, meridiem: "am" },
    { text: '12', value: 12, meridiem: "pm" },
    { text: '01', value: 1, meridiem: "pm" },
    { text: '02', value: 2, meridiem: "pm" },
    { text: '03', value: 3, meridiem: "pm" },
    { text: '04', value: 4, meridiem: "pm" },
    { text: '05', value: 5, meridiem: "pm" },
    { text: '06', value: 6, meridiem: "pm" },
    { text: '07', value: 7, meridiem: "pm" },
    { text: '08', value: 8, meridiem: "pm" },
    { text: '09', value: 9, meridiem: "pm" },
    { text: '10', value: 10, meridiem: "pm" },
    { text: '11', value: 11, meridiem: "pm" }
  ];
  schedules: StreamingArchive;
  queryAsset: string;
  queryGroup: string;
  queryDate: string;
  queryTime: string;
  currentDate: Date;

  constructor(protected store: Store, private router: Router, private route: ActivatedRoute, private dateService: DateService) {
    super(store);
    this.setPageTitle("Streaming Archive - Daily Videos");
  }

  ngOnInit() {
    this.subs.add(
      this.route.queryParams
        .subscribe(params => {
          const { asset, group, date, time } = params
          this.queryAsset = asset;
          this.queryGroup = group;
          this.queryDate = date;
          const isDateValid = this.dateService.isValid(date, 'MM-DD-YYYY');
          if (!isDateValid) return;
          this.currentDate = new Date(this.queryDate);
          if (!time)
            this.store.dispatch(new GetDailyVideos(this.queryAsset, this.queryGroup, this.queryDate));
        }),
      this.dailySchedules$
        .subscribe(schedules => this.schedules = schedules)
    );
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
    this.store.dispatch(new ClearDailyVideos());
  }

  navigate(time: { text: string, value: number, meridiem: string }) {
    this.queryTime = `${time.text}:00 ${time.meridiem}`;
    this.router.navigate(
      ['.'],
      { relativeTo: this.route, queryParams: { asset: this.queryAsset, group: this.queryGroup, date: this.queryDate, time: this.queryTime } }
    );
  }

  onClose() {
    this.router.navigate(['/media/streaming-archive']);
  }

  stepBackward() {
    const yesterday = this.dateService.addDays(this.currentDate, -1);
    const date = this.dateService.formatToDateOnly(yesterday);
    this.router.navigate(
      ['.'],
      { relativeTo: this.route, queryParams: { asset: this.queryAsset, group: this.queryGroup, date: date } }
    );
  }

  stepForward() {
    const tomorrow = this.dateService.addDays(this.currentDate, 1);
    const date = this.dateService.formatToDateOnly(tomorrow);
    this.router.navigate(
      ['.'],
      { relativeTo: this.route, queryParams: { asset: this.queryAsset, group: this.queryGroup, date: date } }
    );
  }
}
