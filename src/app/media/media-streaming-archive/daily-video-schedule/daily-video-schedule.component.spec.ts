import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DailyVideoScheduleComponent } from './daily-video-schedule.component';

describe('DailyVideoScheduleComponent', () => {
  let component: DailyVideoScheduleComponent;
  let fixture: ComponentFixture<DailyVideoScheduleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DailyVideoScheduleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DailyVideoScheduleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
