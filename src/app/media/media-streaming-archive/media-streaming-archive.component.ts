import { Component, OnInit, OnDestroy } from '@angular/core';
import { Store, Select } from '@ngxs/store';
import { MediaState } from '../state/media/media.state';
import { Observable } from 'rxjs';
import { SubSink } from 'subsink/dist/subsink';
import { BaseComponent } from 'src/app/shared/base/base.component';
import { StreamingArchive } from 'src/app/core/models/entity/streaming-archive';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-media-streaming-archive',
  templateUrl: './media-streaming-archive.component.html',
  styleUrls: ['./media-streaming-archive.component.css']
})
export class MediaStreamingArchiveComponent extends BaseComponent implements OnInit, OnDestroy {

  @Select(MediaState.getDailyVideos) dailyVideos$: Observable<StreamingArchive>;

  private subs = new SubSink();
  dailyVideos: StreamingArchive;
  currentAsset: string;
  currentGroup: string;
  currentDate: string;
  currentTime: string;
  isDailyMode: boolean;
  isHourlyMode: boolean;

  constructor(protected store: Store, private route: ActivatedRoute) {
    super(store);
    this.setPageTitle('Streaming Archive');
  }

  ngOnInit() {
    this.subs.add(
      this.route.queryParams
        .subscribe(params => {
          const { asset, group, date, time } = params;
          this.currentAsset = asset;
          this.currentGroup = group;
          this.currentDate = date;
          this.currentTime = time;
          if (asset && group && date && !time) {
            this.isDailyMode = true;
            this.isHourlyMode = false;
          } else if (asset && group && date && time) {
            this.isDailyMode = false;
            this.isHourlyMode = true;
          } else {
            this.isDailyMode = false;
            this.isHourlyMode = false;
          }
        }),
      this.dailyVideos$
        .subscribe(videos => this.dailyVideos = videos)
    );
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }
}
