import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HourlyVideoScheduleComponent } from './hourly-video-schedule.component';

describe('HourlyVideoScheduleComponent', () => {
  let component: HourlyVideoScheduleComponent;
  let fixture: ComponentFixture<HourlyVideoScheduleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HourlyVideoScheduleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HourlyVideoScheduleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
