import { Component, OnInit, ViewChild, ElementRef, OnDestroy, AfterViewInit } from '@angular/core';
import { trigger, state, style, animate, transition } from '@angular/animations';
import { Store, Select } from '@ngxs/store';
import { StreamingArchive, CameraFeed } from 'src/app/core/models/entity/streaming-archive';
import { SubSink } from 'subsink/dist/subsink';
import { ActivatedRoute, Router } from '@angular/router';
import { DateService } from 'src/app/core/services/business/dates/date.service';
import { BaseComponent } from 'src/app/shared/base/base.component';
import { MediaService } from 'src/app/core/services/business/media/media.service';
import { ShowErrorMessage, GetAzureSASToken } from 'src/app/state/app.actions';
import { Observable } from 'rxjs/internal/Observable';
import { Slider, SliderChangeEventArgs, SliderTooltipEventArgs } from '@syncfusion/ej2-angular-inputs';
import { AppState } from 'src/app/state/app.state';
import { SASTokenType } from 'src/app/core/enum/azure-sas-token';

@Component({
  selector: 'app-hourly-video-schedule',
  templateUrl: './hourly-video-schedule.component.html',
  styleUrls: ['./hourly-video-schedule.component.css'],
  animations: [
    trigger('ExpandMinimize', [
      state('true', style({
        opacity: '1'
      })),
      state('false', style({
        opacity: '0'
      })),
      transition('false => true', [
        animate('.6s ease-in')
      ]),
      transition('true => false', [
        animate('.6s ease-out')
      ]),
    ])
  ]
})
export class HourlyVideoScheduleComponent extends BaseComponent implements OnInit, OnDestroy, AfterViewInit {

  @ViewChild('livestreamSeeker') seeker: ElementRef;
  @ViewChild('slider') slider: Slider;
  @ViewChild('sliderLine') sliderLine: ElementRef;
  @ViewChild('timelineContainer') timelineContainer: ElementRef;

  @Select(AppState.getMediaReadSASToken) sasToken$: Observable<string>;

  private subs = new SubSink();
  playIcon = "./assets/images/streaming-archive-play.svg";
  pauseIcon = "./assets/images/icon-pause.svg";
  tooltip: Object = {
    placement: 'After', isVisible: true, cssClass: 'e-tooltip-cutomization'
  };
  limits: object = { enabled: true, minStart: 0, minEnd: 3600 };
  isPlaying = false;
  archive: StreamingArchive;
  timer: number = 0;
  playing: boolean;
  currentlyPlaying: string[] = [];
  changed: boolean;
  intervalId: any;
  currentTime: Date;
  startTime: Date;
  currentTimeString: string;
  currentEndTimeString: string;
  queryDate: string;
  queryAsset: string;
  assetGroup: string;
  queryTime: string;
  sliderTrack: HTMLElement;
  sliderHandle: HTMLElement;
  isTimelineExpanded: boolean;
  timeline: any[] = [];
  timelineSpanText: string;
  timespanStart: string;
  timespanEnd: string;
  currentState = 'initial';
  videoJSElements: any[] = [];
  previousSliderValue: number;
  cameras: HTMLVideoElement[] = [];
  archive$: Observable<StreamingArchive>;
  sliderLineHeight = '0px';
  sliderOffset: string;
  isRendering: boolean;
  validVideoCount: number;
  validVideos: CameraFeed[] = [];

  constructor(protected store: Store, private router: Router, private route: ActivatedRoute, private dateService: DateService,
    private mediaService: MediaService) {
    super(store);
    this.setPageTitle("Streaming Archive - Hourly Videos");
  }

  ngOnInit() {
    this.store.dispatch(new GetAzureSASToken(SASTokenType.Read));
    this.subs.add(
      this.route.queryParams
        .subscribe(params => {
          if (this.slider) this.slider.value = 0;
          this.timer = 0;
          this.getSliderLinePosition(0);
          const { asset, group, date, time } = params;
          this.queryAsset = asset;
          this.assetGroup = group;
          this.queryDate = date;
          this.queryTime = time;
          const isDateValid = this.dateService.isValid(date, 'MM-DD-YYYY');
          const isTimeValid = this.dateService.isValid(time, 'hh:mm a');
          if (!isDateValid || !isTimeValid) return;
          this.generateTimeline();
          this.getHourlyVideos();
        })
    );
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

  ngAfterViewInit() {
    this.sliderLineHeight = `${this.timelineContainer.nativeElement.offsetHeight}px`;
  }

  //#region VIDEOS

  private getHourlyVideos() {
    this.showSpinner();
    this.archive$ = this.mediaService.getArchiveVideos(this.queryAsset, this.assetGroup, this.queryDate, this.queryTime);
    this.subs.sink =
      this.archive$
        .subscribe(archive => {
          if (archive) {
            this.archive = archive;
            this.getAvailableRecordings(0);
            this.validVideos = this.archive.feeds.filter(feed => feed.url);
            if (this.validVideos.length === 0) {
              if (this.isRendering) {
                this.onPlay();
              }
            }
          }
          this.hideSpinner();
        }, err => this.store.dispatch(new ShowErrorMessage(err)));
  }

  getAvailableRecordings(sliderValue: number, normalPlayRate?: boolean) {
    const refDate = new Date(this.queryDate + ' ' + this.queryTime);
    const currentTime = this.dateService.addSeconds(refDate, sliderValue);
    if (!this.archive) return;
    this.archive.feeds.forEach((screen, i) => {
      const video = screen.durations.find(v => v.startTime <= currentTime && v.endTime >= currentTime);
      if (video) {
        screen.url = video.url;
        const videoCurrentTime = this.dateService.difference(currentTime, video.startTime, 's');
        let videoElement = document.getElementById(screen.id) as HTMLVideoElement;

        if (!videoElement) return;
        if (!normalPlayRate) {
          videoElement.currentTime = videoCurrentTime;
        }
        if (normalPlayRate) {
          const diff = videoCurrentTime - videoElement.currentTime;
          if (diff > 1 || diff < 0) {
            videoElement.currentTime = videoCurrentTime;
          }
        }
        if (this.isPlaying && videoElement.paused) {
          let playPromise = videoElement.play();
          if (playPromise !== undefined) {
            playPromise.then(function () {
            }).catch(function (error) {
              screen.url = '';
              screen.url = video.url;
            });
          }
        }
      } else {
        screen.url = '';
      }
    });
  }

  onWaiting(id: any) {
    const screen = this.archive.feeds.find(feed => feed.id == id);
    screen.isLoading = true;
  }

  onCanPlay(id: any) {
    const screen = this.archive.feeds.find(feed => feed.id == id);
    screen.isLoading = false;

    if (this.isRendering) {
      this.validVideos = this.validVideos.filter(v => v.id != id);
      if (this.validVideos.length === 0) {
        this.onPlay();
        this.isRendering = false;
      }
    }
  }

  //#endregion

  //#region TIMELINE CONTROLS

  onPlay() {
    this.isPlaying = true;
    this.intervalId = setInterval(() => {
      if (this.timer === 3600) {
        clearInterval(this.intervalId);
        this.isPlaying = false;
        return;
      }
      this.timer++;
      this.slider.value = this.timer;
    }, 1000);
  }

  onPause() {
    this.isPlaying = false;
    let videos = document.getElementsByTagName('video');
    let videoArray = Array.from(videos);
    videoArray.forEach(video => {
      if (!video.paused) {
        video.pause();
      }
    });
    clearInterval(this.intervalId);
  }

  stepForward() {
    this.timer += 10;
    this.slider.value = this.timer;
  }

  stepBackward() {
    this.timer -= 10;
    this.slider.value = this.timer;
  }

  fastForward() {
    if (this.isPlaying) {
      this.isRendering = true;
      this.onPause();
    }
    const currentTime = new Date(this.queryDate + ' ' + this.queryTime);
    const endTime = this.dateService.addHours(currentTime, 1);
    const date = this.dateService.formatToDateOnly(endTime);
    const time = this.dateService.formatToTimeOnly(endTime);
    this.router.navigate(
      ['.'],
      { relativeTo: this.route, queryParams: { asset: this.queryAsset, group: this.assetGroup, date: date, time: time } }
    );
  }

  fastBackward() {
    if (this.isPlaying) {
      this.isRendering = true;
      this.onPause();
    }
    const currentTime = new Date(this.queryDate + ' ' + this.queryTime);
    const endTime = this.dateService.addHours(currentTime, -1);
    const date = this.dateService.formatToDateOnly(endTime);
    const time = this.dateService.formatToTimeOnly(endTime);
    this.router.navigate(
      ['.'],
      { relativeTo: this.route, queryParams: { asset: this.queryAsset, group: this.assetGroup, date: date, time: time } }
    );
  }

  //#endregion

  //#region TIMELINE SLIDER

  onCreated() {
    this.sliderTrack = document.getElementById('height_slider').querySelector('.e-range');
    this.sliderHandle = document.getElementById('height_slider').querySelector('.e-handle');
    (this.sliderHandle as HTMLElement).style.backgroundColor = '#ffffff';
    (this.sliderTrack as HTMLElement).style.backgroundColor = '#0097a9';
  }

  onSliderChanged(args: SliderChangeEventArgs) {
    const { value, previousValue } = args;
    this.timer = Number(value);
    this.getSliderLinePosition(this.timer);
    this.currentTime = this.dateService.addSeconds(this.startTime, this.timer);
    if (this.timer === 3600) {
      this.fastForward();
    }
    const diff = Number(value) - Number(previousValue);
    this.getAvailableRecordings(this.timer, diff === 1);
  }

  onSliderChanging(args: SliderChangeEventArgs) {
    const value = Number(args.value);
    this.getSliderLinePosition(value);
  }

  tooltipChangeHandler(args: SliderTooltipEventArgs): void {
    const refDate = new Date(this.queryDate + ' ' + this.queryTime);
    const value = Number(args.value);
    let currentValue = this.dateService.addSeconds(refDate, value);
    const toolTipText = this.dateService.formatToString(currentValue, "hh:mm:ss a");
    args.text = toolTipText;
    return;
  }

  getSliderLinePosition(sliderValue: number) {
    const offset = (sliderValue / 3624) * 100;
    this.sliderOffset = `${offset + 0.3}%`;
    console.log("Slider Position: ", this.sliderOffset, ' slider value: ', sliderValue);
  }

  //#endregion

  //#region TIMELINE GRID

  private generateTimeline() {
    this.startTime = new Date(this.queryDate + ' ' + this.queryTime);
    this.currentTime = this.startTime;
    const endTime = this.dateService.addHours(this.startTime, 1);
    this.timespanEnd = this.dateService.formatToString(endTime, 'hh:mm:ss a');
    let time = this.startTime;
    this.timeline = [];
    for (let i = 0; i < 12; i++) {
      const text = this.dateService.formatToString(time, 'hh:mm');
      const meridiem = this.dateService.formatToString(time, 'a');
      let pointInTime: { text: string, value: Date, meridiem: string } = { text: text, meridiem: meridiem, value: time };
      time = this.dateService.addMinutes(time, 5);
      this.timeline.push(pointInTime);
    }
  }

  //#endregion
}
