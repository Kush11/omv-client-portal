import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MediaTelemetryComponent } from './media-telemetry.component';

describe('MediaTelementaryModeComponent', () => {
  let component: MediaTelemetryComponent;
  let fixture: ComponentFixture<MediaTelemetryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MediaTelemetryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MediaTelemetryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
