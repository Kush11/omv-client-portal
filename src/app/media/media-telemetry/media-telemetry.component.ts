import { Component, OnInit, ViewChild, ElementRef, AfterViewInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Store, Select } from '@ngxs/store';
import { MediaState } from '../state/media/media.state';
import { AppState } from 'src/app/state/app.state';
import { Observable } from 'rxjs';
import { MediaItem } from 'src/app/core/models/entity/media';
import { SubSink } from 'subsink/dist/subsink';
import { Tab } from 'src/app/core/models/tab';
import { MediaService } from 'src/app/core/services/business/media/media.service';
import { BaseComponent } from 'src/app/shared/base/base.component';
import { TelemetryFile } from 'src/app/core/models/entity/telemetry';
import { DateService } from 'src/app/core/services/business/dates/date.service';
import { MediaTelemetryFilesComponent } from './media-telemetry-files/media-telemetry-files.component';
import { Slider, SliderChangeEventArgs, SliderTooltipEventArgs } from '@syncfusion/ej2-angular-inputs';

@Component({
  selector: 'app-media-telemetry',
  templateUrl: './media-telemetry.component.html',
  styleUrls: ['./media-telemetry.component.css']
})
export class MediaTelemetryComponent extends BaseComponent implements OnInit, OnDestroy {

  @Select(MediaState.getSelectedTelemetryFeeds) telemetryVideos$: Observable<MediaItem[]>;
  @Select(AppState.getPreviousRoute) previousRoute$: Observable<string>;

  @ViewChild('telModeSeeker') telModeSeeker: ElementRef;
  @ViewChild('slider') slider: Slider;
  @ViewChild('telemetryFilesComponent') telemetryFilesComponent: MediaTelemetryFilesComponent;

  private subs = new SubSink();
  tooltip: Object = { placement: 'Before', isVisible: true, cssClass: 'e-tooltip-cutomization' };
  playIcon = "./assets/images/sync-play-active.svg";
  pauseIcon = './assets/images/playPause.svg';
  videos: MediaItem[] = [];
  readyToPlaythrough: number[] = [];
  isReadyToPlayAll: boolean = false;
  selectedIndex: number;
  progressBarValue: number = 0;
  currentPlayingTime: number = 0;
  duration: number = 0;
  tabs: Tab[] = [];
  mediaItemIds = [1, 2, 3];
  currentMediaItemId: any;
  queryType: any;
  isEventListActive: boolean;
  telemetryFiles: TelemetryFile[] = [];
  sliderTrack: HTMLElement;
  sliderHandle: HTMLElement;
  maxDuration = 0;
  currentTime = 0;
  isPlaying: boolean;
  isReallyPlaying: boolean;
  intervalId: any;
  isBuffering = true;
  canPlayCount = 0;

  get videoPlayers() {
    return Array.from(document.getElementsByClassName('video')) as HTMLVideoElement[];
  }

  constructor(protected store: Store, private router: Router, private route: ActivatedRoute, private mediaService: MediaService,
    private dateService: DateService) {
    super(store);
  }

  ngOnInit() {
    this.subs.add(
      this.route.paramMap
        .subscribe(params => {
          this.currentMediaItemId = params.get('id');
          if (this.currentMediaItemId) {
            this.showSpinner();
            this.mediaService.getTelemetry(this.currentMediaItemId).toPromise()
              .then(telemetry => {
                if (telemetry) {
                  telemetry.feeds.forEach(feed => {
                    feed.nameWithoutExtension = this.getVideoName(feed.name);
                  });
                  this.videos = telemetry.feeds;
                  this.telemetryFiles = telemetry.files;
                }
                this.hideSpinner();
              }, err => this.showErrorMessage(err));
          }
        }),
    );
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

  onBack(route: string) {
    this.router.navigate([route]);
  }

  selectVideo(i: number) {
    this.selectedIndex = i;
    this.videoPlayers.forEach(v => v.muted = true);
    let selectedVideoPlayer = this.videoPlayers[i];
    selectedVideoPlayer.muted = false;
    const selectedVideo = this.videos[i];
    let activeFile: TelemetryFile;
    const originalCSV = this.telemetryFiles.find(f => f.name.includes(selectedVideo.nameWithoutExtension) && f.name.includes('original'));
    if (!originalCSV) {
      const currentCSV = this.telemetryFiles.find(f => f.name.includes(selectedVideo.nameWithoutExtension) && f.name.includes('current'));
      if (!currentCSV) {
        const nonOcrCSV = this.telemetryFiles.find(f => f.name.includes(selectedVideo.nameWithoutExtension) && !f.name.includes('ocr'));
        if (!nonOcrCSV) {
          const OcrCSV = this.telemetryFiles.find(f => f.name.includes(selectedVideo.nameWithoutExtension) && f.name.includes('ocr'));
          if (OcrCSV) {
            activeFile = OcrCSV;
          }
        } else {
          activeFile = nonOcrCSV;
        }
      } else {
        activeFile = currentCSV;
      }
    } else {
      activeFile = originalCSV;
    }
    if (activeFile) {
      this.telemetryFilesComponent.switchTabs(activeFile.name);
    }
  }

  //#region VIDEO CONTROLS

  onPlay() {
    if (this.isBuffering || this.currentTime >= this.maxDuration) return;
    this.isPlaying = true;
    this.playVideos();
  }

  onPause() {
    this.isPlaying = false;
    this.pauseVideos();
  }

  playVideos() {
    this.intervalId = setInterval(() => {
      if (this.currentTime === this.maxDuration) {
        clearInterval(this.intervalId);
        this.isPlaying = false;
        return;
      }
      this.currentTime++;
      this.slider.value = this.currentTime;
    }, 1000);
  }

  pauseVideos() {
    this.videoPlayers.forEach(video => {
      if (!video.paused) {
        video.pause();
      }
    });
    clearInterval(this.intervalId);
  }

  onFullscreen(index: number) {
    let selectedVideoPlayer = this.videoPlayers[index];
    selectedVideoPlayer.requestFullscreen();
  }

  //#endregion

  //#region SLIDER

  onCreated() {
    this.sliderTrack = document.getElementById('height_slider').querySelector('.e-range');
    this.sliderHandle = document.getElementById('height_slider').querySelector('.e-handle');
    (this.sliderHandle as HTMLElement).style.backgroundColor = '#ffffff';
    (this.sliderTrack as HTMLElement).style.backgroundColor = '#0097a9';
  }

  onSliderChanged(args: SliderChangeEventArgs) {
    const { value, previousValue } = args;
    this.isReallyPlaying = (Number(value) - Number(previousValue)) === 1;
    this.currentTime = Number(value);
    if (this.currentTime >= this.maxDuration) {
      return;
    }
    this.prepVideos();
  }

  tooltipChangeHandler(args: SliderTooltipEventArgs): void {
    args.text = this.dateService.convertSecondsToTimeString(Number(args.value));
  }

  //#endregion

  //#region VIDEO EVENTS

  onDurationChange(args: any) {
    if (args) {
      const { duration } = args.target;
      this.maxDuration = duration > this.maxDuration ? duration : this.maxDuration;
    }
  }

  onCanPlay() {
    if (this.canPlayCount < this.videoPlayers.length) this.canPlayCount++;
    if (this.canPlayCount === this.videoPlayers.length) {
      this.isBuffering = false;
      if (this.selectedIndex === undefined) {
        this.selectVideo(0);
      }
      this.videoPlayers.forEach(v => v.muted = true);
      this.videoPlayers[0].muted = false;
    }
  }

  prepVideos() {
    this.videoPlayers.forEach((videoPlayer, index) => {
      if (!videoPlayer.duration || videoPlayer.duration < this.currentTime) {
        if (videoPlayer.src) {
          videoPlayer.pause();
        }
        return;
      }

      if (!this.isReallyPlaying) {
        videoPlayer.currentTime = this.currentTime;
      } else {
        const diff = this.currentTime - videoPlayer.currentTime;
        if (diff > 1 || diff < 0) {
          videoPlayer.currentTime = this.currentTime;
        }
      }

      if (this.isPlaying && videoPlayer.paused) {
        // videoPlayer.load();
        let playPromise = videoPlayer.play();
        if (playPromise !== undefined) {
          playPromise.then(function () {
          }).catch(function (error) {
            console.log("gotoVideo Exception: ", error);
          });
        }
      }
    });
  }

  onVideoEnded(index: number) {
    console.log("onVideoEnded: ", index);
    let videoPlayer = this.videoPlayers[index];
    if (!videoPlayer.paused) {
      videoPlayer.pause();
    }
  }

  //#endregion

  onDataRowSelected(data: number) {
    this.slider.value = data;
  }

  private getVideoName(fullName: string) {
    return fullName.split("@")[0];
  }
}
