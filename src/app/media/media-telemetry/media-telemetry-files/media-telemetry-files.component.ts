import { Component, OnInit, Input, AfterViewInit, Output, EventEmitter, ViewChild } from '@angular/core';
import { TelemetryFile } from 'src/app/core/models/entity/telemetry';
import { Tab } from 'src/app/core/models/tab';
import { TabsComponent } from 'src/app/shared/tabs/tabs.component';
import { DateService } from 'src/app/core/services/business/dates/date.service';

@Component({
  selector: 'app-media-telemetry-files',
  templateUrl: './media-telemetry-files.component.html',
  styleUrls: ['./media-telemetry-files.component.css']
})
export class MediaTelemetryFilesComponent implements OnInit {

  @Input() files: TelemetryFile[] = [];
  @Output() rowSelected = new EventEmitter<number>();

  @ViewChild('tabsComponent') tabsComponent: TabsComponent;

  tabs: Tab[] = [];
  currentFile: TelemetryFile;

  constructor(private dateService: DateService) { }

  ngOnInit() {
    if (this.files) {
      this.currentFile = this.files[0];
      this.setColumns();
      this.files.forEach((file, index) => {
        const tab: Tab = { link: index, disabled: file.data.length === 0, name: file.name, isActive: index === 0, action: this.switchTabs.bind(this, file.name) };
        this.tabs = [...this.tabs, tab];
      });
    }
  }

  switchTabs(name: any) {
    this.currentFile = this.files.find(f => f.name === name);
    const isTabWithData = this.setColumns();
    if (isTabWithData) {
      let currentTab = this.tabs.find(x => x.name === name);
      if (currentTab) {
        this.tabs.forEach(t => t.isActive = false);
        currentTab.isActive = true;
      }
      this.tabsComponent.checkTabIsInView();
    }
  }

  onRowClicked(data: any) {
    const asfIndex = data.ASFIndex || data.asfindex || data.AsfIndex;
    if (!asfIndex) return;
    let seconds = this.dateService.convertTimeStringToSeconds(asfIndex);
    console.log("seconds: ", seconds);
    this.rowSelected.emit(seconds);
  }

  private setColumns(): boolean {
    if (this.currentFile) {
      if (this.currentFile.data.length > 0) {
        this.currentFile.columns = Object.keys(this.currentFile.data[0]);
        return true;
      } else {
        this.currentFile.columns = [];
      }
    }
    return false;
  }
}
