import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MediaTelemetryFilesComponent } from './media-telemetry-files.component';

describe('MediaTelemetryFilesComponent', () => {
  let component: MediaTelemetryFilesComponent;
  let fixture: ComponentFixture<MediaTelemetryFilesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MediaTelemetryFilesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MediaTelemetryFilesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
