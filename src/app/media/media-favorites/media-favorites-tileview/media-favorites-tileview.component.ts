import { Component, OnInit, OnDestroy } from '@angular/core';
import { MediaState } from '../../state/media/media.state';
import { Select, Store } from '@ngxs/store';
import { MediaItem } from 'src/app/core/models/entity/media';
import { GetFavorites, RemoveFavorite, AddSelectedMediaItem, RemoveSelectedMediaItem, ClearSelectedMediaItems, SetSavedFilterAction, SetBreadcrumbs, AddFilterTags, SetFilterTags } from '../../state/media/media.action';
import { Router, ActivatedRoute } from '@angular/router';
import { Tag } from 'src/app/core/models/entity/tag';
import { Breadcrumb } from 'src/app/core/models/breadcrumb';
import { SubSink } from 'subsink/dist/subsink';
import { Observable } from 'rxjs/internal/Observable';
import { FieldsService } from 'src/app/core/services/business/fields/fields.service';
import { ShowWarningMessage, ShowSpinner } from 'src/app/state/app.actions';

@Component({
  selector: 'app-media-favorites-tileview',
  templateUrl: './media-favorites-tileview.component.html',
  styleUrls: ['./media-favorites-tileview.component.css']
})
export class MediaFavoritesTileviewComponent implements OnInit, OnDestroy {

  private subs = new SubSink();
  currentPage: number;
  pageSize = 8;
  totalMedia: number;
  pageCount = 5;
  selectedItems: MediaItem[];

  @Select(MediaState.getFavorites) favoriteMedia$: Observable<MediaItem[]>;
  @Select(MediaState.getTotalMedia) totalMedia$: Observable<number>;
  @Select(MediaState.isFilterCleared) isFilterCleared$: Observable<boolean>;
  @Select(MediaState.isMediaSelectionCleared) isMediaSelectionCleared$: Observable<boolean>;
  @Select(MediaState.getSelectedItems) selectedItems$: Observable<MediaItem[]>;

  constructor(private store: Store, private router: Router, private route: ActivatedRoute, private fieldsService: FieldsService) { }

  ngOnInit() {
    this.subs.add(
      this.route.queryParams
        .subscribe(params => {
          const { pageNumber } = params;
          this.currentPage = pageNumber || 1;
        }),
      this.totalMedia$
        .subscribe(totalMedia => {
          this.totalMedia = totalMedia;
        }),
      this.selectedItems$
        .subscribe(items => this.selectedItems = items)
    );
  }

  ngOnDestroy() {
    this.store.dispatch(new ClearSelectedMediaItems());
    this.subs.unsubscribe();
  }

  tileClickEvent(item: MediaItem) {    
    if (item.isSearch) {
      const isValidJson = this.fieldsService.isValidJson(item.metadata);
      if (!isValidJson) {
        this.store.dispatch(new ShowWarningMessage("Unable to render saved filter."));
        return;
      }
      this.store.dispatch(new ShowSpinner());
      const tags = this.buildFilterTags(item.metadata);
      this.store.dispatch(new SetFilterTags(tags)).toPromise()
        .then(() => {
          this.router.navigate([`media/all`]);
        });
    } else {
      let crumbs: Breadcrumb[] = [
        { link: '/media/all', name: 'Media' },
        { link: '/media/favorites', name: 'Favorites' },
        { name: item.name }
      ];
      this.store.dispatch(new SetBreadcrumbs(crumbs));
      this.router.navigate([`media/${item.documentId}/details`]);
    }
  }

  toggleCheckBoxEvent(item: any) {
    if (!this.selectedItems.includes(item)) {
      this.store.dispatch(new AddSelectedMediaItem(item));
    } else {
      this.store.dispatch(new RemoveSelectedMediaItem(item));
    }
  }

  removeFavoriteEvent(item: MediaItem) {
    this.store.dispatch(new RemoveFavorite(item.favoriteId)).toPromise()
      .then(() => {
        this.store.dispatch(new GetFavorites(this.currentPage, this.pageSize));
      });
  }

  private buildFilterTags(data: string): Tag[] {
    return JSON.parse(data) as Tag[];
  }
}
