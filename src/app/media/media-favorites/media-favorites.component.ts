import { BaseComponent } from './../../shared/base/base.component';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Store, Select } from '@ngxs/store';
import { ViewType } from 'src/app/core/enum/view-type';
import { SubSink } from 'subsink/dist/subsink';
import { GetFavorites } from '../state/media/media.action';
import { MediaState } from '../state/media/media.state';
import { Tag } from 'src/app/core/models/entity/tag';
import { Observable } from 'rxjs/internal/Observable';

@Component({
  selector: 'app-media-favorites',
  templateUrl: './media-favorites.component.html',
  styleUrls: ['./media-favorites.component.css']
})
export class MediaFavoritesComponent extends BaseComponent implements OnInit, OnDestroy {

  @Select(MediaState.getFilterTags) getFilterTags$: Observable<Tag[]>;

  private subs = new SubSink();
  viewType: string;
  currentPage = 1;
  pageSize = 8;
  filterTags: Tag[];

  constructor(protected store: Store, private route: ActivatedRoute) {
    super(store);
    this.setPageTitle("Favorite Media");
  }

  ngOnInit() {
    this.subs.add(
      this.getFilterTags$
        .subscribe(filterTags => {
          this.filterTags = filterTags;
        }),
      this.route.queryParams
        .subscribe(params => {
          const { view, pageNumber } = params;
          this.viewType = view ? view : ViewType.TILE;
          this.currentPage = Number(pageNumber) || 1;
          this.pageSize = this.viewType === ViewType.TILE ? 8 : 12;

          const queryFilterName = 'queryFilter';
          const queryFilter = this.filterTags.find(t => t.name === queryFilterName);
          const query = queryFilter ? queryFilter.value : '';
          this.store.dispatch(new GetFavorites(this.currentPage, this.pageSize, query));
        })
    )
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }
}
