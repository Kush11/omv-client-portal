import { GetFavorites, GetMedia, AddSelectedMediaItem, RemoveSelectedMediaItem, RemoveFavorite, ClearFilterTags, AddFilterTag, ShowFilters, ClearSelectedMediaItems, SetBreadcrumbs, SetSelectedMediaItems, AddSelectedMediaItems, RemoveSelectedMediaItems, SetFilterTags, DownloadMediaItem } from '../../state/media/media.action';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { GridColumn, GridColumnButton } from "../../../core/models/grid.column";
import { Select, Store } from "@ngxs/store";
import { Observable, Subject } from "rxjs";
import { MediaState } from "../../state/media/media.state";
import { MediaItem } from "../../../core/models/entity/media";
import { Router, ActivatedRoute } from "@angular/router";
import { takeUntil } from 'rxjs/operators';
import { Tag } from 'src/app/core/models/entity/tag';
import { Breadcrumb } from 'src/app/core/models/breadcrumb';
import { SubSink } from 'subsink/dist/subsink';
import { ShowWarningMessage, ShowSpinner } from 'src/app/state/app.actions';
import { FieldsService } from 'src/app/core/services/business/fields/fields.service';
declare var require: any;

@Component({
  selector: 'app-media-favorites-listview',
  templateUrl: './media-favorites-listview.component.html',
  styleUrls: ['./media-favorites-listview.component.css']
})

export class MediaFavoritesListviewComponent implements OnInit, OnDestroy {

  @Select(MediaState.getFavorites) favoriteMedia$: Observable<MediaItem[]>;
  @Select(MediaState.getTotalMedia) totalMedia$: Observable<number>;
  @Select(MediaState.isFilterCleared) isFilterCleared$: Observable<boolean>;
  @Select(MediaState.isMediaSelectionCleared) isMediaSelectionCleared$: Observable<boolean>;
  @Select(MediaState.getFavoriteActionSuccess) favoriteActionSuccess$: Observable<boolean>;
  @Select(MediaState.getSelectedItems) selectedItems$: Observable<MediaItem[]>;

  private subs = new SubSink();
  actions: GridColumnButton[] = [
    { type: 'button', text: 'Download', visible: true, action: this.downloadGridEvent.bind(this), right: 45 },
    { type: 'icon', icon: 'pencil', visible: true, action: this.onItemClicked.bind(this) }
  ];
  columns: GridColumn[] = [
    { type: "checkbox", headerText: "Select All", width: 70, field: "" },
    { headerText: "Type", field: "type", width: 95 },
    { headerText: '', type: 'favorite', field: "isFavorite", width: 60, action: this.removeFavoriteEvent.bind(this) },
    { headerText: "Name", field: "nameWithoutExtension", useAsLink: true },
    { headerText: "Date", field: "modifiedOn", type: 'date', format: 'MMM dd, yyyy  hh:mm', width: 200 },
    { custom: true, type: 'buttons', buttons: this.actions, width: 200 }
  ];
  currentPage = 1;
  pageSize = 12;
  totalMedia: number;
  pageCount = 5;
  selectedIds: any[];

  constructor(private store: Store, private router: Router, private route: ActivatedRoute, private fieldsService: FieldsService) { }

  ngOnInit() {
    this.subs.add(
      this.route.queryParams
        .subscribe(params => {
          const { pageNumber } = params;
          this.currentPage = Number(pageNumber) || 1;
        }),
      this.selectedItems$
        .subscribe(items => this.selectedIds = items.map(i => i.documentId))
    );
  }

  ngOnDestroy() {
    this.store.dispatch(new ClearSelectedMediaItems());
    this.subs.unsubscribe();
  }

  downloadGridEvent(data: MediaItem) {
    if (!data.isSearch) {
      this.store.dispatch(new DownloadMediaItem(data.name, data.url));
    }
  }

  rowSelectedEvent(items: MediaItem[]) {
    this.store.dispatch(new AddSelectedMediaItems(items));
  }

  rowDeselectedEvent(items: MediaItem[]) {
    this.store.dispatch(new RemoveSelectedMediaItems(items));
  }

  removeFavoriteEvent(item: MediaItem) {
    this.store.dispatch(new RemoveFavorite(item.favoriteId)).toPromise()
      .then(() => {
        this.store.dispatch(new GetFavorites(this.currentPage, this.pageSize));
      });
  }

  onItemClicked(item: MediaItem) {
    if (item.isSearch) {
      const isValidJson = this.fieldsService.isValidJson(item.metadata);
      if (!isValidJson) {
        this.store.dispatch(new ShowWarningMessage("Unable to render saved filter."));
        return;
      }
      this.store.dispatch(new ShowSpinner());
      const tags = this.buildFilterTags(item.metadata);
      this.store.dispatch(new SetFilterTags(tags)).toPromise()
        .then(() => {
          this.router.navigate([`media/all`]);
        });
    } else {
      this.router.navigate([`media/${item.documentId}/details`]);
    }
  }

  private buildFilterTags(data: string): Tag[] {
    return JSON.parse(data) as Tag[];
  }
}
