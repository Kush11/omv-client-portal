import { MediaRoutingModule } from './media.routing.module';
import { MediaComponent } from "../media/media.component";
import { MediaStreamingArchiveComponent } from "../media/media-streaming-archive/media-streaming-archive.component";
import { NgModule, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from "@angular/core";
import { NgxsModule } from '@ngxs/store';
import { MediaFavoritesComponent } from './media-favorites/media-favorites.component';
import { SharedModule } from '../shared/shared.module';
import { environment } from 'src/environments/environment';
import { MediaFavoritesListviewComponent } from './media-favorites/media-favorites-listview/media-favorites-listview.component';
import { MediaFavoritesTileviewComponent } from './media-favorites/media-favorites-tileview/media-favorites-tileview.component';
import { MediaState } from './state/media/media.state';
import { MediaUploadComponent } from './media-upload/media-upload.component';
import { MediaItemHistoryComponent } from './media-item/media-item-history/media-item-history.component';
import { MediaItemDetailsComponent } from './media-item/media-item-details/media-item-details.component';
import { MediaDataService } from "../core/services/data/media/media.data.service";
import { MediaMockDataService } from "../core/services/data/media/media.mock.data.service";
import { MediaItemRelatedFilesComponent } from './media-item/media-item-related-files/media-item-related-files.component';
import { MediaItemComponent } from './media-item/media-item.component';
import { MediaWebDataService } from '../core/services/data/media/media.web.data.service';
import { MetadataFieldsDataService } from '../core/services/data/metadata-fields/metadata-fields.data.service';
import { MetadataFieldsMockDataService } from '../core/services/data/metadata-fields/metadata-fields.mock.service';
import { MetadataFieldsWebDataService } from '../core/services/data/metadata-fields/metadata-fields.web.data.service';
import { TreeViewModule } from '@syncfusion/ej2-angular-navigations';
import { CheckBoxModule } from '@syncfusion/ej2-angular-buttons';
import { DialogModule } from '@syncfusion/ej2-angular-popups';
import { DropDownListModule } from '@syncfusion/ej2-angular-dropdowns';
import { ListViewModule } from '@syncfusion/ej2-angular-lists';
import { ButtonModule } from '@syncfusion/ej2-angular-buttons';
import { MediaAllComponent } from './media-all/media-all.component';
import { MediaAllListviewComponent } from './media-all/media-all-listview/media-all-listview.component';
import { MediaAllMapviewComponent } from './media-all/media-all-mapview/media-all-mapview.component';
import { MediaAllTileviewComponent } from './media-all/media-all-tileview/media-all-tileview.component';
import { MediaAllTreeviewComponent } from './media-all/media-all-treeview/media-all-treeview.component';
import { DirectoryDataService } from '../core/services/data/directory/directory.data.service';
import { DirectoryMockDataService } from '../core/services/data/directory/directory.mock.data.service';
import { DirectoryWebDataService } from '../core/services/data/directory/directory.web.data.service';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';
import { LeafletMarkerClusterModule } from '@asymmetrik/ngx-leaflet-markercluster';
import { MediaPlaylistComponent } from './media-playlist/media-playlist.component';
import { MediaViewerComponent } from './media-viewer/media-viewer.component';
import { MediaSyncModeComponent } from './media-sync-mode/media-sync-mode.component';
import { DirectoryService } from '../core/services/business/directory/directory.service';
import { MediaService } from '../core/services/business/media/media.service';
import { MetadataFieldsService } from '../core/services/business/metadata-fields/metadata-fields.service';
import { FiltersWebDataService } from '../core/services/data/filters/filters.web.data.service';
import { FiltersMockDataService } from '../core/services/data/filters/filters.mock.data.service';
import { FiltersDataService } from '../core/services/data/filters/filters.data.service';
import { FiltersService } from '../core/services/business/filters/filters.service';
import { MediaTelemetryComponent } from './media-telemetry/media-telemetry.component';
import { LookupService } from '../core/services/business/lookup/lookup.service';
import { LookupDataService } from '../core/services/data/lookup/lookup.data.service';
import { LookupMockDataService } from '../core/services/data/lookup/lookup.mock.data.service';
import { LookupWebDataService } from '../core/services/data/lookup/lookup.web.data.service';
import { SliderModule } from '@syncfusion/ej2-angular-inputs';
import { DailyVideoScheduleComponent } from './media-streaming-archive/daily-video-schedule/daily-video-schedule.component';
import { HourlyVideoScheduleComponent } from './media-streaming-archive/hourly-video-schedule/hourly-video-schedule.component';
import { DateRangePickerModule } from '@syncfusion/ej2-angular-calendars';
import { MediaTelemetryFilesComponent } from './media-telemetry/media-telemetry-files/media-telemetry-files.component';
import { TreeGridAllModule } from '@syncfusion/ej2-angular-treegrid';
import { DataService } from './media-upload/data-service';
import { MediaAllTreeViewService } from './media-all/media-all-treeview/media-all-treeview.service';
import { MediaDirectoriesComponent } from './media-directories/media-directories.component';

@NgModule({
	declarations: [
		MediaAllComponent,
		MediaAllListviewComponent,
		MediaAllMapviewComponent,
		MediaAllTreeviewComponent,
		MediaAllTileviewComponent,
		MediaComponent,
		MediaFavoritesComponent,
		MediaFavoritesListviewComponent,
		MediaFavoritesTileviewComponent,
		MediaStreamingArchiveComponent,
		MediaUploadComponent,
		MediaItemComponent,
		MediaItemDetailsComponent,
		MediaItemHistoryComponent,
		MediaItemRelatedFilesComponent,
		MediaPlaylistComponent,
		MediaViewerComponent,
		MediaSyncModeComponent,
		MediaTelemetryComponent,
		DailyVideoScheduleComponent,
		HourlyVideoScheduleComponent,
		MediaTelemetryFilesComponent,
		MediaDirectoriesComponent,

	],
	imports: [
		SharedModule,
		MediaRoutingModule,
		TreeViewModule,
		CheckBoxModule,
		DialogModule,
		DropDownListModule,
		ListViewModule,
		NgxsModule.forFeature([MediaState]),
		LeafletModule.forRoot(),
		LeafletMarkerClusterModule.forRoot(),
		CheckBoxModule,
		ButtonModule,
		SliderModule,
		DateRangePickerModule,
		TreeGridAllModule
	],
	providers: [
		DataService,
		MediaAllTreeViewService,
		DirectoryService,
		{ provide: DirectoryDataService, useClass: environment.useMocks ? DirectoryMockDataService : DirectoryWebDataService },
		MediaService,
		{ provide: MediaDataService, useClass: environment.useMocks ? MediaMockDataService : MediaWebDataService },
		MetadataFieldsService,
		{ provide: MetadataFieldsDataService, useClass: environment.useMocks ? MetadataFieldsMockDataService : MetadataFieldsWebDataService },
		FiltersService,
		{ provide: FiltersDataService, useClass: environment.useMocks ? FiltersMockDataService : FiltersWebDataService },
		LookupService,
		{ provide: LookupDataService, useClass: environment.useMocks ? LookupMockDataService : LookupWebDataService },
	],
	schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA]
})
export class MediaModule { }
