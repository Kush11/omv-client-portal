import { BaseComponent } from './../shared/base/base.component';
import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Tab } from '../core/models/tab';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { Store, Select } from '@ngxs/store';
import { MediaState } from './state/media/media.state';
import { filter } from 'rxjs/operators';
import { SetPreviousRoute, ShowWarningMessage, GetAzureSASToken } from '../state/app.actions';
import {
  ClearSelectedMediaItems, SetBreadcrumbs, DownloadMediaItems,
  AddFilterTag, RemoveFilterTag, ClearFilterTags, AddFavorite, GetStreamingArchiveAssets,
  GetStreamingArchiveGroups, GetAvailableArchiveDates, SetFilterTags
} from './state/media/media.action';
import { MediaItem } from '../core/models/entity/media';
import { Breadcrumb } from '../core/models/breadcrumb';
import { SubSink } from 'subsink/dist/subsink';
import { ToolBar } from '../core/models/toolbar-action';
import { Observable } from 'rxjs/internal/Observable';
import { Tag } from '../core/models/entity/tag';
import { FieldConfiguration, FieldType } from '../shared/dynamic-components/field-setting';
import { FiltersComponent } from '../shared/filters/filters.component';
import { Lookup } from '../core/models/entity/lookup';
import { FieldsService } from '../core/services/business/fields/fields.service';
import { ArchiveMode } from '../core/enum/archive-mode';
import { DateService } from '../core/services/business/dates/date.service';
import { ModalComponent } from '../shared/modal/modal.component';
import { FormGroup } from '@angular/forms';
import { Filter } from '../core/models/entity/filter';
import { MetadataDataType } from '../core/enum/metadata-data-type';
import { MetadataSearchType } from '../core/enum/metadata-search-type';

const ALL_LINK = '/media/all';
const FAVORITES_LINK = '/media/favorites';
const ARCHIVE_LINK = '/media/streaming-archive';
const DIRECTORIES_LINK = '/media/directories';

@Component({
  selector: 'app-media',
  templateUrl: './media.component.html',
  styleUrls: ['./media.component.css']
})
export class MediaComponent extends BaseComponent implements OnInit, OnDestroy {

  @Select(MediaState.getTotalMedia) totalMedia$: Observable<number>;
  @Select(MediaState.getSelectedItems) selectedItems$: Observable<MediaItem[]>;
  @Select(MediaState.getSelectedVideos) selectedVideos$: Observable<MediaItem[]>;
  @Select(MediaState.getBreadcrumbs) breadcrumbs$: Observable<Breadcrumb[]>;
  // filters
  @Select(MediaState.getFilters) filters$: Observable<Filter[]>;
  @Select(MediaState.getFilterTags) filterTags$: Observable<Tag[]>;
  @Select(MediaState.getArchiveFilterTags) archiveFilterTags$: Observable<Tag[]>;
  @Select(MediaState.showFilterMenu) showFilterMenu$: Observable<boolean>;
  @Select(MediaState.showArchiveFilterMenu) showArchiveFilterMenu$: Observable<boolean>;
  @Select(MediaState.isFilterCleared) isFilterCleared$: Observable<boolean>;
  @Select(MediaState.isArchiveFilterCleared) isArchiveFilterCleared$: Observable<boolean>;
  @Select(MediaState.isSavedFilterApplied) isSavedFilterApplied$: Observable<boolean>;
  @Select(MediaState.isSavedArchiveFilterApplied) isSavedArchiveFilterApplied$: Observable<boolean>;
  @Select(MediaState.getStreamingArchiveMode) archiveMode$: Observable<ArchiveMode>;
  @Select(MediaState.getStreamingArchiveAssets) streamingArchiveAssets$: Observable<Lookup[]>;
  @Select(MediaState.getStreamingArchiveGroups) streamingArchiveGroups$: Observable<Lookup[]>;
  @Select(MediaState.getStreamingArchiveDays) streamingArchiveDays$: Observable<string[]>;

  @ViewChild('filters') filters: FiltersComponent;
  @ViewChild('saveToFavoritesModal') saveToFavoritesModal: ModalComponent;

  private subs = new SubSink();
  selectedItems: any[] = [];
  showTreeView: boolean;
  showMapView: boolean;
  mediaBreadcrumbs: Breadcrumb[] = [];
  lastBreadcrumb: Breadcrumb;
  isVideoInSelectedItems: boolean;
  selectedVideoItems: any[] = [];
  selectedVideos: MediaItem[] = [];
  filterMode = 'default';
  filterTags: Tag[] = [];
  archiveFilterFields: FieldConfiguration[] = [
    { type: FieldType.Select, name: 'asset', label: 'Asset', options: [], cssClass: 'col-md-3', placeholder: `Please select`, order: 0 },
    { type: FieldType.Select, name: 'group', label: 'Group', options: [], cssClass: 'col-md-3', placeholder: `Please select`, order: 1 },
    { type: FieldType.Date, name: 'date', label: 'Date', cssClass: 'col-md-3', placeholder: `Date`, order: 2, allowDateDisabling: true },
    { type: FieldType.Time, name: 'time', label: 'Time', cssClass: 'col-md-3', placeholder: `Time`, order: 3 }
  ];
  showFilterMenu: boolean;
  showArchiveFilterMenu: boolean;
  isStreamingArchive: boolean = false;
  isFavoriteSearch: boolean = false;
  isDirectories: boolean = false;
  isMap: boolean = false;
  mediaTabs: Tab[] = [
    { link: '/media/all', query: 'tile', name: 'All Media', isActive: true },
    { link: '/media/directories', query: 'tile', name: 'Directories' },
    { link: '/media/favorites', query: 'tile', name: 'Favorites' },
    { link: '/media/streaming-archive', query: 'tile', name: 'Streaming Archive' }

  ];
  toolbarItems: ToolBar[] = [
    { text: 'Sync Videos', disabled: true, cssClass: 'sync-video-qa', action: this.addToVideoSyncModePlaylist.bind(this) },
    { text: 'Download All', disabled: true, cssClass: 'download-all-qa', action: this.downloadAll.bind(this) },
    { text: 'Add to Playlist', disabled: true, cssClass: 'add-to-playlist-qa', action: this.addToPlaylist.bind(this) },
  ];
  streamArchiveToolBar: ToolBar[] = [{ text: 'Daily Video Schedule', disabled: false }];
  activeView = 'tile';
  currentRoute: any;
  searchQuery = '';

  //#region Save as Favorite

  favoriteName: FieldConfiguration[] = [
    { type: 'input', label: 'Name', inputType: 'text', name: 'name', required: true, validationMessage: 'Please enter a name.' }
  ];

  //#endregion

  //#region Streaming Archive

  currentArchiveMode: ArchiveMode;
  currentAsset: string;
  currentGroup: string;
  currentDate: string;
  currentTime: string;
  isDailyMode: boolean;
  isHourlyMode: boolean;
  showArchiveFilters: boolean;
  hideMediaTabs: any;
  queryAsset: any;
  queryGroup: any;
  queryDate: any;
  archiveFilterTags: Tag[];

  //#endregion

  //#region favorite search
  currentPage: number;
  //#endregion

  constructor(protected store: Store, private router: Router, private route: ActivatedRoute,
    private fieldsService: FieldsService, private dateService: DateService) {
    super(store);
    this.hideLeftNav();
  }

  async ngOnInit() {
    const query = this.route.snapshot.queryParamMap.get('filter');
    if (query === 'collaborations') {
      const tag: Tag = { name: 'MediaTypeName', value: 'Collaboration', label: '', dataType: MetadataDataType.String, searchType: MetadataSearchType.Checkbox };
      await this.store.dispatch(new SetFilterTags([tag])).toPromise();
    }

    if (!this.tenantPermission.isPermittedMedia) {
      this.router.navigate([`/unauthorize`]);
    }
    this.isStreamingArchive = this.router.url.includes('/streaming-archive');
    this.isFavoriteSearch = this.router.url.includes('/favorites');
    this.isDirectories = this.router.url.includes('/directories');
    this.isMap = this.router.url.includes('view=map');
    this.setViewsByUrl(this.router.url);
    this.setBreadCrumbByUrl(this.router.url);
    this.store.dispatch(new GetAzureSASToken());

    if (this.isStreamingArchive) {
      this.store.dispatch(new GetStreamingArchiveAssets());
      this.store.dispatch(new GetStreamingArchiveGroups());
      this.store.dispatch(new GetAvailableArchiveDates());
    }

    this.subs.add(
      this.route.queryParams
        .subscribe(params => {
          const { view, asset, group, date, time, page } = params;
          this.currentPage = Number(page) || 1;
          this.activeView = view ? view : 'tile';
          this.showArchiveFilters = !asset && !group && !date;
          this.hideMediaTabs = asset && group && date && time;
          this.queryAsset = asset;
          this.queryGroup = group;
          this.queryDate = date;
          if (asset && group && date && !time) {
            const isDateValid = this.dateService.isValid(date, 'MM-DD-YYYY');
            if (!isDateValid) {
              this.store.dispatch(new ShowWarningMessage('Date Filter is invalid.'));
              this.router.navigate(['/media/streaming-archive']);
              return;
            }
            this.currentArchiveMode = ArchiveMode.Daily;
          } else if (asset && group && date && time) {
            const isDateValid = this.dateService.isValid(date, 'MM-DD-YYYY');
            const isTimeValid = this.dateService.isValid(time, 'hh:mm a');
            if (!isDateValid && !isTimeValid) {
              this.store.dispatch(new ShowWarningMessage('Date Filter and Time Filter are invalid.'));
              this.router.navigate(['/media/streaming-archive']);
              return;
            } else if (!isDateValid) {
              this.store.dispatch(new ShowWarningMessage('Date Filter is invalid.'));
              this.router.navigate(['/media/streaming-archive']);
              return;
            } else if (!isTimeValid) {
              this.store.dispatch(new ShowWarningMessage('Time Filter is invalid.'));
              this.router.navigate(['/media/streaming-archive']);
              return;
            }
            this.currentArchiveMode = ArchiveMode.Hourly;
          } else {
            this.currentArchiveMode = null;
          }
          this.setBreadCrumbByUrl('');
        }),
      this.router.events
        .pipe(
          filter(event => event instanceof NavigationEnd))
        .subscribe((event: NavigationEnd) => {
          this.isStreamingArchive = event.url.includes('/streaming-archive');
          this.isDirectories = event.url.includes('/directories');
          this.isFavoriteSearch = event.url.includes('/favorites');
          this.isMap = this.router.url.includes('view=map');

          this.setViewsByUrl(event.url);
          this.setBreadCrumbByUrl(event.url);
        }),
      this.selectedItems$
        .subscribe((data) => {
          this.selectedItems = data;
          this.setToobarProperties();
        }),
      this.selectedVideos$
        .subscribe(playlist => {
          this.selectedVideos = playlist;
          this.setToobarProperties();
        }),

      this.showFilterMenu$
        .subscribe(show => this.showFilterMenu = show),
      this.showArchiveFilterMenu$
        .subscribe(show => this.showArchiveFilterMenu = show),
      this.filterTags$
        .subscribe(tags => {
          this.filterTags = tags;
        }),
      this.archiveFilterTags$
        .subscribe(tags => this.archiveFilterTags = tags),
      this.streamingArchiveAssets$
        .subscribe(assets => {
          this.archiveFilterFields[0].options = this.fieldsService.convertLookupsToListItems(assets);
        }),
      this.streamingArchiveGroups$
        .subscribe(groups => {
          this.archiveFilterFields[1].options = this.fieldsService.convertLookupsToListItems(groups);
        }),
      this.streamingArchiveDays$
        .subscribe(days => {
          this.archiveFilterFields[2].disabledDates = days;
        })
    );
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
    this.store.dispatch(new ClearSelectedMediaItems());
    this.store.dispatch(new SetPreviousRoute('media/all'));
  }

  //#region Filters

  addTag(tag: Tag) {
    this.store.dispatch(new AddFilterTag(tag, false, this.isFavoriteSearch));
  }

  addArchiveTag(tag: Tag) {
    this.store.dispatch(new AddFilterTag(tag, true, this.isFavoriteSearch));
  }

  removeTag(tag: Tag) {
    this.store.dispatch(new RemoveFilterTag(tag, false, this.isFavoriteSearch));
  }

  removeArchiveTag(tag: Tag) {
    this.store.dispatch(new RemoveFilterTag(tag, true, this.isFavoriteSearch));
  }

  onFilterSearch(query: string) {
    const tag: Tag = { name: 'queryFilter', label: '', value: query, cssClass: 'tag-search' };
    (document.getElementById('search-input') as HTMLInputElement).value = '';
    this.store.dispatch(new AddFilterTag(tag, false, this.isFavoriteSearch, this.currentPage));
  }

  showSaveToFavoritesModal() {
    this.saveToFavoritesModal.reset();
    this.saveToFavoritesModal.show();
  }

  saveToFavorites(form: FormGroup) {
    if (form.valid) {
      if (form.dirty) {
        const name = form.value.name;
        this.store.dispatch(new AddFavorite(null, name, this.filterTags));
      }
    }
  }

  applyArchiveFilters(tags: Tag[]) {
    const asset = tags.find(tag => tag.name === 'asset');
    const group = tags.find(tag => tag.name === 'group');
    const date = tags.find(tag => tag.name === 'date');
    if (!asset && !group && !date) {
      this.store.dispatch(new ShowWarningMessage('Please select an asset, a group and a date to continue.'));
    } else if (!asset && !group) {
      this.store.dispatch(new ShowWarningMessage('Please select an asset and a group to continue.'));
    } else if (!asset && !date) {
      this.store.dispatch(new ShowWarningMessage('Please select an asset and a date to continue.'));
    } else if (!group && !date) {
      this.store.dispatch(new ShowWarningMessage('Please select a group and a date to continue.'));
    } else if (!asset) {
      this.store.dispatch(new ShowWarningMessage('Please select an asset to continue.'));
    } else if (!group) {
      this.store.dispatch(new ShowWarningMessage('Please select a group to continue.'));
    } else if (!date) {
      this.store.dispatch(new ShowWarningMessage('Please select a date to continue.'));
    } else {
      const params: any = {};
      tags.forEach(tag => {
        params[tag.name] = tag.value;
      });
      this.router.navigate(['/media/streaming-archive'], { queryParams: params });
    }
  }

  clearArchiveFilters() {
    this.store.dispatch(new ClearFilterTags(true)).toPromise()
      .then(() => {
        if (this.filters) { this.filters.reset(); }
      });
  }

  //#endregion

  setViewsByUrl(url: string) {
    url = url.split('?')[0];
    if (url === '/media/favorites') {
      this.showTreeView = false;
      this.showMapView = false;
    } else {
      this.showTreeView = false;
      this.showMapView = true;
    }
    this.setBreadCrumbByUrl(url);
  }

  setBreadCrumbByUrl(url: string) {
    if (this.currentArchiveMode === ArchiveMode.Daily || this.currentArchiveMode === ArchiveMode.Hourly) {
      this.setStreamingArchiveBreadcrumbs();
    } else {
      url = url.split('?')[0];
      let crumbs: Breadcrumb[];
      switch (url) {
        case ALL_LINK:
          crumbs = [
            { link: '/media/all', name: 'Media' },
            { link: '/media/all', name: 'All Media' }
          ];
          this.store.dispatch(new SetBreadcrumbs(crumbs));
          break;
        case FAVORITES_LINK:
          crumbs = [
            { link: '/media/all', name: 'Media' },
            { link: '/media/favorites', name: 'Favorites' }
          ];
          this.store.dispatch(new SetBreadcrumbs(crumbs));
          break;
        case ARCHIVE_LINK:
          crumbs = [
            { link: '/media/all', name: 'Media' },
            { link: '/media/streaming-archive', name: 'Streaming Archive' }
          ];
          this.store.dispatch(new SetBreadcrumbs(crumbs));
          break;
        case DIRECTORIES_LINK:
          crumbs = [
            { link: '/media/all', name: 'Media' },
            { link: '/media/directories', name: 'Directories' }
          ];
          this.store.dispatch(new SetBreadcrumbs(crumbs));
          break;
      }
    }
  }

  setStreamingArchiveBreadcrumbs() {
    let crumbs: Breadcrumb[] = [];
    if (this.currentArchiveMode === ArchiveMode.Daily) {
      crumbs = [
        { link: '/media/all', name: 'Media' },
        { link: '/media/streaming-archive', name: 'Streaming Archive' },
        { link: '', name: 'Daily Video Schedule' }
      ];
      this.store.dispatch(new SetBreadcrumbs(crumbs));
    } else if (this.currentArchiveMode === ArchiveMode.Hourly) {
      crumbs = [
        { link: '/media/all', name: 'Media' },
        { link: '/media/streaming-archive', name: 'Streaming Archive' },
        { link: '', name: 'Daily Video Schedule', action: this.navigateToDailyVideoSchedule.bind(this) },
        { link: '', name: 'Hourly Video Schedule' }
      ];
      this.store.dispatch(new SetBreadcrumbs(crumbs));
    }
  }

  navigateToDailyVideoSchedule() {
    this.router.navigate(
      ['/media/streaming-archive'], { queryParams: { asset: this.queryAsset, group: this.queryGroup, date: this.queryDate } }
    );
  }

  uploadMedia() {
    this.store.dispatch(new SetPreviousRoute(this.router.url));
    this.router.navigate(['/media/upload']);
  }

  switchBreadCrumbs(breadcrumbLink: string) {
    this.router.navigate([breadcrumbLink]);
  }

  switchTabs(tabLink: string) {
    const isStreamingArchive = this.mediaTabs[3].link === tabLink;
    const isDirectories = this.mediaTabs[1].link === tabLink;
    if (!isStreamingArchive && !isDirectories) {
      this.router.navigate([tabLink], { queryParams: { view: 'tile' } });
    } else {
      if (isStreamingArchive) {
        this.store.dispatch(new GetStreamingArchiveAssets());
        this.store.dispatch(new GetStreamingArchiveGroups());
        this.store.dispatch(new GetAvailableArchiveDates());
      }
      this.router.navigate([tabLink]);
    }
  }

  navigateToView(view: string) {
    const url = this.router.url.split('?')[0];
    this.router.navigate([url], { queryParams: { view } });
  }

  downloadAll() {
    const filesToDownload = [...this.selectedItems];
    this.store.dispatch(new DownloadMediaItems(filesToDownload));
  }

  addToPlaylist() {
    const param = this.selectedVideos.map(x => x.documentId);
    this.router.navigate(['/media/playlist'], { queryParams: { items: param.toString() } });
  }

  addToVideoSyncModePlaylist() {
    const param = this.selectedVideos.map(x => x.documentId);
    this.router.navigate(['/media/sync-mode'], { queryParams: { items: param.toString() } });
  }

  private setToobarProperties() {
    this.toolbarItems.map((item, index) => {
      switch (index) {
        case 0:
          item.disabled = this.selectedVideos.length === 0;
          break;
        case 1:
          item.disabled = this.selectedItems.length === 0;
          break;
        case 2:
          item.disabled = this.selectedVideos.length === 0;
          break;
        default:
          break;
      }
    });
  }
}
