import { Component, OnInit, OnDestroy } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { MediaItem } from 'src/app/core/models/entity/media';
import { MediaState } from '../state/media/media.state';
import { Router, ActivatedRoute } from '@angular/router';
import { ClearPlaylist, SetPlaylist } from '../state/media/media.action';
import { SubSink } from 'subsink/dist/subsink';
import { AppState } from 'src/app/state/app.state';
import { Observable } from 'rxjs/internal/Observable';
import { MediaService } from 'src/app/core/services/business/media/media.service';
import { TranscriptSegment } from 'src/app/core/models/entity/transcript-segment';
import { BaseComponent } from 'src/app/shared/base/base.component';
import { ShowErrorMessage } from 'src/app/state/app.actions';

@Component({
  selector: 'app-media-playlist',
  templateUrl: './media-playlist.component.html',
  styleUrls: ['./media-playlist.component.css']
})
export class MediaPlaylistComponent extends BaseComponent implements OnInit, OnDestroy {

  @Select(MediaState.getSelectedVideoItems) selectedVideoItems$: Observable<MediaItem[]>;
  @Select(MediaState.getPlaylist) playlist$: Observable<MediaItem[]>;
  @Select(AppState.getPreviousRoute) previousRoute$: Observable<string>;

  private subs = new SubSink();
  currentlyPlaying: MediaItem;
  playlist: MediaItem[] = [];
  transcript: TranscriptSegment[];
  videoIds: any[] = [];
  isRemoveMode: boolean;

  constructor(protected store: Store, private router: Router, private route: ActivatedRoute,
    private mediaService: MediaService) {
    super(store);
    this.setPageTitle('Media Playlist');
  }

  ngOnInit() {
    this.subs.add(
      this.route.queryParams
        .subscribe(params => {
          const { items } = params;
          if (!this.isRemoveMode) {
            this.videoIds = items.split(',');
            this.store.dispatch(new SetPlaylist(this.videoIds));
          } else {
            this.isRemoveMode = false;
            this.playlist = this.playlist.filter(x => items.split(',').includes(x.id));
          }
        }),
      this.playlist$
        .subscribe(playlist => {
          if (playlist) {
            this.playlist = playlist;
          }
        })
    );
  }

  ngOnDestroy() {
    this.store.dispatch(new ClearPlaylist());
    this.subs.unsubscribe();
  }

  onBack(route: string) {
    this.router.navigate([route]);
  }

  onGetTranscript(mediaItem: MediaItem) {
    this.showSpinner();
    this.mediaService.getVideoTranscript(mediaItem.id)
      .then(transcript => {
        this.transcript = transcript;
        this.hideSpinner();
      }, err => this.showErrorMessage(err));
  }

  onRemoveFromPlaylist(documentId: any) {
    this.isRemoveMode = true;
    this.videoIds = this.videoIds.filter(v => v != documentId);
    if (this.videoIds.length > 0) {
      this.router.navigate(['/media/playlist'], { queryParams: { items: this.videoIds.join(',') } });
    } else {
      this.router.navigate(['/media']);
    }
  }
}
