import { Component, OnInit, ElementRef, ViewChild, AfterViewInit, OnDestroy, HostListener } from '@angular/core';
import { MediaState } from '../state/media/media.state';
import { AppState } from 'src/app/state/app.state';
import { Observable } from 'rxjs';
import { MediaItem } from 'src/app/core/models/entity/media';
import { Select, Store } from '@ngxs/store';
import { Router, ActivatedRoute } from '@angular/router';
import { SubSink } from 'subsink/dist/subsink';
import { SetPlaylist, ClearPlaylist } from '../state/media/media.action';
import { FsDocument } from 'src/app/core/models/video-element';
import { BaseComponent } from 'src/app/shared/base/base.component';
import { Slider, SliderChangeEventArgs, SliderTooltipEventArgs } from '@syncfusion/ej2-inputs';
import { DateService } from 'src/app/core/services/business/dates/date.service';
import { MediaService } from 'src/app/core/services/business/media/media.service';
import { KeyType } from 'src/app/core/enum/key-type';
declare let videojs: any;

@Component({
  selector: 'app-media-sync-mode',
  templateUrl: './media-sync-mode.component.html',
  styleUrls: ['./media-sync-mode.component.css']
})
export class MediaSyncModeComponent extends BaseComponent implements OnInit, AfterViewInit, OnDestroy {

  @Select(MediaState.getPlaylist) videos$: Observable<MediaItem[]>;
  @Select(AppState.getPreviousRoute) previousRoute$: Observable<string>;

  @ViewChild('slider') slider: Slider;
  sliderTrack: HTMLElement;
  sliderHandle: HTMLElement;

  private subs = new SubSink();
  skipBackwardIcon = './assets/images/syn-backwards-active.svg';
  playIcon = './assets/images/sync-play-active.svg';
  pauseIcon = './assets/images/playPause.svg';
  skipForwardIcon = './assets/images/syn-forward-active.svg';
  tooltip: Object = { placement: 'Before', isVisible: true, cssClass: 'e-tooltip-cutomization' };

  videos: MediaItem[] = [];
  canPlayCount = 0;
  maxDuration = 0;
  isPlaying: boolean;
  isReallyPlaying: boolean;
  isReadyToPlay: boolean;
  currentTime = 0;
  intervalId: any;
  isRemoveMode: boolean;
  isSyncMode = false;
  videoIds: any[] = [];
  activeVideoIndex = 0;
  keyCode: number;

  get videoPlayers() {
    return Array.from(document.getElementsByTagName('video')) as HTMLVideoElement[];
  }

  get videoPlayerContainers() {
    return Array.from(document.getElementsByClassName('video')) as HTMLElement[];
  }

  showContent = false;

  @ViewChild('myModal') modal: ElementRef;

  previousRoute: string;

  constructor(public store: Store, private router: Router, private route: ActivatedRoute, private dateService: DateService,
    private mediaService: MediaService) {
    super(store);
    this.store.dispatch(new ClearPlaylist());
    this.setPageTitle("Sync Player");
  }

  ngOnInit() {
    this.subs.add(
      this.route.queryParams
        .subscribe(params => {
          const items = params.items;
          if (!this.isRemoveMode) {
            this.videoIds = items.split(',');
            this.store.dispatch(new SetPlaylist(this.videoIds));
          } else {
            this.isRemoveMode = false;
            this.videos = this.videos.filter(x => items.split(',').includes(x.id));
            this.getMaxDuration();
          }
        }),
      this.videos$
        .subscribe(videos => {
          if (videos) {
            this.videos = JSON.parse(JSON.stringify(videos));
          }
        })
    );
  }

  ngOnDestroy() {
    this.store.dispatch(new ClearPlaylist());
    this.subs.unsubscribe();
  }

  ngAfterViewInit() {
    this.setVideoControls();
  }

  onBack(route: string) {
    this.router.navigate([route]);
  }

  setVideoControls() {
    const options = {
      controls: true, autoplay: false, preload: 'auto', html5: { hls: { overrideNative: true } }
    };
    this.subs.sink =
      this.videos$
        .subscribe(videos => {
          if (videos) {
            if (this.videos.length > 0) {
              this.videos.forEach((element, index) => {
                let player = document.getElementById(element.id);
                const videoPlayer = new videojs(player, options, function onPlayerReady() {
                  // videojs.log('Your player is ready!');
                  var currentPlayer = this;
                  if (index !== 0) {
                    currentPlayer.volume(0);
                  }
                });
              });
            }
          }
        });
  }

  getMaxDuration() {
    const videoPlayers = this.videoPlayerContainers.filter(v => this.videos.map(x => x.id).includes(v.id));
    let maxDuration = 0;
    videoPlayers.forEach(player => {
      const videoPlayer = player.children[0] as HTMLVideoElement;
      maxDuration = videoPlayer.duration > maxDuration ? videoPlayer.duration : maxDuration;
    });
    this.maxDuration = maxDuration;
  }

  //#region CONTROLS

  toggleSyncMode() {
    this.isSyncMode = !this.isSyncMode;
    if (this.isSyncMode) {
      this.videoPlayers.forEach(v => v.muted = true);
      let selectedVideoPlayer = this.videoPlayers[this.activeVideoIndex];
      selectedVideoPlayer.muted = false;
      selectedVideoPlayer.volume = 1;
    }
    this.onPause();
  }

  onPlay() {
    if (!this.isSyncMode) return;
    if (this.currentTime >= this.maxDuration) return;
    this.isPlaying = true;
    this.playVideos();
  }

  onPause() {
    this.isPlaying = false;
    this.pauseVideos();
  }

  playVideos() {
    this.intervalId = setInterval(() => {
      if (this.currentTime === this.maxDuration) {
        clearInterval(this.intervalId);
        this.isPlaying = false;
        return;
      }
      this.currentTime++;
      this.slider.value = this.currentTime;
    }, 1000);
  }

  pauseVideos() {
    this.videoPlayers.forEach(video => {
      if (!video.paused) {
        video.pause();
      }
    });
    clearInterval(this.intervalId);
  }

  skipBackward() {
    if (!this.isSyncMode) return;
    this.currentTime -= 10;
    if (!this.isPlaying) {
      this.slider.value = this.currentTime;
    }
  }

  skipForward() {
    if (!this.isSyncMode) return;
    this.currentTime += 10;
    if (!this.isPlaying) {
      this.slider.value = this.currentTime;
    }
  }

  //#endregion

  //#region SLIDER

  onCreated() {
    this.sliderTrack = document.getElementById('height_slider').querySelector('.e-range');
    this.sliderHandle = document.getElementById('height_slider').querySelector('.e-handle');
    (this.sliderHandle as HTMLElement).style.backgroundColor = '#ffffff';
    (this.sliderTrack as HTMLElement).style.backgroundColor = '#0097a9';
  }

  onSliderChanged(args: SliderChangeEventArgs) {
    const { value, previousValue } = args;
    this.isReallyPlaying = (Number(value) - Number(previousValue)) === 1;
    this.currentTime = Number(value);
    if (this.currentTime >= this.maxDuration) {
      return;
    }
    this.playVideosInSync();
  }

  tooltipChangeHandler(args: SliderTooltipEventArgs): void {
    args.text = this.dateService.convertSecondsToTimeString(Number(args.value));
  }

  //#endregion

  //#region VIDEO EVENTS

  onVideoSelected(index: number) {
    if (!this.isSyncMode) return;
    this.activeVideoIndex = index;
    this.videoPlayers.forEach(v => v.muted = true);
    let selectedVideoPlayer = this.videoPlayers[this.activeVideoIndex];
    selectedVideoPlayer.muted = false;
    selectedVideoPlayer.volume = 1;
  }

  onDurationChange(args: any) {
    if (args) {
      const { duration } = args.target;
      this.maxDuration = duration > this.maxDuration ? duration : this.maxDuration;
    }
  }

  onCanPlayThrough() {
    if (this.canPlayCount < this.videoPlayers.length) this.canPlayCount++;
    if (this.canPlayCount === this.videoPlayers.length) {
      this.isReadyToPlay = true;
    }
  }

  onVideoEnded(index: number) {
    let videoPlayer = this.videoPlayers[index];
    if (!videoPlayer.paused) {
      videoPlayer.pause();
    }
  }

  onVideoClicked(i: number) {
    let video = this.videoPlayers[i];
    if (video.paused) {
      video.play();
    } else {
      video.pause();
    }
  }

  playVideosInSync() {
    this.videoPlayers.forEach((videoPlayer) => {
      if (videoPlayer.duration < this.currentTime) {
        if (videoPlayer.src) {
          videoPlayer.pause();
        }
        return;
      }

      if (!this.isReallyPlaying) {
        videoPlayer.currentTime = this.currentTime;
      } else {
        const diff = this.currentTime - videoPlayer.currentTime;
        if (diff > 1 || diff < 0) {
          videoPlayer.currentTime = this.currentTime;
        }
      }

      if (this.isPlaying && videoPlayer.paused) {
        let playPromise = videoPlayer.play();
        if (playPromise !== undefined) {
          playPromise.then(function () {
          }).catch(function (error) {
            console.log("gotoVideo Exception: ", error);
          });
        }
      }
    });
  }

  //#endregion

  //#region VIDEO PLAYER

  @HostListener('document:keydown', ['$event'])
  handleKeyDown(event: KeyboardEvent) {
    this.keyCode = event.keyCode;
    event.preventDefault();
    switch (event.keyCode) {
      case KeyType.Right:
        if (this.isSyncMode) {
          this.skipForward();
        }
        break;
      case KeyType.Left:
        if (this.isSyncMode) {
          this.skipBackward();
        }
        break;
      case KeyType.SpaceBar:
        if (this.isSyncMode) {
          if (this.isPlaying) {
            this.onPause();
          } else {
            this.onPlay();
          }
        }
        break;
      case KeyType.S:
        if (this.isReadyToPlay)
          this.toggleSyncMode();
        break;
    }
  }

  toggleFavorite(i: number) {
    const video = this.videos[i];
    video.isFavorite = !video.isFavorite;
    if (video.isFavorite) {
      this.showSpinner();
      this.mediaService.addFavorite(video.id).toPromise()
        .then((favoriteId: number) => {
          this.showSuccessMessage("Video was added to your favorites items.");
          this.hideSpinner();
          video.favoriteId = favoriteId;
        }, err => {
          video.isFavorite = !video.isFavorite;
          this.showErrorMessage(err);
        });
    } else {
      this.mediaService.removeFavorite(video.favoriteId).toPromise()
        .then(() => {
          this.showSuccessMessage("Video was removed from your favorites items.");
          this.hideSpinner();
          video.favoriteId = null;
        }, err => {
          video.isFavorite = !video.isFavorite;
          this.showErrorMessage(err);
        });
    }
  }

  removeVideo(i: number) {
    const video = this.videos[i];
    this.isRemoveMode = true;
    this.videoIds = this.videoIds.filter(v => v != video.id);
    if (this.videoIds.length > 0) {
      this.router.navigate(['/media/sync-mode'], { queryParams: { items: this.videoIds.join(',') } });
    } else {
      this.router.navigate(['/media']);
    }
  }

  //#endregion

  showModal(i) {
    this.showContent = true;
  }

  hideModal() {
    this.modal.nativeElement.style.display = 'none';
  }
}

