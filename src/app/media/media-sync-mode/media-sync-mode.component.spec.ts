import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MediaSyncModeComponent } from './media-sync-mode.component';

describe('MediaSyncModeComponent', () => {
  let component: MediaSyncModeComponent;
  let fixture: ComponentFixture<MediaSyncModeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MediaSyncModeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MediaSyncModeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
