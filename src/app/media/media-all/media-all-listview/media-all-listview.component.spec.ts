import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MediaAllListviewComponent } from './media-all-listview.component';

describe('MediaAllListviewComponent', () => {
  let component: MediaAllListviewComponent;
  let fixture: ComponentFixture<MediaAllListviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MediaAllListviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MediaAllListviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
