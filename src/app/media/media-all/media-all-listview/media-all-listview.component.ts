import { Component, OnInit, OnDestroy } from '@angular/core';
import { GridColumn, GridColumnButton } from 'src/app/core/models/grid.column';
import { Store, Select } from '@ngxs/store';
import { Router, ActivatedRoute } from '@angular/router';
import { MediaState } from '../../state/media/media.state';
import { Observable } from 'rxjs/internal/Observable';
import { MediaItem } from 'src/app/core/models/entity/media';
import {
  AddFavorite, RemoveFavorite, ClearSelectedMediaItems, DownloadMediaItem, AddSelectedMediaItems,
  RemoveSelectedMediaItems, RefetchMedia
} from '../../state/media/media.action';
import { SubSink } from 'subsink/dist/subsink';

@Component({
  selector: 'app-media-all-listview',
  templateUrl: './media-all-listview.component.html',
  styleUrls: ['./media-all-listview.component.css']
})
export class MediaAllListviewComponent implements OnInit, OnDestroy {

  private subs = new SubSink();
  @Select(MediaState.getMedia) media$: Observable<MediaItem[]>;
  @Select(MediaState.getTotalMedia) totalMedia$: Observable<number>;
  @Select(MediaState.isFilterCleared) isFilterCleared$: Observable<boolean>;
  @Select(MediaState.isMediaSelectionCleared) isMediaSelectionCleared$: Observable<boolean>;
  @Select(MediaState.getSelectedItems) selectedItems$: Observable<MediaItem[]>;

  actions: GridColumnButton[] = [
    { type: 'button', text: 'Download', visible: true, action: this.downloadGridEvent.bind(this), right: 45 },
    { type: 'icon', icon: 'pencil', visible: true, action: this.editGridEvent.bind(this) }
  ];
  columns: GridColumn[] = [
    { type: "checkbox", headerText: "Select All", width: 70, field: "" },
    { headerText: "Type", field: "type", width: 95 },
    { headerText: '', type: 'favorite', field: "isFavorite", width: 60, action: this.toggleFavoriteEvent.bind(this) },
    { headerText: "Name", field: "nameWithoutExtension", useAsLink: true },
    { headerText: "Date", field: "modifiedOn", type: 'date', format: 'MMM dd, yyyy  hh:mm', width: 200 },
    { type: 'buttons', buttons: this.actions, width: 200 }
  ];
  media: MediaItem[];
  selectedItemRecords: any;
  currentPage = 1;
  pageSize = 12;
  pageSizes = [12, 25, 50];
  pageCount = 5;
  medias: MediaItem[];
  isPageChangeInvoked: boolean;
  selectedIds: any[];

  constructor(private store: Store, private router: Router, private route: ActivatedRoute) { }

  ngOnInit() {
    this.subs.add(
      this.route.queryParams
        .subscribe(params => {
          const { pageNumber, pageSize } = params;
          this.pageSize = Number(pageSize) || 12;
          this.currentPage = Number(pageNumber) || 1;
        }),
      this.selectedItems$
        .subscribe(items => this.selectedIds = items.map(i => i.documentId))
    );
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
    this.store.dispatch(new ClearSelectedMediaItems());
  }

  rowSelectedEvent(items: MediaItem[]) {
    this.store.dispatch(new AddSelectedMediaItems(items));
  }

  rowDeselectedEvent(items: MediaItem[]) {
    this.store.dispatch(new RemoveSelectedMediaItems(items));
  }

  toggleFavoriteEvent(item: MediaItem) {
    if (!item.isFavorite) {
      this.store.dispatch(new AddFavorite(item.documentId)).toPromise()
        .then(() => {
          this.store.dispatch(new RefetchMedia(this.currentPage, this.pageSize));
        });
    } else {
      this.store.dispatch(new RemoveFavorite(item.favoriteId)).toPromise()
        .then(() => {
          this.store.dispatch(new RefetchMedia(this.currentPage, this.pageSize));
        });
    }
  }

  downloadGridEvent(data: MediaItem) {
    this.store.dispatch(new DownloadMediaItem(data.name, data.url));
  }

  editGridEvent(data: MediaItem) {
    this.router.navigate([`media/${data.documentId}/details`]);
  }
}
