import { BaseComponent } from '../../shared/base/base.component';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ViewType } from 'src/app/core/enum/view-type';
import { Store, Select } from '@ngxs/store';
import { SubSink } from 'subsink/dist/subsink';
import { Tag } from 'src/app/core/models/entity/tag';
import { AddFilterTags, ResetAllFilters, ResetFilter, RefetchMedia, SetFilterTags } from '../state/media/media.action';
import { MediaState } from '../state/media/media.state';
import { Observable } from 'rxjs/internal/Observable';
import { Filter, FilterType } from 'src/app/core/models/entity/filter';
import { DateService } from 'src/app/core/services/business/dates/date.service';
import { MetadataDataType } from 'src/app/core/enum/metadata-data-type';

@Component({
  selector: 'app-media-all',
  templateUrl: './media-all.component.html',
  styleUrls: ['./media-all.component.css']
})
export class MediaAllComponent extends BaseComponent implements OnInit, OnDestroy {

  @Select(MediaState.getFilters) filters$: Observable<Filter[]>;

  public subs = new SubSink();
  viewType: string;
  currentPage = 1;
  pageSize = 8;
  filters: Filter[] = [];

  constructor(protected store: Store, private router: Router, private route: ActivatedRoute, private dateService: DateService) {
    super(store);
    this.setPageTitle("All Media");
  }

  ngOnInit() {
    this.subs.add(
      this.route.queryParams
        .subscribe(params => {
          const { view, pageNumber, pageSize } = params;
          this.viewType = view ? view : ViewType.TILE;
          this.currentPage = Number(pageNumber) || 1;
          this.pageSize = pageSize ? Number(pageSize) : this.viewType === ViewType.TILE ? 8 : 12;
          if (view !== ViewType.MAP)
            this.store.dispatch(new RefetchMedia(this.currentPage, this.pageSize, false));
        }),
      this.filters$
        .subscribe(f => {
          const filters: Filter[] = jsonCopy(f) as Filter[];
          this.filters = filters;
        })
    );
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

  onApplyFilter(filter: Filter) {
    let tags: Tag[] = [];
    if (filter.searchType === FilterType.checkbox) {
      filter.options.forEach(option => {
        if (option.isChecked) {
          let tag: Tag = { name: filter.name, label: filter.label, value: option.label, searchType: filter.searchType, dataType: filter.dataType };
          tags = [...tags, tag];
        }
      });
    } else if (filter.searchType === FilterType.date) {
      const value = `${this.dateService.formatToString(filter.value[0], 'MM/DD/YYYY')} - ${this.dateService.formatToString(filter.value[1], 'MM/DD/YYYY')}`;
      let tag: Tag = { name: filter.name, label: filter.label, value: value, rangeValue: filter.value, type: filter.searchType, dataType: filter.dataType };
      tags = [...tags, tag];
    } else if (filter.searchType === FilterType.range) {
      if (filter.dataType === MetadataDataType.Date) {
        const rangeValues = [new Date(filter.values[0]), new Date(filter.values[1])];
        const value = this.convertTimeToString(rangeValues);
        let tag: Tag = { name: filter.name, label: filter.label, value: value, rangeValue: rangeValues, searchType: filter.searchType, dataType: filter.dataType };
        tags = [...tags, tag];
      } else {
        const value = `${filter.values[0]} - ${filter.values[1]}`;
        let tag: Tag = { name: filter.name, label: filter.label, value: value, rangeValue: filter.values, searchType: filter.searchType, dataType: filter.dataType };
        tags = [...tags, tag];
      }
    }
    this.store.dispatch(new AddFilterTags(filter.name, tags));
  }

  resetFilter(filter: Filter) {
    this.store.dispatch(new ResetFilter(filter));
  }

  resetAllFilters() {
    this.store.dispatch(new ResetAllFilters());
  }

  private convertTimeToString(values: Date[]) {
    let custom = { year: "numeric", month: "short", day: "numeric" };
    return `${new Date(values[0]).toLocaleDateString("en-us", custom)} - ${new Date(values[1]).toLocaleDateString("en-us", custom)}`;
  }
}

function jsonCopy(src: any) {
  return JSON.parse(JSON.stringify(src));
}