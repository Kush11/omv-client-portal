import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { MediaItem } from 'src/app/core/models/entity/media';
import { Router, ActivatedRoute } from '@angular/router';
import { Select, Store } from '@ngxs/store';
import { MediaState } from '../../state/media/media.state';
import { AddFavorite, RemoveFavorite, AddSelectedMediaItem, RemoveSelectedMediaItem, ClearSelectedMediaItems, SetBreadcrumbs, GetTelemetryItems, RefetchMedia } from '../../state/media/media.action';
import { ShowSpinner } from 'src/app/state/app.actions';
import { Breadcrumb } from 'src/app/core/models/breadcrumb';
import { SubSink } from 'subsink/dist/subsink';
import { Observable } from 'rxjs/internal/Observable';
import { TileViewComponent } from 'src/app/shared/tile-view/tile-view.component';

@Component({
  selector: 'app-media-all-tileview',
  templateUrl: './media-all-tileview.component.html',
  styleUrls: ['./media-all-tileview.component.css']
})
export class MediaAllTileviewComponent implements OnInit, OnDestroy {

  private subs = new SubSink();
  media: MediaItem[];
  currentPage = 1;
  pageSize = 8;
  pageSizes = [8, 16, 32];
  pageCount = 5;
  selectedItems: MediaItem[];
  isPageChangeInvoked: boolean;

  @Select(MediaState.getMedia) media$: Observable<MediaItem[]>;
  @Select(MediaState.getTotalMedia) totalMedia$: Observable<number>;
  @Select(MediaState.isFilterCleared) isFilterCleared$: Observable<boolean>;
  @Select(MediaState.isMediaSelectionCleared) isMediaSelectionCleared$: Observable<boolean>;
  @Select(MediaState.getSelectedItems) selectedItems$: Observable<MediaItem[]>;

  @ViewChild("tileView") tileView: TileViewComponent;

  constructor(private store: Store, private router: Router, private route: ActivatedRoute) { }

  ngOnInit() {
    this.subs.add(
      this.route.queryParams
        .subscribe(params => {
          const { pageNumber, pageSize } = params;
          this.pageSize = Number(pageSize) || 8;
          this.currentPage = Number(pageNumber) || 1;
        }),
      this.selectedItems$
        .subscribe(items => this.selectedItems = items)
    );
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
    this.store.dispatch(new ClearSelectedMediaItems());
  }

  navigate(data: MediaItem) {
    if (data.documentTypeCode === 'telemetry') {
      this.store.dispatch(new GetTelemetryItems(data.documentId));
      this.router.navigate([`media/pipeline-view`]);
      return;
    }
    let crumbs: Breadcrumb[] = [
      { link: '/media/all', name: 'Media' },
      { link: '/media/all', name: 'All Media' },
      { name: data.name, isFinal: true }
    ];
    this.store.dispatch(new SetBreadcrumbs(crumbs));
    this.router.navigate([`media/${data.documentId}/details`]);
  }

  toggleCheckBoxEvent(item: MediaItem) {
    const selectedIds = this.selectedItems.map(x => x.documentId);
    if (!selectedIds.includes(item.documentId)) {
      this.store.dispatch(new AddSelectedMediaItem(item));
    } else {
      this.store.dispatch(new RemoveSelectedMediaItem(item));
    }
  }

  toggleFavoriteEvent(item: MediaItem) {
    this.store.dispatch(new ShowSpinner());
    if (!item.isFavorite) {
      this.store.dispatch(new AddFavorite(item.documentId)).toPromise()
        .then(() => {
          this.store.dispatch(new RefetchMedia(this.currentPage, this.pageSize));
        });
    } else {
      this.store.dispatch(new RemoveFavorite(item.favoriteId)).toPromise()
        .then(() => {
          this.store.dispatch(new RefetchMedia(this.currentPage, this.pageSize));
        });
    }
  }
}
