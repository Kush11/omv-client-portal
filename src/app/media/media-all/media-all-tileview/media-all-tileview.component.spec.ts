import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MediaAllTileviewComponent } from './media-all-tileview.component';

describe('MediaAllTileviewComponent', () => {
  let component: MediaAllTileviewComponent;
  let fixture: ComponentFixture<MediaAllTileviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MediaAllTileviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MediaAllTileviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
