import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { MediaState } from '../../state/media/media.state';
import { Select, Store } from '@ngxs/store';
import { GridColumn } from 'src/app/core/models/grid.column';
import { AddSelectedMediaItem, RemoveSelectedMediaItem, ClearSelectedMediaItems, DownloadMediaItem } from '../../state/media/media.action';
import { Router } from '@angular/router';
import { MediaItem } from 'src/app/core/models/entity/media';
import { PageService } from '@syncfusion/ej2-angular-treegrid';
import { SubSink } from 'subsink/dist/subsink';
import { Observable } from 'rxjs/internal/Observable';
import { MediaAllTreeViewService } from './media-all-treeview.service';
import { DirectoryService } from 'src/app/core/services/business/directory/directory.service';
import { Directory } from 'src/app/core/models/entity/directory';
import { BaseComponent } from 'src/app/shared/base/base.component';
import { TreeGridComponent } from 'src/app/shared/tree-grid/tree-grid.component';
import { Tag } from 'src/app/core/models/entity/tag';
import { MediaService } from 'src/app/core/services/business/media/media.service';
import { QueryCellInfoEventArgs } from '@syncfusion/ej2-angular-grids';
import { AppState } from 'src/app/state/app.state';

@Component({
  selector: 'app-media-all-treeview',
  templateUrl: './media-all-treeview.component.html',
  styleUrls: ['./media-all-treeview.component.css'],
  providers: [PageService]
})
export class MediaAllTreeviewComponent extends BaseComponent implements OnInit, OnDestroy {

  private subs = new SubSink();
  media: MediaItem[];

  @ViewChild('treegrid') treegrid: TreeGridComponent;

  @Select(MediaState.getTreeViewMedia) media$: Observable<MediaItem[]>;
  @Select(MediaState.getSelectedItems) selectedItems$: Observable<MediaItem[]>;
  @Select(MediaState.isMediaSelectionCleared) isMediaSelectionCleared$: Observable<boolean>;
  @Select(MediaState.getFilterTags) filters$: Observable<Tag[]>;
  @Select(MediaState.getSearchQuery) searchQuery$: Observable<string>;
  @Select(AppState.getMediaReadSASToken) sasToken$: Observable<string>;

  selectionOptions: Object;
  options: any;
  columns: GridColumn[] = [
    { headerText: '', field: 'checkboxTemplate', width: 50, textAlign: 'Center' },
    { headerText: '', field: 'favoriteTemplate', width: 40 },
    { headerText: 'Name', field: 'name', showCheckbox: true },
    { headerText: 'Date', field: 'modifiedOnString' },
    { headerText: '', field: 'downloadTemplate', width: 120 },
    { headerText: '', field: 'editTemplate', width: 70 }
  ];
  selectedItems: MediaItem[] = [];
  selectedVideoItems: MediaItem[] = [];

  data: Observable<any>;
  state: any;
  formatoption: Object;
  filters: Tag[];
  sasToken: string;

  constructor(protected store: Store, private router: Router, private service: MediaAllTreeViewService, private directoryService: DirectoryService,
    private mediaService: MediaService) {
    super(store);
    this.data = service;
    console.log('data from media tree view', this.data);
  }

  dataStateChange(state: any) {
    if (state.requestType === 'expand') {
      let folder = state.data as Directory;
      this.treegrid.treegrid.showSpinner();
      this.directoryService.getFolders(folder.id).toPromise()
        .then(folders => {
          const queryFilterName = 'queryFilter';
          const queryFilter = this.filters.find(t => t.name === queryFilterName);
          const filters = this.filters.filter(t => t.name != queryFilterName);
          const query = queryFilter ? queryFilter.value : '';
          this.directoryService.getTreeViewMedia(query, filters, folder.id)
            .then(mediaItems => {
              let items = [...folders, ...mediaItems];
              const oldIds = this.service.directories.map(x => x.id);
              items = items.filter(x => !oldIds.includes(x.id));
              state.childData = items;
              state.childDataBind();
              this.treegrid.treegrid.hideSpinner();
            }, err => {
              this.showErrorMessage(err);
              this.treegrid.treegrid.hideSpinner();
            });
        }, err => {
          this.showErrorMessage(err);
          this.treegrid.treegrid.hideSpinner();
        });
    } else {
      this.service.execute();
    }
  }

  ngOnInit() {
    this.selectionOptions = {
      mode: 'Row', cellSelectionMode: 'Flow', type: 'Single', checkboxOnly: false,
      enableVirtualization: true, persistSelection: false, checkboxMode: 'ResetOnRowClick', enableSimpleMultiRowSelection: true,
      enableToggle: false
    };

    this.subs.add(
      this.selectedItems$
        .subscribe(items => {
          this.selectedItems = items;
        }),
      this.isMediaSelectionCleared$
        .subscribe(isCleared => {
          if (isCleared) {
            // this.store.dispatch(new GetTreeViewMedia());
          }
        }),
      this.filters$
        .subscribe(filters => {
          this.filters = filters;
          this.service.execute();
        }),
      this.sasToken$
        .subscribe(token => this.sasToken = token)
    );
  }

  ngOnDestroy() {
    this.store.dispatch(new ClearSelectedMediaItems());
    this.subs.unsubscribe();
  }

  toggleFavoriteEvent(item: any) {
    this.showSpinner()
    if (item.isFavorite) {
      this.mediaService.addFavorite(item.documentId).toPromise()
        .then(favoriteId => {
          item.favoriteId = favoriteId;
          this.showSuccessMessage("Favorite media was successfully saved.");
          this.hideSpinner();
        }, err => this.showErrorMessage(err));
    } else {
      this.mediaService.removeFavorite(item.favoriteId).toPromise()
        .then(() => {
          this.hideSpinner();
        }, err => this.showErrorMessage(err));
    }
  }

  rowChecked(item: MediaItem) {
    if (!this.selectedItems.includes(item)) {
      this.store.dispatch(new AddSelectedMediaItem(item));
    } else {
      this.store.dispatch(new RemoveSelectedMediaItem(item));
    }
  }

  checkboxSelected(action) {
    console.log(action.data);
  }

  addFolderIcon(args: QueryCellInfoEventArgs) {
    console.log("addFolderIcon: ", args);
    if (args.column.field === 'name') {
      if (args.data['isDirectory']) {
        let imgElement: HTMLElement = document.createElement('IMG');
        imgElement.setAttribute('src', './assets/images/icon-folder.svg');
        imgElement.classList.add('folder');
        let div: HTMLElement = document.createElement('DIV');
        div.style.display = 'inline-block';
        div.appendChild(imgElement);
        let cellValue: HTMLElement = document.createElement('DIV');
        if (args.cell.querySelector('.e-treecell')) {
          cellValue.innerHTML = args.cell.querySelector('.e-treecell').innerHTML;
          args.cell.querySelector('.e-treecell').innerHTML = '';
          args.cell.querySelector('.e-treecell').appendChild(div);
          args.cell.querySelector('.e-treecell').appendChild(cellValue);
          cellValue.setAttribute('style', 'display:inline-block;padding-left:6px');
        }
      } else {
        if (!args.data['routeLink']) {
          args.cell.addEventListener('click', this.editEvent.bind(this, args.data), false);
        } else {
          let linkElement: HTMLAnchorElement = document.createElement('a');
          linkElement.setAttribute('href', args.data["routeLink"]);
          if (args.cell.querySelector('.e-treecell')) {
            linkElement.innerHTML = args.data["name"];
            linkElement.classList.add('grid-link');
            args.cell.querySelector('.e-treecell').innerHTML = '';
            args.cell.querySelector('.e-treecell').appendChild(linkElement);
            args.cell.addEventListener('click', this.editEvent.bind(this, args.data), false);
            args.cell.addEventListener("click", function (event) {
              event.preventDefault();
            });
          }
        }
      }
    }
  }

  editEvent(data: any) {
    this.router.navigate([`media/${data.documentId}/details`]);
  }

  download(data: any) {
    this.store.dispatch(new DownloadMediaItem(data.name, data.url + this.sasToken));
  }
}
