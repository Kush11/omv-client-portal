import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MediaAllTreeviewComponent } from './media-all-treeview.component';

describe('MediaAllTreeviewComponent', () => {
  let component: MediaAllTreeviewComponent;
  let fixture: ComponentFixture<MediaAllTreeviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MediaAllTreeviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MediaAllTreeviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
