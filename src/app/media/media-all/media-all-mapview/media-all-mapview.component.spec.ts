import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MediaAllMapviewComponent } from './media-all-mapview.component';

describe('MediaAllMapviewComponent', () => {
  let component: MediaAllMapviewComponent;
  let fixture: ComponentFixture<MediaAllMapviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MediaAllMapviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MediaAllMapviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
