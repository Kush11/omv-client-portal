import { ShowIsFilterResult, GetMapCoordinates } from './../../state/media/media.action';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MediaState } from '../../state/media/media.state';
import { MediaItem } from 'src/app/core/models/entity/media';
import { Select, Store } from '@ngxs/store';
import * as L from 'leaflet';
import 'leaflet.markercluster';
import { RefetchMedia, SetMapCoordinates, HideIsFetching, HideIsFilterResult } from '../../state/media/media.action';
import { Router } from '@angular/router';
import { SubSink } from 'subsink/dist/subsink';
import { Observable } from 'rxjs/internal/Observable';
import { ShowSpinner } from 'src/app/state/app.actions';

@Component({
  selector: 'app-media-all-mapview',
  templateUrl: './media-all-mapview.component.html',
  styleUrls: ['./media-all-mapview.component.css']
})
export class MediaAllMapviewComponent implements OnInit {

  private subs = new SubSink();
  @Select(MediaState.getMedia) media$: Observable<MediaItem[]>;
  @Select(MediaState.getTotalMedia) totalMedia$: Observable<number>;
  @Select(MediaState.getIsFetchingVisibility) isFetching$: Observable<boolean>;
  @Select(MediaState.getFilterResultVisibility) filterResult$: Observable<boolean>;
  @Select(MediaState.isFilterCleared) isFilterCleared$: Observable<boolean>;


  LAYER_OSM = {
    id: 'openstreetmap',
    name: 'Open Street Map',
    enabled: false,
    layer: L.tileLayer('https://api.mapbox.com/styles/v1/mapbox/streets-v11/tiles/{z}/{x}/{y}?access_token=pk.eyJ1Ijoib25pb3lld29sZTE5MTAiLCJhIjoiY2pzcm1yZmxnMGg2czQ0czltNW90M25sMSJ9.tn_vEt25syB3KRQg4fMzVg', {
      noWrap: true,
      maxZoom: 20,
      minZoom: 3,
      attribution: '© <a href="https://www.mapbox.com/feedback/">Mapbox</a> © <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
    })
  };

  layersControlOptions = { position: 'bottomright' };
  baseLayers = {
    'Open Street Map': this.LAYER_OSM.layer
  };
  options = {
    zoom: 2,
    center: L.latLng([0.38460960075573264, -36.157639842566])
  };


  currentPage = 1;
  markerClusterGroup: L.MarkerClusterGroup;
  markerClusterData: any[] = [];
  markerClusterOptions: L.MarkerClusterGroupOptions;
  totalMedia: number;
  isFetchingLabel: boolean;
  moreResult: boolean;
  filterResultLabel: boolean;

  constructor(private store: Store, private router: Router) { }

  ngOnInit() {
    this.store.dispatch(new ShowSpinner());
    this.store.dispatch(new RefetchMedia(this.currentPage, 999, true, []));
    this.subs.add(
      this.isFetching$
        .subscribe((isFetching) => {
          this.isFetchingLabel = isFetching;
        }),
      this.filterResult$
        .subscribe((result) => {
          this.filterResultLabel = result;
        }),
      this.totalMedia$
        .subscribe((totalMedia) => {
          this.totalMedia = totalMedia;
        }),
      this.media$
        .subscribe((media) => {
          // don't display on map if result is more than or equal to 1000
          if (this.totalMedia > 999) {
            this.markerClusterData = [];
            this.store.dispatch(new ShowIsFilterResult());
            this.store.dispatch(new HideIsFetching());
            return;
          }

          const mapMedia = media.filter(m => m.longitude && m.latitude);
          const data: any[] = [];
          if (mapMedia.length > 0) {
            for (let i = 0; i < mapMedia.length; i++) {
              if (mapMedia[i].latitude && mapMedia[i].longitude) {
                const icon = L.icon({
                  iconSize: [25, 41],
                  iconAnchor: [13, 41],
                  iconUrl: 'leaflet/marker-icon.png',
                  shadowUrl: 'leaflet/marker-shadow.png',
                  tooltipAnchor: [0, -41],
                });
                const longitude = mapMedia[i].longitude;
                const latitude = mapMedia[i].latitude;
                data.push(L.marker([longitude, latitude], { icon }).bindPopup(`<a href="media/${mapMedia[i].documentId}/details" target="_blank">${mapMedia[i].name}</a>`));
              }
              this.markerClusterData = data;
            }
          } else {
            this.markerClusterData = [];
          }
          this.store.dispatch(new HideIsFilterResult());
          this.store.dispatch(new HideIsFetching());
        })
    );
  }

  markerClusterReady(group: L.MarkerClusterGroup) {
    this.markerClusterGroup = group;
  }

  clicked(id: any) {
    const url = `media/${id}/details`;
    this.router.navigate([url]);
  }

  applyMapFilter() {
    this.store.dispatch(new RefetchMedia(this.currentPage, 999, true));
  }

  // #region Map zoom 
  handleMapMoveEnd(event) {
    const mapCoordinates = [];
    const map = event.target;
    const bounds = map.getBounds();
    const NECorner = bounds.getNorthEast().wrap();
    const SWCorner = bounds.getSouthWest().wrap();

    const NWCorner = bounds.getNorthWest().wrap();
    const SECorner = bounds.getSouthEast().wrap();


    const NW_Corner = { latitude: NWCorner.lat, longitude: NWCorner.lng };
    const SW_Corner = { latitude: SWCorner.lat, longitude: SWCorner.lng };
    const SE_Corner = { latitude: SECorner.lat, longitude: SECorner.lng };
    const NE_Corner = { latitude: NECorner.lat, longitude: NECorner.lng };

    mapCoordinates.push(NW_Corner, SW_Corner, SE_Corner, NE_Corner);
    console.log('--mapCoordinates--', mapCoordinates);
    this.store.dispatch(new SetMapCoordinates(mapCoordinates));
  }

}


