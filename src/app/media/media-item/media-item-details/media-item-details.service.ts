import { Injectable } from "@angular/core";
import { FieldConfiguration, FieldType } from 'src/app/shared/dynamic-components/field-setting';
import { Validators, ValidatorFn } from '@angular/forms';
import { MediaItem } from 'src/app/core/models/entity/media';
import { MetadataFieldType } from 'src/app/core/enum/metadataFieldType';
import { MetadataField } from 'src/app/core/models/entity/metadata-field';
import { DirectoryService } from 'src/app/core/services/business/directory/directory.service';
import { DateService } from 'src/app/core/services/business/dates/date.service';
import { MetadataDataType } from 'src/app/core/enum/metadata-data-type';
import { MetadataFieldsService } from 'src/app/core/services/business/metadata-fields/metadata-fields.service';
import { FieldsService } from 'src/app/core/services/business/fields/fields.service';
import { SortType, SortDirection } from 'src/app/core/enum/sort-enum';

@Injectable({
  providedIn: 'root'
})
export class MediaItemDetailsService {

  metadataFields: MetadataField[] = [];
  directoryMetadataFields: MetadataField[] = []

  constructor(private foldersService: DirectoryService, private metadataFieldsService: MetadataFieldsService, private fieldsService: FieldsService) { }

  async getMediaItemMetadaFields(directoryId: number, metadata: string): Promise<FieldConfiguration[]> {
    let mediaItemMetadataFields: FieldConfiguration[] = [];
    // Step 1:  Get all metadatafields
    const metadataFields = await this.metadataFieldsService.getMediaMetadataFields().toPromise();
    metadataFields.forEach(m => m.isRequired = false);
    // Get all directory metadatafields
    this.directoryMetadataFields = await this.foldersService.getMetadataFields(directoryId);

    // Step 2: Get media-item's metadata fields
    const mediaItemMetadata = JSON.parse(metadata);
    const mediaItemMetadataNames = Object.keys(mediaItemMetadata);

    // Step 3: Get common fields if mediaItem metadata field is in all, show as control else show as label
    const commonFields = metadataFields.filter(f => mediaItemMetadataNames.includes(f.name));
    commonFields.forEach(field => {
      const name = mediaItemMetadataNames.find(n => n === field.name);
      const value = mediaItemMetadata[name];
      const directoryMetadataField = this.directoryMetadataFields.find(f => f.name === field.name);
      field.isRequired = directoryMetadataField ? directoryMetadataField.isRequired : false;
      const formField = this.buildFormField(field, value);
      mediaItemMetadataFields = [...mediaItemMetadataFields, formField];
    });
    mediaItemMetadataFields.sort((a, b) => a.order - b.order);

    // Step 4: Mark common fields with value as checked in the parent folder's metadata
    const commonFieldNames = mediaItemMetadataFields.map(m => m.name);
    metadataFields.forEach(f => f.isChecked = commonFieldNames.includes(f.name));
    this.metadataFields = this.fieldsService.sort(metadataFields, 'label', SortType.String, SortDirection.Ascending);

    // Step 5: Get all uncommon fields: present in the media-item's metadata but not in all metadata fields - build them as labels
    const metadataFieldNames = metadataFields.map(f => f.name);
    const uncommonFields = mediaItemMetadataNames.filter(name => !metadataFieldNames.includes(name));
    uncommonFields.forEach((name, index) => {
      // Step 6: Choose only fields with values
      const metadataValue = mediaItemMetadata[name];
      if (metadataValue) {
        const config = this.buildLabel(name, mediaItemMetadata[name], index + 1000);
        mediaItemMetadataFields = [...mediaItemMetadataFields, config];
      }
    });
    return mediaItemMetadataFields;
  }

  buildFormField(field: MetadataField, value?: string): FieldConfiguration {
    let config: FieldConfiguration;
    switch (field.type) {
      case MetadataFieldType.Text:
        config = this.buildTextBox(field, value);
        break;
      case MetadataFieldType.Select:
        config = this.buildDropdown(field, value);
        break;
      case MetadataFieldType.Date:
        config = this.buildDate(field, value);
        break;
      case MetadataFieldType.Nested:
        config = this.buildNestedDropdown(field, value);
        break;
    }
    return config;
  }

  private buildLabel(name: string, value: string, order: number): FieldConfiguration {
    return {
      type: FieldType.Label,
      data: {},
      name: name,
      label: name,
      value: value,
      order: order
    };
  }

  private buildTextBox(item: MetadataField, value?: string): FieldConfiguration {
    return {
      type: FieldType.Input,
      data: item,
      id: item.id,
      label: item.label || item.name,
      inputType: (item.dataType === MetadataDataType.Int32
        || item.dataType === MetadataDataType.Int64
        || item.dataType === MetadataDataType.Double) ? 'number' : 'text',
      name: item.name,
      order: item.order,
      placeholder: item.label || item.name,
      required: item.isRequired,
      value: !item.dataType ? value || '' :
        (item.dataType === MetadataDataType.Int32 ||
          item.dataType === MetadataDataType.Int64 ||
          item.dataType === MetadataDataType.Double) ? Number(value) : value || '',
      validations: this.getValidations(item)
    };
  }

  private buildDropdown(item: MetadataField, value?: string): FieldConfiguration {
    return {
      type: FieldType.Select,
      data: item,
      id: item.id,
      label: item.label || item.name,
      name: item.name,
      order: item.order,
      options: item.options,
      placeholder: 'Select',
      required: item.isRequired,
      value: value.toString() || '',
      validations: this.getValidations(item)
    };
  }

  private buildNestedDropdown(item: MetadataField, value?: string): FieldConfiguration {
    return {
      type: FieldType.Select,
      data: item,
      id: item.id,
      parentId: item.parentId,
      label: item.label || item.name,
      name: item.name,
      order: item.order,
      options: item.options,
      placeholder: 'Select',
      value: !item.dataType ? value || '' :
        (item.dataType === MetadataDataType.Int32 ||
          item.dataType === MetadataDataType.Int64 ||
          item.dataType === MetadataDataType.Double) ? Number(value) : value || '',
      required: item.isRequired,
      isNested: true,
      isEnabled: false
    };
  }

  private buildDate(item: MetadataField, value?: string): FieldConfiguration {
    return {
      type: FieldType.Date,
      data: item,
      id: item.id,
      label: item.label || item.name,
      name: item.name,
      order: item.order,
      required: item.isRequired,
      value: new Date(value || ''),
      validations: this.getValidations(item)
    };
  }

  private getValidations(item: MetadataField): ValidatorFn[] {
    let validations: ValidatorFn[] = [];
    if (item.isRequired) {
      let requiredValidation: any = {
        name: 'required',
        validator: Validators.required,
        message: `${item.label || item.name} is required`
      };
      validations.push(requiredValidation);
    }
    return validations;
  }
}