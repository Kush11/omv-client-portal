import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { Router } from "@angular/router";
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs/internal/Observable';
import { GetMediaItem, UpdateMetadata, DeleteMediaItem } from '../../state/media/media.action';
import { MediaState } from '../../state/media/media.state';
import { DynamicFormComponent } from 'src/app/shared/dynamic-components/components/dynamic-form.component';
import { FieldConfiguration, FieldType } from 'src/app/shared/dynamic-components/field-setting';
import { MediaItem } from 'src/app/core/models/entity/media';
import { MediaItemDetailsService } from './media-item-details.service';
import { SetPreviousRoute, ShowSpinner, ShowErrorMessage, HideSpinner, GetAzureSASToken } from 'src/app/state/app.actions';
import { MetadataField } from 'src/app/core/models/entity/metadata-field';
import { SubSink } from 'subsink/dist/subsink';
import { ModalComponent } from 'src/app/shared/modal/modal.component';
import { BaseComponent } from 'src/app/shared/base/base.component';
import { MetadataDataType } from 'src/app/core/enum/metadata-data-type';
import { MediaType } from 'src/app/core/enum/media-type';
import { AppState } from 'src/app/state/app.state';

@Component({
  selector: 'app-media-item-details',
  templateUrl: './media-item-details.component.html',
  styleUrls: ['./media-item-details.component.css'],
  providers: [MediaItemDetailsService]
})
export class MediaItemDetailsComponent extends BaseComponent implements OnInit, OnDestroy {

  @ViewChild(DynamicFormComponent) dynamicForm: DynamicFormComponent;
  @ViewChild('confirmModal') confirmModal: ModalComponent;
  @ViewChild('metadataFieldsModal') metadataFieldsModal: ModalComponent;

  @Select(MediaState.getCurrentMediaItem) currentMediaItem$: Observable<MediaItem>;
  @Select(MediaState.getCurrentMediaItemId) currentMediaItemId$: Observable<number>;
  @Select(MediaState.getCurrentMediaItem) mediaItem$: Observable<MediaItem>;
  @Select(MediaState.getItemFields) itemMetadataFields$: Observable<any[]>;
  @Select(AppState.getMediaReadSASToken) sasToken$: Observable<string>;
  @Select(MediaState.getCurrentItemMetadata) metadataFields$: Observable<any[]>;

  private subs = new SubSink();
  mediaItem: MediaItem;
  currentMediaItemId: any;
  deleteMessage: string;
  metadataFields: MetadataField[];
  currentItemMetadataFields: FieldConfiguration[] = [];
  currentField: FieldConfiguration;
  currentMediaItem: MediaItem;
  sasToken: string;
  id: any;
  

  constructor(protected store: Store, private router: Router, private service: MediaItemDetailsService) {
    super(store);
  }

  ngOnInit() {
    this.store.dispatch(new GetAzureSASToken());
    this.subs.add(
      this.sasToken$
        .subscribe(sasToken => {
          this.sasToken = sasToken;
        }),
      this.currentMediaItemId$
        .subscribe(id => {
          if (id) {
            this.id = id;
            this.currentMediaItemId = id;
            this.store.dispatch(new GetMediaItem(this.currentMediaItemId));
          }
        }),
      this.currentMediaItem$
        .subscribe(item => {
          if (item) {
            this.currentMediaItem = item;
            this.setPageTitle(`${item.name} Details - Media`);
            this.setPageDetails();
          }
        })
    );
  }

  ngOnDestroy() {
    this.currentItemMetadataFields = [];
    this.subs.unsubscribe();
  }

  private setPageDetails() {
    this.showSpinner();
    const { directoryId, metadata } = this.currentMediaItem;
    this.service.getMediaItemMetadaFields(directoryId, metadata)
      .then(metadata => {
        this.currentItemMetadataFields = metadata;
        this.metadataFields = this.service.metadataFields;
        this.setChildrenOptions();
        this.hideSpinner();
        this.dynamicForm.markFormAsUnTouchedAndPristine();
      }, err => this.store.dispatch(new ShowErrorMessage(err)));
  }

  private setChildrenOptions() {
    const childrenFields = this.currentItemMetadataFields.filter(field => field.parentId);
    if (childrenFields) {
      childrenFields.forEach(child => {
        // Step 2: find the child's parent
        const parent = this.currentItemMetadataFields.find(x => x.id === child.parentId);
        if (parent) {
          // Step 3: find selected value of the parent
          const parentOption = parent.options.find(x => x.value === parent.value);
          if (parentOption) {
            // Step 4: filter the options of the child by the parent's selected value
            child.isEnabled = parentOption.id ? true : false;
            child.options = child.isEnabled ? child.data.options.filter(x => x.parentId === parentOption.id) : [];
          }
        }
      });
    }
  }

  discardChanges() {
    this.dynamicForm.reset();
    this.currentItemMetadataFields = [];
    this.setPageDetails();
  }

  submit() {
    let metadata = '{}';
    if (this.dynamicForm) {
      const controls = this.dynamicForm.controls;
      let payload = {};
      controls.forEach(config => {
        const dataType = config.data.dataType || MetadataDataType.String;
        payload[config.name] =
          dataType === MetadataDataType.Int32 || dataType === MetadataDataType.Int64 || dataType === MetadataDataType.Double ?
            Number(config.value) :
            dataType === MetadataDataType.Date ? new Date(config.value) :
              config.value;
      });
      metadata = JSON.stringify(payload);
    }
    const { id } = this.currentMediaItem;
    this.store.dispatch(new UpdateMetadata(id, metadata)).toPromise()
      .then(() => {
        this.dynamicForm.reset(this.dynamicForm.value);
        this.store.dispatch(new GetMediaItem(id));
      });
  }

  openViewer() {
    this.store.dispatch(new SetPreviousRoute(this.router.url));
    if (this.currentMediaItem) {
      if (this.currentMediaItem.type === MediaType.Telemetry) {
        this.router.navigate([`media/${this.currentMediaItemId}/telemetry`]);
      } else {
        this.router.navigate([`media/${this.currentMediaItemId}/viewer`]);
      }
    } else {
      this.store.dispatch(new ShowErrorMessage("Unable to get the current media item."))
    }
  }

  addField() {
    this.metadataFieldsModal.show();
  }

  addMetadataFields(fields: MetadataField[]) {
    fields.forEach(field => {
      const directoryMetadataField = this.service.directoryMetadataFields.find(d => d.name === field.name);
      const defaultValue = directoryMetadataField ? directoryMetadataField.value : '';
      field.isRequired = directoryMetadataField ? directoryMetadataField.isRequired : false;
      const config = this.service.buildFormField(field, defaultValue);
      this.currentItemMetadataFields = [...this.currentItemMetadataFields, config];
    });
    this.currentItemMetadataFields.sort((a, b) => a.order - b.order);
    this.dynamicForm.markFormAsTouchedAndDirty();
  }

  deleteField(field: FieldConfiguration) {
    this.currentItemMetadataFields = this.currentItemMetadataFields.filter(x => x.name !== field.name);
    if (field.type !== FieldType.Label) this.metadataFieldsModal.uncheckField(field);
    this.dynamicForm.markFormAsTouchedAndDirty();
  }

  showDeleteModal() {
    const name = this.currentMediaItem ? this.currentMediaItem.name : 'Unknown';
    this.deleteMessage = `Are you sure you want to delete "${name}"?`;
    this.confirmModal.show();
  }

  deleteMedia() {
    console.log("DElete item: ", this.id, this.mediaItem);
    this.store.dispatch(new DeleteMediaItem(this.id)).toPromise()
      .then(() => {
        this.router.navigate([`media/all`]);
      });
  }

}
