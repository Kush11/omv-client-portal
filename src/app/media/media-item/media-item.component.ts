import { Component, OnInit, OnDestroy } from '@angular/core';
import { Tab } from 'src/app/core/models/tab';
import { Store, Select } from '@ngxs/store';
import { Router, ActivatedRoute } from '@angular/router';
import { BaseComponent } from 'src/app/shared/base/base.component';
import { SetCurrentMediaItemId, GetMediaItem, UpdateBreadcrumb, ClearCurrentMediaItem } from '../state/media/media.action';
import { MediaState } from '../state/media/media.state';
import { Breadcrumb } from 'src/app/core/models/breadcrumb';
import { SubSink } from 'subsink/dist/subsink';
import { Observable } from 'rxjs/internal/Observable';
import { MediaItem } from 'src/app/core/models/entity/media';
import { GetAzureSASToken } from 'src/app/state/app.actions';

@Component({
  selector: 'app-media-item',
  templateUrl: './media-item.component.html',
  styleUrls: ['./media-item.component.css']
})
export class MediaItemComponent extends BaseComponent implements OnInit, OnDestroy {

  private subs = new SubSink();
  id: string;
  mediaItemTabs: Tab[] = [];
  currentTitle: string;
  currentMediaItem: MediaItem;
  breadcrumbs: Breadcrumb[];

  @Select(MediaState.getCurrentMediaItemId) id$: Observable<any>;
  @Select(MediaState.getBreadcrumbs) breadcrumbs$: Observable<Breadcrumb[]>;
  @Select(MediaState.getCurrentMediaItem) currentMediaItem$: Observable<MediaItem>;

  constructor(protected store: Store, private router: Router, private route: ActivatedRoute) {
    super(store);
    this.hideLeftNav();
  }

  ngOnInit() {
    this.subs.add(
      this.route.paramMap
        .subscribe(params => {
          this.id = params.get('id');
          if (this.id) {
            this.store.dispatch(new SetCurrentMediaItemId(this.id));
          }
          this.mediaItemTabs = [
            { link: `/media/${this.id}/details`, name: 'Details', isActive: true },
            { link: `/media/${this.id}/related-items`, name: 'Related Items' },
            { link: `/media/${this.id}/history`, name: 'History' }
          ];
        }),
      this.currentMediaItem$
        .subscribe(item => {
          this.currentMediaItem = item;
          this.setBreadCrumbs();
        })
    );
  }

  ngOnDestroy() {
    this.store.dispatch(new ClearCurrentMediaItem());
    this.subs.unsubscribe();
  }

  setBreadCrumbs() {
    this.subs.sink =
      this.breadcrumbs$
        .subscribe(breadcrumbs => {
          const lastBreadcrumb: Breadcrumb = { name: this.currentMediaItem ? this.currentMediaItem.name : "Unknown" };
          if (breadcrumbs.length === 2) {
            this.breadcrumbs = [
              ...breadcrumbs,
              lastBreadcrumb
            ];
          } else if (breadcrumbs.length === 3) {
            breadcrumbs.pop();
            this.breadcrumbs = [
              ...breadcrumbs,
              lastBreadcrumb
            ];
          }
        });
  }

  switchBreadCrumbs(breadcrumbLink: string) {
    this.router.navigate([breadcrumbLink]);
  }

  switchTabs(tabLink: string) {
    this.router.navigate([tabLink]);
  }
}
