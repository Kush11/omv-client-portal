import { Component, OnInit, OnDestroy } from '@angular/core';
import { GridColumn } from 'src/app/core/models/grid.column';
import { MediaItem } from 'src/app/core/models/entity/media';
import { MediaState } from '../../state/media/media.state';
import { Select, Store } from '@ngxs/store';
import { GetHistory, ClearHistory } from '../../state/media/media.action';
import { SubSink } from 'subsink/dist/subsink';
import { Observable } from 'rxjs/internal/Observable';
import { BaseComponent } from 'src/app/shared/base/base.component';

@Component({
  selector: 'app-media-item-history',
  templateUrl: './media-item-history.component.html',
  styleUrls: ['./media-item-history.component.css']
})
export class MediaItemHistoryComponent extends BaseComponent implements OnInit, OnDestroy {

  private subs = new SubSink();
  @Select(MediaState.getHistory) history$: Observable<MediaItem[]>;
  @Select(MediaState.getCurrentMediaItemId) mediaItemId$: Observable<any>;
  @Select(MediaState.getCurrentMediaItem) currentMediaItem$: Observable<MediaItem>;

  currentPage: number;
  pageSize = 14;
  pageCount = 5;
  mediaHistoryId: number;
  history:MediaItem[] = [];

  columns: GridColumn[] = [
    { type: '', headerText: 'Field Title', field: 'columnName' },
    { type: '', headerText: 'Action Taken', field: 'eventName' },
    { type: '', headerText: 'Updated By', width: '', field: 'createdBy' },
    { type: '', headerText: 'Old Value', field: 'oldValue' },
    { type: '', headerText: 'New Value', width: '', field: 'newValue' },
    { type: 'date', headerText: 'Date', field: 'createdOn' }
  ];

  constructor(protected store: Store) {
    super(store);
  }

  ngOnInit() {
    this.store.dispatch(new ClearHistory());
    this.subs.add(
      this.mediaItemId$
        .subscribe(id => {
          this.mediaHistoryId = id;
          this.store.dispatch(new GetHistory(this.mediaHistoryId, this.currentPage, this.pageSize))
        }),
      this.currentMediaItem$
        .subscribe(item => {
          this.setPageTitle(`${item.name} History - Media`);
        })
    );
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

  paginationEvent(pageNumber: number) {
    this.currentPage = pageNumber;
    this.store.dispatch(new GetHistory(this.mediaHistoryId, this.currentPage, this.pageSize));
    window.scrollTo(0, 0);
  }

}
