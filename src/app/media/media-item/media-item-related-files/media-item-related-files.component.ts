import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { MediaState } from '../../state/media/media.state';
import { MediaItem } from 'src/app/core/models/entity/media';
import { GetRelatedFiles, AddRelatedFiles, AddFavorite, RemoveFavorite, DeleteRelatedFiles, RefetchMedia, DownloadMediaItem } from '../../state/media/media.action';
import { GridColumn, GridColumnButton } from 'src/app/core/models/grid.column';
import { DialogComponent } from '@syncfusion/ej2-angular-popups';
import { ToolBar } from 'src/app/core/models/toolbar-action';
import { Router } from '@angular/router';
import { SubSink } from 'subsink/dist/subsink';
import { Observable } from 'rxjs/internal/Observable';
import { ModalComponent } from 'src/app/shared/modal/modal.component';
import { BaseComponent } from 'src/app/shared/base/base.component';
import { TreeViewComponent } from 'src/app/shared/tree-view/tree-view.component';
import { DirectoryService } from 'src/app/core/services/business/directory/directory.service';

@Component({
  selector: 'app-media-item-related-files',
  templateUrl: './media-item-related-files.component.html',
  styleUrls: ['./media-item-related-files.component.css']
})
export class MediaItemRelatedFilesComponent extends BaseComponent implements OnInit, OnDestroy {

  @Select(MediaState.getCurrentMediaItemId) mediaItemId$: Observable<number>;
  @Select(MediaState.getRelatedItems) relatedItems$: Observable<MediaItem[]>;
  @Select(MediaState.getTreeViewMedia) treeViewMedia$: Observable<MediaItem[]>;
  @Select(MediaState.getCurrentMediaItem) currentMediaItem$: Observable<MediaItem>;

  @ViewChild('relatedItemsDialog') relatedItemsDialog: DialogComponent;
  @ViewChild('confirmModal') confirmModal: ModalComponent;
  @ViewChild('appTreeView') appTreeView: TreeViewComponent;

  private subs = new SubSink();
  media: MediaItem[] = [];
  mediaItemId: any;
  selectedIdsForDelete: string[] = [];
  currentPage = 1;
  pageSize = 25;
  pageCount = 1;
  treeViewFields: Object;
  selectedRelatedItemIds: string[] = [];
  checkedNodes: string[] = [];
  toolbar: ToolBar[] = [{ text: 'Delete', disabled: this.selectedIdsForDelete.length < 1 }];
  actions: GridColumnButton[] = [
    { type: 'button', text: 'Download', visible: true, action: this.downloadGridEvent.bind(this), right: 45 },
    { type: 'icon', icon: 'pencil', visible: true, action: this.editGridEvent.bind(this) }
  ];
  columns: GridColumn[] = [
    { type: 'checkbox', headerText: 'Select All', width: 70, field: '' },
    { headerText: "Type", field: "type", width: 95 },
    { headerText: '', type: 'favorite', field: "isFavorite", width: 60, action: this.toggleFavoriteGridEvent.bind(this) },
    { type: '', headerText: 'Name', field: 'nameWithoutExtension', useAsLink: true },
    { headerText: "Date", field: "modifiedOn", type: 'date', format: 'MMM dd, yyyy  hh:mm a', width: 200 },
    { type: 'buttons', buttons: this.actions, width: 180 }
  ];
  deleteMessage = 'Are you sure you want to remove the item(s) selected?';
  selectedRelatedItems: MediaItem[] = [];
  currentTarget: HTMLLIElement;
  parentID: number;
  relatedItems: MediaItem[] = [];
  selectedItemIds: any[];
  mediaItems: MediaItem[] = [];
  currentDirectoryId: any;
  folderPath: string;
  totalMedia: number;
  selectedIds: any[];

  constructor(protected store: Store, private router: Router, private foldersService: DirectoryService) {
    super(store);
  }

  ngOnInit() {
    this.subs.add(
      this.mediaItemId$
        .subscribe(id => {
          if (id) {
            this.mediaItemId = id;
            this.store.dispatch(new GetRelatedFiles(this.mediaItemId.toString()));
          }
        }),
      this.relatedItems$
        .subscribe(relatedItems => {
          this.relatedItems = relatedItems || [];
          this.showSpinner();
          this.foldersService.getFolders().toPromise()
            .then(folders => {
              if (folders) {
                this.treeViewFields = {
                  dataSource: folders, id: 'id', parentID: 'parentId',
                  text: 'name', iconCss: 'icon', hasChildren: 'hasChild'
                };
                this.hideSpinner();
              }
            }, err => this.showErrorMessage(err));
        }),
      this.currentMediaItem$
        .subscribe(item => {
          this.setPageTitle(`${item.name} Related Items - Media`);
        })
    );
  }
  onChangePage(pageNumber: number) {
    this.foldersService
      .getMedia(this.currentDirectoryId, pageNumber, this.pageSize).toPromise()
      .then(
        res => {
          console.log('media', res);
          this.media = res.data;
          this.hideSpinner();
          // this.buildFolderPath(nodeId);
        },
        error => {
          console.log('media');

          this.appTreeView.treeview.refresh();
          this.showErrorMessage(error);
          this.hideSpinner();
        }
      );
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

  toolbarEvent(index: number) {
    if (index === 0) {
      this.confirmModal.show();
    }
  }

  deleteConfirmed() {
    this.store.dispatch(new DeleteRelatedFiles(this.mediaItemId, this.selectedIdsForDelete));
  }

  downloadGridEvent(data: MediaItem) {
    this.store.dispatch(new DownloadMediaItem(data.name, data.url));
  }

  editGridEvent(item: MediaItem) {
    this.router.navigate([`media/${item.relatedDocumentId}/details`]);
  }

  rowSelectedGridEvent(items: MediaItem[]) {
    items.forEach(item => this.addItem(item));
    this.setToobarProperties();
  }

  rowDeselectedGridEvent(items: MediaItem[]) {
    items.forEach(item => this.removeItem(item));
    this.setToobarProperties();
  }

  private addItem(item: MediaItem) {
    this.selectedIds = this.selectedRelatedItemIds.map(x => x);
    console.log('selected Item', this.selectedIds);

    if (!this.selectedIds.includes(item.documentId)) {
      this.selectedRelatedItemIds = [
        ...this.selectedRelatedItemIds,
        item.documentId
      ];
      console.log('added document Item', this.selectedRelatedItemIds);
    } else {
      this.selectedRelatedItemIds = [
        ...this.selectedRelatedItemIds,
        item.relatedDocumentId
      ];
      console.log('added related Item', this.selectedRelatedItemIds);
    }
  }

  private removeItem(item: MediaItem) {
    this.selectedIds = this.selectedRelatedItemIds.map(x => x);
    if (this.selectedIds.includes(item.documentId)) {
      this.selectedRelatedItemIds = this.selectedRelatedItemIds.filter(x => x !== item.documentId);
    } else {
      this.selectedRelatedItemIds = this.selectedRelatedItemIds.filter(x => x !== item.relatedDocumentId);

    }
  }

  toggleFavoriteGridEvent(item: MediaItem) {
    if (!item.isFavorite) {
      this.store.dispatch(new AddFavorite(item.relatedDocumentId)).toPromise()
        .then(() => {
          this.store.dispatch(new GetRelatedFiles(this.mediaItemId.toString()));
        });
    } else {
      this.store.dispatch(new RemoveFavorite(item.favoriteId)).toPromise()
        .then(() => {
          this.store.dispatch(new GetRelatedFiles(this.mediaItemId.toString()));
        });
    }
  }

  //#region TreeView Modal Events

  rowSelectedTreeEvent(data: any[]) {
    if (!Array.isArray(data)) { return; }
    this.selectedItemIds = data;
  }

  addRelatedItems() {
    this.store.dispatch(new AddRelatedFiles(this.mediaItemId, this.selectedRelatedItemIds));
    this.closeDialog();
  }

  showDialog() {
    this.relatedItemsDialog.show();
    this.selectedRelatedItemIds = [];
  }

  closeDialog() {
    // this.appTreeView.treeview.refresh();
    this.relatedItemsDialog.hide();
  }

  onNodeExpanding() {
    this.currentTarget = this.appTreeView.currentTarget;
    this.parentID = Number(this.appTreeView.parentID);

    this.showSpinner();
    this.foldersService
      .getFolders(Number(this.parentID))
      .toPromise()
      .then(
        folders => {
          folders.forEach(folder => (folder.icon = 'folder'));
          this.appTreeView.treeview.addNodes(
            JSON.parse(JSON.stringify(folders)),
            this.currentTarget
          );
          this.hideSpinner();
        },
        err => {
          this.showErrorMessage(err);
        }
      );
  }

  onNodeSelected(args) {
    this.currentDirectoryId = args.id;
    this.currentPage = 1;
    this.folderPath = '';
    this.showSpinner();
    this.subs.sink = this.foldersService
      .getMedia(this.currentDirectoryId, this.currentPage, this.pageSize)
      .subscribe(
        res => {
          console.log('media', res);
          this.media = res.data;
          this.totalMedia = res.pagination.total;
          this.hideSpinner();
          // this.buildFolderPath(nodeId);
        },
        error => {
          console.log('media');
          this.appTreeView.treeview.refresh();
          this.showErrorMessage(error);
          this.hideSpinner();
        }
      );
  }

  //#endregion

  private setToobarProperties() {
    this.selectedIdsForDelete = this.selectedRelatedItemIds.map(x => x);
    this.toolbar.map(x => x.disabled = this.selectedRelatedItemIds.length < 1);
  }
}
