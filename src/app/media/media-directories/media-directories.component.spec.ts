import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MediaDirectoriesComponent } from './media-directories.component';

describe('MediaDirectoriesComponent', () => {
  let component: MediaDirectoriesComponent;
  let fixture: ComponentFixture<MediaDirectoriesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MediaDirectoriesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MediaDirectoriesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
