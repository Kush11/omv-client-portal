import { MediaService } from 'src/app/core/services/business/media/media.service';
import { SaveSelectedMedia } from './../state/media/media.action';


import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { Store, Select } from '@ngxs/store';
import { ActivatedRoute, Router } from '@angular/router';
import { SubSink } from 'subsink/dist/subsink';
import { ViewType } from 'src/app/core/enum/view-type';
import { RefetchMedia, DownloadMediaItem, AddFavorite, RemoveFavorite, AddSelectedMediaItems, RemoveSelectedMediaItems, SaveExpandedDirectories } from '../state/media/media.action';
import { Observable } from 'rxjs/internal/Observable';
import { MediaAllTreeViewService } from '../media-all/media-all-treeview/media-all-treeview.service';
import {
  ShowSpinner,
  HideSpinner,
  ShowErrorMessage,
  ShowSuccessMessage
} from 'src/app/state/app.actions';
import {
  NodeEditEventArgs,
  DragAndDropEventArgs,
  NodeExpandEventArgs,
  DataSourceChangedEventArgs,
  NodeClickEventArgs
} from '@syncfusion/ej2-angular-navigations';
import { TreeViewComponent } from 'src/app/shared/tree-view/tree-view.component';

import { UpdateFolder } from 'src/app/admin/state/admin-media/admin-media.action';
import { DirectoryService } from 'src/app/core/services/business/directory/directory.service';
import { DirectoryDataService } from 'src/app/core/services/data/directory/directory.data.service';
import { MediaItem, Media } from 'src/app/core/models/entity/media';
import { GridColumn, GridColumnButton } from 'src/app/core/models/grid.column';
import { MediaState } from '../state/media/media.state';
import { DataService } from '../media-upload/data-service';
import { Directory } from 'src/app/core/models/entity/directory';
import { BaseComponent } from 'src/app/shared/base/base.component';

@Component({
  selector: 'app-media-directories',
  templateUrl: './media-directories.component.html',
  styleUrls: ['./media-directories.component.css']
})
export class MediaDirectoriesComponent extends BaseComponent implements OnInit, OnDestroy {

  @Select(MediaState.isMediaSelectionCleared) isMediaSelectionCleared$: Observable<boolean>;
  @Select(MediaState.getSelectedItems) selectedItems$: Observable<MediaItem[]>;
  @Select(MediaState.getExpandedDirectories) expandedDirectories$: Observable<Directory[]>;
  @Select(MediaState.getSelectedMedia) selectedMedia$: Observable<any>;
  @Select(MediaState.getIsNewFolder) isNewFolder$: Observable<any>;



  @ViewChild('appTreeView') appTreeView: TreeViewComponent;


  expandedNodes: string[];
  currentId: any;
  currentFolder: Directory = { id: null, name: null, parentId: null };
  isFolderInvalid: boolean;
  isDragEvent: boolean;
  currentTarget: HTMLLIElement;
  parentID: string;
  selectedIds: any[];
  directories: Directory[] = [];



  public subs = new SubSink();
  viewType: string;
  currentPage = 1;
  pageSize = 25;
  pageCount = 5;
  data: Observable<any>;
  folderPath = '';
  folders: any[];
  field: object;
  media: any[];
  actions: GridColumnButton[] = [
    {
      type: 'button',
      text: 'Download',
      visible: true,
      action: this.downloadGridEvent.bind(this),
      right: 45
    },
    {
      type: 'icon',
      icon: 'pencil',
      visible: true,
      action: this.editGridEvent.bind(this)
    }
  ];
  columns: GridColumn[] = [
    { type: 'checkbox', headerText: 'Select All', width: 70, field: '' },
    { headerText: 'Type', field: 'type', width: 95 },
    {
      headerText: '',
      type: 'favorite',
      field: 'isFavorite',
      width: 60,
      action: this.toggleFavoriteEvent.bind(this)
    },
    {
      headerText: 'Name',
      field: 'nameWithoutExtension',
      useAsLink: true,
      action: this.editGridEvent.bind(this)
    },
    {
      headerText: 'Date',
      field: 'modifiedOn',
      type: 'date',
      format: 'MMM dd, yyyy  hh:mm',
      width: 200
    },
    { type: 'buttons', buttons: this.actions, width: 200 }
  ];
  totalMedia: number;
  currentDirectoryId: number;
  expandedDirectories: Directory[];
  selectedMediaId: any;


  constructor(
    protected store: Store,
    private route: ActivatedRoute,
    private folderDataService: DirectoryDataService,
    private service: MediaAllTreeViewService,
    private folderService: DirectoryService,
    private router: Router  ) {
    super(store);
    this.data = this.service;
    this.setPageTitle('Directories');
  }

  ngOnInit() {
    this.subs.add(
      this.route.queryParams.subscribe(params => {
        const { pageNumber } = params;
        this.currentPage = Number(pageNumber) || 1;
      }),
      this.selectedMedia$.subscribe(selected => {
        if (selected) {
          this.getMedia(selected);
        }
      }),
      this.isNewFolder$.subscribe(newFolder => {
        if (newFolder === true) {
          this.getFolders(false);
        }
      })
    );
    let expandedDirectories = this.store.selectSnapshot(MediaState.getExpandedDirectories);
    if (!expandedDirectories) {
      expandedDirectories = [];
    }
    this.expandedDirectories = JSON.parse(JSON.stringify(expandedDirectories));
    this.getFolders(this.expandedDirectories.length > 0);
  }




  getFolders(usePersistedData: boolean) {
    this.folderService.getFolders()
      .toPromise()
      .then(directories => {
        if (usePersistedData) {
          // get previously expanded directories;
          // check if any has kids, then it's expanded;
          // compare with what's from the database, if it exists, replace with db and update expanded.
          let allDirectories: Directory[] = [...directories];
          console.log('expanded', allDirectories);
          this.expandedDirectories.forEach(oldDirectory => {
            let directory = allDirectories.find(x => x.id === oldDirectory.id);
            if (directory) {
              oldDirectory = directory;
            } else {
              allDirectories = [...allDirectories, oldDirectory];
            }
          });
          allDirectories.forEach(d => {
            d.expanded = allDirectories.some(x => x.parentId === d.id);
          });
          this.directories = [...allDirectories];
        } else {
          this.directories = directories;
        }
        this.field = {
          dataSource: this.directories, id: 'id', parentID: 'parentId',
          text: 'name', iconCss: 'icon', hasChildren: 'hasChild'
        };
        this.hideSpinner();
      }, err => this.showErrorMessage(err));

  }

  rowSelectedEvent(items: MediaItem[]) {
    this.store.dispatch(new AddSelectedMediaItems(items));
  }

  rowDeselectedEvent(items: MediaItem[]) {
    this.store.dispatch(new RemoveSelectedMediaItems(items));
  }
  toggleFavoriteEvent(item: MediaItem) {
    if (!item.isFavorite) {
      this.store.dispatch(new AddFavorite(item.documentId)).toPromise()
        .then(() => {
          this.store.dispatch(new RefetchMedia(this.currentPage, this.pageSize));
        });
    } else {
      this.store.dispatch(new RemoveFavorite(item.favoriteId)).toPromise()
        .then(() => {
          this.store.dispatch(new RefetchMedia(this.currentPage, this.pageSize));
        });
    }
  }

  onNodeEdited(args: NodeEditEventArgs) {
    const errorMessage = this.validateEdit(args);
    if (errorMessage) {
      args.cancel = true;
      this.store.dispatch(new ShowErrorMessage(errorMessage));
      this.currentId = null;
      if (!args.nodeData.id) {
        const targetNodeId = args.node.id;
        this.appTreeView.treeview.removeNodes([targetNodeId.toString()]);
      }
      return;
    }
    this.currentId = args.nodeData.id;
    if (!args.nodeData.id) {
      // Create Folder
      this.currentFolder.name = args.newText;
      this.currentFolder.id = null;
      this.currentFolder.parentId = Number(args.nodeData.parentID);
      this.createFolder(this.currentFolder);
    }
  }

  validateEdit(args: NodeEditEventArgs): string {
    let errorMessage: string;
    this.isFolderInvalid = false;
    if (args.newText.trim() === '') {
      errorMessage = 'Folder name should not be empty';
      this.isFolderInvalid = true;
    } else {
      const existingFolder = this.folders.find(
        f =>
          f.name.trim() === args.newText.trim() &&
          f.parentId === args.nodeData.parentID &&
          f.id !== args.nodeData.id
      );
      if (existingFolder) {
        errorMessage = `${args.newText.trim()} already exists. Please use another name.`;
        this.isFolderInvalid = true;
      }
    }
    return errorMessage;
  }
  onNodeDropped(args: DragAndDropEventArgs) {
    this.isDragEvent = true;
    this.currentId = args.draggedNodeData.id;
  }

  onNodeExpanding() {
    this.currentTarget = this.appTreeView.currentTarget;
    this.parentID = this.appTreeView.parentID;
    const parent = this.parentID;
    this.showSpinner();
    this.folderService
      .getFolders(Number(this.parentID))
      .toPromise()
      .then(
        folders => {
          folders.forEach(folder => (folder.icon = 'folder'));
          this.addFolders(folders);
          this.appTreeView.treeview.addNodes(
            JSON.parse(JSON.stringify(folders)),
            this.currentTarget
          );
          this.hideSpinner();
        },
        err => {
          this.showErrorMessage(err);
        }
      );
  }

  dataSourceChanged(args: DataSourceChangedEventArgs) {
    if (!this.currentId) {
      return;
    }
    const currentFolder = args.data.find(
      data => data.id === Number(this.currentId)
    );

    if (this.isDragEvent) {
      const errorMessage = this.validateMove(currentFolder);
      this.isDragEvent = false;
      if (errorMessage) {
        this.store.dispatch(new ShowErrorMessage(errorMessage));
        this.currentId = null;
        this.appTreeView.treeview.refresh();
        return;
      }
    }

    if (currentFolder) {
      const { id, name, parentId } = currentFolder;
      if (!name) {
        return;
      }
      this.expandedNodes = this.appTreeView.treeview.expandedNodes;
      if (id) {
        // Update Folder
        this.currentFolder.name = name.toString();
        this.currentFolder.id = Number(id);
        this.currentFolder.parentId = Number(parentId);
        this.updateFolder(this.currentFolder);
      }
    }
    this.currentId = null;
  }

  validateMove(folder: any): string {
    let errorMessage: string;

    const text = folder.name.toString();
    const parentId = folder.parentId || null;
    const existingFolder = this.folders.find(
      f =>
        f.name.trim() === text.trim() &&
        f.parentId === parentId &&
        f.id !== folder.id
    );

    if (existingFolder) {
      const parent = this.folders.find(f => f.id === parentId);
      const endText = parent ? ` in ${parent.name}` : '';
      errorMessage = `${text} cannot be moved. Name already exists${endText}.`;
    }
    return errorMessage;
  }

  updateFolder(folder: Directory) {
    this.subs.sink = this.store
      .dispatch(new UpdateFolder(folder.id, folder))
      .subscribe(
        () => {
          this.getFolders(this.expandedDirectories.length > 0);
        },
        () => this.appTreeView.treeview.refresh()
      );
  }

  createFolder(folder: Directory) {
    this.showSpinner();
    this.subs.sink = this.folderDataService.createDirectory(folder).subscribe(
      () => {
        this.hideSpinner();
        this.store.dispatch(
          new ShowSuccessMessage('Folder was successfully created.')
        );
        this.getFolders(this.expandedDirectories.length > 0);
      },
      error => {
        this.appTreeView.treeview.refresh();
        this.store.dispatch(new ShowErrorMessage(error));
      }
    );
  }

  onNodeSelected(args: any) {
    this.currentDirectoryId = Number(args.id);
    this.folderPath = '';
    this.buildDirectoryPath(this.currentDirectoryId);
    this.store.dispatch(new SaveSelectedMedia(this.currentDirectoryId));
    this.getMedia(this.currentDirectoryId);
  }
  getMedia(currentDirectoryId) {
    this.currentPage = 1;
    this.showSpinner();
    this.subs.sink = this.folderService
      .getMedia(currentDirectoryId, this.currentPage, this.pageSize)
      .subscribe(
        res => {
          this.media = res.data;
          this.totalMedia = res.pagination.total;
          this.hideSpinner();
        },
        error => {
          this.appTreeView.treeview.refresh();
          this.showErrorMessage(error);
          this.hideSpinner();
        }
      );
  }
  downloadGridEvent(data: MediaItem) {
    this.store.dispatch(new DownloadMediaItem(data.name, data.url));
  }
  editGridEvent(data: MediaItem) {
    this.router.navigate([`media/${data.id}/details`]);
  }

  collapseAll() {
    this.appTreeView.treeview.collapseAll();
  }

  onChangePage(pageNumber: number) {
    this.folderService
      .getMedia(this.currentDirectoryId, pageNumber, this.pageSize).toPromise()
      .then(
        res => {
          this.media = res.data;
          this.hideSpinner();
        },
        error => {
          console.log('media');

          this.appTreeView.treeview.refresh();
          this.showErrorMessage(error);
          this.hideSpinner();
        }
      );
  }
  ngOnDestroy() {
    this.subs.unsubscribe();
    const expandedNodes = this.appTreeView.treeview.expandedNodes.map(x => Number(x));
    let expandedDirectories: Directory[] = [];
    const treeData = this.appTreeView.treeview.getTreeData();
    let excludedIds: number[] = [];
    treeData.forEach(data => {
      const directory: Directory = data as any;
      // if directory is expanded, get all it's children;
      // if directory is not expanded, exclude all it's children
      if (!directory.expanded) {
        excludedIds = [...excludedIds, directory.id];
      }
      const isExcludedDirectory = excludedIds.includes(directory.id);
      if (isExcludedDirectory) {
        const childrenDirectoryIds = this.directories.filter(d => d.parentId === directory.id).map(x => x.id);
        excludedIds = [...excludedIds, ...childrenDirectoryIds];
      }
    });
    this.directories.forEach(directory => {
      // if it's in expandedNodes && it's parent is also in expanded nodes; then get all it's kids
      // if it's root directory => parentId = null
      if (expandedNodes.includes(directory.id) && (expandedNodes.includes(directory.parentId) ||
       !directory.parentId) && !excludedIds.includes(directory.parentId)) {
        const childrenDirectories = this.directories.filter(d => d.parentId === directory.id);
        const expandedDirectoryExists = expandedDirectories.some(x => x.id === directory.id);
        if (!expandedDirectoryExists) {
          expandedDirectories = [...expandedDirectories, directory];
        }
        childrenDirectories.forEach(childDirectory => {
          const childDirectoryExists = expandedDirectories.some(x => x.id === childDirectory.id);
          if (!childDirectoryExists) {
            expandedDirectories = [...expandedDirectories, childDirectory];
          }
        });
      }
    });
    this.store.dispatch(new SaveExpandedDirectories(expandedDirectories));
  }

  private addFolders(directories: Directory[]) {
    directories.forEach(directory => {
      const folderExists = this.directories.some(f => f.id === directory.id);
      if (folderExists === false) {
        this.directories = [...this.directories, directory];
      }
    });
  }

  private buildDirectoryPath(directoryId: number) {
    const directory = this.directories.find(x => x.id === directoryId);
    const parent = this.directories.find(x => x.id === directory.parentId);
    if (parent) {
      this.folderPath = this.folderPath ? `${directory.name} > ${this.folderPath}` : `${directory.name}`;
      return this.buildDirectoryPath(parent.id);
    }
    return this.folderPath = this.folderPath ? `${directory.name} > ${this.folderPath}` : `${directory.name}`;
  }
}
