import { ResetUploadStatus } from './../state/media/media.action';
import { Component, OnInit, ViewChild, OnDestroy, ElementRef } from '@angular/core';
import { DynamicFormComponent } from 'src/app/shared/dynamic-components/components/dynamic-form.component';
import { Store, Select } from '@ngxs/store';
import { MediaState } from '../state/media/media.state';
import { FieldConfiguration } from 'src/app/shared/dynamic-components/field-setting';
import { BaseComponent } from 'src/app/shared/base/base.component';
import { Directory } from 'src/app/core/models/entity/directory';
import { AppState } from 'src/app/state/app.state';
import { GetAzureUploadConfiguration, GetAzureSASToken } from 'src/app/state/app.actions';
import { MediaUploadService } from './media-upload.service';
import { Router } from '@angular/router';
import { GridColumn } from 'src/app/core/models/grid.column';
import { SubSink } from 'subsink/dist/subsink';
import { Observable } from 'rxjs/internal/Observable';
import { MetadataDataType } from 'src/app/core/enum/metadata-data-type';
import { DataManager } from '@syncfusion/ej2-data';
import { DirectoryService } from 'src/app/core/services/business/directory/directory.service';
import { DataService } from './data-service';
import { TreeGridComponent } from 'src/app/shared/tree-grid/tree-grid.component';
import { SASTokenType } from 'src/app/core/enum/azure-sas-token';

const BROWSE = 'Browse';
const CHANGE = 'Change';

@Component({
  selector: 'app-media-upload',
  templateUrl: './media-upload.component.html',
  styleUrls: ['./media-upload.component.css'],
  providers: [MediaUploadService]
})
export class MediaUploadComponent extends BaseComponent implements OnInit, OnDestroy {

  private subs = new SubSink();
  fileName: string;
  uploadButtonText = BROWSE;
  dataSource: any;
  isFileSelected: boolean;
  isDestinationSelected: boolean;
  selectedFile: File;
  currentFolderId: number;
  folderPath: string;
  directories: Directory[];
  columns: GridColumn[] = [{ headerText: 'Name', field: 'name' }];
  sasToken: any;
  storageAccount: any;
  containerName: any;
  selectionOptions: Object;
  metadata: FieldConfiguration[] = [];
  previousRoute: string;
  isSRAllowed = false;

  @ViewChild('file') file: ElementRef;
  @ViewChild(DynamicFormComponent) dynamicForm: DynamicFormComponent;
  @ViewChild('appTreeGrid') treegrid: TreeGridComponent;

  @Select(AppState.getSpinnerVisibility) showSpinner$: Observable<boolean>;
  @Select(AppState.getAzureContainer) container$: Observable<string>;
  @Select(AppState.getAzureStorageAccount) storageAccount$: Observable<string>;
  @Select(AppState.getPreviousRoute) previousRoute$: Observable<string>;
  @Select(AppState.getMediaWriteSASToken) azureWriteSASToken$: Observable<string>;
  @Select(MediaState.getUploadCompleteStatus) uploadComplete$: Observable<boolean>;
  @Select(MediaState.getDirectories) directories$: Observable<Directory[]>;
  @Select(MediaState.getDirectoryMetadata) directoryMetadata$: Observable<any[]>;
  @Select(MediaState.getMediaTreeData) mediaData$: Observable<any[]>;

  public data1: DataManager;
  public pageSetting: Object;

  data: Observable<any>;
  state: any;
  pageSettings: Object;

  constructor(protected store: Store, private router: Router,
              private mediaUploadService: MediaUploadService, private foldersService: DirectoryService,
              private dataService: DataService) {
    super(store);
    this.hideLeftNav();
    this.data = dataService;
  }

  dataStateChange(state: any) {
    if (state.requestType === 'expand') {
      const folder = state.data as Directory;
      this.treegrid.treegrid.showSpinner();
      this.foldersService.getFolders(folder.id)
        .subscribe(folders => {
          const oldIds = this.dataService.directories.map(x => x.id);
          folders = folders.filter(x => !oldIds.includes(x.id));
          state.childData = folders;
          state.childDataBind();
          this.treegrid.treegrid.hideSpinner();
        }, err => {
          this.showErrorMessage(err);
          this.treegrid.treegrid.hideSpinner();
        });
    } else {
      this.dataService.execute();
    }
  }

  ngOnInit() {
    this.dataService.execute();
    this.selectionOptions = {
      mode: 'Row', cellSelectionMode: 'Flow', type: 'Single', checkboxOnly: false, persistSelection: false,
      checkboxMode: 'ResetOnRowClick', enableSimpleMultiRowSelection: true, enableToggle: false
    };

    if (this.dynamicForm) { this.dynamicForm.form.reset(); }
    this.store.dispatch(new GetAzureUploadConfiguration());
    this.store.dispatch(new GetAzureSASToken(SASTokenType.Write));
    this.subs.add(
      this.uploadComplete$
        .subscribe(complete => {
          if (complete) { this.router.navigate(['/media/all'], { queryParams: { view: 'tile' } }); }
        }),
      this.azureWriteSASToken$
        .subscribe(SASToken => this.sasToken = SASToken),
      this.container$
        .subscribe(container => this.containerName = container),
      this.storageAccount$
        .subscribe(storageAccount => this.storageAccount = storageAccount),
      this.previousRoute$
        .subscribe(route => {
          const default_route = '/media/all'; // in case a user gets to this page other than by routing from the previous page
          if (route.includes('/media/all') || route.includes('/media/favorites') || route.includes('media/archive')) {
            this.previousRoute = route;
          } else {
            this.previousRoute = default_route;
          }
        })
    );
  }

  ngOnDestroy() {
    this.store.dispatch(new ResetUploadStatus());
    this.metadata = [];
    this.subs.unsubscribe();
  }

  onBack(route: string) {
    if (typeof route !== 'string') { return; }
    const url = route.split('?')[0];
    this.router.navigate([url]);
  }

  addFiles() {
    this.file.nativeElement.click();
  }

  onFilesAdded() {
    const files: { [key: string]: File } = this.file.nativeElement.files;
    this.selectedFile = files[0] ? files[0] : this.selectedFile;
    if (this.selectedFile) {
      this.uploadButtonText = CHANGE;
      this.isFileSelected = true;
    }
  }

  changeDestination() {
    this.isDestinationSelected = false;
  }

  rowSelected(args: any) {
    this.showSpinner();
    const data = args.data;
    if (data) {
      this.isDestinationSelected = true;
      this.currentFolderId = data.id;
      this.folderPath = '';
      this.buildFolderPath(data.id);
      if (this.dynamicForm) { this.dynamicForm.form.reset(); }
      this.metadata = [];
      this.mediaUploadService.getFolderMetadataFields(data.id)
        .then(metadata => {
          this.metadata = metadata;
          this.loadDependentDropdowns();
          this.hideSpinner();
        }, err => this.showErrorMessage(err));
    } else {
      this.hideSpinner();
    }
  }

  private loadDependentDropdowns() {
    const childrenFields = this.metadata.filter(field => field.parentId);
    if (childrenFields) {
      childrenFields.forEach(child => {
        // find the selected value of parent
        const parent = this.metadata.find(x => x.id === child.parentId);
        if (parent) {
          const parentOption = parent.options.find(x => x.value === parent.value);
          if (parentOption) {
            child.isEnabled = parentOption.id ? true : false;
            child.options = child.isEnabled ? child.data.options.filter(x => x.parentId === parentOption.id) : [];
          }
        }
      });
    }
  }

  checkValue(event: any) {
    this.isSRAllowed = event.checked;
  }

  upload() {
    const retVal: any = {};
    if (this.dynamicForm) {
      if (!this.dynamicForm.valid) { return; }
      this.dynamicForm.controls.forEach(config => {
        const dataType: string = config.data.dataType || MetadataDataType.String;
        retVal[config.name] = dataType === MetadataDataType.Int32 || dataType === MetadataDataType.Int64 ||
          dataType === MetadataDataType.Double ? Number(config.value) :
          dataType === MetadataDataType.Date ? new Date(config.value) :
            config.value;
      });
    }

    const metadata = this.dynamicForm ? JSON.stringify(retVal) : '{}';
    this.showSpinner();
    this.mediaUploadService.upload(this.currentFolderId, this.isSRAllowed,
      this.selectedFile, metadata, this.containerName, this.sasToken, this.storageAccount);
  }

  private buildFolderPath(directoryId: number) {
    const directory = this.dataService.directories.find(x => x.id === directoryId);
    const parent = this.dataService.directories.find(x => x.id === directory.parentId);
    if (parent) {
      this.folderPath = this.folderPath ? `${directory.name} > ${this.folderPath}` : `${directory.name}`;
      return this.buildFolderPath(parent.id);
    }
    return this.folderPath = this.folderPath ? `${directory.name} > ${this.folderPath}` : `${directory.name}`;
  }
}
