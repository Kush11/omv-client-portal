import { ShowSuccessMessage } from './../../state/app.actions';
import { Injectable } from "@angular/core";
import { Validators, ValidatorFn } from "@angular/forms";
import { FieldConfiguration } from "src/app/shared/dynamic-components/field-setting";
import { MetadataFieldType } from 'src/app/core/enum/metadataFieldType';
import { BlobService, UploadParams } from 'angular-azure-blob-service'
import { MediaItem } from 'src/app/core/models/entity/media';
import { Store } from '@ngxs/store';
import { CreateMediaItem } from '../state/media/media.action';
import { ShowErrorMessage } from 'src/app/state/app.actions';
import { MetadataField } from 'src/app/core/models/entity/metadata-field';
import { DirectoryService } from 'src/app/core/services/business/directory/directory.service';
import { MetadataDataType } from 'src/app/core/enum/metadata-data-type';

@Injectable({
  providedIn: "root"
})
export class MediaUploadService {

  config: any;
  percent: any;
  sasToken: any;
  storageAccount: any;
  containerName: any;

  constructor(private foldersService: DirectoryService, private store: Store, private blob: BlobService) { }

  upload(directoryId: number, isSRAllowed: boolean, file: File, metadata: string, containerName: string, sasToken: string, storageAccount: string) {
    const Config: UploadParams = {
      sas: sasToken,
      storageAccount: storageAccount,
      containerName: containerName
    };
    let splitByLastDot = function (text) {
      const index = text.lastIndexOf('.');
      return [text.slice(0, index), text.slice(index + 1)]
    }
    if (file !== null) {
      const baseUrl = this.blob.generateBlobUrl(Config, file.name);
      const customBlockSize = this.getBlockSize(file.size);
      this.config = {
        baseUrl: baseUrl,
        sasToken: Config.sas,
        blockSize: customBlockSize,
        file: file,
        complete: () => {
          let mediaItem = new MediaItem();
          mediaItem.metadata = metadata;
          mediaItem.size = file.size;
          mediaItem.name = file.name;
          mediaItem.contentType = file.type;
          mediaItem.directoryId = directoryId;
          mediaItem.url = this.config.baseUrl;
          mediaItem.documentTypeCode = splitByLastDot(file.name).pop().toUpperCase();
          mediaItem.isSRAllowed = isSRAllowed;
          this.store.dispatch(new CreateMediaItem(mediaItem));
        },
        error: (err: { statusText: any; }) => {
          this.store.dispatch(new ShowErrorMessage(err.statusText));
        },
        progress: (percent: any) => {
          this.percent = percent;
          console.log(percent);
        }
      };
      this.blob.upload(this.config);
    }
  }

  getFolderMetadataFields(folderId: number) {
    let metadataFields: FieldConfiguration[] = [];
    return this.foldersService.getMetadataFields(folderId)
      .then(response => {
        response.forEach(metadataField => {
          if (metadataField) {
            let field: FieldConfiguration;
            switch (metadataField.type) {
              case MetadataFieldType.Text:
                field = this.buildTextBox(metadataField);
                break;
              case MetadataFieldType.Select:
                field = this.buildDropdown(metadataField);
                break;
              case MetadataFieldType.Date:
                field = this.buildDate(metadataField);
                break;
              case MetadataFieldType.Nested:
                field = this.buildNestedDropdown(metadataField);
                break;
            }
            metadataFields = [...metadataFields, field];
          }
        });
        return metadataFields.sort((a, b) => a.order - b.order);
      });
  }

  private buildTextBox(item: MetadataField): FieldConfiguration {
    return {
      id: item.id,
      data: item,
      type: "input",
      cssClass: 'col-md-6',
      label: item.label || item.name,
      inputType: (item.dataType === MetadataDataType.Int32
        || item.dataType === MetadataDataType.Int64
        || item.dataType === MetadataDataType.Double) ? 'number' : 'text',
      name: item.name,
      order: item.order,
      placeholder: item.name,
      value: !item.dataType ? item.value || '' :
        (item.dataType === MetadataDataType.Int32 ||
          item.dataType === MetadataDataType.Int64 ||
          item.dataType === MetadataDataType.Double) ? Number(item.value) : item.value || '',
      validations: this.getValidations(item)
    };
  }

  private buildDropdown(item: MetadataField): FieldConfiguration {
    return {
      id: item.id,
      data: item,
      type: "select",
      cssClass: 'col-md-6',
      label: item.label || item.name,
      name: item.name,
      order: item.order,
      options: item.options,
      value: item.value ? item.value : '',
      placeholder: 'Select',
      validations: this.getValidations(item)
    };
  }

  private buildNestedDropdown(item: MetadataField): FieldConfiguration {
    return {
      id: item.id,
      data: item,
      parentId: item.parentId,
      type: "select",
      cssClass: 'col-md-6',
      label: item.label || item.name,
      name: item.name,
      order: item.order,
      options: item.options,
      value: item.value ? item.value : '',
      placeholder: 'Select',
      validations: this.getValidations(item),
      isRequired: item.isRequired,
      isNested: true,
      isEnabled: false
    };
  }

  private buildDate(item: MetadataField): FieldConfiguration {
    return {
      id: item.id,
      data: item,
      type: "date",
      cssClass: 'col-md-6',
      label: item.label || item.name,
      name: item.name,
      order: item.order,
      value: new Date(item.value || ''),
      validations: this.getValidations(item)
    };
  }

  private getValidations(item: MetadataField): ValidatorFn[] {
    let validations: ValidatorFn[] = [];
    if (item.isRequired) {
      let requiredValidation: any = {
        name: 'required',
        validator: Validators.required,
        message: `${item.name} is required`
      };
      validations.push(requiredValidation);
    }
    return validations;
  }

  private getBlockSize(filesize: number) {
    let customBlockSize = 0;
    const size10Gb = 1024 * 1024 * 1024 * 10;
    const size500Mb = 1024 * 1024 * 500;
    const size32Mb = 1024 * 1024 * 32;
    const size5mb = 1024 * 1024 * 5;
    if (filesize < size5mb) {
      customBlockSize = (1024 * 32);
    } else if (filesize < size32Mb) {
      customBlockSize = (1024 * 512);
    } else if (filesize < size500Mb) {
      customBlockSize = (1024 * 1024 * 4);
    } else if (filesize < size10Gb) {
      customBlockSize = (1024 * 1024 * 10);
    } else {
      customBlockSize = (1024 * 1024 * 100);
    }
    return customBlockSize;
  }
}
