import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { DirectoryService } from 'src/app/core/services/business/directory/directory.service';
// import { DataStateChangeEventArgs } from '@syncfusion/ej2-treegrid';
import { map } from 'rxjs/internal/operators/map';
import { Directory } from 'src/app/core/models/entity/directory';

@Injectable({
  providedIn: "root"
})
export class DataService extends Subject<Object> {

  directories: Directory[] = [];

  constructor(private foldersService: DirectoryService) {
    super();
  }

  public getData(): Observable<any> {
    return this.foldersService.getFolders()
      .pipe(
        map((response: any) => {
          this.directories = response;
          console.log('form data service', this.directories);
          return (<any>{
            result: response,
            count: parseInt('8', 10)
          });
        })
      );
  }

  public execute(): void {
    this.getData()
      .subscribe(x => {
        super.next(x);
      });
  }
}
