import { TestBed } from '@angular/core/testing';

import { ReportIssuesUploadService } from './report-issues-upload.service';

describe('ReportIssuesUploadService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ReportIssuesUploadService = TestBed.get(ReportIssuesUploadService);
    expect(service).toBeTruthy();
  });
});
