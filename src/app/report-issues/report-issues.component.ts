import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { AppState } from '../state/app.state';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { SubSink } from 'subsink/dist/subsink';
import { ShowWarningMessage, GetAzureSASToken } from '../state/app.actions';
import { FormGroup, FormBuilder, Validators, FormArray, FormControl, ValidatorFn } from '@angular/forms';
import { Ticket } from '../core/models/entity/tickets';
import { CreateReportIssue } from '../state/tickets/tickets.actions';
import { ReportIssuesUploadService } from './report-issues-upload.service';
import { ChangeArgs } from '@syncfusion/ej2-angular-buttons';
import { ChangeEventArgs } from '@syncfusion/ej2-angular-calendars';
import { SASTokenType, SASTokenModule } from '../core/enum/azure-sas-token';
import { BaseComponent } from '../shared/base/base.component';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-report-issues',
  templateUrl: './report-issues.component.html',
  styleUrls: ['./report-issues.component.css']
})
export class ReportIssuesComponent extends BaseComponent implements OnInit, OnDestroy {

  private subs = new SubSink();

  @ViewChild('file') file: any;

  @Select(AppState.getPreviousRoute) previousRoute$: Observable<string>;
  @Select(AppState.getAzureStorageAccount) storageAccount$: Observable<string>;
  @Select(AppState.getCoreWriteSASToken) sasToken$: Observable<string>;

  previousRoute: string;
  reportForm: FormGroup;
  isFileSelected: boolean;
  selectedFiles: any[] = [];
  availability: any[] = [
    { name: 'Monday', value: 0 },
    { name: 'Tuesday', value: 1 },
    { name: 'Wednesday', value: 2 },
    { name: 'Thursday', value: 3 },
    { name: 'Friday', value: 4 },
    { name: 'Saturday', value: 5 },
    { name: 'Sunday', value: 6 }
  ];
  allowFollowUp: boolean;
  fileAddedText: string;
  minValue: Date = new Date('10/8/2017 12:00 AM');
  maxValue: Date = new Date('10/8/2017 11:30 PM');

  startMinValue: Date;
  startMaxValue: Date;
  endMinValue: Date;
  endMaxValue: Date;

  storageAccount: any;
  sasToken: any;
  containerName = environment.issuesContainer;

  constructor(protected router: Router, protected store: Store, private fb: FormBuilder, private uploadService: ReportIssuesUploadService) {
    super(store);
    this.hideLeftNav();
    this.reportForm = this.fb.group({
      subject: ['', [Validators.required]],
      message: ['', [Validators.required]],
      isFollowUp: ['0']
    });
    this.fileAddedText = '';
  }

  ngOnInit() {
    this.hideSpinner();
    this.store.dispatch(new GetAzureSASToken(SASTokenType.Write, SASTokenModule.Core));
    this.subs.add(
      this.previousRoute$
        .subscribe(route => {
          const default_route = '/dashboard'; // in case a user gets to this page other than by routing from the previous page
          if (route.includes(default_route)) {
            this.previousRoute = route;
          } else {
            this.previousRoute = default_route;
          }
        }),
      this.storageAccount$
        .subscribe(storageAccount => this.storageAccount = storageAccount),
      this.sasToken$
        .subscribe(sasToken => this.sasToken = sasToken),
        this.sasToken$
        .subscribe(SASToken => this.sasToken = SASToken),
    );
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

  onBack(route: string) {
    this.router.navigate([route]);
  }

  onFollowupChanged(args: ChangeArgs) {
    this.allowFollowUp = args.value === '1';
    if (this.allowFollowUp) {
      this.addScheduleGroup();
    } else {
      this.removeScheduleGroup();
    }
  }

  onStartTimeChanged(args: ChangeEventArgs) {
    if (args.value) { this.endMinValue = args.value; }
    else { this.endMinValue = this.minValue; }
  }

  onEndTimeChanged(args: ChangeEventArgs) {
    if (args.value) { this.startMaxValue = args.value; }
    else { this.startMaxValue = this.maxValue; }
  }

  save() {
    this.showSpinner();
    if (this.reportForm.valid) {
      if (this.reportForm.dirty) {
        const report: Ticket = { ...this.reportForm.value };
        if (report.isFollowUp == '1') {
          report.followUp = true;

          const startTime = this.reportForm.get('startTime').value;
          const startTimeString = startTime.toString();
          const endTime = this.reportForm.get('endTime').value;
          const endTimeString = endTime.toString();

          if (startTimeString === endTimeString) {
            this.hideSpinner();
            this.store.dispatch(new ShowWarningMessage('Start Time should be greater than End Time'));
            return;
          }

          let selectedDays = [];


          report.availability.forEach((available, index) => {
            const val = this.availability.find(a => a.value === index);
            if (val && available) {
              selectedDays.push(val.name);
            }
          });
          report.availability = selectedDays;
        } else {
          report.followUp = false;
        }

        if (this.selectedFiles.length > 0) {
          this.uploadService.uploadFile(1, this.selectedFiles, this.containerName, environment.storageAccount, this.sasToken, report);
        } else {
          this.store.dispatch(new CreateReportIssue(report));
        }

        this.reportForm.reset();
        this.reportForm.patchValue({
          isFollowUp: '0'
        });
        this.removeScheduleGroup();
        this.allowFollowUp = false;
        this.selectedFiles = [];
        this.fileAddedText = '';
        this.fileAddedText = '';
      }
    }
  }

  onFilesAdded() {
    this.fileAddedText = '';
    const files = this.file.nativeElement.files;
    const mappedFiles = Object.keys(files).map(key => files[key]);
    this.selectedFiles = mappedFiles;
    const fileLength = files.length;
    this.fileAddedText = `${fileLength} ${fileLength > 1 ? 'files' : 'file'} Added`;
  }

  addFiles() {
    this.file.nativeElement.click();
  }

  private addScheduleGroup() {
    const daysArray = this.fb.array([]);
    this.reportForm.addControl('availability', daysArray);
    this.reportForm.addControl('startTime', new FormControl('', Validators.required));
    this.reportForm.addControl('endTime', new FormControl('', Validators.required));
    this.createDays();
  }

  private removeScheduleGroup() {
    this.reportForm.removeControl('availability');
    this.reportForm.removeControl('startTime');
    this.reportForm.removeControl('endTime');
  }

  private createDays() {
    this.availability.map((d, i) => {
      const control = new FormControl(false); // if first item set to true, else false
      (this.reportForm.controls.availability as FormArray).push(control);
    });
  }
}

function daysValidator(min = 1) {
  const validator: ValidatorFn = (formArray: FormArray) => {
    const totalSelected = formArray.controls
      // get a list of checkbox values (boolean)
      .map(control => control.value)
      // total up the number of checked checkboxes
      .reduce((prev, next) => next ? prev + next : prev, 0);

    // if the total is not greater than the minimum, return the error message
    return totalSelected >= min ? null : { required: true };
  };

  return validator;
}
