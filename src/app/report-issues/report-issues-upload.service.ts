import { Injectable } from '@angular/core';
import { UploadParams, BlobService } from 'angular-azure-blob-service';
import { Store } from '@ngxs/store';
import { Ticket } from '../core/models/entity/tickets';
import { Document } from '../core/models/entity/document';
import { ShowErrorMessage } from '../state/app.actions';
import { CreateReportIssue } from '../state/tickets/tickets.actions';

@Injectable({
  providedIn: 'root'
})
export class ReportIssuesUploadService {

  config: any;
  percent: any;
  fileToUpload: any[] = [];
  constructor(private blob: BlobService, private store: Store) { }

  uploadFile(directoryId: number, files: any[], containerName: string, storageAccount: string, sasToken: string, report: Ticket) {
    const Config: UploadParams = {
      sas: sasToken,
      storageAccount: storageAccount,
      containerName: containerName
    };
    let splitByLastDot = function (text) {
      const index = text.lastIndexOf('.');
      return [text.slice(0, index), text.slice(index + 1)]
    };
    let uploadCount = 0;
    if (files != null) {
      files.forEach((file, i) => {

        const baseUrl = this.blob.generateBlobUrl(Config, file.name);
        this.config = {
          baseUrl: baseUrl,
          sasToken: Config.sas,
          blockSize: 1024 * 64, // OPTIONAL, default value is 1024 * 32
          file: file,
          complete: () => {
            let thumbnail = `https://${Config.storageAccount}.blob.core.windows.net/thumbs/${file.name}`;
            let document = new Document;
            const index = i + 1;
            document.size = file.size;
            document.name = file.name;
            document.directoryId = directoryId;
            document.contentType = file.type;
            document.documentUrl = this.config.baseUrl;
            document.documentTypeCode = splitByLastDot(file.name).pop().toUpperCase();
            document.requester = 1;
            document.thumbnailContainerUrl = thumbnail;
            this.fileToUpload.push(document);
            uploadCount++;

            if (uploadCount === files.length) this.store.dispatch(new CreateReportIssue(report, this.fileToUpload));
          },
          error: (err: { statusText: any; }) => {
            this.store.dispatch(new ShowErrorMessage(err.statusText));
          },
          progress: (percent: any) => {
            this.percent = percent;
          }
        };
        this.blob.upload(this.config);
      });
    } else {
      this.store.dispatch(new CreateReportIssue(report));
    }
  }
}
