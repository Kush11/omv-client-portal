import { CollaborationRoom, CollaborationSession, CollaborationScreenshot } from 'src/app/core/models/entity/collaboration';
import { State, Selector, Action, StateContext, Store } from '@ngxs/store';
import {
  GetCollaborationDays, ClearCollaborations, GetCollaborationSessions, ClearRoom, SetBreadcrumbs, UpdateBreadcrumb, JoinRoom, GetLiveStreamAssets, GetLiveStreamGroups, GetLiveStreams,
  GetUsers, GetGroups, AddSessionVideo, RemoveSessionVideo, SetCollaborationSession, ClearCollaborationSession,
  AddParticipant, RemoveParticipant, SaveCollaborationSession, LeaveRoom, GetCollaborationArchive, GetCollaborationSession, CreateCollaborationScreenShot, GetCollaborationScreenShots, DeleteCollaborationSession, AddFilterTag, RemoveFilterTag, DeleteCollaborationScreenShot, IncrementScreenshots, SetScreenshotCreated, SaveExpandedDirectories, ClearExpandedDirectories
} from './collaborations.action';
import { CollaborationsService } from 'src/app/core/services/business/collaborations/collaborations.service';
import { HideSpinner, ShowErrorMessage, ShowSpinner, ShowSuccessMessage } from 'src/app/state/app.actions';
import { tap } from 'rxjs/internal/operators/tap';
import { MediaItem } from 'src/app/core/models/entity/media';
import { User } from 'src/app/core/models/entity/user';
import { LiveStream } from '../../core/models/entity/live-stream';
import { Breadcrumb } from 'src/app/core/models/breadcrumb';
import { Lookup } from 'src/app/core/models/entity/lookup';
import { Group } from 'src/app/core/models/entity/group';
import { LookupService } from 'src/app/core/services/business/lookup/lookup.service';
import { LiveStreamService } from 'src/app/core/services/business/live-stream/live-stream.service';
import { UsersService } from 'src/app/core/services/business/users/users.service';
import { GroupsService } from 'src/app/core/services/business/groups/groups.service';
import { GroupStatus } from 'src/app/core/enum/group-status.enum';
import { AppState } from 'src/app/state/app.state';
import { Tag } from 'src/app/core/models/entity/tag';
import { Directory } from 'src/app/core/models/entity/directory';

export class CollaborationsStateModel {
  breadcrumbs: Breadcrumb[];

  // Collaboration Session
  currentSession: CollaborationSession;
  users: User[];
  totalUsers: number;
  selectedParticipants: User[];
  groups: Group[];
  livestreamAssets: Lookup[];
  livestreamGroups: Lookup[];
  livestreams: LiveStream[];
  totalLiveStreams: number;
  selectedSessionVideos: LiveStream[];
  expandedDirectories: Directory[];

  currentDateFilter: Date;
  collaborationDays: number[];
  collaborationSessions: CollaborationSession[];
  upcomingSessions: number;

  // Filters
  filterTags: Tag[];

  // Collaboration Room
  currentRoom: CollaborationRoom;
  screenshotNameCounter: number;
  screenshotCreated: boolean;

  dailyCollaborations: CollaborationSession[];
  currentCollaboration: CollaborationSession;
  collaborationItems: LiveStream[];

  selectedCollaborationItems: LiveStream[];
  isSelectedCollaborationItemsCleared: boolean;
  participants: User[];
  selectedParticipantsIds: number[];
  selectedParticipantsCount: number;
  filteredParticipants: User[];
  newTitle: string;
  newDate: Date;
  startTime: string;
  endTime: string;
  filterName: string;
  filterType: string;
  roles: string[];
  collaborationArchiveItems: MediaItem[];
  isSessionCreationComplete: boolean;
  activeRoom: CollaborationRoom;
  roomScreenshots: CollaborationScreenshot[];

  // Collaboration chat

}

@State<CollaborationsStateModel>({
  name: 'collaborations',
  defaults: {
    breadcrumbs: [],

    currentSession: null,
    users: [],
    totalUsers: 0,
    selectedParticipants: [],
    groups: [],
    livestreamAssets: [],
    livestreamGroups: [],
    livestreams: [],
    totalLiveStreams: 0,
    selectedSessionVideos: [],
    expandedDirectories: [],

    currentDateFilter: new Date(),
    collaborationDays: null,
    collaborationSessions: [],
    upcomingSessions: 0,

    filterTags: [],

    currentRoom: null,
    screenshotNameCounter: 1,
    screenshotCreated: false,

    dailyCollaborations: [],
    currentCollaboration: null,
    collaborationItems: [],
    participants: [],
    selectedParticipantsIds: [],
    selectedParticipantsCount: 0,
    filteredParticipants: [],
    selectedCollaborationItems: [],
    isSelectedCollaborationItemsCleared: false,
    newTitle: '',
    newDate: new Date(),
    startTime: '',
    endTime: '',
    filterName: '',
    filterType: '',
    roles: [],
    collaborationArchiveItems: [],
    isSessionCreationComplete: false,
    activeRoom: null,
    roomScreenshots: [],
  }
})
export class CollaborationsState {

  //#region S E L E C T O R S

  //#region Breadcrumb

  @Selector()
  static getBreadcrumbs(state: CollaborationsStateModel) {
    return state.breadcrumbs;
  }

  //#endregion



  //#region Collaboration Session

  @Selector()
  static getCurrentSession(state: CollaborationsStateModel) {
    return state.currentSession;
  }

  @Selector()
  static getUsers(state: CollaborationsStateModel) {
    return state.users;
  }

  @Selector()
  static getTotalUsers(state: CollaborationsStateModel) {
    return state.totalUsers;
  }

  @Selector()
  static getSelectedUsers(state: CollaborationsStateModel) {
    return state.selectedParticipants;
  }

  @Selector()
  static getGroups(state: CollaborationsStateModel) {
    return state.groups;
  }

  @Selector()
  static getLiveStreamAssets(state: CollaborationsStateModel) {
    return state.livestreamAssets;
  }

  @Selector()
  static getLiveStreamGroups(state: CollaborationsStateModel) {
    return state.livestreamGroups;
  }

  @Selector()
  static getLiveStreams(state: CollaborationsStateModel) {
    return state.livestreams;
  }

  @Selector()
  static getTotalLiveStreams(state: CollaborationsStateModel) {
    return state.totalLiveStreams;
  }

  @Selector()
  static getSelectedSessionVideos(state: CollaborationsStateModel) {
    return state.selectedSessionVideos;
  }

  @Selector()
  static getExpandedDirectories(state: CollaborationsStateModel) {
    return state.expandedDirectories;
  }

  //#endregion

  //#region collaborations

  @Selector()
  static getCurrentDateFilter(state: CollaborationsStateModel) {
    return state.currentDateFilter;
  }

  @Selector()
  static getCollaborationDays(state: CollaborationsStateModel) {
    return state.collaborationDays;
  }

  @Selector()
  static getUpcomingSessionsCount(state: CollaborationsStateModel) {
    return state.upcomingSessions;
  }

  @Selector()
  static getCollaborationSessions(state: CollaborationsStateModel) {
    return state.collaborationSessions;
  }

  @Selector()
  static getDailyCollaborations(state: CollaborationsStateModel) {
    return state.dailyCollaborations;
  }

  @Selector()
  static getCurrentCollaboration(state: CollaborationsStateModel) {
    return state.currentCollaboration;
  }

  @Selector()
  static getCurrentCollaborationItem(state: CollaborationsStateModel) {
    return state.currentCollaboration;
  }

  @Selector()
  static getScreenshotNameCounter(state: CollaborationsStateModel) {
    return state.screenshotNameCounter;
  }

  @Selector()
  static getScreenshotCreated(state: CollaborationsStateModel) {
    return state.screenshotCreated;
  }

  //#endregion

  //#region Filters

  @Selector()
  static getFilterTags(state: CollaborationsStateModel) {
    return state.filterTags;
  }

  //#endregion

  //#region collaboration items

  @Selector()
  static getCollaborationItems(state: CollaborationsStateModel) {
    return state.collaborationItems;
  }
  @Selector()
  static getSelectedCollaborationItems(state: CollaborationsStateModel) {
    return state.selectedCollaborationItems;
  }
  @Selector()
  static isSelectedCollaborationItemCleared(state: CollaborationsStateModel) {
    return state.isSelectedCollaborationItemsCleared;
  }

  //#endregion

  //#region participants

  @Selector()
  static getParticipants(state: CollaborationsStateModel) {
    return state.participants;
  }
  @Selector()
  static getSelectedParticipantCount(state: CollaborationsStateModel) {
    return state.selectedParticipantsCount;
  }
  @Selector()
  static getSelectedParticipantsIds(state: CollaborationsStateModel) {
    return state.selectedParticipantsIds;
  }
  @Selector()
  static getSelectedParticipants(state: CollaborationsStateModel) {
    return state.selectedParticipants;
  }
  @Selector()
  static getFilteredParticipants(state: CollaborationsStateModel) {
    return state.filteredParticipants;
  }
  //#endregion

  //#region create new collaboration

  @Selector()
  static getNewSessionCreationStatus(state: CollaborationsStateModel) {
    return state.isSessionCreationComplete;
  }

  //#endregion

  //#region new collaboration form

  @Selector()
  static getTitle(state: CollaborationsStateModel) {
    return state.newTitle;
  }
  @Selector()
  static getNewDate(state: CollaborationsStateModel) {
    return state.newDate;
  }
  @Selector()
  static getStartTime(state: CollaborationsStateModel) {
    return state.startTime;
  }
  @Selector()
  static getEndTime(state: CollaborationsStateModel) {
    return state.endTime;
  }
  //#endregion

  //#region roles

  @Selector()
  static getRoles(state: CollaborationsStateModel) {
    return state.roles;
  }

  //#endregion

  //#region archive

  @Selector()
  static getCollaborationArchiveItems(state: CollaborationsStateModel) {
    return state.collaborationArchiveItems;
  }
  //#endregion

  //#region Collaboration Room

  @Selector()
  static getCurrentRoom(state: CollaborationsStateModel) {
    return state.currentRoom;
  }

  //#endregion

  //#region Screenshot

  @Selector()
  static getRoomScreenshot(state: CollaborationsStateModel) {
    return state.roomScreenshots;
  }

  //#endregion

  //#endregion

  constructor(private collaborationService: CollaborationsService, private lookupService: LookupService,
    private livestreamService: LiveStreamService, private usersService: UsersService,
    private groupsService: GroupsService, private store: Store) { }

  //#region A C T I O N S

  //#region Breadcrumb

  @Action(SetBreadcrumbs)
  setBreadcrumbs(ctx: StateContext<CollaborationsStateModel>, { breadcrumbs }: SetBreadcrumbs) {
    const state = ctx.getState();
    ctx.setState({
      ...state,
      breadcrumbs
    });
  }

  @Action(UpdateBreadcrumb)
  updateBreadcrumb(ctx: StateContext<CollaborationsStateModel>, { breadcrumb }: UpdateBreadcrumb) {
    const state = ctx.getState();
    const breadcrumbs = state.breadcrumbs.filter(x => !x.isFinal);
    breadcrumbs.push(breadcrumb);
    ctx.setState({
      ...state,
      breadcrumbs
    });
  }

  //#endregion

  //#region Collaboration Session

  @Action(GetLiveStreams)
  getLiveStreams(ctx: StateContext<CollaborationsStateModel>, { asset, group, pageNumber, pageSize }: GetLiveStreams) {
    ctx.dispatch(new ShowSpinner());
    return this.livestreamService.getLiveStreams(asset, group, pageNumber, pageSize)
      .pipe(
        tap(response => {
          if (!response) return;
          let livestreams = response.data;
          const state = ctx.getState();
          const selectedIds = state.selectedSessionVideos.map(x => x.id);
          livestreams.forEach(l => {
            if (selectedIds.includes(l.id)) l.isChecked = true;
          });
          ctx.setState({
            ...state,
            livestreams: livestreams,
            totalLiveStreams: response.pagination.total
          });
          ctx.dispatch(new HideSpinner());
        }, error => ctx.dispatch(new ShowErrorMessage(error)))
      );
  }

  @Action(AddSessionVideo)
  addSessionVideo(ctx: StateContext<CollaborationsStateModel>, { video }: AddSessionVideo) {
    const state = ctx.getState();
    const selectedVideo = state.selectedSessionVideos.find(v => v.id === video.id);
    if (!selectedVideo) {
      ctx.setState({
        ...state,
        selectedSessionVideos: [
          ...state.selectedSessionVideos,
          video
        ]
      });
    }
  }

  @Action(RemoveSessionVideo)
  removeSessionVideo(ctx: StateContext<CollaborationsStateModel>, { video }: RemoveSessionVideo) {
    const state = ctx.getState();
    const selectedVideos = state.selectedSessionVideos.filter(v => v.id !== video.id);
    ctx.setState({
      ...state,
      selectedSessionVideos: selectedVideos
    });
  }

  @Action(AddParticipant)
  addParticipant(ctx: StateContext<CollaborationsStateModel>, { participant }: AddParticipant) {
    const state = ctx.getState();
    const selectedParticipants = state.selectedParticipants.find(x => x.id === participant.id);
    if (!selectedParticipants) {
      let users: User[] = JSON.parse(JSON.stringify(state.users));
      users.forEach(user => {
        if (user.id === participant.id) {
          user.isSelected = true;
        }
      });
      ctx.setState({
        ...state,
        selectedParticipants: [
          ...state.selectedParticipants,
          participant
        ],
        users: users
      });
    }
  }

  @Action(RemoveParticipant)
  removeParticipant(ctx: StateContext<CollaborationsStateModel>, { participant }: RemoveParticipant) {
    const state = ctx.getState();
    const selectedParticipants = state.selectedParticipants.filter(x => x.id !== participant.id);
    let users: User[] = JSON.parse(JSON.stringify(state.users));
    users.forEach(user => {
      if (user.id === participant.id) {
        user.isSelected = false;
      }
    });
    ctx.setState({
      ...state,
      selectedParticipants: selectedParticipants,
      users: users
    });
  }

  @Action(SetCollaborationSession)
  setCollaborationSession(ctx: StateContext<CollaborationsStateModel>, { session }: SetCollaborationSession) {
    const state = ctx.getState();
    ctx.setState({
      ...state,
      currentSession: session
    });
  }

  @Action(ClearCollaborationSession)
  clearCollaborationSession(ctx: StateContext<CollaborationsStateModel>) {
    const state = ctx.getState();
    ctx.setState({
      ...state,
      currentSession: null,
      selectedSessionVideos: [],
      selectedParticipants: [],
      users: []
    });
  }

  @Action(GetLiveStreamAssets)
  getLiveStreamAssets(ctx: StateContext<CollaborationsStateModel>) {
    ctx.dispatch(new ShowSpinner());
    return this.lookupService.getLiveStreamAssets()
      .pipe(
        tap(assets => {
          const state = ctx.getState();
          ctx.setState({
            ...state,
            livestreamAssets: assets
          });
          ctx.dispatch(new HideSpinner());
        }, error => ctx.dispatch(new ShowErrorMessage(error)))
      );
  }

  @Action(GetLiveStreamGroups)
  getLiveStreamGroups(ctx: StateContext<CollaborationsStateModel>) {
    ctx.dispatch(new ShowSpinner());
    return this.lookupService.getLiveStreamGroups()
      .pipe(
        tap(groups => {
          const state = ctx.getState();
          ctx.setState({
            ...state,
            livestreamGroups: groups
          });
          ctx.dispatch(new HideSpinner());
        }, error => ctx.dispatch(new ShowErrorMessage(error)))
      );
  }

  @Action(GetUsers)
  getUsers(ctx: StateContext<CollaborationsStateModel>, { name, groupId, pageNumber, pageSize }: GetUsers) {
    ctx.dispatch(new ShowSpinner());
    return this.usersService.getActiveUsers(name, groupId, pageNumber, pageSize)
      .pipe(
        tap(response => {
          const currentUser = this.store.selectSnapshot(AppState.getLoggedInUser);
          let users = response.data;
          users.forEach(u => u.isCurrent = u.id === currentUser.id);
          const state = ctx.getState();
          const selectedUsers = state.selectedParticipants.map(x => x.id);
          users.forEach(user => {
            if (selectedUsers.includes(user.id)) {
              user.isSelected = true;
            }
          });
          ctx.setState({
            ...state,
            users: users,
            totalUsers: response.total
          });
          ctx.dispatch(new HideSpinner());
        }, err => ctx.dispatch(new ShowErrorMessage(err)))
      );
  }

  @Action(GetGroups)
  getActiveGroups(ctx: StateContext<CollaborationsStateModel>) {
    ctx.dispatch(new ShowSpinner());
    return this.groupsService.getGroups()
      .pipe(
        tap(response => {
          const state = ctx.getState();
          const groups = response.filter(x => x.status === GroupStatus.Active);
          ctx.setState({
            ...state,
            groups: groups
          });
          ctx.dispatch(new HideSpinner());
        }, (err) => ctx.dispatch(new ShowErrorMessage(err)))
      );
  }

  @Action(SaveCollaborationSession)
  saveCollaborationSession(ctx: StateContext<CollaborationsStateModel>) {
    ctx.dispatch(new ShowSpinner());
    const state = ctx.getState();
    const session = state.currentSession;
    if (!session.id) {
      return this.collaborationService.createSession(session)
        .pipe(
          tap(() => {
            ctx.dispatch(new ShowSuccessMessage(`Collaboration Session was successfully created!`));
            ctx.dispatch(new HideSpinner());
          }, error => ctx.dispatch(new ShowErrorMessage(error)))
        );
    } else {
      return this.collaborationService.updateSession(session)
        .pipe(
          tap(() => {
            ctx.dispatch(new ShowSuccessMessage(`Collaboration Session was successfully updated!`));
            ctx.dispatch(new HideSpinner());
          }, error => ctx.dispatch(new ShowErrorMessage(error)))
        );
    }
  }

  @Action(SaveExpandedDirectories)
  saveExpandedDirectories(ctx: StateContext<CollaborationsStateModel>, { directories }: SaveExpandedDirectories) {
    const state = ctx.getState();
    ctx.setState({
      ...state,
      expandedDirectories: directories
    });
  }

  @Action(ClearExpandedDirectories)
  clearExpandedDirectories(ctx: StateContext<CollaborationsStateModel>) {
    const state = ctx.getState();
    ctx.setState({
      ...state,
      expandedDirectories: []
    });
  }

  //#endregion

  //#region Collaborations

  @Action(GetCollaborationDays)
  getCollaborationDays(ctx: StateContext<CollaborationsStateModel>, { dateFilter, tag, silentCall }: GetCollaborationDays) {
    const state = ctx.getState();
    if (!silentCall) {
      ctx.dispatch(new ShowSpinner());
      ctx.setState({ ...state, collaborationDays: [], currentDateFilter: dateFilter });
    }
    const filterTag = tag || state.filterTags[0];
    const query = filterTag ? filterTag.value : null;
    return this.collaborationService.getCollaborationDays(dateFilter, query)
      .pipe(
        tap(response => {
          const state = ctx.getState();
          const today = new Date();
          const canUpdateUpcomingSessions = dateFilter.getMonth() === today.getMonth() && !tag;
          if (canUpdateUpcomingSessions) {
            ctx.setState({
              ...state,
              collaborationDays: response.days,
              upcomingSessions: response.upcomingSessionsCount
            });
          } else {
            ctx.setState({
              ...state,
              collaborationDays: response.days
            })
          }
          ctx.dispatch(new HideSpinner());
        }, error => ctx.dispatch(new ShowErrorMessage(error)))
      );
  }

  @Action(GetCollaborationSessions)
  getCollaborationSessions(ctx: StateContext<CollaborationsStateModel>, { dateFilter, tag }: GetCollaborationSessions) {
    ctx.dispatch(new ShowSpinner());
    const state = ctx.getState();
    ctx.setState({ ...state, currentDateFilter: dateFilter });
    const filterTag = tag || state.filterTags[0];
    const query = filterTag ? filterTag.value : null;
    return this.collaborationService.getCollaborationSessions(dateFilter, query)
      .pipe(
        tap(sessions => {
          const state = ctx.getState();
          ctx.setState({
            ...state,
            collaborationSessions: sessions
          });
          ctx.dispatch(new HideSpinner());
        }, error => ctx.dispatch(new ShowErrorMessage(error)))
      );
  }

  @Action(GetCollaborationSession)
  getCollaborationSession(ctx: StateContext<CollaborationsStateModel>, { id }: GetCollaborationSession) {
    ctx.dispatch(new ShowSpinner());
    return this.collaborationService.getCollaborationSession(id)
      .pipe(
        tap(session => {
          const state = ctx.getState();
          const sessionVideos = [...session.videos];
          const selectedParticipants = [...session.participants];
          let livestreams: LiveStream[] = JSON.parse(JSON.stringify([...state.livestreams]));
          livestreams.forEach(livestream => {
            livestream.isChecked = sessionVideos.map(x => x.id).includes(livestream.id);
          });
          ctx.setState({
            ...state,
            currentSession: session,
            selectedSessionVideos: sessionVideos,
            selectedParticipants: selectedParticipants,
            livestreams: livestreams
          });
          ctx.dispatch(new HideSpinner());
        }, error => ctx.dispatch(new ShowErrorMessage(error)))
      );
  }

  @Action(DeleteCollaborationSession)
  deleteCollaborationSession(ctx: StateContext<CollaborationsStateModel>, { id }: GetCollaborationSession) {
    const state = ctx.getState();
    let sessions = [...state.collaborationSessions];
    const dateFilter = state.currentDateFilter;
    const tag = state.filterTags[0];
    ctx.dispatch(new GetCollaborationDays(dateFilter, tag, true));
    sessions = sessions.filter(s => s.id !== id);
    ctx.setState({
      ...state,
      collaborationSessions: sessions
    });
  }

  @Action(ClearCollaborations)
  clearCollaborations(ctx: StateContext<CollaborationsStateModel>) {
    const state = ctx.getState();
    ctx.setState({
      ...state,
      collaborationSessions: []
    });
  }

  //#endregion

  //#region Filters

  @Action(AddFilterTag)
  addFilterTag(ctx: StateContext<CollaborationsStateModel>, { tag }: AddFilterTag) {
    const state = ctx.getState();
    const dateFilter = state.currentDateFilter;
    ctx.setState({
      ...state,
      filterTags: [tag]
    });
    ctx.dispatch(new GetCollaborationSessions(dateFilter, tag));
    ctx.dispatch(new GetCollaborationDays(dateFilter, tag));
  }

  @Action(RemoveFilterTag)
  removeFilterTag(ctx: StateContext<CollaborationsStateModel>) {
    const state = ctx.getState();
    const dateFilter = state.currentDateFilter;
    ctx.setState({
      ...state,
      filterTags: []
    });
    ctx.dispatch(new GetCollaborationSessions(dateFilter));
    ctx.dispatch(new GetCollaborationDays(dateFilter));
  }

  //#endregion

  //#region Collaboration Archive

  @Action(GetCollaborationArchive)
  getCollaborationArchive(ctx: StateContext<CollaborationsStateModel>) {
    ctx.dispatch(new ShowSpinner());
    return this.collaborationService.getCollaborationArchiveItems()
      .pipe(
        tap(response => {
          const state = ctx.getState();
          ctx.setState({
            ...state,
            collaborationArchiveItems: response
          });
          ctx.dispatch(new HideSpinner());
        }, error => ctx.dispatch(new ShowErrorMessage(error)))
      );
  }

  //#endregion

  //#region Collaboration Room

  @Action(JoinRoom)
  joinCollaboration(ctx: StateContext<CollaborationsStateModel>, { id }: JoinRoom) {
    ctx.dispatch(new ShowSpinner());
    return this.collaborationService.join(id)
      .pipe(
        tap(room => {
          const state = ctx.getState();
          ctx.setState({
            ...state,
            currentRoom: room
          });
          ctx.dispatch(new HideSpinner());
        }, error => {
          ctx.dispatch(new ShowErrorMessage(error));
          const state = ctx.getState();
          ctx.setState({
            ...state,
            currentRoom: null
          });
        })
      );
  }

  @Action(ClearRoom)
  clearCollaborationRoom(ctx: StateContext<CollaborationsStateModel>) {
    const state = ctx.getState();
    ctx.setState({
      ...state,
      activeRoom: null
    });
  }

  @Action(LeaveRoom)
  leaveRoom(ctx: StateContext<CollaborationsStateModel>, { id }: LeaveRoom) {
    ctx.dispatch(new ShowSpinner());
    return this.collaborationService.leaveRoom(id)
      .pipe(
        tap(() => {
          const state = ctx.getState();
          ctx.setState({
            ...state,
            currentRoom: null
          });
          ctx.dispatch(new HideSpinner());
        }, error => ctx.dispatch(new ShowErrorMessage(error)))
      );
  }

  //#endregion

  //#endregion

  //#region collaboration screenshot

  @Action(CreateCollaborationScreenShot)
  creatCollaborationScreenshot(ctx: StateContext<CollaborationsStateModel>, { id, payload }: CreateCollaborationScreenShot) {
    return this.collaborationService.createScreenshot(id, payload)
      .then(() => {
        const state = ctx.getState();
        ctx.dispatch(new SetScreenshotCreated(true));
        ctx.dispatch(new GetCollaborationScreenShots(id));
      }, error => {
        ctx.dispatch(new SetScreenshotCreated(true));
        ctx.dispatch(new ShowErrorMessage(error))
      })
  }

  @Action(IncrementScreenshots)
  incrementScreenshots(ctx: StateContext<CollaborationsStateModel>, { count }: IncrementScreenshots) {
    const state = ctx.getState();
    const counter = count + 1;
    ctx.setState({
      ...state,
      screenshotNameCounter: counter
    });
  }

  @Action(SetScreenshotCreated)
  setScreenshotCreated(ctx: StateContext<CollaborationsStateModel>, { flag }: SetScreenshotCreated) {
    const state = ctx.getState();
    ctx.setState({
      ...state,
      screenshotCreated: flag
    });
  }

  @Action(GetCollaborationScreenShots)
  getCollaborationScreenshots(ctx: StateContext<CollaborationsStateModel>, { id }: GetCollaborationScreenShots) {
    const state = ctx.getState();
    return this.collaborationService.getScreenshots(id)
      .then((screenshots) => {
        console.log('GetCollaborationScreenShots', screenshots)
        ctx.setState({
          ...state,
          roomScreenshots: screenshots,
        });
      }, error => ctx.dispatch(new ShowErrorMessage(error)))
  }

  @Action(DeleteCollaborationScreenShot)
  deleteCollaborationScreenShot(ctx: StateContext<CollaborationsStateModel>, { collaborationId, screenshotId }: DeleteCollaborationScreenShot) {
    ctx.dispatch(new ShowSpinner());
    return this.collaborationService.deleteScreenshot(collaborationId, screenshotId)
      .then(() => {
        ctx.dispatch(new HideSpinner());
      }, error => ctx.dispatch(new ShowErrorMessage(error)))
  }

  //#endregion 



}
