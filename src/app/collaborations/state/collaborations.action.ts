import { CollaborationRoom, CollaborationSession } from 'src/app/core/models/entity/collaboration';
import { User } from 'src/app/core/models/entity/user';
import { LiveStream } from 'src/app/core/models/entity/live-stream';
import { Breadcrumb } from 'src/app/core/models/breadcrumb';
import { MediaItem } from 'src/app/core/models/entity/media';
import { Tag } from 'src/app/core/models/entity/tag';
import { Directory } from 'src/app/core/models/entity/directory';

//#region BreadCrumb

export class SetBreadcrumbs {
  static readonly type = '[Collaborations] SetBreadcrumbs';

  constructor(public breadcrumbs: Breadcrumb[]) { }
}

export class UpdateBreadcrumb {
  static readonly type = '[Collaborations] UpdateBreadcrumb';

  constructor(public breadcrumb: Breadcrumb) { }
}

//#endregion

export class GetCollaborationDays {
  static readonly type = '[Collaborations] GetCollaborationDays';

  constructor(public dateFilter: Date, public tag?: Tag, public silentCall?: boolean) { }
}

export class GetCollaborationSessions {
  static readonly type = '[Collaborations] GetCollaborationSessions';

  constructor(public dateFilter: Date, public tag?: Tag) { }
}

export class GetDailyCollaboration {
  static readonly type = '[Collaborations] GetDailyCollaboration';

  constructor(public sessionId: number) { }
}

export class ClearCollaborations {
  static readonly type = '[Collaborations] ClearCollaborations';
}

//#region Collaboration Session

export class GetLiveStreams {
  static readonly type = '[Collaborations] GetLiveStreams';

  constructor(public asset?: string, public group?: string, public pageNumber?: number, public pageSize?: number) { }
}

export class AddSessionVideo {
  static readonly type = '[Collaborations] AddSessionVideo';

  constructor(public video: LiveStream) { }
}

export class RemoveSessionVideo {
  static readonly type = '[Collaborations] RemoveSessionVideo';

  constructor(public video: LiveStream) { }
}

export class GetLiveStreamAssets {
  static readonly type = '[Collaborations] GetLiveStreamAssets';
}

export class GetLiveStreamGroups {
  static readonly type = '[Collaborations] GetLiveStreamGroups';
}

export class GetUsers {
  static readonly type = '[Collaborations] GetUsers';

  constructor(public name?: string, public groupId?: number, public pageNumber?: number, public pageSize?: number) { }
}

export class GetGroups {
  static readonly type = '[Collaborations] GetGroups';
}

export class AddParticipant {
  static readonly type = '[Collaborations] AddParticipant';

  constructor(public participant: User) { }
}

export class RemoveParticipant {
  static readonly type = '[Collaborations] RemoveParticipant';

  constructor(public participant: User) { }
}

export class SetCollaborationSession {
  static readonly type = '[Collaborations] SetCollaborationSession';

  constructor(public session: CollaborationSession) { }
}

export class ClearCollaborationSession {
  static readonly type = '[Collaborations] ClearCollaborationSession';
}

export class SaveCollaborationSession {
  static readonly type = '[Collaborations] SaveCollaborationSession';
}

export class SaveExpandedDirectories {
  static readonly type = '[Collaborations] SaveExpandedDirectories';

  constructor(public directories: Directory[]) { }
}

export class ClearExpandedDirectories {
  static readonly type = '[Collaborations] ClearExpandedDirectories';
}

//#endregion

//#region Collaboration 

export class GetCollaborationSession {
  static readonly type = '[Collaborations] GetCollaborationSession';

  constructor(public id: number) { }
}

export class DeleteCollaborationSession {
  static readonly type = '[Collaborations] DeleteCollaborationSession';

  constructor(public id: number) { }
}

//#endregion

//#region Filters

export class AddFilterTag {
  static readonly type = '[Collaborations] AddFilterTag';

  constructor(public tag: Tag) { }
}

export class RemoveFilterTag {
  static readonly type = '[Collaborations] RemoveFilterTag';
}

//#endregion


//#region Collaboration Room

export class GetCurrentCollaborationRoom {
  static readonly type = '[Collaborations] GetCurrentCollaborationRoom';

  constructor(public currentRoom: CollaborationRoom) { }
}

export class ClearRoom {
  static readonly type = '[Collaborations] ClearRoom';
}

export class JoinRoom {
  static readonly type = '[Collaborations] JoinRoom';

  constructor(public id: number) { }
}

export class LeaveRoom {
  static readonly type = '[Collaborations] LeaveRoom';

  constructor(public id: number) { }
}

//#endregion

//#region  

export class IncrementScreenshots {
  static readonly type = '[Collaborations] IncrementScreenshots';

  constructor(public count: number) { }
}

export class SetScreenshotCreated {
  static readonly type = '[Collaborations] SetScreenshotCreated';

  constructor(public flag: boolean) { }
}


export class CreateCollaborationScreenShot {
  static readonly type = '[Collaborations] CreateCollaborationScreenShot';

  constructor(public id: number, public payload: MediaItem) { }
}

export class GetCollaborationScreenShots {
  static readonly type = '[Collaborations] GetCollaborationScreenShots';

  constructor(public id: number) { }
}

export class DeleteCollaborationScreenShot {
  static readonly type = '[Collaborations] DeleteCollaborationScreenShot';

  constructor(public collaborationId: number, public screenshotId: number) { }
}
//endregion

//#region Collaboration Archive

export class GetCollaborationArchive {
  static readonly type = '[Collaborations] GetCollaborationArchive';
}

//#endregion

