import { NgModule, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from "@angular/core";
import { CollaborationsComponent } from "./collaborations.component";
import { CollaborationsRoutingModule } from "./collaborations-routing.module";
import { SharedModule } from "../shared/shared.module";
import { CollaborationsDataService } from '../core/services/data/collaborations/collaborations.data.service';
import { CollaborationsMockDataService } from '../core/services/data/collaborations/collaborations.mock.data.service';
import { CollaborationsWebDataService } from '../core/services/data/collaborations/collaborations.web.data.service';
import { environment } from 'src/environments/environment';
import { NgxsModule } from '@ngxs/store';
import { CollaborationsState } from './state/collaborations.state';
import { CollaborationsService } from '../core/services/business/collaborations/collaborations.service';
import { DropDownListModule } from '@syncfusion/ej2-angular-dropdowns';
import { DatePickerModule, TimePickerModule } from '@syncfusion/ej2-angular-calendars';
import { CollaborationViewerComponent } from './collaboration-viewer/collaboration-viewer.component';
import { CurrentCollaborationsComponent } from './current-collaborations/current-collaborations.component';
import { CurrentCollaborationsCardComponent } from './current-collaborations/current-collaborations-card/current-collaborations-card.component';
import { CollaborationSetupComponent } from './collaboration-setup/collaboration-setup.component';
import { CollaborationSetupSummaryComponent } from './collaboration-setup/collaboration-setup-summary/collaboration-setup-summary.component';
import { CollaborationSetupFirstComponent } from './collaboration-setup/collaboration-setup-first/collaboration-setup-first.component';
import { CollaborationSetupSecondComponent } from './collaboration-setup/collaboration-setup-second/collaboration-setup-second.component';
import { LookupService } from '../core/services/business/lookup/lookup.service';
import { LookupDataService } from '../core/services/data/lookup/lookup.data.service';
import { LiveStreamService } from '../core/services/business/live-stream/live-stream.service';
import { LookupMockDataService } from '../core/services/data/lookup/lookup.mock.data.service';
import { LookupWebDataService } from '../core/services/data/lookup/lookup.web.data.service';
import { UsersService } from '../core/services/business/users/users.service';
import { UsersDataService } from '../core/services/data/users/users.data.service';
import { UsersMockDataService } from '../core/services/data/users/users.mock.data.service';
import { UsersWebDataService } from '../core/services/data/users/users.web.data.service';
import { GroupsService } from '../core/services/business/groups/groups.service';
import { GroupsDataService } from '../core/services/data/groups/groups.data.service';
import { GroupsMockDataService } from '../core/services/data/groups/groups.mock.data.service';
import { GroupsWebDataService } from '../core/services/data/groups/groups.web.data.service';
import { LiveStreamDataService } from '../core/services/data/live-stream/live-stream-data.service';
import { LiveStreamMockDataService } from '../core/services/data/live-stream/live-stream-mock.service';
import { LiveStreamWebDataService } from '../core/services/data/live-stream/live-stream-web.service';
import { CurrentCollaborationsCalendarComponent } from './current-collaborations/current-collaborations-calendar/current-collaborations-calendar.component';
import { CollaborationViewerDetailsComponent } from './collaboration-viewer/collaboration-viewer-details/collaboration-viewer-details.component';
import { CollaborationChatComponent } from './collaboration-viewer/collaboration-chat/collaboration-chat.component';
import { CollaborationControlsComponent } from './collaboration-viewer/collaboration-controls/collaboration-controls.component';
import { DirectoryService } from '../core/services/business/directory/directory.service';
import { DirectoryDataService } from '../core/services/data/directory/directory.data.service';
import { DirectoryMockDataService } from '../core/services/data/directory/directory.mock.data.service';
import { DirectoryWebDataService } from '../core/services/data/directory/directory.web.data.service';
import { AngularDraggableModule } from 'angular2-draggable';
import { TooltipModule } from '@syncfusion/ej2-angular-popups';

@NgModule({
  declarations: [
    CollaborationsComponent, CollaborationViewerComponent, CurrentCollaborationsComponent, CurrentCollaborationsCardComponent,
    CollaborationSetupComponent, CollaborationSetupSummaryComponent, CollaborationSetupFirstComponent,
    CollaborationSetupSecondComponent,
    CurrentCollaborationsCalendarComponent,
    CollaborationViewerDetailsComponent,
    CollaborationChatComponent,
    CollaborationControlsComponent
  ],
  imports: [
    CollaborationsRoutingModule,
    SharedModule,
    DropDownListModule,
    DatePickerModule,
    TimePickerModule,
    AngularDraggableModule,
    TooltipModule,
    NgxsModule.forFeature([CollaborationsState])
  ],
  providers: [
    CollaborationsService,
    { provide: CollaborationsDataService, useClass: environment.useMocks ? CollaborationsMockDataService : CollaborationsWebDataService },
    LookupService,
    { provide: LookupDataService, useClass: environment.useMocks ? LookupMockDataService : LookupWebDataService },
    UsersService,
    { provide: UsersDataService, useClass: environment.useMocks ? UsersMockDataService : UsersWebDataService },
    GroupsService,
    { provide: GroupsDataService, useClass: environment.useMocks ? GroupsMockDataService : GroupsWebDataService },
    LiveStreamService,
    { provide: LiveStreamDataService, useClass: environment.useMocks ? LiveStreamMockDataService : LiveStreamWebDataService },
    DirectoryService,
    { provide: DirectoryDataService, useClass: environment.useMocks ? DirectoryMockDataService : DirectoryWebDataService }
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA]
})
export class CollaborationsModule { }
