import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { BaseComponent } from 'src/app/shared/base/base.component';
import { Store, Select } from '@ngxs/store';
import { Router, ActivatedRoute } from '@angular/router';
import { SubSink } from 'subsink/dist/subsink';
import { CollaborationSession } from 'src/app/core/models/entity/collaboration';
import { CollaborationsState } from '../state/collaborations.state';
import { Observable } from 'rxjs/internal/Observable';
import { ClearCollaborationSession, GetCollaborationSession, ClearExpandedDirectories } from '../state/collaborations.action';
import { ModalComponent } from 'src/app/shared/modal/modal.component';

@Component({
  selector: 'app-collaboration-setup',
  templateUrl: './collaboration-setup.component.html',
  styleUrls: ['./collaboration-setup.component.css']
})
export class CollaborationSetupComponent extends BaseComponent implements OnInit, OnDestroy {

  @Select(CollaborationsState.getCurrentSession) currentSession$: Observable<CollaborationSession>;

  @ViewChild('confirmModal') confirmModal: ModalComponent;

  private subs = new SubSink();
  previousRoute: string;
  isCreateMode: boolean;
  currentStep = 1;
  currentSession: CollaborationSession;
  isSaved: boolean;
  isDirty: boolean;

  constructor(protected store: Store, protected router: Router, protected route: ActivatedRoute) {
    super(store);
    this.hideLeftNav();
    this.setPageTitle("Collaboration Setup");
  }

  ngOnInit() {
    this.subs.add(
      this.route.queryParams
        .subscribe(params => {
          const { stage } = params;
          this.currentStep = Number(stage);
          if (!stage || this.currentStep < 1 || this.currentStep > 3) {
            this.router.navigate(
              ['.'],
              { relativeTo: this.route, queryParams: { stage: 1 } }
            );
          }
        }),
      this.route.paramMap
        .subscribe(params => {
          const collaborationId = Number(params.get('id'));
          this.isCreateMode = !collaborationId;
          if (collaborationId) {
            this.store.dispatch(new GetCollaborationSession(collaborationId));
          }
        }),
      this.currentSession$
        .subscribe(session => {
          this.currentSession = session;
          if (!this.currentSession) {
            this.router.navigate(
              ['.'],
              { relativeTo: this.route, queryParams: { stage: 1 } }
            );
          } else if (this.currentStep === 2) {
            if (!session.title) {
              this.router.navigate(
                ['.'],
                { relativeTo: this.route, queryParams: { stage: 1 } }
              );
            }
          } else if (this.currentStep === 3) {
            if (
              !session.title ||
              session.participants.length === 0 ||
              !session.date ||
              !session.startTime ||
              !session.endTime) {
              this.router.navigate(
                ['.'],
                { relativeTo: this.route, queryParams: { stage: 2 } }
              );
            }
          }
        })
    );
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
    this.store.dispatch(new ClearCollaborationSession());
    this.store.dispatch(new ClearExpandedDirectories());
  }

  onBack() {
    this.router.navigate(['collaborations/current']);
  }

  onSaved() {
    this.isSaved = true;
  }

  onStatusChanged(isDirty: boolean) {
    this.isDirty = isDirty;
  }
}
