import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CollaborationSetupSummaryComponent } from './collaboration-setup-summary.component';

describe('CollaborationSetupSummaryComponent', () => {
  let component: CollaborationSetupSummaryComponent;
  let fixture: ComponentFixture<CollaborationSetupSummaryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CollaborationSetupSummaryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CollaborationSetupSummaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
