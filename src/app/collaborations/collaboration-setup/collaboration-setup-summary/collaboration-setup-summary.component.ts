import { Component, OnInit, OnDestroy, Output, EventEmitter, ViewChild } from '@angular/core';
import { CollaborationsState } from '../../state/collaborations.state';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs/internal/Observable';
import { CollaborationSession } from 'src/app/core/models/entity/collaboration';
import { SubSink } from 'subsink/dist/subsink';
import { Router, ActivatedRoute } from '@angular/router';
import { SaveCollaborationSession, SetCollaborationSession, SaveExpandedDirectories } from '../../state/collaborations.action';
import { DirectoryService } from 'src/app/core/services/business/directory/directory.service';
import { TreeViewComponent } from 'src/app/shared/tree-view/tree-view.component';
import { BaseComponent } from 'src/app/shared/base/base.component';
import { Directory } from 'src/app/core/models/entity/directory';


@Component({
  selector: 'app-collaboration-setup-summary',
  templateUrl: './collaboration-setup-summary.component.html',
  styleUrls: ['./collaboration-setup-summary.component.css']
})
export class CollaborationSetupSummaryComponent extends BaseComponent implements OnInit, OnDestroy {

  @Select(CollaborationsState.getCurrentSession) currentSession$: Observable<CollaborationSession>;
  @Select(CollaborationsState.getExpandedDirectories) expandedDirectories$: Observable<Directory[]>;

  @Output() statusChange = new EventEmitter<boolean>();

  @ViewChild('appTreeView') appTreeView: TreeViewComponent;

  private subscriptions = new SubSink();
  currentSession: CollaborationSession;
  isSaving: boolean;
  directories: Directory[] = [];
  field: object;
  currentTarget: HTMLLIElement;
  parentID: number;
  currentDirectoryId: number;
  currentCollaborationSession: any[];
  directoryPath = '';
  expandedDirectories: Directory[];

  constructor(protected store: Store, protected router: Router, protected route: ActivatedRoute, private foldersService: DirectoryService) {
    super(store);
  }

  ngOnInit() {
    this.currentSession = this.store.selectSnapshot(CollaborationsState.getCurrentSession);
    const expandedDirectories = this.store.selectSnapshot(CollaborationsState.getExpandedDirectories);
    this.expandedDirectories = JSON.parse(JSON.stringify(expandedDirectories));
    if (this.currentSession) {
      this.currentDirectoryId = this.currentSession.directoryId;
      this.directoryPath = this.currentSession.directoryPath;
      this.getFolders(this.expandedDirectories.length > 0);
    }
  }

  ngOnDestroy() {
    this.subscriptions.unsubscribe();
    const expandedNodes = this.appTreeView.treeview.expandedNodes.map(x => Number(x));
    let expandedDirectories: Directory[] = [];
    const treeData = this.appTreeView.treeview.getTreeData();
    let excludedIds: number[] = [];
    treeData.forEach(data => {
      const directory: Directory = data as any;
      // if directory is expanded, get all it's children;
      // if directory is not expanded, exclude all it's children
      if (!directory.expanded) {
        excludedIds = [...excludedIds, directory.id];
      }
      const isExcludedDirectory = excludedIds.includes(directory.id);
      if (isExcludedDirectory) {
        const childrenDirectoryIds = this.directories.filter(d => d.parentId === directory.id).map(x => x.id);
        excludedIds = [...excludedIds, ...childrenDirectoryIds];
      }
    });
    this.directories.forEach(directory => {
      // if it's in expandedNodes && it's parent is also in expanded nodes; then all all it's kids
      // if it's root directory => parentId = null
      if (expandedNodes.includes(directory.id) && (expandedNodes.includes(directory.parentId) || !directory.parentId) && !excludedIds.includes(directory.parentId)) {
        const childrenDirectories = this.directories.filter(d => d.parentId === directory.id);
        const expandedDirectoryExists = expandedDirectories.some(x => x.id === directory.id);
        if (!expandedDirectoryExists)
          expandedDirectories = [...expandedDirectories, directory];
        childrenDirectories.forEach(childDirectory => {
          const childDirectoryExists = expandedDirectories.some(x => x.id === childDirectory.id);
          if (!childDirectoryExists)
            expandedDirectories = [...expandedDirectories, childDirectory];
        });
      }
    });
    this.store.dispatch(new SaveExpandedDirectories(expandedDirectories));
  }

  getFolders(usePersistedData: boolean) {
    this.foldersService.getFolders()
      .toPromise()
      .then(directories => {
        if (usePersistedData) {
          // get previously expanded directories;
          // check if any has kids, then it's expanded;
          // compare with what's from the database, if it exists, replace with db and update expanded.
          let allDirectories: Directory[] = [...directories];
          this.expandedDirectories.forEach(oldDirectory => {
            let directory = allDirectories.find(x => x.id === oldDirectory.id);
            if (directory) {
              oldDirectory = directory;
            } else {
              allDirectories = [...allDirectories, oldDirectory];
            }
          });
          allDirectories.forEach(d => d.expanded = allDirectories.some(x => x.parentId === d.id));
          this.directories = [...allDirectories];
        } else {
          this.directories = directories;
        }
        this.field = {
          dataSource: this.directories, id: 'id', parentID: 'parentId',
          text: 'name', iconCss: 'icon', hasChildren: 'hasChild'
        };
        this.hideSpinner();
      }, err => this.showErrorMessage(err));
  }

  previous() {
    this.router.navigate(
      ['.'],
      { relativeTo: this.route, queryParams: { stage: 2 } }
    );
  }

  cancel() {
    this.router.navigate(['collaborations/current']);
  }

  onNodeExpanding() {
    this.parentID = Number(this.appTreeView.parentID);
    this.showSpinner();
    this.foldersService
      .getFolders(Number(this.parentID))
      .toPromise()
      .then(
        folders => {
          folders.forEach(folder => (folder.icon = 'folder'));
          this.addFolders(folders);
          this.appTreeView.addNodes(folders);
          this.hideSpinner();
        }, err => this.showErrorMessage(err));
  }

  onNodeSelected(args: any) {
    this.currentDirectoryId = Number(args.id);
    this.directoryPath = '';
    this.buildDirectoryPath(this.currentDirectoryId);
    this.statusChange.emit(true);
  }

  create() {
    this.isSaving = true;
    const session: CollaborationSession = {
      ...this.currentSession,
      directoryId: this.currentDirectoryId,
      directoryPath: this.directoryPath
    };
    this.store.dispatch(new SetCollaborationSession(session));
    this.store.dispatch(new SaveCollaborationSession()).toPromise()
      .then(() => {
        this.isSaving = false;
        this.statusChange.emit(false);
        this.router.navigate(['collaborations/current']);
      }, () => this.isSaving = false);
  }

  private addFolders(directories: Directory[]) {
    directories.forEach(directory => {
      const folderExists = this.directories.some(f => f.id === directory.id);
      if (folderExists === false) {
        this.directories = [...this.directories, directory];
      }
    });
  }

  private buildDirectoryPath(directoryId: number) {
    const directory = this.directories.find(x => x.id === directoryId);
    const parent = this.directories.find(x => x.id === directory.parentId);
    if (parent) {
      this.directoryPath = this.directoryPath ? `${directory.name} > ${this.directoryPath}` : `${directory.name}`;
      return this.buildDirectoryPath(parent.id);
    }
    return this.directoryPath = this.directoryPath ? `${directory.name} > ${this.directoryPath}` : `${directory.name}`;
  }
}
