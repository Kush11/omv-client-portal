import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CollaborationSetupComponent } from './collaboration-setup.component';

describe('CollaborationSetupComponent', () => {
  let component: CollaborationSetupComponent;
  let fixture: ComponentFixture<CollaborationSetupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CollaborationSetupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CollaborationSetupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
