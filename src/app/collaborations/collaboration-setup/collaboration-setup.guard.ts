import { Injectable } from "@angular/core";
import { CanDeactivate } from '@angular/router';
import { Observable } from 'rxjs/internal/Observable';
import { CollaborationSetupComponent } from './collaboration-setup.component';
import { Subject } from 'rxjs/internal/Subject';

@Injectable({
  providedIn: 'root'
})
export class CollaborationSetupGuard implements CanDeactivate<CollaborationSetupComponent> {

  canDeactivate(component: CollaborationSetupComponent): Observable<boolean> | Promise<boolean> | boolean {
    if (component.isDirty) {
      const subject = new Subject<boolean>();
      component.confirmModal.show();
      component.confirmModal.response$ = subject;
      return subject.asObservable();
    }
    return true;
  }
}