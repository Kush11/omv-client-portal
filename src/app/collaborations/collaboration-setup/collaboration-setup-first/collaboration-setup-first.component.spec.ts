import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CollaborationSetupFirstComponent } from './collaboration-setup-first.component';

describe('CollaborationSetupFirstComponent', () => {
  let component: CollaborationSetupFirstComponent;
  let fixture: ComponentFixture<CollaborationSetupFirstComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CollaborationSetupFirstComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CollaborationSetupFirstComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
