import { Component, OnInit, OnDestroy, Output, EventEmitter, ViewChild } from '@angular/core';
import { BaseComponent } from 'src/app/shared/base/base.component';
import { Store, Select } from '@ngxs/store';
import { CollaborationsState } from '../../state/collaborations.state';
import { Observable } from 'rxjs/internal/Observable';
import { Lookup } from 'src/app/core/models/entity/lookup';
import { LiveStream } from 'src/app/core/models/entity/live-stream';
import { GetLiveStreamAssets, GetLiveStreamGroups, GetLiveStreams, AddSessionVideo, RemoveSessionVideo, SetCollaborationSession, AddParticipant } from '../../state/collaborations.action';
import { CollaborationsService } from 'src/app/core/services/business/collaborations/collaborations.service';
import { CollaborationSession } from 'src/app/core/models/entity/collaboration';
import { SubSink } from 'subsink/dist/subsink';
import { Router, ActivatedRoute } from '@angular/router';
import { Subject } from 'rxjs/internal/Subject';
import { AppState } from 'src/app/state/app.state';

@Component({
  selector: 'app-collaboration-setup-first',
  templateUrl: './collaboration-setup-first.component.html',
  styleUrls: ['./collaboration-setup-first.component.css']
})
export class CollaborationSetupFirstComponent extends BaseComponent implements OnInit, OnDestroy {

  @Select(CollaborationsState.getLiveStreamAssets) assets$: Observable<Lookup[]>;
  @Select(CollaborationsState.getLiveStreamGroups) groups$: Observable<Lookup[]>;
  @Select(CollaborationsState.getLiveStreams) videos$: Observable<LiveStream[]>;
  @Select(CollaborationsState.getTotalLiveStreams) totalVideos$: Observable<number>;
  @Select(CollaborationsState.getCurrentSession) currentSession$: Observable<CollaborationSession>;
  @Select(CollaborationsState.getSelectedSessionVideos) selectedVideos$: Observable<LiveStream[]>;

  @Output() statusChange = new EventEmitter<boolean>();

  @ViewChild('title') titleInput: any;

  private subs = new SubSink();
  currentSession: CollaborationSession;
  sessionTitle = '';
  fields = { text: 'description', value: 'description' };
  assetId: any;
  groupId: any;
  selectedAsset: string;
  selectedGroup: string;
  currentPage = 1;
  pageSize = 8;
  pageCount = 5;
  isTitleValid: boolean;
  isTitleDirty: boolean;
  isValidating: boolean;
  selectedVideos: LiveStream[];
  isValidatingWithButton: boolean;

  isValid$ = new Subject<boolean>();
  isValid = true;
  isTyping: boolean;

  constructor(protected store: Store, private collaborationsService: CollaborationsService, private router: Router, private route: ActivatedRoute) {
    super(store);
  }

  ngOnInit() {
    this.store.dispatch(new GetLiveStreamAssets());
    this.store.dispatch(new GetLiveStreamGroups());
    this.store.dispatch(new GetLiveStreams(null, null, this.currentPage, this.pageSize));
    this.subs.add(
      this.currentSession$
        .subscribe(session => {
          if (session) {
            this.currentSession = session;
            this.sessionTitle = session.title;
            this.isTitleValid = true;
            this.isValid$.next(true);
            this.isValid$.complete();
          } else {
            this.currentSession = new CollaborationSession();
            this.sessionTitle = '';
            this.isValid$.next(false);
            this.isValid$.complete();
          }
        }),
      this.selectedVideos$
        .subscribe(videos => {
          this.selectedVideos = videos;
        })
    );
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

  evaluate() {
    this.statusChange.emit(true);
  }

  searchVideo() {
    this.currentPage = 1;
    this.store.dispatch(new GetLiveStreams(this.assetId, this.groupId, this.currentPage, this.pageSize));
  }

  selectVideo(video: LiveStream) {
    this.statusChange.emit(true);
    const selectedIds = this.selectedVideos.map(v => v.id);
    if (!selectedIds.includes(video.id)) {
      this.store.dispatch(new AddSessionVideo(video));
    } else {
      this.store.dispatch(new RemoveSessionVideo(video));
    }
  }

  onPageChanged(pageNumber: number) {
    this.currentPage = pageNumber;
    this.store.dispatch(new GetLiveStreams(this.assetId, this.groupId, this.currentPage, this.pageSize));
  }

  onFocusOut(value: string) {
    this.isTitleDirty = true;
    this.isValid$ = new Subject<boolean>();
    if (value) {
      this.isValidating = true;
      this.collaborationsService.validateSessionName(value)
        .then(inValid => {
          this.isValidating = false;
          this.isTitleValid = !inValid;
          this.isValid$.next(!inValid);
          this.isValid$.complete();
        }, err => {
          this.isTitleValid = false;
          this.isValidating = false;
          this.showErrorMessage(err);
        });
    } else {
      this.isTitleValid = false;
    }
  }

  async next() {
    this.isValidatingWithButton = true;
    let subject = new Subject<boolean>();
    subject = this.isValid$;
    const isValid = await subject.toPromise();

    this.isValid = isValid || this.isTitleValid;

    if (this.isValid) {
      const videoIds = this.selectedVideos.map(x => x.id);
      const currentUser = this.store.selectSnapshot(AppState.getLoggedInUser);
      const participants = this.currentSession.participants ? this.currentSession.participants : [currentUser];
      const session: CollaborationSession = {
        ...this.currentSession,
        title: this.sessionTitle,
        videos: this.selectedVideos,
        videoIds: videoIds,
        participants: participants
      };
      this.store.dispatch([new SetCollaborationSession(session), new AddParticipant(currentUser)]);
      this.router.navigate(
        ['.'],
        { relativeTo: this.route, queryParams: { stage: 2 } }
      );
    } else {
      this.isValidating = false;
      this.isTitleValid = false;
      const element = document.getElementById("session-title");
      if (element) {
        element.scrollIntoView({ behavior: 'smooth', block: 'start', inline: 'nearest' })
      }
    }
    this.isValidatingWithButton = false;
  }
}
