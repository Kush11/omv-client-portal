import { Component, OnInit, OnDestroy, Output, EventEmitter } from '@angular/core';
import { SubSink } from 'subsink/dist/subsink';
import { CollaborationsState } from '../../state/collaborations.state';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs/internal/Observable';
import { CollaborationSession } from 'src/app/core/models/entity/collaboration';
import { BaseComponent } from 'src/app/shared/base/base.component';
import { DateService } from 'src/app/core/services/business/dates/date.service';
import { Group } from 'src/app/core/models/entity/group';
import { GetGroups, GetUsers, AddParticipant, RemoveParticipant, SetCollaborationSession } from '../../state/collaborations.action';
import { User } from 'src/app/core/models/entity/user';
import { Router, ActivatedRoute } from '@angular/router';
import { ChangedEventArgs, ChangeEventArgs } from '@syncfusion/ej2-angular-calendars';
import { AppState } from 'src/app/state/app.state';

@Component({
  selector: 'app-collaboration-setup-second',
  templateUrl: './collaboration-setup-second.component.html',
  styleUrls: ['./collaboration-setup-second.component.css']
})
export class CollaborationSetupSecondComponent extends BaseComponent implements OnInit, OnDestroy {

  @Select(CollaborationsState.getCurrentSession) currentSession$: Observable<CollaborationSession>;
  @Select(CollaborationsState.getTotalUsers) totalUsers$: Observable<number>;
  @Select(CollaborationsState.getUsers) users$: Observable<User[]>;
  @Select(CollaborationsState.getGroups) groups$: Observable<Group[]>;
  @Select(CollaborationsState.getSelectedParticipants) participants$: Observable<User[]>;

  @Output() statusChange = new EventEmitter<boolean>();

  private subs = new SubSink();

  today = new Date();
  sessionDate = this.today;
  minValue: Date = new Date('10/8/2017 12:00 AM');
  maxValue: Date = new Date('10/8/2017 11:30 PM');
  startMinValue: Date;
  startMaxValue: Date;
  endMinValue: Date;
  endMaxValue: Date;
  endTime: Date;
  startTime: Date;

  userFilter = '';
  userFilterFocused: boolean;
  groupId: number;
  fields = { text: 'name', value: 'id' };
  currentPage = 1;
  pageSize = 15;
  currentSession: CollaborationSession;
  participants: User[];

  constructor(protected store: Store, private router: Router, private route: ActivatedRoute, private dateService: DateService) {
    super(store);
  }

  ngOnInit() {
    this.store.dispatch(new GetUsers(null, null, this.currentPage, this.pageSize));
    this.store.dispatch(new GetGroups());
    this.startTime = this.minValue;
    this.endTime = this.maxValue;
    this.subs.add(
      this.currentSession$
        .subscribe(session => {
          if (session) {
            this.currentSession = session;
            this.sessionDate = session.date ? session.date : this.today;
            this.startTime = session.startTime ? session.startTime : this.minValue;
            this.endTime = session.endTime ? session.endTime : this.maxValue;
          }
        }),
      this.participants$
        .subscribe(participants => this.participants = participants)
    );
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

  onDateChanged(args: ChangedEventArgs) {
    this.sessionDate = args.value;
    this.statusChange.emit(true);
  }

  onStartTimeChanged(args: ChangeEventArgs) {
    this.startTime = args.value;
    if (args.value) this.endMinValue = this.dateService.addMinutes(args.value, 30);
    else this.endMinValue = this.minValue;
    this.statusChange.emit(true);
  }

  onEndTimeChanged(args: ChangeEventArgs) {
    this.endTime = args.value;
    if (args.value) this.startMaxValue = this.dateService.addMinutes(args.value, -30);
    else this.startMaxValue = this.maxValue;
    this.statusChange.emit(true);
  }

  onUserFilterChanged(value: string) {
    this.userFilter = value;
  }

  searchUser(value?: string) {
    this.userFilter = value || this.userFilter;
    this.currentPage = 1;
    this.store.dispatch(new GetUsers(this.userFilter, this.groupId, this.currentPage, this.pageSize));
  }

  onUserSelected(user: User) {
    if (!user.isSelected) {
      this.store.dispatch(new AddParticipant(user));
    } else {
      this.store.dispatch(new RemoveParticipant(user));
    }
    this.statusChange.emit(true);
  }

  onPageChanged(pageNumber: number) {
    this.currentPage = pageNumber;
    this.store.dispatch(new GetUsers(this.userFilter, this.groupId, this.currentPage, this.pageSize));
  }

  previous() {
    this.router.navigate(
      ['.'],
      { relativeTo: this.route, queryParams: { stage: 1 } }
    );
  }

  next() {
    const participants = [...this.participants];
    const names = participants.map(x => x.displayName);
    const participantIds = this.participants.map(x => x.id);
    const participantNames = names ? names.join(', ') : '';
    const session: CollaborationSession = {
      ...this.currentSession,
      date: this.sessionDate,
      startTime: this.startTime,
      startTimeString: this.dateService.formatToString(this.startTime, 'hh:mm a'),
      endTime: this.endTime,
      endTimeString: this.dateService.formatToString(this.endTime, 'hh:mm a'),
      participants: this.participants,
      participantNames: participantNames,
      participantIds: participantIds
    };
    this.store.dispatch(new SetCollaborationSession(session));
    this.router.navigate(
      ['.'],
      { relativeTo: this.route, queryParams: { stage: 3 } }
    );
  }

}
