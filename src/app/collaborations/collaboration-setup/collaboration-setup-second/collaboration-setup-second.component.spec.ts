import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CollaborationSetupSecondComponent } from './collaboration-setup-second.component';

describe('CollaborationSetupSecondComponent', () => {
  let component: CollaborationSetupSecondComponent;
  let fixture: ComponentFixture<CollaborationSetupSecondComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CollaborationSetupSecondComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CollaborationSetupSecondComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
