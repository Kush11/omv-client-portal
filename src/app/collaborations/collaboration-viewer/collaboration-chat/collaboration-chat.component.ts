import { Component, OnInit, EventEmitter, Input, Output, ViewChild } from '@angular/core';
import Chat from 'twilio-chat';
import { Channel } from 'twilio-chat/lib/channel';
import { DateService } from 'src/app/core/services/business/dates/date.service';


@Component({
  selector: 'app-collaboration-chat',
  templateUrl: './collaboration-chat.component.html',
  styleUrls: ['./collaboration-chat.component.css']
})
export class CollaborationChatComponent implements OnInit {

  @Input() token: string;
  @Input() channelName: string;
  @Input() connectedUserName: string;
  @Input() toggleChat: boolean;
  @Output() minimizeChatBox = new EventEmitter<boolean>();
  @Output() messageAdded = new EventEmitter();



  message = '';
  chatClient: Chat;
  chatMessages: any[] = [];
  testMessage = 'hello';
  ConnectedChannel: any;
  openChat: boolean;
  isME: boolean;
  isMe: boolean;
  notIsMe: any;
  isTyping: boolean;
  isviewed: boolean = false;

  constructor(public dateService: DateService) { }


  ngOnInit() {
    if (this.channelName && this.token) {
      Chat.create(this.token)
        .then(client => {
          this.joinChannelByUniqueName(client, this.channelName).then(channel => {
            this.ConnectedChannel = channel;
          }).catch(err => {
            console.error('error', err);
          });
        })
        .catch(err => console.error('CollaborationChatComponent error', err));
    }
  }

  addMessage = (message) => {
    const messageData = { ...message, me: message.author === this.connectedUserName, time: this.dateService.formatToTimeOnly(new Date()) };
    this.chatMessages = [...this.chatMessages, messageData];
    this.isMe = messageData.me;
    if (!this.isMe) {
      this.messageAdded.emit(true);
    }
  }

  joinChannelByUniqueName = (chatClient, channelName: string) => {
    return new Promise((resolve, reject) => {
      chatClient.getSubscribedChannels().then(() => {
        chatClient.getChannelByUniqueName(channelName).then((channel) => {
          //  this.addMessage({ body: `Joining ${channelName} channel...` })
          this.configureChannelEvents(channel);
          channel.join().then(() => {
            //  this.addMessage({ body: `Joined  ${channelName} channel as ${this.connectedUserName}` })
            window.addEventListener('beforeunload', () => channel.leave());
            // this.configureChannelEvents(channel);
          }).catch(() => reject(Error(`Could not join  ${channelName} channel.`)));

          resolve(channel);
        }).catch(() => reject(Error(`Could not find ${channelName} channel.`)));
      }).catch(() => reject(Error('Could not get channel list.')));
    });
  }

  configureChannelEvents = (channel) => {
    channel.on('messageAdded', ({ author, body }) => {
      console.log('configureChannelEvents', this.ConnectedChannel);
      this.addMessage({ author, body });

    });

    channel.on('memberJoined', (member) => {
      // this.addMessage({ body: `${member.identity} has joined the channel.` })
    });

    channel.on('memberLeft', (member) => {
      // this.addMessage({ body: `${member.identity} has left the channel.` })
    });
    // set up the listener for the typing started Channel event
    channel.on('typingStarted', (member) => {
      // process the member to show typing
      this.updateTypingIndicator(member, true);
    });

    // set  the listener for the typing ended Channel event
    channel.on('typingEnded', (member) => {
      // process the member to stop showing typing
      this.updateTypingIndicator(member, false);
    });
  }

  handleNewMessage = () => {
    console.log('handleNewMessage', this.ConnectedChannel);
    if (this.ConnectedChannel) {
      this.ConnectedChannel.sendMessage(this.message);
      this.message = '';
    }
  }

  onKeyDown() {
    console.log("onKeyDown ConnectedChannel: ", this.ConnectedChannel);
    this.ConnectedChannel.typing();
  }

  updateTypingIndicator = (member: any, status: any) => {
    this.isTyping = status;
  }

  toggleChatBox() {
    this.toggleChat = false;
    this.minimizeChatBox.emit(this.toggleChat);
  }

  onFocus() {
    this.messageAdded.emit(false)
  }
}
