import { Injectable } from "@angular/core";
import { CanDeactivate } from '@angular/router';
import { Observable } from 'rxjs/internal/Observable';
import { CollaborationViewerComponent } from './collaboration-viewer.component';

@Injectable({
  providedIn: 'root'
})
export class CollaborationViewerGuard implements CanDeactivate<CollaborationViewerComponent> {

  canDeactivate(component: CollaborationViewerComponent): Observable<boolean> | Promise<boolean> | boolean {
    if (component.isLocalParticipantPresenting) {
      component.showWarningMessage("Please unshare your screen to exit this page.");
      return false;
    }
    return true;
  }
}