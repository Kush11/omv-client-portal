import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CollaborationViewerDetailsComponent } from './collaboration-viewer-details.component';

describe('CollaborationViewerDetailsComponent', () => {
  let component: CollaborationViewerDetailsComponent;
  let fixture: ComponentFixture<CollaborationViewerDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CollaborationViewerDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CollaborationViewerDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
