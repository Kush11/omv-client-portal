import { Component, OnInit, Input, Output, EventEmitter, OnChanges, OnDestroy } from '@angular/core';
import { LiveStream } from 'src/app/core/models/entity/live-stream';
import { Select, Store } from '@ngxs/store';
import { CollaborationsState } from '../../state/collaborations.state';
import { Observable } from 'rxjs';
import { BaseComponent } from 'src/app/shared/base/base.component';
import { CollaborationsService } from 'src/app/core/services/business/collaborations/collaborations.service';
import { SubSink } from 'subsink/dist/subsink';
import { ActivatedRoute } from '@angular/router';
import { GetCollaborationScreenShots } from '../../state/collaborations.action';
import { CollaborationScreenshot } from 'src/app/core/models/entity/collaboration';
import { trigger, state, style, transition, animate } from '@angular/animations';

@Component({
  selector: 'app-collaboration-viewer-details',
  templateUrl: './collaboration-viewer-details.component.html',
  styleUrls: ['./collaboration-viewer-details.component.css'],
  // animations: [
  //   trigger('slideInOut', [
  //     transition(':enter', [
  //       style({ height: '0px', opacity: 0 }),
  //       animate('1000ms ease-in', style({ height: '*', opacity: 1 }))
  //     ]),
  //     transition(':leave', [
  //       animate('1000ms ease-in', style({ height: '0px', opacity: 0 }))
  //     ])
  //   ])
  // ]
})
export class CollaborationViewerDetailsComponent extends BaseComponent implements OnInit, OnChanges, OnDestroy {

  @Select(CollaborationsState.getRoomScreenshot) screenshots$: Observable<CollaborationScreenshot[]>;

  openChat: boolean;
  toggleDetail: boolean;
  screenshots: CollaborationScreenshot[];
  selectedfeed: LiveStream;

  @Input() sessionTitle: string;
  @Input() currentParticipants: any[] = [];
  @Input() feeds: LiveStream[] = [];

  @Output() toggleCard = new EventEmitter();
  @Output() changeFeed = new EventEmitter();
  @Output() deleteScreenShot = new EventEmitter();
  private subs = new SubSink();

  collaborationId: number;

  constructor(public store: Store, private route: ActivatedRoute, private CollaborationService: CollaborationsService) { super(store) }

  ngOnInit() {
    this.subs.add(
      this.route.paramMap.subscribe(params => {
        const id = Number(params.get('id'));
        this.collaborationId = id;
        this.store.dispatch(new GetCollaborationScreenShots(id));
      }),
      this.screenshots$
        .subscribe(screenshots => this.screenshots = screenshots)
    )
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

  ngOnChanges() {
    if (this.feeds.length > 0) {
      this.selectedfeed = this.feeds[0];
      console.log("selected feed", this.selectedfeed);
    }
  }

  setSelectedFeed(args: LiveStream) {
    this.selectedfeed = args;
    this.changeFeed.emit(this.selectedfeed);
  }

  toggleDetailCard() {
    this.toggleDetail = !this.toggleDetail;
  }

  onChangeFeed(args) {
    this.changeFeed.emit(args);
  }

  getScreenshots(id: number) {
    this.CollaborationService.getScreenshots(id)
      .then(screenshots => this.screenshots = screenshots)
      .catch(err => console.log('CollaborationService', err))
  }

  onDeleteScreenShot(id: number) {
    this.deleteScreenShot.emit(id);
  }
}
