import { Component, OnInit, AfterViewInit, ViewChild, ElementRef, OnDestroy, HostListener } from '@angular/core';
import { BaseComponent } from 'src/app/shared/base/base.component';
import { CollaborationsState } from '../state/collaborations.state';
import { Select, Store } from '@ngxs/store';
import { Observable, Subject } from 'rxjs';
import { Participant, CollaborationRoom } from 'src/app/core/models/entity/collaboration';
import { LocalVideoTrack, LocalDataTrack, connect, createLocalTracks, LocalTrackOptions, LocalTrack, RemoteTrack } from 'twilio-video';
import color from 'color-hash';
import { SubSink } from 'subsink/dist/subsink';
import { ActivatedRoute, Router } from '@angular/router';
import { ClearRoom, LeaveRoom, GetCollaborationScreenShots, DeleteCollaborationScreenShot, GetCollaborationSession, SetScreenshotCreated, ClearCollaborationSession, IncrementScreenshots } from '../state/collaborations.action';
import { CollaborationsService } from 'src/app/core/services/business/collaborations/collaborations.service';
import { LiveStream } from './../../core/models/entity/live-stream';
import { ModalComponent } from 'src/app/shared/modal/modal.component';
import { AppState } from 'src/app/state/app.state';
import { GetAzureUploadConfiguration, GetAzureSASToken, ShowErrorMessage, ShowWarningMessage, ShowSuccessMessage } from 'src/app/state/app.actions';
import { ScreenshotUploadService } from './screenshot-upload/screenshot-upload.service';
import { SASTokenType } from 'src/app/core/enum/azure-sas-token';
import { User } from 'src/app/core/models/entity/user';
import { FieldsService } from 'src/app/core/services/business/fields/fields.service';
declare let WowzaPlayer: any;
declare let jwplayer: any;


const PRESENTER = 'Presenter';
const PARTICIPANT = 'Participant';
const HOST = 'Host';
const enum dataMessage {
  annotation = 'Annotation',
  roomEnded = 'Room Ended',
  initiateRoomEnd = 'Initiate Room End',
  screenshot = 'Screenshot',
  refreshScreenshots = 'Refresh Screenshots',
  takeScreenshot = 'Take Screenshot',
  hostHasJoined = 'Host Has Joined',
};

@Component({
  selector: 'app-collaboration-viewer',
  templateUrl: './collaboration-viewer.component.html',
  styleUrls: ['./collaboration-viewer.component.css'],
  providers: [ScreenshotUploadService],
})
export class CollaborationViewerComponent extends BaseComponent implements OnInit, AfterViewInit, OnDestroy {

  @Select(CollaborationsState.getFilteredParticipants) participants$: Observable<Participant[]>;
  @Select(CollaborationsState.getCurrentRoom) currentRoom$: Observable<CollaborationRoom>;
  @Select(AppState.getAzureContainer) container$: Observable<string>;
  @Select(AppState.getMediaWriteSASToken) azureWriteSASToken$: Observable<string>;
  @Select(AppState.getAzureStorageAccount) storageAccount$: Observable<string>;
  @Select(CollaborationsState.getScreenshotNameCounter) screenshotNameCounter$: Observable<number>;
  @Select(CollaborationsState.getScreenshotCreated) isScreenshotCreated$: Observable<boolean>;
  @Select(AppState.getJwtlicenceKey) jwtLincenceKey$: Observable<string>;

  @ViewChild('player') videoPlayer: ElementRef;
  @ViewChild('participants') allParticipants: ElementRef;
  @ViewChild('confirmModal') confirmModal: ModalComponent;
  @ViewChild('participantModal') participantModal: ModalComponent;

  public subs = new SubSink();
  activeRoom: CollaborationRoom;
  collaborationItems: LiveStream[] = [];
  video: Element;
  canvas: HTMLCanvasElement;
  participants: HTMLElement;
  currentRoomName: string;
  twilioRoom: any;
  colorHash = new (color)();
  connectedParticipants: { id?: any, name: string, description?: string, isPresenting?: boolean }[] = [];
  isShowDetails: boolean;
  currentCollaborationId: number;
  openChat: boolean;
  selectedItem: LiveStream;
  hideScreenShare: boolean;
  chatToken: string;
  containerName: string;
  dataTrack: any;
  dataTrackPublished: any = {}
  deleteMessage: string;
  canvasHeight: number;
  canvasWidth: number;
  chatChannelName: string;
  screenshotVideo: HTMLVideoElement;
  storageAccount: string;
  sasToken: string;
  connectedUserName: string;
  videojsObject: any;
  stream: MediaStream;
  isHost: boolean;
  confirmMessage: string;
  confirmTitle: string;
  windowWidth = window.innerWidth;
  windowHeight = window.innerHeight;
  canAnnotate: boolean;
  isLocalParticipantPresenting: boolean;
  isRemoteParticipantPresenting: boolean;
  screenshotsCount = 0;
  isChatMessageAdd: boolean;
  sessionParticipants: User[] = [];
  sessionTitle: string;
  allowAnnotation: boolean;
  savingScreenshot: boolean;
  plots = [];
  isActive: boolean;
  screenLocalTrack: LocalVideoTrack;
  countDown = 10;
  isMonitoringAnnotation: boolean;
  currentUser: User;
  isPlaying: boolean;
  audioVideoTracks: any[] = [];
  isLocalParticipantMuted = false;
  allowControls: boolean;
  mouseDown: boolean;
  lastX = 0;
  lastY = 0;
  isDrawing: any;
  lastRemoteY = 0;
  lastRemoteX = 0;
  mainplayer: any;
  isLocalVideoMuted: boolean;
  roomStatusCheck: any;
  playerTimer: NodeJS.Timer;
  jwtLincenceKey: any;
  player: HTMLElement;

  constructor(public store: Store, private route: ActivatedRoute, private screenshotUploadService: ScreenshotUploadService, private router: Router,
    private collaborationsService: CollaborationsService, private fieldsService: FieldsService) {
    super(store);
    this.hideLeftNav();
    this.enableFullScreen();
    this.setPageTitle("Collaboration Session");
  }

  //#region Init

  ngOnInit() {

    this.openChat = false;
    this.store.dispatch(new GetAzureUploadConfiguration());
    this.store.dispatch(new GetAzureSASToken(SASTokenType.Write));
    this.subs.add(
      this.route.paramMap
        .subscribe(params => {
          const id = Number(params.get('id'));
          this.currentCollaborationId = id;
          this.collaborationsService.join(id).toPromise()
            .then(room => {
              this.connectedUserName = room.connectedUserName;
              this.isHost = room.isHost;
              this.sessionTitle = room.sessionTitle;
              this.setPageTitle(`${this.sessionTitle}`);
              this.sessionParticipants = JSON.parse(JSON.stringify(room.participants));
              this.connectToRoom(room.name, room.participantToken, room.feeds, room.channelToken);
            }, err => {
              this.showErrorMessage(err);
              this.router.navigate(['collaborations/current']);
            });
        }),
      this.container$.subscribe(container => this.containerName = container),
      this.storageAccount$.subscribe(storageAccount => this.storageAccount = storageAccount),
      this.azureWriteSASToken$.subscribe(sasToken => this.sasToken = sasToken),
      this.isScreenshotCreated$
        .subscribe(isCreated => {
          if (isCreated) {
            const data = { message: dataMessage.refreshScreenshots };
            this.dataTrack.send(JSON.stringify(data));
            this.savingScreenshot = false;
            this.store.dispatch(new SetScreenshotCreated(false));
          }
        }),
      this.jwtLincenceKey$.subscribe(key => this.jwtLincenceKey = key)
    );
    console.log('license key', this.jwtLincenceKey)
    jwplayer.key = this.jwtLincenceKey;
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
    this.stopRoomStatusCheck();
    this.destroyWowzaPlayer();
    this.disableFullScreen();
    this.disconnectFromRoom();
    this.store.dispatch([new ClearRoom(), new ClearCollaborationSession()]);
    if (this.stream) {
      this.stream.getTracks().forEach(track => {
        track.stop();
      });
    }
    this.audioVideoTracks.forEach(track => {
      track.stop();
    });
  }

  ngAfterViewInit() {
    this.canvas = document.getElementById('canvas') as HTMLCanvasElement;
    this.screenshotVideo = document.getElementById('screenshotVideo') as any;
    this.player = document.getElementById('videoPlayer');
  }

  @HostListener('window:resize', ['$event'])
  onResize(event: any) {
    this.windowWidth = event.target.innerWidth;
    this.windowHeight = event.target.innerHeight;
    this.canvasWidth = event.target.innerWidth;
    this.canvasHeight = event.target.innerHeight;
    console.log(`Current resolution: ${this.windowWidth}x${this.windowHeight}`);
    console.log(`Current resolution: ${this.videoPlayer.nativeElement}x${this.windowHeight}`);

    this.player.style.height = `${this.windowHeight}px !important`;


  }

  resetPageStyles() {
    const videoContainer = document.querySelector(".video-js") as HTMLElement;
    if (videoContainer) {
      videoContainer.style.height = `${this.windowHeight}px`;
      videoContainer.style.width = '100%';
      const video = Array.from(videoContainer.children)[0] as HTMLVideoElement;
      if (video) {
        video.style.height = `${this.windowHeight}px`;
        video.style.width = '100%';
      }
    }
  }

  changeCurrentFeed(args: LiveStream) {
    //this.setupWowzaPlayer(args.url, true);
    this.setUpJwPlayer(args.url)
  //  jwplayer().stop();
  }

  destroyWowzaPlayer(): void {
    if (this.mainplayer)
      this.mainplayer.destroy();
  }

  setupWowzaPlayer(source = null, autoPlay = false) {
    this.destroyWowzaPlayer();
    if (source) {
      this.mainplayer = WowzaPlayer.create('videoPlayer', {
        "license": "PLAY1-3zveE-8mZFG-HUjva-W8rdG-Q3BvW",
        "sources": [{
          "sourceURL": source
        }],
        "title": '',
        "description": '',
        "autoPlay": autoPlay,
        "mute": false,
        "volume": 100
      });
    } else {
      this.mainplayer = WowzaPlayer.create('videoPlayer', {
        "license": "PLAY1-3zveE-8mZFG-HUjva-W8rdG-Q3BvW",
        "title": '',
        "description": '',
        "autoPlay": false,
        "mute": false,
        "volume": 100
      });
    }
  }
  reload(streamUrl) {
    jwplayer().load(streamUrl);
    jwplayer().play();
  }

  jumpLive() {
    jwplayer().seek(-jwplayer().getDuration())
  }

  setUpJwPlayer(source: string) {
    jwplayer('videoPlayer').setup({
      sources: [{
        file: source
      }],
      autostart: true,
      responsive: true,
      width: '100%',
      //aspectratio: "16:9",
      height: window.innerHeight + 'px',
      rtmp: {
        bufferlength: 5
      },
      fallback: true,

    });

    //reconnect every 5 seconds

    jwplayer('videoPlayer').onIdle(() => {
      this.playerTimer = setTimeout(jwplayer('videoPlayer').play(), 5000);
    });

    jwplayer('videoPlayer').onError((e) => {
      console.log("onError caught and resetting!!!", e);
      this.reload(source);
    });
  }

  //#endregion

  //#region Room

  async connectToRoom(room: string, token: string, items: LiveStream[], chatToken?: string) {
    try {
      this.collaborationItems = items;
      if (this.collaborationItems.length > 0) {
        const firstVideo = this.collaborationItems[0];
        // this.setupWowzaPlayer(firstVideo ? firstVideo.url : '', true);
        this.setUpJwPlayer(firstVideo ? firstVideo.url : '');
      }
      this.dataTrack = this.setupLocalDataTrack();
      this.audioVideoTracks = await this.setupLocalAudioAndVideoTracks();
      const tracks = this.audioVideoTracks.concat(this.dataTrack);
      this.twilioRoom = await connect(token, {
        name: room,
        tracks: tracks,
        audio: { name: 'microphone' },
        video: true
      });
      if (this.twilioRoom) {
        this.chatToken = chatToken;
        this.chatChannelName = this.twilioRoom.name;
      }
      this.allowControls = true;
      const { identity } = this.twilioRoom.localParticipant;
      this.currentUser = this.sessionParticipants.find(p => p.displayName === identity);
      const localUser = { id: this.currentUser.id, name: identity, description: this.currentUser.isHost ? HOST : PARTICIPANT, isPresenting: false };
      this.connectedParticipants.push(localUser);
      this.twilioRoom.participants.forEach(this.participantConnected);
      this.twilioRoom.on('participantConnected', this.participantConnected);
      this.twilioRoom.on('participantDisconnected', this.participantDisconnected);
      this.twilioRoom.once('disconnected', () => {
        this.twilioRoom.participants.forEach(this.participantDisconnected);
      });
      this.twilioRoom.on('disconnected', error => {
        if (error.code === 20104) {
          this.store.dispatch(new ShowWarningMessage(`Signaling reconnection failed due to expired AccessToken! ${error.message}`));
        } else if (error.code === 53000) {
          this.store.dispatch(new ShowWarningMessage(`Signaling reconnection attempts exhausted! ${error.message}`));
        } else if (error.code === 53204) {
          this.store.dispatch(new ShowWarningMessage(`Signaling reconnection took too long! ${error.message}`));
        }
      });
      this.twilioRoom.on('reconnecting', error => {
        if (error.code === 53001) {
          this.store.dispatch(new ShowWarningMessage(`Reconnecting your signaling connection! ${error.message}`));
        } else if (error.code === 53405) {
          this.store.dispatch(new ShowWarningMessage(`Reconnecting your media connection!! ${error.message}`));
        }
      });
      this.twilioRoom.on('reconnected', () => {
        this.store.dispatch(new ShowSuccessMessage(`You have been reconnected to the room!`));
      });
    } catch (error) {
      this.showErrorMessage(error);
      this.router.navigate(['collaborations/current']);
    }

    this.showWarningMessage("You need to share screen to record video.", null, 0);
    this.scheduleRoomStatusCheck();
  }

  private scheduleRoomStatusCheck() {
    this.roomStatusCheck = setInterval(() => {
      console.log("checking room status...");
      this.collaborationsService.isSessionEnded(this.currentCollaborationId)
        .then(isEnded => {
          if (isEnded) {
            this.showWarningMessage("The meeting has been ended by host/presenter.");
            this.leaveRoom();
          }
        });
    }, 60 * 3000);
  }

  private stopRoomStatusCheck() {
    clearInterval(this.roomStatusCheck);
  }

  disconnectFromRoom() {
    if (this.twilioRoom) {
      this.twilioRoom.disconnect();
    }
  }

  sendMessage(message: string) {
    const data = { message: message };
    this.dataTrack.send(JSON.stringify(data));
  }

  //#endregion

  //#region Participants

  participantConnected = (participant: any) => {
    console.log(`Collaboration Viewer - Participant "${participant.identity}" has connected to the Room`);
    const remoteUser = this.sessionParticipants.find(x => x.displayName === participant.identity);
    if (remoteUser.isHost) {
      // this.showInfoMessage("Host has joined.");
    }
    const remoteParticipant = { id: remoteUser.id, name: participant.identity, description: remoteUser.isHost ? HOST : PARTICIPANT, isPresenting: false };
    this.connectedParticipants.push(remoteParticipant);
    participant.tracks.forEach(trackPublication => {
      trackPublication.on('unsubscribed', track => {
        this.trackUnsubscribed(participant, track);
      });
    });
    participant.on('trackSubscribed', track => {
      this.trackSubscribed(participant, track);
    });
    participant.on('trackPublished', publication => {
      this.trackPublished(participant, publication);
      if (publication.track === this.dataTrack) {
        this.dataTrackPublished.resolve();
      }
      publication.on('unsubscribed', track => {
        this.trackUnsubscribed(participant, track);
      });
    });

    participant.on('trackPublicationFailed', (error: any, track: any) => {
      if (track === this.dataTrack) {
        this.dataTrackPublished.reject(error);
      }
    });

    participant.on('trackAdded', track => {
      if (track.kind === 'data') {
        const color = this.colorHash.hex(track.sid);
        track.on('message', data => {
          const { message, fileName, coordinates } = JSON.parse(data);
          const presenter = this.connectedParticipants.find(x => x.isPresenting);
          const localParticipant = this.twilioRoom.localParticipant;
          switch (message) {
            case dataMessage.refreshScreenshots:
              this.store.dispatch(new GetCollaborationScreenShots(this.currentCollaborationId));
              break;
            case dataMessage.takeScreenshot:
              if (presenter.name === localParticipant.identity) {
                this.screenshot(fileName);
              }
              break;
            case dataMessage.hostHasJoined:
              this.showSuccessMessage("Host has joined.");
              break;
            case dataMessage.annotation:
              if (!presenter) return;
              if (presenter.name === localParticipant.identity) {
                const localX = (coordinates.x * window.innerWidth) / 100;
                const localY = (coordinates.y * window.innerHeight) / 100;
                const localLastX = (coordinates.lastX * window.innerWidth) / 100;
                const localLastY = (coordinates.lastY * window.innerHeight) / 100;
                this.drawOnRemote(color, localX, localY, localLastX, localLastY);
              }
            default:
              break;
            case dataMessage.roomEnded:
              this.showSpinner();
              this.showWarningMessage("The meeting has been ended by host/presenter.");
              setTimeout(() => {
                this.leaveRoom();
              }, 2000);
              break;
          }
        });
      }
    });

    participant.on('trackUnpublished', publication => {
      this.trackUnpublished(participant, publication)
    });
  }

  participantDisconnected = (participant: any) => {
    this.connectedParticipants = this.connectedParticipants.filter(p => p.name !== participant.identity);
  }

  //#endregion

  //#region Tracks

  setupLocalDataTrack = () => {
    const dataTrack: LocalDataTrack = new LocalDataTrack();
    this.dataTrackPublished.promise = new Promise((resolve, reject) => {
      this.dataTrackPublished.resolve = resolve;
      this.dataTrackPublished.reject = reject;
    });
    let percentageCoordinates: { x: number, y: number, lastX: number, lastY: number } = { x: 0, y: 0, lastX: 0, lastY: 0 };
    this.canvas.addEventListener('mousedown', e => {
      this.isDrawing = true;
      [this.lastX, this.lastY] = [e.offsetX, e.offsetY];
      percentageCoordinates.lastX = ((this.lastX * 100) / window.innerWidth);
      percentageCoordinates.lastY = ((this.lastY * 100) / window.innerHeight);
    }, false);

    this.canvas.addEventListener('mousemove', event => {
      const e = event as any;
      const x = e.offsetX || e.layerX - this.canvas.offsetLeft;
      const y = e.offsetY || e.layerY - this.canvas.offsetTop;
      percentageCoordinates.x = ((x * 100) / window.innerWidth);
      percentageCoordinates.y = ((y * 100) / window.innerHeight);

      if (this.isDrawing) {
        if (this.isLocalParticipantPresenting) {
          const color = this.colorHash.hex(dataTrack.id);
          this.draw(color, x, y);
        } else {
          const data = { message: dataMessage.annotation, coordinates: percentageCoordinates };
          this.dataTrack.send(JSON.stringify(data));
          percentageCoordinates.lastX = percentageCoordinates.x;
          percentageCoordinates.lastY = percentageCoordinates.y;
        }
      }
    }, false);

    this.canvas.addEventListener('mouseup', () => {
      this.isDrawing = false;
    }, false);
    this.canvas.addEventListener('mouseout', () => {
      this.isDrawing = false;
    }, false);
    return dataTrack;
  }

  trackPublished = (participant: any, publication: any) => {
    // publication.on('subscribed', track => this.trackSubscribed(participant, track));
    // publication.on('unsubscribed', track => this.trackUnsubscribed(participant, track));
  }

  trackUnpublished = (participant: any, publication: any) => {
    if (publication.name === 'screen') {
      this.videoPlayer.nativeElement.source = 'https://d2zihajmogu5jn.cloudfront.net/bipbop-advanced/bipbop_16x9_variant.m3u8';
      publication.detach();
    }
  }

  trackSubscribed = (participant: any, track: RemoteTrack) => {
    if (track) {
      if (track.kind === 'video' && track.name === 'screen') {
        this.isRemoteParticipantPresenting = true;
        this.isLocalParticipantPresenting = false;
        const remoteParticipant = this.connectedParticipants.find(p => p.name === participant.identity);
        remoteParticipant.isPresenting = true;
        remoteParticipant.description = PRESENTER;
        this.isLocalVideoMuted = this.mainplayer.isMuted();
        this.mainplayer.mute(true);
        this.closeToast();
        this.showInfoMessage(`${remoteParticipant.name}'s screen is being recorded.`, 10000);
        console.log("mainplayer is muted: ", this.isLocalVideoMuted);
      } else if (track.kind === 'video') {
        this.isRemoteParticipantPresenting = false;
        this.isLocalParticipantPresenting = false;
        this.allowAnnotation = false;
      }

      if (track.kind === 'audio' || track.kind === 'video') {
        track.attach(`#sharingDiv`);
      }
    }
  }

  trackUnsubscribed = (participant: any, track: RemoteTrack) => {
    if (track.kind === 'audio' || track.kind === 'video') {
      this.isRemoteParticipantPresenting = false;
      this.isLocalParticipantPresenting = false;
      this.allowAnnotation = false;
      track.detach();
      this.closeToast();
      this.showInfoMessage(`${participant.identity} stopped sharing screen.`, 10000);
      setTimeout(() => {
        this.showWarningMessage("You need to share screen to record video.", null, 0);
      }, 10000);
      this.mainplayer.mute(!this.isLocalVideoMuted);
      console.log("mainplayer is muted: ", this.mainplayer.isMuted());
      const remoteUser = this.sessionParticipants.find(x => x.displayName === participant.identity);
      const remoteParticipant = this.connectedParticipants.find(p => p.name === participant.identity);
      remoteParticipant.isPresenting = false;
      remoteParticipant.description = remoteUser.isHost ? HOST : PARTICIPANT;
    }
  }

  async setupLocalAudioAndVideoTracks() {
    const audioAndVideoTrack: LocalTrack[] = await createLocalTracks({ audio: true, video: false });
    return audioAndVideoTrack;
  }

  //#endregion

  //#region CONTROLS

  //#region Share Screen

  shareScreen = async () => {
    const localParticipant = this.connectedParticipants.find(p => p.name === this.twilioRoom.localParticipant.identity);
    if (this.isLocalParticipantPresenting) {
      this.twilioRoom.localParticipant.removeTrack(this.screenLocalTrack);
      this.closeToast();
      this.showWarningMessage("You need to share screen to record video.", null, 0);
      localParticipant.description = this.currentUser.isHost ? HOST : PARTICIPANT;
      this.isLocalParticipantPresenting = false;
      this.isRemoteParticipantPresenting = false;
      this.allowAnnotation = false;
    } else {
      let navigatorObject = navigator.mediaDevices as any;
      this.stream = await navigatorObject.getDisplayMedia();
      this.isLocalParticipantPresenting = true;
      this.isRemoteParticipantPresenting = false;
      localParticipant.isPresenting = true;
      localParticipant.description = PRESENTER;
      this.screenshotVideo.srcObject = this.stream;
      this.screenshotVideo.play();
      const options: LocalTrackOptions = { name: 'screen', logLevel: 'warn' };
      const screenLocalTrack = new LocalVideoTrack(this.stream.getTracks()[0], options);
      this.screenLocalTrack = screenLocalTrack;
      this.twilioRoom.localParticipant.addTrack(screenLocalTrack);
      this.closeToast();
      this.showInfoMessage(`Your screen is being recorded.`, 5000);
      screenLocalTrack.once('stopped', () => {
        this.twilioRoom.localParticipant.removeTrack(screenLocalTrack);
        this.isLocalParticipantPresenting = false;
        this.isRemoteParticipantPresenting = false;
      });
    }
  }

  //#endregion 

  //#region ScreenShot 

  screenshot(name?: string) {
    const initials = this.fieldsService.getInitialsFromFullname(this.twilioRoom.localParticipant.identity);
    const count = this.store.selectSnapshot(CollaborationsState.getScreenshotNameCounter);
    const fileName = name || `SS_${initials}_${count}.png`;
    if (this.isLocalParticipantPresenting) {
      this.canvas.getContext('2d').drawImage(this.screenshotVideo as any, 0, 0, window.innerWidth, window.innerHeight);
      let dataUrl = this.canvas.toDataURL('image/png');
      const image = this.dataURLtoFile(dataUrl, fileName);
      this.screenshotUploadService.upload(false, image, null, this.containerName, this.sasToken, this.storageAccount, this.currentCollaborationId);
      if (!name) {
        this.simulateScreenShot();
        this.store.dispatch(new IncrementScreenshots(count));
      }
      this.clearCanvas();
    } else {
      this.simulateScreenShot();
      this.store.dispatch(new IncrementScreenshots(count));
      const data = { message: dataMessage.takeScreenshot, fileName: fileName };
      this.dataTrack.send(JSON.stringify(data));
    }
  }

  dataURLtoFile(dataurl: string, filename: string) {
    var arr = dataurl.split(','),
      mime = arr[0].match(/:(.*?);/)[1],
      bstr = atob(arr[1]),
      n = bstr.length,
      u8arr = new Uint8Array(n);

    while (n--) {
      u8arr[n] = bstr.charCodeAt(n);
    }

    return new File([u8arr], filename, { type: mime });
  }

  private simulateScreenShot() {
    const screenshotBeep = new Audio();
    screenshotBeep.src = "./assets/audio/screenshotbeep.mp3";
    screenshotBeep.play();
    const videoPlayer = document.querySelector("#videoPlayer") as HTMLElement;
    if (videoPlayer) {
      videoPlayer.style.border = '4px solid #FFF';
      videoPlayer.style.opacity = '0.6';
      setTimeout(() => {
        videoPlayer.style.border = '';
        videoPlayer.style.opacity = '';
      }, 500);
    }
  }

  deleteScreenShot(id: number) {
    this.store.dispatch(new DeleteCollaborationScreenShot(this.currentCollaborationId, id)).toPromise()
      .then(() => {
        const data = { message: dataMessage.refreshScreenshots };
        this.dataTrack.send(JSON.stringify(data));
        this.store.dispatch(new GetCollaborationScreenShots(this.currentCollaborationId));
      });
  }

  //#endregion

  //#region Annotate

  annotate() {
    this.allowAnnotation = !this.allowAnnotation;
  }

  draw1(x: number, y: number) {
    if (!this.isActive) return;
    this.plots.push({ x: x, y: y });
  }

  startDraw() {
    this.isActive = true;
  }

  endDraw() {
    this.isActive = false;
    if (this.isLocalParticipantPresenting) {
      // this.drawOnCanvas('#ff0000', this.plots);
    } else {
      this.publishAnnotations();
    }
    this.plots = [];
  }

  publishAnnotations() {
    const data = { message: dataMessage.annotation, plots: this.plots };
    this.dataTrack.send(JSON.stringify(data));
  }

  draw(color: any, x: number, y: number) {
    const ctx = this.canvas.getContext('2d');
    ctx.lineWidth = 6;
    ctx.lineCap = 'round';
    ctx.lineJoin = 'round';
    ctx.strokeStyle = color;
    ctx.beginPath();
    ctx.moveTo(this.lastX, this.lastY);
    ctx.lineTo(x, y);
    ctx.stroke();
    [this.lastX, this.lastY] = [x, y];

    this.countDown = 10;
    if (!this.isMonitoringAnnotation) {
      this.isMonitoringAnnotation = true;
      this.scheduleCanvasClear();
    }
  }

  drawOnRemote(color: any, x: number, y: number, lastX: number, lastY: number) {
    const ctx = this.canvas.getContext('2d');
    ctx.lineWidth = 6;
    ctx.lineCap = 'round';
    ctx.lineJoin = 'round';
    ctx.strokeStyle = color;
    ctx.beginPath();
    ctx.moveTo(lastX, lastY);
    ctx.lineTo(x, y);
    ctx.stroke();

    this.countDown = 10;
    if (!this.isMonitoringAnnotation) {
      this.isMonitoringAnnotation = true;
      this.scheduleCanvasClear();
    }
  }

  drawOnCanvas(color: any, plots: any[]) {
    const ctx = this.canvas.getContext('2d');
    ctx.lineWidth = 6;
    ctx.lineCap = 'round';
    ctx.lineJoin = 'round';
    ctx.strokeStyle = color;
    ctx.beginPath();
    ctx.moveTo(plots[0].x, plots[0].y);

    for (var i = 1; i < plots.length; i++) {
      ctx.lineTo(plots[i].x, plots[i].y);
    }
    ctx.stroke();

    this.countDown = 10;
    if (!this.isMonitoringAnnotation) {
      this.isMonitoringAnnotation = true;
      this.scheduleCanvasClear();
    }
  }

  scheduleCanvasClear() {
    if (this.countDown <= 0) {
      this.clearCanvas();
      this.countDown = 10;
      this.isMonitoringAnnotation = false;
    }
    else {
      setTimeout(() => {
        this.countDown -= 1;
        this.scheduleCanvasClear();
      }, 1000);
    }
  }

  clearCanvas() {
    this.canvas.getContext('2d').clearRect(0, 0, window.innerWidth, window.innerHeight);
  }

  //#endregion 

  //#region Chat

  minimizeChatBox($event: any) {
    this.openChat = $event;
  }

  toggleChatBox() {
    this.openChat = !this.openChat;
  }

  onMessageAdded(args: boolean) {
    this.isChatMessageAdd = args;
  }

  //#endregion 

  //#region Add Participants

  showAddParticipantsModal() {
    this.participantModal.getUsers();
    this.sessionParticipants.forEach(x => x.isCurrent = true);
    this.participantModal.canSaveParticipants = false;
    this.participantModal.setPaginatedParticipants();
    this.participantModal.show();
  }

  onSaveParticipants(participants: User[]) {
    this.showSpinner();
    const userIds = participants.map(x => x.id);
    this.collaborationsService.updateSessionParticipants(this.currentCollaborationId, userIds).toPromise()
      .then(() => {
        this.store.dispatch(new GetCollaborationSession(this.currentCollaborationId));
        this.showSuccessMessage("Participants updated successfully.");
        this.hideSpinner();
      }, err => this.showErrorMessage(err));
  }

  //#endregion

  //#region Mute

  mute() {
    if (this.twilioRoom) {
      const localParticipant: any = this.twilioRoom.localParticipant;
      if (localParticipant) {
        this.isLocalParticipantMuted = !this.isLocalParticipantMuted;
        localParticipant.audioTracks.forEach(track => {
          if (this.isLocalParticipantMuted)
            track.disable();
          else
            track.enable();
        });
      }
    }
  }

  //#endregion

  //#region Leave Room

  confirmLeave() {
    const subject = new Subject<boolean>();
    this.confirmModal.title = "Confirm Leave";
    this.confirmModal.message = "Are you sure you want to leave this session?";
    this.confirmModal.show();
    this.confirmModal.response$ = subject;
    subject.toPromise()
      .then(response => {
        if (response) {
          this.leaveRoom();
        }
      });
  }

  leaveRoom() {
    this.router.navigate(['/collaborations']);
  }

  //#endregion 

  //#region End Room

  confirmEnd() {
    const subject = new Subject<boolean>();
    this.confirmModal.title = "Confirm End";
    this.confirmModal.message = "Are you sure you want to end this session?";
    this.confirmModal.show();
    this.confirmModal.response$ = subject;
    subject.toPromise()
      .then(response => {
        if (response) {
          this.endRoom();
        }
      });
  }

  endRoom() {
    this.sendMessage(dataMessage.roomEnded);
    this.showSpinner();
    setTimeout(() => {
      this.store.dispatch(new LeaveRoom(this.currentCollaborationId)).toPromise()
        .then(() => {
          this.disconnectFromRoom();
          this.router.navigate(['/collaborations']);
        });
    }, 3000);
  }

  //#endregion 

  //#endregion
}
