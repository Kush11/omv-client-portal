import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CollaborationViewerComponent } from './collaboration-viewer.component';

describe('CollaborationViewerComponent', () => {
  let component: CollaborationViewerComponent;
  let fixture: ComponentFixture<CollaborationViewerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CollaborationViewerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CollaborationViewerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should contain "collaboration-viewer works!"', () => {
    const viewerElement: HTMLElement = fixture.nativeElement;
    expect(viewerElement.textContent).toContain('collaboration-viewer works!');
  });
});
