import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-collaboration-controls',
  templateUrl: './collaboration-controls.component.html',
  styleUrls: ['./collaboration-controls.component.css']
})
export class CollaborationControlsComponent implements OnInit {

  @Input() allowControls: boolean;
  @Input() showScreenShare: boolean;
  @Input() isPresenting: boolean;
  @Input() showAnnotate: boolean;
  @Input() allowAnnotation: boolean;
  @Input() showScreenshot: boolean;
  @Input() savingScreenshot: boolean;
  @Input() muted: boolean;
  @Input() showLeaveRoom: boolean;
  @Input() showEndRoom: boolean;
  @Input() ischatMessageAdded: boolean;
  @Output() shareScreen = new EventEmitter<any>();
  @Output() annotate = new EventEmitter();
  @Output() screenshot = new EventEmitter<any>();
  @Output() chat = new EventEmitter();
  @Output() addParticipant = new EventEmitter<any>();
  @Output() mute = new EventEmitter<any>();
  @Output() leave = new EventEmitter<any>();
  @Output() end = new EventEmitter<any>();

  isAnnotateActive: boolean;
  isChatActive: boolean;
  isCollapsed: boolean;

  constructor() { }

  ngOnInit() {
  }

  onShareScreen() {
    this.shareScreen.emit();
  }

  onAnnotate() {
    this.annotate.emit();
  }

  onScreenshot() {
    if (!this.savingScreenshot) {
      this.screenshot.emit();
    }
  }

  onChat() {
    this.chat.emit();
  }

  onAddParticipant() {
    this.addParticipant.emit();
  }

  onMute() {
    this.mute.emit();
  }

  onLeaveRoom() {
    this.leave.emit();
  }

  onEndRoom() {
    this.end.emit();
  }
}
