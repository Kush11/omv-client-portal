import { Injectable } from '@angular/core';
import { BlobService, UploadParams } from 'angular-azure-blob-service';
import { Store } from '@ngxs/store';
import { MediaItem } from 'src/app/core/models/entity/media';
import { ShowErrorMessage } from 'src/app/state/app.actions';
import { CreateCollaborationScreenShot } from '../../state/collaborations.action';

@Injectable({
  providedIn: 'root'
})
export class ScreenshotUploadService {


  config: any;
  percent: any;
  sasToken: any;
  storageAccount: any;
  containerName: any;

  constructor(private store: Store, private blob: BlobService) { }
  upload(isSRAllowed: boolean, file: File, metadata: string, containerName: string, sasToken: string, storageAccount: string,
    collaborationId: number) {
    const Config: UploadParams = {
      sas: sasToken,
      storageAccount: storageAccount,
      containerName: containerName
    };
    let splitByLastDot = function (text) {
      const index = text.lastIndexOf('.');
      return [text.slice(0, index), text.slice(index + 1)]
    }
    if (file !== null) {
      const baseUrl = this.blob.generateBlobUrl(Config, file.name);
      const customBlockSize = this.getBlockSize(file.size);
      this.config = {
        baseUrl: baseUrl,
        sasToken: Config.sas,
        blockSize: customBlockSize,
        file: file,
        complete: () => {
          let mediaItem = new MediaItem();
          mediaItem.metadata = metadata;
          mediaItem.size = file.size;
          mediaItem.name = file.name;
          mediaItem.contentType = file.type;
          mediaItem.url = this.config.baseUrl;
          mediaItem.documentTypeCode = 'png';
          mediaItem.isSRAllowed = isSRAllowed;
          this.store.dispatch(new CreateCollaborationScreenShot(collaborationId, mediaItem));
        },
        error: (err: { statusText: any; }) => {
          this.store.dispatch(new ShowErrorMessage(err.statusText));
        },
        progress: (percent: any) => {
          this.percent = percent;
          console.log(percent);
        }
      };
      this.blob.upload(this.config);
    }
  }

  private getBlockSize(filesize: number) {
    let customBlockSize = 0;
    const size10Gb = 1024 * 1024 * 1024 * 10;
    const size500Mb = 1024 * 1024 * 500;
    const size32Mb = 1024 * 1024 * 32;
    const size5mb = 1024 * 1024 * 5;
    if (filesize < size32Mb) {
      customBlockSize = (1024 * 512);
    } else if (filesize < size500Mb) {
      customBlockSize = (1024 * 1024 * 4);
    } else if (filesize < size10Gb) {
      customBlockSize = (1024 * 1024 * 10);
    } else {
      customBlockSize = (1024 * 1024 * 100);
    }
    return customBlockSize;
  }
}
