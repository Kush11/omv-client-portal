import { TestBed } from '@angular/core/testing';

import { ScreenshotUploadService } from './screenshot-upload.service';

describe('ScreenshotUploadService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ScreenshotUploadService = TestBed.get(ScreenshotUploadService);
    expect(service).toBeTruthy();
  });
});
