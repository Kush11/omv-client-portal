import { Component, OnInit, OnDestroy } from '@angular/core';
import { Tab } from '../core/models/tab';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { Store, Select } from '@ngxs/store';
import { Tag } from '../core/models/entity/tag';
import { BaseComponent } from '../shared/base/base.component';
import { FieldConfiguration } from '../shared/dynamic-components/field-setting';
import { SetPreviousRoute } from '../state/app.actions';
import { Observable } from 'rxjs/internal/Observable';
import { Breadcrumb } from '../core/models/breadcrumb';
import { CollaborationsState } from './state/collaborations.state';
import { SubSink } from 'subsink/dist/subsink';
import { SetBreadcrumbs, AddFilterTag } from './state/collaborations.action';
import { filter } from 'rxjs/internal/operators/filter';

const CURRENT_LINK = '/collaborations/current';
const ARCHIVE_LINK = '/collaborations/archive';

@Component({
  selector: 'app-collaborations',
  templateUrl: './collaborations.component.html',
  styleUrls: ['./collaborations.component.css']
})
export class CollaborationsComponent extends BaseComponent implements OnInit, OnDestroy {

  @Select(CollaborationsState.getBreadcrumbs) breadcrumbs$: Observable<Breadcrumb[]>;

  private subs = new SubSink();
  tabs: Tab[] = [
    { link: '/collaborations/current', name: 'Current Collaborations', isActive: true },
    // { link: '/collaborations/archive', name: 'Collaborations Archive', query: 'tile' }
  ];
  showTreeView: boolean;
  showMapView: boolean;
  filterTags: Tag[] = [];
  filterFields: FieldConfiguration[] = [];
  showFilterMenu: boolean;

  constructor(protected store: Store, private router: Router, private route: ActivatedRoute) {
    super(store);
    this.hideLeftNav();
  }

  ngOnInit() {
    this.setBreadCrumbByUrl(this.router.url);
    this.subs.add(
      this.router.events
        .pipe(
          filter(event => event instanceof NavigationEnd))
        .subscribe((event: NavigationEnd) => {
          this.setBreadCrumbByUrl(event.url);
        }),
    );
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

  createNew() {
    this.store.dispatch(new SetPreviousRoute(this.router.url));
    this.router.navigate([`collaborations/0/setup`], { queryParams: { stage: 1 } });
  }

  switchTabs(tabLink: string) {
    const tab = this.tabs.find(t => t.link === tabLink);
    if (tab.query) {
      this.router.navigate([tabLink], { queryParams: { view: 'tile' } });
    } else {
      this.router.navigate([tabLink]);
    }
  }

  tapBreadCrumb(link: string) {

  }

  setBreadCrumbByUrl(url: string) {
    url = url.split('?')[0];
    let crumbs: Breadcrumb[];
    switch (url) {
      case CURRENT_LINK:
        crumbs = [
          { link: '/collaborations/current', name: 'Collaborations' },
          { link: '', name: 'Current Collaborations' }
        ];
        this.store.dispatch(new SetBreadcrumbs(crumbs));
        break;
      case ARCHIVE_LINK:
        crumbs = [
          { link: '/collaborations/archive', name: 'Collaborations' },
          { link: '', name: 'Collaborations Archive' }
        ];
        this.store.dispatch(new SetBreadcrumbs(crumbs));
        break;
    }
  }

  //#region Filter

  onFilterSearch(query: string) {
    const tag: Tag = { name: 'queryFilter', label: '', value: query, cssClass: 'tag-search' };
    this.store.dispatch(new AddFilterTag(tag));
  }

  //#endregion
}
