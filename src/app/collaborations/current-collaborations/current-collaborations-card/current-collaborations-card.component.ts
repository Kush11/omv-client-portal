import { Component, OnInit, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { CollaborationSession } from 'src/app/core/models/entity/collaboration';
import { Router } from '@angular/router';
import { BaseComponent } from 'src/app/shared/base/base.component';
import { Store, Select } from '@ngxs/store';
import { ModalComponent } from 'src/app/shared/modal/modal.component';
import { User } from 'src/app/core/models/entity/user';
import { CollaborationsState } from '../../state/collaborations.state';
import { Observable } from 'rxjs/internal/Observable';
import { CollaborationsService } from 'src/app/core/services/business/collaborations/collaborations.service';
import { trigger, style, transition, animate } from '@angular/animations';
import { DeleteCollaborationSession } from '../../state/collaborations.action';
import { Status } from 'src/app/core/enum/status.enum';
import { AppState } from 'src/app/state/app.state';

@Component({
  selector: 'app-current-collaborations-card',
  templateUrl: './current-collaborations-card.component.html',
  styleUrls: ['./current-collaborations-card.component.css'],
  animations: [
    trigger('slideInOut', [
      transition(':enter', [

      ]),
      transition(':leave', [
        animate('1000ms ease-in', style({ height: '0%', opacity: 0 }))
      ])
    ])
  ]
})
export class CurrentCollaborationsCardComponent extends BaseComponent implements OnInit {

  @Select(CollaborationsState.getUsers) users$: Observable<User[]>;
  @Select(CollaborationsState.getTotalUsers) totalUsers$: Observable<number>;

  @Input() session: CollaborationSession;

  @Output() notify = new EventEmitter<any>();

  @ViewChild('participantModal') participantModal: ModalComponent;
  @ViewChild('confirmModal') confirmModal: ModalComponent;

  participantNames: string;
  participants: User[] = [];
  users: User[] = [];

  pending = Status.Pending;
  inProgress = Status.InProgress;
  processing = Status.Processing;
  completed = Status.Completed;

  visible = true;
  deleteMessage: string;

  constructor(protected store: Store, private router: Router, private collaborationsService: CollaborationsService) {
    super(store);
  }

  ngOnInit() {
    this.participantNames = this.session.participantNames;
    this.users$
      .subscribe(users => {
        this.users = JSON.parse(JSON.stringify(users));
        this.users.forEach(user => {
          user.isSelected = this.participants.map(p => p.displayName).includes(user.displayName);
        });
      });
  }

  editSession() {
    this.router.navigate([`collaborations/${this.session.id}/setup`], { queryParams: { step: 1 } });
  }

  joinSession() {
    const isBrowserSupported = this.isBrowserSupported();
    if (isBrowserSupported) {
      this.router.navigate([`collaborations/${this.session.id}/join`]);
    } else {
      this.showWarningMessage("This feature is not supported on this browser. It is currently supported on Chrome and Firefox.", "Unsupported Browser");
    }
  }

  showConfirmDelete() {
    this.deleteMessage = `Are you sure you want to delete "${this.session.title}"?`;
    this.confirmModal.show();
  }

  removeCollaboration() {
    this.showSpinner();
    this.collaborationsService.removeSession(this.session.id)
      .then(() => {
        this.notify.emit();
        this.visible = false;
        this.showSuccessMessage(`${this.session.title} was successfully deleted.`);
        setTimeout(() => {
          this.store.dispatch(new DeleteCollaborationSession(this.session.id));
        }, 1000);
        this.hideSpinner();
      }, err => this.showErrorMessage(err));
  }

  //#region Participants

  addParticipant() {
    this.participantModal.getUsers();
    this.participants = JSON.parse(JSON.stringify(this.session.participants));
    const currentUser = this.store.selectSnapshot(AppState.getLoggedInUser);
    this.participants.forEach(p => p.isCurrent = p.displayName === currentUser.displayName);
    this.participants.forEach(x => x.isParticipant = true);
    this.participantModal.canSaveParticipants = false;
    this.participantModal.show();
  }

  onSaveParticipants(participants: User[]) {
    this.showSpinner();
    const userIds = participants.map(x => x.id);
    this.collaborationsService.updateSessionParticipants(this.session.id, userIds).toPromise()
      .then(() => {
        this.participantNames = participants.map(x => x.displayName).join(', ');
        this.showSuccessMessage("Participants updated successfully.");
        this.hideSpinner();
      }, err => this.showErrorMessage(err));
  }

  //#endregion

  private isBrowserSupported() {
    if (navigator.userAgent.indexOf("Chrome") != -1 || navigator.userAgent.indexOf("Firefox") != -1) {
      return true;
    }
    return false;
  }
}
