import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CurrentCollaborationsCardComponent } from './current-collaborations-card.component';

describe('CurrentCollaborationsCardComponent', () => {
  let component: CurrentCollaborationsCardComponent;
  let fixture: ComponentFixture<CurrentCollaborationsCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CurrentCollaborationsCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CurrentCollaborationsCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
