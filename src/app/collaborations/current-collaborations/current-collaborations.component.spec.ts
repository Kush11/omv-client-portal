import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CurrentCollaborationsComponent } from './current-collaborations.component';

describe('CurrentCollaborationsComponent', () => {
  let component: CurrentCollaborationsComponent;
  let fixture: ComponentFixture<CurrentCollaborationsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CurrentCollaborationsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CurrentCollaborationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
