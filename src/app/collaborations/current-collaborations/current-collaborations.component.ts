import { Component, OnInit, OnDestroy } from '@angular/core';
import { Store, Select } from '@ngxs/store';
import { CollaborationsState } from '../state/collaborations.state';
import { Observable } from 'rxjs/internal/Observable';
import { CollaborationSession } from 'src/app/core/models/entity/collaboration';
import { BaseComponent } from 'src/app/shared/base/base.component';
import { Tag } from 'src/app/core/models/entity/tag';
import { RemoveFilterTag, GetCollaborationDays, GetCollaborationSessions } from '../state/collaborations.action';
import { Router } from '@angular/router';
import { SubSink } from 'subsink/dist/subsink';

@Component({
  selector: 'app-current-collaborations',
  templateUrl: './current-collaborations.component.html',
  styleUrls: ['./current-collaborations.component.css']
})
export class CurrentCollaborationsComponent extends BaseComponent implements OnInit, OnDestroy {

  @Select(CollaborationsState.getCollaborationSessions) collaborationSessions$: Observable<CollaborationSession[]>;
  @Select(CollaborationsState.getUpcomingSessionsCount) upCompingSessionsCount$: Observable<number>;
  @Select(CollaborationsState.getFilterTags) filterTags$: Observable<Tag[]>;

  private subs = new SubSink();
  sessions: CollaborationSession[] = [];
  paginatedSessions: CollaborationSession[] = [];
  pageSize = 4;
  pageSizes = ["4", "10", "All"]
  currentPage = 1;
  isDeletePerformed: boolean;
  isPageChanged: boolean;

  constructor(protected store: Store, private router: Router) {
    super(store);
    this.hideLeftNav();
    this.setPageTitle("Current Collaborations");
  }

  ngOnInit() {
    this.subs.add(
      this.collaborationSessions$
        .subscribe(sessions => {
          this.sessions = sessions;
          this.setPaginatedSessions();
        })
    );
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

  removeTag() {
    this.store.dispatch(new RemoveFilterTag());
  }

  refresh() {
    const currentDate = this.store.selectSnapshot(CollaborationsState.getCurrentDateFilter);
    this.store.dispatch(new GetCollaborationDays(currentDate));
    this.store.dispatch(new GetCollaborationSessions(currentDate));
  }

  viewArchives() {
    this.router.navigate(['media'], { queryParams: { filter: 'collaborations' } });
  }

  changePage(pageNumber: number) {
    this.currentPage = pageNumber;
    this.isPageChanged = true;
    this.setPaginatedSessions();
  }

  deleteNotification() {
    this.isDeletePerformed = true;
  }

  setPaginatedSessions() {
    const firstSession = this.sessions[0];
    if (firstSession) {
      if (!this.isDeletePerformed && !this.isPageChanged) {
        this.currentPage = 1;
      } else {
        this.isDeletePerformed = false;
        this.isPageChanged = false;
      }
      const start = (this.currentPage - 1) * (this.pageSize);
      const end = start + (this.pageSize);
      this.paginatedSessions = this.sessions.slice(start, end);
      if (this.paginatedSessions.length === 0) {
        if (this.currentPage > 1) {
          this.currentPage--;
          const start = (this.currentPage - 1) * (this.pageSize);
          const end = start + (this.pageSize);
          this.paginatedSessions = this.sessions.slice(start, end);
        }
      }
    } else {
      this.paginatedSessions = [];
    }
  }
}
