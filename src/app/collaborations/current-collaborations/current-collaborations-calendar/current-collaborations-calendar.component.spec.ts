import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CurrentCollaborationsCalendarComponent } from './current-collaborations-calendar.component';

describe('CurrentCollaborationsCalendarComponent', () => {
  let component: CurrentCollaborationsCalendarComponent;
  let fixture: ComponentFixture<CurrentCollaborationsCalendarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CurrentCollaborationsCalendarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CurrentCollaborationsCalendarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
