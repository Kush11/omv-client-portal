import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { CollaborationsState } from '../../state/collaborations.state';
import { Select, Store } from '@ngxs/store';
import { DateService } from 'src/app/core/services/business/dates/date.service';
import { GetCollaborationDays, GetCollaborationSessions } from '../../state/collaborations.action';

@Component({
  selector: 'app-current-collaborations-calendar',
  templateUrl: './current-collaborations-calendar.component.html',
  styleUrls: ['./current-collaborations-calendar.component.css']
})
export class CurrentCollaborationsCalendarComponent implements OnInit {

  @Select(CollaborationsState.getCollaborationDays) collaborationDays$: Observable<number[]>;

  today = new Date();
  currentDate = this.today;
  calendarDays: CalendarDay[] = [];

  constructor(protected store: Store, private dateService: DateService) { }

  ngOnInit() {
    this.setCalendarDays(this.currentDate);
  }

  setCalendarDays(date: Date) {
    this.store.dispatch(new GetCollaborationDays(date));
    this.store.dispatch(new GetCollaborationSessions(date));
    this.calendarDays = this.getMonthDays(date);
  }

  getCollaborations(date: Date) {
    this.store.dispatch(new GetCollaborationSessions(date));
    this.currentDate = date;
  }

  previous() {
    this.currentDate = this.dateService.getPreviousMonth(this.currentDate);
    this.setCalendarDays(this.currentDate);
  }

  next() {
    this.currentDate = this.dateService.getNextMonth(this.currentDate);
    this.setCalendarDays(this.currentDate);
  }

  private getMonthDays = (dateParam: Date) => {
    const year = dateParam.getFullYear();
    const month = dateParam.getMonth();
    const monthIndex = month;
    const date = new Date(year, monthIndex, 1);
    let result: CalendarDay[] = [];
    while (date.getMonth() == monthIndex) {
      const calendarDay: CalendarDay = { day: date.getDate(), date: new Date(date) };
      result = [...result, calendarDay];
      date.setDate(date.getDate() + 1);
    }
    return result;
  }
}

export class CalendarDay {
  day: number;
  date: Date;
}
