import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CollaborationsComponent } from './collaborations.component';
import { CollaborationViewerComponent } from './collaboration-viewer/collaboration-viewer.component';
import { AuthGuard } from '../core/guards/auth-guard.service';
import { CurrentCollaborationsComponent } from './current-collaborations/current-collaborations.component';
import { CollaborationSetupComponent } from './collaboration-setup/collaboration-setup.component';
import { CollaborationSetupGuard } from './collaboration-setup/collaboration-setup.guard';

const collaborationsRoutes: Routes = [
  {
    path: '',
    component: CollaborationsComponent,
    children: [
      { path: '', redirectTo: 'current', pathMatch: 'full' },
      { path: 'current', component: CurrentCollaborationsComponent, canActivate: [AuthGuard] },
    ],
  },
  {
    path: ':id/setup',
    component: CollaborationSetupComponent,
    canActivate: [AuthGuard],
    canDeactivate: [CollaborationSetupGuard]
  },
  {
    path: ':id/join',
    component: CollaborationViewerComponent,
    canActivate: [AuthGuard]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(collaborationsRoutes)
  ],
  exports: [RouterModule]
})
export class CollaborationsRoutingModule { }
