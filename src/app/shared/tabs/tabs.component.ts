import { Component, OnInit, Input, Output, EventEmitter, OnDestroy } from '@angular/core';
import { Tab } from 'src/app/core/models/tab';
import { Router, NavigationEnd } from '@angular/router';
import { filter } from 'rxjs/operators';
import { SubSink } from 'subsink/dist/subsink';

@Component({
  selector: 'app-tabs',
  templateUrl: './tabs.component.html',
  styleUrls: ['./tabs.component.css']
})
export class TabsComponent implements OnInit, OnDestroy {

  private subs = new SubSink();
  currentRoute: string;
  @Input() tabs: Tab[] = [];
  @Input() fontSize: number;
  @Input() applyOverflow: boolean;
  @Output() navigate = new EventEmitter<string>();
  @Output() routeChange = new EventEmitter<any>();

  constructor(private router: Router) { }

  ngOnInit() {
    this.setActiveTab(this.router.url);
    this.subs.sink = this.router.events
      .pipe(
        filter(event => event instanceof NavigationEnd))
      .subscribe((event: NavigationEnd) => {
        this.setActiveTab(event.url);
        this.routeChange.emit(event.url);
      });
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

  performNavigation(link: string) {
    this.setActiveTab(link);
    this.navigate.emit(link);
  }

  setActiveTab(link: string) {
    if (typeof link !== 'string') return;

    let url = link.split('?')[0]; //get the current route without the query params
    let tab = this.tabs.find(x => x.link === url);
    if (tab) {
      this.tabs.map(x => x.isActive = false);
      tab.isActive = true;
    }
  }

  checkTabIsInView() {
    const parent = document.querySelector('.tab.nav');
    const parentProp = parent.getBoundingClientRect();
    const activeTab = this.tabs.find(t => t.isActive);
    const index = this.tabs.indexOf(activeTab)
    const child = document.getElementById(index.toString());
    if (!child) return;
    const childProp = child.getBoundingClientRect();
    this.scrollActiveTabIntoView(index)
  }

  scrollActiveTabIntoView(id: any) {
    let li = document.getElementById(id);
    if (!li) return;
    let ul = li.parentNode as HTMLElement;
    let fudge = ul.offsetWidth / 4;
    if (li.offsetWidth > ul.offsetWidth) {
      fudge = 0;
    } else if ((ul.offsetWidth - li.offsetWidth) < fudge) {
      fudge = ul.offsetWidth - li.offsetWidth;
    }
    ul.scrollLeft = li.offsetLeft - ul.offsetLeft - fudge;
  }
}
