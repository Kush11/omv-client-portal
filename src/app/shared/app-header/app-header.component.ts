import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { Select, Store } from '@ngxs/store';
import { AppState } from '../../state/app.state';
import { AuthService } from 'src/app/core/services/business/auth.service';
import { LogOut } from 'src/app/state/app.actions';
import { User } from 'src/app/core/models/entity/user';
import { SubSink } from 'subsink/dist/subsink';
import { Observable } from 'rxjs/internal/Observable';
import { WorkPlanningParentItem } from 'src/app/core/models/work-planning';
import { GetWorkPlanningParentItems } from 'src/app/work-planning/state/work-planning.actions';
import { filter } from 'rxjs/internal/operators/filter';
import { BaseComponent } from '../base/base.component';

@Component({
  selector: 'app-header',
  templateUrl: './app-header.component.html',
  styleUrls: ['./app-header.component.css']
})
export class AppHeaderComponent extends BaseComponent implements OnInit, OnDestroy {
  private subs = new SubSink();
  public displayWidth: number;
  public activated: boolean;
  isAuthenticated: boolean;
  showWorkPlanningSubmenu: boolean;
  workPlanningLinks: any[] = [];

  @Select(AppState.getCompanyLogo) companyLogo$: Observable<string>;
  @Select(AppState.getLoggedInUser) currentUser$: Observable<User>;
  @Select(AppState.getFullScreenMode) fullScreenMode$: Observable<boolean>;
  @Select(AppState.getWorkPlanningParentItems) parentItems$: Observable<WorkPlanningParentItem[]>;
  @Select(AppState.getMediaReadSASToken) sasToken$: Observable<string>;

  parentItems: WorkPlanningParentItem[];
  unauthorizedUser = 'Default';
  isWorkPlanningActive: boolean;

  @Input() hideMenu: boolean;
  fullScreenMode: boolean;

  constructor(protected store: Store, private router: Router, public auth: AuthService, ) {
    super(store);
  }

  async ngOnInit() {
    this.subs.add(
      this.parentItems$
        .subscribe(items => {
          this.parentItems = items;
        }),
      this.router.events
        .pipe(
          filter(event => event instanceof NavigationEnd))
        .subscribe((event: NavigationEnd) => {
          // #TODO get the parent route and set active work planning
          const workPlanningModuleLink = '/work-planning';
          const collaborationViewerLink = '/join';
          this.isWorkPlanningActive = event.url.includes(workPlanningModuleLink);
          if (!event.url.includes(collaborationViewerLink)) {
            if (this.fullScreenMode)
              this.disableFullScreen();
          }
        }),
      this.fullScreenMode$
        .subscribe(show => this.fullScreenMode = show)
    );
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

  logout() {
    this.saveReturnUrl(this.router.url);
    this.store.dispatch(new LogOut());
  }

  onWorkPlanningClicked() {
    this.store.dispatch(new GetWorkPlanningParentItems());
  }

  private saveReturnUrl(route) {
    const ignored_routes = ['/startup', '/dashboard', '/authorize-check', '/implicit/callback', '/login'];
    if (ignored_routes.includes(route) || route.includes('/implicit/callback')) { return; }
    const key = 'omv-client-return-url';
    if (localStorage.getItem(key)) { localStorage.removeItem(key); }
    localStorage.setItem(key, route);
  }

  navigateToLink(name) {
    const _route = encodeURIComponent(name);
    this.router.navigate([`/work-planning/${_route}`]);
  }
}
