import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { Button } from 'src/app/core/models/button';

@Component({
  selector: 'app-buttons',
  templateUrl: './buttons.component.html',
  styleUrls: ['./buttons.component.css']
})
export class ButtonsComponent implements OnInit {

  @Input() buttons: Button[] = [];
  @Input() position = 'left';
  @Input() spacing = 20;
  @Input() bottom: number;
  @Input() disabled: boolean;

  @Output() actions = new EventEmitter<any>();

  constructor() { }

  ngOnInit() {
    if (this.buttons) {
      this.buttons.forEach(button => {
        if (!button.action) button.action = this.clickEvent.bind(this);
      });
    }
  }

  clickEvent(event: number) {
    this.actions.emit(event);
  }
}
