import { Component, OnInit, ViewChild, Input, Output, EventEmitter } from '@angular/core';
import { DialogComponent } from '@syncfusion/ej2-angular-popups';
import { Button } from 'src/app/core/models/button';

@Component({
  selector: 'app-modal-confirm',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.css']
})
export class ModalConfirmComponent implements OnInit {

  @ViewChild('modal') modal: DialogComponent;

  modalPosition: Object = { X: 'center', Y: 'center' };
  confirmButtons: Button[] = [
    { text: "YES", type: 'button', cssClass: "button-round modal-yes-button-qa" },
    { text: "No", type: 'button', cssClass: "button-no-outline modal-no-button-qa" }
  ];

  @Input() title: string;
  @Input() message = 'Are you sure you want to delete this item?';
  @Input() width = 400;
  @Input() type = "confirm";

  @Output() close = new EventEmitter();
  @Output() yes = new EventEmitter<any>();
  @Output() no = new EventEmitter<any>();

  ngOnInit() { }

  show() {
    this.modal.show();
  }

  closeDialog() {
    this.close.emit();
    this.modal.hide();
  }

  confirmButtonActions(index: number) {
    this.modal.hide();
    if (index === 0) {
      this.yes.emit();
    } else {
      this.no.emit();
    }
  }
}
