import { Component, OnInit, ViewChild, Input, Output, EventEmitter, OnDestroy } from '@angular/core';
import { DialogComponent, createSpinner, showSpinner, hideSpinner } from '@syncfusion/ej2-angular-popups';
import { DynamicFormComponent } from '../dynamic-components/components/dynamic-form.component';
import { FieldConfiguration } from '../dynamic-components/field-setting';
import { Button } from 'src/app/core/models/button';
import { FormGroup, ValidatorFn, Validators } from '@angular/forms';
import { MetadataField } from 'src/app/core/models/entity/metadata-field';
import { ListView } from '@syncfusion/ej2-angular-lists';
import { User } from 'src/app/core/models/entity/user';
import { Store } from '@ngxs/store';
import { ShowWarningMessage, ShowErrorMessage } from 'src/app/state/app.actions';
import { UsersService } from 'src/app/core/services/business/users/users.service';
import { AppState } from 'src/app/state/app.state';
import { Subject } from 'rxjs/internal/Subject';
declare let WowzaPlayer: any;

const BROWSE = 'Browse';
const CHANGE = 'Change';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.css']
})
export class ModalComponent implements OnInit, OnDestroy {

  @ViewChild('modal') modal: DialogComponent;
  @ViewChild('listview') listview: ListView;
  @ViewChild(DynamicFormComponent) dynamicForm: DynamicFormComponent;

  modalPosition: Object = { X: 'center', Y: 100 };
  buttons: Button[] = [
    { text: "SAVE CHANGES", type: 'submit', cssClass: "button-round modal-save-changes-qa", disabled: false, action: this.formSubmit.bind(this) }
  ];
  confirmButtons: Button[] = [
    { text: "YES", type: 'button', cssClass: "button-round modal-yes-button-qa" },
    { text: "No", type: 'button', cssClass: "button-no-outline modal-no-button-qa" }
  ];

  @Input() title: string;
  @Input() message = 'Are you sure you want to delete this item?'; // for confirm modal
  @Input() dataSource = [];
  @Input() currentParticipants = [];
  @Input() isCurrentParticipantOnly: boolean = false;
  @Input() saveText = 'SAVE';
  @Input() width = 500;
  @Input() type: string;
  @Input() fields: Object; // for default listview
  @Input() selectedItems = []; // for listviews
  @Input() controls: FieldConfiguration[] = []; // for form modal
  details: ModalDetail[] = [];
  @Input() allowedFiles: string;
  @Input() videoSource: string; // livestream video
  @Input() participants: User[] = []; // collaboration

  @Output() rowSelected = new EventEmitter<any>();
  @Output() save = new EventEmitter<any>();
  @Output() submit = new EventEmitter(); // for form modal
  @Output() close = new EventEmitter();
  @Output() yes = new EventEmitter<any>(); // for confirm modal
  @Output() no = new EventEmitter<any>(); // for confirm modal
  @Output() add = new EventEmitter<any>(); // for participant modal
  @Output() remove = new EventEmitter<any>(); // for participant modal

  enableSortSave: boolean;
  sortData = [];

  response$ = new Subject<boolean>();

  //#region Upload

  @ViewChild('file') file: any;
  fileName: string;
  isFileTouched: boolean;
  selectedFile: File;
  fileSelectionButtonText = BROWSE;

  //#endregion

  users: User[] = [];
  userFilter = '';
  usersCurrentPage = 1;
  pageSize = 6;
  totalRecordsCount: number;
  participantsCurrentPage = 1;
  participantsPageSize = 6;
  canSaveParticipants: boolean;
  participantsTotalRecordsCount: number;
  paginatedParticipants: User[];
  mainplayer: any;

  constructor(protected store: Store, private usersService: UsersService) { }

  ngOnInit() {
    if (this.type === 'confirm') {
      this.modalPosition = { X: 'center', Y: 'center' };
      this.width = 400;
    } else if (this.type === 'upload') {
      this.buttons[0].text = "UPLOAD";
      this.modalPosition = { X: 'center', Y: 'center' };
    } else if (this.type === 'live-stream') {
      this.width = 900;
    }
    this.controls.forEach(c => {
      c.isModal = true;
      c.value = c.value || '';
      c.validations = this.setValidations(c);
    });
  }

  ngOnDestroy() {
    this.response$.unsubscribe();
    this.destroyWowzaPlayer();
  }

  show() {
    this.modal.show();
  }

  closeDialog() {
    if (this.type === 'multiselect-listview') {
      this.clearSelectedRows();
    } else if (this.type === 'details-view') {
      this.clearDetails();
    } else if (this.type === 'sort') {
      this.revertChanges();
    } else if (this.type === 'upload') {
      this.resetUpload();
    } else if (this.type === 'form') {
      this.dynamicForm.reset();
    } else if (this.type === 'collaboration-participants') {
      this.participants = this.participants.filter(p => p.isCurrent);
    }else if(this.type = 'live-stream'){
      this.destroyWowzaPlayer()
    }
    this.close.emit();
    this.modal.hide();
  }

  showSpinner() {
    createSpinner({
      target: document.getElementById('modalSpinner')
    });
    showSpinner(document.getElementById('modalSpinner'));
  }

  hideSpinner() {
    createSpinner({
      target: document.getElementById('modalSpinner')
    });
    hideSpinner(document.getElementById('modalSpinner'));
  }

  //#region ListViews

  clearSelection() {
    if (this.type === 'multiselect-listview') {
      this.dataSource.map(data => data.isSelected = data.isChecked = false);
      this.selectedItems = [];
      return;
    } else if (this.type === 'listview') {
      this.selectedItems = [];
      this.rowSelected.emit(this.selectedItems);
      this.listview.uncheckAllItems();
    }
  }

  onSave() {
    this.save.emit();
    this.modal.hide();
  }

  //#region Default Listview

  onRowSelect() {
    if (this.listview) {
      this.selectedItems = this.listview.getSelectedItems().data as any[];
      this.rowSelected.emit(this.selectedItems);
    }
  }

  //#endregion

  //#region MultiSelect Listview

  onRowSelected(data: MetadataField) {
    if (data.isChecked) return;
    // Step 1: check if selected field has a parent and select the parent first
    let parentField = this.dataSource.find(field => field.id === data.parentId);
    if (parentField) {
      // Step 2: if parent is already selected or in the form, do nothing; else prepare to add to the screen
      if (!parentField.isSelected && !parentField.isChecked) {
        parentField.isSelected = !parentField.isSelected;
        this.selectedItems.push(parentField);
      }
    }
    this.dataSource.forEach(field => {
      if (field.id === data.id) {
        field.isSelected = !field.isSelected;
        if (field.isSelected) {
          this.selectedItems.push(field);
        } else {
          this.selectedItems = this.selectedItems.filter(x => x.name !== field.name);
        }
      }
    });
  }

  addSelectedRows() {
    if (this.selectedItems.length === 0) return;
    this.modal.hide();
    this.save.emit(this.selectedItems);
    const selectedIds = this.selectedItems.map(x => x.id);
    this.dataSource.map(data => {
      if (selectedIds.includes(data.id)) {
        data.isChecked = true;
        data.isSelected = false;
      }
    });
    this.clearSelectedRows();
  }

  uncheckField(field: any) {
    this.dataSource.map(data => {
      if (data.id === field.id) data.isChecked = false;
    });
  }

  private clearSelectedRows() {
    this.dataSource.map(data => data.isSelected = false);
    this.selectedItems = [];
  }

  //#endregion

  //#region Sort Listview

  sortDown(selectedRow: any) {
    this.performSort(selectedRow, 'down');
  }

  sortUp(selectedRow: any) {
    this.performSort(selectedRow, 'up');
  }

  performSort(selectedRow: any, direction: string) {
    this.dataSource.forEach((data, index) => {
      let previous = this.dataSource[index - 1];
      let next = this.dataSource[index + 1];
      if (data.id === selectedRow.id) {
        if (direction === 'up') {
          if (!previous) return; //If there is no previous, don't decrease order
          data.order--;
          previous.order++;
        } else {
          if (!next) return; // If there is no next, don't increase order
          data.order++;
          next.order--;
        }
      }
    });
    this.dataSource.sort((a, b) => a.order - b.order);
    this.listview.refresh();
    this.enableSortSave = true;
    // this.checkSortOrder();
  }

  onSaveSort() {
    this.save.emit(this.dataSource);
    this.enableSortSave = false;
    this.modal.hide();
  }


  private revertChanges() {
    this.dataSource = [];
    this.enableSortSave = false;
  }

  //#endregion

  //#endregion

  //#region Confirm Section

  confirmButtonActions(index: number) {
    this.modal.hide();
    if (index === 0) {
      this.response$.next(true);
      this.response$.complete();
      this.yes.emit();
    } else {
      this.response$.next(false);
      this.response$.complete();
      this.no.emit();
    }
  }

  //#endregion

  //#region Form Section

  formSubmit() {
    this.submit.emit(this.dynamicForm);
    this.modal.hide();
  }

  setValue(name: string, value: any, disable?: boolean) {
    this.dynamicForm.setValue(name, value, disable);
  }

  reset(form?: FormGroup) {
    if (this.dynamicForm) {
      this.dynamicForm.reset(form);
    }
  }

  enableAll() {
    if (this.dynamicForm) {
      this.dynamicForm.enableAll();
    }
  }

  private setValidations(config: FieldConfiguration): ValidatorFn[] {
    let validations: ValidatorFn[] = [];
    if (config.required) {
      let requiredValidation: any = {
        name: 'required',
        validator: Validators.required,
        message: config.validationMessage
      };
      validations.push(requiredValidation);
    }
    return validations;
  }

  //#endregion

  //#region Details View

  addDetail(name: string, value: any) {
    const detail: ModalDetail = { name: name, value: value };
    this.details.push(detail);
  }

  clearDetails() {
    this.details = [];
  }

  //#endregion

  //#region Upload

  chooseFile() {
    this.file.nativeElement.click();
  }

  images = ["jpeg", "jpg", "bmp", "gif", "svg", "png"];
  docFormats = ['doc', 'docx', 'xls', 'xlsx', 'ppt', 'csv', 'log', 'pdf'];
  onFileChosen() {
    this.isFileTouched = true;
    const files: { [key: string]: File } = this.file.nativeElement.files;
    this.selectedFile = files[0];
    if (this.selectedFile) {
      const index = this.selectedFile.name.lastIndexOf(".") + 1;
      const extension = this.selectedFile.name.substr(index, this.selectedFile.name.length).toLowerCase();
      if (!this.images.includes(extension) && !this.docFormats.includes(extension)) {
        this.selectedFile = null;
        this.store.dispatch(new ShowWarningMessage("File type not allowed. Please upload an image or document."));
        return;
      }
      this.fileSelectionButtonText = CHANGE;
    }
  }

  onUpload() {
    this.save.emit(this.selectedFile);
    this.closeDialog();
  }

  private resetUpload() {
    this.selectedFile = null;
    this.fileSelectionButtonText = BROWSE;
  }

  removeParticipant(data: any) {
    this.remove.emit(data);
  }

  addParticipant(data: any) {
    this.add.emit(data);
  }

  //#endregion

  //#region LiveStream Video

  setupWowzaPlayer() {
    this.destroyWowzaPlayer();
    if (this.videoSource) {
      this.mainplayer = WowzaPlayer.create('videoPlayer', {
        "license": "PLAY1-3zveE-8mZFG-HUjva-W8rdG-Q3BvW",
        "sources": [{
          "sourceURL":this.videoSource
        }],
        "title": '',
        "description": '',
        "autoPlay": true,
        "mute": false,
        "volume": 100
      });
    } else {
      this.mainplayer = WowzaPlayer.create('videoPlayer', {
        "license": "PLAY1-3zveE-8mZFG-HUjva-W8rdG-Q3BvW",
        "title": '',
        "description": '',
        "autoPlay": false,
        "mute": false,
        "volume": 100
      });
    }
  }

  //#endregion

  //#region Collaboration Participants

  getUsers(name?: string) {
    this.showSpinner();
    this.usersService.getActiveUsers(name, null, this.usersCurrentPage, this.pageSize).toPromise()
      .then(response => {
        const currentUser = this.store.selectSnapshot(AppState.getLoggedInUser);
        let users = response.data;
        users.forEach(u => u.isCurrent = u.id === currentUser.id);
        this.users = users;
        this.totalRecordsCount = response.total;
        this.hideSpinner();
      }, err => {
        this.hideSpinner();
        this.store.dispatch(new ShowErrorMessage(err))
      });
  }

  onUsersPageChanged(pageNumber: number) {
    this.usersCurrentPage = pageNumber;
    this.getUsers(this.userFilter);
  }

  onUserFilterChanged(value: string) {
    this.userFilter = value;
  }

  searchUser(value?: string) {
    this.userFilter = value || this.userFilter;
    this.usersCurrentPage = 1;
    this.getUsers(this.userFilter);
  }
  destroyWowzaPlayer(): void {
    if (this.mainplayer)
      this.mainplayer.destroy();
  }

  onParticipantRemoved(participant: User) {
    this.participants = this.participants.filter(p => p.displayName !== participant.displayName);
    this.users.forEach(user => {
      if (user.displayName === participant.displayName) {
        user.isSelected = false;
        user.isParticipant = false;
      }
    });
    this.canSaveParticipants = true;
    this.setPaginatedParticipants();
  }

  onParticipantsPageChanged(pageNumber: number) {
    this.participantsCurrentPage = pageNumber;

    this.setPaginatedParticipants();
  }

  onUserAdded(user: User) {
    if (this.participants.map(p => p.displayName).includes(user.displayName)) {
      this.store.dispatch(new ShowWarningMessage(`${user.displayName} has already been added.`));
      return;
    }
    const participant = { ...user };
    participant.isParticipant = true;
    this.participants = [
      ...this.participants,
      participant
    ];
    this.canSaveParticipants = true;

    this.setPaginatedParticipants();
  }

  onSaveParticipants() {
    this.save.emit(this.participants);
    this.modal.hide();
  }

  setPaginatedParticipants(participants?: User[]) {
    const _participants = this.participants.length > 0 ? this.participants : participants;
    this.participantsTotalRecordsCount = _participants.length;
    const start = (this.participantsCurrentPage - 1) * (this.participantsPageSize);
    const end = start + (this.participantsPageSize);
    this.paginatedParticipants = _participants.slice(start, end);
    if (this.paginatedParticipants.length === 0) {
      if (this.participantsCurrentPage > 1) {
        this.participantsCurrentPage--;
        const start = (this.participantsCurrentPage - 1) * (this.participantsPageSize);
        const end = start + (this.participantsPageSize);
        this.paginatedParticipants = _participants.slice(start, end);
      }
    }
  }

  //#endregion
}


export class ModalDetail {
  name: string;
  value: any;
}
