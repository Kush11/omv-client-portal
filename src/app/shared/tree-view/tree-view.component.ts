import { Component, OnInit, Input, EventEmitter, Output, ViewChild } from '@angular/core';
import { DataBoundEventArgs, NodeSelectEventArgs, NodeExpandEventArgs } from '@syncfusion/ej2-navigations';
import { TreeViewComponent as SyncfusionTreeView } from '@syncfusion/ej2-angular-navigations';

@Component({
  selector: 'app-tree-view',
  templateUrl: './tree-view.component.html',
  styleUrls: ['./tree-view.component.css']
})
export class TreeViewComponent implements OnInit {

  @ViewChild('treeview') treeview: SyncfusionTreeView;

  treeCheckedNodes: string[] = [];

  @Input() checkedNodes: string[] = [];
  @Input() fields: any;

  @Output() rowSelected = new EventEmitter<any>();
  @Output() nodeExpanding = new EventEmitter<any>();
  @Output() nodeSelected = new EventEmitter<any>();

  currentTarget: HTMLLIElement;
  parentID: string;

  constructor() { }

  ngOnInit() {
  }

  dataBound(evt: DataBoundEventArgs) {
  }

  nodeCheckedEvent(args: any): void {
    this.rowSelected.emit(this.treeview.getAllCheckedNodes());
  }

  onNodeSelected(args: NodeSelectEventArgs) {
    this.nodeSelected.emit(args.nodeData);
  }

  onNodeExpanding(args: NodeExpandEventArgs) {
    if ((args.node.querySelectorAll('.e-icons.e-icon-expandable').length > 0) && args.node.querySelectorAll('ul li').length === 0) {
      this.currentTarget = args.node;
      this.parentID = args.node.getAttribute('data-uid');
      this.nodeExpanding.emit();
    }
  }

  addNodes(nodes: any[]) {
    this.treeview.addNodes(
      nodes,
      this.currentTarget
    );
  }
}
