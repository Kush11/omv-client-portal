import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Breadcrumb } from 'src/app/core/models/breadcrumb';
import { ChangedEventArgs } from '@syncfusion/ej2-angular-calendars';

@Component({
  selector: 'app-breadcrumb',
  templateUrl: './breadcrumb.component.html',
  styleUrls: ['./breadcrumb.component.css'],
})
export class BreadcrumbComponent implements OnInit {

  @Input() enableBackRouting: boolean;
  @Input() previousRoute: string;

  @Input() breadCrumbs: Breadcrumb[] = [];
  @Input() hideStroke: boolean;

  @Input() showDateFilter: boolean;
  @Output() dateChange = new EventEmitter<Date>();

  @Output() navigate = new EventEmitter<string>();

  dateValue: Date = new Date();

  constructor() { }

  ngOnInit() { }

  performNavigation(breadcrumb: Breadcrumb) {
    const last = this.breadCrumbs[this.breadCrumbs.length - 1];
    if (breadcrumb.name !== last.name) this.navigate.emit(breadcrumb.link);
  }

  navigateBack(route: string) {
    this.navigate.emit(route);
  }

  onDateFilterChanged(args: ChangedEventArgs) {
    const { value } = args;
    this.dateValue = value || new Date();
    this.dateChange.emit(this.dateValue);
  }
} 