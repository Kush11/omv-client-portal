import { Component, OnInit, Input, AfterViewInit } from '@angular/core';
import { VideoSchedule } from 'src/app/core/models/entity/live-stream';
declare let videojs: any;


@Component({
  selector: 'app-live-stream-list',
  templateUrl: './live-stream-list.component.html',
  styleUrls: ['./live-stream-list.component.css']
})
export class LiveStreamListComponent implements OnInit, AfterViewInit {

  @Input() data: any[] = [];
  @Input() type: any;

  seekModebackImg = "/assets/images/syn-backwards-active.svg";
  seekModeforwardImg = "/assets/images/syn-forward-active.svg";
  seekModePlayImg = "/assets/images/streaming-archive-play.svg";

  dailyTime: any[] = [
    { hour: '11:00', meridiem: "am" },
    { hour: '11:05', meridiem: "am" },
    { hour: '11:10', meridiem: "am" },
    { hour: '11:15', meridiem: "am" },
    { hour: '11:20', meridiem: "am" },
    { hour: '11:25', meridiem: "am" },
    { hour: '11:30', meridiem: "am" },
    { hour: '11:35', meridiem: "am" },
    { hour: '11:40', meridiem: "am" },
    { hour: '11:45', meridiem: "am" },
    { hour: '11:50', meridiem: "am" },
    { hour: '11:55', meridiem: "am" },
    { hour: '12:00', meridiem: "pm" },
  ];

  constructor() { }


  ngOnInit() {
    console.log('LiveStreamListComponent-ngOnInit', this.data)
  }
  ngAfterViewInit() {

  }

}
