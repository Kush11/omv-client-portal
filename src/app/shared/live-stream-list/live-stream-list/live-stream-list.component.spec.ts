import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LiveStreamListComponent } from './live-stream-list.component';

describe('LiveStreamListComponent', () => {
  let component: LiveStreamListComponent;
  let fixture: ComponentFixture<LiveStreamListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LiveStreamListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LiveStreamListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
