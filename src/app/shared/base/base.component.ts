import { OnInit, OnDestroy } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs/internal/Observable';
import {
  SetPageTitle,
  ShowConfirmationBox,
  ShowLeftNav,
  ShowSpinner,
  HideSpinner,
  ShowErrorMessage,
  ShowSuccessMessage,
  ShowWarningMessage,
  ShowFullScreen,
  HideFullScreen,
  ShowInfoMessage,
  CloseToast
} from 'src/app/state/app.actions';
import { AppState } from 'src/app/state/app.state';
import { Permission, permissions, menuNames } from 'src/app/core/enum/permission';
import { createSpinner, showSpinner, hideSpinner } from '@syncfusion/ej2-popups/src/spinner/spinner';
import { AuthService } from 'src/app/core/services/business/auth.service';
import { SubSink } from 'subsink/dist/subsink';
import { User, CustomerMenuPermission_GetByCustomerId, User_Permissions } from 'src/app/core/models/entity/user';
import { TopLevelMenuPermission as TopLevelMenuPermission, UserPermission as UserPermission } from 'src/app/core/dtos/output/users/User_GetByIdOutputDTO';
import { ApiError } from 'src/app/core/models/api-error';
import { RoleType } from 'src/app/core/enum/role-type';
import { ToastIcon } from 'src/app/core/enum/toast';


export class BaseComponent implements OnInit, OnDestroy {

  public subsArray = new SubSink();
  displayWidth: number;
  // _permission: string;
  userHasPermission: boolean;
  currentUserId: number;
  permission: string;

  public currentuser: User;
  public permissions: any;
  public roles: string[];
  public tenantPermission = new TopLevelMenuPermission();
  public userPermission = new UserPermission();

  @Select(AppState.confirmation) confirmation$: Observable<string>;
  @Select(AppState.getUserPermissions) userPermissions$: Observable<Permission[]>;
  @Select(AppState.setDeviceWidth) deviceWidth$: Observable<number>;
  @Select(AppState.getLoggedInUser) currentUser$: Observable<User>;

  constructor(protected store: Store, protected auth?: AuthService) {
    this.subsArray.add(
      this.deviceWidth$
        .subscribe(width => {
          this.displayWidth = width;
        }),
      this.currentUser$
        .subscribe(user => {
          this.currentuser = user;
          if (user) {
            this.permissions = this.currentuser.permissions;
            this.roles = this.currentuser.roleNames;

            if (this.permissions) {
              this.permissions.menus.forEach(menu => {
                this.getTopLevelPermissions(menu);
              });
            }

            if (this.roles) {
              this.roles.forEach(role => {
                if (role == RoleType.ClientAdmin) {
                  this.userPermission.isClientAdmin = true;
                } else if (role == RoleType.OiiAdmin) {
                  this.userPermission.isOIIAdmin = true;
                }
              });
            }

            if (this.permissions) {
              if (typeof (this.permissions.userPermissions) != null) {
                this.permissions.userPermissions.forEach((perm: any) => {
                  this.getUserPermissions(perm);
                  if (typeof (this.userPermission.canEditMetadata) == 'undefined') {
                    this.userPermission.canEditMetadata = false;
                  }
                });
              }
            }
          }
        })
    );
  }

  ngOnInit() {
    console.log('BaseComponent - ngOnInit');
  }

  getTopLevelPermissions(menu: CustomerMenuPermission_GetByCustomerId) {
    switch (menu.menuName) {
      case menuNames.DASHBOARD:
        this.tenantPermission.isPermittedDashboard = menu.isPermitted;
        break;
      case menuNames.MEDIA:
        this.tenantPermission.isPermittedMedia = menu.isPermitted;
        break;
      case menuNames.LIVE_STREAM:
        this.tenantPermission.isPermittedLiveStream = menu.isPermitted;
        break;
      case menuNames.WORK_PLANNING:
        this.tenantPermission.isPermitedWorkPlanning = menu.isPermitted;
        break;
      case menuNames.COLLABORATIONS:
        this.tenantPermission.isPermittedCollaborations = menu.isPermitted;
        break;
    }
  }

  getUserPermissions(perm: any) {
    switch (perm) {
      case permissions.MEDIA_UPLOAD:
        this.userPermission.canUploadMedia = true;
        break;
      case permissions.BULK_MEDIA_UPLOAD:
        this.userPermission.canUploadBulkMedia = true;
        break;
      case permissions.BULK_METADATA_EDIT:
        this.userPermission.canEditBulkMetadata = true;
        break;
      case permissions.METADATA_EDIT:
        this.userPermission.canEditMetadata = true;
        break;
      case permissions.COLLABORATION_EDIT:
        this.userPermission.canEditCollaboration = true;
        break;
      case permissions.CAN_VIEW_ADMIN_PORTAL:
        this.userPermission.canViewAdminPortal = true;
        break;
    }
  }

  ngOnDestroy() {
    this.subsArray.unsubscribe();
  }

  showSpinner() {
    this.store.dispatch(new ShowSpinner());
  }

  hideSpinner() {
    this.store.dispatch(new HideSpinner());
  }

  protected showLefNav() {
    this.store.dispatch(new ShowLeftNav(true));
  }

  protected hideLeftNav() {
    this.store.dispatch(new ShowLeftNav(false));
  }

  enableFullScreen() {
    this.store.dispatch(new ShowFullScreen());
  }

  disableFullScreen() {
    this.store.dispatch(new HideFullScreen());
  }

  protected setPageTitle(pageTitle: string) {
    this.store.dispatch(new SetPageTitle(pageTitle));
  }

  protected showSuccessMessage(message: string) {
    this.store.dispatch(new ShowSuccessMessage(message));
  }

  protected showInfoMessage(message: string, timeOut = 5000, icon?: ToastIcon) {
    this.store.dispatch(new ShowInfoMessage(message, timeOut, icon));
  }

  closeToast() {
    this.store.dispatch(new CloseToast());
  }

  showWarningMessage(message: string, title = 'Warning!', timeOut = 5000) {
    this.store.dispatch(new ShowWarningMessage(message, title, timeOut));
  }

  protected showErrorMessage(message: string) {
    this.store.dispatch(new ShowErrorMessage(message));
  }

  protected confirm(show: boolean) {
    this.store.dispatch(new ShowConfirmationBox(show));
  }

  protected isConfirmed() {
    let confirmed: boolean;
    this.confirmation$.subscribe((res) => {
      res === 'true' ? confirmed = true : confirmed = false;
    });

    return confirmed;
  }
}
