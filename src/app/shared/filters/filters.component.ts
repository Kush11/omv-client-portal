import { Component, OnInit, ViewChild, Input, EventEmitter, Output, OnChanges } from '@angular/core';
import { DialogComponent } from '@syncfusion/ej2-angular-popups';
import { DynamicFormComponent } from '../dynamic-components/components/dynamic-form.component';
import { Tag } from 'src/app/core/models/entity/tag';
import { FieldConfiguration } from '../dynamic-components/field-setting';

@Component({
  selector: 'app-filters',
  templateUrl: './filters.component.html',
  styleUrls: ['./filters.component.css']
})
export class FiltersComponent implements OnInit, OnChanges {

  @ViewChild(DynamicFormComponent) dynamicForm: DynamicFormComponent;
  @ViewChild('searchInput') searchInput: HTMLInputElement;

  @Input() fields: FieldConfiguration[] = [];
  @Input() tags: Tag[] = [];
  @Input() showMenu: boolean;
  @Input() canSaveFavorites: boolean;
  @Input() hideToggler: boolean;
  @Input() mode = 'search';

  @Output() search = new EventEmitter<string>();
  @Output() clear = new EventEmitter<string>();
  @Output() addTag = new EventEmitter<Tag>();
  @Output() removeTag = new EventEmitter<Tag>();
  @Output() applyFilters = new EventEmitter<Tag[]>();
  @Output() clearFilters = new EventEmitter();
  @Output() toggleMenu = new EventEmitter();
  @Output() saveFavorite = new EventEmitter<any>();

  filterQuery = '';

  constructor() { }

  ngOnInit() {
  }

  ngOnChanges() {
    this.markSelectedFields();
  }

  onSearchInputChanged(value: string) {
    this.filterQuery = value;
  }

  onSearch(query: string) {
    if (query) {
      this.search.emit(query);
    }
  }

  onClear() {
    // this.clear.emit();
  }

  markSelectedFields() {
    if (!this.tags || !this.fields) return;
    if (this.tags.length === 0 || this.fields.length === 0) return;
    // Check every tag and mark as selected in dropdowns
    // this.reset();
    this.tags.forEach(tag => {
      // Step 1: Check if field is in the tags
      const field = this.fields.find(f => f.name === tag.name);
      if (field) {
        // Step 2: Mark it's value as selected if in tag and deselect those not in tags
        if (field.options) {
          field.options.forEach(option => {
            if (option.description === tag.value) option.isSelected = true;
          });
        }
      }
    });
  }

  onRemoveTag(tag: Tag) {
    this.removeTag.emit(tag);

    this.fields.forEach(field => {
      if (field.name === tag.name) {
        if (field.options) {
          field.options.forEach(option => {
            if (option.description === tag.value) {
              option.isSelected = false;
              field.value = '';
            }
          });
        }
      }
    });
  }

  onClearFilters() {
    this.clearFilters.emit();
  }

  onApplyFilters() {
    if (this.tags.length > 0) {
      this.applyFilters.emit(this.tags);
    }
  }

  onToggle() {
    this.toggleMenu.emit();
  }

  showSaveToFavoriteDialog() {
    // this.dialog.show();
  }

  saveFavoriteSearch(name: any) {
    if (!name) return;
    this.saveFavorite.emit(name);
    this.closeDialog();
  }

  closeDialog() {
    // this.dialog.hide();
  }

  reset() {
    this.fields.forEach(field => {
      if (field.options) field.options.forEach(option => option.isSelected = false);
      field.value = '';
    });
  }

  markSelectedTags() {
    this.fields.forEach(field => {
      if (field.options) {
        this.tags.forEach(tag => {
          if (field.label === tag.name) {
            field.options.forEach(option => {
              if (option.description === tag.value) option.isSelected = true;
            });
          }
        });
      }
    });
  }

  onAddTag(tag: Tag) {
    const existingTag = this.tags.find(x => x.name === tag.name && x.value === tag.value);
    if (existingTag) return;
    // Is Metadata Column Login
    tag.isMetaColumn = tag.name !== 'FileContent';
    this.addTag.emit(tag);
  }
}