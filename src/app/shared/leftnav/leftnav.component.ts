import { Component, OnInit, Input, AfterViewInit, OnDestroy } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { Select, Store } from '@ngxs/store';
import { AppState } from '../../state/app.state';
import { Observable } from 'rxjs';
import { SubSink } from 'subsink/dist/subsink';
import { filter } from 'rxjs/internal/operators/filter';
import { BaseComponent } from '../base/base.component';

@Component({
  selector: 'app-leftnav',
  templateUrl: './leftnav.component.html',
  styleUrls: ['./leftnav.component.css']
})
export class LeftNavComponent extends BaseComponent implements OnInit, OnDestroy, AfterViewInit {

  @Select(AppState.setDeviceWidth) deviceWidth$: Observable<number>;

  private subs = new SubSink();
  displayWidth: number;
  isAdminNavActive: boolean;

  //#region Admin Dashboard

  isDashboardActive: boolean;
  dashboardLink = '/admin/dashboard';

  //#endregion

  //#region Media

  isMediaMenuOpen: boolean;

  isMediaUploadsActive: boolean;
  isProcessingRulesActive: boolean;
  isBulkUploaderActive: boolean;
  isMediaFolderStructureActive: boolean;

  newMediaUploadsLink = '/admin/media/uploads/new';
  historyMediaUploadsLink = '/admin/media/uploads/history';
  processingRulesLink = '/admin/media/rules';
  bulkploaderLink = '/admin/media/bulk-uploader';
  folderStructureLink = '/admin/media/folders';
  inProgressMediaUploadsLink = '/admin/media/uploads/in-progress';

  //#endregion

  //#region Metadata

  isMetadataMenuOpen: boolean;
  isMetadataFieldsActive: boolean;
  isMetaDataListActive: boolean;

  metadataFieldsLink = '/admin/metadata-fields/media';
  workPlanningLink = '/admin/metadata-fields/workplanning';
  activeMetadataListLink = '/admin/metadata-list/active';
  disabledMetadataListLink = '/admin/metadata-list/disabled';

  //#endregion

  //#region KPIs

  isKPIsActive: boolean;

  //#endregion

  //#region Filters

  isFiltersActive: boolean;

  //#endregion

  //#region Work Planning

  isWorkPlanningMenuOpen: boolean;
  isWorkTemplateActive: boolean;
  isWorkSetActive: boolean;
  activeWorkTemplatesLink = 'admin/work-templates';
  workTemplatesLink = 'admin/work-templates';
  activeWorkSetsLink = 'admin/work-sets';
  workSetsLink = 'admin/work-sets';

  //#endregion

  //#region Users & Groups

  isUsersAndGroupsMenuOpen: boolean;
  isUsersActive: boolean;
  isGroupsActive: boolean;
  activeUsersLink = '/admin/users/active';
  unassignedUsersLink = '/admin/users/unassigned';
  disabledUsersLink = '/admin/users/disabled';
  activeGroupsLink = '/admin/groups/active';
  disabledGroupsLink = '/admin/groups/disabled';

  //#endregion

  //#region Email Templates

  isEmailActive: boolean;
  emailTemplatesLink = '/admin/email-templates';

  //#endregion

  constructor(protected store: Store, protected router: Router) {
    super(store);
  }

  ngOnInit() {
    this.isAdminNavActive = false;
    if (this.router.url !== '/') {
      this.setActiveTab(this.router.url);
      if (this.router.url.includes(this.workTemplatesLink)) this.processWorkPlanningLinks(this.workTemplatesLink);
      else if (this.router.url.includes(this.workSetsLink)) this.processWorkPlanningLinks(this.workSetsLink);
    }
    this.subs.add(
      this.router.events
        .pipe(
          filter(event => event instanceof NavigationEnd))
        .subscribe((event: NavigationEnd) => {
          if (event.url.includes('admin')) {
            this.setActiveTab(event.url);
            if (event.url.includes(this.workTemplatesLink)) this.processWorkPlanningLinks(this.workTemplatesLink);
            else if (event.url.includes(this.workSetsLink)) this.processWorkPlanningLinks(this.workSetsLink);
          }
        }),
      this.deviceWidth$
        .subscribe(width => this.displayWidth = width)
    );
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

  ngAfterViewInit() {

  }

  mediaMenuClicked() {
    this.isMediaMenuOpen = !this.isMediaMenuOpen;
  }

  metadataMenuClicked() {
    this.isMetadataMenuOpen = !this.isMetadataMenuOpen;
  }

  workPlanningMenuClicked() {
    this.isWorkPlanningMenuOpen = !this.isWorkPlanningMenuOpen;
  }

  usersMenuClicked() {
    this.isUsersAndGroupsMenuOpen = !this.isUsersAndGroupsMenuOpen;
  }

  setActiveTab(url: string) {
    this.clearActiveSelections();
    this.closeAllMenus();
    this.router.navigate([url]);
    this.showAdminNav();
    switch (url) {
      case this.dashboardLink:
        this.isDashboardActive = true;
        break;
      case this.newMediaUploadsLink:
      case this.historyMediaUploadsLink:
        this.isMediaMenuOpen = this.isMediaUploadsActive = true;
        break;
      case this.processingRulesLink:
        this.isMediaMenuOpen = this.isProcessingRulesActive = true;
        break;
      case this.bulkploaderLink:
        this.isMediaMenuOpen = this.isBulkUploaderActive = true;
        break;
      case this.folderStructureLink:
        this.isMediaMenuOpen = this.isMediaFolderStructureActive = true;
        break;
      case this.metadataFieldsLink:
        this.isMetadataMenuOpen = this.isMetadataFieldsActive = true;
      case this.workPlanningLink:
        this.isMetadataMenuOpen = this.isMetadataFieldsActive = true;
        break;
      case this.activeMetadataListLink:
      case this.disabledMetadataListLink:
        this.isMetadataMenuOpen = this.isMetaDataListActive = true;
        break;
        // case this.activeWorkTemplatesLink:
        this.isWorkPlanningMenuOpen = this.isWorkTemplateActive = true;
        break;
      case this.activeWorkSetsLink:
        this.isWorkPlanningMenuOpen = this.isWorkSetActive = true;
        break;
      case this.activeGroupsLink:
      case this.disabledGroupsLink:
        this.isUsersAndGroupsMenuOpen = this.isGroupsActive = true;
        break;
      case this.activeUsersLink:
      case this.unassignedUsersLink:
      case this.disabledUsersLink:
        this.isUsersAndGroupsMenuOpen = this.isUsersActive = true;
        break;
      case this.emailTemplatesLink:
        this.isEmailActive = true;
        break;
      default:
        break;
    }
  }

  processWorkPlanningLinks(url: string) {
    this.clearActiveSelections();
    this.closeAllMenus();
    this.isWorkPlanningMenuOpen = true;
    this.isWorkTemplateActive = url === this.workTemplatesLink;
    this.isWorkSetActive = url === this.workSetsLink;
  }

  clearActiveSelections() {
    this.isDashboardActive = this.isMediaUploadsActive = this.isProcessingRulesActive =
      this.isBulkUploaderActive = this.isMediaFolderStructureActive =
      this.isMetadataFieldsActive = this.isMetaDataListActive = this.isKPIsActive =
      this.isFiltersActive = this.isWorkTemplateActive = this.isWorkSetActive = this.isGroupsActive = this.isUsersActive = this.isEmailActive = false;
  }

  closeAllMenus() {
    this.isMediaMenuOpen = this.isMetadataMenuOpen = this.isWorkPlanningMenuOpen = this.isUsersAndGroupsMenuOpen = false;
  }

  showAdminNav() {
    this.isAdminNavActive === true ? this.isAdminNavActive = false : this.isAdminNavActive = true;
  }
}
