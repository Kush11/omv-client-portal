import { Component, OnInit, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { FieldConfiguration } from '../dynamic-components/field-setting';
import { DateService } from 'src/app/core/services/business/dates/date.service';
import { Tag } from 'src/app/core/models/entity/tag';
import { Store } from '@ngxs/store';
import { Filter, FilterDetail } from 'src/app/core/models/entity/filter';
import { RangeEventArgs } from '@syncfusion/ej2-angular-calendars';
import { SliderChangeEventArgs, SliderTooltipEventArgs, SliderTickEventArgs } from '@syncfusion/ej2-angular-inputs';

@Component({
  selector: 'app-facets',
  templateUrl: './facets.component.html',
  styleUrls: ['./facets.component.css']
})
export class FacetsComponent implements OnInit {

  @Input() filters: Filter[] = [];

  @Output() applyFilter = new EventEmitter<Filter>();
  @Output() reset = new EventEmitter<Filter>();
  @Output() resetAll = new EventEmitter();

  feedRange = '0 - 100';
  dateValue: string;

  CHECKBOX = 'checkbox';
  DATE = 'date';
  RANGE = 'range';
  screenWidth: number;
  currentFilter: Filter;
  tooltip: object = { isVisible: true, cssClass: 'e-tooltip-cutomization' };
  sliderValue: any;

  tooltipData: Object = { placement: 'Before', isVisible: true };
  ticksData: Object = { placement: 'None', largeStep: 2 * 86400000 };
  step: number = 86400000;
  value: number = new Date("2013-06-13").getTime();

  constructor() { }

  ngOnInit() {
    this.screenWidth = window.innerWidth;
  }

  onApplyFilter(filter: Filter) {
    console.log("onApplyFilter: ", filter);
    this.applyFilter.emit(filter);
  }

  onReset(filter: Filter) {
    this.reset.emit(filter);
  }

  onResetAll() {
    this.resetAll.emit();
  }

  onDateChange(event: RangeEventArgs) {
    if (event.startDate && event.endDate) {
      if (this.currentFilter) {
        this.currentFilter.value = event.value;
      }
    }
  }

  onSliderChanged(args: SliderChangeEventArgs) {
    if (args) {
      const minValue = args.value[0];
      const maxValue = args.value[1];
      if (minValue && maxValue) {
        if (this.currentFilter) {
          this.currentFilter.values = args.value as number[];
        }
      }
    }
  }
}
