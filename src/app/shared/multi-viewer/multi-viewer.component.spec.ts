import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MultiViewerComponent } from './multi-viewer.component';

describe('MultiViewerComponent', () => {
  let component: MultiViewerComponent;
  let fixture: ComponentFixture<MultiViewerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MultiViewerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MultiViewerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
