import { Component, OnInit, ViewEncapsulation, AfterViewInit, OnDestroy, Input, ElementRef, ViewChild, Output, EventEmitter, HostListener } from '@angular/core';
import { MediaType } from 'src/app/core/enum/media-type';
import 'videojs-seek-buttons';
import { Store } from '@ngxs/store';
import { DownloadMediaItem } from 'src/app/media/state/media/media.action';
import { ShowSuccessMessage } from 'src/app/state/app.actions';
import { environment } from 'src/environments/environment';
import { TranscriptSegment } from 'src/app/core/models/entity/transcript-segment';
import { KeyType } from 'src/app/core/enum/key-type';
declare let videojs: any;

@Component({
  selector: 'app-multi-viewer',
  templateUrl: './multi-viewer.component.html',
  styleUrls: ['./multi-viewer.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class MultiViewerComponent implements OnInit, AfterViewInit, OnDestroy {

  pdfViewerService = environment.api.baseUrl + `/V1/pdfviewer`;
  isImage: boolean;
  isVideo: boolean;
  vidObj: any;
  isDocument: boolean;
  isPDF: boolean;
  isPlaylist: boolean;
  isPlaying = true;
  isRewind: boolean;
  isOthers: boolean;

  @Input() type: string;
  @Input() isWorkPlanning: boolean;
  @Input() source: string;
  @Input() name: string;
  @Input() thumbnails: any;
  @Input() containerUrl:string;
  @Input() thumbnailUrl:string;
  @Input() sasToken:string;
  @Input() transcript: TranscriptSegment[] = [];

  @Output() getTranscript = new EventEmitter<any>();

  @ViewChild('videoPlayer') videoPlayer: ElementRef;
  @ViewChild('barSeeker') barSeeker: ElementRef;
  rewind: any;
  trustedSource: any;
  sourceExtension: string;

  // Playlist
  @Input() playlist = [];
  @Output() removeFromPlaylist = new EventEmitter<any>();
  currentlyPlaying: any;
  currentPlayingIndex = 0;
  progressBarValue = 0;
  currentPlayingTime: any = 0;
  currentPlayingDuration: any = 0;
  bufferDuration = 0;
  playpauseimg = './assets/images/pause.svg';
  pageUrl: string;
  previousSegment: TranscriptSegment;
  isRemoveMode: boolean;
  keyCode: number;

  showTranscript: boolean;
  isFullScreenMode: boolean;
  get hasTranscript(): boolean {
    if (!this.transcript) {
      return false;
    } else if (this.transcript.length === 0) {
      return false;
    }
    return true;
  }

  constructor(private store: Store) { }

  ngOnInit() {
    this.pageUrl = window.location.href;
    this.displayContent();
  }

  ngOnDestroy() {
    this.clearTypes();
  }

  ngAfterViewInit() {
    const options = {
      plugins: {
        seekButtons: {
          forward: 10,
          back: 10
        }
      },
      controlBar: {
        bigPlayButton: false,
        playToggle: true,
        timeDivider: true,
        currentTimeDisplay: true,
        durationDisplay: true,
        remainingTimeDisplay: true,
        progressControl: true,
        fullscreenToggle: true,

      },
      controls: true,
      autoplay: true,
      preload: 'auto',
      html5: {
        hls: {
          overrideNative: true
        }
      }
    };
    if (typeof (this.videoPlayer) != 'undefined') {
      this.vidObj = new videojs(this.videoPlayer.nativeElement, options, () => {
        let controlBar,
          newElement = document.createElement('button'),
          newLink = document.createElement('span');
        newElement.id = 'reverse';
        newElement.setAttribute('title', 'reverse playback');
        newElement.style.bottom = '1px';
        newElement.className = 'vjs-reverse vjs-button';
        newElement.appendChild(newLink),
          newElement.onclick = () => { this.intervalRewind(); this.isRewind = true; },
          controlBar = document.getElementsByClassName('vjs-control-bar')[0];
        const inserBeforeNode = document.getElementsByClassName('vjs-volume-panel')[0];
        controlBar.insertBefore(newElement, inserBeforeNode);
      });

      this.vidObj.one('loadedmetadata',  () => {
        if( this.containerUrl){
            const videoDuration : number = this.vidObj.duration();
            let indexOFlastSlash =  this.thumbnailUrl.lastIndexOf('/');
            let generatedurl =  this.thumbnailUrl.slice(0,indexOFlastSlash);
            let lastLink =  this.thumbnailUrl.slice(indexOFlastSlash + 1);
            let patternFormat = lastLink.split('.');
            let extension = patternFormat[1];
            let pattern =  patternFormat[0].slice(0, -1);
            let j= 1;
            let numberOfSecondsPerThumbnail =  Math.round(videoDuration/50);
            let thumbNails = []
            for(let i=0; i<= videoDuration;  ){
            
              let output = {time:i, src:`${generatedurl}/${pattern+j}.${extension}`};
             thumbNails.push(output)
             i += numberOfSecondsPerThumbnail;
             j++
            }
            const obj = {}
            for (const key of thumbNails) {
              obj[key.time] = {
                src: key.src,
                style: {
                  left: '-62px',
                  width: '135px',
                  height: '94px',
                }
              };
            }
            console.log('THumbnails', obj);
            this.vidObj.thumbnails(
             {
              obj
             }
            );
       
        }else{
        let duration = this.vidObj.duration();
        console.log("current duration", duration);
        let thumbnails = [];
        let indexOFlastSlash =  this.thumbnailUrl.lastIndexOf('/');
        let lastIndexOfDot = this.thumbnailUrl.lastIndexOf(".");
        let extension =this.thumbnailUrl.substring(lastIndexOfDot + 1);
        let generatedurl =  this.thumbnailUrl.slice(0,indexOFlastSlash)
     
        let  i = 0;
        while(i < duration ){
          let output = {time:i, src:`${generatedurl}/thumb-${i}.${extension}`};
           thumbnails.push(output);
          i += 10;
        }
        const obj = {}
        for (const key of thumbnails) {
          obj[key.time] = {
            src: key.src,
            style: {
              left: '-62px',
              width: '135px',
              height: '94px',
            }
          };
        }

        console.log('no conatiner', obj, this.thumbnails);
          this.vidObj.thumbnails(
           // this.thumbnails
           
            obj
           
          );
        }
       
      })
     
    }
  }

  displayContent() {
    this.clearTypes();
    switch (this.type) {
      case MediaType.Image:
        this.isImage = true;
        break;
      case MediaType.Video:
        this.isVideo = true;
        break;
      case MediaType.Document:
        const sourceExt = this.source.split('.').pop();
        this.sourceExtension = sourceExt.split('?')[0];
        this.trustedSource = this.source;
        this.isDocument = true;
        break;
      case MediaType.Pdf:
        this.isPDF = true;
        break;
      case MediaType.Playlist:
        this.isPlaylist = true;
        this.currentlyPlaying = this.playlist[0];
        this.getTranscript.emit(this.currentlyPlaying);
        break;
      default:
        this.isOthers = true;
        break;
    }
  }

  @HostListener('document:keydown', ['$event'])
  handleKeyDown(event: KeyboardEvent) {
    this.keyCode = event.keyCode;
    event.preventDefault();
    let video: HTMLVideoElement;
    if (this.isVideo || this.isPlaylist) {
      video = this.videoPlayer.nativeElement as HTMLVideoElement;
    }
    switch (event.keyCode) {
      case KeyType.Down:
        if (this.isVideo || this.isPlaylist) {
          video.volume = video.volume >= .05 ? video.volume - .05 : 0;
        }
        break;
      case KeyType.Up:
        if (this.isVideo || this.isPlaylist) {
          video.volume = video.volume <= .95 ? video.volume + .05 : 1;
        }
        break;
      case KeyType.Right:
        if (this.isVideo || this.isPlaylist) {
          video.currentTime += 10;
        }
        break;
      case KeyType.Left:
        if (this.isVideo || this.isPlaylist) {
          video.currentTime -= 10;
        }
        break;
      case KeyType.SpaceBar:
        if (this.isVideo || this.isPlaylist) {
          if (video.paused) {
            video.play();
          } else {
            video.pause();
          }
        }
        break;
      case KeyType.F:
        if (this.isVideo || this.isPlaylist) {
          if (this.isFullScreenMode) {
            video.webkitExitFullScreen();
            this.isFullScreenMode = false;
          } else {
            video.requestFullscreen()
              .then(response => {
                this.isFullScreenMode = true;
              }, err => { console.log("requestFullscreen err: ", err); });
          }
        }
        break;
      case KeyType.M:
        if (this.isVideo || this.isPlaylist) {
          video.muted = !video.muted;
        }
        break;
      case KeyType.N:
        if (this.isPlaylist) {
          this.playNextVideo();
        }
        break;
      case KeyType.P:
        if (this.isPlaylist) {
          this.playPreviousVideo();
        }
        break;
      case KeyType.R:
        if (this.isVideo || this.isPlaylist) {
          this.intervalRewind();
        }
        break;
      case KeyType.T:
        if (this.isVideo || this.isPlaylist) {
          this.showTranscript = !this.showTranscript;
        }
        break;
    }
  }

  download(item?: any) {
    if (item) {
      this.source = item.url;
      this.name = item.name;
    }
    this.store.dispatch(new DownloadMediaItem(this.name, this.source));
  }

  /* #region video-controls */

  intervalRewind() {
    if (!this.isRewind) {
      this.rewind = setInterval(() => {
        const { nativeElement } = this.videoPlayer;
        nativeElement.playbackRate = 1.0;
        if (nativeElement.currentTime <= 0) {
          nativeElement.currentTime == 0;
          clearInterval(this.rewind);
          this.isRewind = false;

        }
        if (nativeElement.paused) {
          clearInterval(this.rewind);
          nativeElement.currentTime;
          this.isRewind = false;
        } else {
          nativeElement.currentTime += -.1;

        }
      }, 30);
    }
  }

  onClickPlaylistItem(item: any, index: number) {
    this.currentlyPlaying = item;
    this.getTranscript.emit(this.currentlyPlaying);
    this.currentPlayingIndex = index;
    const video = this.videoPlayer.nativeElement as HTMLVideoElement;
    video.src = item.url;
  }

  onPlayPause(videoPlayer) {
    if (this.rewind) { clearInterval(this.rewind); } clearTimeout();
    if (videoPlayer.paused) {
      videoPlayer.play();
      this.playpauseimg = './assets/images/pause.svg';
    } else {
      videoPlayer.pause();
      this.playpauseimg = './assets/images/play-button.svg';
    }
    this.isRewind = false;
  }

  onRemoveFromPlaylist(id: string) {
    if (this.currentlyPlaying.id === id) {
      const index = this.playlist.indexOf(this.currentlyPlaying);
      if (index === (this.playlist.length - 1)) {
        this.currentlyPlaying = this.playlist[0];
      } else {
        this.currentlyPlaying = this.playlist[index + 1];
      }
    }
    this.removeFromPlaylist.emit(id);
  }

  playPreviousVideo() {
    const isFirstVideo = this.currentPlayingIndex === 0;
    if (!isFirstVideo) {
      this.currentPlayingIndex--;
      this.currentlyPlaying = this.playlist[this.currentPlayingIndex];
      this.getTranscript.emit(this.currentlyPlaying);
      const video = this.videoPlayer.nativeElement as HTMLVideoElement;
      video.src = this.currentlyPlaying.url;
    }
  }

  playNextVideo() {
    const isLastVideo = this.currentPlayingIndex === (this.playlist.length - 1);
    if (!isLastVideo) {
      this.currentPlayingIndex++;
      this.currentlyPlaying = this.playlist[this.currentPlayingIndex];
      this.getTranscript.emit(this.currentlyPlaying);
      const video = this.videoPlayer.nativeElement as HTMLVideoElement;
      video.src = this.currentlyPlaying.url;
    }
  }

  nextVideo() {
    if (this.currentPlayingIndex === this.playlist.length) {
      this.currentPlayingIndex = 0;
      this.playpauseimg = './assets/images/play-button.svg';
    }
    this.currentlyPlaying = this.playlist[this.currentPlayingIndex];
    this.playpauseimg = './assets/images/pause.svg';
    this.getTranscript.emit(this.currentlyPlaying);
  }

  videoUpdate() {
    const { currentTime } = this.videoPlayer.nativeElement;
    if (!this.transcript) return;
    let currentSegment: TranscriptSegment;
    this.transcript.forEach((segment, index) => {
      segment.isCurrent = false;
      const next = this.transcript[index + 1];
      if (next) {
        if (Number(segment.startTime) <= Number(currentTime) && Number(next.startTime) > Number(currentTime)) {
          segment.isCurrent = true;
          currentSegment = segment;
        }
      } else if (Number(segment.startTime) <= Number(currentTime)) {
        segment.isCurrent = true;
        currentSegment = segment;
      }
    });
    if (!currentSegment) return;

    const index = this.transcript.indexOf(currentSegment);
    this.scrollTranscriptToActiveSegment(index);
  }

  scrollTranscriptToActiveSegment(li: any) {
    li = document.getElementById(li);
    if (!li) return;
    let ul = li.parentNode;
    let fudge: number = 0;
    if (li.offsetHeight > ul.offsetHeight) {
      fudge = 0;
    } else if ((ul.offsetHeight - li.offsetHeight) < 40) {
      fudge = ul.offsetHeight - li.offsetHeight;
    } else {
      fudge = 40;
    }
    ul.scrollTop = li.offsetTop - ul.offsetTop - fudge;
  }

  onSegmentClicked(segment: TranscriptSegment) {
    this.videoPlayer.nativeElement.currentTime = segment.startTime;
  }

  /*  #endregion */

  clearTypes() {
    this.isImage = this.isVideo = this.isDocument = this.isPDF = this.isPlaylist = false;
  }

  /* #region copy-to-clipboard */
  copyUrl() {
    this.copyText(this.pageUrl)
  }
  copyText(val: string) {
    let selBox = document.createElement('textarea');
    selBox.style.position = 'fixed';
    selBox.style.left = '0';
    selBox.style.top = '0';
    selBox.style.opacity = '0';
    selBox.value = val;
    document.body.appendChild(selBox);
    selBox.focus();
    selBox.select();
    document.execCommand('copy');
    document.body.removeChild(selBox);
    this.store.dispatch(
      new ShowSuccessMessage('Copied to clipboard.')
    );
  }
  /* #endregion*/
}
