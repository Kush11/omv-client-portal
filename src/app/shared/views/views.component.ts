import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { takeUntil } from 'rxjs/operators';
import { SubSink } from 'subsink/dist/subsink';

@Component({
  selector: 'app-views',
  templateUrl: './views.component.html',
  styleUrls: ['./views.component.css']
})
export class ViewsComponent implements OnInit, OnDestroy {

  private subs = new SubSink();
  @Input() activeView = 'tile';

  @Input() showTreeView = true;
  @Input() showMapView = true;

  @Input() showListView = true;



  constructor(private router: Router, private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.queryParams
      .subscribe(params => {
      let view = params.view;
      this.activeView = view ? view : this.activeView;
    });
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

  selectView(view: string) {    
    this.activeView = view;
    var url = this.router.url.split('?')[0];
    this.router.navigate([url], { queryParams: { view: view } });
  }
}
