import { Component, OnInit, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { Pager, PagerDropDown } from '@syncfusion/ej2-angular-grids';
Pager.Inject(PagerDropDown);

@Component({
  selector: 'app-pager',
  templateUrl: './pager.component.html',
  styleUrls: ['./pager.component.css']
})
export class PagerComponent implements OnInit {

  @Input() totalRecordsCount: number;
  @Input() pageCount: number;
  @Input() pageSize: number;
  @Input() currentPage = 1;
  @Input() pageSizes = [5, 10, 20];
  @Input() showDropdown: boolean;

  @Output() pageNumberChanged = new EventEmitter<number>();
  @Output() pageSizeChanged = new EventEmitter<any>();

  isLoading = true;

  constructor() { }

  ngOnInit() {
  }

  changeEvent(event: any) {
    if (event.currentPage) {
      const props = Object.keys(event.newProp);
      if (props.includes('currentPage')) {
        this.pageNumberChanged.emit(event.currentPage);
      }
    }
    return event;
  }

  onPageSizeChanged(e: any) {
    if (this.isLoading) {
      this.isLoading = false;
      return;
    }
    console.log("pageSizeChanged: ", e);
    this.pageSize = e.pageSize;
    this.pageSizeChanged.emit(this.pageSize);
  }
}
