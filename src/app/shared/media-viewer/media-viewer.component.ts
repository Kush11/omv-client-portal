import { Component, OnInit, ViewEncapsulation, OnDestroy, ViewChild, ElementRef } from '@angular/core';
import { Store, Select } from "@ngxs/store";
import { ActivatedRoute, Router } from "@angular/router";
import { MediaState } from 'src/app/media/state/media/media.state';
import { GetMediaItem, ClearCurrentMediaItem } from 'src/app/media/state/media/media.action';
import { DomSanitizer } from '@angular/platform-browser';
import { BaseComponent } from '../base/base.component';
// import {
//   LinkAnnotationService, BookmarkViewService, MagnificationService, ThumbnailViewService,
//   ToolbarService, NavigationService, TextSearchService, TextSelectionService, PrintService, AnnotationService
// } from '@syncfusion/ej2-angular-pdfviewer';
import { Breadcrumb } from 'src/app/core/models/breadcrumb';
import { AppState } from 'src/app/state/app.state';
import { Observable } from 'rxjs/internal/Observable';
import { SubSink } from 'subsink/dist/subsink';
import { FsDocument } from 'src/app/core/models/video-element';
declare var require: any;

@Component({
  selector: 'app-media-viewer',
  templateUrl: './media-viewer.component.html',
  styleUrls: ['./media-viewer.component.css'],
  encapsulation: ViewEncapsulation.None,
  // providers: [LinkAnnotationService, BookmarkViewService, MagnificationService, ThumbnailViewService, ToolbarService, NavigationService, TextSearchService, TextSelectionService, PrintService, AnnotationService]
})
export class MediaViewerComponent extends BaseComponent implements OnInit, OnDestroy {
  public mediaOBJ: any;
  public media: string;
  public mediaType: string;
  public mediaSource: string;
  public service = 'http://omv.test.eminenttechnology.com/OMV.Api/api/V1/pdfviewer';
  public document: string;
  rewind: any;
  videoPlayer;

  @Select(MediaState.getCurrentMediaItem) mediaItem$: Observable<string>;
  @Select(MediaState.getMedia) media$: Observable<any>;
  @Select(AppState.getPreviousRoute) previousRoute$: Observable<Breadcrumb[]>;

  private subs = new SubSink();
  dataSource: any;
  mediaID: string;
  url: string;
  videoData: {};
  trustedUrl: any;
  currentVideo: object;
  componentActive = true;
  videoFileTypes = ['MP4', 'MOV', 'AVI', 'MKV'];
  docFileTypes = ['DOCX', 'DOC', 'PPT', 'doc', 'docx', 'XLSX', 'XLS', 'xls', 'xlsx'];

  imgFileTypes = ['JPEG', 'PNG', 'JPG', 'GIF', 'jpeg', 'gif', 'png', 'jpg'];
  docViewerVisible: boolean;
  pdfViewerVisible: boolean;
  imgViewerVisible: boolean;
  videoViewerVisible: boolean;
  imageElement: any;
  divPlayer: any;

  currentPlayingTime : any = 0;
  currentPlayingDuration : any =0; 
  bufferDuration:number = 0;
  playpauseimg :string = '../../../assets/images/pause.svg';
  progressBarValue : number = 0;

  @ViewChild('barSeeker') barSeeker :ElementRef;
  
  constructor(protected store: Store, private activeRoute: ActivatedRoute, private router: Router,
    private sanitizer: DomSanitizer) {
    super(store);
  }

  ngOnInit() {
    const activeId = this.activeRoute.snapshot.paramMap.get('id');
    this.mediaID = activeId;
    this.store.dispatch(new GetMediaItem(activeId));

    this.subs.sink = this.mediaItem$
      .subscribe(item => {
        if (item) {
          this.dataSource = item;
          this.toggleMediaViewer();
        }
      });
  }

  ngOnDestroy(): void {
    this.imageElement = '';
    this.url = '';
    if (this.videoFileTypes.includes(this.mediaType)) {
      this.store.dispatch(new ClearCurrentMediaItem());
    }
    this.subs.unsubscribe();
  }

  onBack(route: string) {
    this.router.navigate([route]);
  }

  toggleMediaViewer() {
    this.mediaType = this.dataSource.type;
    if (this.docFileTypes.includes(this.mediaType)) {
      this.imgViewerVisible = false;
      this.videoViewerVisible = false;
      this.docViewerVisible = true;
      this.pdfViewerVisible = false;

      const element = window.document.getElementsByClassName('office iframe');
      if (!element) {
        console.log('not exists');
      }
      this.url = `https://docs.google.com/gview?url=${this.dataSource.url}&embedded=true`;
      this.trustedUrl = this.sanitizer.bypassSecurityTrustResourceUrl(this.url);
    } else if (this.mediaType === 'PDF') {
      this.videoViewerVisible = false;
      this.imgViewerVisible = false;
      this.pdfViewerVisible = true;
      this.docViewerVisible = false;
      this.url = this.dataSource.url;
    }

    else if (this.videoFileTypes.includes(this.mediaType)) {
      this.videoPlayer = document.getElementById('videoPlayer');
      this.pdfViewerVisible = false;
      this.docViewerVisible = false;
      this.imgViewerVisible = false;
      this.videoViewerVisible = true;
      this.videoData = this.dataSource;


    } else {
      this.imgViewerVisible = true;
      this.videoViewerVisible = false;
      this.pdfViewerVisible = false;
      this.docViewerVisible = false;
      this.imageElement = window.document.querySelector('img.image');
      if (this.imageElement) {
        this.url = this.dataSource.url;
      }
      console.log('element', this.imageElement);
      // const viewer = new Viewer(imageElement, {
      //   url: 'data-original',
      //   toolbar: {
      //     oneToOne: true,
      //     prev() {
      //       viewer.prev(true);
      //     },
      //     play: true,
      //     next() {
      //       viewer.next(true);
      //     }
      //   },
      // });
    }


  }

  download() {
    var fileSaver = require('file-saver');
    fileSaver.saveAs(this.dataSource.url, this.dataSource.name);
  }

  /* #region video-controls */
 volume(evt) {
  this.videoPlayer.nativeElement.volume = evt.target.value;
}


  intervalRewind() {
    this.rewind = setInterval(() => {
      const { nativeElement } = this.videoPlayer;
      nativeElement.playbackRate = 1.0;
      if (nativeElement.currentTime <= 0) {
        nativeElement.currentTime == 0;
        clearInterval(this.rewind);
        nativeElement.pause();
        this.playpauseimg = "../../../assets/images/play-button.svg";
      }
      else {
        nativeElement.currentTime += -.1;
      }
    }, 30);
  }

   seek(e) {
    const { nativeElement } = this.videoPlayer;
    this.barSeeker.nativeElement
    var percent =  e.offsetX /  this.barSeeker.nativeElement.offsetWidth;
    const newTime =  percent * nativeElement.duration;
    nativeElement.currentTime =  newTime;
  }

   updateProgressBar() {
    const { nativeElement } = this.videoPlayer; 
  	let percentage = Math.floor((100 / nativeElement.duration) *nativeElement.currentTime);
    this.progressBarValue = percentage;
     this.currentPlayingTime = nativeElement.currentTime;
  }


  onPlayPause(videoPlayer) {
    if(this.rewind)clearInterval(this.rewind);   
    if (videoPlayer.paused) {
      videoPlayer.play();
        this.playpauseimg = "../../../assets/images/pause.svg";
    }
    else {
      videoPlayer.pause();
        this.playpauseimg = "../../../assets/images/play-button.svg";

    }
}


loaded()
{
 
    var v =  this.videoPlayer.nativeElement
    var r = v.buffered;
    var total = v.duration;

    var start = r.start(0);
    var end = r.end(0);
    this.bufferDuration = (end/total)*100;
   
         
}

  moveForward(){
    this.videoPlayer.nativeElement.currentTime += 5;
  }
  moveBackward(){
    this.videoPlayer.nativeElement.currentTime -= 5;
  }

}
