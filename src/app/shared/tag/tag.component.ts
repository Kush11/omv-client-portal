import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Tag } from 'src/app/core/models/entity/tag';

@Component({
  selector: 'app-tag',
  templateUrl: './tag.component.html',
  styleUrls: ['./tag.component.css']
})
export class TagComponent implements OnInit {

  @Input() tag: Tag;
  @Input() right: number;
  @Input() unit = 'px';
  @Input() isSmall: boolean;
  @Output() removeTag = new EventEmitter<Tag>();

  constructor() { }

  ngOnInit() {
  }

  removeTagEvent(tag: Tag) {
    this.removeTag.emit(tag);
  }
}
