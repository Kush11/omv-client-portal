import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SideListMenuComponent } from './side-list-menu.component';

describe('SideListMenuComponent', () => {
  let component: SideListMenuComponent;
  let fixture: ComponentFixture<SideListMenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SideListMenuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SideListMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
