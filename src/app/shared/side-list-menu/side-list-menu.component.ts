import { Component, OnInit, Input, Output, EventEmitter, AfterViewInit } from '@angular/core';
import { MenuItem } from 'src/app/core/models/entity/menu-item';

@Component({
  selector: 'app-side-list-menu',
  templateUrl: './side-list-menu.component.html',
  styleUrls: ['./side-list-menu.component.css']
})
export class SideListMenuComponent implements OnInit {

  activeMenu: MenuItem;
  activeSubMenu: MenuItem;
  @Input() dataSource: MenuItem[] = [];
  @Input() title: string;

  @Output() menuSelected = new EventEmitter<MenuItem>();
  @Output() subMenuSelected = new EventEmitter<MenuItem>();
  @Output() download = new EventEmitter<MenuItem>();

  constructor() { }

  ngOnInit() {
  }

  onMenuSelected(item: MenuItem) {
    const activeMenu = this.dataSource.find(x => x.isSelected);
    if (activeMenu) {
      if (activeMenu.id === item.id) return;
    }
    this.clearSelections();
    this.setActiveMenu(item);
    this.menuSelected.emit(item);
  }

  onSubMenuSelected(subItem: MenuItem) {
    let activeSubMenu: MenuItem;
    this.dataSource.forEach(d => {
      activeSubMenu = activeSubMenu || d.subMenuItems.find(d => d.isSelected);
    });
    if (activeSubMenu) {
      if (activeSubMenu.id === subItem.id) return;
    }
    this.clearSelections();
    this.setActiveSubMenu(subItem);
    this.subMenuSelected.emit(subItem);
  }

  onDownload(subItem: MenuItem) {
    this.download.emit(subItem);
  }

  setActiveMenu(item: MenuItem) {
    this.dataSource.forEach(m => {
      m.isSelected = m.id === item.id;
      if (m.subMenuItems && m.isSelected) {
        m.subMenuItems.forEach((s, i) => {
          if (i === 0) {
            s.isSelected = true;
            this.activeSubMenu = s;
          }
        });
      }
      if (m.isSelected) this.activeMenu = m;
    });
  }

  setActiveSubMenu(subItem: MenuItem) {
    this.dataSource.forEach(m => {
      if (m.subMenuItems) {
        m.subMenuItems.forEach(s => {
          s.isSelected = s.id === subItem.id;
          if (s.isSelected) {
            m.isSelected = true;
            this.activeMenu = m;
            this.activeSubMenu = s;
          }
        });
      }
    });
  }

  clearSelections() {
    this.activeMenu = null;
    this.activeSubMenu = null;
    this.dataSource.forEach(d => {
      d.isSelected = false;
      if (d.subMenuItems) d.subMenuItems.forEach(s => s.isSelected = false);
    });
  }
}
