import { FormGroup } from '@angular/forms';
import { FieldConfiguration, FieldMode } from './field-setting';
import { EventEmitter } from '@angular/core';

export interface Field {
  mode: FieldMode,
  config: FieldConfiguration,
  group: FormGroup,
  allowDeleting?: boolean,
  allowUpload?: boolean,
  enableSortSetting?: boolean,
  enableDeleteSetting?: boolean,
  enableRequiredSetting?: boolean,
  disabled?: boolean;
  disabledDates?: Date[];

  delete?: EventEmitter<any>,
  dropdownChange?: EventEmitter<any>,  
  eventChange?: EventEmitter<any>,
  addTag?: EventEmitter<any>,
  change?: EventEmitter<any>,
  requiredToggle?: EventEmitter<any>,
  checkBoxChange?: EventEmitter<any>,
  uniqueChange?: EventEmitter<any>,
  labelChange?: EventEmitter<any>,
  revertLabel?: EventEmitter<any>,
  sort?: EventEmitter<any>,
  upload?: EventEmitter<any>
}
