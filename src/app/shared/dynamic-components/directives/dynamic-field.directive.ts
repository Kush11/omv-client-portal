import { ComponentFactoryResolver, ComponentRef, Directive, Input, OnInit, ViewContainerRef, EventEmitter, Output, Type, OnChanges } from "@angular/core";
import { FormGroup } from "@angular/forms";
import { Field } from '../field.interface';
import { FieldConfiguration, FieldMode } from '../field-setting';
import { FormLabelComponent } from '../components/form-label.component';
import { FormInputComponent } from '../components/form-input/form-input.component';
import { FormSelectComponent } from '../components/form-select/form-select.component';
import { FormHeaderComponent } from '../components/form-header/form-header.component';
import { FormCheckboxComponent } from '../components/form-checkbox/form-checkbox.component';
import { FormDatePickerComponent } from '../components/form-date-picker/form-date-picker.component';
import { FormComboboxComponent } from '../components/form-combobox/form-combobox.component';
import { FormTimePickerComponent } from '../components/form-time-picker/form-time-picker.component';
import { FormDateRangePickerComponent } from '../components/form-date-range-picker/form-date-range-picker.component';
import { FormDateTimePickerComponent } from '../components/form-date-time-picker/form-date-time-picker.component';

const components: {[type: string]: Type<Field>} = {
  input: FormInputComponent,
  label: FormLabelComponent,
  select: FormSelectComponent,
  date: FormDatePickerComponent,
  dateRange: FormDateRangePickerComponent,
  dateTime: FormDateTimePickerComponent,
  time: FormTimePickerComponent,
  header: FormHeaderComponent,
  combobox: FormComboboxComponent,
  checkbox: FormCheckboxComponent
};

@Directive({
  selector: "[dynamicField]"
})
export class DynamicFieldDirective implements Field, OnChanges, OnInit {
  @Input() mode: FieldMode;
  @Input() config: FieldConfiguration;
  @Input() group: FormGroup;
  @Input() allowDeleting: boolean;
  @Input() allowUpload: boolean;
  @Input() enableDeleteSetting: boolean;
  @Input() enableRequiredSetting: boolean;
  @Input() enableSortSetting: boolean;
  @Input() disabled: boolean;

  @Output() delete = new EventEmitter<any>();
  @Output() dropdownChange = new EventEmitter<any>();
  @Output() addTag = new EventEmitter<any>();
  @Output() change = new EventEmitter<any>();
  @Output() checkBoxChange = new EventEmitter<any>();
  @Output() labelChange = new EventEmitter<any>();
  @Output() revertLabel = new EventEmitter<any>();
  @Output() sort = new EventEmitter<any>();
  @Output() upload = new EventEmitter<any>();

  @Output() formChanges = new EventEmitter<any>();

  component: ComponentRef<Field>;

  constructor(private resolver: ComponentFactoryResolver, private container: ViewContainerRef) {}

  ngOnChanges() {
    if (this.component) {
      this.component.instance.mode = this.mode;
      this.component.instance.config = this.config;
      this.component.instance.group = this.group;
      this.component.instance.allowDeleting = this.allowDeleting;
      this.component.instance.allowUpload = this.allowUpload;
      this.component.instance.delete = this.delete;
      this.component.instance.dropdownChange = this.dropdownChange;
      this.component.instance.enableDeleteSetting = this.enableDeleteSetting;
      this.component.instance.enableRequiredSetting = this.enableRequiredSetting;
      this.component.instance.enableSortSetting = this.enableSortSetting;
      this.component.instance.disabled = this.disabled;
      this.component.instance.addTag = this.addTag;
      this.component.instance.change = this.change;
      this.component.instance.checkBoxChange = this.checkBoxChange;
      this.component.instance.labelChange = this.labelChange;
      this.component.instance.revertLabel = this.revertLabel;
      this.component.instance.sort = this.sort;
      this.component.instance.upload = this.upload;
    }
  }

  ngOnInit() {    
    if (!components[this.config.type]) {
      const supportedTypes = Object.keys(components).join(', ');
      throw new Error(
        `Trying to use an unsupported type (${this.config.type}).
        Supported types: ${supportedTypes}`
      );
    }
    const component = this.resolver.resolveComponentFactory<Field>(components[this.config.type]);
    this.component = this.container.createComponent(component);
    this.component.instance.mode = this.mode;
    this.component.instance.config = this.config;
    this.component.instance.group = this.group;
    this.component.instance.allowDeleting = this.allowDeleting;
    this.component.instance.allowUpload = this.allowUpload;
    this.component.instance.delete = this.delete;
    this.component.instance.dropdownChange = this.dropdownChange;
    this.component.instance.enableDeleteSetting = this.enableDeleteSetting;
    this.component.instance.enableRequiredSetting = this.enableRequiredSetting;
    this.component.instance.enableSortSetting = this.enableSortSetting;
    this.component.instance.disabled = this.disabled;
    this.component.instance.addTag = this.addTag;
    this.component.instance.change = this.change;
    this.component.instance.checkBoxChange = this.checkBoxChange;
    this.component.instance.labelChange = this.labelChange;
    this.component.instance.revertLabel = this.revertLabel;
    this.component.instance.sort = this.sort;
    this.component.instance.upload = this.upload;
  }
}
