import { Component, Output, EventEmitter } from "@angular/core";
import { FormGroup } from "@angular/forms";
import { Field } from '../field.interface';
import { FieldConfiguration, FieldMode } from '../field-setting';
import { Tag } from 'src/app/core/models/entity/tag';

@Component({
  selector: "form-input",
  template: `
    <div [formGroup]="group">
      <label class="form-label">{{ config.label }}</label>
      <table width="100%">
        <tr>
          <td width="90%" [width]="allowDeleting || config.allowDocumentUpload ? '90%' : '1000%'" >
            <label class="form-control-label"> {{ config.value }} </label>
          </td>
          <td width="10%" *ngIf="allowDeleting" [align]="'center'">
            <img src="./assets/images/icon-trash.svg" height="18" (click)="deleteEvent(config)">
          </td>
          <td *ngIf="config.allowDocumentUpload" width="10%" style="cursor: pointer;" [align]="'center'" (click)="onUpload(config)">
            <img src="./assets/images/icon-upload.svg" height="18">
          </td>
        </tr>
      </table>
    </div>
    <br/>
      `,
  styles: []
})
export class FormLabelComponent implements Field {
  mode = FieldMode.Default;
  config: FieldConfiguration;
  group: FormGroup;
  allowDeleting: boolean;
  delete?: EventEmitter<any>;
  upload = new EventEmitter<any>();

  deleteEvent(config: any) {
    this.delete.emit(config);
  }

  onUpload(config: any) {
    this.upload.emit(config);
  }
}
