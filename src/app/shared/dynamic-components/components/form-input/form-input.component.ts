import { Component, EventEmitter } from '@angular/core';
import { Field } from '../../field.interface';
import { FieldConfiguration, FieldMode } from '../../field-setting';
import { FormGroup } from '@angular/forms';
import { Tag } from 'src/app/core/models/entity/tag';

@Component({
  selector: 'form-input',
  templateUrl: './form-input.component.html',
  styleUrls: ['./form-input.component.css']
})
export class FormInputComponent implements Field {
  mode = FieldMode.Default;
  config: FieldConfiguration;
  group: FormGroup;
  allowDeleting: boolean;
  enableSortSetting: boolean;
  enableDeleteSetting: boolean;
  enableRequiredSetting: boolean;
  disabled: boolean;
  delete = new EventEmitter<any>();
  change = new EventEmitter<any>();
  checkBoxChange = new EventEmitter<any>();
  labelChange = new EventEmitter<any>();
  revertLabel = new EventEmitter<any>();
  sort = new EventEmitter<any>();
  addTag = new EventEmitter<Tag>();
  upload = new EventEmitter<any>();

  changeEvent(event: any) {
    this.change.emit(event);
  }

  deleteEvent(config: any) {
    this.delete.emit(config);
  }

  sortEvent(direction: string) {
    const config = { ...this.config, sortDirection: direction };
    this.sort.emit(config);
  }

  onUpload(config: any) {
    this.upload.emit(config);
  }

  requiredEvent() {
    this.config.required = !this.config.required;
    this.checkBoxChange.emit(this.config);
  }

  editableEvent() {
    this.config.editable = !this.config.editable;
    if (!this.config.editable) this.config.required = false;
    this.checkBoxChange.emit(this.config);
  }

  allowDocumentEvent() {
    this.config.allowDocumentUpload = !this.config.allowDocumentUpload;
    this.checkBoxChange.emit(this.config);
  }

  uniqueEvent() {
    this.config.unique = !this.config.unique;
    this.checkBoxChange.emit(this.config);
  }

  labelChangeEvent(data: FieldConfiguration) {
    this.labelChange.emit(data);
  }

  revertLabelEvent(field: FieldConfiguration) {
    this.revertLabel.emit(field);
  }

  onAddTag(value: any) {
    if (value) {
      let filterTag: Tag = {
        label: this.config.label,
        name: this.config.name,
        value: value,
        order: this.config.order
      }
      this.addTag.emit(filterTag);
      this.config.value = '';
    }
  }
}
