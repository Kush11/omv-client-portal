import { Component, EventEmitter, ViewChild } from '@angular/core';
import { Field } from '../../field.interface';
import { FieldConfiguration, FieldMode } from '../../field-setting';
import { FormGroup } from '@angular/forms';
import { DateService } from 'src/app/core/services/business/dates/date.service';
import { Tag } from 'src/app/core/models/entity/tag';
import { ChangedEventArgs } from '@syncfusion/ej2-angular-calendars';

@Component({
  selector: 'app-form-date-time-picker',
  templateUrl: './form-date-time-picker.component.html',
  styleUrls: ['./form-date-time-picker.component.css']
})
export class FormDateTimePickerComponent implements Field {
  mode = FieldMode.Default;
  config: FieldConfiguration;
  group: FormGroup;
  allowDeleting: boolean;
  enableDeleteSetting: boolean;
  enableRequiredSetting: boolean;
  enableSortSetting: boolean;
  disabled: boolean;

  delete = new EventEmitter<any>();
  requiredToggle = new EventEmitter<any>();
  checkBoxChange = new EventEmitter<any>();
  labelChange = new EventEmitter<any>();
  revertLabel = new EventEmitter<any>();
  sort = new EventEmitter<any>();
  maxDate = new Date();
  addTag = new EventEmitter<Tag>();
  upload = new EventEmitter<any>();

  constructor(private dateService: DateService) { }

  deleteEvent(config: any) {
    this.delete.emit(config);
  }

  requiredToggleEvent(config: any) {
    this.config.isRequired = !this.config.isRequired;
    this.requiredToggle.emit(config);
  }

  sortEvent(direction: string) {
    const config = { ...this.config, sortDirection: direction };
    this.sort.emit(config);
  }

  onUpload(config: any) {
    this.upload.emit(config);
  }

  requiredEvent() {
    this.config.required = !this.config.required;
    this.checkBoxChange.emit(this.config);
  }

  editableEvent() {
    this.config.editable = !this.config.editable;
    if (!this.config.editable) this.config.required = false;
    this.checkBoxChange.emit(this.config);
  }

  allowDocumentEvent() {
    this.config.allowDocumentUpload = !this.config.allowDocumentUpload;
    this.checkBoxChange.emit(this.config);
  }

  uniqueEvent() {
    this.config.unique = !this.config.unique;
    this.checkBoxChange.emit(this.config);
  }

  labelChangeEvent(data: FieldConfiguration) {
    this.labelChange.emit(data);
  }

  revertLabelEvent(field: FieldConfiguration) {
    this.revertLabel.emit(field);
  }

  onAddTag(event: ChangedEventArgs) {
    if (!event.value) return;
    const time = this.dateService.formatToString(event.value, 'MM/DD/YYYY  hh:mm A');
    const tag: Tag = {
      label: this.config.label,
      name: this.config.name,
      value: time,
      order: this.config.order
    }
    this.addTag.emit(tag);
  }

}
