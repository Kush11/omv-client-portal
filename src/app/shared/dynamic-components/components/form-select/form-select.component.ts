import { Component, EventEmitter, ViewChild } from '@angular/core';
import { Field } from '../../field.interface';
import { FieldConfiguration, FieldMode } from '../../field-setting';
import { FormGroup } from '@angular/forms';
import { Tag } from 'src/app/core/models/entity/tag';
import { DropDownListComponent } from '@syncfusion/ej2-angular-dropdowns';

@Component({
  selector: 'form-select',
  templateUrl: './form-select.component.html',
  styleUrls: ['./form-select.component.css']
})
export class FormSelectComponent implements Field {
  mode = FieldMode.Default;
  config: FieldConfiguration;
  group: FormGroup;
  allowDeleting: boolean;
  allowUpload: boolean;
  enableSortSetting: boolean;
  enableDeleteSetting: boolean;
  enableRequiredSetting: boolean;
  disabled: boolean;

  fields: Object = { text: 'description', value: 'value' };
  delete = new EventEmitter<any>();
  requiredToggle = new EventEmitter<any>();
  change = new EventEmitter<any>();
  checkBoxChange = new EventEmitter<any>();
  uniqueChange = new EventEmitter<any>();
  labelChange = new EventEmitter<any>();
  revertLabel = new EventEmitter<any>();
  sort = new EventEmitter<any>();
  upload = new EventEmitter<any>();
  addTag = new EventEmitter<Tag>();

  @ViewChild("filterSelect") filterSelect: DropDownListComponent;

  changeEvent(event: any) {
    this.change.emit(event);
  }

  deleteEvent(config: any) {
    this.delete.emit(config);
  }

  requiredToggleEvent(config: any) {
    this.config.isRequired = !this.config.isRequired;
    this.requiredToggle.emit(config);
  }

  requiredEvent() {
    this.config.required = !this.config.required;
    this.checkBoxChange.emit(this.config);
  }

  editableEvent() {
    this.config.editable = !this.config.editable;
    if (!this.config.editable) this.config.required = false;
    this.checkBoxChange.emit(this.config);
  }

  allowDocumentEvent() {
    this.config.allowDocumentUpload = !this.config.allowDocumentUpload;
    this.checkBoxChange.emit(this.config);
  }

  uniqueEvent() {
    this.config.unique = !this.config.unique;
    this.checkBoxChange.emit(this.config);
  }

  labelChangeEvent(data: FieldConfiguration) {
    this.labelChange.emit(data);
  }

  revertLabelEvent(field: FieldConfiguration) {
    this.revertLabel.emit(field);
  }

  sortEvent(direction: string) {
    const config = { ...this.config, sortDirection: direction };
    this.sort.emit(config);
  }

  onUpload(config: any) {
    this.upload.emit(config);
  }

  onAddTag(event: any) {
    const data = event.itemData;
    if (data) {
      if (!data.description) return;
      const tag: Tag = {
        label: this.config.label,
        name: this.config.name,
        value: data.description,
        order: this.config.order
      }
      this.filterSelect.value = null;
      // this.config.value = '';
      this.addTag.emit(tag);
    }
  }
}
