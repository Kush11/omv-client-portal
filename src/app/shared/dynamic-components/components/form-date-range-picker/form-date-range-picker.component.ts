import { Component, EventEmitter, ViewChild } from '@angular/core';
import { Field } from '../../field.interface';
import { FieldConfiguration, FieldMode } from '../../field-setting';
import { FormGroup } from '@angular/forms';
import { DateService } from 'src/app/core/services/business/dates/date.service';
import { Tag } from 'src/app/core/models/entity/tag';
import { DateRangePicker, RangeEventArgs } from '@syncfusion/ej2-angular-calendars';

@Component({
  selector: 'app-form-date-range-picker',
  templateUrl: './form-date-range-picker.component.html',
  styleUrls: ['./form-date-range-picker.component.css']
})
export class FormDateRangePickerComponent implements Field {
  mode = FieldMode.Default;
  config: FieldConfiguration;
  group: FormGroup;
  allowDeleting: boolean;
  enableDeleteSetting: boolean;
  enableRequiredSetting: boolean;
  enableSortSetting: boolean;
  disabled: boolean;

  delete = new EventEmitter<any>();
  requiredToggle = new EventEmitter<any>();
  checkBoxChange = new EventEmitter<any>();
  labelChange = new EventEmitter<any>();
  revertLabel = new EventEmitter<any>();
  sort = new EventEmitter<any>();
  maxDate = new Date();
  addTag = new EventEmitter<Tag>();
  upload = new EventEmitter<any>();

  @ViewChild("dateRange") dateRange: DateRangePicker;

  constructor(private dateService: DateService) { }

  deleteEvent(config: any) {
    this.delete.emit(config);
  }

  requiredToggleEvent(config: any) {
    this.config.isRequired = !this.config.isRequired;
    this.requiredToggle.emit(config);
  }

  sortEvent(direction: string) {
    const config = { ...this.config, sortDirection: direction };
    this.sort.emit(config);
  }

  onUpload(config: any) {
    this.upload.emit(config);
  }

  requiredEvent() {
    this.config.required = !this.config.required;
    this.checkBoxChange.emit(this.config);
  }

  editableEvent() {
    this.config.editable = !this.config.editable;
    if (!this.config.editable) this.config.required = false;
    this.checkBoxChange.emit(this.config);
  }

  allowDocumentEvent() {
    this.config.allowDocumentUpload = !this.config.allowDocumentUpload;
    this.checkBoxChange.emit(this.config);
  }

  uniqueEvent() {
    this.config.unique = !this.config.unique;
    this.checkBoxChange.emit(this.config);
  }

  labelChangeEvent(data: FieldConfiguration) {
    this.labelChange.emit(data);
  }

  revertLabelEvent(field: FieldConfiguration) {
    this.revertLabel.emit(field);
  }

  onAddTag(event: RangeEventArgs) {
    if (event.startDate && event.endDate) {
      const startDate = this.dateService.formatToString(event.startDate, 'DD/MM/YYYY');
      const endDate = this.dateService.formatToString(event.endDate, 'DD/MM/YYYY');
      const tag: Tag = {
        label: this.config.label,
        name: this.config.name,
        value: `${startDate} - ${endDate}`
      }
      this.dateRange.value = null;
      this.addTag.emit(tag);
    }
  }
}
