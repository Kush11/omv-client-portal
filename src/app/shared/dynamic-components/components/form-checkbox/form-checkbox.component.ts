import { Component, OnInit, EventEmitter } from '@angular/core';
import { Field } from '../../field.interface';
import { FormGroup } from '@angular/forms';
import { FieldConfiguration, FieldMode } from '../../field-setting';

@Component({
  selector: 'app-form-checkbox',
  templateUrl: './form-checkbox.component.html',
  styleUrls: ['./form-checkbox.component.css']
})
export class FormCheckboxComponent implements Field {

  mode = FieldMode.Default;
  config: FieldConfiguration;
  group: FormGroup;
  disabled: boolean;
  change = new EventEmitter<any>();
  checkBoxChange = new EventEmitter<any>();
  
  constructor() { }

  changeEvent(event: any) {
    this.config.value = !this.config.value;
    this.checkBoxChange.emit(event);
  }
}
