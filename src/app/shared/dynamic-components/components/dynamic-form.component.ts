import { Component, EventEmitter, Input, OnChanges, OnInit, Output, ViewChild, AfterViewInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { FieldConfiguration, FieldMode, FieldType } from '../field-setting';
import { Tag } from 'src/app/core/models/entity/tag';
import { ModalConfirmComponent } from '../../modal/modal-confirm.component';
import { Subject } from 'rxjs/internal/Subject';

@Component({
  exportAs: "dynamicForm",
  selector: "dynamic-form",
  template: `
    <form class="row" [formGroup]="form" (submit)="handleSubmit($event)">  
      <div [className]="field?.cssClass ? field.cssClass : 'col-md-12'" *ngFor="let field of config;">
        <ng-container dynamicField 
          [mode]="mode"
          [config]="field" 
          [group]="form"
          [allowDeleting]="allowDeleting" 
          [allowUpload]="allowUpload"
          [enableDeleteSetting]="enableDeleteSetting"
          [enableRequiredSetting]="enableRequiredSetting"
          [enableSortSetting]="enableSortSetting"
          [disabled]="disabled"
          (addTag)="addTagEvent($event)"
          (delete)="showConfirmModal($event)" 
          (sort)="sortEvent($event)"
          (upload)="onUpload($event)"
          (change)="changeEvent($event)"
          (dropdownChange)="dropdownChangeEvent($event)"
          (checkBoxChange)="checkBoxChangeEvent($event)"
          (labelChange)="labelChangeEvent($event)"
          (revertLabel)="revertLabelEvent($event)"
          (requiredToggle)="requiredToggleEvent($event)">
        </ng-container>
      </div>
    </form>

    <!-- CONFIRM DELETE MODAL -->
    <app-modal-confirm #confirmModal title="Confirm Delete" [message]="deleteMessage" (yes)="deleteConfirmed()">
    </app-modal-confirm>
  `,
  styles: []
})
export class DynamicFormComponent implements OnChanges, OnInit, AfterViewInit {
  @Input() mode: FieldMode;
  @Input() config: FieldConfiguration[] = [];
  @Input() allowDeleting: boolean;
  @Input() allowUpload: boolean;
  @Input() enableDeleteSetting: boolean;
  @Input() enableRequiredSetting: boolean;
  @Input() enableSortSetting: boolean;
  @Input() disabled: boolean;

  @Output() submit = new EventEmitter<any>();
  @Output() delete = new EventEmitter<any>();
  @Output() dropdownChange = new EventEmitter<any>();
  @Output() addTag = new EventEmitter<Tag>();
  @Output() change = new EventEmitter<any>();
  @Output() checkBoxChange = new EventEmitter<any>();
  @Output() labelChange = new EventEmitter<any>();
  @Output() revertLabel = new EventEmitter<any>();
  @Output() sort = new EventEmitter<any>();
  @Output() upload = new EventEmitter<any>();

  formCreated: Subject<boolean> = new Subject();

  form = this.fb.group({});
  formattedMessage: string;

  get controls() { return this.config.filter(({ type }) => type !== 'button' && type !== 'header'); }
  get changes() { return this.form.valueChanges; }
  get statusChanges() { return this.form.statusChanges; }
  get dirty() { return this.form.dirty; }
  get touched() { return this.form.touched; }
  get pristine() { return this.form.pristine; }
  get valid() { return this.form.valid; }
  get value() { return this.form.value; }

  @ViewChild('confirmModal') confirmModal: ModalConfirmComponent;
  currentField: FieldConfiguration;
  deleteMessage: string;

  constructor(private fb: FormBuilder) { }

  ngOnInit() {
    this.form = this.createGroup();
  }

  ngOnChanges() {
    if (this.form) {
      const controls = Object.keys(this.form.controls);
      const configControls = this.controls.map((item) => item.name);

      controls
        .filter((control) => !configControls.includes(control))
        .forEach((control) => this.form.removeControl(control));

      configControls
        .filter((control) => !controls.includes(control))
        .forEach((name) => {
          const config = this.config.find((control) => control.name === name);
          this.form.addControl(name, this.createControl(config));
        });
    }
  }

  ngAfterViewInit() {
    console.log("this.form: ", this.form);
    this.formCreated.subscribe(data => {
      if (data) {
        this.form.valueChanges.subscribe(val => {
          this.formattedMessage = `My changed values for is ${val}.`;
          console.log(this.formattedMessage);
        });
      }
    });
  }

  createGroup() {
    const group = this.fb.group({});
    this.controls.forEach(control => group.addControl(control.name, this.createControl(control)));    
    this.formCreated.next(true);
    return group;
  }

  createControl(config: FieldConfiguration) {
    const { disabled, validations, value } = config;
    return this.fb.control({ value: value, disabled: disabled }, this.bindValidations(validations || []));
  }

  addControl(config: FieldConfiguration) {
    const control = this.createControl(config);
    this.form.addControl(config.name, control);
  }

  removeControl(name: string) {
    this.form.removeControl(name);
  }

  removeAllControls() {
    this.controls.forEach(control => {
      this.form.removeControl(control.name);
    });
  }

  handleSubmit(event: Event) {
    event.preventDefault();
    event.stopPropagation();
    if (this.form.valid) {
      this.submit.emit(this.value);
    } else {
      this.validateAllFormFields(this.form);
    }
  }

  setDisabled(name: string, disable: boolean) {
    if (this.form.controls[name]) {
      const method = disable ? 'disable' : 'enable';
      this.form.controls[name][method]();
      return;
    }

    this.config = this.config.map((item) => {
      if (item.name === name) {
        item.disabled = disable;
      }
      return item;
    });
  }

  setCustomDisable(name: string) {
    this.config = this.config.map((item) => {
      if (item.name === name) {
        item.disabled = true;
      }
      return item;
    });
  }

  enableAll() {
    this.config.forEach(item => item.disabled = false);
  }

  setValue(name: string, value: any, disable?: boolean) {
    this.form.controls[name].setValue(value, { emitEvent: true });
    if (disable) {
      this.setCustomDisable(name);
    }
  }

  reset(form?: FormGroup) {
    if (form) this.form.reset(this.form.value);
    else this.form.reset();
  }

  markAsDirty(name: string) {
    this.form.controls[name].markAsDirty();
  }

  markAsTouched(name: string) {
    this.form.controls[name].markAsTouched();
  }

  markAsTouchedAndDirty(name: string) {
    this.form.controls[name].markAsTouched();
    this.form.controls[name].markAsDirty();
  }

  markFormAsTouchedAndDirty() {
    this.form.markAsTouched();
    this.form.markAsDirty();
  }

  markFormAsUnTouchedAndPristine() {
    this.form.markAsUntouched();
    this.form.markAsPristine();
  }

  bindValidations(validations: any) {
    if (validations.length > 0) {
      const validList = [];
      validations.forEach(valid => {
        validList.push(valid.validator);
      });
      return Validators.compose(validList);
    }
    return null;
  }

  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      control.markAsTouched({ onlySelf: true });
    });
  }

  dropdownChangeEvent(config: any) {
    this.dropdownChange.emit(config);
  }

  addTagEvent(config: any) {
    this.addTag.emit(config);
  }

  changeEvent(field: FieldConfiguration) {
    //#region Nested Dropdown

    // AIM: To get the options of the dropdown(s) dependent on this field based on its selection
    if (field.type === FieldType.Select) {
      // Step 1: Get the selected option
      if (field.options) {
        const selectedOption = field.options.find(option => option.value === field.value);
        if (selectedOption) {
          // Step 2: Find the children of this parent
          const controls = this.controls.filter(f => f.data);
          const children = controls.filter(f => f.data.parentId === field.id);
          children.forEach(child => {
            if (!child.id) return;
            // Step 3: Get the child's options based on the parent's selected option
            this.controls.forEach(control => {
              if (control.id === child.id) {
                control.options = child.data.options.filter(x => x.parentId === selectedOption.id);
                control.isEnabled = true;
              }
            });
          });
        }
      }
    }

    //#endregion

    this.change.emit(field);
  }

  checkBoxChangeEvent(config: FieldConfiguration) {
    this.markAsTouchedAndDirty(config.name);
    this.checkBoxChange.emit(config);
  }

  labelChangeEvent(config: any) {
    this.labelChange.emit(config);
  }

  revertLabelEvent(field: FieldConfiguration) {
    this.revertLabel.emit(field);
  }

  sortEvent(config: any) {
    this.controls.forEach((field, index) => {
      const next = this.controls[index + 1];
      const previous = this.controls[index - 1];
      if (field.id === config.id) {
        if (config.sortDirection === 'up') {
          if (!previous) return; //If there is no previous, don't decrease order
          field.order--;
          previous.order++;
        } else {
          if (!next) return; // If there is no next, don't increase order
          field.order++;
          next.order--;
        }
        this.markAsTouchedAndDirty(field.name);
      }
    });
    this.config.sort((a, b) => a.order - b.order);
  }

  //#region Upload

  onUpload(config: any) {
    this.upload.emit(config);
  }

  //#endregion

  //#region Confirm Dialog

  showConfirmModal(config: FieldConfiguration) {
    this.currentField = config;

    if (this.currentField.type !== FieldType.Label) {
      const childField = this.controls.find(field => field.parentId === this.currentField.id);
      if (childField) {
        this.deleteMessage = `Are you sure you want to delete "${this.currentField.label}" and "${childField.label}"?`;
      } else {
        this.deleteMessage = `Are you sure you want to delete "${this.currentField.label}"?`;
      }
    } else {
      this.deleteMessage = `Are you sure you want to permanently delete "${this.currentField.label}"?`;
    }
    this.confirmModal.show();
  }

  deleteConfirmed() {
    // check if field has a child and delete the child
    if (this.currentField.type !== FieldType.Label) {
      let childField = this.controls.find(field => field.parentId === this.currentField.id);
      if (childField) this.deleteField(childField);
    }
    this.deleteField(this.currentField);
  }

  deleteField(field: FieldConfiguration) {
    this.delete.emit(field);
  }

  //#endregion

}
