import { Component } from '@angular/core';
import { Field } from '../../field.interface';
import { FieldMode, FieldConfiguration } from '../../field-setting';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-form-header',
  templateUrl: './form-header.component.html',
  styleUrls: ['./form-header.component.css']
})
export class FormHeaderComponent implements Field {
  
  mode = FieldMode.Default;
  config: FieldConfiguration;
  group: FormGroup;

}
