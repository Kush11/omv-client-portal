import { Component, EventEmitter, ViewChild } from '@angular/core';
import { Field } from '../../field.interface';
import { FieldConfiguration, FieldMode } from '../../field-setting';
import { FormGroup } from '@angular/forms';
import { Tag } from 'src/app/core/models/entity/tag';
import { ComboBoxComponent } from '@syncfusion/ej2-angular-dropdowns';
// import { FilteringEventArgs } from '@syncfusion/ej2-dropdowns';

@Component({
  selector: 'app-form-combobox',
  templateUrl: './form-combobox.component.html',
  styleUrls: ['./form-combobox.component.css']
})
export class FormComboboxComponent implements Field {
  mode = FieldMode.Default;
  config: FieldConfiguration;
  group: FormGroup;
  allowDeleting: boolean;
  allowUpload: boolean;
  enableSortSetting: boolean;
  enableDeleteSetting: boolean;
  enableRequiredSetting: boolean;
  disabled: boolean;

  fields: Object = { text: 'description', value: 'value' };
  delete = new EventEmitter<any>();
  requiredToggle = new EventEmitter<any>();
  change = new EventEmitter<any>();
  checkBoxChange = new EventEmitter<any>();
  uniqueChange = new EventEmitter<any>();
  labelChange = new EventEmitter<any>();
  revertLabel = new EventEmitter<any>();
  sort = new EventEmitter<any>();
  upload = new EventEmitter<any>();
  addTag = new EventEmitter<Tag>();


  @ViewChild("comboBox") comboBox: ComboBoxComponent;

  changeEvent(event: any) {
    this.change.emit(event);
  }

  deleteEvent(config: any) {
    this.delete.emit(config);
  }

  requiredToggleEvent(config: any) {
    this.config.isRequired = !this.config.isRequired;
    this.requiredToggle.emit(config);
  }

  requiredEvent() {
    this.config.required = !this.config.required;
    this.checkBoxChange.emit(this.config);
  }

  editableEvent() {
    this.config.editable = !this.config.editable;
    if (!this.config.editable) this.config.required = false;
    this.checkBoxChange.emit(this.config);
  }

  allowDocumentEvent() {
    this.config.allowDocumentUpload = !this.config.allowDocumentUpload;
    this.checkBoxChange.emit(this.config);
  }

  uniqueEvent() {
    this.config.unique = !this.config.unique;
    this.checkBoxChange.emit(this.config);
  }

  labelChangeEvent(data: FieldConfiguration) {
    this.labelChange.emit(data);
  }

  revertLabelEvent(field: FieldConfiguration) {
    console.log('select forms reverted...');
    this.revertLabel.emit(field);
  }

  sortEvent(direction: string) {
    const config = { ...this.config, sortDirection: direction };
    this.sort.emit(config);
  }

  onUpload(config: any) {
    this.upload.emit(config);
  }

  onAddTag(event: any) {
    const data = event.itemData;
    if (data) {
      if (!data.description) return;
      const options = this.config.options.map(o => o.description);
      if (!options.includes(data.description)) return;
      const tag: Tag = {
        label: this.config.label,
        name: this.config.name,
        value: data.description
      }
      this.comboBox.text = '';
      this.comboBox.value = '';
      // this.config.value = '';
      this.addTag.emit(tag);
      this.comboBox.refresh();
    }
  }

  data: { [key: string]: Object }[] = []

  onFiltering(e: any) {
    // this.data = this.config.options as any;
    // // frame the query based on search string with filter type.
    // if (e.text === "") {
    //   console.log("FormSelectComponent onFiltering if: ", e.text);
    //   e.updateData(this.data);
    // } else {
    //   console.log("FormSelectComponent onFiltering else: ", e.text);
    //   let query = new Query();
    //   query = query.where("description", "contains", e.text, true);
    //   e.updateData(this.data, query);
    // }
    // this.comboBox.refresh();
  }

}
