import { ValidatorFn } from '@angular/forms';
import { ListItem } from 'src/app/core/models/entity/list-item';
import { Lookup } from 'src/app/core/models/entity/lookup';
import { FilterDetail } from 'src/app/core/models/entity/filter';

export class FieldConfiguration {
  id?: any; // for folder metadata fields
  parentId?: number; // for nested dropdown
  data?: any; // for folder metadata fields
  cssClass?= 'col-md-12';
  disabled?: boolean;
  label?: string;
  overrideLabel?: string;
  inputType?: string;
  isChecked?: boolean;
  isRequired?: boolean;
  isSelected?: boolean;
  isNested?: boolean;
  isEnabled?: boolean; // for nested dropdown
  name: string;
  options?: ListItem[];
  facetResults?: FilterDetail[];
  optionsId?: number;
  order?: number;
  sortDirection?: string;
  placeholder?: string;
  type: string;
  rows?: number; // for textarea
  validations?: ValidatorFn[];
  value?: any;
  allowDateDisabling?: boolean;
  disabledDates?: string[];

  isDefault?: boolean;
  ignoreMaxDate?: boolean;

  isModal?: boolean;
  unique?: boolean;
  uniqueDisabled?: boolean;
  required?: boolean;
  editable?: boolean;
  allowDocumentUpload?: boolean;
  validationMessage?: string;
  dataSource?: any[]; // for modal dropdown
  fields?: object;
  action?: EventListener;
}

export class FieldValidator {
  name: string;
  validator: any;
  message: string;
}

export enum FieldType {
  Date = 'date',
  Time = 'time',
  DateRange = 'dateRange',
  DateTime = 'dateTime',
  Input = 'input',
  Select = 'select',
  Combobox = 'combobox',
  Header = 'header',
  Label = 'label',
  CheckBox = 'checkbox',
  Slider = 'slider'
}

export enum FieldMode {
  Default = "default",
  Filter = "filter",
  Modal = "modal",
  WorkPlanning = "work-planning"
}
