import { Component, OnInit, Output, Input, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-search-card',
  templateUrl: './search-card.component.html',
  styleUrls: ['./search-card.component.css']
})
export class SearchCardComponent implements OnInit {

  @Input() data: any;
  @Input() hideDate: boolean = false;


  @Output() toggleFavorite = new EventEmitter<any>();
  @Output() tileClick = new EventEmitter<any>();
  
  constructor() { }

  ngOnInit() { }

  toggleFavoriteEvent(data: any) {
    this.toggleFavorite.emit(data);
  }

  tileClickEvent(data: any) {
    
    this.tileClick.emit(data);
  }
}
