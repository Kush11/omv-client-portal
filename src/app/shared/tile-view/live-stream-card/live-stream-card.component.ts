import { Component, OnInit, Input, EventEmitter, Output, ViewChild } from '@angular/core';
import { Store } from '@ngxs/store';
import { BaseComponent } from '../../base/base.component';
import { ModalComponent } from '../../modal/modal.component';

@Component({
  selector: 'app-live-stream-card',
  templateUrl: './live-stream-card.component.html',
  styleUrls: ['./live-stream-card.component.css']
})
export class LiveStreamCardComponent extends BaseComponent implements OnInit {

  @Input() data: any;
  @Input() index: any;
  @Input() showLive: boolean;
  @Input() hasFavorite: boolean;
  @Input() hasPreview: boolean;

  @Output() toggleFavorite = new EventEmitter<any>();
  @Output() toggleCheckbox = new EventEmitter<any>();
  @Output() tileClick = new EventEmitter<any>();
  @Output() previewClick = new EventEmitter<any>();

  @ViewChild('videoModal') videoModal: ModalComponent;

  constructor(protected store: Store) {
    super(store);
  }

  toggleCheckboxEvent(data: any) {
    this.toggleCheckbox.emit(data);
  }

  toggleFavoriteEvent(data: any) {
    this.toggleFavorite.emit(data);
  }

  tileClickEvent(data: any) {
    this.tileClick.emit(data);
  }

  preview() {
    this.videoModal.setupWowzaPlayer();
    this.videoModal.show();
    // this.previewClick.emit(data);
  }
}
