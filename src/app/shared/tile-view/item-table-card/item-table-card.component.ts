import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-item-table-card',
  templateUrl: './item-table-card.component.html',
  styleUrls: ['./item-table-card.component.css']
})
export class ItemTableCardComponent implements OnInit {

  @Input() tableData: any;
  data = [];
  elementData: any;


  constructor() { }


  ngOnInit() {
    const data = this.tableData.cycles;

    data.forEach(element => {
      this.data = element.data;
    });
    console.log('data', this.data);
  }
}
