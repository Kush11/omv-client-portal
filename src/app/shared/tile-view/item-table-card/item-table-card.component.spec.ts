import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemTableCardComponent } from './item-table-card.component';

describe('ItemTableCardComponent', () => {
  let component: ItemTableCardComponent;
  let fixture: ComponentFixture<ItemTableCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ItemTableCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemTableCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
