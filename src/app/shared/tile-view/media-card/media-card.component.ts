import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { Store } from '@ngxs/store';
import { BaseComponent } from '../../base/base.component';

@Component({
  selector: 'app-media-card',
  templateUrl: './media-card.component.html',
  styleUrls: ['./media-card.component.css']
})
export class MediaCardComponent extends BaseComponent implements OnInit {
  public selectedItems: any[] = [];

  @Input() data: any;
  @Input() index;
  
  @Output() toggleFavorite = new EventEmitter<any>();
  @Output() toggleCheckbox = new EventEmitter<any>();
  @Output() tileClick = new EventEmitter<any>();

  constructor(protected store: Store) {
    super(store);
  }

  ngOnInit() {
  }

  toggleCheckboxEvent(data: any) {
    this.toggleCheckbox.emit(data);
  }

  toggleFavoriteEvent(data: any) {
    this.toggleFavorite.emit(data);
  }

  tileClickEvent(data: any) {
    this.tileClick.emit(data);
  }
}
