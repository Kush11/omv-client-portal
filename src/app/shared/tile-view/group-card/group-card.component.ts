import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { TooltipComponent } from '@syncfusion/ej2-angular-popups';

@Component({
  selector: 'app-group-card',
  templateUrl: './group-card.component.html',
  styleUrls: ['./group-card.component.css']
})
export class GroupCardComponent implements OnInit {

  @Input() data: any;
  @Input() sasToken: string;
  @Input() index: any;

  @Output() tileClick = new EventEmitter<any>();
  @Output() toggleCheckbox = new EventEmitter<any>();

  isFindingName: boolean;

  constructor() {
  }
  ngOnInit() {
    if(this.data.findingName) {
      this.isFindingName = true;
    } else {
      this.isFindingName = false;
    }
  }

  toggleCheckboxEvent(data: any) {
    this.toggleCheckbox.emit(data);
  }

  tileClickEvent(data: any) {
    this.tileClick.emit(data);
  }
}
