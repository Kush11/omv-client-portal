import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Type } from '@angular/core';

@Component({
  selector: 'app-participant-card',
  templateUrl: './participant-card.component.html',
  styleUrls: ['./participant-card.component.css']
})
export class ParticipantCardComponent implements OnInit {

  @Input() data: any;
  @Input() status: string;
  @Input() type: any;

  @Output() add = new EventEmitter<any>();
  @Output() remove = new EventEmitter<any>();

  constructor() { }

  ngOnInit() {
    // console.log('ParticipantCardComponent', this.data);
  }

  addParticipant(data: any) {
    this.add.emit(data);
  }

  removeParticipant(data: any) {
    this.remove.emit(data);
  }

}
