import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { BaseComponent } from '../base/base.component';
import { Store } from '@ngxs/store';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-tile-view',
  templateUrl: './tile-view.component.html',
  styleUrls: ['./tile-view.component.css']
})
export class TileViewComponent extends BaseComponent implements OnInit {
  public selectedItems: any[] = [];

  @Input() dataSource = [];
  @Input() sasToken: string;
  @Input() index = [];
  @Input() hasFavorite = true;
  @Input() showLive: boolean;
  @Input() hasPreview: boolean;
  @Input() status: string;
  @Input() isModal: false;
  @Input() isChart: boolean;
  @Output() tileClick = new EventEmitter<any>();
  @Output() setItemName = new EventEmitter<any>();
  @Output() toggleCheckBox = new EventEmitter<any>();
  @Output() toggleFavorite = new EventEmitter<any>();
  @Output() previewClick = new EventEmitter<any>();
  @Output() add = new EventEmitter<any>();
  @Output() edit = new EventEmitter<any>();
  @Output() join = new EventEmitter<any>();
  @Output() remove = new EventEmitter<any>();

  @Input() type: string;

  //pagination
  @Input() allowPagination: boolean;
  @Input() allowPaginationRouting = true;
  @Input() pageCount: number;
  @Input() pageSize: number;
  @Input() pageSizes: number[];
  @Input() currentPage: number;
  @Input() totalRecordsCount: number;
  @Input() showPagerDropdown: boolean;
  @Output() changePage = new EventEmitter<number>();
  @Output() pageSizeChanged = new EventEmitter<number>();
  @Output() cycleSelected = new EventEmitter<any>();

  constructor(protected store: Store, protected router: Router, protected route: ActivatedRoute) {
    super(store);
  }

  ngOnInit() {
    
  }

  toggleCheckboxEvent(data: any) {
    this.toggleCheckBox.emit(data);
  }

  toggleFavoriteEvent(data: any) {
    this.toggleFavorite.emit(data);
  }

  tileClickEvent(data: any) {
    this.tileClick.emit(data);
  }

  onCycleSelected(cycle: any) {
    this.cycleSelected.emit(cycle);
  }

  preview(data: any) {
    this.previewClick.emit(data);
  }

  addParticipant(data: any) {
    this.add.emit(data);
  }

  editSession(data: any) {
    this.edit.emit(data);
  }

  joinSession(data: any) {
    this.join.emit(data);
  }

  removeParticipant(data: any) {
    this.remove.emit(data);
  }

  removeCollaboration(data: any) {
    this.remove.emit(data);
  }

  setItemname(data: any) {
    this.setItemName.emit(data);
  }

  //Pagination
  onPageNumberChanged(pageNumber: number) {
    if (this.allowPaginationRouting) {
      this.router.navigate([], {
        relativeTo: this.route,
        queryParams: { pageNumber: pageNumber },
        queryParamsHandling: "merge"
      });
      this.changePage.emit(pageNumber);
      const element = document.getElementById("tile-container");
      if (element) {
        element.scrollIntoView({ behavior: 'smooth', block: 'start', inline: 'nearest' })
      }
    } else {
      this.changePage.emit(pageNumber);
    }
  }

  onPageSizeChanged(pageSize: number) {
    if (this.allowPaginationRouting) {
      this.router.navigate([], {
        relativeTo: this.route,
        queryParams: { pageNumber: 1, pageSize: pageSize },
        queryParamsHandling: "merge"
      });
      this.pageSizeChanged.emit(pageSize);
      const element = document.getElementById("tile-container");
      if (element) {
        element.scrollIntoView({ behavior: 'smooth', block: 'start', inline: 'nearest' })
      }
    } else {
      this.pageSizeChanged.emit(pageSize);
    }
  }
}
