import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import * as $ from 'jquery';

@Component({
  selector: 'app-doughnut-chart',
  templateUrl: './doughnut-chart.component.html',
  styleUrls: ['./doughnut-chart.component.css']
})
export class DoughnutChartComponent implements OnInit {

  @Input() data: any;
  @Input() cycleLength: number;
  @Input() link: string;

  @Output() cycleSelected = new EventEmitter<any>();

  constructor() { }

  ngOnInit() {
  }


  ngAfterViewInit() {
    $('.pie-wrapper').each(function () {
      const percent = parseInt($(this).data('percent'));
      const deg = 360 * percent / 100;
      const color = $(this).data('color');
      // styling
      $(this).find('.pie .half-circle').css('border-color', color);
      $(this).find('.pie .left-side').css('transform', 'rotate(' + deg + 'deg)');

      // check slicer
      if (percent > 50) {
        $(this).find('.pie .right-side').css('transform', 'rotate(' + 180 + 'deg)');
        $(this).find('.pie').css('clip', 'rect(auto, auto, auto, auto)');
      } else {
        $(this).find('.pie right-side').css('display', 'none');
      }
    });
  }

  onCycleClicked() {
    this.cycleSelected.emit(this.data);
  }
}
