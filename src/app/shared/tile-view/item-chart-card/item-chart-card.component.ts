import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { Store } from '@ngxs/store';
import { BaseComponent } from '../../base/base.component';

@Component({
  selector: 'app-item-chart-card',
  templateUrl: './item-chart-card.component.html',
  styleUrls: ['./item-chart-card.component.css']
})
export class ItemChartCardComponent extends BaseComponent implements OnInit {

  @Input() data: any;
  @Input() index;

  public cycleLength: number;
  name: string;

  @Output() tileClick = new EventEmitter<any>();
  @Output() cycleSelected = new EventEmitter<any>();

  constructor(protected store: Store) {
    super(store);
  }

  ngOnInit() {
    this.cycleLength = this.data.workSetPlanning.length;
  }

  onCycleSelected(cycle: any) {
    this.cycleSelected.emit(cycle);
  }
}
