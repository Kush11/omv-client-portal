import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ItemChartCardComponent } from './item-chart-card.component';

describe('ItemChartCardComponent', () => {
  let component: ItemChartCardComponent;
  let fixture: ComponentFixture<ItemChartCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ItemChartCardComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemChartCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
