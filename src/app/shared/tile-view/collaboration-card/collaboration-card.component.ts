import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-collaboration-card',
  templateUrl: './collaboration-card.component.html',
  styleUrls: ['./collaboration-card.component.css']
})
export class CollaborationCardComponent implements OnInit {

  @Input() data: any;
  @Output() add = new EventEmitter<any>();
  @Output() edit = new EventEmitter<any>();
  @Output() remove = new EventEmitter<any>();
  @Output() join = new EventEmitter<any>();
  @Output() tileClick = new EventEmitter<any>();

  constructor() { }

  ngOnInit() {
  }

  editSession(data) {
    this.edit.emit(data);
  }

  addParticipant(data) {
    this.add.emit(data);
  }

  removeCollaboration(data) {
    this.remove.emit(data);
  }

  joinSession(data: any) {
    this.join.emit(data);
  }

  tileClickEvent(data: any) {
    this.tileClick.emit(data);
  }
}
