import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CollaborationCardComponent } from './collaboration-card.component';

describe('CollaborationCardComponent', () => {
  let component: CollaborationCardComponent;
  let fixture: ComponentFixture<CollaborationCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CollaborationCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CollaborationCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
