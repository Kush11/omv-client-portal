import { NgxDocViewerModule } from 'ngx-doc-viewer';
import { ListComponent } from './list/list.component';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { TileViewComponent } from '../shared/tile-view/tile-view.component';
import { TreeViewComponent } from '../shared/tree-view/tree-view.component';
import { TreeGridComponent } from '../shared/tree-grid/tree-grid.component';
import { TabsComponent } from './tabs/tabs.component';
import { ModalComponent } from './modal/modal.component';
import { EditComponent } from './edit/edit.component';
import { AppHeaderComponent } from './app-header/app-header.component';
import { LeftNavComponent } from './leftnav/leftnav.component';
import { RouterModule } from '@angular/router';
import { BreadcrumbComponent } from './breadcrumb/breadcrumb.component';
import { MediaViewerComponent } from './media-viewer/media-viewer.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { FormSelectComponent } from './dynamic-components/components/form-select/form-select.component';
import { DynamicFormComponent } from './dynamic-components/components/dynamic-form.component';
import { DynamicFieldDirective } from './dynamic-components/directives/dynamic-field.directive';
import { FormLabelComponent } from './dynamic-components/components/form-label.component';
import { FileSizePipe } from '../core/pipes/file-size/file-size.pipe';
import { ImagePreloadDirective } from '../core/directives/image-preload/image-preload.directive';
import { TagComponent } from './tag/tag.component';
import { ViewsComponent } from './views/views.component';
import { SearchCardComponent } from './tile-view/search-card/search-card.component';
import { MediaCardComponent } from './tile-view/media-card/media-card.component';
import { FileNamePipe } from '../core/pipes/file-name/file-name.pipe';
import { FormInputComponent } from './dynamic-components/components/form-input/form-input.component';
import { PagerComponent } from './pager/pager.component';
import { ToolbarComponent } from './toolbar/toolbar.component';
import { MultiViewerComponent } from './multi-viewer/multi-viewer.component';
import { MinuteSecondsPipe } from '../core/pipes/minute-seconds/minute-seconds.pipe';
import { FooterComponent } from './footer/footer.component';
import { ButtonsComponent } from './buttons/buttons.component';
import { RichTextEditorComponent } from './rich-text-editor/rich-text-editor.component';


// SYNCFUSION MODULES
import { DialogModule } from '@syncfusion/ej2-angular-popups';
import { TooltipModule } from '@syncfusion/ej2-angular-popups';
import { PageService as TreeGridPageService, ContextMenuService, SortService as TreeGridSortService, TreeGridModule } from '@syncfusion/ej2-angular-treegrid';
import { ToolbarModule } from "@syncfusion/ej2-angular-navigations";
import { DatePickerModule } from '@syncfusion/ej2-angular-calendars';
import { DropDownListModule } from '@syncfusion/ej2-angular-dropdowns';
import { ComboBoxModule } from '@syncfusion/ej2-angular-dropdowns';
import { DateRangePickerModule } from '@syncfusion/ej2-angular-calendars';
import { TreeViewModule } from '@syncfusion/ej2-angular-navigations';
import { ListViewModule } from '@syncfusion/ej2-angular-lists';
import {
  PageService as GridPageService, SearchService as GridSearchService, GridModule, SortService as GridSortService,
  PagerModule, DetailRowService, ExcelExportService, ToolbarService as GridToolbarService, ResizeService, ReorderService
} from '@syncfusion/ej2-angular-grids';
import { ButtonModule } from '@syncfusion/ej2-angular-buttons';
import { CheckBoxModule } from '@syncfusion/ej2-angular-buttons';
import { RichTextEditorModule } from '@syncfusion/ej2-angular-richtexteditor';
import { FormHeaderComponent } from './dynamic-components/components/form-header/form-header.component';
import { ModalConfirmComponent } from './modal/modal-confirm.component';
import { FiltersComponent } from './filters/filters.component';
import { SwitchModule } from '@syncfusion/ej2-angular-buttons';
import { SideListMenuComponent } from './side-list-menu/side-list-menu.component';
import { ItemChartCardComponent } from './tile-view/item-chart-card/item-chart-card.component';
import { GroupCardComponent } from './tile-view/group-card/group-card.component';
import { FormCheckboxComponent } from './dynamic-components/components/form-checkbox/form-checkbox.component';
import { DoughnutChartComponent } from './tile-view/item-chart-card/doughnut-chart/doughnut-chart.component';
import { LiveStreamCardComponent } from './tile-view/live-stream-card/live-stream-card.component';
import { LiveStreamListComponent } from './live-stream-list/live-stream-list/live-stream-list.component';
import { FormTimePickerComponent } from './dynamic-components/components/form-time-picker/form-time-picker.component';
import { FormComboboxComponent } from './dynamic-components/components/form-combobox/form-combobox.component';
import { FormDatePickerComponent } from './dynamic-components/components/form-date-picker/form-date-picker.component';
import { TimePickerModule } from '@syncfusion/ej2-angular-calendars';
import { FormDateRangePickerComponent } from './dynamic-components/components/form-date-range-picker/form-date-range-picker.component';
import { FormDateTimePickerComponent } from './dynamic-components/components/form-date-time-picker/form-date-time-picker.component';
import { DateTimePickerModule } from '@syncfusion/ej2-angular-calendars';
import { SliderModule } from '@syncfusion/ej2-angular-inputs';
import { CollaborationCardComponent } from './tile-view/collaboration-card/collaboration-card.component';
import { ParticipantCardComponent } from './tile-view/participant-card/participant-card.component';
import { FacetsComponent } from './facets/facets.component';
import { ProfileViewComponent } from './profile-view/profile-view.component';
import { ArrayDisplayPipe } from '../core/pipes/array-display/array-display.pipe';
import { CheckboxComponent } from './checkbox/checkbox.component';
import { AvatarPipe } from '../core/pipes/avatar/avatar.pipe';
import { ItemTableCardComponent } from './tile-view/item-table-card/item-table-card.component';

@NgModule({
  declarations: [
    AppHeaderComponent,
    FooterComponent,
    BreadcrumbComponent,
    DynamicFormComponent,
    EditComponent,
    FormDatePickerComponent,
    FormDateRangePickerComponent,
    FormTimePickerComponent,
    FormDateTimePickerComponent,
    FormInputComponent,
    FormLabelComponent,
    FormHeaderComponent,
    FormComboboxComponent,
    FormSelectComponent,
    FormCheckboxComponent,
    MediaViewerComponent,
    ModalComponent,
    ModalConfirmComponent,
    PageNotFoundComponent,
    LeftNavComponent,
    ListComponent,
    FiltersComponent,
    TileViewComponent,
    MediaCardComponent,
    SearchCardComponent,
    TabsComponent,
    TagComponent,
    PagerComponent,
    ToolbarComponent,
    TreeViewComponent,
    TreeGridComponent,
    ViewsComponent,
    TreeGridComponent,
    DynamicFieldDirective,
    ImagePreloadDirective,
    MultiViewerComponent,
    ButtonsComponent,
    RichTextEditorComponent,
    FileSizePipe,
    FileNamePipe,
    MinuteSecondsPipe,
    GroupCardComponent,
    SideListMenuComponent,
    ItemChartCardComponent,
    DoughnutChartComponent,
    LiveStreamCardComponent,
    LiveStreamListComponent,
    CollaborationCardComponent,
    ParticipantCardComponent,
    FacetsComponent,
    ProfileViewComponent,
    ArrayDisplayPipe,
    CheckboxComponent,
    AvatarPipe,
    ItemTableCardComponent
  ],
  imports: [
    CommonModule,
    ListViewModule,
    ButtonModule,
    FormsModule,
    SwitchModule,
    CheckBoxModule,
    RouterModule,
    GridModule,
    DatePickerModule,
    DateRangePickerModule,
    DateTimePickerModule,
    TimePickerModule,
    DropDownListModule,
    ReactiveFormsModule,
    ComboBoxModule,
    ToolbarModule,
    DialogModule,
    TooltipModule,
    TreeViewModule,
    TreeGridModule,
    PagerModule,
    RichTextEditorModule,
    NgxDocViewerModule,
    SliderModule,
  ],
  exports: [
    FormsModule,
    CommonModule,
    AppHeaderComponent,
    FooterComponent,
    BreadcrumbComponent,
    DynamicFormComponent,
    EditComponent,
    FormDatePickerComponent,
    FormDateRangePickerComponent,
    FormTimePickerComponent,
    FormDateTimePickerComponent,
    FormInputComponent,
    FormLabelComponent,
    FormComboboxComponent,
    FormSelectComponent,
    FormHeaderComponent,
    FormCheckboxComponent,
    LeftNavComponent,
    ListComponent,
    // MapViewComponent,
    ModalComponent,
    PageNotFoundComponent,
    TabsComponent,
    TagComponent,
    TreeViewComponent,
    TreeGridComponent,
    TileViewComponent,
    MediaCardComponent,
    GroupCardComponent,
    SearchCardComponent,
    ViewsComponent,
    PagerComponent,
    ToolbarComponent,
    MultiViewerComponent,
    ButtonsComponent,
    RichTextEditorComponent,
    FiltersComponent,
    SideListMenuComponent,

    DynamicFieldDirective,
    ImagePreloadDirective,

    FileSizePipe,
    MinuteSecondsPipe,
    ItemChartCardComponent,
    LiveStreamListComponent,

    FacetsComponent,
    CollaborationCardComponent,
    ProfileViewComponent,
    LiveStreamCardComponent,
    ArrayDisplayPipe,
    CheckboxComponent,
    AvatarPipe
  ],
  providers: [
    TreeGridPageService,
    TreeGridSortService,
    ContextMenuService,
    GridPageService,
    GridSortService,
    GridToolbarService,
    GridSearchService,
    DetailRowService,
    ExcelExportService,
    ResizeService,
    ReorderService
  ],
  entryComponents: [
    FormDatePickerComponent,
    FormDateRangePickerComponent,
    FormDateTimePickerComponent,
    FormTimePickerComponent,
    FormInputComponent,
    FormLabelComponent,
    FormComboboxComponent,
    FormSelectComponent,
    FormHeaderComponent,
    FormCheckboxComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA]
})
export class SharedModule { }
