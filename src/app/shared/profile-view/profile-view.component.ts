import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { User } from 'src/app/core/models/entity/user';

@Component({
  selector: 'app-profile-view',
  templateUrl: './profile-view.component.html',
  styleUrls: ['./profile-view.component.css']
})
export class ProfileViewComponent implements OnInit {

  @Input() profiles: any[];
  @Input() type = 'default';

  @Output() select = new EventEmitter<User>();
  @Output() remove = new EventEmitter<User>();

  //pagination
  @Input() allowPagination: boolean;
  @Input() pageCount = 3;
  @Input() pageSize = 15;
  @Input() currentPage = 1;
  @Input() totalRecordsCount: number;
  @Output() changePage = new EventEmitter<number>();

  colors = ['#0B2545', '#8E5572', '#EFCB68', '#725AC1', '#EF233C', '#DE6B48', '#8DA9C4', '#EF8275', '#0CCA4A', '#F17300',
    '#054A91', '#3E5641', '#A24936', '#D36135', '#DD9AC2', '#8DA9C4', '#732C2C', '#7E1F86', '#54428E', '#75755D'];

  constructor() { }

  ngOnInit() {
  }

  onProfileSelected(profile: User) {
    this.select.emit(profile);
  }

  changePageEvent(pageNumber: number) {
    this.changePage.emit(pageNumber);
  }

  getAvatarColor(id: number): string {
    const randomNumber = Math.floor(Math.random() * (this.colors.length - 1) + 1);
    const index = id ? id % this.colors.length : randomNumber;
    return this.colors[index];
  }
}
