import { Component, OnInit, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-rich-text-editor',
  templateUrl: './rich-text-editor.component.html',
  styleUrls: ['./rich-text-editor.component.css']
})
export class RichTextEditorComponent implements OnInit {

  @Input() isInvalid: boolean;
  @Input() type = 'text';
  @Input() placeholder: string;
  @Input() formControlName: FormGroup;
  @Input() cssClass: string;

  constructor() { }

  ngOnInit() {
  }

}
