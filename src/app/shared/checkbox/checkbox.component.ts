import { Component, OnInit, Input, Output, EventEmitter, forwardRef } from '@angular/core';
import { FormControlName, FormGroup, NG_VALUE_ACCESSOR } from '@angular/forms';
import { BaseControlValueAccessor } from '../base-control-value-accessor';

@Component({
  selector: 'app-checkbox',
  templateUrl: './checkbox.component.html',
  styleUrls: ['./checkbox.component.css'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => CheckboxComponent),
      multi: true
    }
  ]
})
export class CheckboxComponent extends BaseControlValueAccessor<boolean> implements OnInit {

  @Input() checked: boolean;
  @Input() cssClass = 'c-small';
  @Input() disabled: boolean;

  @Output() change = new EventEmitter();

  constructor() {
    super();
  }

  ngOnInit() {
  }

  writeValue(value: any) {
    this.value = value;
    this.checked = this.value;
  }

  toggle() {
    this.value = !this.value; // comes from parent class
    this.onChange(this.value); // same
    this.checked = this.value;
  }

}
