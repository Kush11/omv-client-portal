import { Component, OnInit, Input, EventEmitter, Output, ViewChild, OnDestroy } from '@angular/core';
import { GridColumn } from 'src/app/core/models/grid.column';
import { TreeGridComponent as TreeGrid, RowExpandingEventArgs } from '@syncfusion/ej2-angular-treegrid';
import { RowSelectEventArgs, QueryCellInfoEventArgs, DataStateChangeEventArgs } from '@syncfusion/ej2-angular-grids';
declare var require: any;

@Component({
  selector: 'app-tree-grid',
  templateUrl: './tree-grid.component.html',
  styleUrls: ['./tree-grid.component.css']
})
export class TreeGridComponent implements OnInit, OnDestroy {

  @ViewChild("treegrid") treegrid: TreeGrid;

  @Input() dataSource: any;
  @Input() columns: GridColumn[];
  @Input() treeColumnIndex: number = 2;
  @Input() treeColumnName = 'name';
  @Input() idMapping: string;
  @Input() parentIdMapping: string;
  @Input() hasChildMapping: boolean;
  @Input() selectionOptions: Object;
  @Input() allowFavoriting: number;
  @Input() showFolderIcon: boolean;
  @Input() allowEditing: boolean;
  @Input() allowPaging: boolean;
  @Input() allowCheckboxSelection: boolean;
  @Input() allowDownloading: boolean;
  @Input() rowLinkIndex: number;

  @Output() toggleFavorite = new EventEmitter<any>();
  @Output() download = new EventEmitter<any>();
  @Output() edit = new EventEmitter<any>();
  @Output() navigate = new EventEmitter<any>();
  @Output() rowChecked = new EventEmitter<any>();
  @Output() rowSelected = new EventEmitter<any>();
  @Output() rowDeselected = new EventEmitter<any>();
  @Output() dataStateChange = new EventEmitter<DataStateChangeEventArgs>();
  @Output() collapsing = new EventEmitter<any>();

  checkboxTemplate = `<input class="c-checkbox-default" type="checkbox" title="" (click)="toggleFavoriteEvent(data)">`;
  notFavoriteTemplate = `<input class="c-checkbox-star" type="checkbox" title="" (click)="toggleFavoriteEvent(data)">`;
  favoriteTemplate = `<input class="c-checkbox-star" type="checkbox" title="" checked (click)="toggleFavoriteEvent(data)">`;
  downloadTemplate = `<button class="button-no-outline button-grid-link" (click)="downloadEvent(data)"> Download </button>`;
  editTemplate = `<img src="./assets/images/icon-pencil.svg" width="18" (click)="editEvent(data)">`;

  

  constructor() { }

  onDataStateChange(state: DataStateChangeEventArgs) {
    this.dataStateChange.emit(state);
  }

  ngOnInit() {
  }

  ngOnDestroy() {
  }

  toggleFavoriteEvent(data: any) {
    if (!data.documentId) return; // do not toggle favorite for folders
    data.isFavorite = !data.isFavorite;
    this.toggleFavorite.emit(data);
  }

  downloadEvent(data: any) {
    this.download.emit(data);
  }

  editEvent(data: any) {
    this.edit.emit(data);
  }

  rowCheckedEvent(data: any) {
    this.rowChecked.emit(data);
  }

  rowSelectedEvent(data: RowSelectEventArgs) {
    var rowSelectedEventClassList = require('classlist');
    if (!data.target) return;
    var list = new rowSelectedEventClassList(data.target);
    let isRowCollapsedOrExpanded = list.contains('expand') || list.contains('collapse') || list.contains('e-treegridcollapse') || list.contains('e-treegridexpand');
    if (isRowCollapsedOrExpanded) return;
    this.rowSelected.emit(data);
  }

  rowDeselectedEvent(data: RowSelectEventArgs) {
    var rowDeselectedEventClassList = require('classlist');
    if (!data.target) return;
    var list = new rowDeselectedEventClassList(data.target);
    let isRowCollapsedOrExpanded = list.contains('expand') || list.contains('collapse') || list.contains('e-treegridcollapse') || list.contains('e-treegridexpand');
    if (isRowCollapsedOrExpanded) return;
    this.rowDeselected.emit(data);
  }

  addFolderIcon(args: QueryCellInfoEventArgs) {
    if (args.column.field === this.treeColumnName) {
      if (args.data['isDirectory']) {
        let imgElement: HTMLElement = document.createElement('IMG');
        imgElement.setAttribute('src', './assets/images/icon-folder.svg');
        imgElement.classList.add('folder');
        let div: HTMLElement = document.createElement('DIV');
        div.style.display = 'inline-block';
        div.appendChild(imgElement);
        let cellValue: HTMLElement = document.createElement('DIV');
        if (args.cell.querySelector('.e-treecell')) {
          cellValue.innerHTML = args.cell.querySelector('.e-treecell').innerHTML;
          cellValue.setAttribute('style', 'display:inline-block;padding-left:6px');
          args.cell.querySelector('.e-treecell').innerHTML = '';
          args.cell.querySelector('.e-treecell').appendChild(div);
          args.cell.querySelector('.e-treecell').appendChild(cellValue);
        }
      } else {
        if (!args.data['routeLink']) {
          args.cell.addEventListener('click', this.editEvent.bind(this, args.data), false);
        } else {
          let linkElement: HTMLAnchorElement = document.createElement('a');
          linkElement.setAttribute('href', args.data["routeLink"]);
          linkElement.innerHTML = args.data["name"];
          linkElement.classList.add('grid-link');
          if (args.cell.querySelector('.e-treecell')) {
            args.cell.querySelector('.e-treecell').innerHTML = '';
            args.cell.querySelector('.e-treecell').appendChild(linkElement);
            args.cell.addEventListener('click', this.editEvent.bind(this, args.data), false);
            args.cell.addEventListener("click", function (event) {
              event.preventDefault();
            });
          }
        }
      }
    } else if (args.column.field === 'checkboxTemplate') {
      if (!args.data['isDirectory']) {
        args.cell.innerHTML = this.checkboxTemplate;
        args.cell.addEventListener('click', this.rowCheckedEvent.bind(this, args.data), false);
      }
    } else if (args.column.field === 'favoriteTemplate') {
      if (!args.data['isDirectory']) {
        const isFavorite = args.data['isFavorite'];
        args.cell.innerHTML = isFavorite ? this.favoriteTemplate : this.notFavoriteTemplate;
        args.cell.addEventListener('click', this.toggleFavoriteEvent.bind(this, args.data), false);
      }
    } else if (args.column.field === 'downloadTemplate') {
      if (!args.data['isDirectory']) {
        args.cell.innerHTML = this.downloadTemplate;
        args.cell.addEventListener('click', this.downloadEvent.bind(this, args.data), false);
      }
    } else if (args.column.field === 'editTemplate') {
      if (!args.data['isDirectory']) {
        args.cell.innerHTML = this.editTemplate;
        args.cell.addEventListener('click', this.editEvent.bind(this, args.data), false);
      }
    }
  }

  showSpinner() {
    if (this.treegrid)
      this.treegrid.showSpinner();
  }

  hideSpinner() {
    if (this.treegrid)
      this.treegrid.hideSpinner();
  }
}
