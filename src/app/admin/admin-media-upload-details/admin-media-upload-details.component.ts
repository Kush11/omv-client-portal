import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { BaseComponent } from 'src/app/shared/base/base.component';
import { Store, Select } from '@ngxs/store';
import { DynamicFormComponent } from 'src/app/shared/dynamic-components/components/dynamic-form.component';
import { GetUploadDetail } from '../state/admin-media/admin-media.action';
import { AdminMediaState } from '../state/admin-media/admin-media.state';
import { ActivatedRoute, Router } from '@angular/router';
import { UploadRequest } from 'src/app/core/models/entity/upload-request';
import { Breadcrumb } from 'src/app/core/models/breadcrumb';
import { AppState } from 'src/app/state/app.state';
import { SubSink } from 'subsink/dist/subsink';
import { Observable } from 'rxjs/internal/Observable';

@Component({
  selector: 'app-admin-media-upload-details',
  templateUrl: './admin-media-upload-details.component.html',
  styleUrls: ['./admin-media-upload-details.component.css']
})
export class AdminMediaUploadDetailsComponent extends BaseComponent implements OnInit, OnDestroy {

  private subs = new SubSink();

  @ViewChild(DynamicFormComponent) dynamicForm: DynamicFormComponent;
  @Select(AdminMediaState.getCurrentUpload) currentUpload$: Observable<any>;
  @Select(AppState.getPreviousRoute) previousRoute$: Observable<string>;

  upload: UploadRequest;
  id: number;
  previousRoute: string;

  mediaBreadcrumbs: Breadcrumb[] = [
    { link: "/admin/media/uploads/new" },
  ];

  constructor(protected store: Store, private activatedRoute: ActivatedRoute, private router: Router) {
    super(store);
    this.hideLeftNav();
  }

  ngOnInit() {
    this.subs.add(
      this.activatedRoute.paramMap
        .subscribe(params => {
          this.id = Number(params.get('id'));
          this.store.dispatch(new GetUploadDetail(this.id));
          this.subs.sink = this.currentUpload$
            .subscribe(upload => {
              if (upload) {
                this.upload = upload;
              }
            });
        }),
      this.previousRoute$
        .subscribe(route => {
          const default_route = "/admin/media/uploads"; // in case a user gets to this page other than by routing from the previous page
          if (route.includes(default_route)) {
            this.previousRoute = route;
          } else {
            this.previousRoute = default_route;
          }
        })
    );
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

  onBack(route: string) {
    this.router.navigate([route]);
  }
}
