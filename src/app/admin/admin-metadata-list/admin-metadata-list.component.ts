import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { ListComponent } from 'src/app/shared/list/list.component';
import { Store, Select } from '@ngxs/store';
import { GridColumn } from 'src/app/core/models/grid.column';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MetadataList } from 'src/app/core/models/entity/metadata-list';
import { DialogComponent } from '@syncfusion/ej2-angular-popups';
import { AdminMetadaListType } from 'src/app/core/enum/admin-user-type';
import { ActivatedRoute, Router } from '@angular/router';
import { SetPreviousRoute } from 'src/app/state/app.actions';
import { ToolBar } from 'src/app/core/models/toolbar-action';
import { SubSink } from 'subsink/dist/subsink';
import { Observable } from 'rxjs/internal/Observable';
import { AdminMetadataState } from '../state/admin-metadata/admin-metadata.state';
import { CreateMetadataList, EnableMetadataLists, DisableMetadataLists, GetActiveMetadataLists, GetDisabledMetadataLists } from '../state/admin-metadata/admin-metadata.action';
import { Button } from 'src/app/core/models/button';

const ENABLE = 'Enable';
const DISABLE = 'Disable';
const ACTIVE_LIST = 0;
const DISABLED_LIST = 1;

@Component({
  selector: 'app-admin-metadata-list',
  templateUrl: './admin-metadata-list.component.html',
  styleUrls: ['./admin-metadata-list.component.css']
})
export class AdminMetadataListComponent extends ListComponent implements OnInit, OnDestroy {

  private subs = new SubSink();
  metadataListFields: Object = { text: 'name', value: 'id' };
  fieldName: string = '';
  fieldType: string = '';
  fieldId: number;
  metadataListForm: FormGroup;
  metadataList = new MetadataList();
  statusChange: string;
  urlparam: string;
  componentActive: boolean = false;
  selectedListId: any;
  showAllListDropdown: boolean;
  selectedList: MetadataList[] = [];
  status: string;
  columns: GridColumn[] = [
    { headerText: ' ', type: 'checkbox', width: '50', field: '' },
    { headerText: 'Name', field: 'name', useAsLink: true, action: this.userPermission.canEditMetadata ? this.editEvent.bind(this) : '' },
    { headerText: 'Status', width: '', field: 'statusName', ignoreFiltering: true, ignoreSorting: true }
  ];
  toolbar: ToolBar[] = [
    { text: '', disabled: this.selectedList.length < 1, action: this.changeStatus.bind(this) }
  ];
  buttons: Button[] = [
    { text: "SAVE CHANGES", type: 'submit', cssClass: "button-round save-changes-qa", disabled: true },
    { text: "Cancel", type: 'button', cssClass: "button-no-outline cancel-qa", action: this.closeDialog.bind(this) },
  ];
  currentTab = ACTIVE_LIST;
  metadataLists: MetadataList[];
  selectedListIds: number[] = [];

  @ViewChild('listview') dialogList: any;
  @ViewChild('listDialog') listDialogList: DialogComponent;
  @ViewChild('metadataListView') metadataListView: ListComponent;

  @Select(AdminMetadataState.getActiveMetadataLists) activeMetadataList$: Observable<MetadataList[]>;
  @Select(AdminMetadataState.getDisabledMetadataLists) disableMetadataList$: Observable<MetadataList[]>;
  @Select(AdminMetadataState.getTotalListCount) totalListCount$: Observable<number>;
  @Select(AdminMetadataState.getIsProcessComplete) isProcessComplete$: Observable<boolean>;

  constructor(protected store: Store, private activatedRoute: ActivatedRoute, protected router: Router,
    private formBuilder: FormBuilder, ) {
    super(store);
    this.showLefNav();
    this.metadataListForm = this.formBuilder.group({
      id: [],
      fieldName: ['', [Validators.required]],
      status: [],
      parentListId: ['']
    });
  }

  ngOnInit() {
    this.subs.add(
      this.activatedRoute.params
        .subscribe(params => {
          this.displayList(params.type);
        }),
      this.activeMetadataList$
        .subscribe(activeLists => (this.metadataLists = activeLists)),
      this.disableMetadataList$
        .subscribe(disabledLists => (this.metadataLists = disabledLists)),
      this.isProcessComplete$
        .subscribe(isComplete => {
          if (isComplete) this.getMetadataLists();
        }),
      this.metadataListForm.valueChanges
        .subscribe(() => this.buttons[0].disabled = !this.metadataListForm.valid || !this.metadataListForm.dirty)
    );
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

  displayList(param: string) {
    this.urlparam = param;
    switch (param) {
      case AdminMetadaListType.Active:
        this.store.dispatch(new GetActiveMetadataLists());
        this.currentTab = ACTIVE_LIST;
        this.setToobarProperties();
        break;
      case AdminMetadaListType.Disabled:
        this.store.dispatch(new GetDisabledMetadataLists());
        this.currentTab = DISABLED_LIST;
        this.setToobarProperties();
        break;
    }
  }

  checkValue(event: any) {
    if (event.checked) this.showAllListDropdown = true;
    else this.showAllListDropdown = false;
  }

  getMetadataLists() {
    if (this.currentTab === ACTIVE_LIST) {
      this.store.dispatch(new GetActiveMetadataLists());
    } else {
      this.store.dispatch(new GetDisabledMetadataLists());
    }
  }

  changeStatus() {
    if (this.currentTab === ACTIVE_LIST) {
      this.store.dispatch(new DisableMetadataLists(this.selectedList)).toPromise()
        .then(() => {
          this.selectedList = [];
          this.setToobarProperties();
        });
    } else {
      this.store.dispatch(new EnableMetadataLists(this.selectedList)).toPromise()
        .then(() => {
          this.selectedList = [];
          this.setToobarProperties();
        });
    }
    this.metadataListView.clearSelection();
  }

  rowSelectedEvent(lists: MetadataList[]) {
    lists.forEach(list => this.addList(list));
    this.setToobarProperties();
  }

  rowDeselectedEvent(lists: MetadataList[]) {
    lists.forEach(list => this.removeList(list));
    this.setToobarProperties();
  }

  private addList(list: MetadataList) {
    const selectedListIds = this.selectedList.map(x => x.id);
    if (!selectedListIds.includes(list.id)) {
      this.selectedList = [
        ...this.selectedList,
        list
      ];
    }
  }

  private removeList(list: MetadataList) {
    const selectedListIds = this.selectedList.map(x => x.id);
    if (selectedListIds.includes(list.id)) {
      this.selectedList = this.selectedList.filter(x => x.id !== list.id);
    }
  }

  editEvent(data?: MetadataList) {
    const route = this.router.url;
    this.store.dispatch(new SetPreviousRoute(route));

    if (!data) this.router.navigate([`admin/metadata-list/0/edit`]);
    else this.router.navigate([`admin/metadata-list/${data.id}/edit`]);
  }

  add() {
    this.listDialogList.show();
  }

  closeDialog() {
    this.listDialogList.hide();
  }

  saveMetadataList() {
    if (this.metadataListForm.valid) {
      if (this.metadataListForm.dirty) {
        const metadataList: MetadataList = { ...this.metadataList, ...this.metadataListForm.value };
        this.store.dispatch(new CreateMetadataList(metadataList));
        this.closeDialog();
        this.metadataListForm.reset();
      }
    }
  }

  private setToobarProperties() {
    this.selectedListIds = this.selectedList.map(x => x.id);
    this.toolbar.map(x => {
      x.text = this.currentTab === ACTIVE_LIST ? DISABLE : ENABLE;
      x.disabled = this.selectedList.length === 0;
    });
    console.log(this.selectedList);
  }
}

