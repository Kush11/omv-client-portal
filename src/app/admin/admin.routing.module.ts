import { AdminMediaUploadsListComponent } from './admin-media-uploads-list/admin-media-uploads-list.component';
import { AdminGroupsTabsComponent } from './admin-groups-list/admin-groups-tabs/admin-groups-tabs.component';
import { AdminUsersTabsComponent } from './admin-users-list/admin-users-tabs/admin-users-tabs.component';
import { AdminUserEditComponent } from './admin-user-edit/admin-user-edit.component';
import { AdminDashboardComponent } from './admin-dashboard/admin-dashboard.component';
import { Routes, RouterModule } from '@angular/router';
import { AdminUsersListComponent } from './admin-users-list/admin-users-list.component';
import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { AdminGroupsListComponent } from './admin-groups-list/admin-groups-list.component';
import { AdminGroupEditComponent } from './admin-group-edit/admin-group-edit.component';
import { AdminGroupEditGuard } from './admin-group-edit/admin-group-edit.guard';
import { AdminUserEditGuard } from './admin-user-edit/admin-user-edit.guard';
import { AdminMetadataFieldsComponent } from './admin-metadata-fields/admin-metadata-fields.component';
import { AdminMediaNewUploadsComponent } from './admin-media-uploads-list/admin-media-new-uploads/admin-media-new-uploads.component';
import { AdminMediaUploadsHistoryComponent } from './admin-media-uploads-list/admin-media-uploads-history/admin-media-uploads-history.component';
import { AdminMetadataListComponent } from './admin-metadata-list/admin-metadata-list.component';
import { AdminMediaUploadDetailsComponent } from './admin-media-upload-details/admin-media-upload-details.component';
import { AdminMetadataListTabComponent } from './admin-metadata-list/admin-metadata-list-tab/admin-metadata-list-tab.component';
import { AdminMetadataListEditComponent } from './admin-metadata-list-edit/admin-metadata-list-edit.component';
import { AdminFolderStructureComponent } from './admin-folder-structure/admin-folder-structure.component';
import { AdminFolderMetadataComponent } from './admin-folder-metadata/admin-folder-metadata.component';
import { AdminFolderMetadataGuard } from './admin-folder-metadata/admin-folder-metadata.guard';
import { AdminProcessingRulesListComponent } from './admin-processing-rules-list/admin-processing-rules-list.component';
import { AdminProcessingRuleEditGuard } from './admin-processing-rule-edit/admin-processing-rule-edit.guard';
import { AdminProcessingRuleEditComponent } from './admin-processing-rule-edit/admin-processing-rule-edit.component';
import { AdminEmailTemplateEditGuard } from './admin-email-template-edit/admin-email-template-edit.guard';
import { AdminEmailTemplateEditComponent } from './admin-email-template-edit/admin-email-template-edit.component';
import { AdminEmailTemplatesListComponent } from './admin-email-templates-list/admin-email-templates-list.component';
import { AdminWorkTemplatesListComponent } from './admin-work-templates-list/admin-work-templates-list.component';
import { AdminWorkTemplateEditComponent } from './admin-work-template-edit/admin-work-template-edit.component';
import { AuthGuard } from '../core/guards/auth-guard.service';
import { AdminWorkTemplateGeneralInfoComponent } from './admin-work-template-edit/admin-work-template-general-info/admin-work-template-general-info.component';
import { AdminWorkTemplateProcessComponent } from './admin-work-template-edit/admin-work-template-process/admin-work-template-process.component';
import { AdminWorkTemplateFindingsComponent } from './admin-work-template-edit/admin-work-template-findings/admin-work-template-findings.component';
import { AdminWorkTemplateVersionsComponent } from './admin-work-template-edit/admin-work-template-versions/admin-work-template-versions.component';
import { AdminWorkTemplateHistoryComponent } from './admin-work-template-edit/admin-work-template-history/admin-work-template-history.component';
import { AdminWorkTemplateFormsComponent } from './admin-work-template-edit/admin-work-template-forms/admin-work-template-forms.component';
import { AdminWorkTemplateStepsComponent } from './admin-work-template-edit/admin-work-template-steps/admin-work-template-steps.component';
import { AdminBulkUploaderDownloaderComponentComponent } from './admin-bulk-uploader-downloader-component/admin-bulk-uploader-downloader-component.component';
import { AdminWorkSetsListComponent } from './admin-work-sets-list/admin-work-sets-list.component';
import { AdminWorkSetEditComponent } from './admin-work-set-edit/admin-work-set-edit.component';
import { AdminWorkSetGeneralInfoComponent } from './admin-work-set-edit/admin-work-set-general-info/admin-work-set-general-info.component';
import { AdminWorkSetScheduleComponent } from './admin-work-set-edit/admin-work-set-schedule/admin-work-set-schedule.component';
import { AdminWorkSetWorkItemsComponent } from './admin-work-set-edit/admin-work-set-work-items/admin-work-set-work-items.component';
import { AdminWorkSetHistoryComponent } from './admin-work-set-edit/admin-work-set-history/admin-work-set-history.component';
import { AdminWorkTemplateFieldsComponent } from './admin-work-template-edit/admin-work-template-fields/admin-work-template-fields.component';
import { AdminWorkSetCyclesComponent } from './admin-work-set-edit/admin-work-set-cycles/admin-work-set-cycles.component';
import { AdminWorkTemplateFormsGuard } from './admin-work-template-edit/admin-work-template-forms/admin-work-template-forms.guard';
import { AdminWorkSetGeneralInfoGuard } from './admin-work-set-edit/admin-work-set-general-info/admin-work-set-general-info.guard';
import { AdminWorkSetScheduleGuard } from './admin-work-set-edit/admin-work-set-schedule/admin-work-set-schedule.guard';
import { AdminWorkTemplateFindingsGuard } from './admin-work-template-edit/admin-work-template-findings/admin-work-template-findings.guard';
import { AdminMediaUploadsInprogressComponent } from './admin-media-uploads-list/admin-media-uploads-inprogress/admin-media-uploads-inprogress.component';
import { AdminWorkTemplateFieldsGuard } from './admin-work-template-edit/admin-work-template-fields/admin-work-template-fields.guard';
import { AdminWorkTemplateStepsGuard } from './admin-work-template-edit/admin-work-template-steps/admin-work-template-steps.guard';
import { AdminWorkTemplateGeneralInfoGuard } from './admin-work-template-edit/admin-work-template-general-info/admin-work-template-general-info.guard';

const adminRoutes: Routes = [
  {
    path: '',
    component: AdminDashboardComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'dashboard',
    component: AdminDashboardComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'media/uploads',
    component: AdminMediaUploadsListComponent,
    children: [
      { path: '', redirectTo: 'new', pathMatch: 'full' },
      { path: 'new', component: AdminMediaNewUploadsComponent },
      { path: 'in-progress', component: AdminMediaUploadsInprogressComponent },
      { path: 'history', component: AdminMediaUploadsHistoryComponent }
    ],
    canActivate: [AuthGuard]
  },
  {
    path: 'media/uploads/:id/details',
    component: AdminMediaUploadDetailsComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'users',
    component: AdminUsersTabsComponent,
    children: [
      { path: '', redirectTo: 'active', pathMatch: 'full' },
      { path: ':type', component: AdminUsersListComponent }
    ],
    canActivate: [AuthGuard]
  },
  {
    path: 'users/:id/edit',
    canDeactivate: [AdminUserEditGuard],
    component: AdminUserEditComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'groups',
    component: AdminGroupsTabsComponent,
    canActivate: [AuthGuard],
    children: [
      { path: '', redirectTo: 'active', pathMatch: 'full' },
      { path: ':type', component: AdminGroupsListComponent }
    ]
  },
  {
    path: 'groups/:id/edit',
    canDeactivate: [AdminGroupEditGuard],
    component: AdminGroupEditComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'metadata-fields/:type',
    component: AdminMetadataFieldsComponent,
    canActivate: [AuthGuard],
    children: [
      { path: ':type', component: AdminMetadataFieldsComponent }
    ]
  },
  {
    path: 'metadata-list/:id/edit',
    component: AdminMetadataListEditComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'metadata-list',
    component: AdminMetadataListTabComponent,
    canActivate: [AuthGuard],
    children: [
      { path: '', redirectTo: 'active', pathMatch: 'full' },
      { path: ':type', component: AdminMetadataListComponent, canActivate: [AuthGuard] }
    ]
  },
  {
    path: 'media/folders',
    component: AdminFolderStructureComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'media/folders/:id/metadata',
    canActivate: [AuthGuard],
    canDeactivate: [AdminFolderMetadataGuard],
    component: AdminFolderMetadataComponent
  },
  {
    path: 'media/rules',
    component: AdminProcessingRulesListComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'media/rules/:id/edit',
    canActivate: [AuthGuard],
    canDeactivate: [AdminProcessingRuleEditGuard],
    component: AdminProcessingRuleEditComponent
  },
  {
    path: 'media/bulk-uploader',
    component: AdminBulkUploaderDownloaderComponentComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'email-templates',
    component: AdminEmailTemplatesListComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'email-templates/:id/edit',
    canActivate: [AuthGuard],
    canDeactivate: [AdminEmailTemplateEditGuard],
    component: AdminEmailTemplateEditComponent,
  },
  {
    path: 'work-templates',
    component: AdminWorkTemplatesListComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'work-templates/:id',
    canActivate: [AuthGuard],
    component: AdminWorkTemplateEditComponent,
    children: [
      { path: '', redirectTo: 'general-info', pathMatch: 'full' },
      { path: 'general-info', component: AdminWorkTemplateGeneralInfoComponent, canActivate: [AuthGuard], canDeactivate: [AdminWorkTemplateGeneralInfoGuard] },
      { path: 'fields', component: AdminWorkTemplateFieldsComponent, canActivate: [AuthGuard], canDeactivate: [AdminWorkTemplateFieldsGuard] },
      { path: 'process', component: AdminWorkTemplateProcessComponent, canActivate: [AuthGuard] },
      { path: 'steps', component: AdminWorkTemplateStepsComponent, canActivate: [AuthGuard], canDeactivate: [AdminWorkTemplateStepsGuard] },
      { path: 'forms', component: AdminWorkTemplateFormsComponent, canActivate: [AuthGuard], canDeactivate: [AdminWorkTemplateFormsGuard] },
      { path: 'findings', component: AdminWorkTemplateFindingsComponent, canActivate: [AuthGuard], canDeactivate: [AdminWorkTemplateFindingsGuard] },
      { path: 'versions', component: AdminWorkTemplateVersionsComponent, canActivate: [AuthGuard] },
      { path: 'history', component: AdminWorkTemplateHistoryComponent, canActivate: [AuthGuard] },
    ],
  },
  {
    path: 'work-sets',
    component: AdminWorkSetsListComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'work-sets/:id',
    component: AdminWorkSetEditComponent,
    canActivate: [AuthGuard],
    children: [
      { path: '', redirectTo: 'general-info', pathMatch: 'full' },
      { path: 'general-info', component: AdminWorkSetGeneralInfoComponent, canActivate: [AuthGuard], canDeactivate: [AdminWorkSetGeneralInfoGuard] },
      { path: 'schedule', component: AdminWorkSetScheduleComponent, canActivate: [AuthGuard], canDeactivate: [AdminWorkSetScheduleGuard] },
      { path: 'cycles', component: AdminWorkSetCyclesComponent, canActivate: [AuthGuard] },
      { path: 'work-items', component: AdminWorkSetWorkItemsComponent, canActivate: [AuthGuard] },
      { path: 'history', component: AdminWorkSetHistoryComponent, canActivate: [AuthGuard] },
    ],
  }
];

@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(adminRoutes)
  ],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
