import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminEmailTemplatesListComponent } from './admin-email-templates-list.component';

describe('AdminEmailTemplatesListComponent', () => {
  let component: AdminEmailTemplatesListComponent;
  let fixture: ComponentFixture<AdminEmailTemplatesListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminEmailTemplatesListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminEmailTemplatesListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
