import { Component, OnInit, ViewChild } from '@angular/core';
import { GetEmailTemplates, RemoveEmailTemplate } from '../state/admin-email-templates/admin-email-templates.action';
import { Select, Store } from '@ngxs/store';
import { AdminEmailTemplatesState } from '../state/admin-email-templates/admin-email-templates.state';
import { EmailTemplate } from 'src/app/core/models/entity/email-templates';
import { FormGroup } from '@angular/forms';
import { GridColumn, GridColumnButton } from 'src/app/core/models/grid.column';
import { Router } from '@angular/router';
import { SetPreviousRoute, ShowSpinner } from 'src/app/state/app.actions';
import { Observable } from 'rxjs/internal/Observable';
import { ListComponent } from 'src/app/shared/list/list.component';
import { ModalComponent } from 'src/app/shared/modal/modal.component';

@Component({
  selector: 'app-admin-email-templates-list',
  templateUrl: './admin-email-templates-list.component.html',
  styleUrls: ['./admin-email-templates-list.component.css']
})
export class AdminEmailTemplatesListComponent extends ListComponent implements OnInit {

  @Select(AdminEmailTemplatesState.getEmailTemplates) emailTemplates$: Observable<EmailTemplate[]>;
  @ViewChild('confirmModal') confirmModal: ModalComponent;

  emailTemplateForm: FormGroup;
  deleteMessage: string;
  emailTemplate = new EmailTemplate();
  actions: GridColumnButton[] = [
    { type: 'button', text: 'Remove', query: 'canDelete', action: this.showConfirmDialog.bind(this), cssClass: 'button-no-outline button-grid-link remove-button-qa' },
    { type: 'button', text: 'Edit', visible: true, action: this.editTemplate.bind(this), cssClass: 'button-no-outline button-grid-link edit-button-qa' }
  ];
  columns: GridColumn[] = [
    { headerText: 'Name', field: 'name', useAsLink: true, action: this.editTemplate.bind(this) },
    { headerText: 'Added By', field: 'createdBy' },
    { headerText: 'Last Modified', field: 'modifiedOnString' },
    { headerText: 'Actions', type: 'buttons', buttons: this.actions, width: 200 }
  ];
  selectedEmailTemplateId: any;

  constructor(protected store: Store, protected router: Router, ) {
    super(store);
    this.showLefNav();
  }

  ngOnInit() {
    this.store.dispatch(new GetEmailTemplates());
  }

  addNewTemplate() {
    const route = this.router.url;
    this.store.dispatch(new SetPreviousRoute(route));
    this.router.navigate([`admin/email-templates/0/edit`]);
  }

  editTemplate(data?: EmailTemplate) {
    const route = this.router.url;
    this.store.dispatch(new SetPreviousRoute(route));
    this.router.navigate([`admin/email-templates/${data.id.trim()}/edit`]);
  }

  showConfirmDialog(data) {
    this.selectedEmailTemplateId = data.id;
    this.deleteMessage = `Are you sure you want to delete ${data.name}?`;
    this.confirmModal.show();
  }

  deleteField() {
    this.store.dispatch(new ShowSpinner());
    this.store.dispatch(new RemoveEmailTemplate(this.selectedEmailTemplateId));
  }
}
