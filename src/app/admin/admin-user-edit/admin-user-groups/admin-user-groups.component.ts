import { ClearUserGroups } from './../../state/admin-users/admin-users.actions';
import { GridColumn } from './../../../core/models/grid.column';
import { Group } from '../../../core/models/entity/group';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Store, Select } from '@ngxs/store';
import { GetActiveGroups } from '../../state/admin-groups/admin-groups.action';
import { AdminGroupState } from '../../state/admin-groups/admin-groups.state';
import { Observable } from 'rxjs/internal/Observable';
import { GetUserGroups, UpdateUserGroups } from '../../state/admin-users/admin-users.actions';
import { AdminUserState } from '../../state/admin-users/admin-users.state';
import { ActivatedRoute } from '@angular/router';
import { BaseComponent } from "../../../shared/base/base.component";
import { SubSink } from 'subsink/dist/subsink';
import { FieldsService } from 'src/app/core/services/business/fields/fields.service';
import { Button } from 'src/app/core/models/button';


@Component({
  selector: 'app-admin-user-groups',
  templateUrl: './admin-user-groups.component.html',
  styleUrls: ['./admin-user-groups.component.css']
})
export class AdminUserGroupsComponent extends BaseComponent implements OnInit, OnDestroy {

  private subs = new SubSink();
  userId: number;
  groups: Group[] = [];
  isLoading: boolean = true;
  columns: GridColumn[] = [
    { type: 'checkbox', headerText: 'Select All', width: '100', field: '' },
    { type: '', headerText: 'Groups', width: '', field: 'name' }
  ];
  footer: Button[] = [
    { text: 'Update Groups', type: "button", cssClass: "button-default update-groups-button-qa", disabled: true, action: this.updateGroups.bind(this) }
  ];
  userGroupIds: number[] = [];
  initialGroupIds: number[] = [];
  selectedGroups: Group[] = [];

  @Select(AdminGroupState.getActiveGroups) groups$: Observable<Group[]>;
  @Select(AdminUserState.getGroups) userGroups$: Observable<Group[]>;

  constructor(protected store: Store, protected activatedRoute: ActivatedRoute, private fieldsService: FieldsService) {
    super(store);
  }

  ngOnInit() {
    this.store.dispatch(new GetActiveGroups());
    this.subs.add(
      // 1st subscription
      this.activatedRoute.paramMap
        .subscribe(params => {
          this.userId = Number(params.get('id'));
          if (this.userId) this.store.dispatch(new GetUserGroups(this.userId));
          else this.store.dispatch(new ClearUserGroups());
        }),
      // 2nd subscription
      this.userGroups$
        .subscribe(groups => {
          if (groups) {
            this.initialGroupIds = groups.map(x => x.id);
            this.isLoading = false;
            this.selectedGroups = groups;
            console.log("selectedGroups userGroup",  this.selectedGroups)
            this.setFooterProperties();
          }
        }),
      this.groups$
        .subscribe(groups => {
          console.log("groups$ groups",  groups)
          if(groups)this.groups = groups
        })
    );
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
    this.store.dispatch(new ClearUserGroups());
  }

  updateGroups() {
    const groupIds = this.selectedGroups.map(group => group.id);
    this.subs.add(
      this.store.dispatch(new UpdateUserGroups(this.userId, groupIds))
        .subscribe(() => {
          this.store.dispatch(new GetUserGroups(this.userId));
        })
    );
  }

  rowSelectedEvent(groups: Group[]) {
    groups.forEach(group => this.addGroup(group));
    this.setFooterProperties();
  }

  rowDeselectedEvent(groups: Group[]) {
    groups.forEach(group => this.removeGroup(group));
    this.setFooterProperties();
  }

  private addGroup(group: Group) {
    console.log('addGroup selectedGroups', this.selectedGroups, 'private group', group)
    const selectedGroupIds = this.selectedGroups.map(x => x.id);
    if (!selectedGroupIds.includes(group.id)) {
      this.selectedGroups = [
        ...this.selectedGroups,
        group
      ];
    }
  }

  private removeGroup(group: Group) {
    const selectedGroupIds = this.selectedGroups.map(x => x.id);
    if (selectedGroupIds.includes(group.id)) {
      this.selectedGroups = this.selectedGroups.filter(x => x.id !== group.id);
    }
  }

  private setFooterProperties() {
    this.userGroupIds = this.selectedGroups.map(x => x.id);
    this.footer.forEach(x => {
      x.disabled = this.fieldsService.compareNumberArrays(this.selectedGroups.map(x => x.id), this.initialGroupIds);
    });
    console.log("selectedGroups: ", this.selectedGroups);
  }
}