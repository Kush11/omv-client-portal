import { Component, OnDestroy, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Select, Store } from '@ngxs/store';
import { AdminUserState } from '../state/admin-users/admin-users.state';
import { User } from '../../core/models/entity/user';
import { Tab } from './../../core/models/tab';
import { ListComponent } from 'src/app/shared/list/list.component';
import { InitializeUser, CreateUser, DisableUser, EnableUser, GetUser, UpdateUser } from '../state/admin-users/admin-users.actions';
import { UserStatus } from 'src/app/core/enum/user-status.enum';
import { AppState } from 'src/app/state/app.state';
import { SubSink } from 'subsink/dist/subsink';
import { Observable } from 'rxjs/internal/Observable';
import { Button } from 'src/app/core/models/button';

const CREATE_USER = 'Create User';
const UPDATE_USER = 'Update User';
const DISABLE_USER = 'Disable User';
const ENABLE_USER = 'Enable User';

@Component({
  selector: 'app-admin-user-edit',
  templateUrl: './admin-user-edit.component.html',
  styleUrls: ['./admin-user-edit.component.css', './../../app.component.css']
})
export class AdminUserEditComponent extends ListComponent implements OnInit, OnDestroy {

  private subs = new SubSink();
  userId: number;
  userForm: FormGroup;
  user = new User();
  tabs: Tab[] = [
    { link: '', name: 'Groups', isActive: true }
  ];
  userActionText: string;
  errorMessage: string;
  previousRoute: string;
  buttons: Button[] = [
    { text: "", type: 'submit', cssClass: "button-default create-update-user-button-qa" },
    { text: "", type: 'button', cssClass: "button-default enable-disable-user-button-qa" },
  ];

  @Select(AdminUserState.getCurrentUser) currentUser$: Observable<User>;
  @Select(AdminUserState.getCurrentUserId) currentUserId$: Observable<number>;
  @Select(AppState.getPreviousRoute) previousRoute$: Observable<string>;

  constructor(protected store: Store,
    protected router: Router,
    private formBuilder: FormBuilder,
    private activatedRoute: ActivatedRoute) {
    super(store);
    this.hideLeftNav();
    this.setPageTitle('Admin User Edit');
    // Initialize the userForm
    this.userForm = this.formBuilder.group({
      firstName: ['', [Validators.required]],
      lastName: ['', [Validators.required]],
      userName: ['', [Validators.required]],
      emailAddress: ['', [Validators.required, Validators.email]]
    });
  }

  ngOnInit() {
    this.subs.add(
      // Get the id in the browser url and reach out for the User
      this.activatedRoute.paramMap
        .subscribe(params => {
          this.userId = Number(params.get('id'));
          if (this.userId) {
            this.store.dispatch(new GetUser(this.userId));
          } else {
            this.store.dispatch(new InitializeUser());
          }
          this.setButtonsProps();
        }),
      // Get the currentUser
      this.currentUser$
        .subscribe(user => {
          if (user) { // Existing User
            this.userActionText = user.status === UserStatus.Active ? DISABLE_USER : ENABLE_USER;
            this.userForm.setValue({
              firstName: user.firstName,
              lastName: user.lastName,
              userName: user.userName,
              emailAddress: user.emailAddress
            });
            this.user = user;
          } else {
            this.userForm.reset();
          }
          this.setButtonsProps();
        }),
      this.previousRoute$
        .subscribe(route => {
          const default_route = "/admin/users"; // in case a user gets to this page other than by routing from the previous page
          if (route.includes(default_route)) {
            this.previousRoute = route;
          } else {
            this.previousRoute = default_route;
          }
        }),
      this.userForm.valueChanges
        .subscribe(() => {
          const createUpdateUserButton = this.buttons[0];
          createUpdateUserButton.disabled = !this.userForm.valid || !this.userForm.dirty;
        })
    );
  }

  ngOnDestroy() {
    this.userForm.reset();
    this.subs.unsubscribe();
  }

  onBack(route: string) {
    this.router.navigate([route]);
  }

  buttonsClick(index: number) {
    if (index === 1) {
      this.changeStatus();
    }
    this.setButtonsProps();
  }

  save() {
    if (this.userForm.valid) {
      if (this.userForm.dirty) {
        const user: User = { ...this.user, ...this.userForm.value };

        if (this.userId === 0) { // Create User
          this.store.dispatch(new CreateUser(user));
          this.subs.sink = this.currentUserId$
            .subscribe(userId => {
              if (userId) {
                this.userForm.reset();
                this.router.navigate([`/admin/users/${userId}/edit`]);
              }
            });
        } else { // Update User
          this.subs.sink = this.store.dispatch(new UpdateUser(user.id, user))
            .subscribe(() => {
              this.userForm.reset(this.userForm.value);
              this.setButtonsProps();
            });
        }
      }
    } else {
      this.errorMessage = "Please correct the validation errors.";
    }
  }

  changeStatus() {
    const statusButton = this.buttons[1];
    statusButton.disabled = true;
    if (statusButton.text === ENABLE_USER) {
      this.subs.sink = this.store.dispatch(new EnableUser(this.user))
        .subscribe(() => {
          statusButton.text = DISABLE_USER;
          statusButton.disabled = false;
        }, () => statusButton.disabled = false);
    } else {
      this.subs.sink = this.store.dispatch(new DisableUser(this.user))
        .subscribe(() => {
          statusButton.text = ENABLE_USER;
          statusButton.disabled = false;
        }, () => statusButton.disabled = false);
    }
  }

  private setButtonsProps() {
    this.buttons.map((button, index) => {
      if (index === 0) { // Update List
        button.text = !this.userId ? CREATE_USER : UPDATE_USER;
        button.disabled = !this.userForm.valid || !this.userForm.dirty;
      } else { // Enable / Disable User
        button.text = this.user.status === UserStatus.Active ? DISABLE_USER : ENABLE_USER;
        button.hidden = !this.userId ? true : false;
      }
    });
  }
}
