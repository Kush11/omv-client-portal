import { AddGroupMembers } from '../../state/admin-groups/admin-groups.action';
import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { GridColumn } from 'src/app/core/models/grid.column';
import { Select, Store } from '@ngxs/store';
import { AdminGroupState } from '../../state/admin-groups/admin-groups.state';
import { Observable } from 'rxjs/internal/Observable';
import { GetGroupMembers, RemoveGroupMembers } from '../../state/admin-groups/admin-groups.action';
import { ActivatedRoute } from '@angular/router';
import { User } from 'src/app/core/models/entity/user';
import { DialogComponent } from '@syncfusion/ej2-angular-popups';
import { AdminUserState } from '../../state/admin-users/admin-users.state';
import { GetActiveUsers } from '../../state/admin-users/admin-users.actions';
import { ShowErrorMessage } from 'src/app/state/app.actions';
import { SubSink } from 'subsink/dist/subsink';
import { FieldsService } from 'src/app/core/services/business/fields/fields.service';
import { ModalComponent } from 'src/app/shared/modal/modal.component';
import { Button } from 'src/app/core/models/button';
import { ListComponent } from 'src/app/shared/list/list.component';

@Component({
  selector: 'app-admin-group-members',
  templateUrl: './admin-group-members.component.html',
  styleUrls: ['./admin-group-members.component.css', './../../../app.component.css']
})
export class AdminGroupMembersComponent implements OnInit, OnDestroy {

  isLoading: boolean = true;
  private subs = new SubSink();
  groupId: number;
  componentActive = true;
  users: User[] = [];
  filteredUsers: User[] = [];
  member: string;
  groupMembers: User[] = [];
  selectedMembers: User[] = [];
  columns: GridColumn[] = [
    { type: "checkbox", headerText: "Select All", width: "100", field: "" },
    { type: "", headerText: "Name", width: "", field: "displayName" },
    { type: "", headerText: "Email", width: "", field: "emailAddress" },
  ];
  footer: Button[] = [
    { text: 'Add Member(s)', type: "button", cssClass: "button-default add-members-qa", disabled: false, action: this.showMembersModal.bind(this) },
    { text: 'Remove Member(s)', type: "button", cssClass: "button-default remove-members-qa", disabled: true, action: this.showConfirmModal.bind(this) },
  ];
  usersFields: Object = { text: 'displayName', value: 'id' };
  userIds: any;

  @Select(AdminUserState.getActiveUsers) getUsers$: Observable<User[]>;
  @Select(AdminGroupState.getGroupMembers) getGroupMembers$: Observable<User[]>;

  @ViewChild('groupDialog') membersDialog: DialogComponent;
  @ViewChild('confirmDialog') confirmDialog: DialogComponent;
  @ViewChild('confirmModal') confirmModal: ModalComponent;
  @ViewChild('membersList') membersList: ListComponent;

  constructor(private store: Store, private activatedRoute: ActivatedRoute, private fieldsService: FieldsService) { }

  ngOnInit() {
    this.store.dispatch(new GetActiveUsers());
    this.subs.add(
      // Get the id in the browser url and reach out for the Group
      this.activatedRoute.paramMap
        .subscribe(params => {
          this.groupId = Number(params.get('id'));
          this.store.dispatch(new GetGroupMembers(this.groupId));
        }),
      this.getUsers$
        .subscribe(users => this.users = users),
      this.getGroupMembers$
        .subscribe(memberIds => {
          this.groupMembers = memberIds;
          this.isLoading = false;
        })
    );
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

  rowSelectedEvent(members: User[]) {
    members.forEach(m => this.addUser(m));
    this.setFooterProperties();
  }

  rowDeselectedEvent(members: User[]) {
    members.forEach(m => this.removeUser(m));
    this.setFooterProperties();
  }

  private addUser(user: User) {
    const selectedMemberIds = this.selectedMembers.map(x => x.id);
    if (!selectedMemberIds.includes(user.id)) {
      this.selectedMembers = [
        ...this.selectedMembers,
        user
      ];
    }
  }

  private removeUser(user: User) {
    const selectedMemberIds = this.selectedMembers.map(x => x.id);
    if (selectedMemberIds.includes(user.id)) {
      this.selectedMembers = this.selectedMembers.filter(x => x.id !== user.id);
    }
  }

  showMembersModal() {
    const membersIds: number[] = [];
    this.groupMembers.forEach(m => membersIds.push(m.id));
    this.filteredUsers = this.users.filter(x => !membersIds.includes(x.id));
    this.membersDialog.show();
  }

  showConfirmModal() {
    this.confirmModal.show();
  }

  addMembersClick() {
    let memberIds = this.groupMembers.map(ids => ids.id);
    const found = memberIds.some(r => this.userIds.includes(r));
    if (!found) {
      this.store.dispatch(new AddGroupMembers(this.groupId, this.userIds));
      this.userIds = null;
    }
    else {
      this.store.dispatch(new ShowErrorMessage('Member Exists'));
      this.userIds = null;
    }
    this.membersDialog.hide();
  }

  cancelRemove() {
    this.confirmDialog.hide();
  }

  closeDialog() {
    this.membersDialog.hide();
  }

  //#region Confirm Dialog

  deleteMembers() {
    const membersIds = this.selectedMembers.map(member => member.id);
    this.subs.sink = this.store.dispatch(new RemoveGroupMembers(this.groupId, membersIds))
      .subscribe(() => {
        this.selectedMembers = [];
        this.membersList.clearSelection();
        this.setFooterProperties();
      });
  }

  //#endregion

  private setFooterProperties() {
    this.footer.forEach((x, index) => {
      if (index === 1) {
        x.disabled = this.selectedMembers.length === 0;
      }
    });
  }
}
