import { GroupStatus as GroupStatus } from './../../core/enum/group-status.enum';
import { Tab } from 'src/app/core/models/tab';
import { ClearGroup, CreateGroup, DisableGroup, EnableGroup, GetGroup, UpdateGroup } from '../state/admin-groups/admin-groups.action';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Select, Store } from '@ngxs/store';
import { Observable } from "rxjs";
import { Group } from 'src/app/core/models/entity/group';
import { ActivatedRoute, Router } from '@angular/router';
import { AdminGroupState } from '../state/admin-groups/admin-groups.state';
import { EditComponent } from 'src/app/shared/edit/edit.component';
// import { permission } from 'src/app/core/enum/permission';
import { SubSink } from 'subsink/dist/subsink';
import { AppState } from 'src/app/state/app.state';
import { Button } from 'src/app/core/models/button';

const CREATE_GROUP = 'Create Group';
const UPDATE_GROUP = 'Update Group';
const ENABLE_GROUP = 'Enable Group';
const DISABLE_GROUP = 'Disable Group';
const PERMISSIONS_TAB = 0;
const MEMBERS_TAB = 1;
const MEDIA_ACCESS = 2;

@Component({
  selector: 'app-admin-group-edit',
  templateUrl: './admin-group-edit.component.html',
  styleUrls: ['./admin-group-edit.component.css', './../../app.component.css']
})
export class AdminGroupEditComponent extends EditComponent implements OnInit {

  private subs = new SubSink();
  showPermissions: boolean;
  showMembers: boolean;
  showMediaAccess: boolean;

  groupId: number;
  groupForm: FormGroup;
  group = new Group();
  groupItemTabs: Tab[] = [
    { link: PERMISSIONS_TAB, name: 'Permissions' },
    { link: MEMBERS_TAB, name: 'Members' },
    { link: MEDIA_ACCESS, name: 'Media Access' }
  ];
  groupActionText: string;
  createGroupButtonText: string;
  errorMessage: string;
  previousRoute: string;
  buttons: Button[] = [
    { text: "", type: 'submit', cssClass: "button-default create-update-group-qa" },
    { text: "", type: 'button', cssClass: "button-default enable-disable-group-qa" },
  ];

  @Select(AdminGroupState.getCurrentGroup) currentGroup$: Observable<Group>;
  @Select(AdminGroupState.getCurrentGroupId) currentGroupId$: Observable<number>;
  @Select(AppState.getPreviousRoute) previousRoute$: Observable<string>;

  constructor(protected store: Store, protected router: Router, private fb: FormBuilder, private activatedRoute: ActivatedRoute) {
    super(store, router);
    // this.Permission = permission.VIEW_GROUP_EDIT;
    this.hideLeftNav();
    this.setPageTitle('Admin Group Edit');
  }

  ngOnInit() {
    // Initialize the groupForm
    this.groupForm = this.fb.group({
      id: '',
      name: ['', [Validators.required]],
      description: ''
    });

    this.subs.add(
      // Get the id in the browser url and reach out for the group
      this.activatedRoute.paramMap
        .subscribe(params => {
          this.groupId = Number(params.get('id'));
          if (this.groupId) this.store.dispatch(new GetGroup(this.groupId));
          else this.store.dispatch(new ClearGroup());
          this.setButtonsProps();
        }),
      // Get the current group
      this.currentGroup$
        .subscribe(group => {
          if (group) { // Existing Group
            this.groupForm.patchValue({
              id: group.id,
              name: group.name,
              description: group.description
            });
            this.group = group;
            this.setButtonsProps();
          }
        }),
      // 3rd sub
      this.previousRoute$
        .subscribe(route => {
          const default_route = "/admin/groups"; // in case a user gets to this page other than by routing from the previous page
          if (route.includes(default_route)) {
            this.previousRoute = route;
          } else {
            this.previousRoute = default_route;
          }
        }),
      this.groupForm.valueChanges
        .subscribe(() => {
          const createUpdateGroupButton = this.buttons[0];
          createUpdateGroupButton.disabled = !this.groupForm.valid || !this.groupForm.dirty;
        })
    )
    this.setActiveTab(PERMISSIONS_TAB);
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

  onBack(route: string) {
    this.router.navigate([route]);
  }

  buttonsClick(index: number) {
    if (index === 1) {
      this.changeStatus();
    }
    this.setButtonsProps();
  }

  save() {
    if (this.groupForm.valid) {
      if (this.groupForm.dirty) {
        const group: Group = { ...this.group, ...this.groupForm.value };

        if (this.groupId === 0) {
          this.store.dispatch(new CreateGroup(group));
          this.subs.add(
            this.currentGroupId$
              .subscribe(groupId => {
                if (groupId) {
                  this.groupForm.reset();
                  this.router.navigate([`/admin/groups/${groupId}/edit`]);
                }
              })
          );
        } else {
          this.subs.sink = this.store.dispatch(new UpdateGroup(group.id, group))
            .subscribe(() => {
              this.groupForm.reset(this.groupForm.value);
              this.setButtonsProps();
            });
        }
      }
    }
  }

  changeStatus() {
    const statusButton = this.buttons[1];
    statusButton.disabled = true;
    if (statusButton.text === ENABLE_GROUP) {
      this.subs.sink = this.store.dispatch(new EnableGroup(this.group))
        .subscribe(() => {
          statusButton.text = DISABLE_GROUP;
          statusButton.disabled = false;
        }, () => statusButton.disabled = false);
    } else {
      this.subs.sink = this.store.dispatch(new DisableGroup(this.group))
        .subscribe(() => {
          statusButton.text = ENABLE_GROUP;
          statusButton.disabled = false;
        }, () => statusButton.disabled = false);
    }
  }

  switchTabs(tabLink: any) {
    this.clearActiveTab();
    this.setActiveTab(tabLink);
  }

  setActiveTab(tabIndex: number) {
    switch (tabIndex) {
      case PERMISSIONS_TAB:
        this.showPermissions = true;
        break;
      case MEMBERS_TAB:
        this.showMembers = true;
        break;
      case MEDIA_ACCESS:
        this.showMediaAccess = true;
        break;
      default:
        break;
    }
    this.groupItemTabs[tabIndex].isActive = true;
  }

  clearActiveTab() {
    this.showPermissions = this.showMembers = this.showMediaAccess = false;
    this.groupItemTabs.map(x => x.isActive = false);
  }

  private setButtonsProps() {
    this.buttons.map((button, index) => {
      if (index === 0) { // Update List
        button.text = !this.groupId ? CREATE_GROUP : UPDATE_GROUP;
        button.disabled = !this.groupForm.valid || !this.groupForm.dirty;
      } else { // Enable / Disable User
        button.text = this.group.status === GroupStatus.Active ? DISABLE_GROUP : ENABLE_GROUP;
        button.hidden = !this.groupId ? true : false;
      }
    });
  }
}
