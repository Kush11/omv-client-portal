import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { AdminGroupState } from '../../state/admin-groups/admin-groups.state';
import { Observable } from 'rxjs/internal/Observable';
import { MediaAccess } from 'src/app/core/models/media-access';
import { Select, Store } from '@ngxs/store';
import { GetGroupFolders, UpdateGroupFolders } from '../../state/admin-groups/admin-groups.action';
import { TreeViewComponent, NodeExpandEventArgs } from '@syncfusion/ej2-angular-navigations';
import { ActivatedRoute } from '@angular/router';
import { DirectoryService } from 'src/app/core/services/business/directory/directory.service';
import { SubSink } from 'subsink/dist/subsink';
import { ShowSpinner, ShowErrorMessage, HideSpinner } from 'src/app/state/app.actions';
import { BaseComponent } from 'src/app/shared/base/base.component';
import { Directory } from 'src/app/core/models/entity/directory';

@Component({
  selector: 'app-admin-group-media-access',
  templateUrl: './admin-group-media-access.component.html',
  styleUrls: ['./admin-group-media-access.component.css', '../../../app.component.css']
})
export class AdminGroupMediaAccessComponent extends BaseComponent implements OnInit, OnDestroy {

  @ViewChild('treeview') treeview: TreeViewComponent;
  @Select(AdminGroupState.getMediaAccess) getMediaAccess$: Observable<MediaAccess[]>;
  @Select(AdminGroupState.getRoleMediaAccessIds) currentGroupMediaAccessIds$: Observable<number[]>

  private subs = new SubSink();
  folders: Directory[] = [];
  field: Object;
  groupId: number;
  checkedNode: string[] = [];
  currentTarget: HTMLLIElement;
  parentID: string;

  constructor(protected store: Store, private activatedRoute: ActivatedRoute, private foldersService: DirectoryService) {
    super(store);
  }

  ngOnInit() {
    this.subs.add(
      this.activatedRoute.paramMap
        .subscribe(params => {
          this.groupId = Number(params.get('id'));
          this.store.dispatch(new GetGroupFolders(this.groupId));
        }),
      this.foldersService.getFolders()
        .subscribe(data => {
          this.store.dispatch(new ShowSpinner());
          this.folders = data;
          this.folders.forEach(item => {
            item.icon = 'folder';
            if (this.checkedNode.map(v => parseInt(v)).includes(item.id)) {
              item.isChecked = true;
              // item.expanded = true;
            }
          });
          this.field = {
            dataSource: this.folders, id: 'id', parentID: 'parentId',
            text: 'name', iconCss: 'icon', hasChildren: 'hasChild'
          };
          this.store.dispatch(new HideSpinner());
        }, err => this.store.dispatch(new ShowErrorMessage(err)))
    );
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

  dataBound(data: any) {
    this.subs.sink = this.currentGroupMediaAccessIds$
      .subscribe((checkedNodes) => {
        this.checkedNode = checkedNodes.map(String);
      });
  }

  nodeChecked(data: any): void {
    console.log('The checked node id is: ' + this.treeview.checkedNodes);
    console.log('selected child nodes', this.treeview.getAllCheckedNodes());
  }

  updateMediaAccess() {
    this.checkedNode = this.treeview.getAllCheckedNodes();
    const ids = this.checkedNode.map(v => parseInt(v));
    this.store.dispatch(new UpdateGroupFolders(this.groupId, ids)).toPromise().then(() => {
      this.store.dispatch(new GetGroupFolders(this.groupId));
    });
  }

  onNodeExpanding(args: NodeExpandEventArgs) {
    if ((args.node.querySelectorAll(".e-icons.e-icon-expandable").length > 0) && args.node.querySelectorAll("ul li").length == 0) {
      this.currentTarget = args.node;
      this.parentID = args.node.getAttribute("data-uid");
      this.showSpinner();
      let parentFolder = args.nodeData as any;
      console.log('onNodeExpanding: ', args);
      if (this.checkedNode.map(v => parseInt(v)).includes(parentFolder.id)) {
        parentFolder.isChecked = true;
      }
      this.foldersService.getFolders(Number(this.parentID)).toPromise()
        .then(folders => {
          folders.forEach(folder => {
            folder.icon = 'folder';
            if (this.checkedNode.map(v => parseInt(v)).includes(folder.id)) {
              folder.isChecked = true;
            }
          });
          this.treeview.addNodes(JSON.parse(JSON.stringify(folders)), this.currentTarget);
          this.hideSpinner();
        }, err => {
          this.showErrorMessage(err);
        });
    }

  }
}
