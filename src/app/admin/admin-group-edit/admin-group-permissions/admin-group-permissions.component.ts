import { UpdateGroupPermissions } from '../../state/admin-groups/admin-groups.action';
import { Component, OnInit } from '@angular/core';
import { GridColumn } from 'src/app/core/models/grid.column';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs/internal/Observable';
import { Permission } from 'src/app/core/enum/permission';
import { AdminGroupState } from '../../state/admin-groups/admin-groups.state';
import { ActivatedRoute } from '@angular/router';
import { GetGroupPermissions } from '../../state/admin-groups/admin-groups.action';
import { AdminPermissionState } from '../../state/admin-permissions/admin-permissions.state';
import { GetPermissions } from '../../state/admin-permissions/admin-permissions.action';
import { BaseComponent } from "../../../shared/base/base.component";
import { ShowErrorMessage } from 'src/app/state/app.actions';
import { SubSink } from 'subsink/dist/subsink';
import { FieldsService } from 'src/app/core/services/business/fields/fields.service';
import { Button } from 'src/app/core/models/button';
@Component({
  selector: 'app-admin-group-permissions',
  templateUrl: './admin-group-permissions.component.html',
  styleUrls: ['./admin-group-permissions.component.css']
})
export class AdminGroupPermissionsComponent extends BaseComponent implements OnInit {

  isLoading: boolean = true;
  private subs = new SubSink();
  groupId: number;
  componentActive = true;
  permissions: Permission[] = [];
  selectedPermissions: Permission[] = [];
  groupPermissionIds: string[] = [];
  columns: GridColumn[] = [
    { type: "checkbox", headerText: "Select All", width: "100", field: "" },
    { type: "", headerText: "Permission Title", width: "", field: "name" }
  ];
  footer: Button[] = [
    { text: 'Update Permissions', type: "button", cssClass: "button-default update-permissions-qa", disabled: true, action: this.updatePermissions.bind(this) }
  ];

  @Select(AdminPermissionState.getPermissions) permissions$: Observable<Permission[]>;
  @Select(AdminGroupState.getPermissionsByGroupId) userPermissions$: Observable<Permission[]>;

  constructor(protected store: Store, protected activatedRoute: ActivatedRoute, private fieldsService: FieldsService) {
    super(store);
  }

  ngOnInit() {
    this.store.dispatch(new GetPermissions());
    this.subs.add(
      // 1st sub
      this.activatedRoute.paramMap
        .subscribe(params => {
          this.groupId = Number(params.get('id'));
          this.store.dispatch(new GetGroupPermissions(this.groupId));
        }),
      // 2nd sub
      this.permissions$
        .subscribe(permissions => this.permissions = permissions),
      // 3rd sub
      this.userPermissions$
        .subscribe(permissions => {
          if (permissions) {
            this.selectedPermissions = permissions;
            this.isLoading = false;
            this.groupPermissionIds = permissions.map(x => x.id);
            this.setFooterProperties();
          }
        })
    );
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

  updatePermissions() {
    const permissionsIds = this.selectedPermissions.map(permission => permission.id);
    this.subs.add(
      this.store.dispatch(new UpdateGroupPermissions(this.groupId, permissionsIds))
        .subscribe(() => { }, (err) => this.store.dispatch(new ShowErrorMessage(err)))
    );
  }

  rowSelectedEvent(permissions: Permission[]) {
    permissions.forEach(group => this.addPermission(group));
    this.setFooterProperties();
  }

  rowDeselectedEvent(permissions: Permission[]) {
    permissions.forEach(group => this.removePermission(group));
    this.setFooterProperties();
  }

  private addPermission(permission: Permission) {
    const selectedPermissionIds = this.selectedPermissions.map(x => x.id);
    if (!selectedPermissionIds.includes(permission.id)) {
      this.selectedPermissions = [
        ...this.selectedPermissions,
        permission
      ];
    }
  }

  private removePermission(permission: Permission) {
    const selectedPermissionIds = this.selectedPermissions.map(x => x.id);
    if (selectedPermissionIds.includes(permission.id)) {
      this.selectedPermissions = this.selectedPermissions.filter(x => x.id !== permission.id);
    }
  }

  private setFooterProperties() {
    this.footer.forEach(x => {
      x.disabled = this.fieldsService.compareStringArrays(this.selectedPermissions.map(x => x.id), this.groupPermissionIds);
    });
  }
}

