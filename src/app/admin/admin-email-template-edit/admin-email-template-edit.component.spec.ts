import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminEmailTemplateEditComponent } from './admin-email-template-edit.component';

describe('AdminEmailTemplateEditComponent', () => {
  let component: AdminEmailTemplateEditComponent;
  let fixture: ComponentFixture<AdminEmailTemplateEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminEmailTemplateEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminEmailTemplateEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
