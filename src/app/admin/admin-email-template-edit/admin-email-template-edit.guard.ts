import { Injectable } from '@angular/core';
import { AdminEmailTemplateEditComponent } from './admin-email-template-edit.component';
import { Observable } from 'rxjs/internal/Observable';
import { CanDeactivate } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AdminEmailTemplateEditGuard implements CanDeactivate<AdminEmailTemplateEditComponent> {
  canDeactivate (component: AdminEmailTemplateEditComponent): Observable<boolean> | Promise<boolean> | boolean {
    if (component.emailTemplateForm.dirty) {
      const emailName = component.emailTemplateForm.get('name').value || 'new EmailTemplates';
      return confirm (`Navigate away and lose all changes to ${emailName}`);
    }
    return true;
  } 
}