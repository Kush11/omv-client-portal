import { Component, OnInit, ViewChild } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { SubSink } from 'subsink/dist/subsink';
import { Select, Store } from '@ngxs/store';
import { AdminEmailTemplatesState } from '../state/admin-email-templates/admin-email-templates.state';
import { EmailTemplate } from 'src/app/core/models/entity/email-templates';
import { AppState } from 'src/app/state/app.state';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { GetEmailTemplate, CreateEmailTemplate, UpdateEmailTemplate, ClearCurrentEmailTemplate, GetEmailPlaceholders, ClearEmailPlaceholders } from '../state/admin-email-templates/admin-email-templates.action';
import { BaseComponent } from 'src/app/shared/base/base.component';
import { Button } from 'src/app/core/models/button';
import { InsertHtml, NodeSelection, HTMLFormatter } from '@syncfusion/ej2-angular-richtexteditor';
import { RichTextEditorComponent } from '@syncfusion/ej2-angular-richtexteditor';
import { Dialog } from '@syncfusion/ej2-popups';
import { Lookup } from 'src/app/core/models/entity/lookup';
import { DropDownList } from '@syncfusion/ej2-angular-dropdowns';

@Component({
  selector: 'app-admin-email-template-edit',
  templateUrl: './admin-email-template-edit.component.html',
  styleUrls: ['./admin-email-template-edit.component.css']
})
export class AdminEmailTemplateEditComponent extends BaseComponent implements OnInit {

  private subs = new SubSink();

  @Select(AdminEmailTemplatesState.getCurrentEmailTemplate) currentEmailTemplate$: Observable<EmailTemplate>;
  @Select(AdminEmailTemplatesState.getCurrentEmailTemplateId) currentEmailTemplateId$: Observable<number>;
  @Select(AppState.getPreviousRoute) previousRoute$: Observable<string>;
  @Select(AdminEmailTemplatesState.getPlaceholders) placeholders$: Observable<Lookup[]>;

  @ViewChild('customRTE') public rteObj: RichTextEditorComponent;
  public tools: object = {
    items: ['Bold', 'Italic', 'Underline', '|', 'Formats', 'Alignments', 'OrderedList',
    'UnorderedList', '|', 'CreateLink', 'Image', '|', 
    'SourceCode', '|', 
    'Undo', 'Redo',
    {
        tooltipText: 'Insert value',
        template: '<input type="text" id="dropdown" />'
    }
    ]
  };
  public height: any = '350px';
  public selection: NodeSelection = new NodeSelection();
  public textArea: HTMLTextAreaElement;
  public ranges: Range;

  previousRoute: string;
  emailTemplateForm: FormGroup;
  emailTemplateId: string;
  currentEmailTemplate = new EmailTemplate();
  emailtemplate: any;
  errorMessage: string;
  emailActionText: string;
  createEmailButtonText: string;

  placeholders: any[];

  buttons: Button[] = [
    { text: "SAVE CHANGES", type: 'submit', cssClass: "button-round save-changes-qa", disabled: true },
    { text: "Cancel", type: 'button', cssClass: "button-no-outline cancel-qa" },
  ];


  constructor(protected store: Store, protected router: Router, private formBuilder: FormBuilder,
    private activatedRoute: ActivatedRoute) {
    super(store);
    this.hideLeftNav();
    this.emailTemplateForm = this.formBuilder.group({
      id: '',
      name: ['', [Validators.required]],
      subject: ['', [Validators.required]],
      body: ['', [Validators.required]],
    });
  }

  ngOnInit() {
    this.store.dispatch(new GetEmailPlaceholders());
    this.subs.add(
      this.activatedRoute.paramMap
        .subscribe(params => {
          this.emailTemplateId = String(params.get('id'));
          if (this.emailTemplateId != '0') {
            this.store.dispatch(new GetEmailTemplate(this.emailTemplateId));
          } else {
            this.emailTemplateForm.reset();
          }
        }),
      this.currentEmailTemplate$
        .subscribe(emailTemplate => {
          if (emailTemplate) {
            this.emailTemplateForm.patchValue({
              id: emailTemplate.id,
              name: emailTemplate.name,
              subject: emailTemplate.subject,
              body: emailTemplate.body,
            });
            this.currentEmailTemplate = emailTemplate;
            this.emailtemplate = { ...this.currentEmailTemplate, ...this.emailTemplateForm.value };
          }
        }),
      this.previousRoute$
        .subscribe(route => {
          const default_route = "/admin/email-templates"; // in case a user gets to this page other than by routing from the previous page
          if (route.includes(default_route)) {
            this.previousRoute = route;
          } else {
            this.previousRoute = default_route;
          }
        }),
      this.emailTemplateForm.valueChanges
        .subscribe(() => this.buttons[0].disabled = !this.emailTemplateForm.valid || !this.emailTemplateForm.dirty),
        this.placeholders$.subscribe(placeholders => {
          this.placeholders = placeholders;
        })
    );
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
    this.store.dispatch(new ClearCurrentEmailTemplate());
    this.store.dispatch(new ClearEmailPlaceholders());
  }

  onBack(route: string) {
    this.router.navigate([route]);
  }

  buttonActions(index: number) {
    if (index === 1) {
      this.cancel();
    }
  }
  toggleButton(e) {
    console.log(e, 'there is an action')
    this.buttons[0].disabled = false;
  }
  save() {
    if (this.emailTemplateForm.valid) {
      if (this.emailTemplateForm.dirty) {
        const EmailTemplate: EmailTemplate = { ...this.currentEmailTemplate, ...this.emailTemplateForm.value };

        if (this.emailTemplateId === '0') {
          this.store.dispatch(new CreateEmailTemplate(EmailTemplate));
          this.subs.add(
            this.currentEmailTemplateId$
              .subscribe(emailTemplateId => {
                if (emailTemplateId) {
                  this.emailTemplateForm.reset();
                  this.router.navigate([`/admin/email-templates/${emailTemplateId}/edit`]);
                  this.emailTemplateForm.reset();
                }
              })
          );
        } else {
          this.subs.sink = this.store.dispatch(new UpdateEmailTemplate(EmailTemplate.id, EmailTemplate))
            .subscribe(() => {
              this.emailTemplateForm.reset(this.emailTemplateForm.value);
            });
        }
      }
    } else {
      this.errorMessage = "Please correct the validation errors.";
    }
  }

  cancel() {
    this.router.navigate([`/admin/email-templates`]);
  }

  public onCreate(): void {
    const dropdown: DropDownList = new DropDownList({
      dataSource: this.placeholders,
      fields: { text: 'description', value: 'name' },
      placeholder: 'Insert Fields',
      width: '100px',
      change: (args: any) => {
        this.ranges = this.selection.getRange(document);
        InsertHtml.Insert(document, document.createTextNode(args.value));
      }
    });
    dropdown.appendTo('#dropdown');
  }

  // public actionComplete(e: any): void {
  //   console.log('e', e);
  //   this.textArea.style.display = 'block';
  //   this.textArea.style.width = '100%';
  // }
}
