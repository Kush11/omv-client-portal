import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminFolderStructureComponent } from './admin-folder-structure.component';

describe('AdminFolderStructureComponent', () => {
  let component: AdminFolderStructureComponent;
  let fixture: ComponentFixture<AdminFolderStructureComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminFolderStructureComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminFolderStructureComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
