import { ShowNewFolder } from './../../media/state/media/media.action';
import { MediaService } from 'src/app/core/services/business/media/media.service';
import { DataService } from './../../media/media-upload/data-service';
import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { Store } from '@ngxs/store';
import {
  TreeViewComponent,
  ContextMenuComponent,
  MenuItemModel,
  MenuEventArgs,
  DataSourceChangedEventArgs,
  NodeEditEventArgs,
  DragAndDropEventArgs,
  NodeExpandEventArgs
} from '@syncfusion/ej2-angular-navigations';
import { Router } from '@angular/router';
import { SetPreviousRoute, ShowSpinner, HideSpinner, ShowErrorMessage, ShowSuccessMessage } from 'src/app/state/app.actions';
import { FormGroup } from '@angular/forms';
import { UpdateFolder, RemoveFolder } from '../state/admin-media/admin-media.action';
import { BaseComponent } from 'src/app/shared/base/base.component';
import { DirectoryDataService } from 'src/app/core/services/data/directory/directory.data.service';
import { SubSink } from 'subsink/dist/subsink';
import { ModalComponent } from 'src/app/shared/modal/modal.component';
import { Query, DataManager, ODataV4Adaptor } from '@syncfusion/ej2-data'
import { DirectoryService } from 'src/app/core/services/business/directory/directory.service';
import { Directory } from 'src/app/core/models/entity/directory';

@Component({
  selector: 'app-admin-folder-structure',
  templateUrl: './admin-folder-structure.component.html',
  styleUrls: ['./admin-folder-structure.component.css',
    '../admin-group-edit/admin-group-media-access/admin-group-media-access.component.css']
})
export class AdminFolderStructureComponent extends BaseComponent implements OnInit, OnDestroy {

  @ViewChild('treeview') treeview: TreeViewComponent;
  @ViewChild('contentmenutree') contentmenutree: ContextMenuComponent;
  @ViewChild('confirmModal') confirmModal: ModalComponent;

  private subs = new SubSink();
  folders: Directory[];
  field: Object;
  index: number = 1;

  menuItems: MenuItemModel[] = [
    { text: 'Add New Item' },
    { text: 'Rename Item' },
    { text: 'Remove Item' },
    { text: 'Manage Metadata' }
  ];
  directoryForm: FormGroup;
  addTreeData: boolean;
  currentId: any;
  addRootTreeData: boolean;
  filteredData: Directory[];
  targetNodeId: number;
  currentFolder: Directory = { id: null, name: null, parentId: null };
  expandedNodes: string[] = [];
  isFolderInvalid: boolean;

  token = 'Bearer eyJraWQiOiJpeVRFd3BUY2pHOWZFQTJwd1d1eGo0RzBlNExCbFU4V0tfOHRkSjR6OTJJIiwiYWxnIjoiUlMyNTYifQ.eyJ2ZXIiOjEsImp0aSI6IkFULlVLX2FQb1o4RUhhNUZ6bmNFbFBPOHVYVWE1YlRpQ01wbDlVVjVmOEl4SWMiLCJpc3MiOiJodHRwczovL2Rldi0xMDQ5MTgub2t0YS5jb20vb2F1dGgyL2RlZmF1bHQiLCJhdWQiOiJhcGk6Ly9kZWZhdWx0IiwiaWF0IjoxNTcxNDIyNDM2LCJleHAiOjE1NzE0MjYwMzYsImNpZCI6IjBvYWh2amxwczFrMmRVTlhKMzU2IiwidWlkIjoiMDB1aHRlYW02cGs3U0dPTkYzNTYiLCJzY3AiOlsib3BlbmlkIiwicHJvZmlsZSIsImVtYWlsIl0sImZuYW1lIjoiSmVzdXBlbHVtaSIsImxOYW1lIjoiQWRldHVuamkiLCJzdWIiOiJqYWRldHVuamlAZW1pbmVudHRlY2hub2xvZ3kuY29tIn0.u-jBAmamXhrB6_7jKAFknU7IclrqO3htwf-9pfpZivDgbiEfdX-p4BSfVmkJf4UGNHsXw3UGeRgS9CUH9ut-n5GJYLe80C3L-HOYVg7bhOmhNNrLLODE2_D8S7h2uG3Asqj8HFCUC3spTbnlVSUIXMqXs1VEFDDJzNQtPXT8R76j5s8KhuhLgz_vWUFDs9WddgYIx1Cv2uDtT06XvvQZi9ypp7XJLHhfYm_UwllluHaePxXc_pAsmBaj5OEIu2LkmJ4Y4swA2MB_BVEL7q-9tkEh6LDyKNC0xbSmDusN0JbTS8h4ap0F9ySV5jem_bp8M7rC7zfdhGV34CHawHnWkg';

  public data: Object = new DataManager({
    url: 'http://omvapi.test.eminenttechnology.com/api/v1/directories',
    adaptor: new ODataV4Adaptor,
    headers: [{ 'Authorization': this.token }],
    crossDomain: true,
  });
  public query: Object = new Query().select('directoryId,directoryName,hasChild');
  public query1: Object = new Query().select('directoryId,directoryName,directoryParentId');
  public fields: Object = {
    dataSource: this.data, query: this.query, id: 'directoryId', text: 'directoryName', hasChildren: 'hasChild', tooltip: 'directoryName', iconCss: 'icon',
    child: { dataSource: this.data, query: this.query1, id: 'directoryId', parentID: 'directoryParentId', text: 'directoryName' }
  };
  currentTarget: HTMLLIElement;
  parentID: string;

  constructor(protected store: Store, private router: Router,
              private folderDataService: DirectoryDataService,
              private folderService: DirectoryService,
              private mediaService: MediaService) {
    super(store);
    this.showLefNav();
    this.setPageTitle('Admin Folder Structure');
  }

  ngOnInit() {
    this.getFolders();
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

  getFolders() {
    this.store.dispatch(new ShowSpinner());
    this.subs.add(
      this.folderDataService.getFolders()
        .subscribe(response => {
          if (response) {
            this.folders = response;
            this.folders.forEach(item => {
              item.icon = 'folder'; // show folder icon on only directories
              this.field = {
                dataSource: this.folders, id: 'id', parentID: 'parentId',
                text: 'name', iconCss: 'icon', hasChildren: 'hasChild'
              };
            });
            this.store.dispatch(new HideSpinner());
          }
        }, err => this.store.dispatch(new ShowErrorMessage(err)))
    );
  }

  //#region Events

  //#region TreeView

  dataBound(args: any) {
    this.treeview.expandedNodes = this.expandedNodes;
  }

  rowSelectedTreeEvent(args: any) {
    console.log('AdminFolderStructure rowSelectedTreeEvent: ');
    const id = Number(args.nodeData.id);
    this.filteredData = this.folders.filter(directories => directories.id === id);
    console.log('AdminFolderStructure rowSelectedTreeEvent: ', this.filteredData);
  }

  onNodeEdited(args: NodeEditEventArgs) {
    const errorMessage = this.validateEdit(args);
    if (errorMessage) {
      args.cancel = true;
      this.store.dispatch(new ShowErrorMessage(errorMessage));
      this.currentId = null;
      if (!args.nodeData.id) {
        const targetNodeId = args.node.id;
        this.treeview.removeNodes([targetNodeId.toString()]);
      }
      return;
    }
    this.currentId = args.nodeData.id;
    if (!args.nodeData.id) {
      // Create Folder
      this.currentFolder.name = args.newText;
      this.currentFolder.id = null;
      this.currentFolder.parentId = Number(args.nodeData.parentID);
      this.createFolder(this.currentFolder);
    }
  }

  isDragEvent: boolean;

  onNodeDropped(args: DragAndDropEventArgs) {
    this.isDragEvent = true;
    this.currentId = args.draggedNodeData.id;
  }

  dataSourceChanged(args: DataSourceChangedEventArgs) {
    if (!this.currentId) return;
    const currentFolder = args.data.find(data => data.id === Number(this.currentId));

    if (this.isDragEvent) {
      const errorMessage = this.validateMove(currentFolder);
      this.isDragEvent = false;
      if (errorMessage) {
        this.store.dispatch(new ShowErrorMessage(errorMessage));
        this.currentId = null;
        this.treeview.refresh();
        return;
      }
    }

    if (currentFolder) {
      const { id, name, parentId } = currentFolder;
      if (!name) return;
      this.expandedNodes = this.treeview.expandedNodes;
      if (id) {
        // Update Folder
        this.currentFolder.name = name.toString();
        this.currentFolder.id = Number(id);
        this.currentFolder.parentId = Number(parentId);
        this.updateFolder(this.currentFolder);
      }
    }
    this.currentId = null;
  }

  //#endregion

  //#region Context Menu

  menuClick(args: MenuEventArgs) {
    let targetNodeId: string = this.treeview.selectedNodes[0];
    this.addRootTreeData = false;
    this.addTreeData = false;
    if (args.item.text === 'Add New Item') {
      let nodeId = '';
      let item: { [key: string]: Object } = { id: nodeId, name: 'New Folder' };
      this.treeview.addNodes([item], targetNodeId, null);
      this.index++;
      this.addTreeData = true;
      this.treeview.beginEdit(nodeId);
    } else if (args.item.text === 'Remove Item') {
      this.targetNodeId = Number(targetNodeId);
      this.confirmModal.show();
    } else if (args.item.text === 'Rename Item') {
      this.treeview.beginEdit(targetNodeId);
    } else if (args.item.text === 'Manage Metadata') {
      this.store.dispatch(new SetPreviousRoute(this.router.url));
      this.router.navigate([`admin/media/folders/${targetNodeId}/metadata`]);
    }
  }

  //#endregion

  //#endregion

  addRootDirectory() {
    console.log('AdminFolderStructure addRootDirectory: ');
    let targetNodeId = undefined;
    let nodeId = '';
    let item: { [key: string]: Object } = { id: nodeId, name: 'New Folder' };
    this.treeview.addNodes([item], targetNodeId, null);
    this.addRootTreeData = true;
    this.treeview.beginEdit(nodeId);
  }

  validateEdit(args: NodeEditEventArgs): string {
    let errorMessage: string;
    this.isFolderInvalid = false;
    if (args.newText.trim() === '') {
      errorMessage = 'Folder name should not be empty';
      this.isFolderInvalid = true;
    }
    else {
      const existingFolder = this.folders.find(f => f.name.trim() == args.newText.trim() && f.parentId == args.nodeData.parentID && f.id != args.nodeData.id);
      if (existingFolder) {
        errorMessage = `${args.newText.trim()} already exists. Please use another name.`;
        this.isFolderInvalid = true;
      }
    }
    return errorMessage;
  }

  validateMove(folder: any): string {
    let errorMessage: string;

    const text = folder.name.toString();
    const parentId = folder.parentId || null;
    const existingFolder = this.folders.find(f => f.name.trim() == text.trim() && f.parentId == parentId && f.id != folder.id);

    if (existingFolder) {
      const parent = this.folders.find(f => f.id === parentId);
      const endText = parent ? ` in ${parent.name}` : '';
      errorMessage = `${text} cannot be moved. Name already exists${endText}.`;
    }
    return errorMessage;
  }

  createFolder(folder: Directory) {
    this.showSpinner();
    this.subs.sink =
      this.folderDataService.createDirectory(folder)
        .subscribe(() => {
          this.hideSpinner();
          this.store.dispatch(new ShowSuccessMessage('Folder was successfully created.'));
          this.getFolders();
          this.store.dispatch(new ShowNewFolder());
        }, error => {
          this.treeview.refresh();
          this.store.dispatch(new ShowErrorMessage(error));
        });
  }

  updateFolder(folder: Directory) {
    this.subs.sink =
      this.store.dispatch(new UpdateFolder(folder.id, folder))
        .subscribe(() => {
          this.getFolders();
          this.store.dispatch(new ShowNewFolder());
        }, () => this.treeview.refresh());
  }

  //#region Confirm Dialog

  deleteConfirmed() {
    this.treeview.removeNodes([this.targetNodeId.toString()]);
    this.subs.sink =
      this.store.dispatch(new RemoveFolder(this.targetNodeId))
        .subscribe(() => {
          this.getFolders();
        }, (error) => this.store.dispatch(new ShowErrorMessage(error)));
  }

  //#endregion

  onNodeExpanding(args: NodeExpandEventArgs) {
    if ((args.node.querySelectorAll('.e-icons.e-icon-expandable').length > 0) && args.node.querySelectorAll('ul li').length == 0) {
      this.currentTarget = args.node;
      this.parentID = args.node.getAttribute('data-uid');

      this.showSpinner();
      this.folderService.getFolders(Number(this.parentID)).toPromise()
        .then(folders => {
          folders.forEach(folder => folder.icon = 'folder');
          this.treeview.addNodes(JSON.parse(JSON.stringify(folders)), this.currentTarget);
          this.hideSpinner();
        }, err => {
          this.showErrorMessage(err);
        });
    }

  }

}
