import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminFolderMetadataComponent } from './admin-folder-metadata.component';

describe('AdminFolderMetadataComponent', () => {
  let component: AdminFolderMetadataComponent;
  let fixture: ComponentFixture<AdminFolderMetadataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminFolderMetadataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminFolderMetadataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
