import { Injectable } from '@angular/core';
import { FieldConfiguration } from "src/app/shared/dynamic-components/field-setting";
import { DirectoryDataService } from 'src/app/core/services/data/directory/directory.data.service';
import { MetadataFieldType } from 'src/app/core/enum/metadataFieldType';
import { MetadataField } from 'src/app/core/models/entity/metadata-field';
import { MetadataDataType } from 'src/app/core/enum/metadata-data-type';

@Injectable({
  providedIn: "root"
})
export class AdminFolderMetadataService {

  constructor(private foldersDataService: DirectoryDataService) { }

  async getFolderMetadataFields(folderId: number) {
    let metadataFields: FieldConfiguration[] = [];
    const responseMetadataFields = await this.foldersDataService.getMetadataFields(folderId).toPromise();
    if (responseMetadataFields) {
      responseMetadataFields.forEach(async field => {
        const config = this.buildFormField(field);
        metadataFields.push(config);
      });
    }
    return metadataFields;
  }

  buildFormField(field: MetadataField): FieldConfiguration {
    let config: FieldConfiguration;
    switch (field.type) {
      case MetadataFieldType.Text:
        config = this.buildTextBox(field);
        break;
      case MetadataFieldType.Select:
        config = this.buildDropdown(field);
        break;
      case MetadataFieldType.Date:
        config = this.buildDate(field);
        break;
      case MetadataFieldType.Nested:
        config = this.buildNestedDropdown(field);
        break;
    }
    return config;
  }

  private buildTextBox(item: MetadataField): FieldConfiguration {
    return {
      type: "input",
      data: item,
      id: item.id,
      cssClass: 'col-md-12',
      label: item.label ? item.label : item.name,
      inputType: (item.dataType === MetadataDataType.Int32
        || item.dataType === MetadataDataType.Int64
        || item.dataType === MetadataDataType.Double) ? 'number' : 'text',
      name: item.name,
      order: item.order,
      placeholder: item.name,
      required: item.isRequired,
      value: !item.dataType ? item.value || '' :
        (item.dataType === MetadataDataType.Int32 ||
          item.dataType === MetadataDataType.Int64 ||
          item.dataType === MetadataDataType.Double) ? Number(item.value) : item.value || '',
    };
  }

  private buildDropdown(item: MetadataField): FieldConfiguration {
    return {
      type: "select",
      data: item,
      id: item.id,
      cssClass: 'col-md-12',
      label: item.label ? item.label : item.name,
      name: item.name,
      order: item.order,
      options: item.options,
      value: !item.dataType ? item.value || '' :
        (item.dataType === MetadataDataType.Int32 ||
          item.dataType === MetadataDataType.Int64 ||
          item.dataType === MetadataDataType.Double) ? Number(item.value) : item.value || '',
      placeholder: 'Select',
      required: item.isRequired
    };
  }

  private buildNestedDropdown(item: MetadataField): FieldConfiguration {
    return {
      type: "select",
      data: item,
      id: item.id,
      parentId: item.parentId,
      cssClass: 'col-md-12',
      label: item.label ? item.label : item.name,
      name: item.name,
      order: item.order,
      options: item.options,
      value: !item.dataType ? item.value || '' :
        (item.dataType === MetadataDataType.Int32 ||
          item.dataType === MetadataDataType.Int64 ||
          item.dataType === MetadataDataType.Double) ? Number(item.value) : item.value || '',
      placeholder: 'Select',
      required: item.isRequired,
      isNested: true,
      isEnabled: false
    };
  }

  private buildDate(item: MetadataField): FieldConfiguration {
    return {
      type: "date",
      data: item,
      id: item.id,
      cssClass: 'col-md-12',
      label: item.label ? item.label : item.name,
      name: item.name,
      order: item.order,
      required: item.isRequired,
      value: new Date(item.value)
    };
  }
}