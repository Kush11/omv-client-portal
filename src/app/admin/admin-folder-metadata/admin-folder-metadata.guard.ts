import { Injectable } from "@angular/core";
import { CanDeactivate } from '@angular/router';
import { AdminFolderMetadataComponent } from './admin-folder-metadata.component';
import { Observable } from 'rxjs/internal/Observable';

@Injectable({
  providedIn: 'root'
})
export class AdminFolderMetadataGuard implements CanDeactivate<AdminFolderMetadataComponent> {
  canDeactivate(component: AdminFolderMetadataComponent): Observable<boolean> | Promise<boolean> | boolean {
    if (component.dynamicForm.dirty) {
      return confirm (`If you navigate away, you will lose all metadata changes.`);
    }
    return true;
  }
}