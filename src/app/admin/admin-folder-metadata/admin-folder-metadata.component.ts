import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/internal/Observable';
import { Store, Select } from '@ngxs/store';
import { GetFolder } from '../state/admin-media/admin-media.action';
import { AdminMediaState } from '../state/admin-media/admin-media.state';
import { AdminFolderMetadataService } from './admin-folder-metadata.service';
import { DynamicFormComponent } from 'src/app/shared/dynamic-components/components/dynamic-form.component';
import { FieldConfiguration } from 'src/app/shared/dynamic-components/field-setting';
import { MetadataField } from 'src/app/core/models/entity/metadata-field';
import { MetadataFieldsService } from 'src/app/core/services/business/metadata-fields/metadata-fields.service';
import { DirectoryService } from 'src/app/core/services/business/directory/directory.service';
import { ShowSuccessMessage, ShowSpinner, HideSpinner, ShowErrorMessage } from 'src/app/state/app.actions';
import { BaseComponent } from 'src/app/shared/base/base.component';
import { AppState } from 'src/app/state/app.state';
import { SubSink } from 'subsink/dist/subsink';
import { ModalComponent } from 'src/app/shared/modal/modal.component';
import { Button } from 'src/app/core/models/button';
import { MetadataFieldType } from 'src/app/core/enum/metadataFieldType';
import { MetadataDataType } from 'src/app/core/enum/metadata-data-type';
import { Directory } from 'src/app/core/models/entity/directory';

@Component({
  selector: 'app-admin-folder-metadata',
  templateUrl: './admin-folder-metadata.component.html',
  styleUrls: ['./admin-folder-metadata.component.css'],
  providers: [DirectoryService, AdminFolderMetadataService]
})
export class AdminFolderMetadataComponent extends BaseComponent implements OnInit, OnDestroy {

  @Select(AdminMediaState.getCurrentFolder) currentFolder$: Observable<Directory>;
  @Select(AppState.getPreviousRoute) previousRoute$: Observable<string>;

  @ViewChild("dynamicForm") dynamicForm: DynamicFormComponent;
  @ViewChild('metadataFieldsModal') metadataFieldsModal: ModalComponent;

  private subs = new SubSink();
  folderId: number;
  previousRoute: string;
  currentFolder: Directory;
  deleteMessage: string;
  metadataFields: MetadataField[];

  //#region Folder Metadata Fields

  existingFieldIds: number[] = [];
  currentField: FieldConfiguration;
  currentFolderMetadataFields: FieldConfiguration[] = [];
  buttons: Button[] = [
    { text: "SAVE CHANGES", type: 'button', cssClass: "button-round save-changes-qa", action: this.saveChanges.bind(this) },
    { text: "Cancel", type: 'button', cssClass: "button-no-outline cancel-qa", action: this.cancel.bind(this) }
  ];

  //#endregion

  constructor(protected store: Store, private router: Router, private route: ActivatedRoute,
    private adminFolderMetadataService: AdminFolderMetadataService, private metadataFieldsService: MetadataFieldsService,
    private foldersService: DirectoryService) {
    super(store);
    this.setPageTitle("Admin Folder Metadata");
    this.hideLeftNav();
  }

  ngOnInit() {
    this.subs.add(
      this.route.paramMap
        .subscribe(params => {
          this.folderId = Number(params.get('id'));
          this.store.dispatch(new GetFolder(this.folderId));
          this.setPageDetails();
        }),
      this.previousRoute$
        .subscribe(route => {
          const default_route = "/admin/media/folders"; // in case a user gets to this page other than by routing from the previous page
          if (route.includes(default_route)) this.previousRoute = route;
          else this.previousRoute = default_route;
        })
    );
  }

  ngOnDestroy() {
    this.currentFolderMetadataFields = [];
    this.subs.unsubscribe();
  }

  private setPageDetails() {
    this.store.dispatch(new ShowSpinner());
    // get the metadata fields for the current folder
    this.adminFolderMetadataService.getFolderMetadataFields(this.folderId)
      .then(metadataFields => {
        this.currentFolderMetadataFields = metadataFields.sort((a, b) => a.order - b.order);

        this.loadDependentDropdowns();

        this.existingFieldIds = metadataFields.map(field => field.id);

        this.subs.add(
          this.metadataFieldsService.getMediaMetadataFields()
            .subscribe(fields => {
              fields = fields.filter(f => !f.isSystem);
              fields.forEach(field => field.isChecked = this.existingFieldIds.includes(field.id));
              this.metadataFields = fields.filter(f => f.type != MetadataFieldType.Label);
              this.store.dispatch(new HideSpinner());
            }, error => this.store.dispatch(new ShowErrorMessage(error)))
        )
      }, (error) => this.store.dispatch(new ShowErrorMessage(error)));
  }

  private loadDependentDropdowns() {
    const childrenFields = this.currentFolderMetadataFields.filter(field => field.parentId);
    if (childrenFields) {
      childrenFields.forEach(child => {
        // find the selected value of parent
        const parent = this.currentFolderMetadataFields.find(x => x.id === child.parentId);
        if (parent) {
          const parentOption = parent.options.find(x => x.value === parent.value);
          if (parentOption) {
            child.isEnabled = parentOption.id ? true : false;
            child.options = child.isEnabled ? child.data.options.filter(x => x.parentId === parentOption.id) : [];
          }
        }
      });
    }
  }

  onBack(route: string) {
    this.router.navigate([route]);
  }

  saveChanges() {
    this.store.dispatch(new ShowSpinner());
    if (!this.dynamicForm) return;
    const controls = this.dynamicForm.controls;
    controls.forEach((config, index) => {
      const isLastField = index === (controls.length - 1);
      const dataType: string = config.data.dataType || MetadataDataType.String;
      const value = dataType === MetadataDataType.Int32 || dataType === MetadataDataType.Int64 ||
        dataType === MetadataDataType.Double ? Number(config.value) :
        dataType === MetadataDataType.Date ? new Date(config.value) :
          config.value;
      const payload: MetadataField = {
        ...config.data,
        isRequired: config.required,
        value: value,
        order: index
      };
      if (payload.settingId) {
        this.subs.sink =
          this.foldersService.updateMetadataField(this.folderId, payload)
            .subscribe(() => {
              if (isLastField) {
                //this.store.dispatch(new ShowSuccessMessage("Metadata Fields successfully updated!"));
                this.store.dispatch(new ShowSuccessMessage("Folder will be processed shortly!"));
                this.dynamicForm.form.reset(this.dynamicForm.form.value);
                this.store.dispatch(new HideSpinner());
                this.setPageDetails();
              }
            }, (err: any) => this.store.dispatch(new ShowErrorMessage(err)));
      } else {
        this.subs.sink =
          this.foldersService.addMetadataField(this.folderId, payload)
            .subscribe(() => {
              if (isLastField) {
                //this.store.dispatch(new ShowSuccessMessage("Metadata Fields successfully updated!"));
                this.store.dispatch(new ShowSuccessMessage("Folder will be processed shortly!"));
                this.dynamicForm.form.reset(this.dynamicForm.form.value);
                this.store.dispatch(new HideSpinner());
                this.setPageDetails();
              }
            }, (err: any) => this.store.dispatch(new ShowErrorMessage(err)));
      }
    });
  }

  cancel() {
    this.dynamicForm.reset();
    this.currentFolderMetadataFields = [];
    this.setPageDetails();
  }

  //#region Metadata Fields Modal

  addMetadataFields(fields: MetadataField[]) {
    this.resetMetadataFieldsOrder();
    fields.forEach(field => {
      const config = this.adminFolderMetadataService.buildFormField(field);
      config.order = this.currentFolderMetadataFields.length;
      this.currentFolderMetadataFields = [
        ...this.currentFolderMetadataFields,
        config
      ];
      this.dynamicForm.markFormAsTouchedAndDirty();
    });
  }

  showFieldsDialog() {
    this.metadataFieldsModal.show();
  }

  //#endregion

  //#region Confirm Dialog

  deleteField(field: FieldConfiguration) {
    this.currentFolderMetadataFields = this.currentFolderMetadataFields.filter(x => x.id !== field.id);
    if (field.data.settingId) {
      this.subs.sink = this.foldersService.deleteMetadataField(field.data.settingId)
        .subscribe(() => {
          this.store.dispatch(new ShowSuccessMessage(`${field.label} was successfully deleted`));
          this.metadataFieldsModal.uncheckField(field);
          this.resetMetadataFieldsOrder();
        }, (error) => this.store.dispatch(new ShowErrorMessage(error)));
    } else {
      this.metadataFieldsModal.uncheckField(field);
      this.resetMetadataFieldsOrder();
    }
  }

  //#endregion

  private resetMetadataFieldsOrder() {
    this.currentFolderMetadataFields.forEach((x, i) => x.order = i);
  }
}
