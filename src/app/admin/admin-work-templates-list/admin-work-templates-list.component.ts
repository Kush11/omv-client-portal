import { Component, OnInit, ViewChild } from '@angular/core';
import { GridColumn } from 'src/app/core/models/grid.column';
import { AdminWorkPlanningState } from '../state/admin-work-planning/admin-work-planning.state';
import { Select, Store } from '@ngxs/store';
import { WorkTemplate } from 'src/app/core/models/entity/work-template';
import { GetWorkTemplateGroups, SetCurrentWorkTemplateGroupId } from '../state/admin-work-planning/admin-work-planning.actions';
import { Router } from '@angular/router';
import { SetPreviousRoute, SetBreadcrumbs } from 'src/app/state/app.actions';
import { ListComponent } from 'src/app/shared/list/list.component';
import { Breadcrumb } from 'src/app/core/models/breadcrumb';
import { AppState } from 'src/app/state/app.state';
import { Observable } from 'rxjs/internal/Observable';
import { Status } from 'src/app/core/enum/status.enum';

@Component({
  selector: 'app-admin-work-templates-list',
  templateUrl: './admin-work-templates-list.component.html',
  styleUrls: ['./admin-work-templates-list.component.css']
})
export class AdminWorkTemplatesListComponent extends ListComponent implements OnInit {

  @Select(AdminWorkPlanningState.getWorkTemplateGroups) workTemplates$: Observable<WorkTemplate[]>;
  @Select(AppState.getBreadcrumbs) breadcrumbs$: Observable<Breadcrumb[]>;

  columns: GridColumn[] = [
    { headerText: 'Name', field: 'name', useAsLink: true, action: this.editTemplate.bind(this) },
    { headerText: 'Item', field: 'workItem', useAsLink: true, action: this.editTemplate.bind(this) },
    { headerText: 'Version', field: 'version', textAlign: 'Center', width: 120 },
    { headerText: 'Last Modified', field: 'modifiedOn', type: 'date', format: 'MMM dd, yyyy  hh:mm a', width: 180 },
    { headerText: 'Modified By', field: 'modifiedBy' }
  ];

  selectedWorkTemplateId: any;
  deleteMessage: string;

  constructor(protected store: Store, protected router: Router) {
    super(store);
    this.showLefNav();
    let crumbs: Breadcrumb[] = [{ name: 'Listings' }];
    this.store.dispatch(new SetBreadcrumbs(crumbs));
    this.setPageTitle("Admin Work Templates");
  }

  ngOnInit() {
    if (!this.userPermission.isClientAdmin) {
      this.router.navigate([`/unauthorize`]);
    }
    this.store.dispatch(new GetWorkTemplateGroups(Status.Draft));
    
  }

  addNewTemplate() {
    const route = this.router.url;
    this.store.dispatch(new SetPreviousRoute(route));
    this.store.dispatch(new SetCurrentWorkTemplateGroupId(null));
    this.router.navigate([`admin/work-templates/new/general-info`]);
  }

  editTemplate(data?: WorkTemplate) {
    const route = this.router.url;
    this.store.dispatch(new SetPreviousRoute(route));
    this.store.dispatch(new SetCurrentWorkTemplateGroupId(data.groupId));
    this.router.navigate([`admin/work-templates/${data.groupId}/general-info`]);
  }

  switchBreadCrumbs(breadcrumbLink: string) {
    this.router.navigate([breadcrumbLink]);
  }
}
