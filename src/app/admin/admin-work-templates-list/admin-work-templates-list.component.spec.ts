import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminWorkTemplatesListComponent } from './admin-work-templates-list.component';

describe('AdminWorkTemplateListComponent', () => {
  let component: AdminWorkTemplatesListComponent;
  let fixture: ComponentFixture<AdminWorkTemplatesListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminWorkTemplatesListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminWorkTemplatesListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
