import { AdminUserType } from './../../core/enum/admin-user-type';
import { GetActiveGroups } from '../state/admin-groups/admin-groups.action';
import { Group } from '../../core/models/entity/group';
import { ListComponent } from './../../shared/list/list.component';
import { Component, OnInit, ViewEncapsulation, ViewChild, OnDestroy, EventEmitter, Output } from '@angular/core';
import { User } from 'src/app/core/models/entity/user';
import { GridColumn } from 'src/app/core/models/grid.column';
import { DropDownListComponent } from '@syncfusion/ej2-angular-dropdowns';
import { Store, Select } from '@ngxs/store';
import { AdminUserState } from '../state/admin-users/admin-users.state';
import {
  EnableUsers, DisableUsers, UpdateUsersGroups, GetActiveUsers, GetDisabledUsers, GetUnassignedUsers, ExportActiveUsers,
  ExportDisabledUsers, ExportUnassignedUsers
} from '../state/admin-users/admin-users.actions';
import { AdminGroupState } from '../state/admin-groups/admin-groups.state';
// import { permission } from 'src/app/core/enum/permission';
import { Router, ActivatedRoute } from '@angular/router';
import { ToolBar } from 'src/app/core/models/toolbar-action';
import { ShowSpinner, SetPreviousRoute } from 'src/app/state/app.actions';
import { SubSink } from 'subsink/dist/subsink';
import { Observable } from 'rxjs/internal/Observable';
import { ModalComponent } from 'src/app/shared/modal/modal.component';
import { GroupsService } from 'src/app/core/services/business/groups/groups.service';
import { AdminUsersListService } from './admin-users-list.service';

const STATUS_ENABLE = 'Enable';
const STATUS_DISABLE = 'Disable';
const ACTIVE_USERS = 0;
const UNASSIGNED_USERS = 1;
const DISABLED_USERS = 2;

@Component({
  selector: 'app-admin-users-list',
  templateUrl: './admin-users-list.component.html',
  styleUrls: ['./admin-users-list.component.css'],
  providers: [GroupsService],
  encapsulation: ViewEncapsulation.None
})
export class AdminUsersListComponent extends ListComponent implements OnInit, OnDestroy {

  private subs = new SubSink();
  selectedUsers: User[] = [];
  selectedGroups: Group[] = [];
  users: User[];
  usersToExport: User[];
  status = STATUS_ENABLE;
  ENABLE = 'Enable';
  DISABLE = 'Disable';
  groupFields = { text: 'name', value: 'id' };
  groupid: number;
  searchName = '';
  urlparam: string;
  columns: GridColumn[] = [
    { type: 'checkbox', headerText: 'Select All', width: '50', field: '', customAttributes: { class: 'user-check-qa' } },
    { type: '', headerText: 'Name', field: 'displayName', useAsLink: true },
    { type: '', headerText: 'First Name', field: 'firstName', visible: false },
    { type: '', headerText: 'Last Name', field: 'lastName', visible: false },
    { type: '', headerText: 'User Name', field: 'displayName', visible: false },
    { type: '', headerText: 'Email', field: 'emailAddress', useAsLink: true },
    { type: 'date', headerText: 'Last Modified', width: 180, field: 'modifiedOn', format: 'MM/dd/yyyy' },
    { type: '', headerText: 'Group', field: 'groups', width: 180 },
    { type: '', headerText: 'Last  Modified', width: 180, field: 'modifiedOnString', visible: false },
  ];
  toolbar: ToolBar[] = [
    { text: '', cssClass: 'toolbar-enable-disable-button-qa', disabled: this.selectedUsers.length < 1, action: this.changeStatus.bind(this) },
    { text: 'Assign to Groups', cssClass: 'toolbar-assign-to-groups-button-qa', disabled: this.selectedUsers.length < 1, action: this.assignToGroups.bind(this) },
  ];
  currentTab: number;
  currentPage = 1;
  pageSize = 20;
  totalUsers: number;
  isUnassignedUsers: boolean;
  selectedUserIds: number[] = [];

  @Select(AdminUserState.getActiveUsers) activeUsers$: Observable<User[]>;
  @Select(AdminUserState.getActiveUsersForExport) activeUsersExport$: Observable<User[]>;
  @Select(AdminUserState.getUnassignedUsers) unassignedUsers$: Observable<User[]>;
  @Select(AdminUserState.getUnassignedUsersForExport) unassignedUsersExport$: Observable<User[]>;
  @Select(AdminUserState.getDisabledUsers) disabledUsers$: Observable<User[]>;
  @Select(AdminUserState.getDisabledUsersForExport) disabledUsersExport$: Observable<User[]>;
  @Select(AdminUserState.getTotalUsers) totalUsers$: Observable<number>;
  @Select(AdminUserState.getIsProcessComplete) isProcessComplete$: Observable<boolean>;
  @Select(AdminGroupState.getActiveGroups) groups$: Observable<Group[]>;

  @ViewChild('usersList') usersList: ListComponent;
  @ViewChild('groupSelect') groupSelect: DropDownListComponent;
  @ViewChild('listviewModal') listviewModal: ModalComponent;

  @Output() exportExcel = new EventEmitter<string>();

  constructor(protected store: Store, protected router: Router, private activatedRoute: ActivatedRoute, private service: AdminUsersListService) {
    super(store);
    // this.Permission = permission.VIEW_USERS;
    this.showLefNav();
    this.setPageTitle('Admin Users');
  }

  ngOnInit() {
    this.store.dispatch(new GetActiveGroups());
    this.subs.add(
      this.activatedRoute.params
        .subscribe(params => {
          this.selectedUsers = [];
          this.displayUsers(params.type);
          this.groupid = null;
          this.searchName = '';
        }),
      this.activeUsers$
        .subscribe(activeUsers => this.users = activeUsers),
      this.unassignedUsers$
        .subscribe(unassignedUsers => this.users = unassignedUsers),
      this.disabledUsers$
        .subscribe(disabledUsers => this.users = disabledUsers),
      this.totalUsers$
        .subscribe(total => {
          this.totalUsers = total;
          const pageCount = Math.ceil(this.totalUsers / this.pageSize);
          this.pageCount = pageCount ? pageCount : 1;
        }),
      this.isProcessComplete$
        .subscribe(isComplete => {
          if (isComplete) this.getUsers();
        }),
    );
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

  displayUsers(param: string) {
    this.urlparam = param;
    switch (param) {
      case AdminUserType.Active:
        this.isUnassignedUsers = false;
        this.store.dispatch(new GetActiveUsers());
        this.currentTab = ACTIVE_USERS;
        this.setToobarProperties();
        break;
      case AdminUserType.Unassigned:
        this.isUnassignedUsers = true;
        this.store.dispatch(new GetUnassignedUsers());
        this.currentTab = UNASSIGNED_USERS;
        this.setToobarProperties();
        break;
      case AdminUserType.Disabled:
        this.isUnassignedUsers = false;
        this.store.dispatch(new GetDisabledUsers());
        this.currentTab = DISABLED_USERS;
        this.setToobarProperties();
        break;
      default:
        break;
    }
  }

  search() {
    this.currentPage = 1;
    this.getUsers();
  }

  exportToExcel() {
    if (this.currentTab === ACTIVE_USERS) {
      this.store.dispatch(new ExportActiveUsers(this.searchName, this.groupid, this.currentPage, 1000));
      this.subs.sink = this.activeUsersExport$.subscribe(activeUsersExport => this.usersToExport = activeUsersExport)
    } else if (this.currentTab === DISABLED_USERS) {
      this.store.dispatch(new ExportDisabledUsers(this.searchName, this.groupid, this.currentPage, 1000));
      this.subs.sink = this.disabledUsersExport$.subscribe(disabledUsersExport => this.usersToExport = disabledUsersExport)
    } else if (this.currentTab === UNASSIGNED_USERS) {
      this.store.dispatch(new ExportUnassignedUsers(this.searchName, this.groupid, this.currentPage, 1000));
      this.subs.sink = this.unassignedUsersExport$.subscribe(unassignedUsersExport => this.usersToExport = unassignedUsersExport)
    }
    this.usersList.excelExport(this.usersToExport, this.currentTab);
  }

  rowSelectedEvent(users: User[]) {
    users.forEach(user => this.addUser(user));
    this.setToobarProperties();
  }

  rowDeselectedEvent(users: User[]) {
    users.forEach(user => this.removeUser(user));
    this.setToobarProperties();
  }

  private addUser(user: User) {
    const selectedUserIds = this.selectedUsers.map(x => x.id);
    if (!selectedUserIds.includes(user.id)) {
      this.selectedUsers = [
        ...this.selectedUsers,
        user
      ];
    }
  }

  private removeUser(user: User) {
    const selectedUserIds = this.selectedUsers.map(x => x.id);
    if (selectedUserIds.includes(user.id)) {
      this.selectedUsers = this.selectedUsers.filter(x => x.id !== user.id);
    }
  }

  assignToGroups() {
    this.listviewModal.show();
  }

  addNewUser() {
    this.store.dispatch(new SetPreviousRoute(this.router.url));
    this.router.navigate([`/admin/users/0/edit`]);
  }

  editUser(data?: User) {
    this.store.dispatch(new SetPreviousRoute(this.router.url));
    this.router.navigate([`/admin/users/${data.id}/edit`]);
  }

  changeStatus() {
    if (this.currentTab === DISABLED_USERS) {
      this.store.dispatch(new EnableUsers(this.selectedUsers)).toPromise()
        .then(() => {
          this.selectedUsers = [];
          this.setToobarProperties()
        });
    } else {
      this.store.dispatch(new DisableUsers(this.selectedUsers)).toPromise()
        .then(() => {
          this.selectedUsers = [];
          this.setToobarProperties()
        });
    }
  }

  modalRowSelected(groups: Group[]) {
    this.selectedGroups = groups;
  }

  clearGroupSelection() {
    this.listviewModal.clearSelection();
  }

  saveGroups() {
    this.store.dispatch(new ShowSpinner());
    let groupIds: any[] = [];
    let userIds: number[] = [];
    this.selectedGroups.forEach(group => groupIds.push(group.id));
    this.selectedUsers.forEach(x => userIds.push(x.id))

    this.subs.sink = this.store.dispatch(new UpdateUsersGroups(userIds, groupIds)).
      subscribe(() => {
        this.selectedUsers = [];
        this.setToobarProperties()
      });
    this.clearGroupSelection();
  }

  paginationEvent(pageNumber: number) {
    this.currentPage = pageNumber;
    this.getUsers();
    window.scrollTo(0, 0);
  }

  private getUsers() {
    if (this.currentTab === ACTIVE_USERS) {
      this.store.dispatch(new GetActiveUsers(this.searchName, this.groupid, this.currentPage, this.pageSize));
    } else if (this.currentTab === DISABLED_USERS) {
      this.store.dispatch(new GetDisabledUsers(this.searchName, this.groupid, this.currentPage, this.pageSize));
    } else if (this.currentTab === UNASSIGNED_USERS) {
      this.store.dispatch(new GetUnassignedUsers(this.searchName, this.groupid, this.currentPage, this.pageSize));
    }
  }

  private setToobarProperties() {
    this.selectedUserIds = this.selectedUsers.map(x => x.id);
    this.toolbar.map((x, i) => {
      if (i === 0) {
        x.text = this.currentTab === ACTIVE_USERS ? STATUS_DISABLE : this.currentTab === DISABLED_USERS ? STATUS_ENABLE : '';
      }
      x.disabled = this.selectedUsers.length < 1;
    });
  }
}
