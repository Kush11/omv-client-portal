import { Injectable } from '@angular/core';
import { Store } from '@ngxs/store';
import { GetActiveGroups } from '../state/admin-groups/admin-groups.action';
import { User } from 'src/app/core/models/entity/user';

@Injectable({
  providedIn: 'root'
})
export class AdminUsersListService {

  users: User[];

  constructor(protected store: Store) { 
    
  }

  getActiveGroups() {
    this.store.dispatch(new GetActiveGroups());
  }
}
