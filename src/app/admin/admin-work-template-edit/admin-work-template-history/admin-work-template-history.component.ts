import { Component, OnInit } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { AdminWorkPlanningState } from '../../state/admin-work-planning/admin-work-planning.state';
import { Observable } from 'rxjs/internal/Observable';
import { GridColumn } from 'src/app/core/models/grid.column';
import { SubSink } from 'subsink/dist/subsink';
import { GetWorkTemplateHistory } from '../../state/admin-work-planning/admin-work-planning.actions';
import { WorkTemplateHistory } from 'src/app/core/models/entity/work-template';
import { BaseComponent } from 'src/app/shared/base/base.component';

@Component({
  selector: 'app-admin-work-template-history',
  templateUrl: './admin-work-template-history.component.html',
  styleUrls: ['./admin-work-template-history.component.css']
})
export class AdminWorkTemplateHistoryComponent extends BaseComponent implements OnInit {

  @Select(AdminWorkPlanningState.getWorkTemplateHistory) history$: Observable<WorkTemplateHistory[]>;
  @Select(AdminWorkPlanningState.getCurrentGroupId) groupId$: Observable<number>;
  @Select(AdminWorkPlanningState.getCurrentTemplateId) currentTemplateId$: Observable<number>;
  @Select(AdminWorkPlanningState.getTotalWorkTemplateHistory) totalHistory$: Observable<number>;

  private subs = new SubSink();
  columns: GridColumn[] = [
    { headerText: 'Date', field: 'createdOn', type: 'date', format: 'MMM dd, yyyy  hh:mm a' },
    { headerText: 'Activated By', field: 'createdBy' },
    { headerText: 'Event', field: 'event' }
  ];
  currentPage = 1;
  pageSize = 12;
  currentWorkTemplateId: number;

  constructor(protected store: Store) {
    super(store);
    this.setPageTitle("Admin Work Template History");
  }

  ngOnInit() {
    this.subs.add(
      this.currentTemplateId$
        .subscribe(id => {
          this.currentWorkTemplateId = id;
          this.store.dispatch(new GetWorkTemplateHistory(this.currentWorkTemplateId, this.currentPage, this.pageSize));
        })
    );
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

  paginationEvent(pageNumber: number) {
    this.currentPage = pageNumber;
    this.store.dispatch(new GetWorkTemplateHistory(this.currentWorkTemplateId, pageNumber, this.pageSize));
    window.scrollTo(0, 0);
  }
}
