import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminWorkTemplateHistoryComponent } from './admin-work-template-history.component';

describe('AdminWorkTemplateHistoryComponent', () => {
  let component: AdminWorkTemplateHistoryComponent;
  let fixture: ComponentFixture<AdminWorkTemplateHistoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminWorkTemplateHistoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminWorkTemplateHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
