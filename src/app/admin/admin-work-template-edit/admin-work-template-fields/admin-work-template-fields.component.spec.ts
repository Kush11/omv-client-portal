import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminWorkTemplateFieldsComponent } from './admin-work-template-fields.component';

describe('AdminWorkTemplateFieldsComponent', () => {
  let component: AdminWorkTemplateFieldsComponent;
  let fixture: ComponentFixture<AdminWorkTemplateFieldsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminWorkTemplateFieldsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminWorkTemplateFieldsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
