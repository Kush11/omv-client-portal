import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { BaseComponent } from 'src/app/shared/base/base.component';
import { Store, Select } from '@ngxs/store';
import { AdminWorkPlanningState } from '../../state/admin-work-planning/admin-work-planning.state';
import { Observable } from 'rxjs/internal/Observable';
import { ModalComponent } from 'src/app/shared/modal/modal.component';
import { SubSink } from 'subsink/dist/subsink';
import { FormGroup } from '@angular/forms';
import { Button } from 'src/app/core/models/button';
import { AdminWorkTemplateFieldsService } from './admin-work-template-fields.service';
import { FieldConfiguration } from 'src/app/shared/dynamic-components/field-setting';
import { MetadataField } from 'src/app/core/models/entity/metadata-field';
import { DynamicFormComponent } from 'src/app/shared/dynamic-components/components/dynamic-form.component';
import { WorkTemplateField } from 'src/app/core/models/entity/work-template';
import { UpdateWorkTemplateFields } from '../../state/admin-work-planning/admin-work-planning.actions';
import {
  ShowErrorMessage,
  ShowWarningMessage
} from 'src/app/state/app.actions';

@Component({
  selector: 'app-admin-work-template-fields',
  templateUrl: './admin-work-template-fields.component.html',
  styleUrls: ['./admin-work-template-fields.component.css'],
  providers: [AdminWorkTemplateFieldsService]
})
export class AdminWorkTemplateFieldsComponent extends BaseComponent implements OnInit, OnDestroy {

  @Select(AdminWorkPlanningState.isReadOnly) isReadOnlyMode$: Observable<boolean>;
  @Select(AdminWorkPlanningState.getCurrentTemplateId) currentTemplateId$: Observable<number>;

  @ViewChild('metadataFieldsModal') metadataFieldsModal: ModalComponent;
  @ViewChild('confirmModal') confirmModal: ModalComponent;
  @ViewChild('processDetailModal') processDetailsModal: ModalComponent;
  @ViewChild('dynamicForm') dynamicForm: DynamicFormComponent;

  private subs = new SubSink();
  fieldForm: FormGroup;
  currentWorkTemplateId: number;
  templateFields: FieldConfiguration[] = [];
  metadataFields: MetadataField[];
  buttons: Button[] = [{ text: 'Add Item Field', type: 'button', action: this.showFieldsModal.bind(this), cssClass: 'button-plus add-process-btn-qa' }];
  formButtons: Button[] = [
    { text: 'SAVE CHANGES', type: 'button', action: this.saveChanges.bind(this), cssClass: 'button-round save-changes-button-qa' },
    { text: 'Cancel', type: 'button', action: this.cancel.bind(this), cssClass: 'button-no-outline cancel-button-qa', override: true }
  ];

  constructor(protected store: Store, private service: AdminWorkTemplateFieldsService) {
    super(store);
    this.setPageTitle('Admin Work Template Fields');
  }

  ngOnInit() {
    this.subs.add(
      this.currentTemplateId$
        .subscribe(templateId => {
          this.currentWorkTemplateId = templateId;
          this.setFields();
        })
    );
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

  showFieldsModal() {
    this.metadataFieldsModal.show();
  }

  private setFields() {
    this.showSpinner();
    this.service.getFields(this.currentWorkTemplateId).then(
      fields => {
        this.templateFields = fields;
        this.metadataFields = this.service.metadataFields;
        this.checkUniqueRule();
        this.hideSpinner();
      },
      error => this.store.dispatch(new ShowErrorMessage(error))
    );
  }

  //#region Metadata Fields Modal

  addMetadataFields(fields: MetadataField[]) {
    fields.forEach(field => {
      field.isEditable = true;
      field.isRequired = false;
      field.allowDocumentUpload = true;
      const config = this.service.buildField(field);

      this.templateFields = [...this.templateFields, config];
      this.dynamicForm.markFormAsTouchedAndDirty();
    });
    this.checkUniqueRule();
  }

  //#endregion

  //#region Work Template Form Fields

  checkUniqueRule() {
    const uniqueControlNames = this.templateFields
      .filter(f => f.unique)
      .map(x => x.name);
    if (uniqueControlNames.length > 1) {
      this.disableNonUniqueControls(uniqueControlNames);
    } else {
      this.enableAllControls();
    }
  }

  disableNonUniqueControls(uniqueControls: string[]) {
    this.templateFields.forEach(
      f => (f.uniqueDisabled = uniqueControls.indexOf(f.name) === -1)
    );
  }

  enableAllControls() {
    this.templateFields.forEach(f => (f.uniqueDisabled = false));
  }

  saveChanges() {
    const uniqueControlNames = this.templateFields.find(f => f.unique);
    if (!uniqueControlNames) {
      this.store.dispatch(new ShowWarningMessage('Please select at least one unique field.'));
      return;
    } else {
      if (this.dynamicForm) {
        let fields: WorkTemplateField[] = [];
        this.dynamicForm.controls.forEach(control => {
          let field: WorkTemplateField = {
            name: control.name,
            isUnique: control.unique || false
          };
          fields = [...fields, field];
        });
        this.store.dispatch(new UpdateWorkTemplateFields(this.currentWorkTemplateId, fields))
          .toPromise()
          .then(() => {
            this.dynamicForm.form.reset();
            this.setFields();
          });
      }
    }
  }

  cancel() {
    this.setFields();
    this.dynamicForm.form.reset();
  }

  //#endregion

  //#region Confirm Dialog

  deleteField(field: FieldConfiguration) {
    this.templateFields = this.templateFields.filter(x => x.id !== field.id);
    this.metadataFieldsModal.uncheckField(field);
    this.dynamicForm.markFormAsTouchedAndDirty();
    this.checkUniqueRule();
  }

  //#endregion
}
