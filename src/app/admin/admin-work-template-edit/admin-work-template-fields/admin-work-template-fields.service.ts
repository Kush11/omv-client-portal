import { Injectable } from '@angular/core';
import { FieldConfiguration, FieldType } from 'src/app/shared/dynamic-components/field-setting';
import { MetadataField } from 'src/app/core/models/entity/metadata-field';
import { MetadataFieldType } from 'src/app/core/enum/metadataFieldType';
import { WorkTemplatesService } from 'src/app/core/services/business/work-templates/work-templates.service';
import { MetadataFieldsService } from 'src/app/core/services/business/metadata-fields/metadata-fields.service';

@Injectable({
  providedIn: "root"
})
export class AdminWorkTemplateFieldsService {

  metadataFields: MetadataField[] = [];

  constructor(private workTemplatesService: WorkTemplatesService, private metadataFieldsService: MetadataFieldsService) { }

  async getFields(templateId: number): Promise<FieldConfiguration[]> {
    // Step 1: Build the form header
    const header = this.buildHeader();
    let fieldConfigs: FieldConfiguration[] = [];
    fieldConfigs = [...fieldConfigs, header];

    // Step 2: Get work template Fields
    const templateFieldsModel = await this.workTemplatesService.getFields(templateId);
    let templateFields = templateFieldsModel.fields;

    // Step 3: Get all work planning metadatafields
    return await this.metadataFieldsService.getWorkPlanningMetadataFields().toPromise()
      .then(metadataFields => {
        if (metadataFields) {
          this.metadataFields = metadataFields.filter(mf => mf.type !== MetadataFieldType.Label);
          const templateFieldNames: string[] = templateFields ? templateFields.map(x => x.name) : [];
          metadataFields.forEach(async field => {
            if (templateFieldNames.indexOf(field.name) !== -1) {
              field.isChecked = true;
              field.isUnique = templateFields.find(x => x.name === field.name).isUnique;
              const config = this.buildField(field);
              fieldConfigs = [...fieldConfigs, config];
            }
          });
        }
        return fieldConfigs;
      });
  }

  clearFormFields() {
    const header = this.buildHeader();
    let metadataFields: FieldConfiguration[] = [];
    metadataFields = [...metadataFields, header];
    return metadataFields;
  }

  buildField(field: MetadataField): FieldConfiguration {
    let config: FieldConfiguration;
    switch (field.type) {
      case MetadataFieldType.Text:
        config = this.buildTextBox(field);
        break;
      case MetadataFieldType.Select:
        config = this.buildDropdown(field);
        break;
      case MetadataFieldType.Date:
        config = this.buildDate(field);
        break;
      case MetadataFieldType.Nested:
        config = this.buildDependentDropdown(field);
        break;
    }
    return config;
  }

  private buildTextBox(item: MetadataField): FieldConfiguration {
    return {
      type: "input",
      data: item,
      id: item.id,
      disabled: true,
      cssClass: 'col-md-12',
      label: item.label || item.name,
      overrideLabel: item.overrideLabel,
      inputType: "text",
      name: item.name,
      order: item.order,
      placeholder: item.name,
      unique: item.isUnique
    };
  }

  private buildDropdown(item: MetadataField): FieldConfiguration {
    return {
      type: "select",
      data: item,
      id: item.id,
      disabled: true,
      cssClass: 'col-md-12',
      label: item.label || item.name,
      name: item.name,
      order: item.order,
      unique: item.isUnique
    };
  }

  private buildDependentDropdown(item: MetadataField): FieldConfiguration {
    return {
      type: "select",
      data: item,
      id: item.id,
      parentId: item.parentId,
      disabled: true,
      cssClass: 'col-md-12',
      label: item.label,
      name: item.name,
      order: item.order,
      unique: item.isUnique,
      isNested: true,
      isEnabled: false
    };
  }

  private buildDate(item: MetadataField): FieldConfiguration {
    return {
      type: "date",
      data: item,
      id: item.id,
      cssClass: 'col-md-12',
      label: item.label,
      name: item.name,
      order: item.order,
      unique: item.isUnique,
      disabled: true,
      value: item.value
    };
  }

  private buildHeader(): FieldConfiguration {
    return {
      type: FieldType.Header,
      label: '',
      name: '_header_'
    };
  }
}