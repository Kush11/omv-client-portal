import { Injectable } from '@angular/core';
import { FieldConfiguration, FieldType } from "src/app/shared/dynamic-components/field-setting";
import { MetadataFieldType } from 'src/app/core/enum/metadataFieldType';
import { MetadataField } from 'src/app/core/models/entity/metadata-field';
import { WorkTemplatesService } from 'src/app/core/services/business/work-templates/work-templates.service';

@Injectable({
  providedIn: "root"
})
export class AdminWorkTemplateFormsService {

  constructor(private workTemplatesService: WorkTemplatesService) { }

  async getFormFields(stepId: number): Promise<FieldConfiguration[]> {
    const header = this.buildHeader();
    let metadataFields: FieldConfiguration[] = [];
    metadataFields = [...metadataFields, header];
    return await this.workTemplatesService.getForms(stepId)
      .then(formFields => {
        if (formFields) {
          formFields.forEach(async field => {
            const config = this.buildFormField(field);
            metadataFields = [...metadataFields, config];
          });
        }
        return metadataFields;
      });
  }

  clearFormFields() {
    const header = this.buildHeader();
    let metadataFields: FieldConfiguration[] = [];
    metadataFields = [...metadataFields, header];
    return metadataFields;
  }

  buildFormField(field: MetadataField): FieldConfiguration {
    let config: FieldConfiguration;
    switch (field.type) {
      case MetadataFieldType.Text:
        config = this.buildTextBox(field);
        break;
      case MetadataFieldType.Select:
        config = this.buildDropdown(field);
        break;
      case MetadataFieldType.Date:
        config = this.buildDate(field);
        break;
      case MetadataFieldType.Nested:
        config = this.buildDependentDropdown(field);
        break;
    }
    return config;
  }

  private buildTextBox(item: MetadataField): FieldConfiguration {
    return {
      type: "input",
      data: item,
      id: item.id,
      disabled: true,
      cssClass: 'col-md-12',
      label: item.label ? item.label : item.name,
      overrideLabel: item.overrideLabel,
      inputType: "text",
      name: item.name,
      order: item.order,
      placeholder: item.name,
      required: item.isRequired,
      editable: item.isEditable,
      allowDocumentUpload: item.allowDocumentUpload,
      value: item.value ? item.value : '',
    };
  }

  private buildDropdown(item: MetadataField): FieldConfiguration {
    return {
      type: "select",
      data: item,
      id: item.id,
      disabled: true,
      cssClass: 'col-md-12',
      label: item.label ? item.label : item.name,
      overrideLabel: item.overrideLabel,
      name: item.name,
      order: item.order,
      required: item.isRequired,
      editable: item.isEditable,
      allowDocumentUpload: item.allowDocumentUpload,
    };
  }

  private buildDependentDropdown(item: MetadataField): FieldConfiguration {
    return {
      type: "select",
      data: item,
      id: item.id,
      parentId: item.parentId,
      disabled: true,
      cssClass: 'col-md-12',
      label: item.label ? item.label : item.name,
      overrideLabel: item.overrideLabel,
      name: item.name,
      order: item.order,
      required: item.isRequired,
      editable: item.isEditable,
      allowDocumentUpload: item.allowDocumentUpload,
      isNested: true,
      isEnabled: false
    };
  }

  private buildDate(item: MetadataField): FieldConfiguration {
    return {
      type: "date",
      data: item,
      id: item.id,
      cssClass: 'col-md-12',
      label: item.label ? item.label : item.name,
      overrideLabel: item.overrideLabel,
      name: item.name,
      order: item.order,
      required: item.isRequired,
      editable: item.isEditable,
      allowDocumentUpload: item.allowDocumentUpload,
      disabled: true,
      value: item.value
    };
  }

  private buildHeader(): FieldConfiguration {
    return {
      type: FieldType.Header,
      label: '',
      name: '_header_',
      order: -1001
    };
  }
}