import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminWorkTemplateFormsComponent } from './admin-work-template-forms.component';

describe('AdminWorkTemplateFormsComponent', () => {
  let component: AdminWorkTemplateFormsComponent;
  let fixture: ComponentFixture<AdminWorkTemplateFormsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminWorkTemplateFormsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminWorkTemplateFormsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
