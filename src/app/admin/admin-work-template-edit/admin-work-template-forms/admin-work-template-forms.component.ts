import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { AdminWorkPlanningState } from '../../state/admin-work-planning/admin-work-planning.state';
import { WorkTemplateProcess, WorkTemplateStep, WorkTemplate } from 'src/app/core/models/entity/work-template';
import { Observable } from 'rxjs/internal/Observable';
import { SubSink } from 'subsink/dist/subsink';
import { SetCurrentWorkTemplateProcess } from '../../state/admin-work-planning/admin-work-planning.actions';
import { ModalComponent } from 'src/app/shared/modal/modal.component';
import { DynamicFormComponent } from 'src/app/shared/dynamic-components/components/dynamic-form.component';
import { MetadataField } from 'src/app/core/models/entity/metadata-field';
import { FieldConfiguration, FieldType } from 'src/app/shared/dynamic-components/field-setting';
import { AdminWorkTemplateFormsService } from './admin-work-template-forms.service';
import { MetadataFieldsService } from 'src/app/core/services/business/metadata-fields/metadata-fields.service';
import { ShowSpinner, HideSpinner, ShowErrorMessage, ShowSuccessMessage } from 'src/app/state/app.actions';
import { Button } from 'src/app/core/models/button';
import { WorkTemplatesService } from 'src/app/core/services/business/work-templates/work-templates.service';
import { FormGroup } from '@angular/forms';
import { MetadataFieldType } from 'src/app/core/enum/metadataFieldType';
import { BaseComponent } from 'src/app/shared/base/base.component';
import { FieldsService } from 'src/app/core/services/business/fields/fields.service';
import { SortType, SortDirection } from 'src/app/core/enum/sort-enum';

@Component({
  selector: 'app-admin-work-template-forms',
  templateUrl: './admin-work-template-forms.component.html',
  styleUrls: ['./admin-work-template-forms.component.css'],
  providers: [AdminWorkTemplateFormsService]
})
export class AdminWorkTemplateFormsComponent extends BaseComponent implements OnInit, OnDestroy {

  @Select(AdminWorkPlanningState.getProcesses) processes$: Observable<WorkTemplateProcess[]>;
  @Select(AdminWorkPlanningState.getSteps) steps$: Observable<WorkTemplateStep[]>;
  @Select(AdminWorkPlanningState.getCurrentStep) currentStep$: Observable<WorkTemplateStep>;
  @Select(AdminWorkPlanningState.getCurrentGroupId) currentWorkGroupId$: Observable<number>;
  @Select(AdminWorkPlanningState.isReadOnly) isReadOnlyMode$: Observable<boolean>;

  @ViewChild('metadataFieldsModal') metadataFieldsModal: ModalComponent;
  @ViewChild("labelOverrideModal") labelOverrideModal: ModalComponent;
  @ViewChild('confirmModal') confirmModal: ModalComponent;
  @ViewChild("dynamicForm") dynamicForm: DynamicFormComponent;

  private subs = new SubSink();

  processFields = { text: 'name', value: 'id' };
  selectedProcessId: number;
  stepFields = { text: 'name', value: 'id' };
  selectedStepId: number;
  steps: WorkTemplateStep[];
  currentStep: WorkTemplateStep;
  deleteMessage: string;
  metadataFields: MetadataField[];

  //#region Work Template Form Fields

  currentFormFields: FieldConfiguration[] = [];
  existingFieldIds: number[] = [];
  currentFormField: FieldConfiguration;
  buttons: Button[] = [
    { text: "SAVE CHANGES", type: 'button', action: this.saveChanges.bind(this), cssClass: "button-round save-changes-button-qa" },
    { text: "Cancel", type: 'button', action: this.cancel.bind(this), cssClass: "button-no-outline cancel-button-qa" },
  ];

  //#endregion

  //#region Override Label Modal

  overrrideLabelControls: FieldConfiguration[] = [
    { type: "input", label: "Override Label", inputType: "text", name: "overrideLabel", required: true, validationMessage: "Please enter a label" },
    { type: "input", label: "Default Value", inputType: "text", name: "defaultValue", placeholder: "Enter Sign-Off Default Value" }
  ];
  isReadOnlyMode: boolean;

  //#endregion

  constructor(protected store: Store, private metadataFieldsService: MetadataFieldsService, private fieldsService: FieldsService,
    private formService: AdminWorkTemplateFormsService, private workTemplatesService: WorkTemplatesService) {
    super(store);
    this.setPageTitle("Admin Work Template Forms");
  }

  ngOnInit() {
    this.subs.add(
      this.processes$
        .subscribe(processes => {
          if (processes.length > 0) {
            const process = processes[0];
            this.selectedProcessId = process.id;
            this.store.dispatch(new SetCurrentWorkTemplateProcess(process));
          }
        }),
      this.steps$
        .subscribe(steps => {
          this.steps = steps;
          if (steps.length > 0) {
            const step = steps[0];
            this.currentStep = step;
            this.selectedStepId = this.currentStep.id;
          }
        }),
      this.currentStep$
        .subscribe(step => {
          this.currentStep = step;
          this.setFormFields();
        }),
    );
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

  private setFormFields() {
    if (!this.currentStep) {
      this.currentFormFields = this.formService.clearFormFields();
      return;
    }
    this.showSpinner();
    const currentTemplate = this.store.selectSnapshot(AdminWorkPlanningState.getCurrentTemplate);
    const fieldIds = currentTemplate.fields.map(x => x.id);

    // Step 1: Get all form metadata fields
    this.formService.getFormFields(this.currentStep.id)
      .then(formFields => {
        this.currentFormFields = formFields;
        this.currentFormFields.sort((a, b) => a.order - b.order);

        // Step2: Store common fields
        this.existingFieldIds = formFields.map(f => f.id);

        // Step 3: Get all metadata fields
        this.subs.sink =
          this.metadataFieldsService.getWorkPlanningMetadataFields()
            .subscribe(metadataFields => {
              metadataFields = metadataFields.filter(mf => mf.type !== MetadataFieldType.Label && !fieldIds.includes(mf.id));
              // Step 4: Mark common fields as selected in the modal
              metadataFields.forEach(f => f.isChecked = this.existingFieldIds.includes(f.id));
              this.metadataFields = this.fieldsService.sort(metadataFields, 'label', SortType.String, SortDirection.Ascending);

              this.hideSpinner();
            }, err => this.store.dispatch(new ShowErrorMessage(err)));
      }, err => this.store.dispatch(new ShowErrorMessage(err)));
  }

  //#region Process & Steps 

  processChanged(args: any) {
    const process: WorkTemplateProcess = args.itemData;
    this.store.dispatch(new SetCurrentWorkTemplateProcess(process));
  }

  stepChanged(args: any) {
    this.currentStep = args.itemData;
    this.setFormFields();
  }

  addField() {
    this.metadataFieldsModal.show();
  }

  //#endregion

  //#region Work Template Form Fields

  saveChanges() {
    if (this.dynamicForm) {
      let createdCount = 0; // for testing
      let updatedCount = 0; // for testing
      const controls = this.dynamicForm.controls;
      controls.forEach((config, index) => {
        const isLastField = index === (controls.length - 1);
        const field: MetadataField = {
          ...config.data,
          isRequired: config.required,
          isEditable: config.editable,
          allowDocumentUpload: config.allowDocumentUpload,
          overrideLabel: config.overrideLabel,
          value: config.value,
          order: index
        };
        if (field.settingId) {
          updatedCount++;
          this.subs.sink =
            this.workTemplatesService.updateForm(this.currentStep.id, field)
              .then(() => {
                if (isLastField) {
                  this.store.dispatch(new ShowSuccessMessage("Form Fields successfully updated!"));
                  this.setFormFields();
                }
              }, (error) => this.store.dispatch(new ShowErrorMessage(error)))
        } else {
          createdCount++;
          this.subs.sink =
            this.workTemplatesService.createForm(this.currentStep.id, field)
              .then(() => {
                if (isLastField) {
                  this.store.dispatch(new ShowSuccessMessage("Form Fields successfully updated!"));
                  this.setFormFields();
                }
              }, (error) => this.store.dispatch(new ShowErrorMessage(error)))
        }
      });
      console.log(`WorkTemplateForms analysis: ${createdCount} created and ${updatedCount} updated.`);
      this.dynamicForm.form.reset(this.dynamicForm.form.value);
    }
  }

  cancel() {
    this.setFormFields();
    this.dynamicForm.form.reset();
  }

  //#endregion

  //#region Metadata Fields Modal

  addMetadataFields(fields: MetadataField[]) {
    this.resetMetadataFieldsOrder();
    fields.forEach(field => {
      field.isEditable = true;
      field.isRequired = false;
      field.allowDocumentUpload = true;
      const config = this.formService.buildFormField(field);
      config.order = this.currentFormFields.length - 1;
      this.currentFormFields = [
        ...this.currentFormFields,
        config
      ];
      this.dynamicForm.markFormAsTouchedAndDirty();
    });
  }

  //#endregion

  //#region Confirm Dialog

  deleteField(field: FieldConfiguration) {
    this.currentFormFields = this.currentFormFields.filter(x => x.id !== field.id);
    if (field.data.settingId) {
      this.subs.sink = this.workTemplatesService.deleteForm(this.currentStep.id, field.data)
        .then(() => {
          this.store.dispatch(new ShowSuccessMessage(`${field.name} was successfully deleted`));
          this.metadataFieldsModal.uncheckField(field);
          this.resetMetadataFieldsOrder();
        }, (error) => this.store.dispatch(new ShowErrorMessage(error)));
    } else {
      this.metadataFieldsModal.uncheckField(field);
      this.resetMetadataFieldsOrder();
    }
  }

  //#endregion

  //#region Override Label 

  overrideLabel(field: FieldConfiguration) {
    this.currentFormField = field;
    const form = this.labelOverrideModal.dynamicForm.form;
    const name = this.overrrideLabelControls[0].name;
    this.labelOverrideModal.setValue(name, field.overrideLabel);
    this.labelOverrideModal.reset(form.value);
    this.labelOverrideModal.show();
  }

  revertLabelAction(field: FieldConfiguration) {
    this.currentFormFields.forEach(f => {
      if (!f.data) return;
      if (f.id === field.id) {
        f.overrideLabel = '';
      }
    });
    this.dynamicForm.markAsTouchedAndDirty(field.name);
  }

  saveLabel(form: FormGroup) {
    if (form.valid) {
      if (form.dirty) {
        this.currentFormFields.forEach(f => {
          if (f.type === FieldType.Header) return;
          if (f.id === this.currentFormField.id) {
            const { overrideLabel, defaultValue } = form.value;
            f.overrideLabel = overrideLabel;
            f.value = defaultValue;
          }
        });
      }
    }
    this.dynamicForm.markAsTouchedAndDirty(this.currentFormField.name);
  }

  //#endregion

  private resetMetadataFieldsOrder() {
    this.currentFormFields.forEach((x, i) => {
      if (x.type !== 'header') x.order = i - 1;
    });
  }
}
