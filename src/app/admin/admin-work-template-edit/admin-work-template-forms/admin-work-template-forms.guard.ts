import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { CanDeactivate } from '@angular/router';
import { AdminWorkTemplateFormsComponent } from './admin-work-template-forms.component';
import { Store } from '@ngxs/store';
import { SetActiveWorkTemplateTab } from '../../state/admin-work-planning/admin-work-planning.actions';
import { Subject } from 'rxjs/internal/Subject';

@Injectable({
  providedIn: 'root'
})
export class AdminWorkTemplateFormsGuard implements CanDeactivate<AdminWorkTemplateFormsComponent> {

  constructor(private store: Store) { }
  
  canDeactivate(component: AdminWorkTemplateFormsComponent): Observable<boolean> | Promise<boolean> | boolean {
    if (component.dynamicForm.touched && component.dynamicForm.dirty) {
      const subject = new Subject<boolean>();
      component.confirmModal.title = "Confirm Leave";
      component.confirmModal.message = "Are you sure you want to leave? If you do, you'll lose all changes.";
      component.confirmModal.show();
      component.confirmModal.response$ = subject;
      return subject.asObservable()
        .toPromise()
        .then(response => {
          if (!response) {
            this.store.dispatch(new SetActiveWorkTemplateTab('forms'));
          }
          return response;
        });
    }
    return true;
  }
}