import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminWorkTemplateEditComponent } from './admin-work-template-edit.component';

describe('AdminWorkTemplateEditComponent', () => {
  let component: AdminWorkTemplateEditComponent;
  let fixture: ComponentFixture<AdminWorkTemplateEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminWorkTemplateEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminWorkTemplateEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
