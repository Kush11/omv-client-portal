import { Select, Store } from '@ngxs/store';
import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { FormGroup } from '@angular/forms';
import { AdminWorkPlanningState } from '../../state/admin-work-planning/admin-work-planning.state';
import { GridColumn, GridColumnButton } from 'src/app/core/models/grid.column';
import {
  DeleteWorkTemplateStep, CreateWorkTemplateStep, UpdateWorkTemplateStep, SetCurrentWorkTemplateProcess, ClearCurrentWorkTemplateStep, SortWorkTemplateSteps, GetWorkTemplateGroup
} from '../../state/admin-work-planning/admin-work-planning.actions';
import { ModalComponent } from 'src/app/shared/modal/modal.component';
import { WorkTemplateStep, WorkTemplateProcess } from 'src/app/core/models/entity/work-template';
import { SubSink } from 'subsink/dist/subsink';
import { FieldConfiguration } from 'src/app/shared/dynamic-components/field-setting';
import { GetEmailTemplates } from '../../state/admin-email-templates/admin-email-templates.action';
import { AdminEmailTemplatesState } from '../../state/admin-email-templates/admin-email-templates.state';
import { EmailTemplate } from 'src/app/core/models/entity/email-templates';
import { AdminGroupState } from '../../state/admin-groups/admin-groups.state';
import { Group } from 'src/app/core/models/entity/group';
import { GetActiveGroups } from '../../state/admin-groups/admin-groups.action';
import { Button } from 'src/app/core/models/button';
import { BaseComponent } from 'src/app/shared/base/base.component';
import { ListComponent } from 'src/app/shared/list/list.component';

@Component({
  selector: 'app-admin-work-template-steps',
  templateUrl: './admin-work-template-steps.component.html',
  styleUrls: ['./admin-work-template-steps.component.css']
})
export class AdminWorkTemplateStepsComponent extends BaseComponent implements OnInit, OnDestroy {

  @Select(AdminWorkPlanningState.getProcesses) processes$: Observable<WorkTemplateProcess[]>;
  @Select(AdminWorkPlanningState.getSteps) steps$: Observable<WorkTemplateStep[]>;
  @Select(AdminEmailTemplatesState.getEmailTemplates) emailTemplates$: Observable<EmailTemplate[]>;
  @Select(AdminGroupState.getActiveGroups) groups$: Observable<Group[]>;
  @Select(AdminWorkPlanningState.isReadOnly) isReadOnlyMode$: Observable<boolean>;

  @ViewChild('confirmModal') confirmModal: ModalComponent;
  @ViewChild('stepModal') stepModal: ModalComponent;
  @ViewChild('stepDetailsModal') stepDetailsModal: ModalComponent;
  @ViewChild('stepsGrid') stepsGrid: ListComponent;

  private subs = new SubSink();
  templateForm: FormGroup;
  processes: WorkTemplateProcess[];
  processFields = { text: 'name', value: 'id' };
  selectedProcessId: number;
  buttons: Button[] = [
    { text: "Add Step", type: 'button', action: this.addStep.bind(this), cssClass: "button-plus add-step-qa" }
  ];
  gridButtons: Button[] = [
    { text: "SAVE CHANGES", type: 'button', action: this.saveSort.bind(this), cssClass: "button-round save-changes-button-qa" },
    { text: "Cancel", type: 'button', action: this.cancelSorting.bind(this), cssClass: "button-no-outline cancel-button-qa", override: true },
  ];
  actions: GridColumnButton[] = [
    { text: 'Remove', type: 'button', visible: true, action: this.showConfirmDialog.bind(this), cssClass: 'button-no-outline button-grid-link remove-button-qa' },
    { text: 'Edit', type: 'button', visible: true, action: this.showStepModal.bind(this), cssClass: 'button-no-outline button-grid-link edit-button-qa' }
  ];
  columns: GridColumn[] = [
    { headerText: 'Name', field: 'name', useAsLink: true, action: this.showStepModal.bind(this) },
    { headerText: 'Code', field: 'code', useAsLink: true, action: this.showStepModal.bind(this) },
    { headerText: 'Actions', type: 'buttons', buttons: this.actions, width: 200 }
  ];
  deleteMessage: string;
  currentStep: WorkTemplateStep;
  stepModalTitle: string;
  emailTemplates: EmailTemplate[];
  stepControls: FieldConfiguration[] = [
    { type: "input", label: "Step Name", inputType: "text", name: "name", required: true, validationMessage: "Please enter a step name" },
    { type: "input", label: "Step Code", inputType: "text", name: "code", required: true, validationMessage: "Please enter a step code" },
    { type: "select", label: "Notify/Edited By", inputType: "text", name: "notifyByGroup", placeholder: 'Select group', required: true, validationMessage: "Please select a group" },
    { type: "select", label: "Notification Template", inputType: "text", name: "templateCode", placeholder: 'Select template' },
    { type: "input", label: "Advance Button", inputType: "text", name: "advanceButton", required: true, validationMessage: "Please enter button text" },
    { type: "select", label: "Advanced By", inputType: "text", name: "advancedByGroup", placeholder: 'Select group', required: true, validationMessage: "Please select a group" },
  ];
  isUpdateMode: boolean;
  isReadOnlyMode: boolean;
  groups: Group[];
  steps: WorkTemplateStep[];
  stepsForSorting: any[] = [];
  enableGridButtons: boolean;
  defaultSteps: WorkTemplateStep[];

  constructor(protected store: Store) {
    super(store);
    this.setPageTitle("Admin Work Template Steps");
  }

  ngOnInit() {
    this.store.dispatch(new GetEmailTemplates());
    this.store.dispatch(new GetActiveGroups());
    this.subs.add(
      this.processes$
        .subscribe(processes => {
          if (processes.length > 0) {
            this.selectedProcessId = this.isUpdateMode ? this.selectedProcessId : processes[0].id;
            const process = processes.find(p => p.id === this.selectedProcessId);
            this.store.dispatch(new SetCurrentWorkTemplateProcess(process));
            this.isUpdateMode = false;
          }
        }),
      this.isReadOnlyMode$
        .subscribe(isReadOnly => {
          this.isReadOnlyMode = isReadOnly;
          this.columns.forEach(column => {
            if (column.type === 'buttons') column.buttons.forEach(b => b.visible = !isReadOnly);
          });
        }),
      this.emailTemplates$
        .subscribe(emailTemplates => {
          this.emailTemplates = emailTemplates;
          const notificationTemplate = this.stepControls[3];
          notificationTemplate.dataSource = emailTemplates;
          notificationTemplate.fields = { text: 'name', value: 'id' };
        }),
      this.groups$
        .subscribe(groups => {
          this.groups = groups;
          const notifyByGroup = this.stepControls[2];
          notifyByGroup.dataSource = groups;
          notifyByGroup.fields = { text: 'name', value: 'id' };

          const advancedBy = this.stepControls[5];
          advancedBy.dataSource = groups;
          advancedBy.fields = { text: 'name', value: 'id' };
        }),
      this.steps$
        .subscribe(steps => {
          this.steps = [...steps];
          this.defaultSteps = [...steps];
          this.enableGridButtons = false;
        })
    );
  }

  ngOnDestroy() {
    this.store.dispatch(new ClearCurrentWorkTemplateStep());
    this.subs.unsubscribe();
  }

  processChanged(args: any) {
    const process: WorkTemplateProcess = args.itemData;
    this.store.dispatch(new SetCurrentWorkTemplateProcess(process));
  }

  addStep() {
    this.stepModalTitle = "Add Step";
    this.currentStep = null;
    this.stepModal.reset();
    this.stepModal.show();
  }

  showStepModal(event: WorkTemplateStep) {
    this.currentStep = event;
    if (!this.isReadOnlyMode) {
      this.stepModalTitle = `"${event.name}" Details`;
      this.stepControls.forEach((control) => {
        const propName: string = control.name;
        this.stepModal.setValue(propName, event[propName]);
      });
      const form = this.stepModal.dynamicForm.form;
      this.stepModal.reset(form.value);
      this.stepModal.show();
    } else {
      const nameControl = this.stepControls[0];
      const codeControl = this.stepControls[1];
      const notifyByControl = this.stepControls[2];
      const templateControl = this.stepControls[3];
      const advanceControl = this.stepControls[4];
      const advancedByControl = this.stepControls[5];

      const notifyByGroup = this.groups.find(g => g.id === event.notifyByGroup);
      const notifyByValue = notifyByGroup ? notifyByGroup.name : 'N/A';

      const template = this.emailTemplates.find(t => t.id === event.templateCode);
      const templateValue = template ? template.name : 'N/A';

      const advancedByGroup = this.groups.find(g => g.id === event.advancedByGroup);
      const advancedByGroupValue = advancedByGroup ? advancedByGroup.name : 'N/A';

      this.stepDetailsModal.clearDetails();
      this.stepDetailsModal.addDetail(nameControl.label, event.name);
      this.stepDetailsModal.addDetail(codeControl.label, event.code);
      this.stepDetailsModal.addDetail(notifyByControl.label, notifyByValue);
      this.stepDetailsModal.addDetail(templateControl.label, templateValue);
      this.stepDetailsModal.addDetail(advanceControl.label, event.advanceButton);
      this.stepDetailsModal.addDetail(advancedByControl.label, advancedByGroupValue);
      this.stepDetailsModal.show();
    }
  }

  saveStep(stepForm: FormGroup) {
    this.isUpdateMode = true;
    if (stepForm.valid) {
      if (stepForm.dirty) {
        const step: WorkTemplateStep = { ...this.currentStep, ...stepForm.value };
        if (!this.currentStep) {
          step.processId = this.selectedProcessId;
          step.order = this.steps.length;
          this.store.dispatch(new CreateWorkTemplateStep(step));
        } else {
          // Save order
          this.steps.forEach((s, i) => {
            if (s.id === step.id) step.order = i;
          });
          this.store.dispatch(new UpdateWorkTemplateStep(step));
        }
      }
    }
  }

  //#region Grid Sorting

  onRowDrop(args: any) {
    this.enableGridButtons = true;
  }

  saveSort() {
    this.store.dispatch(new SortWorkTemplateSteps(this.selectedProcessId, this.steps)).toPromise()
      .then(() => {
        this.enableGridButtons = false;
      });
  }

  cancelSorting() {
    this.steps = [...this.defaultSteps];
    this.enableGridButtons = false;
  }

  //#endregion

  //#region Confirm Delete Dialog

  showConfirmDialog(step: WorkTemplateStep) {
    this.currentStep = step;
    this.deleteMessage = `Are you sure you want to delete ${step.name}?`;
    this.confirmModal.show();
  }

  deleteStep() {
    this.isUpdateMode = true;
    this.store.dispatch(new DeleteWorkTemplateStep(this.currentStep));
  }

  //#endregion
}
