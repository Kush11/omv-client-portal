import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminWorkTemplateStepsComponent } from './admin-work-template-steps.component';

describe('AdminWorkTemplateStepsComponent', () => {
  let component: AdminWorkTemplateStepsComponent;
  let fixture: ComponentFixture<AdminWorkTemplateStepsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminWorkTemplateStepsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminWorkTemplateStepsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
