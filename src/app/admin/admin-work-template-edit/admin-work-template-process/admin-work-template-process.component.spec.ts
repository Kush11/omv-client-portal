import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminWorkTemplateProcessComponent } from './admin-work-template-process.component';

describe('AdminWorkTemplateProcessComponent', () => {
  let component: AdminWorkTemplateProcessComponent;
  let fixture: ComponentFixture<AdminWorkTemplateProcessComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminWorkTemplateProcessComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminWorkTemplateProcessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
