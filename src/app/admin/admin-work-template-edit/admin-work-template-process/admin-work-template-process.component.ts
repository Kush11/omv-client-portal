import { Select, Store } from '@ngxs/store';
import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { FormGroup } from '@angular/forms';
import { GridColumn, GridColumnButton } from 'src/app/core/models/grid.column';
import { AdminWorkPlanningState } from '../../state/admin-work-planning/admin-work-planning.state';
import {
  CreateWorkTemplateProcess, UpdateWorkTemplateProcess, DeleteWorkTemplateProcess
} from '../../state/admin-work-planning/admin-work-planning.actions';
import { ModalComponent } from 'src/app/shared/modal/modal.component';
import { WorkTemplateProcess, WorkTemplate } from 'src/app/core/models/entity/work-template';
import { SubSink } from 'subsink/dist/subsink';
import { FieldConfiguration } from 'src/app/shared/dynamic-components/field-setting';
import { Button } from 'src/app/core/models/button';
import { BaseComponent } from 'src/app/shared/base/base.component';

@Component({
  selector: 'app-admin-work-template-process',
  templateUrl: './admin-work-template-process.component.html',
  styleUrls: ['./admin-work-template-process.component.css']
})
export class AdminWorkTemplateProcessComponent extends BaseComponent implements OnInit, OnDestroy {

  @Select(AdminWorkPlanningState.getCurrentTemplate) currentTemplate$: Observable<WorkTemplate>;
  @Select(AdminWorkPlanningState.getProcesses) workTemplateProcesses$: Observable<WorkTemplateProcess[]>;
  @Select(AdminWorkPlanningState.getCurrentTemplateId) currentTemplateId$: Observable<number>;
  @Select(AdminWorkPlanningState.isReadOnly) isReadOnlyMode$: Observable<boolean>;

  @ViewChild("processModal") processModal: ModalComponent;
  @ViewChild('confirmModal') confirmModal: ModalComponent;
  @ViewChild('processDetailModal') processDetailsModal: ModalComponent;

  private subs = new SubSink();
  processForm: FormGroup;
  typeFields = { text: 'type', value: 'id' };
  buttons: Button[] = [
    { text: "Add Process", type: 'button', action: this.addProcess.bind(this), cssClass: "button-plus add-process-btn-qa" },
  ];
  actions: GridColumnButton[] = [
    { text: 'Remove', type: 'button', visible: true, action: this.showConfirmDialog.bind(this), cssClass: 'button-no-outline button-grid-link remove-button-qa' },
    { text: 'Edit', type: 'button', visible: true, action: this.showProcessModal.bind(this), cssClass: 'button-no-outline button-grid-link edit-button-qa' }
  ];
  columns: GridColumn[] = [
    { headerText: 'Name', field: 'name', useAsLink: true, action: this.showProcessModal.bind(this) },
    { headerText: 'Code', field: 'code', useAsLink: true, action: this.showProcessModal.bind(this) },
    { headerText: 'Actions', type: 'buttons', buttons: this.actions, width: 200 }
  ];
  deleteMessage: string;
  currentProcess: WorkTemplateProcess;
  processControls: FieldConfiguration[] = [
    { type: "input", label: "Process Name", inputType: "text", name: "name", required: true, validationMessage: "Please enter a process name" },
    { type: "input", label: "Process Code", inputType: "text", name: "code", required: true, validationMessage: "Please enter a process code" }
  ];
  currentTemplateId: number;
  isReadOnlyMode: boolean;
  processModalTitle: string;
  isLoading: boolean;

  constructor(protected store: Store) {
    super(store);
    this.setPageTitle("Admin Work Template Processes");
  }

  ngOnInit() {
    this.subs.add(
      this.currentTemplateId$
        .subscribe(id => this.currentTemplateId = id),
      this.isReadOnlyMode$
        .subscribe(isReadOnly => {
          this.isReadOnlyMode = isReadOnly;
          this.columns.forEach(column => {
            if (column.type === 'buttons') column.buttons.forEach(b => b.visible = !isReadOnly);
          });
        })
    );
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

  addProcess() {
    this.processModalTitle = "Add Process";
    this.currentProcess = null;
    this.processModal.reset();
    this.processModal.show();
  }

  showProcessModal(event: WorkTemplateProcess) {
    this.currentProcess = event;
    const form = this.processModal.dynamicForm.form;
    const nameControl = this.processControls[0];
    const codeControl = this.processControls[1];

    if (!this.isReadOnlyMode) {
      this.processModalTitle = `"${event.name}" Details`;
      this.processModal.setValue(nameControl.name, event.name);
      this.processModal.setValue(codeControl.name, event.code);
      this.processModal.reset(form.value);
      this.processModal.show();
    } else {
      this.processDetailsModal.clearDetails();
      this.processDetailsModal.addDetail(nameControl.label, event.name);
      this.processDetailsModal.addDetail(codeControl.label, event.code);
      this.processDetailsModal.show();
    }
  }

  saveProcess(processForm: FormGroup) {
    if (processForm.valid) {
      if (processForm.dirty) {
        const process: WorkTemplateProcess = { ...this.currentProcess, ...processForm.value };
        if (!this.currentProcess) {
          process.templateId = this.currentTemplateId;
          this.store.dispatch(new CreateWorkTemplateProcess(process));
        } else {
          this.store.dispatch(new UpdateWorkTemplateProcess(process.id, process));
        }
      }
    }
  }

  //#region Confirm Delete Dialog

  showConfirmDialog(event: WorkTemplateProcess) {
    this.currentProcess = event;
    this.deleteMessage = `Are you sure you want to delete Process - ${event.name}?`;
    this.confirmModal.show();
  }

  deleteProcess() {
    this.store.dispatch(new DeleteWorkTemplateProcess(this.currentProcess));
  }

  //#endregion

}
