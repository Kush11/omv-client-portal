import { Component, OnInit, OnDestroy } from '@angular/core';
import { Tab } from 'src/app/core/models/tab';
import { Store, Select } from '@ngxs/store';
import { BaseComponent } from 'src/app/shared/base/base.component';
import { ActivatedRoute, Router } from '@angular/router';
import {
  SetCurrentWorkTemplateGroupId, GetWorkTemplateGroup, ClearCurrentWorkTemplateGroup,
  SetCurrentWorkTemplate,
  SetActiveWorkTemplateTab
} from '../state/admin-work-planning/admin-work-planning.actions';
import { SubSink } from 'subsink/dist/subsink';
import { AdminWorkPlanningState } from '../state/admin-work-planning/admin-work-planning.state';
import { WorkTemplate, WorkTemplateProcess, WorkTemplateStep, WorkTemplateVersion } from 'src/app/core/models/entity/work-template';
import { Observable } from 'rxjs/internal/Observable';
import { AppState } from 'src/app/state/app.state';
import { Breadcrumb } from 'src/app/core/models/breadcrumb';
import { SetBreadcrumbs, ClearBreadcrumbs } from 'src/app/state/app.actions';
import { WorkTemplateStatus } from 'src/app/core/enum/work-template-status';

const GENERAL_INFO_TAB = 'general-info';
const FIELDS_TAB = 'fields';
const PROCESS_TAB = 'process';
const STEPS_TAB = 'steps';
const FORMS_TAB = 'forms';
const FINDINGS_TAB = 'findings';
const VERSIONS_TAB = 'versions';
const HISTORY_TAB = 'history';

@Component({
  selector: 'app-admin-work-template-edit',
  templateUrl: './admin-work-template-edit.component.html',
  styleUrls: ['./admin-work-template-edit.component.css']
})
export class AdminWorkTemplateEditComponent extends BaseComponent implements OnInit, OnDestroy {

  @Select(AppState.getBreadcrumbs) breadcrumbs$: Observable<Breadcrumb[]>;
  @Select(AdminWorkPlanningState.getCurrentTemplate) currentWorkTemplate$: Observable<WorkTemplate>;
  @Select(AdminWorkPlanningState.getCurrentProcess) currentProcess$: Observable<WorkTemplateProcess>;
  @Select(AdminWorkPlanningState.getProcesses) processes$: Observable<WorkTemplateProcess[]>;
  @Select(AdminWorkPlanningState.getCurrentStep) currentStep$: Observable<WorkTemplateStep>;
  @Select(AdminWorkPlanningState.hasSteps) hasSteps$: Observable<boolean>;
  @Select(AdminWorkPlanningState.getWorkTemplateVersions) versions$: Observable<WorkTemplateVersion[]>;
  @Select(AdminWorkPlanningState.getActiveWorkTemplateTab) activeTab$: Observable<string>;

  private subs = new SubSink();
  showGeneralInfo: boolean;
  showFields: boolean;
  showProcess: boolean;
  showSteps: boolean;
  showForms: boolean;
  showFindings: boolean;
  showVersions: boolean;
  showHistory: boolean;

  tabs: Tab[] = [
    { link: GENERAL_INFO_TAB, name: 'General Info' },
    { link: FIELDS_TAB, name: 'Fields' },
    { link: PROCESS_TAB, name: 'Process' },
    { link: STEPS_TAB, name: 'Steps' },
    { link: FORMS_TAB, name: 'Forms' },
    { link: FINDINGS_TAB, name: 'Findings' },
    { link: VERSIONS_TAB, name: 'Versions' },
    { link: HISTORY_TAB, name: 'History' },
  ];
  workGroupId: number;
  versionFields: object = { text: 'version', value: 'version' };
  selectedVersion: number;

  constructor(protected store: Store, private router: Router, private route: ActivatedRoute) {
    super(store);
    this.showLefNav();
  }

  ngOnInit() {
    this.setActiveTab(this.router.url);
    this.subs.add(
      this.route.params
        .subscribe(params => {
          const { id } = params;
          let workGroupId = Number(id);
          if (!Number.isNaN(workGroupId)) {
            this.workGroupId = workGroupId;
            this.store.dispatch(new SetCurrentWorkTemplateGroupId(id));
            this.store.dispatch(new GetWorkTemplateGroup(id));
          } else {
            this.store.dispatch(new SetCurrentWorkTemplateGroupId(null));
            this.store.dispatch(new ClearCurrentWorkTemplateGroup());
          }
        }),
      this.versions$
        .subscribe(versions => {
          if (versions) {
            const draftVersion = versions.find(v => v.status === WorkTemplateStatus.Draft);
            if (draftVersion) {
              this.selectedVersion = draftVersion.version;
            }
          }
        }),
      this.currentWorkTemplate$
        .subscribe(template => {
          const processTab = this.tabs[1]; // if there's no template, lock the process tab
          processTab.disabled = !template;
          if (template) {
            const crumbs: Breadcrumb[] = [
              { link: '/admin/work-templates', name: 'Listings' },
              { isFinal: true, name: template.name }
            ];
            this.store.dispatch(new SetBreadcrumbs(crumbs));
          } else {
            let crumbs: Breadcrumb[] = [
              { link: '/admin/work-templates', name: 'Listings' },
              { isFinal: true, name: 'New Template' }
            ];
            this.store.dispatch(new SetBreadcrumbs(crumbs));
          }
        }),
      this.processes$
        .subscribe(processes => {
          const stepTab = this.tabs[3]; // if there are no processes, lock the step tab
          stepTab.disabled = processes.length === 0;
        }),
      this.hasSteps$
        .subscribe(hasSteps => {
          const formTab = this.tabs[4]; // if there are no steps, lock forms and findings
          const findingsTab = this.tabs[5];
          formTab.disabled = !hasSteps;
          findingsTab.disabled = !hasSteps;
        }),
      this.activeTab$
        .subscribe(url => {
          if (url) {
            this.clearActiveTab();
            this.setActiveTab(url);
            this.store.dispatch(new SetActiveWorkTemplateTab(null));
          }
        })
    );
  }

  ngOnDestroy() {
    this.store.dispatch(new ClearCurrentWorkTemplateGroup());
    this.store.dispatch(new ClearBreadcrumbs());
    this.clearActiveTab();
    this.subs.unsubscribe();
  }

  switchBreadCrumbs(breadcrumbLink: string) {
    this.router.navigate([breadcrumbLink]);
  }

  onRouteChange(route: any) {
    this.clearActiveTab();
    this.setActiveTab(route);
  }

  switchTabs(tabLink: any) {
    const route = `/admin/work-templates/${this.workGroupId}/${tabLink}`;
    this.router.navigate([route]);
  }

  setActiveTab(tabLink: string) {
    if (tabLink.includes(GENERAL_INFO_TAB)) {
      this.showGeneralInfo = true;
    } else if (tabLink.includes(FIELDS_TAB)) {
      this.showFields = true;
    } else if (tabLink.includes(PROCESS_TAB)) {
      this.showProcess = true;
    } else if (tabLink.includes(STEPS_TAB)) {
      this.showSteps = true;
    } else if (tabLink.includes(FORMS_TAB)) {
      this.showForms = true;
    } else if (tabLink.includes(FINDINGS_TAB)) {
      this.showFindings = true;
    } else if (tabLink.includes(VERSIONS_TAB)) {
      this.showVersions = true;
    } else if (tabLink.includes(HISTORY_TAB)) {
      this.showHistory = true;
    }
    const activeTab = this.tabs.find(x => tabLink.includes(x.link));
    if (activeTab)
      activeTab.isActive = true;
  }

  clearActiveTab() {
    this.showGeneralInfo = this.showFields = this.showProcess = this.showSteps =
      this.showForms = this.showFindings = this.showVersions = this.showHistory = false;
    this.tabs.map(x => x.isActive = false);
  }

  versionChanged(args: any) {
    const { version } = args.itemData;
    if (version) {
      this.store.dispatch(new SetCurrentWorkTemplate(null, version));
    }
  }
}
