import { Component, OnInit, ViewChild } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { AdminWorkPlanningState } from '../../state/admin-work-planning/admin-work-planning.state';
import { WorkTemplateProcess, WorkTemplateStep } from 'src/app/core/models/entity/work-template';
import { ModalComponent } from 'src/app/shared/modal/modal.component';
import { DynamicFormComponent } from 'src/app/shared/dynamic-components/components/dynamic-form.component';
import { SubSink } from 'subsink/dist/subsink';
import { MetadataField } from 'src/app/core/models/entity/metadata-field';
import { FieldConfiguration, FieldType } from 'src/app/shared/dynamic-components/field-setting';
import { MetadataFieldsService } from 'src/app/core/services/business/metadata-fields/metadata-fields.service';
import { WorkTemplatesService } from 'src/app/core/services/business/work-templates/work-templates.service';
import { SetCurrentWorkTemplateProcess } from '../../state/admin-work-planning/admin-work-planning.actions';
import { ShowSpinner, HideSpinner, ShowErrorMessage, ShowSuccessMessage } from 'src/app/state/app.actions';
import { FormGroup } from '@angular/forms';
import { Observable } from 'rxjs/internal/Observable';
import { Button } from 'src/app/core/models/button';
import { AdminWorkTemplateFindingsService } from './admin-work-template-findings.service';
import { MetadataFieldType } from 'src/app/core/enum/metadataFieldType';
import { BaseComponent } from 'src/app/shared/base/base.component';
import { SortType, SortDirection } from 'src/app/core/enum/sort-enum';
import { FieldsService } from 'src/app/core/services/business/fields/fields.service';
import { ChangeEventArgs } from '@syncfusion/ej2-dropdowns';
import { Subject } from 'rxjs/internal/Subject';

@Component({
  selector: 'app-admin-work-template-findings',
  templateUrl: './admin-work-template-findings.component.html',
  styleUrls: ['./admin-work-template-findings.component.css'],
  providers: [AdminWorkTemplateFindingsService]
})
export class AdminWorkTemplateFindingsComponent extends BaseComponent implements OnInit {

  @Select(AdminWorkPlanningState.getProcesses) processes$: Observable<WorkTemplateProcess[]>;
  @Select(AdminWorkPlanningState.getSteps) steps$: Observable<WorkTemplateStep[]>;
  @Select(AdminWorkPlanningState.getCurrentStep) currentStep$: Observable<WorkTemplateStep>;
  @Select(AdminWorkPlanningState.getCurrentGroupId) currentWorkGroupId$: Observable<number>;
  @Select(AdminWorkPlanningState.isReadOnly) isReadOnlyMode$: Observable<boolean>;

  @ViewChild('metadataFieldsModal') metadataFieldsModal: ModalComponent;
  @ViewChild("labelOverrideModal") labelOverrideModal: ModalComponent;
  @ViewChild("confirmModal") confirmModal: ModalComponent;
  @ViewChild("dynamicForm") dynamicForm: DynamicFormComponent;

  private subs = new SubSink();
  processFields = { text: 'name', value: 'id' };
  selectedProcessId: number;
  stepFields = { text: 'name', value: 'id' };
  selectedStepId: number;
  steps: WorkTemplateStep[];
  currentStep: WorkTemplateStep;
  deleteMessage: string;
  metadataFields: MetadataField[];

  //#region Work Template Finding Fields

  currentFindingFields: FieldConfiguration[] = [];
  existingFieldIds: number[] = [];
  currentFindingField: FieldConfiguration;
  buttons: Button[] = [
    { text: "SAVE CHANGES", type: 'button', action: this.saveChanges.bind(this), cssClass: "button-round save-changes-qa" },
    { text: "Cancel", type: 'button', action: this.cancel.bind(this), cssClass: "button-no-outline cancel-qa" },
  ];

  //#endregion

  //#region Override Label Modal

  overrrideLabelControls: FieldConfiguration[] = [
    { type: "input", label: "Override Label", inputType: "text", name: "overrideLabel", required: true, validationMessage: "Please enter a label" }
  ];

  //#endregion

  constructor(protected store: Store, private metadataFieldsService: MetadataFieldsService, private fieldsService: FieldsService,
    private findingsService: AdminWorkTemplateFindingsService, private workTemplatesService: WorkTemplatesService) {
    super(store);
    this.setPageTitle("Admin Work Template Findings");
  }

  ngOnInit() {
    this.subs.add(
      this.processes$
        .subscribe(processes => {
          if (processes.length > 0) {
            const process = processes[0];
            this.selectedProcessId = process.id;
            this.store.dispatch(new SetCurrentWorkTemplateProcess(process));
          }
        }),
      this.steps$
        .subscribe(steps => {
          this.steps = steps;
          if (steps.length > 0) {
            const step = steps[0];
            this.currentStep = step;
            this.selectedStepId = this.currentStep.id;
          }
        }),
      this.currentStep$
        .subscribe(step => {
          this.currentStep = step;
          this.setFindingFields();
        })
    );
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

  private setFindingFields() {
    if (!this.currentStep) {
      this.currentFindingFields = this.findingsService.clearFormFields();
      return;
    }
    this.showSpinner();
    const currentTemplate = this.store.selectSnapshot(AdminWorkPlanningState.getCurrentTemplate);
    const fieldIds = currentTemplate.fields.map(x => x.id);
    // Step 1: Get all finding metadata fields
    this.findingsService.getFindingFields(this.currentStep.id)
      .then(findingsField => {
        this.currentFindingFields = findingsField;
        this.currentFindingFields.sort((a, b) => a.order - b.order);

        // Step2: Store common fields
        this.existingFieldIds = findingsField.map(f => f.id);

        // Step 3: Get all metadata fields
        this.subs.sink =
          this.metadataFieldsService.getWorkPlanningMetadataFields()
            .subscribe(metadataFields => {
              metadataFields = metadataFields.filter(mf => mf.type !== MetadataFieldType.Label && !fieldIds.includes(mf.id));
              // Step 4: Mark common fields as selected in the modal
              metadataFields.forEach(f => f.isChecked = this.existingFieldIds.includes(f.id));
              this.metadataFields = this.fieldsService.sort(metadataFields, 'label', SortType.String, SortDirection.Ascending);

              this.store.dispatch(new HideSpinner());
            }, err => this.store.dispatch(new ShowErrorMessage(err)));
      }, err => this.store.dispatch(new ShowErrorMessage(err)));
  }

  //#region Process & Steps 

  processChanged(args: ChangeEventArgs) {
    const process: WorkTemplateProcess = args.itemData as any;
    this.store.dispatch(new SetCurrentWorkTemplateProcess(process));
  }

  stepChanged(args: any) {
    this.currentStep = args.itemData;
    this.setFindingFields();
  }

  addField() {
    this.metadataFieldsModal.show();
  }

  //#endregion

  //#region Work Template Finding Fields

  saveChanges() {
    if (this.dynamicForm) {
      let createdCount = 0; // for testing
      let updatedCount = 0; // for testing
      const controls = this.dynamicForm.controls;
      controls.forEach((config, index) => {
        const isLastField = index === (controls.length - 1);
        const field: MetadataField = {
          ...config.data,
          isRequired: config.required,
          isEditable: config.editable,
          allowDocumentUpload: config.allowDocumentUpload,
          overrideLabel: config.overrideLabel,
          order: index
        };
        if (field.settingId) {
          updatedCount++;
          this.subs.sink =
            this.workTemplatesService.updateFinding(this.currentStep.id, field)
              .then(() => {
                if (isLastField) {
                  this.store.dispatch(new ShowSuccessMessage("Finding Fields successfully updated!"));
                  this.setFindingFields();
                }
              }, (error) => this.store.dispatch(new ShowErrorMessage(error)))
        } else {
          createdCount++;
          this.subs.sink =
            this.workTemplatesService.createFinding(this.currentStep.id, field)
              .then(() => {
                if (isLastField) {
                  this.store.dispatch(new ShowSuccessMessage("Finding Fields successfully updated!"));
                  this.setFindingFields();
                }
              }, (error) => this.store.dispatch(new ShowErrorMessage(error)))
        }
      });
      console.log(`WorkTemplateForms analysis: ${createdCount} created and ${updatedCount} updated.`);
      this.dynamicForm.form.reset(this.dynamicForm.form.value);
    }
  }

  cancel() {
    this.setFindingFields();
    this.dynamicForm.form.reset();
  }

  //#endregion

  //#region Metadata Fields Modal

  addMetadataFields(fields: MetadataField[]) {
    this.resetMetadataFieldsOrder();
    fields.forEach(field => {
      field.isEditable = true;
      field.isRequired = false;
      field.allowDocumentUpload = true;
      const config = this.findingsService.buildFindingField(field);
      config.order = this.currentFindingFields.length - 1;
      this.currentFindingFields = [
        ...this.currentFindingFields,
        config
      ];
      this.dynamicForm.markFormAsTouchedAndDirty();
    });
  }

  //#endregion

  //#region Confirm Dialog

  deleteField(field: FieldConfiguration) {
    this.currentFindingFields = this.currentFindingFields.filter(x => x.id !== field.id);
    if (field.data.settingId) {
      this.workTemplatesService.deleteForm(this.currentStep.id, field.data)
        .then(() => {
          this.store.dispatch(new ShowSuccessMessage(`${field.name} was successfully deleted`));
          this.metadataFieldsModal.uncheckField(field);
          this.resetMetadataFieldsOrder();
        }, (error) => this.store.dispatch(new ShowErrorMessage(error)));
    } else {
      this.metadataFieldsModal.uncheckField(field);
      this.resetMetadataFieldsOrder();
    }
  }

  //#endregion

  //#region Override Label 

  overrideLabel(field: FieldConfiguration) {
    this.currentFindingField = field;
    const form = this.labelOverrideModal.dynamicForm.form;
    const name = this.overrrideLabelControls[0].name;
    this.labelOverrideModal.setValue(name, field.overrideLabel);
    this.labelOverrideModal.reset(form.value);
    this.labelOverrideModal.show();
  }

  revertLabelAction(field: FieldConfiguration) {
    this.currentFindingFields.forEach(f => {
      if (!f.data) return;
      if (f.id === field.id) {
        f.overrideLabel = '';
      }
    });
    this.dynamicForm.markAsTouchedAndDirty(field.name);
  }

  saveLabel(form: FormGroup) {
    if (form.valid) {
      if (form.dirty) {
        this.currentFindingFields.forEach(f => {
          if (f.type === FieldType.Header) return;
          if (f.id === this.currentFindingField.id) {
            const { overrideLabel } = form.value;
            f.overrideLabel = overrideLabel;
          }
        });
      }
    }
    this.dynamicForm.markAsTouchedAndDirty(this.currentFindingField.name);
  }

  //#endregion

  private resetMetadataFieldsOrder() {
    this.currentFindingFields.forEach((x, i) => {
      if (x.type !== 'header') x.order = i - 1;
    });
  }
}
