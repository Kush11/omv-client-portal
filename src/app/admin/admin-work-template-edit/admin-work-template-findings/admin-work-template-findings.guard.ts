import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { CanDeactivate } from '@angular/router';
import { Store } from '@ngxs/store';
import { SetActiveWorkTemplateTab } from '../../state/admin-work-planning/admin-work-planning.actions';
import { Subject } from 'rxjs/internal/Subject';
import { AdminWorkTemplateFindingsComponent } from './admin-work-template-findings.component';

@Injectable({
  providedIn: 'root'
})
export class AdminWorkTemplateFindingsGuard implements CanDeactivate<AdminWorkTemplateFindingsComponent> {

  constructor(private store: Store) { }
  
  canDeactivate(component: AdminWorkTemplateFindingsComponent): Observable<boolean> | Promise<boolean> | boolean {
    if (component.dynamicForm.touched && component.dynamicForm.dirty) {
      const subject = new Subject<boolean>();
      component.confirmModal.title = "Confirm Leave";
      component.confirmModal.message = "Are you sure you want to leave? If you do, you'll lose all changes.";
      component.confirmModal.show();
      component.confirmModal.response$ = subject;
      return subject.asObservable()
        .toPromise()
        .then(response => {
          if (!response) {
            this.store.dispatch(new SetActiveWorkTemplateTab('findings'));
          }
          return response;
        });
    }
    return true;
  }
}