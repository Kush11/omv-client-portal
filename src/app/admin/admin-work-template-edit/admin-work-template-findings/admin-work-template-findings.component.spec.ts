import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminWorkTemplateFindingsComponent } from './admin-work-template-findings.component';

describe('AdminWorkTemplateFindingsComponent', () => {
  let component: AdminWorkTemplateFindingsComponent;
  let fixture: ComponentFixture<AdminWorkTemplateFindingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminWorkTemplateFindingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminWorkTemplateFindingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
