import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { WorkTemplate } from 'src/app/core/models/entity/work-template';
import { Observable } from 'rxjs/internal/Observable';
import { AdminWorkPlanningState } from '../../state/admin-work-planning/admin-work-planning.state';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { SubSink } from 'subsink/dist/subsink';
import { CreateWorkTemplate, UpdateWorkTemplate } from '../../state/admin-work-planning/admin-work-planning.actions';
import { Select, Store } from '@ngxs/store';
import { Button } from 'src/app/core/models/button';
import { MetadataField } from 'src/app/core/models/entity/metadata-field';
import { BaseComponent } from 'src/app/shared/base/base.component';
import { GetWorkPlanningMetadataFields } from '../../state/admin-metadata/admin-metadata.action';
import { AdminMetadataState } from '../../state/admin-metadata/admin-metadata.state';
import { MetadataFieldType } from 'src/app/core/enum/metadataFieldType';
import { ModalComponent } from 'src/app/shared/modal/modal.component';

@Component({
  selector: 'app-admin-work-template-general-info',
  templateUrl: './admin-work-template-general-info.component.html',
  styleUrls: ['./admin-work-template-general-info.component.css']
})
export class AdminWorkTemplateGeneralInfoComponent extends BaseComponent implements OnInit, OnDestroy {

  @Select(AdminWorkPlanningState.getCurrentTemplate) workTemplate$: Observable<WorkTemplate>;
  @Select(AdminWorkPlanningState.getCurrentGroupId) currentWorkGroupId$: Observable<number>;
  @Select(AdminWorkPlanningState.isReadOnly) isReadOnlyMode$: Observable<boolean>;
  @Select(AdminMetadataState.getWorkPlanningMetadataFields) assets$: Observable<MetadataField[]>;

  @ViewChild('confirmModal') confirmModal: ModalComponent;

  private subs = new SubSink();
  workTemplateId: number;
  workTemplate: WorkTemplate;
  templateForm: FormGroup;
  assets: MetadataField[];

  buttons: Button[] = [
    { text: "SAVE CHANGES", type: 'submit', cssClass: "button-round save-changes-qa", disabled: true },
    { text: "Cancel", type: 'button', cssClass: "button-no-outline cancel-qa" },
  ];
  lockForm: boolean;
  metadataFormField: Object = { text: 'label', value: 'id' };
  uniqueFields: { [key: string]: Object }[] = [
    { name: 'Item No', code: 'itemNo' },
    { name: 'Item Name', code: 'itemName' },
    { name: 'Group', code: 'group' },
    { name: 'Location', code: 'location' }
  ];
  checkFields: Object = { text: 'name', value: 'code' };

  constructor(protected store: Store, private router: Router, private formBuilder: FormBuilder) {
    super(store);
    this.setPageTitle("Admin Work Template General Info");
    this.templateForm = this.formBuilder.group({
      name: ['', [Validators.required]],
      workType: ['', [Validators.required]],
      workItem: ['', [Validators.required]],
      workItemParent: ['', [Validators.required]],
      assetParent: ['', [Validators.required]]
    });
  }

  ngOnInit() {
    this.store.dispatch(new GetWorkPlanningMetadataFields(true));
    this.subs.add(
      this.workTemplate$
        .subscribe(template => {
          if (template) {
            this.workTemplate = template;
            this.setFormValue(this.workTemplate);
          } else {
            this.workTemplate = null;
            this.templateForm.reset();
          }
        }),
      this.assets$
        .subscribe(metadata => {
          if (metadata) {
            this.assets = metadata.filter(obj => obj.type === MetadataFieldType.Select || obj.type === MetadataFieldType.Nested);
            this.templateForm.reset(this.templateForm.value);
          }
        }),
      this.templateForm.valueChanges
        .subscribe(() => this.buttons[0].disabled = !this.templateForm.valid || !this.templateForm.dirty),
      this.isReadOnlyMode$
        .subscribe(isReadOnly => {
          if (isReadOnly) {
            this.templateForm.disable();
            this.templateForm.reset(this.templateForm.value);
          } else {
            this.templateForm.reset(this.templateForm.value);
            this.templateForm.enable();
          }
        })
    );
  }

  ngOnDestroy() {
    this.templateForm.reset();
    this.subs.unsubscribe();
  }

  private setFormValue(template: WorkTemplate) {
    this.templateForm.setValue({
      name: template.name,
      workType: template.workType,
      workItem: template.workItem,
      workItemParent: template.workItemParent,
      assetParent: template.assetParent
    });
  }

  buttonActions(index: number) {
    if (index === 1) {
      this.cancel();
    }
  }

  save() {
    this.buttons[0].disabled = true;
    if (this.templateForm.valid) {
      if (this.templateForm.dirty) {
        const workTemplate: WorkTemplate = { ...this.workTemplate, ...this.templateForm.value };
        if (!workTemplate.id) {
          this.store.dispatch(new CreateWorkTemplate(workTemplate));
          this.subs.sink = this.currentWorkGroupId$
            .subscribe(id => {
              if (id) {
                this.templateForm.reset();
                this.router.navigate([`/admin/work-templates/${id}/general-info`]);
              }
            });
        } else {
          this.subs.sink = this.store.dispatch(new UpdateWorkTemplate(workTemplate.id, workTemplate))
            .subscribe(() => {
              this.templateForm.reset();
              this.setFormValue(workTemplate);
            });
        }
      }
    }
  }

  cancel() {
    this.router.navigate([`/admin/work-templates`]);
  }
}
