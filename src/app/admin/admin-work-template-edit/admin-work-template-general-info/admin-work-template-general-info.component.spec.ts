import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminWorkTemplateGeneralInfoComponent } from './admin-work-template-general-info.component';

describe('AdminWorkTemplateGeneralInfoComponent', () => {
  let component: AdminWorkTemplateGeneralInfoComponent;
  let fixture: ComponentFixture<AdminWorkTemplateGeneralInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminWorkTemplateGeneralInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminWorkTemplateGeneralInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
