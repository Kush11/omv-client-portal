import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { CanDeactivate } from '@angular/router';
import { Store } from '@ngxs/store';
import { SetActiveWorkTemplateTab } from '../../state/admin-work-planning/admin-work-planning.actions';
import { Subject } from 'rxjs/internal/Subject';
import { AdminWorkTemplateGeneralInfoComponent } from './admin-work-template-general-info.component';

@Injectable({
  providedIn: 'root'
})
export class AdminWorkTemplateGeneralInfoGuard implements CanDeactivate<AdminWorkTemplateGeneralInfoComponent> {

  constructor(private store: Store) { }
  
  canDeactivate(component: AdminWorkTemplateGeneralInfoComponent): Observable<boolean> | Promise<boolean> | boolean {
    if (component.templateForm.touched && component.templateForm.dirty) {
      const subject = new Subject<boolean>();
      component.confirmModal.title = "Confirm Leave";
      component.confirmModal.message = "Are you sure you want to leave? If you do, you'll lose all changes.";
      component.confirmModal.show();
      component.confirmModal.response$ = subject;
      return subject.asObservable()
        .toPromise()
        .then(response => {
          if (!response) {
            this.store.dispatch(new SetActiveWorkTemplateTab('general-info'));
          }
          return response;
        });
    }
    return true;
  }
}