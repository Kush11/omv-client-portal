import { Component, OnInit, ViewChild, OnDestroy, ElementRef } from '@angular/core';
import { AdminWorkPlanningState } from '../../state/admin-work-planning/admin-work-planning.state';
import { Observable } from 'rxjs/internal/Observable';
import { Select, Store } from '@ngxs/store';
import { GridColumn, GridColumnButton } from 'src/app/core/models/grid.column';
import { WorkTemplateVersion } from 'src/app/core/models/entity/work-template';
import { Button } from 'src/app/core/models/button';
import { ActivateWorkTemplate, DownloadWorkTemplate, ImportWorkTemplate } from '../../state/admin-work-planning/admin-work-planning.actions';
import { SubSink } from 'subsink/dist/subsink';
import { ModalComponent } from 'src/app/shared/modal/modal.component';
import { FieldConfiguration } from 'src/app/shared/dynamic-components/field-setting';
import { BaseComponent } from 'src/app/shared/base/base.component';

@Component({
  selector: 'app-admin-work-template-versions',
  templateUrl: './admin-work-template-versions.component.html',
  styleUrls: ['./admin-work-template-versions.component.css']
})
export class AdminWorkTemplateVersionsComponent extends BaseComponent implements OnInit, OnDestroy {

  @Select(AdminWorkPlanningState.getCurrentGroupId) groupId$: Observable<number>;
  @Select(AdminWorkPlanningState.getWorkTemplateVersions) versions$: Observable<WorkTemplateVersion[]>;

  @ViewChild('commentModal') commentModal: ModalComponent;
  @ViewChild('confirmModal') confirmModal: ModalComponent;
  @ViewChild('file') file: ElementRef;

  private subs = new SubSink();
  actions: GridColumnButton[] = [
    { text: 'Download', type: 'button', query: 'canDownload', action: this.download.bind(this), cssClass: 'button-no-outline button-grid-link download-button-qa' },
    { text: 'Activate', type: 'button', query: 'canActivate', action: this.activate.bind(this), cssClass: 'button-no-outline button-grid-link activate-button-qa' }
  ];
  columns: GridColumn[] = [
    { headerText: 'Version', field: 'version', textAlign: 'center', width: '120' },
    { headerText: 'Status', field: 'status', width: '140' },
    { headerText: 'Activated On', field: 'publishedOn', type: 'date', format: 'MMM dd, yyyy  hh:mm a' },
    { headerText: 'Activated By', field: 'publishedBy' },
    { headerText: 'Comments', field: 'comment' },
    { headerText: 'Actions', type: 'buttons', buttons: this.actions }
  ];
  buttons: Button[] = [{ text: 'Import Configuration', type: 'button', action: this.importConfiguration.bind(this), cssClass: 'button-plain import-configuration-btn-qa' }];
  modalButtons: Button[] = [
    { text: 'SAVE CHANGES', type: 'submit', action: this.saveComment.bind(this), cssClass: 'button-round modal-save-changes-qa', right: 30, disabled: false },
    { text: 'Skip', type: 'button', action: this.skipComment.bind(this), cssClass: 'button-no-outline skip-button-qa', override: true }
  ];
  controls: FieldConfiguration[] = [{ type: 'input', label: 'Comment', inputType: 'textarea', rows: 4, name: 'comment', required: true }];
  deleteMessage: string;
  currentVersion: WorkTemplateVersion;
  versions: WorkTemplateVersion[] = [];
  selectedFile: File;
  base64StringFile: string;

  constructor(protected store: Store) {
    super(store);
    this.showLefNav();
    this.setPageTitle("Admin Work Template Versions");
  }

  ngOnInit() {
    this.commentModal.buttons = this.modalButtons;
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

  download(event: WorkTemplateVersion) {
    this.store.dispatch(new DownloadWorkTemplate(event.templateId));
  }

  //#region Activate

  activate(event: WorkTemplateVersion) {
    this.currentVersion = event;
    this.commentModal.reset();
    this.commentModal.show();
  }

  saveComment() {
    if (this.commentModal.dynamicForm.valid && this.commentModal.dynamicForm.dirty) {
      const { comment } = this.commentModal.dynamicForm.value;
      this.store.dispatch(new ActivateWorkTemplate(this.currentVersion.templateId, this.currentVersion.version, comment));
    }
    this.commentModal.closeDialog();
  }

  skipComment() {
    this.store.dispatch(new ActivateWorkTemplate(this.currentVersion.templateId, this.currentVersion.version));
    this.commentModal.closeDialog();
  }

  //#endregion

  //#region Import Configuration

  importConfiguration() {
    this.file.nativeElement.click();
  }

  onFileChosen() {
    const files: { [key: string]: File } = this.file.nativeElement.files;
    this.selectedFile = files[0];
    if (this.selectedFile) {
      let reader = new FileReader();
      reader.readAsDataURL(this.selectedFile);
      reader.onload = () => {
        this.base64StringFile = reader.result.toString().split(',')[1];
        this.file.nativeElement.value = '';
        this.confirmModal.show();
      };
    }
  }

  confirmImport() {
    this.store.dispatch(new ImportWorkTemplate(this.base64StringFile));
  }

  //#endregion
}

