import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminWorkTemplateVersionsComponent } from './admin-work-template-versions.component';

describe('AdminWorkTemplateVersionsComponent', () => {
  let component: AdminWorkTemplateVersionsComponent;
  let fixture: ComponentFixture<AdminWorkTemplateVersionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminWorkTemplateVersionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminWorkTemplateVersionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
