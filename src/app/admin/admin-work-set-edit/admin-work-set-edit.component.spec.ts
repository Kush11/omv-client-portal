import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminWorkSetEditComponent } from './admin-work-set-edit.component';

describe('AdminWorkSetEditComponent', () => {
  let component: AdminWorkSetEditComponent;
  let fixture: ComponentFixture<AdminWorkSetEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminWorkSetEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminWorkSetEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
