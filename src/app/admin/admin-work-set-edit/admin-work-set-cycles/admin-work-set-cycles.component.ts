import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { WorkSetCycle } from 'src/app/core/models/entity/work-set-cycle';
import { Observable } from 'rxjs/internal/Observable';
import { AdminWorkPlanningState } from '../../state/admin-work-planning/admin-work-planning.state';
import { Select, Store } from '@ngxs/store';
import { SubSink } from 'subsink/dist/subsink';
import { BaseComponent } from 'src/app/shared/base/base.component';
import { GetWorkSetCycles, UpdateWorkSetCycle, OpenWorkSetCycle, PublishWorkSetCycle, DeleteWorkSetCycle } from '../../state/admin-work-planning/admin-work-planning.actions';
import { GridColumn, GridColumnButton } from 'src/app/core/models/grid.column';
import { ModalComponent } from 'src/app/shared/modal/modal.component';
import { FormGroup, ValidatorFn, Validators } from '@angular/forms';
import { FieldConfiguration } from 'src/app/shared/dynamic-components/field-setting';
import { DynamicFormComponent } from 'src/app/shared/dynamic-components/components/dynamic-form.component';

@Component({
  selector: 'app-admin-work-set-cycles',
  templateUrl: './admin-work-set-cycles.component.html',
  styleUrls: ['./admin-work-set-cycles.component.css']
})
export class AdminWorkSetCyclesComponent extends BaseComponent implements OnInit, OnDestroy {

  @Select(AdminWorkPlanningState.getWorkSetCycles) cycles$: Observable<WorkSetCycle[]>;
  @Select(AdminWorkPlanningState.getCurrentWorkSetId) currentWorkSetId$: Observable<number>;
  @Select(AdminWorkPlanningState.getTotalWorkSetCycles) total$: Observable<number>;

  @ViewChild("cycleModal") cycleModal: ModalComponent;
  @ViewChild('confirmModal') confirmModal: ModalComponent;

  private subs = new SubSink();
  processForm: FormGroup;
  typeFields = { text: 'type', value: 'id' };
  actions: GridColumnButton[] = [
    { text: 'Edit', type: 'button', query: 'canEdit', action: this.showCycleModal.bind(this), cssClass: 'button-no-outline button-grid-link edit-button-qa' },
    { text: 'Delete', type: 'button', query: 'canDelete', action: this.showConfirmModal.bind(this, ActionType.Delete), cssClass: 'button-no-outline button-grid-link delete-button-qa' },
    { text: 'Open', type: 'button', query: 'canOpen', action: this.showConfirmModal.bind(this, ActionType.Open), cssClass: 'button-no-outline button-grid-link open-button-qa' },
    { text: 'Publish', type: 'button', query: 'canPublish', action: this.showConfirmModal.bind(this, ActionType.Publish), cssClass: 'button-no-outline button-grid-link publish-button-qa' }
  ];
  columns: GridColumn[] = [
    { headerText: 'Start Date', field: 'startDate', type: 'date', format: 'MMM dd, yyyy', useAsLink: true },
    { headerText: 'End Date', field: 'endDate', type: 'date', format: 'MMM dd, yyyy', useAsLink: true },
    { headerText: 'Status', field: 'statusDisplay', type: 'badge' },
    { headerText: 'Actions', type: 'buttons', buttons: this.actions, width: 200 }
  ];
  confirmModalMessage: string;
  confirmModalTitle: string;
  currentCycle: WorkSetCycle;
  cycleControls: FieldConfiguration[] = [
    { type: "date", label: "Start Date", name: "startDate", validations: this.getValidations("Start Date") },
    { type: "date", label: "End Date", name: "endDate", validations: this.getValidations("End Date") }
  ];
  currentTemplateId: number;
  isReadOnlyMode: boolean;
  processModalTitle: string;
  isLoading: boolean;
  currentWorkSetId: number;
  currentPage: number;
  pageSize = 20;
  currentActionType: ActionType;

  constructor(protected store: Store) {
    super(store);
    this.setPageTitle("Admin Work Set Cycles");
  }

  ngOnInit() {
    this.subs.add(
      this.currentWorkSetId$
        .subscribe(id => {
          this.currentWorkSetId = id;
          this.store.dispatch(new GetWorkSetCycles(this.currentWorkSetId, this.currentPage, this.pageSize));
        })
    );
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

  showCycleModal(event: WorkSetCycle) {
    this.currentCycle = event;
    const form = this.cycleModal.dynamicForm.form;
    const startDateControl = this.cycleControls[0];
    const endDateControl = this.cycleControls[1];
    this.cycleModal.setValue(startDateControl.name, new Date(event.startDate));
    this.cycleModal.setValue(endDateControl.name, new Date(event.endDate));
    this.cycleModal.reset(form.value);
    this.cycleModal.show();
  }

  updateCycle(dynamicForm: DynamicFormComponent) {
    if (dynamicForm.valid && dynamicForm.dirty) {
      const value = dynamicForm.value;
      this.currentCycle.startDate = value.startDate;
      this.currentCycle.endDate = value.endDate;

      this.store.dispatch(new UpdateWorkSetCycle(this.currentWorkSetId, this.currentCycle)).toPromise()
        .then(() => {
          this.store.dispatch(new GetWorkSetCycles(this.currentWorkSetId, this.currentPage, this.pageSize));
        });
    }
  }

  openCycle(cycle: WorkSetCycle) {
    this.store.dispatch(new OpenWorkSetCycle(this.currentWorkSetId, cycle)).toPromise()
      .then(() => {
        this.store.dispatch(new GetWorkSetCycles(this.currentWorkSetId, this.currentPage, this.pageSize));
      });
  }

  publishCycle(cycle: WorkSetCycle) {
    this.store.dispatch(new PublishWorkSetCycle(this.currentWorkSetId, cycle.id)).toPromise()
      .then(() => {
        this.store.dispatch(new GetWorkSetCycles(this.currentWorkSetId, this.currentPage, this.pageSize));
      });
  }

  deleteCycle(cycle: WorkSetCycle) {
    this.store.dispatch(new DeleteWorkSetCycle(this.currentWorkSetId, cycle.id)).toPromise()
      .then(() => {
        this.store.dispatch(new GetWorkSetCycles(this.currentWorkSetId, this.currentPage, this.pageSize));
      });
  }

  //#region Confirm Modal Events

  showConfirmModal(type: ActionType, event: WorkSetCycle) {
    this.currentCycle = event;
    this.currentActionType = type;
    switch (type) {
      case ActionType.Delete:
        this.confirmModalTitle = "Confirm Delete";
        this.confirmModalMessage = `Are you sure you want to delete this cycle?`;
        break;
      case ActionType.Open:
        this.confirmModalTitle = "Confirm Open";
        this.confirmModalMessage = `Are you sure you want to open this cycle?`;
        break;
      case ActionType.Publish:
        this.confirmModalTitle = this.currentCycle.isPublished ? "Confirm Republish" : "Confirm Publish";
        this.confirmModalMessage = this.currentCycle.isPublished ?
          `Are you sure you want to republish this cycle?` : `Are you sure you want to publish this cycle?`;
        break;
      default:
        break;
    }
    this.confirmModal.show();
  }

  onConfirmed() {
    switch (this.currentActionType) {
      case ActionType.Delete:
        this.deleteCycle(this.currentCycle);
        break;
      case ActionType.Open:
        this.openCycle(this.currentCycle);
        break;
      case ActionType.Publish:
        this.publishCycle(this.currentCycle);
        break;
      default:
        break;
    }
  }

  //#endregion

  paginationEvent(pageNumber: number) {
    this.currentPage = pageNumber;
    this.store.dispatch(new GetWorkSetCycles(this.currentWorkSetId, this.currentPage, this.pageSize));
    window.scrollTo(0, 0);
  }

  private getValidations(label: string): ValidatorFn[] {
    let validations: ValidatorFn[] = [];
    let requiredValidation: any = {
      name: 'required',
      validator: Validators.required,
      message: `${label} is required`
    };
    validations.push(requiredValidation);
    return validations;
  }
}

export enum ActionType {
  Delete = 'Delete',
  Open = 'Open',
  Publish = 'Publish'
}
