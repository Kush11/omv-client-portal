import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminWorkSetCyclesComponent } from './admin-work-set-cycles.component';

describe('AdminWorkSetCyclesComponent', () => {
  let component: AdminWorkSetCyclesComponent;
  let fixture: ComponentFixture<AdminWorkSetCyclesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminWorkSetCyclesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminWorkSetCyclesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
