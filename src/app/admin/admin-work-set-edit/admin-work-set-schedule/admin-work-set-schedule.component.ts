import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Button } from 'src/app/core/models/button';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { SubSink } from 'subsink/dist/subsink';
import { Select, Store } from '@ngxs/store';
import { AdminWorkPlanningState } from '../../state/admin-work-planning/admin-work-planning.state';
import { Observable } from 'rxjs';
import { WorkSetSchedule, FrequencyType } from 'src/app/core/models/entity/work-set';
import { CreateWorkSetSchedule, UpdateWorkSetSchedule, GetFrequencyTypes, GetCustomFrequencyTypes, GetWorkSet } from '../../state/admin-work-planning/admin-work-planning.actions';
import { Lookup } from 'src/app/core/models/entity/lookup';
import { BaseComponent } from 'src/app/shared/base/base.component';
import { ModalComponent } from 'src/app/shared/modal/modal.component';

@Component({
  selector: 'app-admin-work-set-schedule',
  templateUrl: './admin-work-set-schedule.component.html',
  styleUrls: ['./admin-work-set-schedule.component.css']
})
export class AdminWorkSetScheduleComponent extends BaseComponent implements OnInit, OnDestroy {

  private subs = new SubSink();
  @Select(AdminWorkPlanningState.getCurrentSchedule) schedules$: Observable<WorkSetSchedule>;
  @Select(AdminWorkPlanningState.getFrequencyTypes) frequencyTypes$: Observable<Lookup[]>;
  @Select(AdminWorkPlanningState.getCustomFrequencyTypes) customFrequencyTypes$: Observable<Lookup[]>;
  @Select(AdminWorkPlanningState.getCurrentWorkSetId) worsetIds$: Observable<number>;

  @ViewChild("confirmModal") confirmModal: ModalComponent;

  currentWorkSetId: number;
  scheduleForm: FormGroup
  buttons: Button[] = [
    { text: "SAVE CHANGES", type: 'submit', cssClass: "button-round save-changes-qa", disabled: true, action: this.save.bind(this) },
    { text: "Cancel", type: 'button', cssClass: "button-no-outline cancel-qa", action: this.cancel.bind(this), override: true }
  ];
  workSchedule: WorkSetSchedule;
  workGroupId: number;
  frequencyTypes: FrequencyType[];
  frequencyTypeFields: object = { text: "description", value: "value" };
  startDate: Date;
  selectedType: number;
  isCustomFrequency: boolean;

  constructor(protected store: Store, private formBuilder: FormBuilder) {
    super(store);
    this.setPageTitle("Admin Work Set Schedule");
    this.scheduleForm = this.formBuilder.group({
      cycleName: ['', [Validators.required]],
      frequencyType: ['', [Validators.required]],
      startDate: ['', [Validators.required]],
      endDate: ['', [Validators.required]]
    });
  }

  ngOnInit() {
    this.store.dispatch([new GetFrequencyTypes()]);
    this.subs.add(
      this.schedules$
        .subscribe(schedule => {
          if (schedule) {
            this.workSchedule = schedule;
            this.isCustomFrequency = schedule.frequencyType == 0;
            this.setFormValue(schedule);
          } else {
            this.workSchedule = null;
          }
        }),
      this.worsetIds$.
        subscribe(id => this.currentWorkSetId = id),
      this.scheduleForm.valueChanges
        .subscribe(() => this.buttons[0].disabled = !this.scheduleForm.valid || !this.scheduleForm.dirty)
    );
  }

  ngOnDestroy() {
    this.scheduleForm.reset();
    this.subs.unsubscribe();
  }

  private setFormValue(schedule: WorkSetSchedule) {
    this.scheduleForm.patchValue({
      cycleName: schedule.cycleName,
      frequencyType: schedule.frequencyType,
      startDate: new Date(schedule.startDate),
      endDate: new Date(schedule.endDate)
    });
    if (this.isCustomFrequency) {
      this.addCustomFrequencyControls();
      this.scheduleForm.patchValue({
        customDuration: schedule.customDuration,
        customFrequency: schedule.customFrequency,
        repeatDuration: schedule.repeatDuration,
        repeatFrequency: schedule.repeatFrequency
      });
    }
    this.scheduleForm.reset(this.scheduleForm.value);
  }

  frequencyChanged(args: any) {
    const { itemData } = args;
    if (!itemData) return;
    const { value } = itemData;
    this.isCustomFrequency = value === 0;
    if (this.isCustomFrequency) this.addCustomFrequencyControls();
    else this.removeCustomFrequencyControls();
  }

  save() {
    if (this.scheduleForm.valid) {
      if (this.scheduleForm.dirty) {
        const workSchedule: WorkSetSchedule = { ...this.workSchedule, ...this.scheduleForm.value };
        if (!this.isCustomFrequency) {
          workSchedule.customDuration = null;
          workSchedule.customDurationName = null;
          workSchedule.customFrequency = null;
          workSchedule.repeatDuration = null;
          workSchedule.repeatFrequency = null;
        }
        if (!this.workSchedule) {
          this.store.dispatch(new CreateWorkSetSchedule(this.currentWorkSetId, workSchedule)).toPromise()
            .then(() => this.store.dispatch(new GetWorkSet(this.currentWorkSetId)));
        } else {
          this.store.dispatch(new UpdateWorkSetSchedule(this.currentWorkSetId, workSchedule)).toPromise()
            .then(() => this.store.dispatch(new GetWorkSet(this.currentWorkSetId)));
        }
      }
      this.scheduleForm.reset(this.scheduleForm.value);
    }
  }

  cancel() {
    if (this.workSchedule) {
      this.isCustomFrequency = this.workSchedule.frequencyType == 0;
      this.setFormValue(this.workSchedule);
    } else {
      this.workSchedule = null;
      this.scheduleForm.reset();
    }
  }

  private addCustomFrequencyControls() {
    this.scheduleForm.addControl('customDuration', this.formBuilder.control("", Validators.required));
    this.scheduleForm.addControl('customFrequency', this.formBuilder.control("", Validators.required));
    this.scheduleForm.addControl('repeatDuration', this.formBuilder.control(""));
    this.scheduleForm.addControl('repeatFrequency', this.formBuilder.control(""));
  }

  private removeCustomFrequencyControls() {
    this.scheduleForm.removeControl("customDuration");
    this.scheduleForm.removeControl("customFrequency");
    this.scheduleForm.removeControl("repeatDuration");
    this.scheduleForm.removeControl("repeatFrequency");
  }
}
