import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminWorkSetScheduleComponent } from './admin-work-set-schedule.component';

describe('AdminWorkSetScheduleComponent', () => {
  let component: AdminWorkSetScheduleComponent;
  let fixture: ComponentFixture<AdminWorkSetScheduleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminWorkSetScheduleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminWorkSetScheduleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
