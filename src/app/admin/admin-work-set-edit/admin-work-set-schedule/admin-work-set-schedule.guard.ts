import { CanDeactivate } from '@angular/router';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { Subject } from 'rxjs/internal/Subject';
import { SetActiveWorkSetTab } from '../../state/admin-work-planning/admin-work-planning.actions';
import { Store } from '@ngxs/store';
import { AdminWorkSetScheduleComponent } from './admin-work-set-schedule.component';

@Injectable({
  providedIn: 'root'
})
export class AdminWorkSetScheduleGuard implements CanDeactivate<AdminWorkSetScheduleComponent> {

  constructor(private store: Store) { }

  canDeactivate(component: AdminWorkSetScheduleComponent): Observable<boolean> | Promise<boolean> | boolean {
    if (component.scheduleForm.touched && component.scheduleForm.dirty) {
      const subject = new Subject<boolean>();
      component.confirmModal.title = "Confirm Leave";
      component.confirmModal.message = "Are you sure you want to leave? If you do, you'll lose all changes.";
      component.confirmModal.show();
      component.confirmModal.response$ = subject;
      return subject.asObservable()
        .toPromise()
        .then(response => {
          if (!response) {
            this.store.dispatch(new SetActiveWorkSetTab('/schedule'));
          }
          return response;
        });
    }
    return true;
  }
}