import { SubSink } from 'subsink/dist/subsink';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { GridColumn } from 'src/app/core/models/grid.column';
import { Observable } from 'rxjs/internal/Observable';
import { WorkTemplateHistory } from 'src/app/core/models/entity/work-template';
import { Select, Store } from '@ngxs/store';
import { AdminWorkPlanningState } from '../../state/admin-work-planning/admin-work-planning.state';
import { BaseComponent } from 'src/app/shared/base/base.component';
import { GetWorkSetHistory } from '../../state/admin-work-planning/admin-work-planning.actions';


@Component({
  selector: 'app-admin-work-set-history',
  templateUrl: './admin-work-set-history.component.html',
  styleUrls: ['./admin-work-set-history.component.css']
})
export class AdminWorkSetHistoryComponent extends BaseComponent implements OnInit, OnDestroy {

  @Select(AdminWorkPlanningState.getWorkSetHistory) workSetHistory$: Observable<WorkTemplateHistory[]>;
  @Select(AdminWorkPlanningState.getCurrentWorkSetId) currentWorkSetId$: Observable<number>;
  @Select(AdminWorkPlanningState.getTotalWorkSetHistory) totalHistory$: Observable<number>;

  private subs = new SubSink();
  columns: GridColumn[] = [
    { headerText: 'Date', field: 'createdOn', type: 'date', format: 'MMM dd, yyyy  hh:mm a', width: 180 },
    { headerText: 'Activated By', field: 'createdBy', width: 250 },
    { headerText: 'Event', field: 'event' }
  ];
  currentPage = 1;
  pageSize = 12;
  currentWorkSetId: number;

  constructor(protected store: Store) {
    super(store);
    this.setPageTitle("Admin Work Set History");
  }

  ngOnInit() {
    this.subs.add(
      this.currentWorkSetId$
        .subscribe(id => {
          this.currentWorkSetId = id;
          this.store.dispatch(new GetWorkSetHistory(this.currentWorkSetId, this.currentPage, this.pageSize));
        })
    );
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

  paginationEvent(pageNumber: number) {
    this.currentPage = pageNumber;
    this.store.dispatch(new GetWorkSetHistory(this.currentWorkSetId, pageNumber, this.pageSize));
    window.scrollTo(0, 0);
  }
}
