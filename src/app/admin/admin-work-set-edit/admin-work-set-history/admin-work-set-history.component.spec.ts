import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminWorkSetHistoryComponent } from './admin-work-set-history.component';

describe('AdminWorkSetHistoryComponent', () => {
  let component: AdminWorkSetHistoryComponent;
  let fixture: ComponentFixture<AdminWorkSetHistoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminWorkSetHistoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminWorkSetHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
