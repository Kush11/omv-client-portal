import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { SubSink } from 'subsink/dist/subsink';
import { Select, Store } from '@ngxs/store';
import { AdminWorkPlanningState } from '../../state/admin-work-planning/admin-work-planning.state';
import { WorkSet } from 'src/app/core/models/entity/work-set';
import { Observable, Subject } from 'rxjs';
import { Button } from 'src/app/core/models/button';
import { Router } from '@angular/router';
import { WorkTemplate } from 'src/app/core/models/entity/work-template';
import { CreateWorkSet, UpdateWorkSet, GetWorkSetTemplateGroups, GetWorkSetAssets, ClearWorkSetAssets, GetWorkSet } from '../../state/admin-work-planning/admin-work-planning.actions';
import { ModalComponent } from 'src/app/shared/modal/modal.component';
import { WorkItem } from 'src/app/core/models/entity/work-item';
import { MetadataListItem } from 'src/app/core/models/entity/metadata-list-item';
import { BaseComponent } from 'src/app/shared/base/base.component';
import { ShowErrorMessage } from 'src/app/state/app.actions';
import { TreeViewComponent } from 'src/app/shared/tree-view/tree-view.component';
import { DirectoryService } from 'src/app/core/services/business/directory/directory.service';
import { Directory } from 'src/app/core/models/entity/directory';

@Component({
  selector: 'app-admin-work-set-general-info',
  templateUrl: './admin-work-set-general-info.component.html',
  styleUrls: ['./admin-work-set-general-info.component.css']
})
export class AdminWorkSetGeneralInfoComponent extends BaseComponent implements OnInit, OnDestroy {

  private subs = new SubSink();
  @Select(AdminWorkPlanningState.getWorkSetTemplates) workTemplates$: Observable<WorkTemplate[]>;
  @Select(AdminWorkPlanningState.getCurrentWorkSet) currentWorkSet$: Observable<WorkSet>;
  @Select(AdminWorkPlanningState.getWorkItems) currentWorkItems$: Observable<WorkItem[]>;
  @Select(AdminWorkPlanningState.getWorkSetAssets) assets$: Observable<MetadataListItem[]>;
  @Select(AdminWorkPlanningState.getCurrentWorkSetId) currentWorkSetId$: Observable<WorkSet>;

  @ViewChild("confirmModal") confirmModal: ModalComponent;
  @ViewChild('appTreeView') appTreeView: TreeViewComponent;

  setForm: FormGroup;
  workSet: WorkSet;
  workSetId: number;
  workGroupId: number;
  workTemplates: Array<WorkTemplate>;
  buttons: Button[] = [
    { text: "SAVE CHANGES", type: 'submit', cssClass: "button-round save-changes-qa" },
    { text: "Cancel", type: 'button', cssClass: "button-no-outline cancel-qa", action: this.cancel.bind(this) }
  ];
  currentWorkSet: WorkSet;
  workTemplateFields: Object = { text: 'name', value: 'groupId' };
  workAssetFields: Object = { text: 'description', value: 'id' };
  currentWorkItems: WorkItem[] = [];
  assetPlaceholder = "Please select";
  isFormLocked: boolean;
  directories: Directory[];
  field: Object;
  parentID: number;
  currentDirectoryId: number;
  directoryPath: string;

  constructor(protected store: Store, private router: Router, private formBuilder: FormBuilder, private directoryService: DirectoryService) {
    super(store);
    this.setPageTitle("Admin Work Set General Info");
    this.setForm = this.formBuilder.group({
      name: ['', [Validators.required]],
      templateGroupId: ['', [Validators.required]],
      asset: ['', [Validators.required]]
    });
  }

  ngOnInit() {
    this.store.dispatch(new GetWorkSetTemplateGroups());
    this.subs.add(
      this.workTemplates$
        .subscribe(templates => {
          if (templates) this.workTemplates = templates;
        }),
      this.currentWorkSet$
        .subscribe(workset => {
          if (workset) {
            this.workSet = workset;
            this.currentDirectoryId = workset.directoryId;
            this.directoryPath = workset.directoryPath;
            this.setFormValue(workset);
          } else {
            this.setForm.reset();
            this.workSet = null;
          }
        }),
      this.directoryService.getFolders()
        .subscribe(directories => {
          this.showSpinner();
          directories.forEach(item => item.icon = 'folder');
          this.directories = directories;
          this.field = {
            dataSource: this.directories, id: 'id', parentID: 'parentId',
            text: 'name', iconCss: 'icon', hasChildren: 'hasChild'
          };
          this.hideSpinner();
        }, err => this.store.dispatch(new ShowErrorMessage(err))),
      this.currentWorkItems$
        .subscribe(items => this.currentWorkItems = items)
    );
  }

  ngOnDestroy() {
    this.setForm.reset();
    this.subs.unsubscribe();
  }

  private setFormValue(set: WorkSet) {
    this.setForm.setValue({
      name: set.name,
      templateGroupId: set.templateGroupId,
      asset: set.asset
    });
    if (set.metadataListId) {
      this.store.dispatch(new GetWorkSetAssets(set.metadataListId)).toPromise()
        .then(() => {
          this.setForm.reset(this.setForm.value);
        });
    }
    else {
      this.store.dispatch(new ClearWorkSetAssets()).toPromise()
        .then(() => {
          this.setForm.reset(this.setForm.value);
        });
    };
    this.setForm.reset(this.setForm.value);
  }

  onTemplateChange(args: any) {
    if (!args.itemData) return;
    const { metadataListId } = args.itemData as WorkTemplate;
    if (metadataListId) this.store.dispatch(new GetWorkSetAssets(metadataListId));
    else this.store.dispatch(new ClearWorkSetAssets);
  }

  async save() {
    if (this.setForm.valid) {
      if (this.setForm.dirty) {
        const workSet: WorkSet = { ...this.workSet, ...this.setForm.value };
        workSet.directoryId = this.currentDirectoryId;
        workSet.directoryPath = this.directoryPath;
        const template = this.workTemplates.find(t => t.groupId === workSet.templateGroupId);
        if (!template) {
          this.store.dispatch(new ShowErrorMessage("Template was not found."));
          return;
        }
        workSet.templateId = template.activeId;
        if (!this.workSet) {
          this.store.dispatch(new CreateWorkSet(workSet));
          this.subs.sink =
            this.currentWorkSetId$
              .subscribe(id => {
                if (id) {
                  this.setForm.reset();
                  this.router.navigate([`/admin/work-sets/${id}/general-info`]);
                }
              });
        } else {
          this.currentWorkSet = workSet;
          if (this.currentWorkItems.length > 0) {
            const subject = new Subject<boolean>();
            this.confirmModal.title = "Confirm Update";
            this.confirmModal.message = "This update will delete all associated work-items. Are you sure you want to continue?";
            this.confirmModal.show();
            this.confirmModal.response$ = subject;
            const yes = await subject.toPromise();
            if (!yes) return;
          }
          this.updateWorkSet();
        }
      }
    }
  }

  cancel() {
    this.router.navigate([`/admin/work-sets`]);
  }

  updateWorkSet() {
    this.subs.sink =
      this.store.dispatch(new UpdateWorkSet(this.currentWorkSet.id, this.currentWorkSet))
        .subscribe(() => {
          this.store.dispatch(new GetWorkSet(this.currentWorkSet.id));
          this.setForm.reset(this.setForm.value);
        });
  }

  onNodeExpanding() {
    this.parentID = Number(this.appTreeView.parentID);
    this.showSpinner();
    this.directoryService
      .getFolders(Number(this.parentID))
      .toPromise()
      .then(
        folders => {
          folders.forEach(folder => (folder.icon = 'folder'));
          this.addFolders(folders);
          this.appTreeView.addNodes(folders);
          this.hideSpinner();
        }, err => this.showErrorMessage(err));
  }

  onNodeSelected(args: any) {
    this.currentDirectoryId = Number(args.id);
    this.setForm.markAsTouched();
    this.setForm.markAsDirty();
    this.directoryPath = '';
    this.buildDirectoryPath(this.currentDirectoryId);
  }

  private addFolders(directories: Directory[]) {
    directories.forEach(directory => {
      const folderExists = this.directories.some(f => f.id === directory.id);
      if (folderExists === false) {
        this.directories = [...this.directories, directory];
      }
    });
  }

  private buildDirectoryPath(directoryId: number) {
    const directory = this.directories.find(x => x.id === directoryId);
    const parent = this.directories.find(x => x.id === directory.parentId);
    if (parent) {
      this.directoryPath = this.directoryPath ? `${directory.name} > ${this.directoryPath}` : `${directory.name}`;
      return this.buildDirectoryPath(parent.id);
    }
    return this.directoryPath = this.directoryPath ? `${directory.name} > ${this.directoryPath}` : `${directory.name}`;
  }
}


