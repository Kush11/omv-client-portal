import { AdminWorkSetGeneralInfoComponent } from './admin-work-set-general-info.component';
import { CanDeactivate } from '@angular/router';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { Subject } from 'rxjs/internal/Subject';
import { SetActiveWorkSetTab } from '../../state/admin-work-planning/admin-work-planning.actions';
import { Store } from '@ngxs/store';

@Injectable({
  providedIn: 'root'
})
export class AdminWorkSetGeneralInfoGuard implements CanDeactivate<AdminWorkSetGeneralInfoComponent> {

  constructor(private store: Store) { }

  canDeactivate(component: AdminWorkSetGeneralInfoComponent): Observable<boolean> | Promise<boolean> | boolean {
    if (component.setForm.touched && component.setForm.dirty) {
      const subject = new Subject<boolean>();
      component.confirmModal.title = "Confirm Leave";
      component.confirmModal.message = "Are you sure you want to leave? If you do, you'll lose all changes.";
      component.confirmModal.show();
      component.confirmModal.response$ = subject;
      return subject.asObservable()
        .toPromise()
        .then(response => {
          if (!response) {
            this.store.dispatch(new SetActiveWorkSetTab('/general-info'));
          }
          return response;
        });
    }
    return true;
  }
}