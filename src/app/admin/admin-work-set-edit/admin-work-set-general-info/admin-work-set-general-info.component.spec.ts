import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminWorkSetGeneralInfoComponent } from './admin-work-set-general-info.component';

describe('AdminWorkSetGeneralInfoComponent', () => {
  let component: AdminWorkSetGeneralInfoComponent;
  let fixture: ComponentFixture<AdminWorkSetGeneralInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminWorkSetGeneralInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminWorkSetGeneralInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
