import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminWorkSetWorkItemsComponent } from './admin-work-set-work-items.component';

describe('AdminWorkSetWorkItemsComponent', () => {
  let component: AdminWorkSetWorkItemsComponent;
  let fixture: ComponentFixture<AdminWorkSetWorkItemsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminWorkSetWorkItemsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminWorkSetWorkItemsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
