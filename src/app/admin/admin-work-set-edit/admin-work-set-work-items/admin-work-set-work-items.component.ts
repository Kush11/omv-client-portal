import { Component, OnInit, ViewChild, OnDestroy, ElementRef } from '@angular/core';
import { GridColumn, GridColumnButton } from 'src/app/core/models/grid.column';
import { Store, Select } from '@ngxs/store';
import { Button } from 'src/app/core/models/button';
import { AdminWorkPlanningState } from '../../state/admin-work-planning/admin-work-planning.state';
import { WorkItem } from 'src/app/core/models/entity/work-item';
import { Observable } from 'rxjs';
import { ModalComponent } from 'src/app/shared/modal/modal.component';
import { FieldConfiguration } from 'src/app/shared/dynamic-components/field-setting';
import { SubSink } from 'subsink/dist/subsink';
import { WorkTemplateProcess } from 'src/app/core/models/entity/work-template';
import { DeletWorkItem, DownloadWorkItem, ImportWorkSetItem, GetWorkSet } from '../../state/admin-work-planning/admin-work-planning.actions';
import { CreateWorkItem, UpdateWorkItem } from '../../state/admin-work-planning/admin-work-planning.actions';
import { AdminGroupState } from '../../state/admin-groups/admin-groups.state';
import { Group } from 'src/app/core/models/entity/group';
import { WorkSet } from 'src/app/core/models/entity/work-set';
import { BaseComponent } from 'src/app/shared/base/base.component';
import { AdminWorkSetWorkItemsService } from './admin-work-set-work-items.service';
import { DynamicFormComponent } from 'src/app/shared/dynamic-components/components/dynamic-form.component';
import { ShowErrorMessage } from 'src/app/state/app.actions';
import { DateService } from 'src/app/core/services/business/dates/date.service';

@Component({
  selector: 'app-admin-work-set-work-items',
  templateUrl: './admin-work-set-work-items.component.html',
  styleUrls: ['./admin-work-set-work-items.component.css'],
  providers: [AdminWorkSetWorkItemsService]
})
export class AdminWorkSetWorkItemsComponent extends BaseComponent implements OnInit, OnDestroy {

  @Select(AdminWorkPlanningState.getWorkSetProcesses) processes$: Observable<WorkTemplateProcess[]>;
  @Select(AdminWorkPlanningState.getCurrentWorkSetId) currentSetId$: Observable<number>;
  @Select(AdminWorkPlanningState.getWorkItems) workItems$: Observable<WorkItem[]>;
  @Select(AdminWorkPlanningState.getCurrentWorkSetTemplateId) currentTemplateId$: Observable<number>;
  @Select(AdminWorkPlanningState.getCurrentWorkSet) currentWorkSet$: Observable<WorkSet>;
  @Select(AdminGroupState.getActiveGroups) groups$: Observable<Group[]>;

  @ViewChild('confirmModal') confirmModal: ModalComponent;
  @ViewChild('itemModal') itemModal: ModalComponent;
  @ViewChild('itemDetailsModal') itemDetailsModal: ModalComponent;
  @ViewChild('confirmModalImport') confirmImportModal: ModalComponent;
  @ViewChild('file') file: ElementRef;

  private subs = new SubSink();
  currentTemplateId: number;
  processes: WorkTemplateProcess[];
  processFields = { text: 'name', value: 'id' };
  workGroupId: number;
  deleteMessage: string;
  comfirmImportMessage: string;
  isReadOnlyMode: boolean;
  currentWorkSet: WorkSet;
  selectedFile: File;
  base64StringFile: string;
  actions: GridColumnButton[] = [
    { text: 'Remove', type: 'button', visible: true, action: this.showConfirmDialog.bind(this), cssClass: 'button-no-outline button-grid-link remove-button-qa' },
    { text: 'Edit', type: 'button', visible: true, action: this.showItemModal.bind(this), cssClass: 'button-no-outline button-grid-link edit-button-qa' }
  ];
  columns: GridColumn[] = [];
  buttons: Button[] = [
    { text: "Add Work Item", type: 'button', action: this.addWorkItem.bind(this), cssClass: "button-plus sm add-work-item-qa" },
    { text: "Download Work Items", type: 'button', action: this.downloadItem.bind(this), override: true, cssClass: "button-plain download-work-item-qa" },
    { text: "Import Work Items", type: 'button', action: this.importConfiguration.bind(this), cssClass: "button-plain import-work-item-qa" }
  ];
  workItems: WorkItem[];
  currentWorkItem: any;
  currentSetId: number;
  currentGroupId: number;
  workItemControls: FieldConfiguration[] = [];
  showWorkItems: boolean;
  dynamicWorkItems: any[] = [];

  constructor(protected store: Store, private service: AdminWorkSetWorkItemsService, private dateService: DateService) {
    super(store);
    this.setPageTitle("Admin Work Set Work Items");
  }

  ngOnInit() {
    this.subs.add(
      this.currentTemplateId$.
        subscribe(templateId => {
          this.currentTemplateId = templateId;
        }),
      this.currentWorkSet$
        .subscribe(async workset => {
          if (workset) {
            this.showSpinner();
            this.service.getWorkItemControls(workset.templateId)
              .then(controls => {
                this.workItemControls = controls;
                const sortColumn: GridColumn = { headerText: '#', field: this.service.sortControlName, width: 40 };
                this.columns = [...this.columns, sortColumn];
                const uniqueControls = this.workItemControls.filter(w => w.unique);
                uniqueControls.forEach(control => {
                  const column: GridColumn = { headerText: control.label, field: control.name.toLowerCase(), useAsLink: true };
                  this.columns = [...this.columns, column];
                });
                const processColumn: GridColumn = { headerText: 'Process', field: '_processColumn' };
                const actionsColumn: GridColumn = { headerText: 'Actions', type: 'buttons', buttons: this.actions, width: 160 };
                this.columns = [...this.columns, processColumn];
                this.columns = [...this.columns, actionsColumn];
                this.showWorkItems = true;
                this.hideSpinner();
              }, error => {
                const processColumn: GridColumn = { headerText: 'Process', field: '' };
                const actionsColumn: GridColumn = { headerText: 'Actions', type: 'buttons', buttons: this.actions, width: 160 };
                this.columns = [...this.columns, processColumn];
                this.columns = [...this.columns, actionsColumn];
                this.showWorkItems = true;
                this.store.dispatch(new ShowErrorMessage(error));
              });
          }
          this.currentWorkSet = workset;
        }),
      this.workItems$
        .subscribe(items => {
          this.workItems = items;
          if (this.workItems) {
            this.dynamicWorkItems = [];
            this.workItems.forEach(item => {
              const data = JSON.parse(item.data);
              let workItem = {};
              for (let key in data) {
                const value = this.dateService.isValid(data[key]) ? this.dateService.formatToShortdate(data[key]) : data[key];
                workItem[key.toLowerCase()] = value;
              }
              workItem = {
                ...workItem,
                id: item.id,
                _processColumn: item.processName
              };
              workItem[this.service.processControlName] = item.processId;
              workItem[this.service.sortControlName] = item.sort;
              this.dynamicWorkItems = [...this.dynamicWorkItems, workItem];
            });
          }
        }),
      this.currentSetId$
        .subscribe(id => this.currentSetId = id)
    );
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

  addWorkItem() {
    this.currentWorkItem = null;
    this.itemModal.enableAll();
    this.itemModal.reset();
    this.itemModal.show();
  }

  showItemModal(workItem: any) {
    this.currentWorkItem = workItem;
    this.workItemControls.forEach((control) => {
      const propName = control.name;
      const propValue = workItem[propName];
      const value = this.dateService.isValidFormat(propValue, "MMM DD, YYYY") ? new Date(propValue) : propValue;
      const disabled = control.unique || control.name === this.service.processControlName;
      this.itemModal.setValue(propName, value, disabled);
    });
    const form = this.itemModal.dynamicForm.form;
    this.itemModal.reset(form.value);
    this.itemModal.show();
  }

  onFileChosen() {
    const files: { [key: string]: File } = this.file.nativeElement.files;
    this.selectedFile = files[0];
    if (this.selectedFile) {
      let reader = new FileReader();
      reader.readAsDataURL(this.selectedFile);
      reader.onload = () => {
        this.base64StringFile = reader.result.toString().split(',')[1];
        this.file.nativeElement.value = '';
        this.confirmImportModal.show();
      };
    }
  }

  importConfiguration() {
    this.file.nativeElement.click();
  }

  saveItem(dynamicForm: DynamicFormComponent) {
    if (!this.currentWorkSet) return;
    if (dynamicForm.valid) {
      if (dynamicForm.dirty) {
        const processControl = dynamicForm.controls.find(c => c.name === this.service.processControlName);
        const sortControl = dynamicForm.controls.find(c => c.name === this.service.sortControlName);
        if (!processControl) return;

        const processId = processControl.value;
        dynamicForm.removeControl(this.service.processControlName);
        const sortOrder = sortControl.value;
        dynamicForm.removeControl(this.service.sortControlName);
        const data = JSON.stringify(dynamicForm.value);

        if (!this.currentWorkItem) {
          const workItem: WorkItem = {
            processId: processId,
            workSetId: this.currentWorkSet.id,
            data: data,
            sort: sortOrder
          };
          this.store.dispatch(new CreateWorkItem(this.currentSetId, workItem))
            .toPromise()
            .then(() => {
              this.store.dispatch(new GetWorkSet(this.currentWorkSet.id));
            });
        } else {
          this.currentWorkItem.processId = processId;
          this.currentWorkItem.data = data;
          this.currentWorkItem.sort = sortOrder;
          this.store.dispatch(new UpdateWorkItem(this.currentSetId, this.currentWorkItem))
            .toPromise()
            .then(() => {
              this.store.dispatch(new GetWorkSet(this.currentWorkSet.id));
            });
        }
        this.itemModal.controls = [];
      }
    }
  }

  //#region Confirm Delete Dialog

  showConfirmDialog(item: any) {
    this.currentWorkItem = item;
    this.deleteMessage = `Are you sure you want to delete the work item?`;
    this.confirmModal.show();
  }

  deleteItem() {
    this.store.dispatch(new DeletWorkItem(this.currentSetId, this.currentWorkItem.id))
      .toPromise()
      .then(() => {
        this.store.dispatch(new GetWorkSet(this.currentWorkSet.id));
      });
    this.itemModal.controls = [];
  }

  //#endregion

  downloadItem() {
    this.store.dispatch(new DownloadWorkItem(this.currentWorkSet));
  }

  confirmImport() {
    this.store.dispatch(new ImportWorkSetItem(this.base64StringFile))
      .toPromise()
      .then(() => {
        this.store.dispatch(new GetWorkSet(this.currentWorkSet.id));
      });
  }
}
