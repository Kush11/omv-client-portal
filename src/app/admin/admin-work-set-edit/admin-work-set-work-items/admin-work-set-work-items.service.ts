import { Injectable } from '@angular/core';
import { WorkTemplatesService } from 'src/app/core/services/business/work-templates/work-templates.service';
import { MetadataField } from 'src/app/core/models/entity/metadata-field';
import { FieldConfiguration } from 'src/app/shared/dynamic-components/field-setting';
import { MetadataFieldType } from 'src/app/core/enum/metadataFieldType';
import { WorkTemplateProcess } from 'src/app/core/models/entity/work-template';
import { ListItem } from 'src/app/core/models/entity/list-item';
import { ValidatorFn, Validators } from '@angular/forms';

@Injectable({
  providedIn: "root"
})
export class AdminWorkSetWorkItemsService {

  processControlName = "_workTemplateProcess";
  sortControlName = "_sortControlName";
  workTemplateFields: FieldConfiguration[];

  constructor(private workTemplatesService: WorkTemplatesService) { }

  async getWorkItemControls(workTemplateId: number): Promise<FieldConfiguration[]> {
    let fieldConfigs: FieldConfiguration[] = [];

    const workTemplateFieldsModel = await this.workTemplatesService.getFields(workTemplateId);
    const workTemplateFields = workTemplateFieldsModel.fields;
    const workTempleteProcesses = workTemplateFieldsModel.processes;

    let options = this.mapProcessesToOptions(workTempleteProcesses);
    const processField = this.buildProcessControl(options);
    fieldConfigs = [...fieldConfigs, processField];

    workTemplateFields.forEach(field => {
      const config = this.buildControl(field);
      fieldConfigs = [...fieldConfigs, config];
    });

    const sortField = this.buildSortControl();
    fieldConfigs = [...fieldConfigs, sortField];
    return fieldConfigs;
  }

  buildControl(field: MetadataField): FieldConfiguration {
    let config: FieldConfiguration;
    switch (field.type) {
      case MetadataFieldType.Text:
        config = this.buildTextBox(field);
        break;
      case MetadataFieldType.Select:
        config = this.buildDropdown(field);
        break;
      case MetadataFieldType.Date:
        config = this.buildDate(field);
        break;
      case MetadataFieldType.Nested:
        config = this.buildDependentDropdown(field);
        break;
    }
    return config;
  }

  private buildTextBox(item: MetadataField): FieldConfiguration {
    return {
      type: "input",
      data: item,
      id: item.id,
      cssClass: 'col-md-12',
      label: item.label || item.name,
      overrideLabel: item.overrideLabel,
      inputType: "text",
      name: item.name.toLowerCase(),
      order: item.order,
      placeholder: `Enter ${item.label}`,
      unique: item.isUnique,
      value: '',
      validations: this.getValidations(item)
    };
  }

  private buildDropdown(item: MetadataField): FieldConfiguration {
    return {
      type: "select",
      data: item,
      id: item.id,
      cssClass: 'col-md-12',
      fields: { text: 'description', value: 'value' },
      label: item.label || item.name,
      name: item.name.toLowerCase(),
      options: item.options,
      order: item.order,
      unique: item.isUnique,
      placeholder: `Select ${item.label}`,
      value: '',
      validations: this.getValidations(item)
    };
  }

  private buildDependentDropdown(item: MetadataField): FieldConfiguration {
    return {
      type: "select",
      data: item,
      id: item.id,
      parentId: item.parentId,
      cssClass: 'col-md-12',
      label: item.label,
      name: item.name.toLowerCase(),
      order: item.order,
      unique: item.isUnique,
      isNested: true,
      isEnabled: false,
      placeholder: `Select ${item.label}`,
      value: '',
      validations: this.getValidations(item)
    };
  }

  private buildDate(item: MetadataField): FieldConfiguration {
    return {
      type: "date",
      data: item,
      id: item.id,
      cssClass: 'col-md-12',
      label: item.label,
      name: item.name.toLowerCase(),
      order: item.order,
      unique: item.isUnique,
      placeholder: `Select`,
      value: '',
      validations: this.getValidations(item)
    };
  }

  private buildProcessControl(options: ListItem[]): FieldConfiguration {
    return {
      type: "select",
      cssClass: 'col-md-12',
      label: 'Process',
      name: this.processControlName,
      options: options,
      order: -100,
      placeholder: 'Select Process',
      fields: { text: 'description', value: 'value' },
      validationMessage: 'Please select a process',
      value: '',
      validations: this.getProcessValidations()
    };
  }

  private mapProcessesToOptions(workTemplateProcesses: WorkTemplateProcess[]): ListItem[] {
    let options: ListItem[] = [];
    workTemplateProcesses.forEach(process => {
      let option: ListItem = {
        description: `${process.code} - ${process.name}`,
        value: process.id
      }
      options = [
        ...options,
        option
      ];
    });
    return options;
  }

  private buildSortControl(): FieldConfiguration {
    return {
      type: "input",
      cssClass: 'col-md-12',
      label: 'Sort Order',
      inputType: "text",
      name: this.sortControlName,
      order: 1000,
      placeholder: `Enter Sort Order`,
      value: ''
    };
  }

  private getValidations(item: MetadataField): ValidatorFn[] {
    let validations: ValidatorFn[] = [];
    if (item.isUnique) {
      let requiredValidation: any = {
        name: 'required',
        validator: Validators.required,
        message: `${item.label || item.name} is required`
      };
      validations.push(requiredValidation);
    }
    return validations;
  }

  private getProcessValidations(): ValidatorFn[] {
    let validations: ValidatorFn[] = [];
    let requiredValidation: any = {
      name: 'required',
      validator: Validators.required,
      message: `Process is required`
    };
    validations.push(requiredValidation);
    return validations;
  }
}