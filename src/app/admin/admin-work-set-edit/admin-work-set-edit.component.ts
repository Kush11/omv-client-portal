import { Component, OnInit, OnDestroy } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { AppState } from 'src/app/state/app.state';
import { Observable } from 'rxjs';
import { Breadcrumb } from 'src/app/core/models/breadcrumb';
import { SubSink } from 'subsink/dist/subsink';
import { Tab } from 'src/app/core/models/tab';
import { Router, ActivatedRoute } from '@angular/router';
import { BaseComponent } from 'src/app/shared/base/base.component';
import { AdminWorkPlanningState } from '../state/admin-work-planning/admin-work-planning.state';
import { WorkSet } from 'src/app/core/models/entity/work-set';
import { SetBreadcrumbs, ClearBreadcrumbs } from 'src/app/state/app.actions';
import { SetCurrentWorkSetId, GetWorkSet, ClearWorkSet, SetActiveWorkSetTab } from '../state/admin-work-planning/admin-work-planning.actions';

const GENERAL_INFO_TAB = 'general-info';
const SCHEDULE_TAB = 'schedule';
const CYCLES_TAB = 'cycles';
const WORK_ITEMS_TAB = 'work-items';
const HISTORY_TAB = 'history';

@Component({
  selector: 'app-admin-work-set-edit',
  templateUrl: './admin-work-set-edit.component.html',
  styleUrls: ['./admin-work-set-edit.component.css']
})

export class AdminWorkSetEditComponent extends BaseComponent implements OnInit, OnDestroy {

  @Select(AppState.getBreadcrumbs) breadcrumbs$: Observable<Breadcrumb[]>;
  @Select(AdminWorkPlanningState.getCurrentWorkSet) currentWorkSet$: Observable<WorkSet>;
  @Select(AdminWorkPlanningState.getCurrentSchedule) currentSchedule$: Observable<WorkSet>;
  @Select(AdminWorkPlanningState.getActiveWorksetTab) activeTab$: Observable<string>;

  private subs = new SubSink();
  showGeneralInfoTab: boolean;
  showAssets: boolean;
  showScheduleTab: boolean;
  showWorkItemsTab: boolean;
  showCyclesTab: boolean;
  showHistoryTab: boolean;
  workSetId: number;

  versionFields: object = { text: 'version', value: 'version' };
  selectedVersion: number;

  tabs: Tab[] = [
    { link: GENERAL_INFO_TAB, name: 'General Info' },
    { link: SCHEDULE_TAB, name: 'Schedule' },
    { link: CYCLES_TAB, name: 'Cycles' },
    { link: WORK_ITEMS_TAB, name: 'Work Items' },
    { link: HISTORY_TAB, name: 'History' },
  ];

  constructor(protected store: Store, private router: Router, private route: ActivatedRoute) {
    super(store);
    this.showLefNav();
  }

  ngOnInit() {
    this.setActiveTab(this.router.url);
    this.subs.add(
      this.route.params
        .subscribe(params => {
          const { id } = params;
          let workSetId = Number(id);
          if (!Number.isNaN(workSetId)) {
            this.workSetId = workSetId;
            this.store.dispatch(new SetCurrentWorkSetId(this.workSetId));
            this.store.dispatch(new GetWorkSet(this.workSetId));
          } else {
            this.store.dispatch(new SetCurrentWorkSetId(null));
            this.store.dispatch(new ClearWorkSet());
          }
        }),
      this.currentWorkSet$
        .subscribe(workset => {
          const scheduleTab = this.tabs[1]; // if there's no workset, lock the schedule tab
          const cyclesTab = this.tabs[2]; // if there's no workset, lock the cycles tab
          const itemTab = this.tabs[3]; // if there's no workset, lock the Item tab
          scheduleTab.disabled = !workset;
          cyclesTab.disabled = !workset;
          itemTab.disabled = !workset;
          if (workset) {
            const crumbs: Breadcrumb[] = [
              { link: '/admin/work-sets', name: 'Listings' },
              { isFinal: true, name: workset.name }
            ];
            this.store.dispatch(new SetBreadcrumbs(crumbs));
          } else {
            const crumbs: Breadcrumb[] = [
              { link: '/admin/work-sets/', name: 'Listings' },
              { isFinal: true, name: 'New Work Set' }
            ];
            this.store.dispatch(new SetBreadcrumbs(crumbs));
          }
        }),
      this.activeTab$
        .subscribe(url => {
          if (url) {
            this.clearActiveTab();
            this.setActiveTab(url);
            this.store.dispatch(new SetActiveWorkSetTab(null));
          }
        })
    );
  }

  ngOnDestroy() {
    this.clearActiveTab();
    this.store.dispatch(new ClearBreadcrumbs());
    this.store.dispatch(new ClearWorkSet());
    this.subs.unsubscribe();
  }

  switchBreadCrumbs(breadcrumbLink: string) {
    this.router.navigate([breadcrumbLink]);
  }

  switchTabs(tabLink: any) {
    const route = `/admin/work-sets/${this.workSetId}/${tabLink}`;
    this.router.navigate([route]);
  }

  onRouteChange(url: string) {
    this.clearActiveTab();
    this.setActiveTab(url);
  }

  setActiveTab(tabLink: string) {
    if (tabLink.includes(GENERAL_INFO_TAB)) {
      this.showGeneralInfoTab = true;
    } else if (tabLink.includes(SCHEDULE_TAB)) {
      this.showScheduleTab = true;
    } else if (tabLink.includes(CYCLES_TAB)) {
      this.showCyclesTab = true;
    } else if (tabLink.includes(WORK_ITEMS_TAB)) {
      this.showWorkItemsTab = true;
    } else if (tabLink.includes(HISTORY_TAB)) {
      this.showHistoryTab = true;
    }
    const activeTab = this.tabs.find(x => tabLink.includes(x.link));
    if (activeTab)
      activeTab.isActive = true;
  }

  clearActiveTab() {
    this.showGeneralInfoTab = this.showAssets = this.showScheduleTab =
      this.showWorkItemsTab = this.showCyclesTab = this.showHistoryTab = false;
    this.tabs.forEach(x => x.isActive = false);
  }
}


