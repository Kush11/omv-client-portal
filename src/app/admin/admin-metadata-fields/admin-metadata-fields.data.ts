export const MetadataDataTypes: Type[] = [
  { text: 'Date', value: 'date' },
  { text: 'Double', value: 'double' },
  { text: 'Int32', value: 'int32' },
  { text: 'Int64', value: 'int64' },
  { text: 'String', value: 'string' }
];

export const MetadataSearchTypes: Type[] = [
  { text: 'None', value: 'none' },
  { text: 'Checkbox', value: 'checkbox' },
  { text: 'Date Range', value: 'date' },
  { text: 'Range Slider', value: 'range' }
];

export const MetadataControlTypes: Type[] = [
  { text: 'Dropdown', value: 'Select' },
  { text: 'Nested Dropdown', value: 'Nested' },
  { text: 'Text', value: 'Text' },
  { text: 'Date Picker', value: 'Date' }
]

export class Type {
  text: string;
  value: string;
}