import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { ListComponent } from 'src/app/shared/list/list.component';
import { Store, Select } from '@ngxs/store';
import { GridColumn, GridColumnButton } from 'src/app/core/models/grid.column';
import { Observable } from 'rxjs/internal/Observable';
import { MetadataField } from 'src/app/core/models/entity/metadata-field';
import { DialogComponent } from '@syncfusion/ej2-angular-popups';
import { FormGroup, FormBuilder, Validators, ValidatorFn, FormArray } from '@angular/forms';
import { MetadataList } from 'src/app/core/models/entity/metadata-list';
import { FieldType } from 'src/app/core/models/entity/field-type';
import { ShowSpinner } from 'src/app/state/app.actions';
import { SubSink } from 'subsink/dist/subsink';
import { AdminMetadataState } from '../state/admin-metadata/admin-metadata.state';
import { GetFieldTypes, CreateMetadataField, UpdateMetadataField, GetMetadataList, RemoveMetadataField, GetActiveMetadataLists, GetMetadataModules, GetMediaMetadataFields, GetWorkPlanningMetadataFields, SortMetadataFields } from '../state/admin-metadata/admin-metadata.action';
import { ModalComponent } from 'src/app/shared/modal/modal.component';
import { Lookup } from 'src/app/core/models/entity/lookup';
import { MetadataFieldType } from 'src/app/core/enum/metadataFieldType';
import { Tab } from 'src/app/core/models/tab';
import { Router, ActivatedRoute } from '@angular/router';
import { Button } from 'src/app/core/models/button';
import { FieldsService } from 'src/app/core/services/business/fields/fields.service';
import { SortType, SortDirection } from 'src/app/core/enum/sort-enum';
import { MetadataDataType } from 'src/app/core/enum/metadata-data-type';
import { MetadataDataTypes, MetadataSearchTypes } from './admin-metadata-fields.data';
import { MetadataSearchType } from 'src/app/core/enum/metadata-search-type';

const MEDIA = 'media';
const WORK_PLANNING = 'workplanning';
const MEDIA_MODULE = 1;
const WORK_PLANNING_MODULE = 2;

@Component({
  selector: 'app-admin-metadata-fields',
  templateUrl: './admin-metadata-fields.component.html',
  styleUrls: ['./admin-metadata-fields.component.css']
})
export class AdminMetadataFieldsComponent extends ListComponent implements OnInit, OnDestroy {

  @ViewChild('listview') dialogList: any;
  @ViewChild('fieldDialog') fieldDialogList: DialogComponent;
  @ViewChild('confirmModal') confirmModal: ModalComponent;
  @ViewChild('confirmSaveModal') confirmSaveModal: ModalComponent;

  @Select(AdminMetadataState.getMediaMetadataFields) mediaMetadataFields$: Observable<MetadataField[]>;
  @Select(AdminMetadataState.getWorkPlanningMetadataFields) workPlanningMetadataFields$: Observable<MetadataField[]>;
  @Select(AdminMetadataState.getFieldTypes) fieldTypeList$: Observable<FieldType[]>;
  @Select(AdminMetadataState.getActiveMetadataLists) metadataLists$: Observable<MetadataList[]>;
  @Select(AdminMetadataState.getModules) modules$: Observable<Lookup[]>;

  private subs = new SubSink();
  tabs: Tab[] = [
    { link: '/admin/metadata-fields/media', name: 'Media', isActive: true },
    { link: '/admin/metadata-fields/workplanning', name: 'Work Planning', isNotPermitted: !this.tenantPermission.isPermitedWorkPlanning }
    //{ link: '/admin/metadata-fields/workplanning', name: 'Work Planning'}
  ];
  actions: GridColumnButton[] = [
    { type: 'button', text: 'Edit', visible: true, action: this.showFieldModal.bind(this) }
  ];

  confirmButtons: Button[] = [
    { text: 'YES', type: 'button', cssClass: 'button-round modal-yes-button-qa', action: this.saveField.bind(this) },
    { text: 'No', type: 'button', cssClass: 'button-no-outline modal-no-button-qa', action: this.closeConfirmDialog.bind(this) }
  ];

  columns: GridColumn[] = [
    {
      headerText: 'Name', field: 'name', useAsLink: true,
      action: this.userPermission.canEditMetadata ? this.showFieldModal.bind(this) : ''
    },
    {
      headerText: 'Label', field: 'label', useAsLink: true,
      action: this.userPermission.canEditMetadata ? this.showFieldModal.bind(this) : ''
    },
    { headerText: 'Control', field: 'controlType', filterType: 'checkbox' },
    { headerText: 'Type', field: 'typeDescription', filterType: 'checkbox' },
    { headerText: 'List', field: 'listName', ignoreFiltering: true },
    { headerText: 'Actions', type: 'buttons', buttons: this.actions, width: 120, visible: this.userPermission.canEditMetadata }
  ];
  gridButtons: Button[] = [
    {
      text: "SAVE CHANGES", type: 'button', action: this.saveSort.bind(this),
      cssClass: "button-round save-changes-button-qa"
    },
    {
      text: "Cancel", type: 'button', action: this.cancelSorting.bind(this),
      cssClass: "button-no-outline cancel-button-qa", override: true
    },
  ];
  listFields: Object = { text: 'itemDescription', value: 'itemValue' };
  metadataListFields: Object = { text: 'name', value: 'id' };
  fieldTypeData: FieldType[];
  typeFields: Object = { text: 'description', value: 'id' };
  isFieldTypeList: boolean;
  medataFieldsList: Object = { text: 'name', value: 'id' };
  dataTypesFields: Object = { text: 'text', value: 'value' };
  searchTypeFields: Object = { text: 'text', value: 'value' };
  fieldForm: FormGroup;
  currentMetadataField: MetadataField;
  componentActive: boolean;
  metadataFields: MetadataField[];
  data: MetadataList[];
  isEditing: boolean;
  showListControl: boolean;
  allMetadataList: MetadataList[];
  selectedMetadataFieldId: any;
  hideListName: boolean;
  showParentField: boolean;
  deleteMessage: string;
  addMetadataField: string;
  modules: Lookup[];
  previousFieldType: string;
  fieldTypes: FieldType[];
  currentRoute = 'media';
  currentModule = 1;
  enableGridButtons: boolean;
  defaultMetadataFields: any;
  currentMetadataFields: MetadataField[] = [];
  mediaFields: MetadataField[] = [];
  workPlanningFields: MetadataField[] = [];
  dataTypes = MetadataDataTypes;
  searchTypes = MetadataSearchTypes;
  currentDataType = 'string';
  currentSearchType: string = MetadataSearchType.None;
  modalTitle: string;
  isFilterableChecked: boolean;
  isSaving: boolean;
  isConfirmModal: boolean;

  constructor(protected store: Store, private fb: FormBuilder, public router: Router, private activatedRoute: ActivatedRoute,
    private fieldsService: FieldsService) {
    super(store);
    this.showLefNav();

    this.fieldForm = this.fb.group({
      name: ['', [Validators.required, Validators.pattern(/^[a-zA-Z_]\w*(\.[a-zA-Z_]\w*)*$/)]],
      label: ['', [Validators.required]],
      typeId: ['', [Validators.required]],
      dataType: [this.dataTypes[4].value, [Validators.required]],
      searchType: [this.searchTypes[0].value],
      isFilterable: [null]
    });
  }

  ngOnInit() {
    this.store.dispatch(new GetFieldTypes());
    this.store.dispatch(new GetActiveMetadataLists());
    this.store.dispatch(new GetMetadataModules());

    this.subs.add(
      this.modules$
        .subscribe(modules => {
          this.modules = modules;
        }),
      this.fieldTypeList$
        .subscribe(types => {
          this.fieldTypes = types;
        }),
      this.activatedRoute.params
        .subscribe(params => {
          this.displayMetadata(params.type);
        }),
      this.workPlanningMetadataFields$
        .subscribe(_fields => {
          let fields = [..._fields];
          let sortedFields = this.fieldsService.sort(fields, 'order', SortType.Number, SortDirection.Ascending);
          this.workPlanningFields = [...sortedFields];
          this.defaultMetadataFields = [...sortedFields];
          this.enableGridButtons = false;
        }),
      this.mediaMetadataFields$
        .subscribe(_fields => {
          let fields = [..._fields];
          let sortedFields = this.fieldsService.sort(fields, 'order', SortType.Number, SortDirection.Ascending);
          this.mediaFields = [...sortedFields];
          this.defaultMetadataFields = [...sortedFields];
          this.enableGridButtons = false;
        })
    );
  }

  displayMetadata(params: string) {
    if (params === MEDIA) {
      this.currentRoute = params;
      this.currentModule = MEDIA_MODULE;
      this.store.dispatch(new GetMediaMetadataFields());
    }
    else if (params === WORK_PLANNING) {
      this.currentRoute = params;
      this.currentModule = WORK_PLANNING_MODULE;
      this.store.dispatch(new GetWorkPlanningMetadataFields());
    }
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

  showConfirmDialog(data: any) {
    this.selectedMetadataFieldId = data.id;
    this.deleteMessage = `Are you sure you want to delete ${data.name}?`;
    this.confirmModal.show();
  }

  addField() {
    this.resetForm();
    this.fieldDialogList.show();
  }

  closeDialog() {
    this.resetForm();
    this.fieldDialogList.hide();
  }

  closeConfirmDialog() {
    this.isConfirmModal = false;
  }

  showFieldModal(field?: MetadataField) {
    this.isConfirmModal = false;
    if (field) {
      this.isEditing = true;
      this.modalTitle = "Edit Metadata Field";
      this.currentMetadataField = field;
      this.previousFieldType = field.type;
      this.currentSearchType = field.searchType || MetadataSearchType.None;
      this.fieldForm.setValue({
        name: field.name,
        label: field.label,
        typeId: field.typeId,
        dataType: field.dataType,
        searchType: this.currentSearchType,
        isFilterable: field.isFilterable || false,
      });
      this.fieldForm.get('isFilterable').disable();
      this.isFilterableChecked = field.isFilterable;
      if (field.type === MetadataFieldType.Select) {
        this.addListControl();
        this.setValue('listId', field.listId);
        if (field.listId) {
          this.store.dispatch(new GetMetadataList(field.listId));
        }
        this.showListControl = true;
        this.showParentField = false;
      } else if (field.type === MetadataFieldType.Nested) {
        this.addListControl();
        this.addParentFieldControl();
        this.setValue('listId', field.listId);
        this.setValue('parentId', field.parentId);
        if (field.listId) {
          this.store.dispatch(new GetMetadataList(field.listId));
        }
        this.showListControl = true;
        this.showParentField = true;
      } else {
        this.showListControl = false;
        this.showParentField = false;
      }
      this.fieldForm.reset(this.fieldForm.value);
      this.fieldForm.get('isFilterable').reset(field.isFilterable);
    } else {
      this.isEditing = false;
      this.resetForm();
      this.fieldForm.get('isFilterable').enable();
      this.isFilterableChecked = false;
      this.modalTitle = "Add Metadata Field";
      this.currentMetadataField = null;
      this.currentSearchType = MetadataSearchType.None;
    }
    this.fieldDialogList.show();
  }

  onFieldTypeChange(event: any) {
    const fieldType: FieldType = event.itemData;
    if (!fieldType) return;

    if (fieldType.type === MetadataFieldType.Select) {
      this.currentDataType = this.currentDataType === MetadataDataType.Date ? MetadataDataType.String : this.currentDataType;
      this.removeParentFieldControl();
      this.addListControl();
      this.showListControl = true;
      this.showParentField = false;
    } else if (fieldType.type === MetadataFieldType.Nested) {
      this.currentDataType = this.currentDataType === MetadataDataType.Date ? MetadataDataType.String : this.currentDataType;
      this.addListControl();
      this.addParentFieldControl();
      this.showListControl = true;
      this.showParentField = true;
    } else if (fieldType.type === MetadataFieldType.Date) {
      this.currentDataType = MetadataDataType.Date;
      this.removeParentFieldControl();
      this.removeListControl();
      this.showParentField = false;
      this.showListControl = false;
    } else {
      this.currentDataType = this.currentDataType === MetadataDataType.Date ? MetadataDataType.String : this.currentDataType;
      this.removeParentFieldControl();
      this.removeListControl();
      this.showParentField = false;
      this.showListControl = false;
    }
  }

  confirmSaveField() {
    this.isConfirmModal = true;
  }

  saveField() {
    this.isSaving = true;
    if (this.fieldForm.valid) {
      const field: MetadataField = { ...this.currentMetadataField, ...this.fieldForm.value };
      if (!this.currentMetadataField) {
        field.module = this.currentModule;
        field.searchType = field.isFilterable ? field.searchType : null;
        this.store.dispatch(new CreateMetadataField(field)).toPromise()
          .then(() => {
            if (this.currentModule === WORK_PLANNING_MODULE) this.store.dispatch(new GetWorkPlanningMetadataFields());
            else this.store.dispatch(new GetMediaMetadataFields());
            this.closeDialog();
            this.isSaving = false;
          }, () => {
            this.isSaving = false;
            this.closeDialog();
          });
      } else {
        const fieldType = this.fieldTypes.find(f => f.id === field.typeId);
        field.parentId = fieldType.type === MetadataFieldType.Nested ? field.parentId : null;
        field.listId = fieldType.type === MetadataFieldType.Nested || fieldType.type === MetadataFieldType.Select ? field.listId : null;
        field.searchType = field.isFilterable ? field.searchType : null;
        this.store.dispatch(new UpdateMetadataField(field.id, field)).toPromise()
          .then(() => {
            if (this.currentModule === WORK_PLANNING_MODULE) this.store.dispatch(new GetWorkPlanningMetadataFields());
            else this.store.dispatch(new GetMediaMetadataFields());
            this.closeDialog();
            this.isSaving = false;
          }, () => {
            this.isSaving = false;
            this.closeDialog();
          });
      }
    }
  }

  //#region Confirm Dialog

  deleteField() {
    this.store.dispatch(new ShowSpinner());
    this.store.dispatch(new RemoveMetadataField(this.selectedMetadataFieldId)).toPromise()
      .then(() => {
        if (this.currentModule === WORK_PLANNING_MODULE) this.store.dispatch(new GetWorkPlanningMetadataFields());
        else this.store.dispatch(new GetMediaMetadataFields());
      });
  }

  //#endregion

  switchTabs(tabLink: string) {
    this.router.navigate([tabLink]);
  }

  //#region Grid Sorting

  onRowDrop(args: any) {
    this.enableGridButtons = true;
  }

  saveSort() {
    this.enableGridButtons = false;
    if (this.currentModule === WORK_PLANNING_MODULE) {
      this.store.dispatch(new SortMetadataFields(this.workPlanningFields, false));
    } else {
      this.store.dispatch(new SortMetadataFields(this.mediaFields, true));
    }
  }

  cancelSorting() {
    if (this.currentModule === WORK_PLANNING_MODULE) {
      this.workPlanningFields = [...this.defaultMetadataFields];
    } else {
      this.mediaFields = [...this.defaultMetadataFields]
    }
    this.enableGridButtons = false;
  }

  //#endregion

  //#region Add & Remove Controls

  private resetForm() {
    this.removeListControl();
    this.removeParentFieldControl();
    this.showListControl = false;
    this.showParentField = false;
    this.fieldForm.reset();
  }

  private addListControl(value?: any) {
    this.addControl('listId');
  }

  private removeListControl() {
    this.removeControl('listId');
  }

  private addParentFieldControl() {
    this.addControl('parentId');
  }

  private removeParentFieldControl() {
    this.removeControl('parentId');
  }

  private addControl(name: string) {
    const control = this.fieldForm.get(name);
    if (!control) {
      const listControl = this.fb.control('', Validators.required);
      this.fieldForm.addControl(name, listControl);
    }
  }

  private setValue(name: string, value: any) {
    const control = this.fieldForm.get(name);
    if (control) control.setValue(value);
  }

  private removeControl(name: string) {
    const control = this.fieldForm.get(name);
    if (control) this.fieldForm.removeControl(name);
  }

  //#endregion
}

function minimumValidator(min = 1) {
  const validator: ValidatorFn = (formArray: FormArray) => {
    const totalSelected = formArray.controls
      // get a list of checkbox values (boolean)
      .map(control => control.value)
      // total up the number of checked checkboxes
      .reduce((prev, next) => next ? prev + next : prev, 0);

    // if the total is not greater than the minimum, return the error message
    return totalSelected >= min ? null : { required: true };
  };

  return validator;
}
