import { Component, OnInit } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { AppState } from 'src/app/state/app.state';
import { Observable } from 'rxjs';
import { Breadcrumb } from 'src/app/core/models/breadcrumb';
import { GridColumn } from 'src/app/core/models/grid.column';
import { Router } from '@angular/router';
import { SetBreadcrumbs, SetPreviousRoute } from 'src/app/state/app.actions';
import { WorkSet } from 'src/app/core/models/entity/work-set';
import { AdminWorkPlanningState } from '../state/admin-work-planning/admin-work-planning.state';
import { GetWorkSets } from '../state/admin-work-planning/admin-work-planning.actions';
import { ListComponent } from 'src/app/shared/list/list.component';
import { BaseComponent } from 'src/app/shared/base/base.component';

@Component({
  selector: 'app-admin-work-sets-list',
  templateUrl: './admin-work-sets-list.component.html',
  styleUrls: ['./admin-work-sets-list.component.css']
})
export class AdminWorkSetsListComponent extends ListComponent implements OnInit {

  // isClientAdmin: boolean;

  @Select(AdminWorkPlanningState.getWorkSets) workSets$: Observable<WorkSet[]>;
  @Select(AppState.getBreadcrumbs) breadcrumbs$: Observable<Breadcrumb[]>;

  columns: GridColumn[] = [
    { headerText: 'Name', field: 'name', useAsLink: true },
    { headerText: 'Modified On', field: 'modifiedOn', type: 'date', format: 'MMM dd, yyyy  hh:mm a' },
    { headerText: 'Modified By', field: 'modifiedBy' }
  ];

  constructor(protected store: Store, protected router: Router) {
    super(store);
    this.showLefNav();
    let crumbs: Breadcrumb[] = [{ name: 'Listings' }];
    this.store.dispatch(new SetBreadcrumbs(crumbs));
    this.setPageTitle("Admin Work Sets");
  }

  ngOnInit() {
    if (!this.userPermission.isClientAdmin) {
      this.router.navigate([`/unauthorize`]);
    }
    this.store.dispatch(new GetWorkSets());
  }

  createTemplate() {
    const route = this.router.url;
    this.store.dispatch(new SetPreviousRoute(route));
    this.router.navigate([`admin/work-sets/new/general-info`]);
  }

  editTemplate(data?: WorkSet) {
    const route = this.router.url;
    this.store.dispatch(new SetPreviousRoute(route));
    this.router.navigate([`admin/work-sets/${data.id}/general-info`]);
  }

  switchBreadCrumbs(breadcrumbLink: string) {
    this.router.navigate([breadcrumbLink]);
  }
}
