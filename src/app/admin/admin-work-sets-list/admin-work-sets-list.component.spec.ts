import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminWorkSetsListComponent } from './admin-work-sets-list.component';

describe('AdminWorkSetsListComponent', () => {
  let component: AdminWorkSetsListComponent;
  let fixture: ComponentFixture<AdminWorkSetsListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminWorkSetsListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminWorkSetsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
