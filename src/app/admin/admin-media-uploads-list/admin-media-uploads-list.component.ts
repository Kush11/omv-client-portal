import { Component, OnInit } from '@angular/core';
import { ListComponent } from 'src/app/shared/list/list.component';
import { Store } from '@ngxs/store';
import { Directory } from 'src/app/core/models/entity/directory';

@Component({
  selector: 'app-admin-media-uploads-list',
  templateUrl: './admin-media-uploads-list.component.html',
  styleUrls: ['./admin-media-uploads-list.component.css']
})
export class AdminMediaUploadsListComponent extends ListComponent implements OnInit {

  data: any[];
  tabView: string;
  total: number;
  directories: Directory[];
  folderPath: string;

  constructor(protected store: Store) {
    super(store);
    this.showLefNav();
    this.setPageTitle('Admin Media Uploads');
  }

  ngOnInit() { }
}

