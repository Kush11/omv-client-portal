import { Component, OnInit, ViewChild } from '@angular/core';
import { GridColumn } from "../../../core/models/grid.column";
import { Select, Store } from "@ngxs/store";
import { AppState } from "../../../state/app.state";
import { Observable } from "rxjs";
import { AdminMediaState } from '../../state/admin-media/admin-media.state';
import { ListComponent } from 'src/app/shared/list/list.component';
import { GetNewUploads, ApproveUploads, RejectUploads, CancelUploads } from '../../state/admin-media/admin-media.action';
import { UploadRequest } from 'src/app/core/models/entity/upload-request';
import { Router } from '@angular/router';
import { ToolBar } from 'src/app/core/models/toolbar-action';
import { SetPreviousRoute } from 'src/app/state/app.actions';
import { ModalComponent } from 'src/app/shared/modal/modal.component';

const REJECT_MESSAGE = 'Are you sure you want to reject the upload request(s)?';
const CANCEL_MESSAGE = 'Are you sure you want to cancel the upload request(s)?';

@Component({
  selector: 'app-admin-media-new-uploads',
  templateUrl: './admin-media-new-uploads.component.html',
  styleUrls: ['./admin-media-new-uploads.component.css']
})
export class AdminMediaNewUploadsComponent extends ListComponent implements OnInit {
  @Select(AppState.setDeviceWidth) deviceWidth$: Observable<number>;
  @Select(AdminMediaState.getNewUploads) newUploads$: Observable<UploadRequest[]>;

  @ViewChild('confirmModal') confirmModal: ModalComponent;

  deviceWidth: number;
  newUploads: UploadRequest[];
  columns: GridColumn[] = [
    { type: 'checkbox', headerText: 'Select All', width: '50', field: '' },
    { headerText: 'Source', field: 'source', useAsLink: true },
    { headerText: 'Destination', field: 'destination' },
    { headerText: 'Date', field: 'createdOn', type: 'date', format: 'MMM dd, yyyy  hh:mm a', width: 200 },
    { headerText: 'Size', field: 'sizeDisplay', width: 120 },
    { headerText: '# Files', field: 'files', width: 120 }
  ];
  toolbar: ToolBar[] = [
    { text: 'Approve', disabled: true },
    { text: 'Reject', disabled: true },
    // { text: 'Cancel', disabled: true },
  ];
  selectedRequests: UploadRequest[] = [];
  message: string;

  constructor(protected store: Store, protected router: Router) {
    super(store);
  }

  ngOnInit() {
    this.store.dispatch(new GetNewUploads());
  }

  onToolbarClick(index: number) {
    switch (index) {
      case 0:
        this.approve();
        break;
      case 1:
        this.message = REJECT_MESSAGE;
        this.confirmModal.show();
        break;
      case 2:
        this.message = CANCEL_MESSAGE;
        this.confirmModal.show();
        break;
    }
  }

  rowSelectedEvent(uploadRequests: UploadRequest[]) {
    uploadRequests.forEach(request => this.addUploadRequest(request));
    this.setToobarProperties();
  }

  rowDeselectedEvent(uploadRequests: UploadRequest[]) {
    uploadRequests.forEach(request => this.removeUploadRequest(request));
    this.setToobarProperties();
  }

  private addUploadRequest(uploadRequest: UploadRequest) {
    const selectedRequestIds = this.selectedRequests.map(x => x.id);
    if (!selectedRequestIds.includes(uploadRequest.id)) {
      this.selectedRequests = [
        ...this.selectedRequests,
        uploadRequest
      ];
    }
  }

  private removeUploadRequest(uploadRequest: UploadRequest) {
    const selectedRequestIds = this.selectedRequests.map(x => x.id);
    if (selectedRequestIds.includes(uploadRequest.id)) {
      this.selectedRequests = this.selectedRequests.filter(x => x.id !== uploadRequest.id);
    }
  }

  approve() {
    let uploadRequestIds: number[] = [];
    this.selectedRequests.forEach(r => uploadRequestIds.push(r.id));
    this.store.dispatch(new ApproveUploads(uploadRequestIds));
  }

  reject() {
    let uploadRequestIds: number[] = [];
    this.selectedRequests.forEach(r => uploadRequestIds.push(r.id));
    this.store.dispatch(new RejectUploads(uploadRequestIds));
  }

  cancel() {
    let uploadRequestIds: number[] = [];
    this.selectedRequests.forEach(r => uploadRequestIds.push(r.id));
    this.store.dispatch(new CancelUploads(uploadRequestIds));
  }

  editEvent(data?: UploadRequest) {
    if (data) {
      this.store.dispatch(new SetPreviousRoute(this.router.url));
      this.router.navigate([`/admin/media/uploads/${data.id}/details`]);
    }
  }

  //#region Confirm Modal

  confirmed() {
    if (this.message === REJECT_MESSAGE) {
      this.reject();
    } else if (this.message === CANCEL_MESSAGE) {
      this.cancel();
    }
  }

  //#endregion

  private setToobarProperties(status?: string) {
    this.toolbar.map(x => x.disabled = this.selectedRequests.length === 0);
    console.log(this.selectedRequests);
  }
}
