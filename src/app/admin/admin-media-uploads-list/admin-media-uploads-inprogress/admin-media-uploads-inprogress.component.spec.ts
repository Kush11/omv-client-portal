import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminMediaUploadsInprogressComponent } from './admin-media-uploads-inprogress.component';

describe('AdminMediaUploadsInprogressComponent', () => {
  let component: AdminMediaUploadsInprogressComponent;
  let fixture: ComponentFixture<AdminMediaUploadsInprogressComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminMediaUploadsInprogressComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminMediaUploadsInprogressComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
