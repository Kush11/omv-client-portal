import { GetUploadsInProgress, ApproveUploads, RejectUploads } from './../../state/admin-media/admin-media.action';
import { Component, OnInit, ViewChild } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { AppState } from 'src/app/state/app.state';
import { Observable } from 'rxjs';
import { AdminMediaState } from '../../state/admin-media/admin-media.state';
import { UploadRequest } from 'src/app/core/models/entity/upload-request';
import { ModalComponent } from 'src/app/shared/modal/modal.component';
import { GridColumn } from 'src/app/core/models/grid.column';
import { ToolBar } from 'src/app/core/models/toolbar-action';
import { Router } from '@angular/router';
import { ListComponent } from 'src/app/shared/list/list.component';
import { SetPreviousRoute } from 'src/app/state/app.actions';



@Component({
  selector: 'app-admin-media-uploads-inprogress',
  templateUrl: './admin-media-uploads-inprogress.component.html',
  styleUrls: ['./admin-media-uploads-inprogress.component.css']
})
export class AdminMediaUploadsInprogressComponent implements OnInit {

  @Select(AdminMediaState.getUploadsInProgress) uploadInProgressMedia$: Observable<UploadRequest[]>;

  columns: GridColumn[] = [
    { headerText: 'Status', width: '120', field: 'iconCss', type: 'icon', textAlign: 'Center' },
    { headerText: 'Source', field: 'source', useAsLink: true },
    { headerText: 'Destination', field: 'destination' },
    { headerText: 'Date', field: 'createdOn', format: 'MMM dd, yyyy  hh:mm a', type: 'date', width: 200 },
    { headerText: 'Size', field: 'sizeDisplay', width: 120 },
    { headerText: '# Files', field: 'files', width: 120 }
  ];
  toolbar: ToolBar[] = [
    { text: 'Processing', iconCssClass: 'c-icon-pending', visible: true }
  ];

  constructor(protected store: Store) {
  }

  ngOnInit() {
    this.store.dispatch(new GetUploadsInProgress());
  }

}
