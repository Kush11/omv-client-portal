import { Component, OnInit } from '@angular/core';
import { GridColumn } from '../../../core/models/grid.column';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { AdminMediaState } from '../../state/admin-media/admin-media.state';
import { UploadHistory } from '../../../core/models/entity/uploadhistory';
import { GetUploadsHistory } from '../../state/admin-media/admin-media.action';
import { ToolBar } from 'src/app/core/models/toolbar-action';
import { UploadRequest } from 'src/app/core/models/entity/upload-request';

@Component({
  selector: 'app-admin-media-uploads-history',
  templateUrl: './admin-media-uploads-history.component.html',
  styleUrls: ['./admin-media-uploads-history.component.css']
})
export class AdminMediaUploadsHistoryComponent implements OnInit {

  @Select(AdminMediaState.getUploadHistory) uploadHistoryMedia$: Observable<UploadRequest[]>;

  columns: GridColumn[] = [
    { headerText: 'Status', width: '120', field: 'iconCss', type: 'icon', textAlign: 'Center' },
    { headerText: 'ID', field: 'id', width: 120 },
    { headerText: 'Source', field: 'source', useAsLink: true },
    { headerText: 'Destination', field: 'destination' },
    { headerText: 'Created By', field: 'requesterName' },
    { headerText: 'Date', field: 'createdOn', format: 'MMM dd, yyyy  hh:mm a', type: 'date', width: 200 },
    { headerText: 'Size', field: 'sizeDisplay', width: 120 },
    { headerText: '# Files', field: 'files', width: 120 }
  ];
  toolbar: ToolBar[] = [
    // { text: 'Processing', iconCssClass: 'c-icon-pending', visible: true },
    { text: 'Completed', iconCssClass: 'c-icon-success', visible: true },
    { text: 'Rejected', iconCssClass: 'c-icon-warning', visible: true },
    { text: 'Completed With Errors', iconCssClass: 'c-icon-danger', visible: true }
  ];

  constructor(private store: Store) { }

  ngOnInit() {
    this.store.dispatch(new GetUploadsHistory());
  }
}
