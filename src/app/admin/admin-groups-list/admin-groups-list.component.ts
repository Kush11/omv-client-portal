import { GetActiveGroups, DisableGroups, EnableGroups, UpdateGroupsPermissions, GetDisabledGroups } from '../state/admin-groups/admin-groups.action';
import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { GridColumn } from "../../core/models/grid.column";
import { ListComponent } from "../../shared/list/list.component";
import { Router, ActivatedRoute } from '@angular/router';
import { Store, Select } from '@ngxs/store';
import { AdminGroupState } from '../state/admin-groups/admin-groups.state';
import { Group } from 'src/app/core/models/entity/group';
import { AdminGroupType } from 'src/app/core/enum/admin-user-type';
import { Permission } from 'src/app/core/enum/permission';
import { DialogComponent } from '@syncfusion/ej2-angular-popups';
import { AdminPermissionState } from '../state/admin-permissions/admin-permissions.state';
import { GetPermissions } from '../state/admin-permissions/admin-permissions.action';
import { ToolBar } from 'src/app/core/models/toolbar-action';
import { ShowSpinner, SetPreviousRoute } from 'src/app/state/app.actions';
import { SubSink } from 'subsink/dist/subsink';
import { Observable } from 'rxjs/internal/Observable';
import { ModalComponent } from 'src/app/shared/modal/modal.component';

const STATUS_ENABLE = 'Enable';
const STATUS_DISABLE = 'Disable';
const ACTIVE_TAB = 0;
const DISABLED_TAB = 1;

@Component({
  selector: 'app-admin-groups-list',
  templateUrl: './admin-groups-list.component.html',
  styleUrls: ['./admin-groups-list.component.css']
})
export class AdminGroupsListComponent extends ListComponent implements OnInit, OnDestroy {

  @Select(AdminGroupState.getActiveGroups) activeGroups$: Observable<Group[]>;
  @Select(AdminGroupState.getDisabledGroups) disabledGroups$: Observable<Group[]>;
  @Select(AdminPermissionState.getPermissions) permissions$: Observable<Permission[]>;
  @Select(AdminGroupState.getIsProcessComplete) isProcessComplete$: Observable<boolean>;
  @Select(AdminGroupState.getTotalGroups) totalGroups$: Observable<boolean>;

  @ViewChild('groupDialog') permissionsDialog: DialogComponent;
  @ViewChild('listviewpermissions') groupDialogList: any;
  @ViewChild('groupsList') groupsList: ListComponent;
  @ViewChild('permissionsModal') permissionsModal: ModalComponent;

  private subs = new SubSink();
  status: string;
  permissions: Permission[] = [];
  selectedGroups: Group[] = [];
  selectedPermissions: Permission[] = [];
  groups: Group[] = [];
  columns: GridColumn[] = [
    { type: "checkbox", headerText: "Select All", width: "50", field: "" },
    { headerText: "Name", field: "nameWithBadge", width: '200', useAsLink: true, action: this.editGroup.bind(this) },
    { headerText: "Description", field: "description" },
    { headerText: "Last Modified", field: "modifiedOn", width: 180, type: 'date', format: 'MM/dd/yyyy' },
    { headerText: "Modified By", field: "modifiedBy" },
    { headerText: "Members", field: "memberCount", width: 160, textAlign: 'center' }
  ];
  toolbar: ToolBar[] = [
    { text: '', disabled: this.selectedGroups.length < 1, action: this.changeStatus.bind(this) },
    { text: 'Assign To Permissions', disabled: this.selectedGroups.length < 1, action: this.assignToPermissions.bind(this) },
  ];
  ENABLE: string = "Enable";
  DISABLE: string = "Disable";
  field = { text: 'name', value: 'id' };
  currentTab = ACTIVE_TAB;
  selectedGroupIds: number[] = [];

  constructor(protected store: Store, private activatedRoute: ActivatedRoute, protected router: Router) {
    super(store);
    // this.Permission = permission.VIEW_GROUP;
    this.showLefNav();
    this.setPageTitle('Admin Groups');
  }

  ngOnInit() {
    this.store.dispatch(new GetPermissions());
    this.subs.add(
      this.activatedRoute.params
        .subscribe(params => {
          this.displayGroups(params.type);
        }),
      this.activeGroups$
        .subscribe(activeGroups => (this.groups = activeGroups)),
      this.disabledGroups$
        .subscribe(disabledGroups => (this.groups = disabledGroups)),
      this.isProcessComplete$
        .subscribe(isComplete => {
          if (isComplete) this.getGroups();
        })
    );
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

  displayGroups(param: string) {
    switch (param) {
      case AdminGroupType.Active:
        this.currentTab = ACTIVE_TAB;
        break;
      case AdminGroupType.Disabled:
        this.currentTab = DISABLED_TAB;
        break;
    }
    this.getGroups();
    this.setToobarProperties();
  }

  getGroups() {
    if (this.currentTab === ACTIVE_TAB) this.store.dispatch(new GetActiveGroups());
    else this.store.dispatch(new GetDisabledGroups());
  }

  rowSelectedEvent(groups: Group[]) {
    groups.forEach(group => this.addGroup(group));
    this.setToobarProperties();
  }

  rowDeselectedEvent(groups: Group[]) {
    groups.forEach(group => this.removeGroup(group));
    this.setToobarProperties();
  }

  private addGroup(group: Group) {
    const selectedGroupIds = this.selectedGroups.map(x => x.id);
    if (!selectedGroupIds.includes(group.id)) {
      this.selectedGroups = [
        ...this.selectedGroups,
        group
      ];
    }
  }

  private removeGroup(group: Group) {
    const selectedGroupIds = this.selectedGroups.map(x => x.id);
    if (selectedGroupIds.includes(group.id)) {
      this.selectedGroups = this.selectedGroups.filter(x => x.id !== group.id);
    }
  }

  changeStatus() {
    this.store.dispatch(new ShowSpinner());
    if (this.currentTab === ACTIVE_TAB) {
      this.store.dispatch(new DisableGroups(this.selectedGroups)).toPromise()
        .then(() => {
          this.selectedGroups = [];
          this.groupsList.clearSelection();
          this.setToobarProperties();
        });
    }
    else {
      this.store.dispatch(new EnableGroups(this.selectedGroups)).toPromise()
        .then(() => {
          this.selectedGroups = [];
          this.groupsList.clearSelection();
          this.setToobarProperties();
        });
    }
  }

  assignToPermissions() {
    this.permissionsModal.show();
  }

  modalRowSelected(permissions: Permission[]) {
    this.selectedPermissions = permissions;
  }

  editGroup(data?: Group) {
    let route = this.router.url;
    this.store.dispatch(new SetPreviousRoute(route));
    if (!data) {
      this.router.navigate([`/admin/groups/0/edit`]);
    } else {
      this.router.navigate([`/admin/groups/${data.id}/edit`]);
    }
  }

  savePermissions() {
    let permissionsIds: string[] = [];
    let groupsIds: number[] = [];
    this.selectedPermissions.forEach(p => permissionsIds.push(p.id));
    this.selectedGroups.forEach(g => groupsIds.push(g.id));

    this.store.dispatch(new UpdateGroupsPermissions(groupsIds, permissionsIds)).toPromise()
      .then(() => {
        this.selectedGroups = [];
        this.groupsList.clearSelection();
        this.setToobarProperties();
      });;
    this.clearPermissionsSelection();
  }

  clearPermissionsSelection() {
    this.permissionsModal.clearSelection();
  }

  private setToobarProperties() {
    this.selectedGroupIds = this.selectedGroups.map(x => x.id);
    this.toolbar.map((x, i) => {
      if (i === 0) x.text = this.currentTab === ACTIVE_TAB ? STATUS_DISABLE : STATUS_ENABLE;
      x.disabled = this.selectedGroups.length < 1;
    });
  }

  closeDialog() {
    this.permissionsDialog.hide();
  }
}
