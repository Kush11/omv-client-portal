import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { CanDeactivate } from '@angular/router';
import { AdminProcessingRuleEditComponent } from './admin-processing-rule-edit.component';

@Injectable({
  providedIn: 'root'
})
export class AdminProcessingRuleEditGuard implements CanDeactivate<AdminProcessingRuleEditComponent> {
  canDeactivate(component: AdminProcessingRuleEditComponent): Observable<boolean> | Promise<boolean> | boolean {
    if (component.ruleForm.touched && component.ruleForm.dirty) {
      const name = component.ruleForm.get('name').value || 'this form';
      return confirm (`If you navigate away, you will lose all changes to ${name}.`);
    }
    return true;
  }
}