import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminProcessingRuleEditComponent } from './admin-processing-rule-edit.component';

describe('AdminProcessingRuleEditComponent', () => {
  let component: AdminProcessingRuleEditComponent;
  let fixture: ComponentFixture<AdminProcessingRuleEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminProcessingRuleEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminProcessingRuleEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
