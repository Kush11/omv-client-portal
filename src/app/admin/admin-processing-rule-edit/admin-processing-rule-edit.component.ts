import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Store, Select } from '@ngxs/store';
import { Observable } from 'rxjs/internal/Observable';
import { BaseComponent } from 'src/app/shared/base/base.component';
import { AdminGroupState } from '../state/admin-groups/admin-groups.state';
import { Group } from 'src/app/core/models/entity/group';
import { Router, ActivatedRoute } from '@angular/router';
import { GetActiveGroups } from '../state/admin-groups/admin-groups.action';
import { AdminMediaState } from '../state/admin-media/admin-media.state';
import { Rule } from 'src/app/core/models/entity/rule';
import { ClearCurrentRule, GetRule, CreateRule, UpdateRule } from '../state/admin-media/admin-media.action';
import { SubSink } from 'subsink/dist/subsink';
import { AppState } from 'src/app/state/app.state';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

const BROWSE = 'Browse';
const CHANGE = 'Change';

@Component({
  selector: 'app-admin-processing-rule-edit',
  templateUrl: './admin-processing-rule-edit.component.html',
  styleUrls: ['./admin-processing-rule-edit.component.css']
})
export class AdminProcessingRuleEditComponent extends BaseComponent implements OnInit, OnDestroy {

  @Select(AdminGroupState.getActiveGroups) groups$: Observable<Group[]>;
  @Select(AdminMediaState.getCurrentRule) currentRule$: Observable<Rule>;
  @Select(AdminMediaState.getCurrentRuleId) currentRuleId$: Observable<number>;
  @Select(AppState.getPreviousRoute) previousRoute$: Observable<string>;

  @ViewChild('file') file;

  private subs = new SubSink();
  fileName: string;
  groupFields = { text: 'name', value: 'id' };
  previousRoute: string;
  ruleForm: FormGroup;
  ruleId: number;
  currentRule: Rule;
  isFileTouched: boolean;
  selectedFile: File;
  fileSelectionButtonText = BROWSE;
  isEdit: boolean;

  constructor(protected store: Store, private router: Router, private route: ActivatedRoute, private formBuilder: FormBuilder) {
    super(store);
    this.hideLeftNav();
    this.setPageTitle('Manage Processing Rule');
  }

  ngOnInit() {
    this.store.dispatch(new GetActiveGroups());
    this.ruleForm = this.formBuilder.group({
      name: ['', [Validators.required]],
      class: ['', [Validators.required]],
      file: ['', [Validators.required]],
      fileName: [''],
      approvalGroupId: ['', Validators.required]
    });

    this.subs.add(
      this.route.paramMap
        .subscribe(params => {
          this.ruleId = Number(params.get('id'));
          if (this.ruleId) {
            this.store.dispatch(new GetRule(this.ruleId));
          }
        }),
      this.currentRule$
        .subscribe(rule => {
          if (rule) {
            this.isEdit = true;
            this.ruleForm.patchValue({
              name: rule.name,
              class: rule.class,
              file: rule.file,
              approvalGroupId: rule.approvalGroupId
            });
            // this.ruleForm.removeControl('class');
            // this.ruleForm.removeControl('file');
            // this.ruleForm.markAsPristine();
          }
          this.currentRule = rule;
        }),
      this.previousRoute$
        .subscribe(route => {
          const default_route = "/admin/media/rules"; // in case a user gets to this page other than by routing from the previous page
          if (route.includes(default_route)) {
            this.previousRoute = route;
          } else {
            this.previousRoute = default_route;
          }
        })
    );
  }

  ngOnDestroy() {
    this.store.dispatch(new ClearCurrentRule());
    this.subs.unsubscribe();
  }

  onBack(route: string) {
    this.router.navigate([route]);
  }

  chooseFile() {
    this.file.nativeElement.click();
  }

  onFileChosen() {
    this.isFileTouched = true;
    const files: { [key: string]: File } = this.file.nativeElement.files;
    this.selectedFile = files[0] ? files[0] : this.selectedFile;
    if (this.selectedFile) {
      this.fileSelectionButtonText = CHANGE;

      let reader = new FileReader();
      reader.readAsDataURL(this.selectedFile);
      reader.onload = () => {
        this.ruleForm.patchValue({
          file: reader.result.toString().split(',')[1],
          fileName: this.selectedFile.name
        });
      };
    } else {
      this.ruleForm.patchValue({
        file: '',
        fileName: ''
      });
    }
  }

  saveRule() {
    if (this.ruleForm.valid && this.ruleForm.dirty) {
      const rule: Rule = { ...this.currentRule, ...this.ruleForm.value };
      if (!this.ruleId) {
        console.log('AdminProcessingRuleEdit saveRule rule: ', rule);

        this.subs.sink = this.store.dispatch(new CreateRule(rule))
          .subscribe(() => {
            this.subs.sink = this.currentRuleId$
              .subscribe(ruleId => {
                if (ruleId) {
                  this.ruleForm.reset();
                  //this.router.navigate([`/admin/media/rules/${ruleId}/edit`]);
                  this.router.navigate([`/admin/media/rules`]);
                }
              });
          });
      } else {
        this.subs.add(
          this.store.dispatch(new UpdateRule(this.ruleId, rule))
            .subscribe(() => {
              this.ruleForm.reset(this.ruleForm.value);
            this.router.navigate([`/admin/media/rules`]);
            })
        );
      }
    }
  }

  cancel() {
    this.ruleForm.reset();
    this.selectedFile = null;
    this.router.navigate([`/admin/media/rules`]);
  }
}
