import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminBulkUploaderDownloaderComponentComponent } from './admin-bulk-uploader-downloader-component.component';

describe('AdminBulkUploaderDownloaderComponentComponent', () => {
  let component: AdminBulkUploaderDownloaderComponentComponent;
  let fixture: ComponentFixture<AdminBulkUploaderDownloaderComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminBulkUploaderDownloaderComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminBulkUploaderDownloaderComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
