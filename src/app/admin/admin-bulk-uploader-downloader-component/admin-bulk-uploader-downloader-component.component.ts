import { Component, OnInit } from '@angular/core';
import { Store, Select } from '@ngxs/store';
import { AppState } from 'src/app/state/app.state';
import { Observable } from 'rxjs/internal/Observable';

@Component({
  selector: 'app-admin-bulk-uploader-downloader-component',
  templateUrl: './admin-bulk-uploader-downloader-component.component.html',
  styleUrls: ['./admin-bulk-uploader-downloader-component.component.css']
})
export class AdminBulkUploaderDownloaderComponentComponent implements OnInit {

  @Select(AppState.getBulkUploaderMacContainerUrl) bulkUploaderMacContainerUrl$: Observable<string>;
  @Select(AppState.getBulkUploaderWindowsContainerUrl) bulkUploaderWindowsContainerUrl$: Observable<string>;

  constructor() { }

  ngOnInit() { }

}
