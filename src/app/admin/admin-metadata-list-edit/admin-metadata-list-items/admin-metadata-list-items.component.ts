import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { GridColumn, GridColumnButton } from 'src/app/core/models/grid.column';
import { MetadataListItem } from 'src/app/core/models/entity/metadata-list-item';
import { MetadataList } from 'src/app/core/models/entity/metadata-list';
import { Select, Store } from '@ngxs/store';
import { ActivatedRoute } from '@angular/router';
import { ListComponent } from 'src/app/shared/list/list.component';
import { DialogComponent } from '@syncfusion/ej2-angular-popups';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ShowSpinner } from 'src/app/state/app.actions';
import { SubSink } from 'subsink/dist/subsink';
import { Observable } from 'rxjs/internal/Observable';
import { AdminMetadataState } from '../../state/admin-metadata/admin-metadata.state';
import {
  GetMetadataListItems, GetMetaDataDependentListItems, ClearListItem,
  ClearMetadataDependentListItems, CreateMetadataListItem, RemoveMetadataListItem, UpdateMetadataListItem
} from '../../state/admin-metadata/admin-metadata.action';
import { ModalComponent } from 'src/app/shared/modal/modal.component';
import { Button } from 'src/app/core/models/button';
import { FieldConfiguration } from 'src/app/shared/dynamic-components/field-setting';

@Component({
  selector: 'app-admin-metadata-list-items',
  templateUrl: './admin-metadata-list-items.component.html',
  styleUrls: ['./admin-metadata-list-items.component.css']
})
export class AdminMetadataListItemsComponent extends ListComponent implements OnInit, AfterViewInit {

  private subs = new SubSink();
  name = '';
  listId: number;
  metadataListItems: MetadataListItem[] = [];
  metadataListItemForm: FormGroup;
  metadataList = new MetadataListItem();
  actions: GridColumnButton[] = [
    {
      type: 'button', text: 'Remove', visible: true, action: this.showConfirmModal.bind(this),
      cssClass: 'button-no-outline button-grid-link remove-button-qa'
    },
    {
      type: 'button', text: 'Edit', visible: true, action: this.showListModal.bind(this),
      cssClass: 'button-no-outline button-grid-link edit-button-qa'
    }

  ];
  columns: GridColumn[] = [
    { type: '', headerText: 'Name', width: 80, field: 'description', useAsLink: true, action: this.showListModal.bind(this) },
    { type: 'buttons', headerText: 'Action', buttons: this.actions, width: 40 }
  ];
  columnsWithParentListItems: GridColumn[] = [
    { headerText: 'Name', width: 60, field: 'description' },
    { headerText: 'Parent', width: 60, field: 'parentName' },
    { type: 'buttons', headerText: 'Action', buttons: this.actions, width: 40 }
  ];
  footer: Button[] = [
    { text: 'Add List item', type: 'button', cssClass: 'button-default add-list-item-qa', action: this.addListMembers.bind(this) }
  ];

  metadataListControls: FieldConfiguration[] = [
    {
      type: 'input', label: 'List Name', inputType: 'text', name: 'name',
      required: true, validationMessage: 'Please enter a process name'
    },
    {
      type: 'input', label: 'Dependency', inputType: 'text', name: 'code',
      required: true, validationMessage: 'Please enter a process code'
    }
  ];

  listItemId: number;
  componentActive = true;
  parentFields: Object = { text: 'description', value: 'id' };
  showParentListItems: boolean;
  isLoadingDone: boolean;
  deleteMessage: string;
  currentPage: number;
  pageSize = 1;
  listModalTitle: string;
  isEdit: boolean;


  @Select(AdminMetadataState.getCurrentMetadataListId) currentListid$: Observable<MetadataList[]>;
  @Select(AdminMetadataState.getCurrentMetadataList) currentMetadataList$: Observable<MetadataList>;
  @Select(AdminMetadataState.getCurrentMetadataListItems) metadataListItems$: Observable<MetadataListItem[]>;
  @Select(AdminMetadataState.getCurrentDependentMetadataListItem) parentMetadataListItems$: Observable<MetadataListItem[]>;

  @ViewChild('listItemDialog') listItemDialog: DialogComponent;
  @ViewChild('confirmModal') confirmModal: ModalComponent;
  @ViewChild('listDetailModal') listDetailModal: ModalComponent;

  selectedMetadataListItemId: number;
  selectedMetadataListItem: any;

  constructor(protected store: Store, protected activatedRoute: ActivatedRoute, private formBuilder: FormBuilder, ) { super(store) }

  ngOnInit() {

    this.metadataListItemForm = this.formBuilder.group({
      id: null,
      description: ['', Validators.required],
      parentId: ['']
    });
    this.subs.add(
      this.activatedRoute.paramMap
        .subscribe(params => {
          this.listId = Number(params.get('id'));
          if (this.listId) {
            this.store.dispatch(new GetMetadataListItems(this.listId));
          }
        }),
      this.currentMetadataList$
        .subscribe(item => {
          if (item) {
            if (item.parentListId) {
              this.store.dispatch(new GetMetaDataDependentListItems(item.parentListId));
              this.showParentListItems = true;
              // this.columns = [
              //   { headerText: 'Name', width: '60', field: 'description' },
              //   { headerText: 'Parent', width: '60', field: 'parentName' },
              //   { type: 'buttons', headerText: 'Action', buttons: this.actions, width: 40 }
              // ];
            } else {
              this.showParentListItems = false;
              // this.columns = [
              //   { type: '', width: '80', headerText: 'Name', field: 'description' },
              //   { type: 'buttons', headerText: 'Action', buttons: this.actions, width: 40 }
              // ];
            }
            this.isLoadingDone = true;
          }
        }),
      this.parentMetadataListItems$
        .subscribe(parentListItems => {
          if (parentListItems) {

          }
        })
    );
  }

  ngAfterViewInit() {
    this.columns = this.columns;
    console.log('ngAfterViewInit: ', this.columns);
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
    this.store.dispatch(new ClearListItem());
    this.store.dispatch(new ClearMetadataDependentListItems());
  }

  addListMembers(item?) {
    this.isEdit = false;
    this.listItemDialog.show();
  }

  paginationEvent(pageNumber: number) {
    this.currentPage = pageNumber;
    this.store.dispatch(new GetMetadataListItems(this.listId));
    window.scrollTo(0, 0);
  }

  clearForm() {
    this.metadataListItemForm.reset();
  }

  addMembersClick() {
    if (this.metadataListItemForm.valid) {
      if (this.metadataListItemForm.dirty) {
        const initialMetadataListItem: MetadataListItem = {
          id: 0,
          sort: '',
          description: '',
          listId: this.listId,
          parentId: null
        }
        const metadataListItem: MetadataListItem = { ...initialMetadataListItem, ...this.metadataListItemForm.value };
        this.metadataListItemForm.reset();
        this.store.dispatch(new CreateMetadataListItem(this.listId, metadataListItem));
        this.closeDialog();
        this.clearForm();
      }
    }
    this.listItemDialog.hide();
  }

  showConfirmModal(data: MetadataListItem) {
    this.selectedMetadataListItemId = data.id;
    this.deleteMessage = `Are you sure you want to remove ${data.description}?`;
    this.confirmModal.show();
  }

  updateListItem() {
    if (this.metadataListItemForm.valid) {
      if (this.metadataListItemForm.dirty) {
        const updatedMetadataListItem: MetadataListItem = {
          id: 0,
          sort: '',
          description: '',
          listId: this.listId,
          parentId: null
        };
        const selectedMetadataListItemId = this.metadataListItemForm.value.id;
        const newMetadataListItem: MetadataListItem = { ...updatedMetadataListItem, ...this.metadataListItemForm.value };
        this.store.dispatch(new UpdateMetadataListItem(selectedMetadataListItemId, newMetadataListItem));
        this.clearForm();
        this.closeDialog();
      }
    }
  }

  showListModal(item: MetadataListItem) {
    this.isEdit = true;
    if (item) {
      this.selectedMetadataListItem = item;
      this.metadataListItemForm.patchValue({
        description: item.description,
        parentId: item.parentId,
        id: item.id
      });
    }
    this.listItemDialog.show();
  }

  closeDialog() {
    this.metadataListItemForm.reset();
    this.listItemDialog.hide();
  }

  //#region Confirm Dialog

  deleteItems() {
    this.store.dispatch(new ShowSpinner());
    this.store.dispatch(new RemoveMetadataListItem(this.listId, this.selectedMetadataListItemId));
  }

  //#endregion
}



