import { Component, OnInit } from '@angular/core';
import { EditComponent } from 'src/app/shared/edit/edit.component';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { Store, Select } from '@ngxs/store';
import { Tab } from 'src/app/core/models/tab';
import { MetadataList } from 'src/app/core/models/entity/metadata-list';
import { MetadataListStatus } from 'src/app/core/enum/metadata-list-status';
import { AppState } from 'src/app/state/app.state';
import { SubSink } from 'subsink/dist/subsink';
import { Observable } from 'rxjs/internal/Observable';
import { GetMetadataList, UpdateMetadataList, DisableMetadataList, EnableMetadataList } from '../state/admin-metadata/admin-metadata.action';
import { AdminMetadataState } from '../state/admin-metadata/admin-metadata.state';
import { Button } from 'src/app/core/models/button';

const METADATALIST_TAB = 0;
const DISABLE_LIST = 'Disable list';
const ENABLE_LIST = 'Enable list';

@Component({
  selector: 'app-admin-metadata-list-edit',
  templateUrl: './admin-metadata-list-edit.component.html',
  styleUrls: ['./admin-metadata-list-edit.component.css', './../../app.component.css']
})

export class AdminMetadataListEditComponent extends EditComponent implements OnInit {

  @Select(AdminMetadataState.getCurrentMetadataList) currentMetadataList$: Observable<MetadataList>;
  @Select(AppState.getPreviousRoute) previousRoute$: Observable<string>;

  private subs = new SubSink();
  metadataItemTabs: Tab[] = [
    { link: METADATALIST_TAB, name: 'List Items', isActive: true }
  ];
  buttons: Button[] = [
    { text: "Update List", type: 'submit', cssClass: "button-default update-metadata-list-qa" },
    { text: "Disable List", type: 'button', cssClass: "button-default enable-disable-metadata-list-qa" },
  ];
  metadataListForm: FormGroup;
  metadataList = new MetadataList();
  metadataId: number;
  metaDataActionText = DISABLE_LIST;
  createMetaDataButtonText: string;
  metadataStatus = MetadataListStatus;
  previousRoute: string;
  parentListName: string;
  showParentField: boolean;

  constructor(protected store: Store, protected router: Router, private fb: FormBuilder, private activatedRoute: ActivatedRoute) {
    super(store, router)
    this.hideLeftNav();
    this.metadataListForm = this.fb.group({
      id: '',
      name: ['', [Validators.required]]
    });
  }

  ngOnInit() {
    this.subs.add(
      this.activatedRoute.paramMap
        .subscribe(params => {
          this.metadataId = Number(params.get('id'));
          this.store.dispatch(new GetMetadataList(this.metadataId));
        }),
      this.currentMetadataList$
        .subscribe(metadataList => {
          if (metadataList) { // Existing 
            this.metadataListForm.setValue({
              id: metadataList.id,
              name: metadataList.name
            });
            this.metaDataActionText = metadataList.status === MetadataListStatus.Active ? DISABLE_LIST : ENABLE_LIST;
            this.metadataList = metadataList;
            this.showParentField = Boolean(metadataList.parentListId);
            this.parentListName = metadataList.parentListName;
            this.setButtonsProps();
          }
        }),
      this.previousRoute$
        .subscribe(route => {
          const default_route = "/admin/metadata-list"; // in case a user gets to this page other than by routing from the previous page
          if (route.includes(default_route)) {
            this.previousRoute = route;
          } else {
            this.previousRoute = default_route;
          }
        }),
      this.metadataListForm.valueChanges
        .subscribe(() => this.buttons[0].disabled = !this.metadataListForm.valid || !this.metadataListForm.dirty)
    );
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

  buttonsClick(index: number) {
    if (index === 1) {
      this.changeStatus();
    }
    this.setButtonsProps();
  }

  onBack(route: string) {
    this.router.navigate([route]);
  }

  save() {
    if (this.metadataListForm.valid) {
      if (this.metadataListForm.dirty) {
        const metadataList: MetadataList = { ...this.metadataList, ...this.metadataListForm.value };
        if (this.metadataId) {
          this.subs.sink = this.store.dispatch(new UpdateMetadataList(this.metadataId, metadataList))
            .subscribe(() => {
              this.metadataListForm.reset(this.metadataListForm.value);
              this.setButtonsProps();
            });
        }
      }
    }
  }

  changeStatus() {
    const statusButton = this.buttons[1];
    statusButton.disabled = true;
    if (statusButton.text === DISABLE_LIST) {
      this.subs.sink = this.store.dispatch(new DisableMetadataList(this.metadataList))
        .subscribe(() => {
          statusButton.text = ENABLE_LIST;
          statusButton.disabled = false;
        }, () => statusButton.disabled = false);
    }
    else {
      this.subs.sink = this.store.dispatch(new EnableMetadataList(this.metadataList))
        .subscribe(() => {
          statusButton.text = DISABLE_LIST;
          statusButton.disabled = false;
        }, () => statusButton.disabled = false);
    }
  }

  private setButtonsProps() {
    this.buttons.map((button, index) => {
      if (index === 0) { // Update List
        button.disabled = !this.metadataListForm.valid || !this.metadataListForm.dirty;
      } else { // Enable / Disable List
        button.text = this.metadataList.status === MetadataListStatus.Active ? DISABLE_LIST : ENABLE_LIST;
        button.hidden = !this.metadataId ? true : false;
      }
    });
  }
}



