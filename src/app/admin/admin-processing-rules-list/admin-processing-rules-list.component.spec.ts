import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminProcessingRulesListComponent } from './admin-processing-rules-list.component';

describe('AdminProcessingRulesListComponent', () => {
  let component: AdminProcessingRulesListComponent;
  let fixture: ComponentFixture<AdminProcessingRulesListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminProcessingRulesListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminProcessingRulesListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
