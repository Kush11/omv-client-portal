import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { Store, Select } from '@ngxs/store';
import { Observable } from 'rxjs/internal/Observable';
import { SubSink } from 'subsink/dist/subsink';
import { GridColumn, GridColumnButton } from 'src/app/core/models/grid.column';
import { AdminMediaState } from '../state/admin-media/admin-media.state';
import { Rule } from 'src/app/core/models/entity/rule';
import { SetPreviousRoute } from 'src/app/state/app.actions';
import { GetRules, DeleteRule } from '../state/admin-media/admin-media.action';
import { ListComponent } from 'src/app/shared/list/list.component';
import { ModalComponent } from 'src/app/shared/modal/modal.component';

@Component({
  selector: 'app-admin-processing-rules-list',
  templateUrl: './admin-processing-rules-list.component.html',
  styleUrls: ['./admin-processing-rules-list.component.css']
})
export class AdminProcessingRulesListComponent extends ListComponent implements OnInit, OnDestroy {

  @Select(AdminMediaState.getRules) rules$: Observable<Rule[]>;
  @ViewChild('confirmModal') confirmModal: ModalComponent;

  private subs = new SubSink();
  actions: GridColumnButton[] = [
    { type: 'button', text: 'Remove', visible: true, action: this.showConfirmDialog.bind(this), cssClass: 'button-no-outline button-grid-link remove-button-qa' },
    { type: 'button', text: 'Edit', visible: true, action: this.editRule.bind(this), cssClass: 'button-no-outline button-grid-link edit-button-qa' }
  ];
  columns: GridColumn[] = [
    { headerText: 'Name', field: 'name', useAsLink: true, action: this.editRule.bind(this) },
    { headerText: 'Approval Group', field: 'approvalGroupName' },
    { headerText: 'Last Modified', field: 'modifiedOn', type: 'date' },
    { headerText: 'Actions', type: 'buttons', buttons: this.actions, width: 200 }
  ];
  deleteMessage: string;
  currentRule: Rule;

  constructor(protected store: Store, protected router: Router) {
    super(store);
    this.showLefNav();
  }

  ngOnInit() {
    this.store.dispatch(new GetRules());
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

  addNewRule() {
    this.store.dispatch(new SetPreviousRoute(this.router.url));
    this.router.navigate([`/admin/media/rules/0/edit`]);
  }

  editRule(rule?: Rule) {
    this.store.dispatch(new SetPreviousRoute(this.router.url));
    this.router.navigate([`/admin/media/rules/${rule.id}/edit`]);
  }

  //#region Confirm Delete Dialog

  showConfirmDialog(rule: Rule) {
    this.currentRule = rule;
    this.deleteMessage = `Are you sure you want to delete ${rule.name}?`;
    this.confirmModal.show();
  }

  deleteRule() {
    this.store.dispatch(new DeleteRule(this.currentRule.id));
  }

  //#endregion
}
