import { AdminGroupState } from './state/admin-groups/admin-groups.state';
import { AdminGroupEditComponent } from './admin-group-edit/admin-group-edit.component';

import { AdminDashboardComponent } from './admin-dashboard/admin-dashboard.component';
import { AdminUsersListComponent } from './admin-users-list/admin-users-list.component';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';

import { SharedModule } from '../shared/shared.module';
import { AdminRoutingModule } from './admin.routing.module';
import { environment } from 'src/environments/environment';
import { AdminGroupsListComponent } from "./admin-groups-list/admin-groups-list.component";
import { AdminUserState } from './state/admin-users/admin-users.state';

/* NgXS */
import { NgxsModule } from '@ngxs/store';

import { AdminUserEditComponent } from './admin-user-edit/admin-user-edit.component';
import { GroupsWebDataService } from '../core/services/data/groups/groups.web.data.service';
import { AdminUsersTabsComponent } from './admin-users-list/admin-users-tabs/admin-users-tabs.component';
import { AdminGroupsTabsComponent } from './admin-groups-list/admin-groups-tabs/admin-groups-tabs.component';
import { PermissionsDataService } from '../core/services/data/permissions/permissions.data.service';
import { PermissionsMockService } from '../core/services/data/permissions/permissions.mock.service';
import { PermissionsWebService } from '../core/services/data/permissions/permissions.web.data.service';
import { AdminUserGroupsComponent } from './admin-user-edit/admin-user-groups/admin-user-groups.component';
import { AdminGroupPermissionsComponent } from './admin-group-edit/admin-group-permissions/admin-group-permissions.component';
import { AdminGroupMembersComponent } from './admin-group-edit/admin-group-members/admin-group-members.component';
import { AdminGroupMediaAccessComponent } from './admin-group-edit/admin-group-media-access/admin-group-media-access.component';
import { ReactiveFormsModule } from '@angular/forms';
import { AdminPermissionState } from './state/admin-permissions/admin-permissions.state';
import { AdminMediaUploadsListComponent } from './admin-media-uploads-list/admin-media-uploads-list.component';
import { AdminMediaUploadsTabsComponent } from './admin-media-uploads-list/admin-media-uploads-tabs/admin-media-uploads-tabs.component';
import { AdminMediaState } from './state/admin-media/admin-media.state';
import { AdminMetadataFieldsComponent } from './admin-metadata-fields/admin-metadata-fields.component';
import { AdminMediaNewUploadsComponent } from "./admin-media-uploads-list/admin-media-new-uploads/admin-media-new-uploads.component";
import { AdminMediaUploadsHistoryComponent } from "./admin-media-uploads-list/admin-media-uploads-history/admin-media-uploads-history.component";
import { AdminMetadataListComponent } from "./admin-metadata-list/admin-metadata-list.component";
import { AdminMediaUploadDetailsComponent } from './admin-media-upload-details/admin-media-upload-details.component';
import { CheckBoxModule } from '@syncfusion/ej2-angular-buttons';
import { SliderModule } from '@syncfusion/ej2-angular-inputs';
import { DropDownListModule } from '@syncfusion/ej2-angular-dropdowns';
import { MultiSelectModule } from '@syncfusion/ej2-angular-dropdowns';
import { TreeViewModule } from '@syncfusion/ej2-angular-navigations';
import { ListViewModule } from '@syncfusion/ej2-angular-lists';
import { AdminMetadataListTabComponent } from './admin-metadata-list/admin-metadata-list-tab/admin-metadata-list-tab.component';
import { AdminMetadataListEditComponent } from './admin-metadata-list-edit/admin-metadata-list-edit.component';
import { AdminMetadataListItemsComponent } from './admin-metadata-list-edit/admin-metadata-list-items/admin-metadata-list-items.component';
import { AdminFolderStructureComponent } from './admin-folder-structure/admin-folder-structure.component';
import { ContextMenuModule } from '@syncfusion/ej2-angular-navigations';
import { AdminFolderMetadataComponent } from './admin-folder-metadata/admin-folder-metadata.component';
import { MetadataFieldsDataService } from '../core/services/data/metadata-fields/metadata-fields.data.service';
import { MetadataFieldsWebDataService } from '../core/services/data/metadata-fields/metadata-fields.web.data.service';
import { MetadataFieldsMockDataService } from '../core/services/data/metadata-fields/metadata-fields.mock.service';
import { DirectoryDataService } from '../core/services/data/directory/directory.data.service';
import { DirectoryWebDataService } from '../core/services/data/directory/directory.web.data.service';
import { DirectoryMockDataService } from '../core/services/data/directory/directory.mock.data.service';
import { UploadRequestsDataService } from '../core/services/data/upload-requests/upload-requests.data.service';
import { UploadRequestsMockDataService } from '../core/services/data/upload-requests/upload-requests.mock.data.service';
import { UploadRequestsWebDataService } from '../core/services/data/upload-requests/upload-requests.web.data.service';
import { MetadataListDataService } from '../core/services/data/metadata-list/metadata-list.data.service';
import { MetadataListMockDataService } from '../core/services/data/metadata-list/metadata-list.mock.data.service';
import { MetadataListWebDataService } from '../core/services/data/metadata-list/metadata-list.web.data.service';
import { MetadataListItemsDataService } from '../core/services/data/metadata-list-items/metadata-list-items.data.service';
import { MetadataListItemsWebDataService } from '../core/services/data/metadata-list-items/metadata-list-items.web.data.service';
import { FieldTypesDataService } from '../core/services/data/field-types/field-types.data.service';
import { FieldTypesMockDataService } from '../core/services/data/field-types/field-types.mock.data.service';
import { FieldTypesWebDataService } from '../core/services/data/field-types/field-types.web.data.service';
import { MetadataListItemsMockDataService } from '../core/services/data/metadata-list-items/metadata-list-items.mock.data.service';
import { GroupsDataService } from '../core/services/data/groups/groups.data.service';
import { GroupsMockDataService } from '../core/services/data/groups/groups.mock.data.service';
import { AdminProcessingRulesListComponent } from './admin-processing-rules-list/admin-processing-rules-list.component';
import { AdminProcessingRuleEditComponent } from './admin-processing-rule-edit/admin-processing-rule-edit.component';
import { RulesDataService } from '../core/services/data/rules/rules.data.service';
import { RulesWebDataService } from '../core/services/data/rules/rules.web.data.service';
import { RulesMockDataService } from '../core/services/data/rules/rules.mock.data.service';
import { EmailTemplatesDataService } from '../core/services/data/email-templates/email-templates.data.service';
import { EmailTemplatesMockDataService } from '../core/services/data/email-templates/email-templates.mock.service';
import { EmailTemplatesWebDataService } from '../core/services/data/email-templates/email-templates.web.data.service';
import { AdminEmailTemplatesListComponent } from './admin-email-templates-list/admin-email-templates-list.component';
import { AdminEmailTemplateEditComponent } from './admin-email-template-edit/admin-email-template-edit.component';
import { AdminEmailTemplatesState } from './state/admin-email-templates/admin-email-templates.state';
import { AdminWorkTemplatesListComponent } from './admin-work-templates-list/admin-work-templates-list.component';
import { AdminWorkTemplateEditComponent } from './admin-work-template-edit/admin-work-template-edit.component';
import { AdminWorkPlanningState } from './state/admin-work-planning/admin-work-planning.state';
import { WorkTemplatesDataService } from '../core/services/data/work-templates/work-templates.data.service';
import { WorkTemplatesMockDataService } from '../core/services/data/work-templates/work-templates.mock.data.service';
import { WorkTemplatesWebDataService } from '../core/services/data/work-templates/work-templates.web.data.service';
import { AdminWorkTemplateGeneralInfoComponent } from './admin-work-template-edit/admin-work-template-general-info/admin-work-template-general-info.component';
import { AdminWorkTemplateFieldsComponent } from './admin-work-template-edit/admin-work-template-fields/admin-work-template-fields.component';
import { AdminWorkTemplateProcessComponent } from './admin-work-template-edit/admin-work-template-process/admin-work-template-process.component';
import { AdminMetadataState } from './state/admin-metadata/admin-metadata.state';
import { AdminWorkTemplateStepsComponent } from './admin-work-template-edit/admin-work-template-steps/admin-work-template-steps.component';
import { AdminWorkTemplateFormsComponent } from './admin-work-template-edit/admin-work-template-forms/admin-work-template-forms.component';
import { AdminWorkTemplateFindingsComponent } from './admin-work-template-edit/admin-work-template-findings/admin-work-template-findings.component';
import { AdminWorkTemplateVersionsComponent } from './admin-work-template-edit/admin-work-template-versions/admin-work-template-versions.component';
import { AdminWorkTemplateHistoryComponent } from './admin-work-template-edit/admin-work-template-history/admin-work-template-history.component';
import { GroupsService } from '../core/services/business/groups/groups.service';
import { PermissionsService } from '../core/services/business/permissions/permissions.service';
import { MetadataFieldsService } from '../core/services/business/metadata-fields/metadata-fields.service';
import { MetadataListService } from '../core/services/business/metadata-list/metadata-list.service';
import { MetadataListItemsService } from '../core/services/business/metadata-list-items/metadata-list-items.service';
import { FieldTypesService } from '../core/services/business/field-types/field-types.service';
import { DirectoryService } from '../core/services/business/directory/directory.service';
import { UploadRequestsService } from '../core/services/business/upload-requests/upload-requests.service';
import { RulesService } from '../core/services/business/rules/rules.service';
import { EmailTemplatesService } from '../core/services/business/email-templates/email-templates.service';
import { WorkTemplatesService } from '../core/services/business/work-templates/work-templates.service';
import { AdminBulkUploaderDownloaderComponentComponent } from './admin-bulk-uploader-downloader-component/admin-bulk-uploader-downloader-component.component';
import { ToolbarService, LinkService, ImageService, HtmlEditorService, RichTextEditorModule, QuickToolbarService } from '@syncfusion/ej2-angular-richtexteditor';
import { AdminWorkSetsListComponent } from './admin-work-sets-list/admin-work-sets-list.component';
import { AdminWorkSetEditComponent } from './admin-work-set-edit/admin-work-set-edit.component';
import { AdminWorkSetGeneralInfoComponent } from './admin-work-set-edit/admin-work-set-general-info/admin-work-set-general-info.component';
import { AdminWorkSetScheduleComponent } from './admin-work-set-edit/admin-work-set-schedule/admin-work-set-schedule.component';
import { AdminWorkSetWorkItemsComponent } from './admin-work-set-edit/admin-work-set-work-items/admin-work-set-work-items.component';
import { AdminWorkSetHistoryComponent } from './admin-work-set-edit/admin-work-set-history/admin-work-set-history.component';
import { WorkSetsService } from '../core/services/business/work-sets/work-sets.service';
import { WorkSetsDataService } from '../core/services/data/work-sets/work-sets.data.service';
import { WorkSetsMockDataService } from '../core/services/data/work-sets/work-sets.mock.data.service';
import { WorkSetsWebDataService } from '../core/services/data/work-sets/work-sets.web.data.service';
import { DatePickerModule } from '@syncfusion/ej2-angular-calendars';
import { AdminFolderState } from './state/admin-folders/admin-folder-state';
import { LookupService } from '../core/services/business/lookup/lookup.service';
import { LookupDataService } from '../core/services/data/lookup/lookup.data.service';
import { LookupMockDataService } from '../core/services/data/lookup/lookup.mock.data.service';
import { LookupWebDataService } from '../core/services/data/lookup/lookup.web.data.service';
import { AdminWorkSetCyclesComponent } from './admin-work-set-edit/admin-work-set-cycles/admin-work-set-cycles.component';
import { DialogModule } from '@syncfusion/ej2-angular-popups';
import { AdminMediaUploadsInprogressComponent } from './admin-media-uploads-list/admin-media-uploads-inprogress/admin-media-uploads-inprogress.component';

@NgModule({
  declarations: [
    AdminUsersListComponent,
    AdminUserEditComponent,
    AdminUsersTabsComponent,
    AdminGroupsListComponent,
    AdminGroupEditComponent,
    AdminGroupsTabsComponent,
    AdminDashboardComponent,
    AdminGroupsTabsComponent,
    AdminUserGroupsComponent,
    AdminGroupPermissionsComponent,
    AdminGroupMembersComponent,
    AdminGroupMediaAccessComponent,
    AdminMediaUploadsListComponent,
    AdminMediaUploadsTabsComponent,
    AdminMetadataListComponent,
    AdminMetadataFieldsComponent,
    AdminMediaNewUploadsComponent,
    AdminMediaUploadsHistoryComponent,
    AdminMetadataListComponent,
    AdminMetadataFieldsComponent,
    AdminMetadataListTabComponent,
    AdminMetadataListEditComponent,
    AdminMetadataListItemsComponent,
    AdminMediaUploadDetailsComponent,
    AdminMediaUploadsHistoryComponent,
    AdminMetadataListComponent,
    AdminMediaUploadDetailsComponent,
    AdminMetadataFieldsComponent,
    AdminFolderStructureComponent,
    AdminFolderMetadataComponent,
    AdminProcessingRulesListComponent,
    AdminProcessingRuleEditComponent,
    AdminEmailTemplatesListComponent,
    AdminEmailTemplateEditComponent,
    AdminWorkTemplatesListComponent,
    AdminWorkTemplateEditComponent,
    AdminWorkTemplateGeneralInfoComponent,
    AdminWorkTemplateFieldsComponent,
    AdminWorkTemplateProcessComponent,
    AdminWorkTemplateStepsComponent,
    AdminWorkTemplateFormsComponent,
    AdminWorkTemplateFindingsComponent,
    AdminWorkTemplateVersionsComponent,
    AdminWorkTemplateHistoryComponent,
    AdminBulkUploaderDownloaderComponentComponent,
    AdminWorkSetsListComponent,
    AdminWorkSetEditComponent,
    AdminWorkSetGeneralInfoComponent,
    AdminWorkSetScheduleComponent,
    AdminWorkSetWorkItemsComponent,
    AdminWorkSetHistoryComponent,
    AdminWorkSetCyclesComponent,
    AdminMediaUploadsInprogressComponent
  ],
  imports: [
    SharedModule,
    AdminRoutingModule,
    DialogModule,
    SliderModule,
    DropDownListModule,
    MultiSelectModule,
    ReactiveFormsModule,
    TreeViewModule,
    ListViewModule,
    RichTextEditorModule,
    ContextMenuModule,
    CheckBoxModule,
    DatePickerModule,
    NgxsModule.forFeature([
      AdminUserState,
      AdminGroupState,
      AdminPermissionState,
      AdminMediaState,
      AdminEmailTemplatesState,
      AdminWorkPlanningState,
      AdminMetadataState,
      AdminFolderState
    ])
  ],
  providers: [
    GroupsService,
    { provide: GroupsDataService, useClass: environment.useMocks ? GroupsMockDataService : GroupsWebDataService },
    PermissionsService,
    { provide: PermissionsDataService, useClass: environment.useMocks ? PermissionsMockService : PermissionsWebService },
    MetadataFieldsService,
    { provide: MetadataFieldsDataService, useClass: environment.useMocks ? MetadataFieldsMockDataService : MetadataFieldsWebDataService },
    MetadataListService,
    { provide: MetadataListDataService, useClass: environment.useMocks ? MetadataListMockDataService : MetadataListWebDataService },
    MetadataListItemsService,
    { provide: MetadataListItemsDataService, useClass: environment.useMocks ? MetadataListItemsMockDataService : MetadataListItemsWebDataService },
    FieldTypesService,
    { provide: FieldTypesDataService, useClass: environment.useMocks ? FieldTypesMockDataService : FieldTypesWebDataService },
    DirectoryService,
    { provide: DirectoryDataService, useClass: environment.useMocks ? DirectoryMockDataService : DirectoryWebDataService },
    UploadRequestsService,
    { provide: UploadRequestsDataService, useClass: environment.useMocks ? UploadRequestsMockDataService : UploadRequestsWebDataService },
    RulesService,
    { provide: RulesDataService, useClass: environment.useMocks ? RulesMockDataService : RulesWebDataService },
    EmailTemplatesService,
    { provide: EmailTemplatesDataService, useClass: environment.useMocks ? EmailTemplatesMockDataService : EmailTemplatesWebDataService },
    WorkSetsService,
    { provide: WorkSetsDataService, useClass: environment.useMocks ? WorkSetsMockDataService : WorkSetsWebDataService },
    WorkTemplatesService,
    { provide: WorkTemplatesDataService, useClass: environment.useMocks ? WorkTemplatesMockDataService : WorkTemplatesWebDataService },
    LookupService,
    { provide: LookupDataService, useClass: environment.useMocks ? LookupMockDataService : LookupWebDataService },
    ToolbarService, LinkService, ImageService, HtmlEditorService, QuickToolbarService
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA]
})
export class AdminModule { }
