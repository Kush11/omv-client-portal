import { Group } from '../../../core/models/entity/group';

export class GetActiveGroups {
  static readonly type = '[Admin Groups] GetActiveGroups';

  constructor(public pageNumber?: number, public pageSize?: number) { }
}

export class GetDisabledGroups {
  static readonly type = '[Admin Groups] GetDisabledGroups';

  constructor(public pageNumber?: number, public pageSize?: number) { }
}

export class GetGroup {
  static readonly type = '[Admin Groups] GetGroup';

  constructor(public id: number) { }
}

export class ClearGroup {
  static readonly type = '[Admin Groups] ClearGroup';
}

export class CreateGroup {
  static readonly type = '[Admin Groups] CreateGroup';

  constructor(public payload: Group) { }
}

export class UpdateGroup {
  static readonly type = '[Admin Groups] UpdateGroup';

  constructor(public id: number, public payload: Group) { }
}

export class DisableGroup {
  static readonly type = '[Admin Groups] DisableGroup';

  constructor(public group: Group) { }
}

export class EnableGroup {
  static readonly type = '[Admin Groups] EnableGroup';

  constructor(public group: Group) { }
}

export class DisableGroups {
  static readonly type = '[Admin Groups] DisableGroups';

  constructor(public groups: Group[]) { }
}

export class EnableGroups {
  static readonly type = '[Admin Groups] EnableGroups';

  constructor(public groups: Group[]) { }
}

export class SetCurrentGroup {
  static readonly type = '[Admin Groups] SetCurrentGroup';

  constructor(public payload: Group) { }
}

export class UpdateGroupsPermissions {
  static readonly type = '[Admin Groups] UpdateGroupsPermissions';

  constructor(public groupsIds: number[], public permissionIds: string[]) { }
}

export class SetCurrentGroupId {
  static readonly type = '[Admin Groups] SetCurrentGroupId';

  constructor(public id: number) { }
}

export class SetSelectedGroupIds {
  static readonly type = '[Admin Groups] SetSelectedGroupIds';

  constructor(public id: number[]) { }
}

export class ClearMembers {
  static readonly type = '[Admin Groups] ClearMembers';
}

export class GetGroupPermissions {
  static readonly type = '[Admin Groups] GetGroupPermissions';

  constructor(public groupId: number) { }
}

export class UpdateGroupPermissions {
  static readonly type = '[Admin Groups] UpdateGroupPermissions';

  constructor(public groupId: number, public payload: string[]) { }
}

export class GetGroupMembers {
  static readonly type = '[Admin Groups] GetGroupMembers';

  constructor(public groupId: number) { }
}

export class AddGroupMembers {
  static readonly type = '[Admin Groups] AddGroupMembers';

  constructor(public groupId: number, public payload: number[]) { }
}

export class RemoveGroupMembers {
  static readonly type = '[Admin Groups] RemoveGroupMembers';

  constructor(public groupId: number, public payload: number[]) { }
}

export class GetGroupFolders {
  static readonly type = '[Admin Groups] GetGroupFolders';

  constructor(public groupId: number) { }
}
export class UpdateGroupFolders {
  static readonly type = '[Admin Groups] UpdateRoleMediaAccess';

  constructor(public groupid: number, public payload: number[]) {

  }
}