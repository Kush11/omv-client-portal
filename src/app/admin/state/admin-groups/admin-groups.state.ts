import { Group } from '../../../core/models/entity/group';
import { GroupsService } from '../../../core/services/business/groups/groups.service';
import {
  GetActiveGroups, DisableGroup, EnableGroup, UpdateGroup, GetGroup, CreateGroup,
  SetCurrentGroupId, GetGroupMembers, GetGroupPermissions, UpdateGroupPermissions, AddGroupMembers, RemoveGroupMembers, ClearGroup, GetGroupFolders, UpdateGroupFolders, EnableGroups, DisableGroups, UpdateGroupsPermissions, GetDisabledGroups
} from './admin-groups.action';
import { Action, State, StateContext, Selector } from '@ngxs/store';
import { tap } from 'rxjs/operators';
import { User } from 'src/app/core/models/entity/user';
import { GroupStatus } from 'src/app/core/enum/group-status.enum';
import { Permission } from 'src/app/core/enum/permission';
import { Directory_GetAllOutputDTO } from 'src/app/core/dtos/output/directories/Directory_GetAllOutputDTO';
import { ShowErrorMessage, ShowSuccessMessage, ShowSpinner, HideSpinner } from 'src/app/state/app.actions';


export class AdminGroupStateModel {
  activeGroups: Group[];
  disabledGroups: Group[];
  totalGroups: number;
  currentGroupId: number;
  currentGroup: Group;
  currentGroupPermission: Permission[];
  currentGroupmediaAccess: Directory_GetAllOutputDTO;
  currentGroupmembers: User[];
  currentGroupMediaAccessIds: number[];
  isProcessComplete: boolean;
}

const initialGroup: Group = {
  id: 0,
  name: '',
  nameWithBadge: '',
  description: '',
  isSystem: false,
  memberCount: 0,
  status: 1,
  modifiedBy: '',
  modifiedOn: new Date()
}

@State<AdminGroupStateModel>({
  name: 'admin_groups',
  defaults: {
    activeGroups: [],
    disabledGroups: [],
    totalGroups: 0,
    currentGroupId: null,
    currentGroup: initialGroup,
    currentGroupPermission: null,
    currentGroupmediaAccess: null,
    currentGroupmembers: [],
    currentGroupMediaAccessIds: [],
    isProcessComplete: false
  }
})
export class AdminGroupState {

  //#region S E L E C T O R S

  @Selector()
  static getActiveGroups(state: AdminGroupStateModel) {
    return state.activeGroups;
  }

  @Selector()
  static getDisabledGroups(state: AdminGroupStateModel) {
    return state.disabledGroups;
  }

  @Selector()
  static getTotalGroups(state: AdminGroupStateModel) {
    return state.totalGroups;
  }

  @Selector()
  static getIsProcessComplete(state: AdminGroupStateModel) {
    return state.isProcessComplete;
  }

  @Selector()
  static getCurrentGroupId(state: AdminGroupStateModel) {
    return state.currentGroupId;
  }

  @Selector()
  static getCurrentGroup(state: AdminGroupStateModel) {
    return state.currentGroup;
  }

  @Selector()
  static getGroupMembers(state: AdminGroupStateModel) {
    return state.currentGroupmembers;
  }

  @Selector()
  static getMediaAccess(state: AdminGroupStateModel) {
    return state.currentGroupmediaAccess;
  }

  @Selector()
  static getPermissionsByGroupId(state: AdminGroupStateModel) {
    console.log(" AdminGroupState - getPermissionsByGroupId " + state.currentGroupPermission);
    return state.currentGroupPermission;
  }

  @Selector()
  static getRoleMediaAccessIds(state: AdminGroupStateModel) {
    // console.log(" AdminGroupState - currentGroupMediaAccessIds " + state.currentGroupMediaAccessIds);
    return state.currentGroupMediaAccessIds;
  }
  //#endregion

  constructor(private groupsService: GroupsService) { }

  //#region A C T I O N S

  @Action(GetActiveGroups)
  getActiveGroups(ctx: StateContext<AdminGroupStateModel>) {
    ctx.dispatch(new ShowSpinner());
    return this.groupsService.getGroups()
      .pipe(
        tap(response => {
          const state = ctx.getState();
          const groups = response.filter(x => x.status === GroupStatus.Active);
          ctx.setState({
            ...state,
            activeGroups: groups,
            totalGroups: groups.length,
            isProcessComplete: false
          });
          ctx.dispatch(new HideSpinner());
        }, (err) => ctx.dispatch(new ShowErrorMessage(err)))
      );
  }

  @Action(GetDisabledGroups)
  getDisabledGroups(ctx: StateContext<AdminGroupStateModel>, { pageNumber, pageSize }: GetDisabledGroups) {
    ctx.dispatch(new ShowSpinner());
    const status = 0;
    return this.groupsService.getGroups(status, pageNumber, pageSize)
      .pipe(
        tap(response => {
          const state = ctx.getState();
          const groups = response.filter(x => x.status === GroupStatus.Disabled)
          ctx.setState({
            ...state,
            disabledGroups: groups,
            totalGroups: groups.length,
            isProcessComplete: false
          });
          ctx.dispatch(new HideSpinner());
        }, (err) => ctx.dispatch(new ShowErrorMessage(err)))
      );
  }

  @Action(GetGroup)
  getGroup(ctx: StateContext<AdminGroupStateModel>, { id }: GetGroup) {
    ctx.dispatch(new ShowSpinner());
    return this.groupsService.getGroup(id)
      .pipe(
        tap(group => {
          const state = ctx.getState();
          ctx.setState({
            ...state,
            currentGroup: group
          });
          ctx.dispatch(new HideSpinner());
        }, (err) => ctx.dispatch(new ShowErrorMessage(err)))
      );
  }

  @Action(ClearGroup)
  clearGroup({ getState, setState }: StateContext<AdminGroupStateModel>) {
    const state = getState();
    setState({
      ...state,
      currentGroup: initialGroup
    });
  }

  @Action(CreateGroup)
  createGroup(ctx: StateContext<AdminGroupStateModel>, { payload }: CreateGroup) {
    return this.groupsService.createGroup(payload).pipe(
      tap(group => {
        ctx.dispatch(new ShowSuccessMessage('Group created successfully.'));
        const state = ctx.getState();
        ctx.setState({
          ...state,
          currentGroupId: group.id
        });
      }, err => ctx.dispatch(new ShowErrorMessage(err)))
    );
  }

  @Action(UpdateGroup)
  updateGroup(ctx: StateContext<AdminGroupStateModel>, { payload, id }: UpdateGroup) {
    return this.groupsService.updateGroup(id, payload).pipe(
      tap(() => {
        ctx.dispatch(new ShowSuccessMessage('Group updated successfully.'));
      }, err => ctx.dispatch(new ShowErrorMessage(err)))
    );
  }

  @Action(EnableGroup)
  enableGroup(ctx: StateContext<AdminGroupStateModel>, { group }: EnableGroup) {
    ctx.dispatch(new ShowSpinner());
    return this.groupsService.enableGroup(group)
      .then(() => {
        ctx.dispatch(new HideSpinner());
        ctx.dispatch(new ShowSuccessMessage(`${group.name} was enabled successfully.`));
      }, err => ctx.dispatch(new ShowErrorMessage(err))
      );
  }

  @Action(DisableGroup)
  disableGroup(ctx: StateContext<AdminGroupStateModel>, { group }: DisableGroup) {
    ctx.dispatch(new ShowSpinner());
    return this.groupsService.disableGroup(group)
      .then(() => {
        ctx.dispatch(new HideSpinner());
        ctx.dispatch(new ShowSuccessMessage(`${group.name} was disabled successfully.`));
      }, err => ctx.dispatch(new ShowErrorMessage(err))
      );
  }

  @Action(EnableGroups)
  enableGroups(ctx: StateContext<AdminGroupStateModel>, { groups }: EnableGroups) {
    ctx.dispatch(new ShowSpinner());
    const lastGroup = groups[groups.length - 1];
    groups.forEach(group => {
      return this.groupsService.enableGroup(group)
        .then(() => {
          if (group.id === lastGroup.id) {
            const message = groups.length > 1 ? `Groups were enabled successfully` : 'Group was enabled successfully';
            ctx.dispatch(new ShowSuccessMessage(message));
            const state = ctx.getState();
            ctx.setState({
              ...state,
              isProcessComplete: true
            });
          }
        }, (err) => ctx.dispatch(new ShowErrorMessage(err)));
    });
  }

  @Action(DisableGroups)
  disableGroups(ctx: StateContext<AdminGroupStateModel>, { groups }: DisableGroups) {
    ctx.dispatch(new ShowSpinner());
    const lastGroup = groups[groups.length - 1];
    groups.forEach(group => {
      return this.groupsService.disableGroup(group)
        .then(() => {
          if (group.id === lastGroup.id) {
            const message = groups.length > 1 ? `Groups were disabled successfully` : 'Group was disabled successfully';
            ctx.dispatch(new ShowSuccessMessage(message));
            const state = ctx.getState();
            ctx.setState({
              ...state,
              isProcessComplete: true
            });
          }
        }, (err) => ctx.dispatch(new ShowErrorMessage(err)));
    });
  }

  @Action(UpdateGroupsPermissions)
  updateGroupsPermissions(ctx: StateContext<AdminGroupStateModel>, { groupsIds, permissionIds }: UpdateGroupsPermissions) {
    const lastGroupId = groupsIds[groupsIds.length - 1];
    groupsIds.forEach(id => {
      return this.groupsService.updatePermissions(id, permissionIds)
        .then(() => {
          if (id === lastGroupId) {
            ctx.dispatch(new ShowSuccessMessage('Permissions were updated successfully.'));
          }
        }, (err) => {
          ctx.dispatch(new ShowErrorMessage(err));
        });
    });
  }

  @Action(SetCurrentGroupId)
  setCurrentGroupId({ getState, setState }: StateContext<AdminGroupStateModel>, { id }: SetCurrentGroupId) {
    var state = getState();
    return setState({
      ...state,
      currentGroupId: id,
    });
  }

  @Action(GetGroupMembers)
  getGroupMembers(ctx: StateContext<AdminGroupStateModel>, { groupId }: GetGroupMembers) {
    return this.groupsService.getGroupMembers(groupId)
      .pipe(
        tap(users => {
          const state = ctx.getState();
          return ctx.setState({
            ...state,
            currentGroupmembers: users
          });
        }, err => ctx.dispatch(new ShowErrorMessage(err)))
      );
  }

  @Action(GetGroupPermissions)
  getGroupPermissions(ctx: StateContext<AdminGroupStateModel>, { groupId }: GetGroupPermissions) {
    return this.groupsService.getGroupPermissions(groupId)
      .pipe(
        tap(permissions => {
          const state = ctx.getState();
          return ctx.setState({
            ...state,
            currentGroupPermission: permissions
          });
        }, err => ctx.dispatch(new ShowErrorMessage(err)))
      );
  }

  @Action(UpdateGroupPermissions)
  updateGroupPermissions(ctx: StateContext<AdminGroupStateModel>, { groupId, payload }: UpdateGroupPermissions) {
    return this.groupsService.updateGroupPermissions(groupId, payload)
      .pipe(
        tap(() => {
          ctx.dispatch(new ShowSuccessMessage('Group permissions updated.'));
          ctx.dispatch(new GetGroupPermissions(groupId));
        }, (err) => ctx.dispatch(new ShowErrorMessage(err)))
      );
  }

  @Action(AddGroupMembers)
  addGroupMembers(ctx: StateContext<AdminGroupStateModel>, { groupId, payload }: AddGroupMembers) {
    return this.groupsService.addGroupMembers(groupId, payload)
      .pipe(
        tap(response => {
          const state = ctx.getState();
          ctx.dispatch(new ShowSuccessMessage(`Group members updated.`));
          var members = response as User[];
          ctx.setState({
            ...state,
            currentGroupmembers: members
          });
        }, err => ctx.dispatch(new ShowErrorMessage(err))));
  }

  @Action(RemoveGroupMembers)
  removeGroupMembers(ctx: StateContext<AdminGroupStateModel>, { groupId, payload }: RemoveGroupMembers) {
    ctx.dispatch(new ShowSpinner());
    return this.groupsService.removeGroupMembers(groupId, payload)
      .pipe(
        tap(() => {
          ctx.dispatch(new ShowSuccessMessage(`${payload.length} member(s) removed.`));
          ctx.dispatch(new HideSpinner());
          ctx.dispatch(new GetGroupMembers(groupId));
        }, (err) => ctx.dispatch(new ShowErrorMessage(err))
        ));
  }

  @Action(GetGroupFolders)
  getGroupFolders(ctx: StateContext<AdminGroupStateModel>, { groupId }: GetGroupFolders) {
    ctx.dispatch(new ShowSpinner());
    return this.groupsService.getFolders(groupId)
      .pipe(
        tap((groupFolders) => {
          const state = ctx.getState();
          const groupFolderIds: any[] = groupFolders.map(folder => folder.id);
          ctx.setState({
            ...state,
            currentGroupMediaAccessIds: groupFolderIds
          });
          ctx.dispatch(new HideSpinner());
        }, (err) => ctx.dispatch(new ShowErrorMessage(err)))
      );
  }

  @Action(UpdateGroupFolders)
  updateGroupFolders(ctx: StateContext<AdminGroupStateModel>, { groupid, payload }: UpdateGroupFolders) {
    return this.groupsService.updateFolders(groupid, payload)
      .pipe(
        tap(() => {
          ctx.dispatch(new ShowSuccessMessage('Group media access was updated successfully'));
        }, (err) => ctx.dispatch(new ShowErrorMessage(err)))
      );
  }

  //#endregion
}
