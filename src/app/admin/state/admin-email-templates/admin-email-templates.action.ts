import { EmailTemplate } from 'src/app/core/models/entity/email-templates';

export class GetEmailTemplates {
    static readonly type = '[Admin Email] GetEmailTemplates';
}
export class GetEmailTemplate {
    static readonly type = '[Admin Email] GetEmailTemplate';

    constructor(public id: string) { }
}

export class CreateEmailTemplate {
    static readonly type = '[Admin Email] CreateEmailTemplate';

    constructor(public payload: EmailTemplate) { }
}

export class UpdateEmailTemplate {
    static readonly type = '[Admin Email] UpdateEmailTemplate';

    constructor(public id: string, public payload: EmailTemplate) { }
}

export class RemoveEmailTemplate {
    static readonly type = '[Admin Email] RemoveEmailTemplate';

    constructor(public id: string) { }
}

export class SetCurrentEmailTemplate {
    static readonly type = '[Admin Email] SetEmailTemplate';

    constructor(public payload: EmailTemplate) { }
  }

export class ClearCurrentEmailTemplate {
    static readonly type = '[Admin Email] ClearCurrentEmailTemplate';
  }

export class SetCurrentEmailTemplateId {
    static readonly type = '[Admin Email] SetCurrentEmailTemplateId';

    constructor(public id: string) { }
  }
export class GetEmailPlaceholders {
    static readonly type = '[Admin Email] GetEmailPlaceholders';

  }
export class ClearEmailPlaceholders {
    static readonly type = '[Admin Email] ClearEmailPlaceholders';

  }
