import { EmailTemplate } from 'src/app/core/models/entity/email-templates';
import { State, Selector, Action, StateContext } from '@ngxs/store';
import { GetEmailTemplates, GetEmailTemplate, CreateEmailTemplate, UpdateEmailTemplate, RemoveEmailTemplate, SetCurrentEmailTemplateId, ClearCurrentEmailTemplate, GetEmailPlaceholders, ClearEmailPlaceholders } from './admin-email-templates.action';
import { EmailTemplatesService } from 'src/app/core/services/business/email-templates/email-templates.service';
import { HideSpinner, ShowErrorMessage, ShowSuccessMessage } from 'src/app/state/app.actions';
import { DateService } from 'src/app/core/services/business/dates/date.service';
import { tap } from 'rxjs/internal/operators/tap';
import { Lookup } from 'src/app/core/models/entity/lookup';
import { LookupService } from 'src/app/core/services/business/lookup/lookup.service';

export class AdminEmailTemplatesStateModel {
  emailTemplates: EmailTemplate[];
  currentEmailTemplate: EmailTemplate;
  currentEmailTemplateId: string;
  placeholders: Lookup[]
}

const initialEmailTemplate: EmailTemplate = {
  id: '',
  name: '',
  subject: '',
  body: '',
  createdOn: new Date(),
  createdBy: '',
  modifiedOn: new Date(),
  modifiedBy: '',
}

@State<AdminEmailTemplatesStateModel>({
  name: 'admin_email_templates',
  defaults: {
    emailTemplates: [],
    currentEmailTemplate: initialEmailTemplate,
    currentEmailTemplateId: null,
    placeholders: []
  }
})

export class AdminEmailTemplatesState {

  @Selector()
  static getEmailTemplates(state: AdminEmailTemplatesStateModel) {
    return state.emailTemplates;
  }

  @Selector()
  static getEmailTemplate(state: AdminEmailTemplatesStateModel) {
    return state.emailTemplates;
  }

  @Selector()
  static getCurrentEmailTemplate(state: AdminEmailTemplatesStateModel) {
    return state.currentEmailTemplate;
  }

  @Selector()
  static getCurrentEmailTemplateId(state: AdminEmailTemplatesStateModel) {
    return state.currentEmailTemplateId;
  }

  @Selector()
  static getPlaceholders(state: AdminEmailTemplatesStateModel) {
    return state.placeholders;
  }

  constructor(private emailTemplatesDataService: EmailTemplatesService, private dateService: DateService, private lookupService: LookupService) { }

  @Action(GetEmailTemplates)
  getEmailTemplates({ getState, setState }: StateContext<AdminEmailTemplatesStateModel>) {
    return this.emailTemplatesDataService.getEmailTemplates().pipe(
      tap(email => {
        const state = getState();
        setState({
          ...state,
          emailTemplates: email
        });
      })
    );
  }

  @Action(GetEmailTemplate)
  getEmailTemplate(ctx: StateContext<AdminEmailTemplatesStateModel>, { id }: GetEmailTemplate) {
    return this.emailTemplatesDataService.getEmailTemplate(id)
      .pipe(
        tap(email => {
          const state = ctx.getState();
          ctx.setState({
            ...state,
            currentEmailTemplate: email ? email : null
          });
        }, (err) => ctx.dispatch(new ShowSuccessMessage(err)))
      );
  }

  @Action(CreateEmailTemplate)
  createEmailTemplate(ctx: StateContext<AdminEmailTemplatesStateModel>, { payload }: CreateEmailTemplate) {
    return this.emailTemplatesDataService.createEmailTemplate(payload)
      .pipe(
        tap(newTemplateId => {
          const state = ctx.getState();
          ctx.setState({
            ...state,
            currentEmailTemplateId: newTemplateId
          });
          ctx.dispatch(new ShowSuccessMessage('Email Template was saved successfully.'));
          ctx.dispatch(new HideSpinner());
        }, (err) => ctx.dispatch(new ShowErrorMessage(err)))
      );
  }

  @Action(UpdateEmailTemplate)
  updateEmailTemplate(ctx: StateContext<AdminEmailTemplatesStateModel>, { payload, id }: UpdateEmailTemplate) {
    return this.emailTemplatesDataService.updateEmailTemplate(id, payload)
      .pipe(
        tap(() => {
          ctx.dispatch(new ShowSuccessMessage('Email Template was updated successfully.'));
          ctx.dispatch(new GetEmailTemplate(id));
          ctx.dispatch(new HideSpinner());
        }, (err) => ctx.dispatch(new ShowErrorMessage(err)))
      );
  }

  @Action(SetCurrentEmailTemplateId)
  setCurrentEmailTemplateId({ getState, setState }: StateContext<AdminEmailTemplatesStateModel>, { id }: SetCurrentEmailTemplateId) {
    const state = getState();
    return setState({
      ...state,
      currentEmailTemplateId: id,
    });
  }

  @Action(RemoveEmailTemplate)
  removeEmailTemplate(ctx: StateContext<AdminEmailTemplatesStateModel>, { id }: RemoveEmailTemplate) {
    return this.emailTemplatesDataService.removeEmailTemplate(id)
      .pipe(
        tap(() => {
          ctx.dispatch(new GetEmailTemplates());
          ctx.dispatch(new ShowSuccessMessage('Template was successfully deleted.'));
          ctx.dispatch(new HideSpinner());
        }, (err) => ctx.dispatch(new ShowErrorMessage(err)))
      );
  }

  @Action(ClearCurrentEmailTemplate)
  clearCurrentEmailTemplate({ getState, setState }: StateContext<AdminEmailTemplatesStateModel>) {
    const state = getState();
    return setState({
      ...state,
      currentEmailTemplate: initialEmailTemplate,
    });
  }

  @Action(GetEmailPlaceholders)
  getEmailPlaceholders(ctx: StateContext<AdminEmailTemplatesStateModel>) {
    return this.lookupService.getEmailPlaceholderLookup()
      .pipe(
        tap(emailPlaceholders => {
          const state = ctx.getState();
          ctx.setState({
            ...state,
            placeholders: emailPlaceholders
          });
        })
      );
  }

  @Action(ClearEmailPlaceholders)
  clearEmailPlaceholders({ getState, setState }: StateContext<AdminEmailTemplatesStateModel>) {
    const state = getState();
    return setState({
      ...state,
      placeholders: [],
    });
  }

}
