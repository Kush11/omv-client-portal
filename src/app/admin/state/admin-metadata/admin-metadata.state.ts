import { State, Selector, Action, StateContext } from '@ngxs/store';
import { MetadataField } from 'src/app/core/models/entity/metadata-field';
import { MetadataList } from 'src/app/core/models/entity/metadata-list';
import { MetadataListItem } from 'src/app/core/models/entity/metadata-list-item';
import { MetadataListStatus } from 'src/app/core/enum/metadata-list-status';
import { MetadataListService } from 'src/app/core/services/business/metadata-list/metadata-list.service';
import { MetadataListItemsService } from 'src/app/core/services/business/metadata-list-items/metadata-list-items.service';
import { FieldTypesService } from 'src/app/core/services/business/field-types/field-types.service';
import { MetadataFieldsService } from 'src/app/core/services/business/metadata-fields/metadata-fields.service';
import {
  CreateMetadataField, UpdateMetadataField, RemoveMetadataField, GetFieldTypes,
  GetMetadataList, CreateMetadataList, UpdateMetadataList, SetCurrentMetadataListId, EnableMetadataList, DisableMetadataList,
  EnableMetadataLists, DisableMetadataLists, RemoveMetadataList, GetMetadataListItems, GetMetaDataDependentListItems,
  ClearMetadataDependentListItems, ClearListItem, CreateMetadataListItem, RemoveMetadataListItem, GetActiveMetadataLists, GetDisabledMetadataLists, GetMetadataModules, GetMediaMetadataFields, GetWorkPlanningMetadataFields, UpdateMetadataListItem, SortMetadataFields
} from './admin-metadata.action';
import { HideSpinner, ShowSpinner, ShowErrorMessage, ShowSuccessMessage } from 'src/app/state/app.actions';
import { tap } from 'rxjs/internal/operators/tap';
import { FieldType } from 'src/app/core/models/entity/field-type';
import { MetadataFieldType } from 'src/app/core/enum/metadataFieldType';
import { Lookup } from 'src/app/core/models/entity/lookup';
import { LookupService } from 'src/app/core/services/business/lookup/lookup.service';

export class AdminMetadataStateModel {
  mediaMetadataFields: MetadataField[];
  workPlanningMetadataFields: MetadataField[];

  activeMetadataLists: MetadataList[];
  disabledMetadataLists: MetadataList[];
  totalList: number;

  currentMetadataList: MetadataList;
  currentMetadataListId: number;

  metadataListsItems: MetadataListItem[];
  currentMetadataListItems: MetadataListItem[];
  dependentMetadataListItems: MetadataListItem[];

  fieldTypes: FieldType[];
  isProcessComplete: boolean;

  modules: Lookup[];
}
@State<AdminMetadataStateModel>({
  name: 'adminMetadata',
  defaults: {
    mediaMetadataFields: [],
    workPlanningMetadataFields: [],

    activeMetadataLists: [],
    disabledMetadataLists: [],
    totalList: 0,

    currentMetadataListId: null,
    currentMetadataList: null,

    metadataListsItems: [],
    dependentMetadataListItems: [],
    currentMetadataListItems: [],

    fieldTypes: [],
    isProcessComplete: false,

    modules: []
  }
})

export class AdminMetadataState {

  //#region MetadataFields

  @Selector()
  static getMediaMetadataFields(state: AdminMetadataStateModel) {
    return state.mediaMetadataFields;
  }

  @Selector()
  static getWorkPlanningMetadataFields(state: AdminMetadataStateModel) {
    return state.workPlanningMetadataFields;
  }

  @Selector()
  static getFieldTypes(state: AdminMetadataStateModel) {
    return state.fieldTypes;
  }

  @Selector()
  static getModules(state: AdminMetadataStateModel) {
    return state.modules;
  }

  //#endregion

  //#region MetadataList

  @Selector()
  static getActiveMetadataLists(state: AdminMetadataStateModel) {
    return state.activeMetadataLists;
  }

  @Selector()
  static getDisabledMetadataLists(state: AdminMetadataStateModel) {
    return state.disabledMetadataLists;
  }

  @Selector()
  static getTotalListCount(state: AdminMetadataStateModel) {
    return state.totalList;
  }

  @Selector()
  static getCurrentMetadataListId(state: AdminMetadataStateModel) {
    return state.currentMetadataListId;
  }

  @Selector()
  static getCurrentMetadataList(state: AdminMetadataStateModel) {
    return state.currentMetadataList;
  }

  @Selector()
  static getIsProcessComplete(state: AdminMetadataStateModel) {
    return state.isProcessComplete;
  }

  //#endregion

  //#region MetadataListItems

  @Selector()
  static getMetaDataListItem(state: AdminMetadataStateModel) {
    return state.metadataListsItems;
  }

  @Selector()
  static getCurrentMetadataListItems(state: AdminMetadataStateModel) {
    return state.currentMetadataListItems;
  }
  @Selector()
  static getCurrentDependentMetadataListItem(state: AdminMetadataStateModel) {
    return state.dependentMetadataListItems;
  }

  //#endregion

  constructor(private metadataListService: MetadataListService,
    private metadataListItemsService: MetadataListItemsService, private fieldTypesService: FieldTypesService,
    private metadataFieldsService: MetadataFieldsService, private lookupService: LookupService) { }


  //#region MetadataFields

  @Action(GetMediaMetadataFields)
  getMediaMetadataFields(ctx: StateContext<AdminMetadataStateModel>) {
    ctx.dispatch(new ShowSpinner());
    return this.metadataFieldsService.getMediaMetadataFields()
      .pipe(
        tap(fields => {
          const state = ctx.getState();
          ctx.setState({
            ...state,
            mediaMetadataFields: fields
          });
          ctx.dispatch(new HideSpinner());
        }, err => ctx.dispatch(new ShowErrorMessage(err)))
      );
  }


  @Action(GetWorkPlanningMetadataFields)
  getWorkPlanningMetadataFields(ctx: StateContext<AdminMetadataStateModel>, { hideSpinner }: GetWorkPlanningMetadataFields) {
    if (!hideSpinner)
      ctx.dispatch(new ShowSpinner());
    return this.metadataFieldsService.getWorkPlanningMetadataFields()
      .pipe(
        tap(fields => {
          const state = ctx.getState();
          ctx.setState({
            ...state,
            workPlanningMetadataFields: fields
          });
          if (!hideSpinner)
            ctx.dispatch(new HideSpinner());
        }, err => ctx.dispatch(new ShowErrorMessage(err)))
      );
  }

  @Action(CreateMetadataField)
  createMetaDataField(ctx: StateContext<AdminMetadataStateModel>, { payload }: CreateMetadataField) {
    return this.metadataFieldsService.createMetaDataField(payload).pipe(
      tap(() => {
        ctx.dispatch(new ShowSuccessMessage('Create successful.'));
      }, err => ctx.dispatch(new ShowErrorMessage(err)))
    );
  }

  @Action(UpdateMetadataField)
  updateMetaDataField(ctx: StateContext<AdminMetadataStateModel>, { id, payload }: UpdateMetadataField) {
    return this.metadataFieldsService.updateMetaDataField(id, payload).pipe(
      tap(() => {
        ctx.dispatch(new ShowSuccessMessage("Field updated successfully"));
      }, err => ctx.dispatch(new ShowErrorMessage(err)))
    );
  }

  @Action(RemoveMetadataField)
  removeMetaDataFields(ctx: StateContext<AdminMetadataStateModel>, { id }: RemoveMetadataField) {
    return this.metadataFieldsService.removeMetadataField(id)
      .pipe(
        tap(() => {
          ctx.dispatch(new ShowSuccessMessage('Delete successful.'));
          ctx.dispatch(new HideSpinner());
        }, err => ctx.dispatch(new ShowErrorMessage(err)))
      );
  }

  @Action(GetFieldTypes)
  getFieldType(ctx: StateContext<AdminMetadataStateModel>) {
    return this.fieldTypesService.getFieldTypes()
      .pipe(
        tap(fieldTypes => {
          fieldTypes = fieldTypes.filter(f => f.type !== MetadataFieldType.Label);
          const state = ctx.getState();
          ctx.setState({
            ...state,
            fieldTypes: fieldTypes
          });
        })
      );
  }

  @Action(GetMetadataModules)
  getMetadataModules(ctx: StateContext<AdminMetadataStateModel>) {
    return this.lookupService.getMetadataModules()
      .pipe(
        tap(modules => {
          const state = ctx.getState();
          ctx.setState({
            ...state,
            modules: modules
          });
        })
      );
  }

  @Action(SortMetadataFields)
  sortMetadataFields(ctx: StateContext<AdminMetadataStateModel>, { metadataFields, isMedia }: SortMetadataFields) {
    ctx.dispatch(new ShowSpinner());
    const payload = metadataFields.map((item, index) => Object.assign({}, item, { order: index }));
    this.metadataFieldsService.sortFields(payload)
      .then(() => {
        ctx.dispatch(new ShowSuccessMessage(`MetadataFields were successfully sorted.`));
        if (isMedia) ctx.dispatch(new GetMediaMetadataFields());
        else ctx.dispatch(new GetWorkPlanningMetadataFields());
        ctx.dispatch(new HideSpinner());
      }, error => ctx.dispatch(new ShowErrorMessage(error)));
  }

  //#endregion

  //#region MetadataList

  @Action(GetActiveMetadataLists)
  getActiveMetaDataLists(ctx: StateContext<AdminMetadataStateModel>) {
    ctx.dispatch(new ShowSpinner());
    return this.metadataListService.getMetadataLists()
      .pipe(
        tap(lists => {
          lists = lists.filter(x => x.status === MetadataListStatus.Active);
          const state = ctx.getState();
          ctx.setState({
            ...state,
            activeMetadataLists: lists,
            totalList: lists.length,
            isProcessComplete: false
          });
          ctx.dispatch(new HideSpinner());
        }, err => ctx.dispatch(new ShowErrorMessage(err)))
      );
  }

  @Action(GetDisabledMetadataLists)
  getDisabledMetadataLists(ctx: StateContext<AdminMetadataStateModel>) {
    ctx.dispatch(new ShowSpinner());
    return this.metadataListService.getMetadataLists()
      .pipe(
        tap(lists => {
          lists = lists.filter(x => x.status === MetadataListStatus.Disabled);
          const state = ctx.getState();
          ctx.setState({
            ...state,
            disabledMetadataLists: lists,
            totalList: lists.length,
            isProcessComplete: false
          });
          ctx.dispatch(new HideSpinner());
        }, err => ctx.dispatch(new ShowErrorMessage(err)))
      );
  }

  @Action(GetMetadataList)
  getMetaDataList(ctx: StateContext<AdminMetadataStateModel>, { id }: GetMetadataList) {
    ctx.dispatch(new ShowSpinner());
    return this.metadataListService.getMetadataList(id)
      .pipe(
        tap(list => {
          const state = ctx.getState();
          ctx.setState({
            ...state,
            currentMetadataList: list
          });
          ctx.dispatch(new HideSpinner());
        }, err => ctx.dispatch(new ShowErrorMessage(err)))
      );
  }

  @Action(CreateMetadataList)
  createMetaDataList(ctx: StateContext<AdminMetadataStateModel>, { payload }: CreateMetadataList) {
    ctx.dispatch(new ShowSpinner());
    return this.metadataListService.createMetaDataList(payload)
      .pipe(
        tap(() => {
          ctx.dispatch(new ShowSuccessMessage('Metadata List was created successfully.'));
          ctx.dispatch(new GetActiveMetadataLists());
        }, err => ctx.dispatch(new ShowErrorMessage(err)))
      );
  }

  @Action(UpdateMetadataList)
  updateMetadataList(ctx: StateContext<AdminMetadataStateModel>, { payload, id }: UpdateMetadataList) {
    return this.metadataListService.updateMetadataList(id, payload).pipe(
      tap(() => {
        ctx.dispatch(new ShowSuccessMessage('Metadata List was updated successfully.'));
        ctx.dispatch(new GetMetadataList(id));
      }, (err) => {
        ctx.dispatch(new ShowErrorMessage(err));
      })
    );
  }

  @Action(SetCurrentMetadataListId)
  setCurrentMetadataListId({ getState, setState }: StateContext<AdminMetadataStateModel>, { id }: SetCurrentMetadataListId) {
    const state = getState();
    return setState({
      ...state,
      currentMetadataListId: id
    });
  }

  @Action(EnableMetadataList)
  enableMetadataList(ctx: StateContext<AdminMetadataStateModel>, { list }: EnableMetadataList) {
    ctx.dispatch(new ShowSpinner());
    return this.metadataListService.enableMetadataList(list)
      .then(() => {
        ctx.dispatch(new ShowSuccessMessage(`${list.name} was enabled successfully.`));
        ctx.dispatch(new HideSpinner());
      }, (err) => ctx.dispatch(new ShowErrorMessage(err))
      );
  }

  @Action(DisableMetadataList)
  disableMetadataList(ctx: StateContext<AdminMetadataStateModel>, { list }: DisableMetadataList) {
    ctx.dispatch(new ShowSpinner());
    return this.metadataListService.disableMetadataList(list)
      .then(() => {
        ctx.dispatch(new ShowSuccessMessage(`${list.name} was disabled successfully.`));
        ctx.dispatch(new HideSpinner());
      }, (err) => ctx.dispatch(new ShowErrorMessage(err))
      );
  }

  @Action(EnableMetadataLists)
  enableMetadataLists(ctx: StateContext<AdminMetadataStateModel>, { lists }: EnableMetadataLists) {
    ctx.dispatch(new ShowSpinner());
    const lastList = lists[lists.length - 1];
    lists.forEach(list => {
      return this.metadataListService.enableMetadataList(list)
        .then(() => {
          if (list.id === lastList.id) {
            const message = lists.length > 1 ? 'Metadata Lists were enabled successfully.' : 'Metadata List was enabled successfully.';
            ctx.dispatch(new ShowSuccessMessage(message));
            const state = ctx.getState();
            ctx.setState({
              ...state,
              isProcessComplete: true
            });
          }
        }, error => ctx.dispatch(new ShowErrorMessage(error)));
    });
  }

  @Action(DisableMetadataLists)
  disableMetadataLists(ctx: StateContext<AdminMetadataStateModel>, { lists }: DisableMetadataLists) {
    ctx.dispatch(new ShowSpinner());
    const lastList = lists[lists.length - 1];
    lists.forEach(list => {
      return this.metadataListService.disableMetadataList(list)
        .then(() => {
          if (list.id === lastList.id) {
            const message = lists.length > 1 ? 'Metadata Lists were disabled successfully.' : 'Metadata List was disabled successfully.';
            ctx.dispatch(new ShowSuccessMessage(message));
            const state = ctx.getState();
            ctx.setState({
              ...state,
              isProcessComplete: true
            });
          }
        }, error => ctx.dispatch(new ShowErrorMessage(error)));
    });
  }

  @Action(RemoveMetadataList)
  removeMetaDataList(ctx: StateContext<AdminMetadataStateModel>, { id }: RemoveMetadataList) {
    return this.metadataListService.removeMetadataList(id).pipe(
      tap(() => {
        ctx.dispatch(new ShowSuccessMessage('Metadata List was deleted successfully.'));
        const state = ctx.getState();
        ctx.setState({
          ...state,
          isProcessComplete: true
        });
      }, (err) => ctx.dispatch(new ShowErrorMessage(err)))
    );
  }

  //#endregion

  //#region MetadataListItems

  @Action(GetMetadataListItems)
  getMetadataListItems(ctx: StateContext<AdminMetadataStateModel>, { listId }: GetMetadataListItems) {
    ctx.dispatch(new ShowSpinner());
    return this.metadataListItemsService.getMetadataListItems(listId).pipe(
      tap(lists => {
        const state = ctx.getState();
        ctx.setState({
          ...state,
          currentMetadataListItems: lists
        });
        ctx.dispatch(new HideSpinner());
      }, error => ctx.dispatch(new ShowErrorMessage(error)))
    );
  }

  @Action(GetMetaDataDependentListItems)
  getDependentMetadataListItems(ctx: StateContext<AdminMetadataStateModel>, { listId }: GetMetadataListItems) {
    ctx.dispatch(new ShowSpinner());
    return this.metadataListItemsService.getMetadataListItems(listId)
      .pipe(
        tap(lists => {
          const state = ctx.getState();
          ctx.setState({
            ...state,
            dependentMetadataListItems: lists
          });
          ctx.dispatch(new HideSpinner());
        }, error => ctx.dispatch(new ShowErrorMessage(error)))
      );
  }

  @Action(ClearMetadataDependentListItems)
  clearDependentMetadataListItems({ getState, setState }: StateContext<AdminMetadataStateModel>) {
    const state = getState();
    setState({
      ...state,
      dependentMetadataListItems: []
    });
  }

  @Action(ClearListItem)
  clearListItem({ getState, setState }: StateContext<AdminMetadataStateModel>) {
    const state = getState();
    setState({
      ...state,
      currentMetadataList: null
    });
  }

  @Action(CreateMetadataListItem)
  createMetaDataListItem(ctx: StateContext<AdminMetadataStateModel>, { id, payload }: CreateMetadataListItem) {
    ctx.dispatch(new ShowSpinner());
    return this.metadataListService.createMetaDataListItem(id, payload)
      .pipe(
        tap(() => {
          ctx.dispatch(new ShowSuccessMessage(`${payload.description} was created successfully.`));
          ctx.dispatch(new GetMetadataListItems(id));
        }, (err) => ctx.dispatch(new ShowErrorMessage(err)))
      );
  }

  @Action(UpdateMetadataListItem)
  updateMetadataListItem(ctx: StateContext<AdminMetadataStateModel>, { id, payload }: UpdateMetadataListItem) {
    ctx.dispatch(new ShowSpinner());
    return this.metadataListItemsService.updateMetadataListItem(id, payload).pipe(
      tap(() => {
        ctx.dispatch(new ShowSuccessMessage(`${payload.description} was updated successfully.`));
        ctx.dispatch(new GetMetadataListItems(payload.listId));
        ctx.dispatch(new HideSpinner());
      }, (err) => {
        ctx.dispatch(new ShowErrorMessage(err));
      })
    );
  }


  @Action(RemoveMetadataListItem)
  removeMetaDataListsItem(ctx: StateContext<AdminMetadataStateModel>, { id, metadataListItemId }: RemoveMetadataListItem) {
    ctx.dispatch(new ShowSpinner());
    return this.metadataListItemsService.removeMetadataListItem(id, metadataListItemId)
      .pipe(
        tap(() => {
          ctx.dispatch(new ShowSuccessMessage('Metadata List Item was deleted successfully.'));
          ctx.dispatch(new GetMetadataListItems(id));
        }, (err) => ctx.dispatch(new ShowErrorMessage(err)))
      );
  }

  //#endregion

}
