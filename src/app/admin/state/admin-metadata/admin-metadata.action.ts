import { MetadataField } from 'src/app/core/models/entity/metadata-field';
import { MetadataListItem } from 'src/app/core/models/entity/metadata-list-item';
import { MetadataList } from 'src/app/core/models/entity/metadata-list';

//#region MetadataFields

export class GetMediaMetadataFields {
  static readonly type = '[Admin Metadata] GetMediaMetadataFields';
}

export class GetWorkPlanningMetadataFields {
  static readonly type = '[Admin Metadata] GetWorkPlanningMetadataFields';

  constructor(public hideSpinner?: boolean) { }
}

export class CreateMetadataField {
  static readonly type = '[Admin Metadata] CreateMetadataField';

  constructor(public payload: MetadataField) { }
}

export class UpdateMetadataField {
  static readonly type = '[Admin Metadata] UpdateMetadataField';

  constructor(public id: number, public payload: MetadataField, public hideMetadalistName?: boolean) { }
}

export class RemoveMetadataField {
  static readonly type = '[Admin Metadata] RemoveMetadataField';

  constructor(public id: number) { }
}

export class GetFieldTypes {
  static readonly type = '[Admin Metadata] GetFieldTypes';
}

export class GetMetadataModules {
  static readonly type = '[Admin Metadata] GetMetadataModules';
}

export class SortMetadataFields {
  static readonly type = '[Admin Metadata] SortMetadataFields';

  constructor(public metadataFields: MetadataField[], public isMedia: boolean = true) { }
}

//#endregion

//#region MetadataLists

export class GetActiveMetadataLists {
  static readonly type = '[Admin Metadata] GetActiveMetadataLists';
}

export class GetDisabledMetadataLists {
  static readonly type = '[Admin Metadata] GetDisabledMetadataLists';
}

export class GetMetadataList {
  static readonly type = '[Admin Metadata] GetMetadataList';

  constructor(public id: number) { }
}

export class CreateMetadataList {
  static readonly type = '[Admin Metadata] CreateMetadataList';

  constructor(public payload: MetadataList) { }
}

export class UpdateMetadataList {
  static readonly type = '[Admin Metadata] UpdateMetadataList';

  constructor(public id: number, public payload: MetadataList) { }
}

export class RemoveMetadataList {
  static readonly type = '[Admin Metadata] RemoveMetadataList';

  constructor(public id: number) { }
}

export class DisableMetadataList {
  static readonly type = '[Admin Metadata] DisableMetadataList';

  constructor(public list: MetadataList) { }
}

export class EnableMetadataList {
  static readonly type = '[Admin Metadata] EnableMetadataList';

  constructor(public list: MetadataList) { }
}

export class DisableMetadataLists {
  static readonly type = '[Admin Metadata] DisableMetadataLists';

  constructor(public lists: MetadataList[]) { }
}

export class EnableMetadataLists {
  static readonly type = '[Admin Metadata] EnableMetadataLists';

  constructor(public lists: MetadataList[]) { }
}

export class SetCurrentMetadataList {
  static readonly type = '[Admin Metadata] SetMetadataList';

  constructor(public payload: MetadataList) { }
}

export class SetCurrentMetadataListId {
  static readonly type = '[Admin Metadata] SetMetadataListId';

  constructor(public id: number) { }
}

//#endregion

//#region MetadataListItems

export class GetMetadataListItems {
  static readonly type = '[Admin Metadata] GetMetadataListItems';

  constructor(public listId: number) { }
}
export class GetMetaDataDependentListItems {
  static readonly type = '[Admin Metadata]  GetMetaDataDependentListItems';

  constructor(public listId: number) { }
}

export class ClearMetadataDependentListItems {
  static readonly type = '[Admin Metadata]  ClearMetadataDependentListItems';
}

export class ClearListItem {
  static readonly type = '[Admin Metadata]  ClearListItem';
}

export class GetMetadataListItem {
  static readonly type = '[Admin Metadata] GetMetadataListItem';

  constructor(public id: number) { }
}

export class CreateMetadataListItem {
  static readonly type = '[Admin Metadata] CreateMetadataListItem';

  constructor(public id: number, public payload: MetadataListItem) { }
}

export class UpdateMetadataListItem {
  static readonly type = '[Admin Metadata] UpdateMetadataListItem';

  constructor(public id: number, public payload: MetadataListItem) { }
}

export class RemoveMetadataListItem {
  static readonly type = '[Admin Metadata] RemoveMetadataListItem';

  constructor(public id: number, public metadataListItemId: number) { }
}

//#endregion
