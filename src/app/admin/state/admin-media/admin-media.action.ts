import { MetadataField } from 'src/app/core/models/entity/metadata-field';
import { Rule } from 'src/app/core/models/entity/rule';
import { Directory } from 'src/app/core/models/entity/directory';

//#region Uploads

export class GetUploadsHistory {
  static readonly type = '[Admin Media] GetUploadsHistory';
}

export class GetNewUploads {
  static readonly type = '[Admin Media] GetNewUploads';
}

export class GetUploadsInProgress {
  static readonly type = '[Admin Media] GetUploadsInProgress';
}

export class GetUploadDetail {
  static readonly type = '[Admin Media] GetUploadDetail';

  constructor(public id: number) { }
}

export class ApproveUploads {
  static readonly type = '[Admin Media] ApproveUploads';

  constructor(public uploadRequestIds: number[]) { }
}

export class RejectUploads {
  static readonly type = '[Admin Media] RejectUploads';

  constructor(public uploadRequestIds: number[]) { }
}

export class CancelUploads {
  static readonly type = '[Admin Media] CancelUploads';

  constructor(public uploadRequestIds: number[]) { }
}

//#endregion

//#region Media Folders

export class GetFolders {
  static readonly type = '[Admin media] GetFolders';
}

export class GetDirectories {
  static readonly type = '[Admin Media] GetDirectories';
}

export class UpdateFolder {
  static readonly type = '[Admin Media] UpdateFolder';

  constructor(public id: number, public payload: Directory) { }
}

export class RemoveFolder {
  static readonly type = '[Admin Media] RemoveFolder';

  constructor(public id: number) { }
}

export class GetFolder {
  static readonly type = '[Admin media] GetFolder';

  constructor(public folderId: number) { }
}

export class GetFolderMetadataFields {
  static readonly type = '[Admin media] GetFolderMetadataFields';

  constructor(public folderId: number) { }
}

export class AddFolderMetadataField {
  static readonly type = '[Admin media] AddFolderMetadataField';

  constructor(public folderId: number, public payload: MetadataField) { }
}

export class UpdateFolderMetadataField {
  static readonly type = '[Admin media] UpdateFolderMetadataField';

  constructor(public folderId: number, public payload: MetadataField) { }
}

export class DeleteFolderMetadataField {
  static readonly type = '[Admin media] DeleteFolderMetadataField';

  constructor(public folderId: number) { }
}

//#endregion

//#region Rules

export class GetRules {
  static readonly type = '[Admin Media Rules] GetRules';
}

export class GetRule {
  static readonly type = '[Admin Media Rules] GetRule';

  constructor(public id: number) { }
}

export class CreateRule {
  static readonly type = '[Admin Media Rules] CreateRule';

  constructor(public payload: Rule) { }
}

export class UpdateRule {
  static readonly type = '[Admin Media Rules] UpdateRule';

  constructor(public id: number, public payload: Rule) { }
}

export class DeleteRule {
  static readonly type = '[Admin Media Rules] DeleteRule';

  constructor(public id: number) { }
}

export class ClearCurrentRule {
  static readonly type = '[Admin Media Rules] ClearCurrentRule';
}

//#endregion

