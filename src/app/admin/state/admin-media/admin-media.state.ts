import { State, Selector, Action, StateContext } from '@ngxs/store';
import {
  GetUploadsHistory, GetUploadDetail, GetNewUploads, ApproveUploads, RejectUploads, GetFolder, GetDirectories, RemoveFolder,
  UpdateFolder, GetRules, GetRule, CreateRule, UpdateRule, DeleteRule, ClearCurrentRule,
  CancelUploads,
  GetUploadsInProgress
} from './admin-media.action';
import { DateService } from 'src/app/core/services/business/dates/date.service';
import { HideSpinner, ShowErrorMessage, ShowSuccessMessage, ShowSpinner } from 'src/app/state/app.actions';
import { FolderStructure } from 'src/app/core/models/entity/folder-structure';
import { DirectoryDataService } from 'src/app/core/services/data/directory/directory.data.service';
import { UploadRequestsService } from 'src/app/core/services/business/upload-requests/upload-requests.service';
import { UploadRequest } from 'src/app/core/models/entity/upload-request';
import { Rule } from 'src/app/core/models/entity/rule';
import { tap } from 'rxjs/internal/operators/tap';
import { map } from 'rxjs/internal/operators/map';
import { RulesService } from 'src/app/core/services/business/rules/rules.service';
import { Directory } from 'src/app/core/models/entity/directory';

export class AdminMediaStateModel {
  //uploads
  newUploads: UploadRequest[];
  uploadsHistory: UploadRequest[];
  currentUpload: UploadRequest;
  uploadsInProgress: UploadRequest[];

  // folders
  folderStructures: FolderStructure[];
  folders: Directory[];
  currentFolder: Directory;

  //rules
  rules: Rule[];
  currentRule: Rule;
  currentRuleId: number;
}


@State<AdminMediaStateModel>({
  name: 'adminMedia',
  defaults: {
    // uploads
    newUploads: [],
    uploadsHistory: [],
    uploadsInProgress: [],
    currentUpload: null,

    // folders
    folderStructures: [],
    folders: [],
    currentFolder: null,

    // rules
    rules: [],
    currentRule: null,
    currentRuleId: 0,
  }
})

export class AdminMediaState {

  //#region Uploads

  @Selector()
  static getUploadHistory(state: AdminMediaStateModel) {
    return state.uploadsHistory;
  }

  @Selector()
  static getCurrentUpload(state: AdminMediaStateModel) {
    return state.currentUpload;
  }

  @Selector()
  static getNewUploads(state: AdminMediaStateModel) {
    return state.newUploads;
  }

  @Selector()
  static getUploadsInProgress(state: AdminMediaStateModel) {
    return state.uploadsInProgress;
  }

  //#endregion

  //#region Folders

  @Selector()
  static getMediaFolderStructures(state: AdminMediaStateModel) {
    return state.folderStructures;
  }

  @Selector()
  static getDirectories(state: AdminMediaStateModel) {
    return state.folders;
  }

  @Selector()
  static getCurrentFolder(state: AdminMediaStateModel) {
    return state.currentFolder;
  }

  //#endregion

  //#region Rules

  @Selector()
  static getRules(state: AdminMediaStateModel) {
    return state.rules;
  }

  @Selector()
  static getCurrentRule(state: AdminMediaStateModel) {
    return state.currentRule;
  }

  @Selector()
  static getCurrentRuleId(state: AdminMediaStateModel) {
    return state.currentRuleId;
  }

  //#endregion

  constructor(private dateService: DateService, private foldersDataService: DirectoryDataService,
    private uploadRequestsService: UploadRequestsService, private rulesService: RulesService) { }

  //#region Uploads

  @Action(GetUploadsHistory)
  getUploadsHistory(ctx: StateContext<AdminMediaStateModel>) {
    return this.uploadRequestsService.getUploadHistory()
      .pipe(
        tap(history => {
          history.map(item => item.createdOnString = this.dateService.formatToString(item.createdOn, 'MMM DD, YYYY'));
          const state = ctx.getState();
          ctx.setState({
            ...state,
            uploadsHistory: history
          });
        })
      );
  }

  @Action(GetNewUploads)
  getNewUploads(ctx: StateContext<AdminMediaStateModel>) {
    ctx.dispatch(new ShowSpinner());
    return this.uploadRequestsService.getNewUploads()
      .pipe(
        tap(newUploads => {
          const state = ctx.getState();
          ctx.setState({
            ...state,
            newUploads: newUploads
          });
          ctx.dispatch(new HideSpinner());
        }, (err) => ctx.dispatch(new ShowErrorMessage(err)))
      );
  }

  @Action(GetUploadsInProgress)
  getUploadsInProgress(ctx: StateContext<AdminMediaStateModel>) {
    ctx.dispatch(new ShowSpinner());
    return this.uploadRequestsService.getUploadsInProgress()
      .pipe(
        tap(uploadsInProgress => {
          uploadsInProgress.map(item => item.createdOnString = this.dateService.formatToString(item.createdOn, 'MMM DD, YYYY'));
          const state = ctx.getState();
          ctx.setState({
            ...state,
            uploadsInProgress: uploadsInProgress
          });
          ctx.dispatch(new HideSpinner());
        }, (err) => ctx.dispatch(new ShowErrorMessage(err.error)))
      );
  }

  @Action(GetUploadDetail)
  getUploadRequest({ getState, setState }: StateContext<AdminMediaStateModel>, { id }: GetUploadDetail) {
    return this.uploadRequestsService.getUpload(id)
      .pipe(
        map(upload => {
          const state = getState();
          setState({
            ...state,
            currentUpload: upload
          });
        })
      );
  }

  @Action(ApproveUploads)
  approveUpload(ctx: StateContext<AdminMediaStateModel>, { uploadRequestIds }: ApproveUploads) {
    ctx.dispatch(new ShowSpinner());
    const lastRequestId = uploadRequestIds[uploadRequestIds.length - 1];
    uploadRequestIds.forEach(id => {
      return this.uploadRequestsService.approveUpload(id)
        .then(() => {
          if (id === lastRequestId) {
            ctx.dispatch(new ShowSuccessMessage('Request(s) were successfully approved.'));
            ctx.dispatch(new GetNewUploads());
          }
        }, (err) => ctx.dispatch(new ShowErrorMessage(err)));
    });
  }

  @Action(RejectUploads)
  rejectUpload(ctx: StateContext<AdminMediaStateModel>, { uploadRequestIds }: RejectUploads) {
    ctx.dispatch(new ShowSpinner());
    const lastRequestId = uploadRequestIds[uploadRequestIds.length - 1];
    uploadRequestIds.forEach(id => {
      return this.uploadRequestsService.rejectUpload(id)
        .then(() => {
          if (id === lastRequestId) {
            ctx.dispatch(new ShowSuccessMessage('Request(s) were successfully rejected.'));
            ctx.dispatch(new GetNewUploads());
          }
        }, (err) => ctx.dispatch(new ShowErrorMessage(err)));
    });
  }

  @Action(CancelUploads)
  cancelUpload(ctx: StateContext<AdminMediaStateModel>, { uploadRequestIds }: CancelUploads) {
    ctx.dispatch(new ShowSpinner());
    const lastRequestId = uploadRequestIds[uploadRequestIds.length - 1];
    uploadRequestIds.forEach(id => {
      return this.uploadRequestsService.cancelUpload(id)
        .then(() => {
          if (id === lastRequestId) {
            ctx.dispatch(new ShowSuccessMessage('Request(s) were successfully canceled.'));
            ctx.dispatch(new GetNewUploads());
          }
        }, (err) => ctx.dispatch(new ShowErrorMessage(err)));
    });
  }

  //#endregion

  //#region Folders

  @Action(UpdateFolder)
  updateDirectory(ctx: StateContext<AdminMediaStateModel>, { id, payload }: UpdateFolder) {
    ctx.dispatch(new ShowSpinner());
    return this.foldersDataService.updateDirectory(id, payload)
      .pipe(
        tap(() => {
          ctx.dispatch(new HideSpinner());
          ctx.dispatch(new ShowSuccessMessage('Folder was successfully updated.'));
        }, (err) => ctx.dispatch(new ShowErrorMessage(err)))
      );
  }

  @Action(RemoveFolder)
  removeDirectory(ctx: StateContext<AdminMediaStateModel>, { id }: RemoveFolder) {
    ctx.dispatch(new ShowSpinner());
    return this.foldersDataService.removeDirectory(id)
      .pipe(
        tap(() => {
          ctx.dispatch(new HideSpinner());
          ctx.dispatch(new ShowSuccessMessage('Folder was successfully removed.'));
        }, (err) => ctx.dispatch(new ShowErrorMessage(err)))
      );
  }

  @Action(GetDirectories)
  getDirectories(ctx: StateContext<AdminMediaStateModel>) {
    return this.foldersDataService.getFolders().pipe(
      tap(directories => {
        const state = ctx.getState();
        ctx.setState({
          ...state,
          folders: directories
        });
      }, error => ctx.dispatch(new ShowErrorMessage(error)))
    );
  }

  @Action(GetFolder)
  getFolder(ctx: StateContext<AdminMediaStateModel>, { folderId }: GetFolder) {
    return this.foldersDataService.getFolder(folderId).pipe(
      tap(folder => {
        const state = ctx.getState();
        ctx.setState({
          ...state,
          currentFolder: folder
        });
      }, error => ctx.dispatch(new ShowErrorMessage(error)))
    );
  }

  //#endregion

  //#region Rules

  @Action(GetRules)
  getRules(ctx: StateContext<AdminMediaStateModel>) {
    ctx.dispatch(new ShowSpinner());
    return this.rulesService.getRules().pipe(
      tap(rules => {
        const state = ctx.getState();
        ctx.setState({
          ...state,
          rules: rules
        });
        ctx.dispatch(new HideSpinner());
      }, (err) => ctx.dispatch(new ShowErrorMessage(err)))
    );
  }

  @Action(GetRule)
  getRule(ctx: StateContext<AdminMediaStateModel>, { id }: GetRule) {
    ctx.dispatch(new ShowSpinner());
    return this.rulesService.getRule(id).pipe(
      tap(rule => {
        const state = ctx.getState();
        ctx.setState({
          ...state,
          currentRule: rule
        });
        ctx.dispatch(new HideSpinner());
      }, (err) => ctx.dispatch(new ShowErrorMessage(err)))
    );
  }

  @Action(CreateRule)
  createRule(ctx: StateContext<AdminMediaStateModel>, { payload }: CreateRule) {
    ctx.dispatch(new ShowSpinner());
    return this.rulesService.createRule(payload).pipe(
      tap(ruleId => {
        const state = ctx.getState();
        ctx.setState({
          ...state,
          currentRuleId: ruleId
        });
        ctx.dispatch(new ShowSuccessMessage(`Rule was saved successfully.`));
        ctx.dispatch(new HideSpinner());
        return ruleId;
      }, (err) => ctx.dispatch(new ShowErrorMessage(err)))
    );
  }

  @Action(UpdateRule)
  updateRule(ctx: StateContext<AdminMediaStateModel>, { id, payload }: UpdateRule) {
    ctx.dispatch(new ShowSpinner());
    return this.rulesService.updateRule(id, payload).pipe(
      tap(() => {
        ctx.dispatch(new ShowSuccessMessage(`Rule was updated successfully.`));
        ctx.dispatch(new HideSpinner());
      }, (err) => ctx.dispatch(new ShowErrorMessage(err)))
    );
  }

  @Action(DeleteRule)
  deleteRule(ctx: StateContext<AdminMediaStateModel>, { id }: DeleteRule) {
    ctx.dispatch(new ShowSpinner());
    return this.rulesService.deleteRule(id).pipe(
      tap(() => {
        ctx.dispatch(new GetRules());
        ctx.dispatch(new HideSpinner());
      }, (err) => ctx.dispatch(new ShowErrorMessage(err)))
    );
  }

  @Action(ClearCurrentRule)
  clearCurrentRule(ctx: StateContext<AdminMediaStateModel>) {
    const state = ctx.getState();
    ctx.setState({
      ...state,
      currentRule: null
    })
  }

  //#endregion
}
