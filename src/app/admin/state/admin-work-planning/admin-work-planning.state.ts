import { State, Selector, StateContext, Action } from '@ngxs/store';
import {
  GetWorkTemplateGroups, UpdateWorkTemplate, CreateWorkTemplate, ClearCurrentWorkTemplate, SetCurrentWorkTemplateId, DeleteWorkTemplateProcess, DeleteWorkTemplateStep, GetCurrentWorkTemplate, SetCurrentWorkTemplateGroupId,
  CreateWorkTemplateProcess, UpdateWorkTemplateProcess, SetWorkTemplateProcesses, ClearWorkTemplateProcesses, ClearWorkTemplateSteps, CreateWorkTemplateStep, UpdateWorkTemplateStep, SetWorkTemplateSteps, SetCurrentWorkTemplateProcess,
  SetCurrentWorkTemplateStep, ClearCurrentWorkTemplateStep, ClearCurrentWorkTemplateProcess, GetWorkTemplateHistory, ActivateWorkTemplate, GetWorkTemplateGroup, SetCurrentWorkTemplate, ClearCurrentWorkTemplateGroup,
  SetWorkTemplates, DownloadWorkTemplate, GetWorkSets, CreateWorkSet, UpdateWorkSet, ImportWorkTemplate, ClearWorkSet, CreateWorkItem, ClearWorkItem, CreateWorkSetSchedule, ClearWorkSetSchedule, GetWorkSetProcesses, ClearWorkSetProcesses, UpdateWorkSetAsset, GetWorkSetHistory, SetCurrentWorkSetId,
  SetCurrentWorkSetItems, SetWorkTemplateGroup, SetCurrentWorkSetSchedule, UpdateWorkSetSchedule, SetCurrentWorkSetTemplateId, UpdateWorkItem, DeletWorkItem, DownloadWorkItem, ImportWorkSetItem, GetFrequencyTypes, GetWorkSetTemplateGroups, GetWorkTemplateMetadataFields, GetWorkSetAssets,
  GetCustomFrequencyTypes, SortWorkTemplateSteps, ClearWorkSetAssets, UpdateWorkTemplateFields, SetWorkTemplateVersions, GetWorkSet, ClearWorkSetHistory, GetWorkSetCycles, UpdateWorkSetCycle, OpenWorkSetCycle, PublishWorkSetCycle, DeleteWorkSetCycle, SetActiveWorkSetTab, SetActiveWorkTemplateTab
} from './admin-work-planning.actions';
import { ShowSuccessMessage, HideSpinner, ShowErrorMessage, ShowSpinner } from 'src/app/state/app.actions';
import { WorkTemplate, WorkTemplateProcess, WorkTemplateStep, WorkTemplateVersion, WorkTemplateHistory, WorkGroup } from 'src/app/core/models/entity/work-template';
import { tap } from 'rxjs/internal/operators/tap';
import { WorkTemplatesDataService } from 'src/app/core/services/data/work-templates/work-templates.data.service';
import { WorkTemplatesService } from 'src/app/core/services/business/work-templates/work-templates.service';
import { WorkTemplateStatus } from 'src/app/core/enum/work-template-status';
import { WorkSet, WorkSetSchedule } from 'src/app/core/models/entity/work-set';
import { WorkSetsDataService } from 'src/app/core/services/data/work-sets/work-sets.data.service';
import { WorkSetsService } from 'src/app/core/services/business/work-sets/work-sets.service';
import { WorkItem } from 'src/app/core/models/entity/work-item';
import { MetadataField } from 'src/app/core/models/entity/metadata-field';
import { MetadataFieldsService } from 'src/app/core/services/business/metadata-fields/metadata-fields.service';
import { Lookup } from 'src/app/core/models/entity/lookup';
import { LookupService } from 'src/app/core/services/business/lookup/lookup.service';
import { MetadataListService } from 'src/app/core/services/business/metadata-list/metadata-list.service';
import { MetadataListItem } from 'src/app/core/models/entity/metadata-list-item';
import { WorkSetCycle } from 'src/app/core/models/entity/work-set-cycle';
import { WorkSetHistory } from 'src/app/core/models/entity/work-set-history';

export class AdminWorkPlanningStateModel {

  //#region WORK TEMPLATES MODULE

  templateGroups: WorkTemplate[];
  templates: WorkTemplate[];
  currentTemplate: WorkTemplate;
  currentTemplateId: number;
  currentGroupId: number;
  processes: WorkTemplateProcess[];
  currentProcess: WorkTemplateProcess;
  hasSteps: boolean;
  steps: WorkTemplateStep[];
  currentStep: WorkTemplateStep;
  workTemplateAssets: MetadataField[];
  workTemplateVersions: WorkTemplateVersion[];
  workTemplateHistory: WorkTemplateHistory[];
  totalWorkTemplateHistory: number;
  isReadOnly: boolean;

  //#endregion

  //#region WORK SETS MODULE

  workSets: WorkSet[];
  currentWorkSetId: number;
  currentWorkSet: WorkSet;
  workSetAssets: MetadataListItem[];
  currentWorkSetSchedule: WorkSetSchedule;
  frequencyTypes: Lookup[];
  customFrequencyTypes: Lookup[];
  workSetProcesses: WorkTemplateProcess[];
  workSetCycles: WorkSetCycle[];
  totalWorkSetCycles: number;
  currentWorkSetCycle: WorkSetCycle;
  workItems: WorkItem[];
  workSetHistory: WorkSetHistory[];
  totalWorkSetHistory: number;
  workSetTemplates: WorkTemplate[];
  currentWorkSetTemplateId: number;
  activeWorkTemplateTab: string;
  activeWorksetTab: string;

  //#endregion

}

@State<AdminWorkPlanningStateModel>({
  name: 'adminWorkPlanning',
  defaults: {
    //#region WORK TEMPLATES MODULE

    templateGroups: [],
    templates: [],
    currentTemplate: null,
    currentTemplateId: null,
    currentGroupId: null,
    processes: [],
    currentProcess: null,
    hasSteps: false,
    steps: [],
    currentStep: null,
    workTemplateAssets: [],
    workTemplateVersions: [],
    workTemplateHistory: [],
    totalWorkTemplateHistory: null,
    isReadOnly: false,

    //#endregion

    //#region WORK SETS MODULE

    workSets: [],
    currentWorkSetId: null,
    currentWorkSet: null,
    workSetAssets: [],
    currentWorkSetSchedule: null,
    frequencyTypes: [],
    customFrequencyTypes: [],
    workSetCycles: [],
    totalWorkSetCycles: null,
    currentWorkSetCycle: null,
    workSetProcesses: [],
    workItems: [],
    workSetHistory: [],
    totalWorkSetHistory: null,
    workSetTemplates: [],
    currentWorkSetTemplateId: null,
    activeWorkTemplateTab: null,
    activeWorksetTab: null

    //#endregion
  }
})

export class AdminWorkPlanningState {

  //#region S E L E C T O R S

  //#region WORK TEMPLATES MODULE

  @Selector()
  static getWorkTemplateGroups(state: AdminWorkPlanningStateModel) {
    return state.templateGroups;
  }

  @Selector()
  static isReadOnly(state: AdminWorkPlanningStateModel) {
    return state.isReadOnly;
  }

  //#region Work Templates

  @Selector()
  static getWorkTemplates(state: AdminWorkPlanningStateModel) {
    return state.templates;
  }

  @Selector()
  static getCurrentTemplate(state: AdminWorkPlanningStateModel) {
    return state.currentTemplate;
  }

  @Selector()
  static getCurrentTemplateId(state: AdminWorkPlanningStateModel) {
    return state.currentTemplateId;
  }

  @Selector()
  static getCurrentGroupId(state: AdminWorkPlanningStateModel) {
    return state.currentGroupId;
  }

  @Selector()
  static getWorkTemplateAssets(state: AdminWorkPlanningStateModel) {
    return state.workTemplateAssets;
  }

  //#endregion

  //#region Work Template Process

  @Selector()
  static getProcesses(state: AdminWorkPlanningStateModel) {
    return state.processes;
  }

  @Selector()
  static getCurrentProcess(state: AdminWorkPlanningStateModel) {
    return state.currentProcess;
  }

  @Selector()
  static hasSteps(state: AdminWorkPlanningStateModel) {
    return state.hasSteps;
  }

  //#endregion

  //#region Work Template Step

  @Selector()
  static getSteps(state: AdminWorkPlanningStateModel) {
    return state.steps;
  }

  @Selector()
  static getCurrentStep(state: AdminWorkPlanningStateModel) {
    return state.currentStep;
  }

  //#endregion

  //#region Work Template Version

  @Selector()
  static getWorkTemplateVersions(state: AdminWorkPlanningStateModel) {
    return state.workTemplateVersions;
  }

  //#endregion

  //#region Work Template History

  @Selector()
  static getWorkTemplateHistory(state: AdminWorkPlanningStateModel) {
    return state.workTemplateHistory;
  }

  @Selector()
  static getTotalWorkTemplateHistory(state: AdminWorkPlanningStateModel) {
    return state.totalWorkTemplateHistory;
  }

  //#endregion

  //#endregion

  //#region WORK SETS MODULE

  //#region Work Set

  @Selector()
  static getWorkSets(state: AdminWorkPlanningStateModel) {
    return state.workSets;
  }

  @Selector()
  static getWorkSetTemplates(state: AdminWorkPlanningStateModel) {
    return state.workSetTemplates;
  }

  @Selector()
  static getCurrentWorkSetTemplateId(state: AdminWorkPlanningStateModel) {
    return state.currentWorkSetTemplateId;
  }

  @Selector()
  static getCurrentWorkSet(state: AdminWorkPlanningStateModel) {
    return state.currentWorkSet;
  }

  @Selector()
  static getCurrentWorkSetId(state: AdminWorkPlanningStateModel) {
    return state.currentWorkSetId;
  }

  @Selector()
  static getWorkSetAssets(state: AdminWorkPlanningStateModel) {
    return state.workSetAssets;
  }

  //#endregion

  //#region Schedule

  @Selector()
  static getCurrentSchedule(state: AdminWorkPlanningStateModel) {
    return state.currentWorkSetSchedule;
  }

  @Selector()
  static getFrequencyTypes(state: AdminWorkPlanningStateModel) {
    return state.frequencyTypes;
  }

  @Selector()
  static getCustomFrequencyTypes(state: AdminWorkPlanningStateModel) {
    return state.customFrequencyTypes;
  }

  @Selector()
  static getWorkSetProcesses(state: AdminWorkPlanningStateModel) {
    return state.workSetProcesses;
  }

  @Selector()
  static getActiveWorkTemplateTab(state: AdminWorkPlanningStateModel) {
    return state.activeWorkTemplateTab;
  }

  @Selector()
  static getActiveWorksetTab(state: AdminWorkPlanningStateModel) {
    return state.activeWorksetTab;
  }

  //#endregion

  //#region Work Set Cycle

  @Selector()
  static getWorkSetCycles(state: AdminWorkPlanningStateModel) {
    return state.workSetCycles;
  }

  @Selector()
  static getTotalWorkSetCycles(state: AdminWorkPlanningStateModel) {
    return state.totalWorkSetCycles;
  }

  @Selector()
  static getCurrentWorkSetCycle(state: AdminWorkPlanningStateModel) {
    return state.currentWorkSetCycle;
  }

  //#endregion

  //#region WorkItem

  @Selector()
  static getWorkItems(state: AdminWorkPlanningStateModel) {
    return state.workItems;
  }

  //#endregion

  //#region Work Set History

  @Selector()
  static getWorkSetHistory(state: AdminWorkPlanningStateModel) {
    return state.workSetHistory;
  }

  @Selector()
  static getTotalWorkSetHistory(state: AdminWorkPlanningStateModel) {
    return state.totalWorkSetHistory;
  }

  //#endregion

  //#endregion

  //#endregion S E L E C T O R S

  constructor(private workSetsService: WorkSetsService, private workSetsDataService: WorkSetsDataService, private workTemplatesService: WorkTemplatesService,
    private workTemplatesDataService: WorkTemplatesDataService, private metadataFieldsService: MetadataFieldsService, private metadataListService: MetadataListService,
    private lookupService: LookupService) { }

  //#region A C T I O N S

  //#region WORK TEMPLATES MODULE

  //#region Work Template Group

  @Action(GetWorkTemplateGroups)
  getWorkTemplateGroups(ctx: StateContext<AdminWorkPlanningStateModel>, { status }: GetWorkTemplateGroups) {
    ctx.dispatch(new ShowSpinner());
    return this.workTemplatesService.getGroups(status)
      .pipe(
        tap(templates => {
          const state = ctx.getState();
          ctx.setState({
            ...state,
            templateGroups: templates
          });
          ctx.dispatch(new HideSpinner());
        }, (err) => ctx.dispatch(new ShowErrorMessage(err)))
      );
  }

  @Action(GetWorkTemplateGroup)
  getWorkGroup(ctx: StateContext<AdminWorkPlanningStateModel>, { groupId }: GetWorkTemplateGroup) {
    ctx.dispatch(new ShowSpinner());
    return this.workTemplatesService.getGroup(groupId)
      .pipe(
        tap(group => {
          ctx.dispatch(new SetWorkTemplateGroup(group));
        }, err => ctx.dispatch(new ShowErrorMessage(err)))
      );
  }

  @Action(SetWorkTemplateGroup)
  setWorkGroup(ctx: StateContext<AdminWorkPlanningStateModel>, { group }: SetWorkTemplateGroup) {
    ctx.dispatch(new SetWorkTemplates(group.workTemplates));
    ctx.dispatch(new SetWorkTemplateVersions(group.workTemplateVersions));
  }

  @Action(ClearCurrentWorkTemplateGroup)
  clearCurrentWorkGroup(ctx: StateContext<AdminWorkPlanningStateModel>) {
    const state = ctx.getState();
    ctx.setState({
      ...state,
      currentTemplate: null,
      processes: [],
      steps: [],
      workTemplateVersions: [{ version: 1, status: WorkTemplateStatus.Draft }],
      workTemplateHistory: [],
      isReadOnly: false
    });
  }

  //#endregion

  //#region Work Template

  @Action(SetWorkTemplates)
  setWorkTemplates(ctx: StateContext<AdminWorkPlanningStateModel>, { templates }: SetWorkTemplates) {
    const state = ctx.getState();
    ctx.setState({
      ...state,
      templates: templates
    });
    const template = templates.find(t => t.status === WorkTemplateStatus.Draft);
    ctx.dispatch(new SetCurrentWorkTemplate(template));
  }

  @Action(SetCurrentWorkTemplate)
  async setCurrentWorkTemplate(ctx: StateContext<AdminWorkPlanningStateModel>, { template, version }: SetCurrentWorkTemplate) {
    ctx.dispatch(new ShowSpinner());
    const state = ctx.getState();
    const templates = state.templates;
    const currentTemplate = !version ? template : templates.find(t => t.version === version);

    ctx.setState({
      ...state,
      currentTemplate: currentTemplate,
      isReadOnly: currentTemplate.status !== WorkTemplateStatus.Draft
    });

    if (currentTemplate) {
      ctx.dispatch(new SetCurrentWorkTemplateId(currentTemplate.id));
      await ctx.dispatch(new SetWorkTemplateProcesses(currentTemplate.processes)).toPromise();
    }
    ctx.dispatch(new HideSpinner());
  }

  @Action(GetCurrentWorkTemplate)
  getCurrentWorkTemplate(ctx: StateContext<AdminWorkPlanningStateModel>, { groupId, version }: GetCurrentWorkTemplate) {
    ctx.dispatch(new ShowSpinner());
    return this.workTemplatesService.getTemplate(groupId, version)
      .pipe(
        tap(template => {
          const state = ctx.getState();
          ctx.setState({
            ...state,
            currentTemplate: template
          });
          // Set Current Process
          if (template) {
            ctx.dispatch(new SetCurrentWorkTemplateId(template.id));
            ctx.dispatch(new SetWorkTemplateProcesses(template.processes));
          }
          ctx.dispatch(new HideSpinner());
        }, (err) => ctx.dispatch(new ShowErrorMessage(err)))
      );
  }

  @Action(GetWorkTemplateMetadataFields)
  getWorkTemplateMetadataFields(ctx: StateContext<AdminWorkPlanningStateModel>) {
    ctx.dispatch(new ShowSpinner());
    return this.metadataFieldsService.getMetadataFields().pipe(
      tap(fields => {
        const state = ctx.getState();
        ctx.setState({
          ...state,
          workTemplateAssets: fields
        });
        ctx.dispatch(new HideSpinner())
      })
    );
  }

  @Action(CreateWorkTemplate)
  createWorkTemplate(ctx: StateContext<AdminWorkPlanningStateModel>, { payload }: CreateWorkTemplate) {
    ctx.dispatch(new ShowSpinner());
    return this.workTemplatesService.createTemplate(payload)
      .pipe(
        tap(workGroupId => {
          ctx.dispatch(new SetCurrentWorkTemplateGroupId(workGroupId));
          ctx.dispatch(new ShowSuccessMessage(`${payload.name} was saved successfully.`));
          ctx.dispatch(new HideSpinner());
        }, (err) => ctx.dispatch(new ShowErrorMessage(err)))
      );
  }

  @Action(UpdateWorkTemplate)
  updateWorkTemplate(ctx: StateContext<AdminWorkPlanningStateModel>, { payload, id }: UpdateWorkTemplate) {
    return this.workTemplatesDataService.updateTemplate(id, payload)
      .pipe(
        tap(() => {
          const state = ctx.getState();
          ctx.dispatch(new GetWorkTemplateGroup(state.currentGroupId));
          ctx.dispatch(new ShowSuccessMessage('Work Template was updated successfully.'));
          ctx.dispatch(new HideSpinner());
        }, (err) => ctx.dispatch(new ShowErrorMessage(err)))
      );
  }

  @Action(SetCurrentWorkTemplateId)
  setCurrentWorkTemplateId({ getState, setState }: StateContext<AdminWorkPlanningStateModel>, { id }: SetCurrentWorkTemplateId) {
    const state = getState();
    return setState({
      ...state,
      currentTemplateId: id,
    });
  }

  @Action(SetCurrentWorkTemplateGroupId)
  setCurrentWorkGroupId({ getState, setState }: StateContext<AdminWorkPlanningStateModel>, { groupId }: SetCurrentWorkTemplateGroupId) {
    const state = getState();
    return setState({
      ...state,
      currentGroupId: groupId
    });
  }

  @Action(ClearCurrentWorkTemplate)
  clearCurrentWorkTemplate({ getState, setState }: StateContext<AdminWorkPlanningStateModel>) {
    const state = getState();
    return setState({
      ...state,
      currentTemplate: null,
    });
  }

  @Action(ImportWorkTemplate)
  importWorkTemplate(ctx: StateContext<AdminWorkPlanningStateModel>, { file }: ImportWorkTemplate) {
    ctx.dispatch(new ShowSpinner());
    const state = ctx.getState();
    const { currentGroupId } = state;
    return this.workTemplatesService.importTemplate(currentGroupId, file)
      .pipe(
        tap((group: WorkGroup) => {
          const response = this.workTemplatesService.processWorkGroup(group);
          ctx.dispatch(new SetWorkTemplateGroup(response));
          ctx.dispatch(new ShowSuccessMessage(`Configuration was imported successfully.`));
          ctx.dispatch(new HideSpinner());
        }, (err) => ctx.dispatch(new ShowErrorMessage(err)))
      );
  }

  @Action(ActivateWorkTemplate)
  activateWorkTemplate(ctx: StateContext<AdminWorkPlanningStateModel>, { id, version, comment }: ActivateWorkTemplate) {
    ctx.dispatch(new ShowSpinner());
    return this.workTemplatesService.activateTemplate(id, comment)
      .pipe(
        tap(() => {
          const state = ctx.getState();
          const currentGroupId = state.currentGroupId;
          ctx.dispatch(new GetWorkTemplateGroup(currentGroupId));
          ctx.dispatch(new ShowSuccessMessage(`Version ${version} was activated.`));
          ctx.dispatch(new HideSpinner());
        }, (err) => ctx.dispatch(new ShowErrorMessage(err)))
      );
  }

  @Action(DownloadWorkTemplate)
  downloadWorkTemplate(ctx: StateContext<AdminWorkPlanningStateModel>, { id }: DownloadWorkTemplate) {
    ctx.dispatch(new ShowSpinner());
    const state = ctx.getState();
    const template = state.templates.find(t => t.id === id);
    return this.workTemplatesService.downloadTemplate(id, template)
      .pipe(
        tap(() => {
          ctx.dispatch(new HideSpinner());
        }, (err) => ctx.dispatch(new ShowErrorMessage(err)))
      );
  }

  //#endregion

  //#region Work Template Fields

  @Action(UpdateWorkTemplateFields)
  updateWorkTemplateFields(ctx: StateContext<AdminWorkPlanningStateModel>, { templateId, fields }: UpdateWorkTemplateFields) {
    ctx.dispatch(new ShowSpinner());
    return this.workTemplatesService.updateFields(templateId, fields)
      .pipe(
        tap(() => {
          ctx.dispatch(new ShowSuccessMessage("Fields updated successfully."));
          const state = ctx.getState();
          ctx.dispatch(new GetWorkTemplateGroup(state.currentGroupId));
          ctx.dispatch(new HideSpinner());
        }, (err) => ctx.dispatch(new ShowErrorMessage(err)))
      )
  }

  //#endregion

  //#region Work Template Process

  @Action(SetWorkTemplateProcesses)
  async setWorkTemplateProcesses(ctx: StateContext<AdminWorkPlanningStateModel>, { processes }: SetWorkTemplateProcesses) {
    const state = ctx.getState();
    if (processes.length === 0) {
      ctx.dispatch(new ClearWorkTemplateSteps());
    }
    const firstProcessWithSteps = processes.find(process => process.steps.length > 0);
    ctx.setState({
      ...state,
      processes: processes,
      hasSteps: firstProcessWithSteps ? true : false
    });
    // Set current work
    if (processes.length > 0) {
      const currentProcess = state.currentProcess || processes[0];
      const updatedProcess = processes.find(p => p.id === currentProcess.id);
      await ctx.dispatch(new SetCurrentWorkTemplateProcess(updatedProcess)).toPromise();
    } else {
      ctx.dispatch(new ClearWorkTemplateSteps());
    }
  }

  @Action(SetCurrentWorkTemplateProcess)
  async setCurrentWorkTemplateProcess(ctx: StateContext<AdminWorkPlanningStateModel>, { process }: SetCurrentWorkTemplateProcess) {
    const state = ctx.getState();
    if (!process) return;
    ctx.setState({
      ...state,
      currentProcess: process
    });
    // Set current work steps
    await ctx.dispatch(new SetWorkTemplateSteps(process.steps)).toPromise();
  }

  @Action(ClearWorkTemplateProcesses)
  clearProcesses(ctx: StateContext<AdminWorkPlanningStateModel>) {
    const state = ctx.getState();
    ctx.setState({
      ...state,
      processes: []
    });
    ctx.dispatch(new ClearCurrentWorkTemplateProcess());
  }

  @Action(ClearCurrentWorkTemplateProcess)
  clearCurrentProcess(ctx: StateContext<AdminWorkPlanningStateModel>) {
    const state = ctx.getState();
    ctx.setState({
      ...state,
      currentProcess: null
    });
    ctx.dispatch(new ClearWorkTemplateSteps());
  }

  @Action(CreateWorkTemplateProcess)
  createWorkTemplateProcess(ctx: StateContext<AdminWorkPlanningStateModel>, { process }: CreateWorkTemplateProcess) {
    ctx.dispatch(new ShowSpinner());
    this.workTemplatesService.createProcess(process)
      .then(() => {
        const state = ctx.getState();
        ctx.dispatch(new GetWorkTemplateGroup(state.currentGroupId));
        ctx.dispatch(new ShowSuccessMessage(`${process.name} was successfully created.`));
      }, error => ctx.dispatch(new ShowErrorMessage(error)));
  }

  @Action(UpdateWorkTemplateProcess)
  updateWorkTemplateProcess(ctx: StateContext<AdminWorkPlanningStateModel>, { id, process }: UpdateWorkTemplateProcess) {
    ctx.dispatch(new ShowSpinner());
    this.workTemplatesService.updateProcess(id, process)
      .then(() => {
        const state = ctx.getState();
        ctx.dispatch(new GetWorkTemplateGroup(state.currentGroupId));
        ctx.dispatch(new ShowSuccessMessage(`${process.name} was successfully updated.`));
      }, error => ctx.dispatch(new ShowErrorMessage(error)));
  }

  @Action(DeleteWorkTemplateProcess)
  deleteWorkTemplateProcess(ctx: StateContext<AdminWorkPlanningStateModel>, { process }: DeleteWorkTemplateProcess) {
    ctx.dispatch(new ShowSpinner());
    this.workTemplatesService.deleteProcess(process.id, process.templateId)
      .then(() => {
        const state = ctx.getState();
        ctx.dispatch(new GetWorkTemplateGroup(state.currentGroupId));
        ctx.dispatch(new ShowSuccessMessage(`${process.name} was successfully deleted.`));
      }, (errorResponse: any) => ctx.dispatch(new ShowErrorMessage(errorResponse)));
  }

  //#endregion

  //#region Work Template Step

  @Action(SetWorkTemplateSteps)
  async setWorkTemplateSteps(ctx: StateContext<AdminWorkPlanningStateModel>, { steps }: SetWorkTemplateSteps) {
    const state = ctx.getState();
    ctx.setState({
      ...state,
      steps: steps
    });
    if (steps.length > 0) {
      const firstStep = steps[0];
      await ctx.dispatch(new SetCurrentWorkTemplateStep(firstStep)).toPromise();
    }
  }

  @Action(SetCurrentWorkTemplateStep)
  setCurrentWorkTemplateStep(ctx: StateContext<AdminWorkPlanningStateModel>, { step }: SetCurrentWorkTemplateStep) {
    const state = ctx.getState();
    ctx.setState({
      ...state,
      currentStep: step
    });
  }

  @Action(ClearWorkTemplateSteps)
  clearSteps(ctx: StateContext<AdminWorkPlanningStateModel>) {
    const state = ctx.getState();
    ctx.setState({
      ...state,
      steps: []
    });
  }

  @Action(ClearCurrentWorkTemplateStep)
  clearCurrentStep(ctx: StateContext<AdminWorkPlanningStateModel>) {
    const state = ctx.getState();
    ctx.setState({
      ...state,
      currentStep: null
    });
  }

  @Action(CreateWorkTemplateStep)
  createStep(ctx: StateContext<AdminWorkPlanningStateModel>, { step }: CreateWorkTemplateStep) {
    ctx.dispatch(new ShowSpinner());
    this.workTemplatesService.createStep(step)
      .then(() => {
        const state = ctx.getState();
        ctx.dispatch(new GetWorkTemplateGroup(state.currentGroupId));
        ctx.dispatch(new ShowSuccessMessage(`${step.name} was successfully created.`));
      }, error => ctx.dispatch(new ShowErrorMessage(error)));
  }

  @Action(UpdateWorkTemplateStep)
  updateStep(ctx: StateContext<AdminWorkPlanningStateModel>, { step }: UpdateWorkTemplateStep) {
    ctx.dispatch(new ShowSpinner());
    this.workTemplatesService.updateStep(step)
      .then(() => {
        const state = ctx.getState();
        ctx.dispatch(new GetWorkTemplateGroup(state.currentGroupId));
        ctx.dispatch(new ShowSuccessMessage(`${step.name} was successfully updated.`));
      }, error => ctx.dispatch(new ShowErrorMessage(error)));
  }

  @Action(DeleteWorkTemplateStep)
  deleteStep(ctx: StateContext<AdminWorkPlanningStateModel>, { step }: DeleteWorkTemplateStep) {
    ctx.dispatch(new ShowSpinner());
    this.workTemplatesService.deleteStep(step)
      .then(() => {
        const state = ctx.getState();
        ctx.dispatch(new GetWorkTemplateGroup(state.currentGroupId));
        ctx.dispatch(new ShowSuccessMessage(`${step.name} was successfully deleted.`));
      }, error => ctx.dispatch(new ShowErrorMessage(error)));
  }

  @Action(SortWorkTemplateSteps)
  sortSteps(ctx: StateContext<AdminWorkPlanningStateModel>, { processId, steps }: SortWorkTemplateSteps) {
    ctx.dispatch(new ShowSpinner());
    const payload = steps.map((item, index) => Object.assign({}, item, { order: index }));
    this.workTemplatesService.sortSteps(processId, payload)
      .then(() => {
        const state = ctx.getState();
        ctx.dispatch(new GetWorkTemplateGroup(state.currentGroupId));
        ctx.dispatch(new ShowSuccessMessage(`Steps were successfully sorted.`));
      }, error => ctx.dispatch(new ShowErrorMessage(error)));
  }

  //#endregion

  //#region Work Template Version

  @Action(SetWorkTemplateVersions)
  setWorkTemplateVersions(ctx: StateContext<AdminWorkPlanningStateModel>, { versions }: SetWorkTemplateVersions) {
    const state = ctx.getState();
    ctx.setState({
      ...state,
      workTemplateVersions: versions
    });
  }

  //#endregion

  //#region Work Template History

  @Action(GetWorkTemplateHistory)
  setWorkTemplateHistory(ctx: StateContext<AdminWorkPlanningStateModel>, { workTemplateId, pageNumber, pageSize }: GetWorkTemplateHistory) {
    ctx.dispatch(new ShowSpinner());
    return this.workTemplatesService.getHistory(workTemplateId, pageNumber, pageSize)
      .then(response => {
        const state = ctx.getState();
        const history = response.data;
        ctx.setState({
          ...state,
          workTemplateHistory: history,
          totalWorkTemplateHistory: response.pagination.total
        });
        ctx.dispatch(new HideSpinner());
      }, (err) => ctx.dispatch(new ShowErrorMessage(err)));
  }

  //#endregion

  //#endregion

  //#region WORK SETS MODULE

  //#region Work Set

  @Action(GetWorkSets)
  getWorkSets(ctx: StateContext<AdminWorkPlanningStateModel>) {
    ctx.dispatch(new ShowSpinner());
    return this.workSetsService.getSets()
      .pipe(
        tap(sets => {
          const state = ctx.getState();
          ctx.setState({
            ...state,
            workSets: sets
          });
          ctx.dispatch(new HideSpinner());
        }, (err) => ctx.dispatch(new ShowErrorMessage(err)))
      );
  }

  @Action(GetWorkSet)
  getWorkSet(ctx: StateContext<AdminWorkPlanningStateModel>, { id }: GetWorkSet) {
    ctx.dispatch(new ShowSpinner());
    return this.workSetsService.getSet(id)
      .pipe(
        tap(model => {
          const state = ctx.getState();
          const workSet = model.data;
          ctx.setState({
            ...state,
            currentWorkSet: workSet
          });
          ctx.dispatch(new SetCurrentWorkSetItems(workSet.workItems));
          ctx.dispatch(new SetCurrentWorkSetSchedule(workSet.schedule));
          ctx.dispatch(new HideSpinner());
        }, (err) => {
          ctx.dispatch(new ClearWorkSet());
          ctx.dispatch(new ShowErrorMessage(err));
        })
      );
  }


  @Action(SetCurrentWorkSetTemplateId)
  setCurrentWorkSetTemplateId(ctx: StateContext<AdminWorkPlanningStateModel>, { id }: SetCurrentWorkSetTemplateId) {
    const state = ctx.getState();
    ctx.setState({
      ...state,
      currentWorkSetTemplateId: id
    });
  }

  @Action(SetActiveWorkTemplateTab)
  setActiveWorkTemplateTab(ctx: StateContext<AdminWorkPlanningStateModel>, { url }: SetActiveWorkTemplateTab) {
    const state = ctx.getState();
    return ctx.setState({
      ...state,
      activeWorkTemplateTab: url
    });
  }

  @Action(SetActiveWorkSetTab)
  setActiveWorksetTab(ctx: StateContext<AdminWorkPlanningStateModel>, { url }: SetActiveWorkSetTab) {
    const state = ctx.getState();
    return ctx.setState({
      ...state,
      activeWorksetTab: url
    });
  }

  @Action(GetWorkSetTemplateGroups)
  getWorkSetTemplateGroups(ctx: StateContext<AdminWorkPlanningStateModel>) {
    ctx.dispatch(new ShowSpinner());
    return this.workTemplatesService.getGroups()
      .pipe(
        tap(templateGroups => {
          const state = ctx.getState();
          ctx.setState({
            ...state,
            workSetTemplates: templateGroups
          });
          ctx.dispatch(new HideSpinner());
        }, (err) => ctx.dispatch(new ShowErrorMessage(err)))
      );
  }

  @Action(SetCurrentWorkSetId)
  setCurrentWorkSetId({ getState, setState }: StateContext<AdminWorkPlanningStateModel>, { id }: SetCurrentWorkSetId) {
    const state = getState();
    return setState({
      ...state,
      currentWorkSetId: id,
    });
  }

  @Action(ClearWorkSet)
  clearCurrentWorkSet(ctx: StateContext<AdminWorkPlanningStateModel>) {
    const state = ctx.getState();
    ctx.setState({
      ...state,
      currentWorkSet: null,
      currentWorkSetId: null
    });
    ctx.dispatch(new ClearWorkSetSchedule());
    ctx.dispatch(new ClearWorkSetHistory());
  }

  @Action(CreateWorkSet)
  createWorkSet(ctx: StateContext<AdminWorkPlanningStateModel>, { payload }: CreateWorkSet) {
    ctx.dispatch(new ShowSpinner());
    return this.workSetsService.createSet(payload)
      .pipe(
        tap(id => {
          ctx.dispatch(new SetCurrentWorkSetId(id));
          ctx.dispatch(new ShowSuccessMessage(`${payload.name} was saved successfully.`));
          ctx.dispatch(new HideSpinner());
        }, (err) => ctx.dispatch(new ShowErrorMessage(err)))
      );
  }

  @Action(UpdateWorkSet)
  updateWorkSet(ctx: StateContext<AdminWorkPlanningStateModel>, { payload, id }: UpdateWorkSet) {
    return this.workSetsService.updateSet(id, payload)
      .pipe(
        tap(() => {
          ctx.dispatch(new ShowSuccessMessage('Work Set was updated successfully.'));
          ctx.dispatch(new HideSpinner());
        }, (err) => ctx.dispatch(new ShowErrorMessage(err)))
      );
  }

  //#endregion

  //#region Work Set Asset

  @Action(GetWorkSetAssets)
  getWorkSetAssets(ctx: StateContext<AdminWorkPlanningStateModel>, { metadataListId }: GetWorkSetAssets) {
    ctx.dispatch(new ShowSpinner());
    return this.metadataListService.getMetadataListItems(metadataListId)
      .pipe(
        tap(listItems => {
          const state = ctx.getState();
          ctx.setState({
            ...state,
            workSetAssets: listItems
          });
          ctx.dispatch(new HideSpinner());
        }, (err) => ctx.dispatch(new ShowErrorMessage(err)))
      );
  }

  @Action(ClearWorkSetAssets)
  clearWorkSetAssets(ctx: StateContext<AdminWorkPlanningStateModel>) {
    const state = ctx.getState();
    ctx.setState({
      ...state,
      workSetAssets: []
    });
  }

  @Action(UpdateWorkSetAsset)
  updateWorkSetAsset(ctx: StateContext<AdminWorkPlanningStateModel>, { id, payload }: UpdateWorkSetAsset) {
    ctx.dispatch(new ShowSpinner());
    return this.workSetsService.updateSetAsset(id, payload)
      .pipe(
        tap(() => {
          const state = ctx.getState();
          ctx.dispatch(new ShowSuccessMessage(`Work Set Assets was updated successfully.`));
          ctx.dispatch(new HideSpinner());
        }, (err) => ctx.dispatch(new ShowErrorMessage(err)))
      );
  }

  //#endregion

  //#region Work Set Schedule

  @Action(GetFrequencyTypes)
  getFrequencyType(ctx: StateContext<AdminWorkPlanningStateModel>) {
    ctx.dispatch(new ShowSpinner());
    const state = ctx.getState();
    return this.lookupService.getWorkSetFrequencyTypes()
      .pipe(
        tap(types => {
          ctx.dispatch(new HideSpinner());
          ctx.setState({
            ...state,
            frequencyTypes: types
          });
          ctx.dispatch(new GetCustomFrequencyTypes());
        }, err => ctx.dispatch(new ShowErrorMessage(err)))
      );
  }

  @Action(GetCustomFrequencyTypes)
  getCustomFrequencyType(ctx: StateContext<AdminWorkPlanningStateModel>) {
    ctx.dispatch(new ShowSpinner());
    const state = ctx.getState();
    return this.lookupService.getCustomWorkSetFrequencyTypes()
      .pipe(
        tap(types => {
          ctx.dispatch(new HideSpinner());
          ctx.setState({
            ...state,
            customFrequencyTypes: types,
          });
        }, err => ctx.dispatch(new ShowErrorMessage(err)))
      );
  }

  @Action(CreateWorkSetSchedule)
  createWorkSetSchedule(ctx: StateContext<AdminWorkPlanningStateModel>, { workSetId, payload }: CreateWorkSetSchedule) {
    ctx.dispatch(new ShowSpinner());
    return this.workSetsService.createSetSchedule(workSetId, payload)
      .pipe(
        tap(() => {
          const state = ctx.getState();
          ctx.dispatch(new ShowSuccessMessage(`Work Set Schedule was created successfully.`));
          ctx.dispatch(new HideSpinner());
        }, (err) => ctx.dispatch(new ShowErrorMessage(err)))
      );
  }

  @Action(ClearWorkSetSchedule)
  clearWorkSetSchedule(ctx: StateContext<AdminWorkPlanningStateModel>) {
    const state = ctx.getState();
    ctx.setState({
      ...state,
      currentWorkSetSchedule: null
    });
  }

  @Action(SetCurrentWorkSetSchedule)
  setCurrentWorkSetSchedule({ getState, setState }: StateContext<AdminWorkPlanningStateModel>, { payload }: SetCurrentWorkSetSchedule) {
    const state = getState();
    const schedule = payload.cycleName ? payload : null;
    return setState({
      ...state,
      currentWorkSetSchedule: schedule
    });
  }

  @Action(UpdateWorkSetSchedule)
  updateWorkSetSchedule(ctx: StateContext<AdminWorkPlanningStateModel>, { workSetId, payload }: UpdateWorkSetSchedule) {
    ctx.dispatch(new ShowSpinner());
    return this.workSetsService.updateSetSchedule(workSetId, payload)
      .pipe(
        tap(() => {
          const state = ctx.getState();
          ctx.dispatch(new ShowSuccessMessage(`Work Schedule was updated successfully.`));
          ctx.dispatch(new HideSpinner());
        }, (err) => ctx.dispatch(new ShowErrorMessage(err)))
      );
  }

  //#endregion

  //#region Work Set Cycles

  @Action(GetWorkSetCycles)
  getWorkSetCycles(ctx: StateContext<AdminWorkPlanningStateModel>, { workSetId, pageNumber, pageSize }: GetWorkSetCycles) {
    ctx.dispatch(new ShowSpinner());
    return this.workSetsService.getCycles(workSetId, pageNumber, pageSize)
      .then(model => {
        const state = ctx.getState();
        ctx.setState({
          ...state,
          workSetCycles: model.data,
          totalWorkSetCycles: model.pagination.total
        });
        ctx.dispatch(new HideSpinner());
      }, error => ctx.dispatch(new ShowErrorMessage(error)));
  }

  @Action(UpdateWorkSetCycle)
  updateWorkSetCycle(ctx: StateContext<AdminWorkPlanningStateModel>, { workSetId, cycle }: UpdateWorkSetCycle) {
    ctx.dispatch(new ShowSpinner());
    return this.workSetsService.updateCycle(workSetId, cycle)
      .then(() => {
        ctx.dispatch(new HideSpinner());
        ctx.dispatch(new ShowSuccessMessage("Cycle was successfully updated."));
      }, error => ctx.dispatch(new ShowErrorMessage(error)));
  }

  @Action(DeleteWorkSetCycle)
  deleteWorkSetCycle(ctx: StateContext<AdminWorkPlanningStateModel>, { workSetId, cycleId }: DeleteWorkSetCycle) {
    ctx.dispatch(new ShowSpinner());
    return this.workSetsService.deleteCycle(workSetId, cycleId)
      .then(() => {
        ctx.dispatch(new HideSpinner());
        ctx.dispatch(new ShowSuccessMessage("Cycle was successfully deleted."));
      }, error => ctx.dispatch(new ShowErrorMessage(error)));
  }

  @Action(OpenWorkSetCycle)
  openWorkSetCycle(ctx: StateContext<AdminWorkPlanningStateModel>, { workSetId, cycle }: OpenWorkSetCycle) {
    ctx.dispatch(new ShowSpinner());
    return this.workSetsService.openCycle(workSetId, cycle)
      .then(() => {
        ctx.dispatch(new HideSpinner());
        ctx.dispatch(new ShowSuccessMessage("Cycle was successfully opened."));
      }, error => ctx.dispatch(new ShowErrorMessage(error)));
  }

  @Action(PublishWorkSetCycle)
  publishWorkSetCycle(ctx: StateContext<AdminWorkPlanningStateModel>, { workSetId, cycleId }: PublishWorkSetCycle) {
    ctx.dispatch(new ShowSpinner());
    return this.workSetsService.publishCycle(workSetId, cycleId)
      .then(() => {
        ctx.dispatch(new HideSpinner());
        ctx.dispatch(new ShowSuccessMessage("Cycle was successfully published."));
      }, error => ctx.dispatch(new ShowErrorMessage(error)));
  }

  //#endregion

  //#region Work Set Items

  @Action(CreateWorkItem)
  createWorkItem(ctx: StateContext<AdminWorkPlanningStateModel>, { workSetId, payload }: CreateWorkItem) {
    ctx.dispatch(new ShowSpinner());
    return this.workSetsService.createSetItem(workSetId, payload)
      .pipe(
        tap(() => {
          ctx.dispatch(new ShowSuccessMessage(`Work Item was saved successfully.`));
          ctx.dispatch(new HideSpinner());
        }, (err) => ctx.dispatch(new ShowErrorMessage(err)))
      );
  }

  @Action(ClearWorkItem)
  clearWorkItem(ctx: StateContext<AdminWorkPlanningStateModel>) {
    const state = ctx.getState();
    ctx.setState({
      ...state,
      workItems: [],
    });
  }

  @Action(SetCurrentWorkSetItems)
  setCurrentWorkSetItems({ getState, setState }: StateContext<AdminWorkPlanningStateModel>, { payload }: SetCurrentWorkSetItems) {
    const state = getState();
    const items = payload ? payload : [];
    return setState({
      ...state,
      workItems: items
    });
  }

  @Action(UpdateWorkItem)
  updateWorkItem(ctx: StateContext<AdminWorkPlanningStateModel>, { workSetId, payload }: UpdateWorkItem) {
    ctx.dispatch(new ShowSpinner());
    return this.workSetsService.updateSetItem(workSetId, payload)
      .pipe(
        tap(() => {
          ctx.dispatch(new ShowSuccessMessage(`Work Item was saved successfully.`));
          ctx.dispatch(new HideSpinner());
        }, (err) => ctx.dispatch(new ShowErrorMessage(err)))
      );
  }

  @Action(DeletWorkItem)
  deleteWorkItem(ctx: StateContext<AdminWorkPlanningStateModel>, { workSetId, ItemId }: DeletWorkItem) {
    ctx.dispatch(new ShowSpinner());
    return this.workSetsService.deleteSetItem(workSetId, ItemId)
      .pipe(
        tap(() => {
          ctx.dispatch(new ShowSuccessMessage(`Work Item was deleted successfully.`));
          ctx.dispatch(new HideSpinner());
        }, (err) => ctx.dispatch(new ShowErrorMessage(err)))
      );
  }

  @Action(ImportWorkSetItem)
  importWorkSetItem(ctx: StateContext<AdminWorkPlanningStateModel>, { file }: ImportWorkSetItem) {
    ctx.dispatch(new ShowSpinner());
    const state = ctx.getState();
    const { currentWorkSetId: currentSetId } = state;
    return this.workSetsService.importWorkSetItem(currentSetId, file)
      .pipe(
        tap(() => {
          ctx.dispatch(new ShowSuccessMessage(`Configuration was imported successfully.`));
          ctx.dispatch(new HideSpinner());
        }, (err) => ctx.dispatch(new ShowErrorMessage(err)))
      );
  }

  @Action(DownloadWorkItem)
  downloadWorkItem(ctx: StateContext<AdminWorkPlanningStateModel>, { workSet }: DownloadWorkItem) {
    ctx.dispatch(new ShowSpinner());
    return this.workSetsService.downloadSetItem(workSet)
      .pipe(
        tap(() => {
          ctx.dispatch(new HideSpinner());
        }, (err) => ctx.dispatch(new ShowErrorMessage(err)))
      );
  }

  //#endregion

  //#region Work Set Process

  @Action(GetWorkSetProcesses)
  getWorkSetProcess(ctx: StateContext<AdminWorkPlanningStateModel>, { templateId }: GetWorkSetProcesses) {
    return this.workSetsService.getprocess(templateId)
      .pipe(
        tap(list => {
          const state = ctx.getState();
          ctx.setState({
            ...state,
            workSetProcesses: list
          });
        }, (err) => ctx.dispatch(new ShowErrorMessage(err)))
      );
  }

  @Action(ClearWorkSetProcesses)
  clearWorkSetProcesses(ctx: StateContext<AdminWorkPlanningStateModel>) {
    const state = ctx.getState();
    ctx.setState({
      ...state,
      workSetProcesses: [],
    });
  }

  //#endregion

  //#region Work Set History

  @Action(GetWorkSetHistory)
  getWorkSetHistory(ctx: StateContext<AdminWorkPlanningStateModel>, { workSetId, pageNumber, pageSize }: GetWorkSetHistory) {
    ctx.dispatch(new ShowSpinner());
    return this.workSetsService.getHistory(workSetId, pageNumber, pageSize)
      .then(response => {
        const state = ctx.getState();
        const list = response.data;
        ctx.setState({
          ...state,
          workSetHistory: list,
          totalWorkSetHistory: response.pagination.total
        });
        ctx.dispatch(new HideSpinner());
      }, (err) => ctx.dispatch(new ShowErrorMessage(err)));
  }

  @Action(ClearWorkSetHistory)
  clearWorkSetHistory(ctx: StateContext<AdminWorkPlanningStateModel>) {
    const state = ctx.getState();
    ctx.setState({
      ...state,
      workSetHistory: []
    });
  }

  //#endregion

  //#endregion

  //#endregion A C T I O N S
}
