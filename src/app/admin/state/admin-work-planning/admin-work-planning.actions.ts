import { WorkSet, WorkSetSchedule } from 'src/app/core/models/entity/work-set';
import { WorkTemplate, WorkTemplateProcess, WorkTemplateStep, WorkTemplateVersion, WorkGroup, WorkTemplateField } from 'src/app/core/models/entity/work-template';
import { WorkItem } from 'src/app/core/models/entity/work-item';
import { WorkAsset } from 'src/app/core/models/entity/work-set-asset';
import { WorkSetCycle } from 'src/app/core/models/entity/work-set-cycle';
import { Status } from 'src/app/core/enum/status.enum';

//#region WORK TEMPLATES MODULE

export class GetWorkTemplateGroups {
  static readonly type = '[Admin Work Planning] GetWorkTemplateGroups';

  constructor(public status?: string) { }
}

export class GetWorkTemplateGroup {
  static readonly type = '[Admin Work Planning] GetWorkTemplateGroup';

  constructor(public groupId: number) { }
}

export class SetWorkTemplateGroup {
  static readonly type = '[Admin Work Planning] SetWorkTemplateGroup';

  constructor(public group: WorkGroup) { }
}

export class ClearCurrentWorkTemplateGroup {
  static readonly type = '[Admin Work Planning] ClearCurrentWorkTemplateGroup';
}

export class SetCurrentWorkTemplateGroupId {
  static readonly type = '[Admin Work Planning] SetCurrentWorkTemplateGroupId';

  constructor(public groupId: any) { }
}

//#region Work Template

export class GetCurrentWorkTemplate {
  static readonly type = '[Admin Work Planning] GetCurrentWorkTemplate';

  constructor(public groupId: number, public version?: number) { }
}

export class SetWorkTemplates {
  static readonly type = '[Admin Work Planning] SetWorkTemplates';

  constructor(public templates: WorkTemplate[]) { }
}

export class SetCurrentWorkTemplate {
  static readonly type = '[Admin Work Planning] SetCurrentWorkTemplate';

  constructor(public template: WorkTemplate, public version?: number) { }
}

export class CreateWorkTemplate {
  static readonly type = '[Admin Work Planning] CreateWorkTemplate';

  constructor(public payload: WorkTemplate) { }
}

export class UpdateWorkTemplate {
  static readonly type = '[Admin Work Planning] UpdateWorkTemplate';

  constructor(public id: number, public payload: WorkTemplate) { }
}

export class ClearCurrentWorkTemplate {
  static readonly type = '[Admin Work Planning] ClearCurrentWorkTemplate';
}

export class SetCurrentWorkTemplateId {
  static readonly type = '[Admin Work Planning] SetCurrentWorkTemplateId';

  constructor(public id: number) { }
}


export class ImportWorkTemplate {
  static readonly type = '[Admin Work Planning] ImportWorkTemplate';

  constructor(public file: string) { }
}

export class ActivateWorkTemplate {
  static readonly type = '[Admin Work Planning] ActivateWorkTemplate';

  constructor(public id: number, public version: number, public comment?: string) { }
}

export class DownloadWorkTemplate {
  static readonly type = '[Admin Work Planning] DownloadWorkTemplate';

  constructor(public id: number) { }
}

//#endregion

//#region Work Template Fields

export class UpdateWorkTemplateFields {
  static readonly type = '[Admin Work Planning] UpdateWorkTemplateFields';

  constructor(public templateId: number, public fields: WorkTemplateField[]) { }
}

//#endregion

//#region Work Template Process

export class SetWorkTemplateProcesses {
  static readonly type = '[Admin Work Planning] SetWorkTemplateProcesses';

  constructor(public processes: WorkTemplateProcess[]) { }
}

export class SetCurrentWorkTemplateProcess {
  static readonly type = '[Admin Work Planning] SetCurrentWorkTemplateProcess';

  constructor(public process: WorkTemplateProcess) { }
}

export class ClearWorkTemplateProcesses {
  static readonly type = '[Admin Work Planning] ClearWorkTemplateProcesses';
}

export class ClearCurrentWorkTemplateProcess {
  static readonly type = '[Admin Work Planning] ClearCurrentWorkTemplateProcess';
}

export class GetWorkTemplateProcesses {
  static readonly type = '[Admin Work Planning] GetWorkTemplateProcesses';

  constructor(public workTemplateId?: number) { }
}

export class CreateWorkTemplateProcess {
  static readonly type = '[Admin Work Planning] CreateWorkTemplateProcess';

  constructor(public process: WorkTemplateProcess) { }
}

export class UpdateWorkTemplateProcess {
  static readonly type = '[Admin Work Planning] UpdateWorkTemplateProcess';

  constructor(public id: number, public process: WorkTemplateProcess) { }
}

export class DeleteWorkTemplateProcess {
  static readonly type = '[Admin Work Planning] DeleteWorkTemplateProcess';

  constructor(public process: WorkTemplateProcess) { }
}

//#endregion

//#region Work Template Step

export class SetWorkTemplateSteps {
  static readonly type = '[Admin Work Planning] SetWorkTemplateSteps';

  constructor(public steps: WorkTemplateStep[]) { }
}

export class SetCurrentWorkTemplateStep {
  static readonly type = '[Admin Work Planning] SetCurrentWorkTemplateStep';

  constructor(public step: WorkTemplateStep) { }
}

export class ClearWorkTemplateSteps {
  static readonly type = '[Admin Work Planning] ClearWorkTemplateSteps';
}

export class ClearCurrentWorkTemplateStep {
  static readonly type = '[Admin Work Planning] ClearCurrentWorkTemplateStep';
}

export class GetWorkTemplateMetadataFields {
  static readonly type = '[Admin Work Planning] GetWorkTemplateMetadataFields';
}

export class GetWorkTemplateSteps {
  static readonly type = '[Admin Work Planning] GetWorkTemplatesSteps';

  constructor(public processId: number) { }
}

export class CreateWorkTemplateStep {
  static readonly type = '[Admin Work Planning] CreateWorkTemplateStep';

  constructor(public step: WorkTemplateStep) { }
}

export class UpdateWorkTemplateStep {
  static readonly type = '[Admin Work Planning] UpdateWorkTemplateStep';

  constructor(public step: WorkTemplateStep) { }
}

export class DeleteWorkTemplateStep {
  static readonly type = '[Admin Work Planning] DeleteWorkTemplateStep';

  constructor(public step: WorkTemplateStep) { }
}

export class SortWorkTemplateSteps {
  static readonly type = '[Admin Work Planning] SortWorkTemplateSteps';

  constructor(public processId: number, public steps: WorkTemplateStep[]) { }
}

//#endregion

//#region Work Template Versions

export class SetWorkTemplateVersions {
  static readonly type = '[Admin Work Planning] GetWorkTemplateVersions';

  constructor(public versions: WorkTemplateVersion[]) { }
}

export class ClearWorkTemplateVersions {
  static readonly type = '[Admin Work Planning] ClearCurrentWorkTemplateVersions';
}

export class DownloadWorkTemplateVersion {
  static readonly type = '[Admin Work Planning] DownloadWorkTemplateVersion';

  constructor(public templateId: number) { }
}

export class ImportWorkTemplateVersion {
  static readonly type = '[Admin Work Planning] ImportWorkTemplateVersion';
}

//#endregion

//#region Work Template History

export class GetWorkTemplateHistory {
  static readonly type = '[Admin Work Planning] GetWorkTemplateHistory';

  constructor(public workTemplateId: number, public pageNumber: number, public pageSize: number) { }
}

export class ClearWorkTemplateHistory {
  static readonly type = '[Admin Work Planning] ClearCurrentWorkTemplateHistory';
}

//#endregion

//#endregion


//#region WORK SETS MODULE

//#region Work Set

export class GetWorkSets {
  static readonly type = '[Admin Work Planning] GetWorkSets';
}

export class GetWorkSet {
  static readonly type = '[Admin Work Planning] GetWorkSet';

  constructor(public id: number) { }
}

export class CreateWorkSet {
  static readonly type = '[Admin Work Planning] CreateWorkSet';

  constructor(public payload: WorkSet) { }
}

export class UpdateWorkSet {
  static readonly type = '[Admin Work Planning] UpdateWorkSet';

  constructor(public id: number, public payload: WorkSet) { }
}

export class ClearWorkSet {
  static readonly type = '[Admin Work Planning] ClearWorkSet';
}

export class ClearWorkSets {
  static readonly type = '[Admin Work Planning] ClearWorkSet';
}

export class SetCurrentWorkSetId {
  static readonly type = '[Admin Work Planning] SetCurrentWorkSetId';

  constructor(public id: number) { }
}

export class GetWorkSetTemplateGroups {
  static readonly type = '[Admin Work Planning] GetWorkSetTemplateGroups';
}

export class SetCurrentWorkSetTemplateId {
  static readonly type = '[Admin Work Planning] SetCurrentWorkSetTemplateId';

  constructor(public id: number) { }
}

export class SetActiveWorkTemplateTab {
  static readonly type = '[Admin Work Planning] SetActiveWorkTemplateTab';

  constructor(public url: string) { }
}

export class SetActiveWorkSetTab {
  static readonly type = '[Admin Work Planning] SetActiveWorkSetTab';

  constructor(public url: string) { }
}

//#endregion

//#region Work Set Asset

export class GetWorkSetAssets {
  static readonly type = '[Admin Work Planning] GetWorkSetAssets';

  constructor(public metadataListId: number) { }
}

export class ClearWorkSetAssets {
  static readonly type = '[Admin Work Planning] ClearWorkSetAssets';
}

export class UpdateWorkSetAsset {
  static readonly type = '[Admin Work Planning] UpdateWorkSetAsset';

  constructor(public id: number, public payload: WorkAsset) { }
}

//#endregion

//#region Work Set Schedule

export class CreateWorkSetSchedule {
  static readonly type = '[Admin Work Planning] CreateWorkSetSchedule';

  constructor(public workSetId: number, public payload: WorkSetSchedule) { }
}

export class SetCurrentWorkSetSchedule {
  static readonly type = '[Admin Work Planning] SetCurrentWorkSetSchedule';

  constructor(public payload: WorkSetSchedule) { }
}

export class UpdateWorkSetSchedule {
  static readonly type = '[Admin Work Planning] UpdateWorkSetSchedule';

  constructor(public workSetId: number, public payload: WorkSetSchedule) { }
}

export class ClearWorkSetSchedule {
  static readonly type = '[Admin Work Planning] ClearWorkSetSchedule';
}

export class GetFrequencyTypes {
  static readonly type = '[Admin Work Planning] GetFrequencyTypes';
}

export class GetCustomFrequencyTypes {
  static readonly type = '[Admin Work Planning] GetCustomFrequencyTypes';
}

//#endregion

//#region Work Set Cycles

export class GetWorkSetCycles {
  static readonly type = "[Admin Work Planning] GetWorkSetCycles";

  constructor(public workSetId: number, public pageNumber?: number, public pageSize?: number) { }
}

export class UpdateWorkSetCycle {
  static readonly type = "[Admin Work Planning] UpdateWorkSetCycle";

  constructor(public workSetId: number, public cycle: WorkSetCycle) { }
}

export class DeleteWorkSetCycle {
  static readonly type = "[Admin Work Planning] DeleteWorkSetCycle";

  constructor(public workSetId: number, public cycleId: number) { }
}

export class OpenWorkSetCycle {
  static readonly type = "[Admin Work Planning] OpenWorkSetCycle";

  constructor(public workSetId: number, public cycle: WorkSetCycle) { }
}

export class PublishWorkSetCycle {
  static readonly type = "[Admin Work Planning] PublishWorkSetCycle";

  constructor(public workSetId: number, public cycleId: number) { }
}

//#endregion

//#region Work Set Item

export class CreateWorkItem {
  static readonly type = '[Admin Work Planning] CreateWorkItem';

  constructor(public workSetId: number, public payload: WorkItem) { }
}

export class SetCurrentWorkSetItems {
  static readonly type = '[Admin Work Planning] SetCurrentWorkSetItems';

  constructor(public payload: WorkItem[]) { }
}

export class UpdateWorkItem {
  static readonly type = '[Admin Work Planning] UpdateWorkItem';

  constructor(public workSetId: number, public payload: WorkItem) { }
}

export class ClearWorkItem {
  static readonly type = '[Admin Work Planning] ClearWorkItem';
}

export class DownloadWorkItem {
  static readonly type = '[Admin Work Planning] DownloadWorkItem';

  constructor(public workSet: WorkSet) { }
}

export class ImportWorkSetItem {
  static readonly type = '[Admin Work Planning] ImportWorkSetItem';

  constructor(public file: string) { }
}

export class DeletWorkItem {
  static readonly type = '[Admin Work Planning] DeletWorkItem';

  constructor(public workSetId, public ItemId) { }
}

//#endregion

//#region Work Set Template-Process

export class GetWorkSetProcesses {
  static readonly type = '[Admin Work Planning] GetWorkSetProcess';
  constructor(public templateId: number) { }
}

export class ClearWorkSetProcesses {
  static readonly type = '[Admin Work Planning] GetWorkSetProcess';

}
//#endregion

//#region Work Set History

export class GetWorkSetHistory {
  static readonly type = '[Admin Work Planning] SetWorkSetHistory';

  constructor(public workSetId: number, public pageNumber?: number, public pageSize?: number) { }
}

export class ClearWorkSetHistory {
  static readonly type = '[Admin Work Planning] ClearWorkSetHistory';
}

//#endregion

//#endregion