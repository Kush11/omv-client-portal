import { Action, State, StateContext, Selector } from '@ngxs/store';
import { DirectoryService } from 'src/app/core/services/business/directory/directory.service';
import { ShowErrorMessage, ShowSpinner, HideSpinner } from 'src/app/state/app.actions';
import { GetFolders } from './admin-folders-action';
import { tap } from 'rxjs/operators';

export class AdminFolderStateModel {
  directories: any[];
  directoryMetadata: any[];
}

@State<AdminFolderStateModel>({
  name: 'AdminFolders',
  defaults: {
    //directories
    directories: [],
    directoryMetadata: []
  }
})

export class AdminFolderState {

  @Selector()
  static getDirectories(state: AdminFolderStateModel) {
    return state.directories;
  }



  constructor(private foldersService: DirectoryService) { }


  @Action(GetFolders)
  getDirectories(ctx: StateContext<AdminFolderStateModel>) {
    ctx.dispatch(new ShowSpinner());
    return this.foldersService.getFolders().pipe(
      tap(directories => {
        const state = ctx.getState();
        ctx.setState({
          ...state,
          directories: directories
        });
        ctx.dispatch(new HideSpinner());
      }, error => ctx.dispatch(new ShowErrorMessage(error)))
    );
  }
}