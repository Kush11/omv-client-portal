import { User } from 'src/app/core/models/entity/user';

export class GetUsers {
  static readonly type = '[Admin Users] GetUsers';

  constructor(public name?: string, public groupId?: number, public status?: number,
    public pageNumber?: number, public pageSize?: number) { }
}

export class GetActiveUsers {
  static readonly type = '[Admin Users] GetActiveUsers';

  constructor(public name?: string, public groupId?: number, public pageNumber?: number, public pageSize?: number) { }
}

export class ExportDisabledUsers {
  static readonly type = '[Admin Users] ExportDisabledUsers';

  constructor(public name?: string, public groupId?: number, public pageNumber?: number, public pageSize?: number) { }
}

export class ExportUnassignedUsers {
  static readonly type = '[Admin Users] ExportUnassignedUsers';

  constructor(public name?: string, public groupId?: number, public pageNumber?: number, public pageSize?: number) { }
}

export class ExportActiveUsers {
  static readonly type = '[Admin Users] ExportActiveUsers';

  constructor(public name?: string, public groupId?: number, public pageNumber?: number, public pageSize?: number) { }
}

export class GetDisabledUsers {
  static readonly type = '[Admin Users] GetDisabledUsers';

  constructor(public name?: string, public groupId?: number, public pageNumber?: number, public pageSize?: number) { }
}

export class GetUnassignedUsers {
  static readonly type = '[Admin Users] GetUnassignedUsers';

  constructor(public name?: string, public groupId?: number, public pageNumber?: number, public pageSize?: number) { }
}

export class GetUser {
  static readonly type = '[Admin Users] GetUser';

  constructor(public id: number) { }
}

export class InitializeUser {
  static readonly type = '[Admin Users] InitializeUser';
}

export class CreateUser {
  static readonly type = '[Admin Users] CreateUser';

  constructor(public payload: User) { }
}

export class UpdateUser {
  static readonly type = '[Admin Users] UpdateUser';

  constructor(public id: number, public payload: User) { }
}

export class DeleteUser {
  static readonly type = '[Admin Users] DeleteUser';

  constructor(public id: number, public payload: User) { }
}

export class DisableUser {
  static readonly type = '[Admin Users] DisableUser';

  constructor(public payload: User) { }
}

export class EnableUser {
  static readonly type = '[Admin Users] EnableUser';

  constructor(public payload: User) { }
}

export class DisableUsers {
  static readonly type = '[Admin Users] DisableUsers';

  constructor(public users: User[]) { }
}

export class EnableUsers {
  static readonly type = '[Admin Users] EnableUsers';

  constructor(public users: User[]) { }
}

export class GetUserGroups {
  static readonly type = '[Admin Users] GetUserGroups';

  constructor(public userId: number) { }
}

export class ClearUserGroups {
  static readonly type = '[Admin Users] ClearUserGroups';
}

export class UpdateUserGroups {
  static readonly type = '[Admin Users] UpdateUserGroups';

  constructor(public userid: number, public payload: number[]) { }
}

export class UpdateUsersGroups {
  static readonly type = '[Admin Users] UpdateUsersGroups';

  constructor(public userIds: number[], public groupIds: number[]) { }
}

export class SetCurrentUserId {
  static readonly type = '[Admin Users] SetCurrentUserId';

  constructor(public id: number) { }
}

export class SetSelectedUserIds {
  static readonly type = '[Admin Users] SetSelectedUsers';

  constructor(public id: number[]) { }
}

export class SearchUsers {
  static readonly type = '[Admin Users] SearchUsers';

  constructor(public name: string, public groupid: number) { }
}