import { State, Action, StateContext, Selector } from "@ngxs/store";
import { tap } from "rxjs/operators";
import { GetUsers, UpdateUser, EnableUser, DisableUser, GetUser, UpdateUserGroups, GetUserGroups, CreateUser, InitializeUser, ClearUserGroups, EnableUsers, DisableUsers, UpdateUsersGroups, GetActiveUsers, GetDisabledUsers, GetUnassignedUsers, ExportActiveUsers, ExportDisabledUsers, ExportUnassignedUsers } from "./admin-users.actions";
import { User } from "src/app/core/models/entity/user";
import { UserStatus } from "src/app/core/enum/user-status.enum";
import { Group } from "src/app/core/models/entity/group";
import { ShowSuccessMessage, ShowErrorMessage, ShowSpinner, HideSpinner } from 'src/app/state/app.actions';
import { DateService } from 'src/app/core/services/business/dates/date.service';
import { UsersService } from 'src/app/core/services/business/users/users.service';

export class AdminUserStateModel {
  users: User[];
  activeUsers: User[];
  unassignedUsers: User[];
  disabledUsers: User[];
  activeExportUsers: User[];
  unassignedExportUsers: User[];
  disabledExportUsers: User[];
  totalUsers: number;
  isProcessComplete: boolean;
  currentUserId: number;
  currentUser: User;
  groups: Group[];
}

const initialUser: User = {
  id: 0,
  displayName: '',
  firstName: '',
  lastName: '',
  userName: '',
  emailAddress: '',
  roleNames: [],
  status: 1
}


@State<AdminUserStateModel>({
  name: "adminUsers",
  defaults: {
    users: [],
    activeUsers: [],
    unassignedUsers: [],
    disabledUsers: [],
    activeExportUsers: [],
    unassignedExportUsers: [],
    disabledExportUsers: [],
    totalUsers: 0,
    isProcessComplete: false,
    currentUserId: null,
    currentUser: null,
    groups: []
  }
})
export class AdminUserState {

  // #region S E L E C T O R S

  @Selector()
  static getUsers(state: AdminUserStateModel) {
    return state.users;
  }

  @Selector()
  static getActiveUsers(state: AdminUserStateModel) {
    return state.activeUsers;
  }

  @Selector()
  static getUnassignedUsers(state: AdminUserStateModel) {
    return state.unassignedUsers;
  }

  @Selector()
  static getDisabledUsers(state: AdminUserStateModel) {
    return state.disabledUsers;
  }

  @Selector()
  static getActiveUsersForExport(state: AdminUserStateModel) {
    return state.activeExportUsers;
  }

  @Selector()
  static getUnassignedUsersForExport(state: AdminUserStateModel) {
    return state.unassignedExportUsers;
  }

  @Selector()
  static getDisabledUsersForExport(state: AdminUserStateModel) {
    return state.disabledExportUsers;
  }

  @Selector()
  static getTotalUsers(state: AdminUserStateModel) {
    return state.totalUsers;
  }

  @Selector()
  static getIsProcessComplete(state: AdminUserStateModel) {
    return state.isProcessComplete;
  }

  @Selector()
  static getCurrentUserId(state: AdminUserStateModel) {
    return state.currentUserId;
  }

  @Selector()
  static getCurrentUser(state: AdminUserStateModel) {
    return state.currentUser;
  }

  @Selector()
  static getGroups(state: AdminUserStateModel) {
    return state.groups;
  }

  //#endregion

  constructor(private usersService: UsersService) { }

  // #region A C T I O N S
  
  @Action(GetUsers)
  getUsers(ctx: StateContext<AdminUserStateModel>, { name, groupId, status, pageNumber, pageSize }: GetUsers) {
    ctx.dispatch(new ShowSpinner());
    return this.usersService.getUsers(name, groupId, status, pageNumber, pageSize)
      .pipe(
        tap(response => {
          let users = response.data;
          const state = ctx.getState();
          ctx.setState({
            ...state,
            users: users,
            totalUsers: response.total
          });
          ctx.dispatch(new HideSpinner());
        }, err => ctx.dispatch(new ShowErrorMessage(err)))
      );
  }

  @Action(GetActiveUsers)
  getActiveUsers(ctx: StateContext<AdminUserStateModel>, { name, groupId, pageNumber, pageSize }: GetActiveUsers) {
    ctx.dispatch(new ShowSpinner());
    const status = 1;
    return this.usersService.getUsers(name, groupId, status, pageNumber, pageSize)
      .pipe(
        tap(response => {
          let users = response.data;
          const state = ctx.getState();
          ctx.setState({
            ...state,
            activeUsers: users,
            totalUsers: response.total,
            isProcessComplete: false
          });
          ctx.dispatch(new HideSpinner());
        }, err => ctx.dispatch(new ShowErrorMessage(err)))
      );
  }

  @Action(GetDisabledUsers)
  getDisabledUsers(ctx: StateContext<AdminUserStateModel>, { name, groupId, pageNumber, pageSize }: GetDisabledUsers) {
    ctx.dispatch(new ShowSpinner());
    const status = 0;
    return this.usersService.getUsers(name, groupId, status, pageNumber, pageSize)
      .pipe(
        tap(response => {
          let users = response.data;
          const state = ctx.getState();
          ctx.setState({
            ...state,
            disabledUsers: users,
            totalUsers: response.total,
            isProcessComplete: false
          });
          ctx.dispatch(new HideSpinner());
        }, err => ctx.dispatch(new ShowErrorMessage(err)))
      );
  }

  @Action(GetUnassignedUsers)
  getUnassignedUsers(ctx: StateContext<AdminUserStateModel>, { name, groupId, pageNumber, pageSize }: GetUnassignedUsers) {
    ctx.dispatch(new ShowSpinner());
    const status = 2;
    return this.usersService.getUsers(name, groupId, status, pageNumber, pageSize)
      .pipe(
        tap(response => {
          let users = response.data.filter(x => !x.groups);
          const state = ctx.getState();
          ctx.setState({
            ...state,
            unassignedUsers: users,
            totalUsers: response.total,
            isProcessComplete: false
          });
          ctx.dispatch(new HideSpinner());
        }, err => ctx.dispatch(new ShowErrorMessage(err)))
      );
  }

  @Action(ExportActiveUsers)
  getActiveUsersForExport(ctx: StateContext<AdminUserStateModel>, { name, groupId, pageNumber, pageSize }: ExportActiveUsers) {
    ctx.dispatch(new ShowSpinner());
    const status = 1;
    return this.usersService.getUsersForExport(name, groupId, status, pageNumber, pageSize)
      .pipe(
        tap(response => {
          let users = response.data;
          const state = ctx.getState();
          ctx.setState({
            ...state,
            activeExportUsers: users,
            totalUsers: response.total,
            isProcessComplete: false
          });
          ctx.dispatch(new HideSpinner());
        }, err => ctx.dispatch(new ShowErrorMessage(err)))
      );
  }

  @Action(ExportDisabledUsers)
  getDisabledUsersForExport(ctx: StateContext<AdminUserStateModel>, { name, groupId, pageNumber, pageSize }: ExportDisabledUsers) {
    ctx.dispatch(new ShowSpinner());
    const status = 0;
    return this.usersService.getUsersForExport(name, groupId, status, pageNumber, pageSize)
      .pipe(
        tap(response => {
          let users = response.data;
          const state = ctx.getState();
          ctx.setState({
            ...state,
            disabledExportUsers: users,
            totalUsers: response.total,
            isProcessComplete: false
          });
          ctx.dispatch(new HideSpinner());
        }, err => ctx.dispatch(new ShowErrorMessage(err)))
      );
  }

  @Action(ExportUnassignedUsers)
  getUnassignedUsersForExport(ctx: StateContext<AdminUserStateModel>, { name, groupId, pageNumber, pageSize }: ExportUnassignedUsers) {
    ctx.dispatch(new ShowSpinner());
    const status = 2;
    return this.usersService.getUsersForExport(name, groupId, status, pageNumber, pageSize)
      .pipe(
        tap(response => {
          let users = response.data.filter(x => !x.groups);
          const state = ctx.getState();
          ctx.setState({
            ...state,
            unassignedExportUsers: users,
            totalUsers: response.total,
            isProcessComplete: false
          });
          ctx.dispatch(new HideSpinner());
        }, err => ctx.dispatch(new ShowErrorMessage(err)))
      );
  }

  @Action(GetUser)
  getUser(ctx: StateContext<AdminUserStateModel>, { id }: GetUser) {
    ctx.dispatch(new ShowSpinner());
    return this.usersService.getUser(id)
      .pipe(
        tap(user => {
          const state = ctx.getState();
          ctx.setState({
            ...state,
            currentUser: user ? user : null
          });
          ctx.dispatch(new HideSpinner());
        }, err => ctx.dispatch(new ShowErrorMessage(err)))
      );
  }

  @Action(InitializeUser)
  initializeUser({ getState, setState }: StateContext<AdminUserStateModel>) {
    const state = getState();
    setState({
      ...state,
      currentUser: initialUser
    });
  }

  @Action(GetUserGroups)
  getGroups(ctx: StateContext<AdminUserStateModel>, { userId }: GetUserGroups) {
    ctx.dispatch(new ShowSpinner());
    return this.usersService.getGroups(userId)
      .pipe(
        tap(groups => {
          const state = ctx.getState();
          ctx.setState({
            ...state,
            groups: groups
          });
          ctx.dispatch(new HideSpinner());
        }, err => ctx.dispatch(new ShowErrorMessage(err)))
      );
  }

  @Action(ClearUserGroups)
  clearGroups({ getState, setState }: StateContext<AdminUserStateModel>) {
    const state = getState();
    setState({
      ...state,
      groups: null
    });
  }

  @Action(CreateUser)
  createUser(ctx: StateContext<AdminUserStateModel>, { payload }: CreateUser) {
    ctx.dispatch(new ShowSpinner());
    return this.usersService.createUser(payload)
      .pipe(
        tap(result => {
          ctx.dispatch(new ShowSuccessMessage("User created successfully"));
          const user = result as User;
          const state = ctx.getState();
          ctx.setState({
            ...state,
            currentUserId: user.id
          });
          ctx.dispatch(new HideSpinner());
        }, err => ctx.dispatch(new ShowErrorMessage(err)))
      );
  }

  @Action(UpdateUser)
  updateUser(ctx: StateContext<AdminUserStateModel>, { id, payload }: UpdateUser) {
    ctx.dispatch(new ShowSpinner());
    return this.usersService.updateUser(id, payload)
      .pipe(
        tap(() => {
          ctx.dispatch(new ShowSuccessMessage("User updated successfully"));
          ctx.dispatch(new HideSpinner());
        }, err => ctx.dispatch(new ShowErrorMessage(err)))
      );
  }

  @Action(UpdateUserGroups)
  updateGroups(ctx: StateContext<AdminUserStateModel>, { userid, payload }: UpdateUserGroups) {
    return this.usersService.updateGroups(userid, payload)
      .then(() => {
        const state = ctx.getState();
        const user = state.currentUser;
        ctx.dispatch(new ShowSuccessMessage(`${user.displayName}'s groups were updated successfully.`));
      }, err => ctx.dispatch(new ShowErrorMessage(err)));
  }

  @Action(UpdateUsersGroups)
  updateUsersGroups(ctx: StateContext<AdminUserStateModel>, { userIds, groupIds }: UpdateUsersGroups) {
    const lastId = userIds[userIds.length - 1];
    userIds.forEach(id => {
      return this.usersService.updateGroups(id, groupIds, true)
        .then(() => {
          if (id === lastId) {
            ctx.dispatch(new ShowSuccessMessage("Users were assigned to groups successfully."));
            const state = ctx.getState();
            ctx.setState({
              ...state,
              isProcessComplete: true
            });
          }
        }, err => ctx.dispatch(new ShowErrorMessage(err)));
    })
  }

  @Action(DisableUser)
  disableUser(ctx: StateContext<AdminUserStateModel>, { payload }: DisableUser) {
    ctx.dispatch(new ShowSpinner());
    return this.usersService.disableUser(payload)
      .then(user => {
        ctx.dispatch(new HideSpinner());
        ctx.dispatch(new ShowSuccessMessage(`${user.displayName} was disabled successfully.`));
      }, err => ctx.dispatch(new ShowErrorMessage(err)));
  }

  @Action(EnableUser)
  enableUser(ctx: StateContext<AdminUserStateModel>, { payload }: EnableUser) {
    ctx.dispatch(new ShowSpinner());
    return this.usersService.enableUser(payload)
      .then(user => {
        ctx.dispatch(new HideSpinner());
        ctx.dispatch(new ShowSuccessMessage(`${user.displayName} was enabled successfully.`));
      }, err => ctx.dispatch(new ShowErrorMessage(err)));
  }
 
  @Action(EnableUsers)
  enableUsers(ctx: StateContext<AdminUserStateModel>, { users }: EnableUsers) {
    ctx.dispatch(new ShowSpinner());
    const lastUser = users[users.length - 1];
    users.forEach(user => {
      return this.usersService.enableUser(user)
        .then(() => {
          if (user.id === lastUser.id) {
            const message = users.length > 1 ? `Users were enabled successfully` : 'User was enabled successfully';
            ctx.dispatch(new ShowSuccessMessage(message));
            const state = ctx.getState();
            ctx.setState({
              ...state,
              isProcessComplete: true
            });
          }
        }, err => {
          ctx.dispatch(new ShowErrorMessage(err));
        });
    });
  }

  @Action(DisableUsers)
  disableUsers(ctx: StateContext<AdminUserStateModel>, { users }: DisableUsers) {
    ctx.dispatch(new ShowSpinner());
    const lastUser = users[users.length - 1];
    users.forEach(user => {
      return this.usersService.disableUser(user)
        .then(() => {
          if (user.id === lastUser.id) {
            const message = users.length > 1 ? `Users were disabled successfully` : 'User was disabled successfully';
            ctx.dispatch(new ShowSuccessMessage(message));
            const state = ctx.getState();
            ctx.setState({
              ...state,
              isProcessComplete: true
            });
          }
        }, err => {
          ctx.dispatch(new ShowErrorMessage(err));
        });
    });
  }

  //#endregion
}
