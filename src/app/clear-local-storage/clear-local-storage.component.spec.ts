import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClearLocalStorageComponent } from './clear-local-storage.component';

describe('ClearLocalStorageComponent', () => {
  let component: ClearLocalStorageComponent;
  let fixture: ComponentFixture<ClearLocalStorageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClearLocalStorageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClearLocalStorageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
