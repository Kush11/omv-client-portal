import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-clear-local-storage',
  templateUrl: './clear-local-storage.component.html',
  styleUrls: ['./clear-local-storage.component.css']
})
export class ClearLocalStorageComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    localStorage.removeItem('@@STATE');
  }
}
