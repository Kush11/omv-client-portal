import { DateService } from './../core/services/business/dates/date.service';
import { Component, ViewEncapsulation, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Store, Select } from '@ngxs/store';
import { ShowLeftNav, SetPageTitle, GetAllWidgets, GetUserDashboardWidgets, UpdateUserDashboardWidgets, ShowSpinner } from '../state/app.actions';
import { BaseComponent } from '../shared/base/base.component';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/internal/Observable';
import { AppState } from '../state/app.state';
import { SubSink } from 'subsink/dist/subsink';
import { Widget, UserWidget } from '../core/models/entity/widget';
import { ButtonComponent } from '@syncfusion/ej2-angular-buttons';
import { DialogComponent } from '@syncfusion/ej2-angular-popups';
import {
  Category, StackingColumnSeries, Legend, Chart, Tooltip, AccumulationChart, AccumulationDataLabel, AccumulationTooltip, IAccTooltipRenderEventArgs, IAccTextRenderEventArgs
} from '@syncfusion/ej2-angular-charts';
import { DashboardLayout, PanelModel } from '@syncfusion/ej2-layouts';
import { ModalComponent } from '../shared/modal/modal.component';
import { User } from '../core/models/entity/user';
declare const paginator: any;
Chart.Inject(Legend, StackingColumnSeries, Category, Tooltip);
AccumulationChart.Inject(AccumulationDataLabel, AccumulationTooltip);

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
  encapsulation: ViewEncapsulation.None
})

export class DashboardComponent extends BaseComponent implements OnInit, OnDestroy {

  @ViewChild('saveBtn') saveBtn: ButtonComponent;
  @ViewChild('cancelBtn') cancelBtn: ButtonComponent;
  @ViewChild('sectionDialog') sectionsDialogList: DialogComponent;
  @ViewChild('layoutDialog') layoutDialogList: DialogComponent;
  @ViewChild('confirmModal') confirmModal: ModalComponent;


  @Select(AppState.getUserDashboardWidgets) userDashboardSections$: Observable<UserWidget[]>;
  @Select(AppState.getLoggedInUser) currentUser$: Observable<User>;
  @Select(AppState.getAllWidgets) allWidgets$: Observable<Widget[]>;

  //#region varibles 
  args: any;
  public columns = 4;
  private index: number;
  private count = 1;
  private subs = new SubSink();
  private widgetNames: string[];
  changeTemplateMessage: string;
  public isCustomizable: boolean;
  private stackedColumnChart: Chart;
  public dashboard: DashboardLayout;
  private pieChart: AccumulationChart;
  public cellSpacing: number[] = [20, 20];
  public panel: any; public panelsData: any[]; private widgetContainer: any;
  public allowDragging = false; public allowResizing = false; public allowFloating = false;
  public widgetLabels: any[] = []; public allWidgets: any[] = []; private savedModel: any = [];
  private currentDashboardModel: any[];
  public liveStreamHistoryTableHeader = ['Camera Name', 'Folder Path', 'KPI Type', 'KPI Value', 'Reported'];
  public storageGridChartTableHeader = ['Region', 'File Size (TB)', 'File Count'];
  public workPlanningItemsTableHeader = ['Work Type', 'Total Items Complete', 'Total Items To Complete'];
  public workPlanningFindingsTableHeader = ['Work Type', 'Total Findings'];
  public workPlanningsTableHeader = ['Work Type', 'Total'];
  public liveStreamHistoryTableId = 'live-stream-kpi-table';
  public storageGridChartAllTimeId = 'media-file-all-time';
  public storageGridChartCurrentMonthId = 'media-file-current-month';
  public storageGridChartLastMonthId = 'media-file-last-month';
  public storageGridChartCurrentYearId = 'media-file-current-year';
  public storageGridChartLastYearId = 'media-file-last-year';
  public storagePieChartAllTimeId = '#spc-all-time';
  public storagePieChartCurrentMonthId = '#spc-current-month';
  public storagePieChartLastMonthId = '#spc-last-month';
  public storagePieChartCurrentYearId = '#spc-current-year';
  public storagePieChartLastYearId = '#spc-last-year';
  public uploadsHistoryBarChartAllTimeId = '#bar-chart-all-time';
  public uploadsHistoryBarChartCurrentMonthId = '#bar-chart-current-month';
  public uploadsHistoryBarChartLastMonthId = '#bar-chart-last-month';
  public uploadsHistoryBarChartCurrentYearId = '#bar-chart-current-year';
  public uploadsHistoryBarChartLastYearId = '#bar-chart-last-year';
  public workPlanningItemsId = 'work-planning-items';
  public workPlanningFindingsId = 'work-planning-findings';
  public workPlanningsId = 'work-planning-due';

  // predefined dashboard layout templates
  private templatesData: any = [
    {
      panel1: { sizeX: 1, sizeY: 1, row: 0, col: 0 },
      panel2: { sizeX: 1, sizeY: 1, row: 0, col: 1 },
      panel3: { sizeX: 1, sizeY: 1, row: 0, col: 2 },
      panel4: { sizeX: 1, sizeY: 1, row: 0, col: 3 },
      panel5: { sizeX: 2, sizeY: 1, row: 1, col: 0 },
      panel6: { sizeX: 1, sizeY: 1, row: 1, col: 2 },
      panel7: { sizeX: 1, sizeY: 1, row: 1, col: 3 },
      panel8: { sizeX: 4, sizeY: 1, row: 2, col: 0 }
    },
    {
      panel1: { sizeX: 1, sizeY: 1, row: 0, col: 0 },
      panel2: { sizeX: 1, sizeY: 1, row: 0, col: 1 },
      panel3: { sizeX: 1, sizeY: 1, row: 0, col: 2 },
      panel4: { sizeX: 1, sizeY: 1, row: 0, col: 3 },
      panel5: { sizeX: 2, sizeY: 1, row: 1, col: 0 },
      panel6: { sizeX: 2, sizeY: 1, row: 1, col: 2 },
      panel7: { sizeX: 4, sizeY: 1, row: 2, col: 0 },
    },
    {
      panel1: { sizeX: 1, sizeY: 1, row: 0, col: 0 },
      panel2: { sizeX: 1, sizeY: 1, row: 0, col: 1 },
      panel3: { sizeX: 1, sizeY: 1, row: 0, col: 2 },
      panel4: { sizeX: 1, sizeY: 1, row: 0, col: 3 },
      panel5: { sizeX: 1, sizeY: 1, row: 1, col: 0 },
      panel6: { sizeX: 1, sizeY: 1, row: 1, col: 1 },
      panel7: { sizeX: 2, sizeY: 1, row: 1, col: 2 },
      panel8: { sizeX: 4, sizeY: 1, row: 2, col: 0 }
    }
  ];

  // initialize X axis for bar column chart
  private primaryXAxis: object = {
    majorGridLines: { width: 0 },
    minorGridLines: { width: 0 },
    majorTickLines: { width: 0 },
    minorTickLines: { width: 0 },
    interval: 1,
    lineStyle: { width: 0 },
    valueType: 'Category',
    border: { width: 0 }
  };

  // initialize Y axis for bar column chart
  private primaryYAxis: object = {
    lineStyle: { width: 0 },
    majorTickLines: { width: 0 },
    majorGridLines: { width: 1 },
    minorGridLines: { width: 0 },
    minorTickLines: { width: 0 },
    labelFormat: '{value}',
  };
  private panels: any = this.templatesData;
  public sumOfPoints: number;
  public userFolderPath: string[];

  //#endregion

  constructor(protected store: Store, protected router: Router, private dateService: DateService) {
    super(store);
    this.store.dispatch(new ShowLeftNav(false));
    this.store.dispatch(new SetPageTitle('Dashboard'));
    this.disableFullScreen();
  }

  ngOnInit() {
    if (!this.tenantPermission.isPermittedDashboard) {
      this.router.navigate([`/unauthorize`]);
    } else {

      this.store.dispatch(new GetUserDashboardWidgets());
      this.store.dispatch(new GetAllWidgets());

      this.subs.add(
        this.allWidgets$
          .subscribe(widgets => {
            console.log('dashboard-component-ts ngOnInit - allWidgets ', widgets);
            this.allWidgets = widgets;
            if (this.allWidgets) { this.toggleWidgetsDisplay(this.allWidgets); }
          }),
        this.currentUser$
          .subscribe(user => {
            this.userFolderPath = user.folderPaths;
          }),
        this.userDashboardSections$
          .subscribe(sections => {
            console.log('dashboard-component-ts ngOnInit - userDashboardSections ', sections);
            const sect = JSON.parse(JSON.stringify(sections));
            this.panelsData = sections;
            this.buildDashboardLayout();

            if (this.panelsData) {
              setTimeout(() => {
                this.widgetNames = sect.map(wN => wN.name);
                // build user corresponding charts
                for (let i = 0; i < sect.length; i++) {
                  this.buildDashboardWidgets(sect[i]);
                }
              }, 100);
            }

            // this.addClickEventToListItems();
          }));
    }
  }

  private buildDashboardLayout() {
    if (this.dashboard) { this.dashboard.removeAll(); }
    // initialize dashboardlayout component
    this.dashboard = new DashboardLayout({
      cellSpacing: this.cellSpacing,
      allowFloating: this.allowFloating,
      allowResizing: this.allowResizing,
      allowDragging: this.allowDragging,
      columns: this.columns,
      panels: this.panelsData
    });
    // render initialized dashboardlayout
    this.dashboard.appendTo('#dashboard_layout');
  }

  buildDashboardWidgets(widget) {
    switch (widget.name) {
      case 'Storage-Pie-Chart : All Time':
        this.buildStoragePieChartWidget(widget.data, this.storagePieChartAllTimeId);
        break;
      case 'Storage-Pie-Chart : Current Month':
        this.buildStoragePieChartWidget(widget.data, this.storagePieChartCurrentMonthId);
        break;
      case 'Storage-Pie-Chart : Last Month':
        this.buildStoragePieChartWidget(widget.data, this.storagePieChartLastMonthId);
        break;
      case 'Storage-Pie-Chart : Current Year':
        this.buildStoragePieChartWidget(widget.data, this.storagePieChartCurrentYearId);
        break;
      case 'Storage-Pie-Chart : Last Year':
        this.buildStoragePieChartWidget(widget.data, this.storagePieChartLastYearId);
        break;
      case 'Uploads-History-Bar-Chart : All Time':
        this.buildUploadsHistoryBarChartSection(widget, this.uploadsHistoryBarChartAllTimeId);
        break;
      case 'Uploads-History-Bar-Chart : Current Month':
        this.buildUploadsHistoryBarChartSection(widget, this.uploadsHistoryBarChartCurrentMonthId);
        break;
      case 'Uploads-History-Bar-Chart : Last Month':
        this.buildUploadsHistoryBarChartSection(widget, this.uploadsHistoryBarChartLastMonthId);
        break;
      case 'Uploads-History-Bar-Chart : Current Year':
        this.buildUploadsHistoryBarChartSection(widget, this.uploadsHistoryBarChartCurrentYearId);
        break;
      case 'Uploads-History-Bar-Chart : Last Year':
        this.buildUploadsHistoryBarChartSection(widget, this.uploadsHistoryBarChartLastYearId);
        break;
      case 'Live-Streams : KPI History':
        this.populateDataTable(this.liveStreamHistoryTableId, this.liveStreamHistoryTableHeader, widget.data);
        break;
      case 'Storage-Grid-Chart : All Time':
        this.buildStorageGridChartSection(widget.data, this.storageGridChartAllTimeId);
        break;
      case 'Storage-Grid-Chart : Current Month':
        this.buildStorageGridChartSection(widget.data, this.storageGridChartCurrentMonthId);
        break;
      case 'Storage-Grid-Chart : Last Month':
        this.buildStorageGridChartSection(widget.data, this.storageGridChartLastMonthId);
        break;
      case 'Storage-Grid-Chart : Current Year':
        this.buildStorageGridChartSection(widget.data, this.storageGridChartCurrentYearId);
        break;
      case 'Storage-Grid-Chart : Last Year':
        this.buildStorageGridChartSection(widget.data, this.storageGridChartLastYearId);
        break;
      case 'Work-Planning : Total Findings':
        this.populateDataTable(this.workPlanningFindingsId, this.workPlanningFindingsTableHeader, widget.data);
        break;
      case 'Work-Planning : Items':
        this.populateDataTable(this.workPlanningItemsId, this.workPlanningItemsTableHeader, widget.data);
        break;
      case 'Work-Planning : Pending':
        this.populateDataTable(this.workPlanningsId, this.workPlanningsTableHeader, widget.data);
        break;
      default:
        break;
    }
  }

  buildUploadsHistoryBarChartSection(widget, barChartId) {
    this.formatUploadsHistoryBarChartData(widget, barChartId);
  }

  formatUploadsHistoryBarChartData(section, barChartId) {
    const storageData = {
      completed: [], rejected: [], completedWithErrors: [],
    };
    const req = section.data;
    if (req) {
      for (let i = 0; i < req.length; i++) {
        if (req[i].Status == 'Completed') {
          storageData.completed.push(req[i]);
        } else if (req[i].Status == 'Rejected') {
          storageData.rejected.push(req[i]);
        } else if (req[i].Status == 'CompletedWithErrors') {
          storageData.completedWithErrors.push(req[i]);
        }
      }
    }
    this.buildUploadsHistoryBarChartWidget(storageData, barChartId);
  }

  buildStorageGridChartSection(data, gridId) {
    if (data) {
      this.getStorageGridChartWidgetData(data, gridId);
    }
  }

  getStorageGridChartWidgetData(data, gridId) {
    const storageData = [];
    for (let i = 0; i < data.length; i++) {
      storageData.push({ Region: data[i].Region, SizeInTB: data[i].SizeInTB, FileCount: data[i].FileCount });
    }
    this.populateDataTable(gridId, this.storageGridChartTableHeader, storageData);
  }

  getUserFoldersPath(data) {
    // format reportedOn datetime
    if (data[0].ReportedOn) {
      for (let i = 0; i < data.length; i++) {
        data[i].ReportedOn = this.dateService.formatToFullDate(data[i].ReportedOn);
      }
    }

    // get users folder paths
    let userFolderPathData = [];
    for (let i = 0; i < data.length; i++) {
      const userFolderPath = this.userFolderPath.map(path => Number(path));
      const dataFolderPath: string[] = data[i].FolderPathId.split('|');
      const folderPath: number[] = dataFolderPath.map(path => Number(path));
      delete data[i].FolderPathId;
      userFolderPath.forEach(folder => {
        const isUserFolderPath = folderPath.includes(folder);
        if (isUserFolderPath) {
          if (userFolderPathData.indexOf(data[i]) == -1) { userFolderPathData.push(data[i]); }
        }
      });
    }

    if (data.length > 0 && !data[0].CameraName) {
      // group data by work template
      const groupWorkItemparent = userFolderPathData.reduce((r, a) => {
        r[a.WorkItemParent] = [...r[a.WorkItemParent] || [], a];
        return r;
      }, {});
      const workItemparents = Object.keys(groupWorkItemparent);
      const folderData = userFolderPathData;
      userFolderPathData = [];

      // get data items counts
      for (let i = 0; i < workItemparents.length; i++) {
        const workItemParentGroup = groupWorkItemparent[workItemparents[i]];
        const workItemCount = workItemParentGroup.length;
        if (folderData[0].Status) {
          // get count of complete items and items to complete
          const completeItems = workItemParentGroup.filter(x => x.status == 1).length;
          const inCompleteItems = workItemCount - completeItems;
          userFolderPathData.push({ workItemParent: workItemparents[i], completeItems, inCompleteItems });
        } else {
          userFolderPathData.push({ workItemParent: workItemparents[i], workItemCount });
        }
      }
    }

    return userFolderPathData;
  }

  populateDataTable(tableId, tableHeader, tableData) {
    if (tableData && tableData.length > 0) {
      let data = tableData;
      if (tableId.indexOf('live-stream') !== -1 || tableId.indexOf('work-planning') !== -1) {
        tableData = this.getUserFoldersPath(data);
      }
    }

    const liveStreamTable = document.getElementById(tableId);
    if (!liveStreamTable) { return; }
    const liveStreamsTables = liveStreamTable.getElementsByTagName('table');

    if (liveStreamsTables.length === 1) {
      return;
    } else {
      this.buildLiveStreamPaginationTable(document.getElementById(tableId), false, false, false, tableHeader, tableData);
    }
  }

  buildLiveStreamPaginationTable(parentNode, head, body, data, tableHeader, tableData) {
    if (typeof head == 'undefined') { head = true; }
    if (typeof body == 'undefined') { body = true; }
    if (!data) {
      data = {
        head: tableHeader,
        body: tableData
      };
    }

    let table = document.createElement('table');
    let tr: HTMLTableRowElement;
    let th: HTMLTableHeaderCellElement;
    let td: HTMLTableDataCellElement;

    // header
    tr = document.createElement('tr');
    let headers = data.head || [];
    for (let i = 0; i < headers.length; i++) {
      th = document.createElement('th');
      th.style.fontSize = '13px';
      th.style.color = '#5b7f95';
      th.innerHTML = headers[i];
      tr.appendChild(th);
    }
    if (head) {
      let thead = document.createElement('thead');
      thead.appendChild(tr);
      table.appendChild(thead);
    } else {
      table.appendChild(tr);
    }
    // end header

    // body
    let table_body = data.body || [];
    if (body) {
      var tbody = document.createElement('tbody');
    }
    for (var i = 0; i < table_body.length; i++) {
      tr = document.createElement('tr');
      tr.style.fontSize = '13px';
      const tbs: any = Object.values(table_body[i]);
      for (var j = 0; j < tbs.length; j++) {
        td = document.createElement('td');
        td.innerHTML = tbs[j];
        tr.appendChild(td);
      }
      if (body) {
        tbody.appendChild(tr);
      } else {
        table.appendChild(tr);
      }
    }
    if (body) {
      table.appendChild(tbody);
    }
    // end body

    if (parentNode) {
      parentNode.appendChild(table);
    }
    // return table;
  }

  buildUploadsHistoryBarChartWidget(data, barChartId) {
    this.stackedColumnChart = new Chart({
      primaryXAxis: this.primaryXAxis,
      primaryYAxis: this.primaryYAxis,
      series: [
        {
          type: 'StackingColumn', dataSource: data.completed,
          xName: 'Region', yName: 'FileCount', name: 'Completed', fill: '#009B77', legendShape: 'Rectangle'
        }, {
          type: 'StackingColumn', dataSource: data.rejected,
          xName: 'Region', yName: 'FileCount', name: 'Rejected', fill: '#FFC72C', legendShape: 'Rectangle'
        }, {
          type: 'StackingColumn', dataSource: data.completedWithErrors,
          xName: 'Region', yName: 'FileCount', name: 'Completed With Errors', fill: '#D60F36', legendShape: 'Rectangle'
        }
      ],
      legendSettings: {
        visible: true,
        position: 'Right',
      },
      tooltip: {
        enable: true,
      },
    });
    this.stackedColumnChart.appendTo(barChartId);
  }

  buildStoragePieChartWidget(data, chartId) {
    if (data && data.length > 0) {
      this.pieChart = new AccumulationChart({
        enableSmartLabels: true,
        series: [
          {
            dataSource: data,
            pointColorMapping: 'fill',
            name: '',
            xName: 'Region',
            yName: 'FileCount',
            dataLabel: { visible: true, position: 'Outside', connectorStyle: { length: '10%' }, name: 'text' }
          }
        ],
        textRender: (args: IAccTextRenderEventArgs) => {
          this.sumOfPoints = args.series['sumOfPoints'];
          let value = args.point.y / this.sumOfPoints * 100;
          args['text'] = args.point.x + ': ' + args.point.y + ' - ' + value.toFixed(1) + '' + ' %';
        },
        tooltip: {
          enable: true,
          format: '${point.x} : <b>${point.y}</b>'
        },
        title: 'Processed File Count - Total : ' + data[0].TotalFileCount + ' Files',
      });
      this.pieChart.appendTo(chartId);
    }
  }

  toggleWidgetsDisplay(widgets) {
    if (this.dashboard) {
      this.currentDashboardModel = this.dashboard.serialize();
      const availablePanels = this.currentDashboardModel.map(p => p.id);
      this.widgetLabels = widgets.filter(w => availablePanels.indexOf(w.id) == -1);
    } else {
      this.widgetLabels = this.allWidgets;
    }
  }

  onSaveClick() {
    this.store.dispatch(new ShowSpinner());
    const currentModel = this.dashboard.serialize();
    this.savedModel = currentModel.filter(cm => cm.id.indexOf('Layout') != -1);
    this.savedModel.forEach((model) => {
      model.widgetId = model.id.slice(7);
    });


    this.store.dispatch(new UpdateUserDashboardWidgets(this.savedModel));
    this.store.dispatch(new GetAllWidgets());
    this.dashboard.allowDragging = this.dashboard.allowResizing = this.dashboard.allowFloating = this.isCustomizable = false;
  }

  addWidget(e) {
    if (e == 'add-panel') {
      this.panel = {
        id: 'layout__' + this.count.toString(),
        row: 0,
        sizeX: 2,
        sizeY: 1,
        col: 0,
        name: '',
        header: '<div id="close" class="e-template-icon"><span class="e-edit-icon"> &#x270E; </span><span class="e-clear-icon"></span></div>',
        content: '',
        widgetId: ''
      };

      this.dashboard.addPanel(this.panel);
      this.closeWidgetsDialogList();
      this.showEditAndDeleteIcons();
      this.count += 1;
      this.dashboard.refresh();
    } else {
      const targetId = (<HTMLElement>this.widgetContainer.target).offsetParent.id;
      const newTargetId = e.widgetId;
      const dashboardModel = this.dashboard.serialize();
      const widgetModel = dashboardModel.filter(m => m.id == targetId);
      const newWidget = this.widgetLabels.filter(nw => nw.widgetId == newTargetId);
      this.dashboard.removePanel(targetId);
      this.panel = {
        id: 'Layout_' + newWidget[0].widgetId,
        row: widgetModel[0].row,
        col: widgetModel[0].col,
        name: newWidget[0].name,
        header: newWidget[0].header,
        content: newWidget[0].preview,
        widgetId: newWidget[0].widgetId
      };

      switch (e.name) {
        case 'Uploads-History-Bar-Chart : All Time':
        case 'Uploads-History-Bar-Chart : Current Month':
        case 'Uploads-History-Bar-Chart : Last Month':
        case 'Uploads-History-Bar-Chart : Current Year':
        case 'Uploads-History-Bar-Chart : Last Year':
          this.panel.sizeX = 3;
          break;
        case 'Live-Streams : KPI History':
        case 'Storage-Grid-Chart : All Time':
        case 'Storage-Grid-Chart : Current Month':
        case 'Storage-Grid-Chart : Last Month':
        case 'Storage-Grid-Chart : Current Year':
        case 'Storage-Grid-Chart : Last Year':
        case 'Storage-Pie-Chart : All Time':
        case 'Storage-Pie-Chart : Current Month':
        case 'Storage-Pie-Chart : Last Month':
        case 'Storage-Pie-Chart : Current Year':
        case 'Storage-Pie-Chart : Last Year':
        case 'Work-Planning : Items':
          this.panel.sizeX = 2;
          break;
        default:
          this.panel.sizeX = 1;
          break;
      }

      this.dashboard.addPanel(this.panel);
      this.dashboard.refresh();
      this.widgetLabels = this.widgetLabels.filter(w => w.name != this.panel.name);
      this.closeWidgetsDialogList();
      this.showEditAndDeleteIcons();
      this.toggleWidgetsDisplay(this.allWidgets);
      this.currentDashboardModel = this.dashboard.serialize();
    }
  }

  customizeDashboard() {
    this.showEditAndDeleteIcons();
    this.dashboard.allowDragging = this.dashboard.allowResizing = this.dashboard.allowFloating = this.isCustomizable = true;
  }

  oncancelClick() {
    this.store.dispatch(new GetUserDashboardWidgets());
    this.hideEditAndDeleteIcons();
    this.dashboard.allowDragging = this.dashboard.allowResizing = this.dashboard.allowFloating = this.isCustomizable = false;
    setTimeout(() => {
      this.store.dispatch(new GetAllWidgets());
    }, 2000);
  }

  onCloseIconHandler(event: any): void {
    const targetElement = (<HTMLElement>event.target).offsetParent;
    if (targetElement) {
      // const targetId = targetElement.id;
      // const dashboardModel = this.dashboard.serialize();
      // const widgetModel = dashboardModel.filter(m => m.id == targetId);
      this.dashboard.removePanel(targetElement.id);
      this.dashboard.refresh();
    }

    this.currentDashboardModel = this.dashboard.serialize();
    this.toggleWidgetsDisplay(this.allWidgets);
  }

  showlayoutDialogList() {
    this.layoutDialogList.show();
  }

  onEditIconHandler(args: any) {
    if (!this.isCustomizable) { return; }
    this.widgetContainer = args;
    this.sectionsDialogList.show();
  }

  closeLayoutDialog() {
    this.layoutDialogList.hide();
  }

  closeWidgetsDialogList() {
    this.sectionsDialogList.hide();
  }

  changeTemplate(e) {
    if (e) {
      this.onTemplateConfirm(this.args);
    }
  }

  // on selecting template layout
  onTemplateClick(args: any): void {
    const currentModel = this.dashboard.serialize();
    this.args = args;
    if (currentModel.length > 0) {
      this.changeTemplateMessage = 'Previous widgets displayed will be cleared.';
      this.layoutDialogList.hide();
      this.confirmModal.show();
    } else {
      this.onTemplateConfirm(this.args);
    }
  }

  onTemplateConfirm(args) {
    let target: any = args.target;
    let selectedElement: any = document.getElementsByClassName('e-selected-style');
    if (selectedElement.length) {
      selectedElement[0].classList.remove('e-selected-style');
    }
    if ((<HTMLElement>target).className === 'image-pattern-style') {
      this.initializeTemplate(<HTMLElement>args.target);
    }
    (<HTMLElement>target).classList.add('e-selected-style');
    this.closeLayoutDialog();
    setTimeout(() => {
      this.showEditAndDeleteIcons();
    }, 100);
  }

  // initializing selected template layout
  initializeTemplate(element: HTMLElement): void {
    let updatedPanels: PanelModel[] = [];
    this.index = parseInt(element.getAttribute('data-id'), 10) - 1;
    const panel: any = Object.keys(this.panels[this.index]).map((panelIndex: string) => {
      return this.panels[this.index][panelIndex];
    });

    for (let i = 0; i < panel.length; i++) {
      const panelModelValue: PanelModel = {
        row: panel[i].row,
        col: panel[i].col,
        sizeX: panel[i].sizeX,
        sizeY: panel[i].sizeY,
        header: '<div id="close" class="e-template-icon"><span class="e-edit-icon">&#x270E;</span><span class="e-clear-icon"></span></div>',
        content: panel[i].content
      };
      updatedPanels.push(panelModelValue);
    }

    this.dashboard.panels = updatedPanels;
    this.widgetLabels = this.allWidgets;
  }

  closeDialog() {
    // this.layoutDialogList.hide();
  }

  showEditAndDeleteIcons() {
    const closeElements: any = document.getElementsByClassName('e-clear-icon');
    const editElements: any = document.getElementsByClassName('e-edit-icon');
    for (let i: number = 0; i < closeElements.length; i++) {
      closeElements[i].style.display = 'inline-block';
      closeElements[i].addEventListener('click', this.onCloseIconHandler.bind(this));
      editElements[i].style.display = 'inline-block';
      editElements[i].addEventListener('click', this.onEditIconHandler.bind(this));
    }
  }

  hideEditAndDeleteIcons() {
    const closeElements: any = document.getElementsByClassName('e-clear-icon');
    const editElements: any = document.getElementsByClassName('e-edit-icon');
    for (let i = 0; i < closeElements.length; i++) {
      closeElements[i].style.display = 'none';
      closeElements[i].addEventListener('click', this.onCloseIconHandler.bind(this));
      editElements[i].style.display = 'none';
      editElements[i].addEventListener('click', this.onEditIconHandler.bind(this));
    }
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

}

