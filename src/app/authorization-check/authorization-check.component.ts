import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '../shared/base/base.component';
import { Store } from '@ngxs/store';
import { Router } from '@angular/router';

@Component({
  template: `
<img style="margin: auto; display: block; width: 75px;" src="./assets/images/icon-loading-colored.svg">
<label style="text-align: center; font-family: 'Open Sans', sans-serif; font-size: 20px; display: block;"> Please wait... </label>` })
export class AuthorizationCheckComponent extends BaseComponent implements OnInit {

  constructor(protected store: Store, private router: Router) {
    super(store);
    this.hideLeftNav();
  }

  ngOnInit() {
    const key = 'omv-client-return-url';
    const route = localStorage.getItem(key);
    this.router.navigateByUrl(route);
  }
}
