import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { AuthService } from '../services/business/auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  authenticated: Promise<boolean>;

  constructor(private auth: AuthService, private router: Router) {
    this.authenticated = auth.isAuthenticated();
  }

  async canActivate() {
    try {
      const authenticated = await this.authenticated;
      if (authenticated) {
        return true;
      }
    } catch (ex) {
      // token expired, so logout
      const route = window.location.pathname;
      this.saveReturnUrl(route);
      this.auth.logout();
      return false;
    }
    // Redirect to login flow.
    const route = window.location.pathname;
    this.saveReturnUrl(route);
    this.auth.login();
    return false;
  }

  private saveReturnUrl(route: string) {
    const ignored_routes = ['/startup', '/authorize-check', '/implicit/callback', '/login'];
    if (ignored_routes.includes(route) || route.includes('/implicit/callback')) { return; }
    const key = 'omv-client-return-url';
    if (localStorage.getItem(key)) { localStorage.removeItem(key); }
    localStorage.setItem(key, route);
  }
}
