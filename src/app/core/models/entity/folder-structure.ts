export class FolderStructure{
    id: number;
    name: string;
    child: [];
    pid: number;
    hasChild: boolean;
    isChecked: boolean;
}