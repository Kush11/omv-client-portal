export class Lookup {
  description: string;
  sort?: string;
  type?: string;
  value: any;
  status?: number;
  name?: string;
}