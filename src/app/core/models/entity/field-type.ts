import { BaseModel } from '../base-model';

export class FieldType extends BaseModel {
  id: number;
  description?: string;
  type: string;
}
