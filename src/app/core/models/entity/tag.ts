export class Tag {
  key?: string;
  value: any;
  values?: any[];
  rangeValue?: any;
  type?: string;
  label: string;
  name: string;
  isMetaColumn?: boolean;
  order?: number;
  cssClass?: string;
  dataType?: string;
  searchType?: string;
}
