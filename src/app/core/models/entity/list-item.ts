export class ListItem {
  id?: number | null;
  parentId?: number;
  value: any;
  description: string;
  sort?: number;
  isSelected?: boolean;
  count?: number;
  key?: string;
  type?: string;
  isChecked?: boolean;
  label?: string;
}