import { BaseModel } from '../base-model';
import { Pagination } from './pagination';


export class MediaHistories {
  pagination: Pagination;
  data: MediaHistory[];
}

export class MediaHistory extends BaseModel {
  auditId: string;
  eventName: string;
  entityType: string;
  entityId: string;
  columnName: string;
  oldValue: string;
  newValue: string;
}

