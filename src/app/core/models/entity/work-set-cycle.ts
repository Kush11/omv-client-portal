import { Pagination } from './pagination';
import { BaseModel } from '../base-model';

export class WorkSetCycles {
  pagination: Pagination;
  total: number;
  data: WorkSetCycle[];
}

export class WorkSetCycle extends BaseModel {
  id: number;
  workSetId: number;
  startDate: Date | string;
  startDateString: string;
  endDate: Date | string;
  endDateString: string;
  status: number;
  statusName?: string;
  statusDisplay?: string;
  isPublished: boolean;
}