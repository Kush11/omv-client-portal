import { ListItem } from './list-item';
import { BaseModel } from '../base-model';

export class MetadataField extends BaseModel {
  metadataFieldId?: number;
  entityId?: string;
  entityName?: string;
  fieldName?: string;
  listId?: number;
  listName?: string;
  fieldTypeId?: number;
  controlType?: string;
  dataType?: string;
  searchType?: string;
  typeDescription?: string;

  id?: number;
  fieldId?: number;
  parentId?: number; // for nested
  settingId?: number;
  name?: string;
  label?: string;
  overrideLabel?: string;
  type?: string;
  typeId?: number;
  options?: ListItem[];
  value?: any;
  order?: number;
  isRequired?: boolean;
  isUnique?: boolean;
  isEditable?: boolean;
  allowDocumentUpload?: boolean;


  canUpload?: boolean;
  canEdit?: boolean;

  status?: number;

  isFilterable?: boolean;
  isChecked?: boolean;
  isSelected?: boolean;
  isNested?: boolean;
  isSystem?: boolean;

  module?: number;
}
