import { Pagination } from './pagination';
import { BaseModel } from '../base-model';

export interface VideoSchedule {
  hour: number;
  meridiem: string;
  value?: string;
}

export class LiveStreams {
  pagination: Pagination;
  data: LiveStream[];
}

export class LiveStream extends BaseModel {
  id: number;
  name: string;
  url: string;
  poster: string;
  isFavorite?: boolean;
  isChecked?: boolean;
  date?: string;
  favoriteId: number;
  isMultiple?:boolean;
  value:string;
  favouriteIds?:number[];
}
