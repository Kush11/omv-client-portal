export class MetadataList {
  id: number;
  name: string;
  status: number;
  statusName: string;
  itemValue?: string;
  itemDescription?: string;
  itemSort?: string;
  parentItemValue?: string;
  parentListName?: string;
  parentListId:number;
}



