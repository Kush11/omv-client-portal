import { BaseModel } from '../base-model'
import { LiveStream } from './live-stream';
import { User } from './user';

export class CollaborationSession {
	id: number;
	title: string;
	videos: LiveStream[];
	videoIds: number[];
	date: Date | null;
	startTime: Date | null;
	startTimeString?: string;
	endTime: Date | null;
	endTimeString?: string;
	participants: User[];
	participantIds: number[];
	participantNames: string;
	isDeleted?: boolean;
	status: string;
	canJoin: boolean;
	canEdit: boolean;
	isPast: boolean;	
	isHost: boolean;
	directoryId: number;
	directoryPath: string;
	day: number;
	month: number;
}

export class CollaborationSessionItem {
	id: any;
	type: string;
}

export class Collaboration extends BaseModel {
	id?: number;
	title: string;
	startTime: string;
	endTime: string;
	ParticipantIds: number[];
	date?: Date;
	participants?: User[];
	participantsDisplay?: string;
	isProgress?: boolean;
	media?: LiveStream[];
	insertItems?: CollaborationInserItem[];
}

export class Participant {
	id: number;
	firstName: string;
	lastName: string;
	displayName: string;
	role: string;
	imageUrl: string;
}

export class CollaborationItem {
	id: number;
	name: string;
	url?: string;
	poster?: string;
	isFavorite?: boolean;
	isChecked?: boolean;
	type?: string;
}

export class CollaborationRoom {
	name: string;
	title?: string;
	participantToken?: string;
	channelToken?: string;
	feeds?: LiveStream[];
	isHost?: boolean;
	hostIdentity: string;
	connectedUserName?: string;
	sessionTitle: string;
	participants: User[];
}

export interface StepOne {

	title: string;
	selectedLiveStreams: LiveStream[];

}

export interface StepTwo {

	selectedDate: Date;
	selectedEndTime: string;
	selectedStartTime: string;

}

export class CollaborationInserItem {
	id: number;
	type: string
}

export class CollaborationScreenshot {
	id: number;
	name: string;
	url: string;
}