export class RelatedFiles {
    relatedItemId: number;
    documentId: string;
    relatedDocumentId: string;
    directoryId: number | null;
    entityType: string;
    entityId: string;
    documentTypeCode: string;
    name: string;
    url: string;
    metadata: string;
    contentType: string;
    size: number | null;
    thumbnailContainerUrl: string;
    status: number;
}