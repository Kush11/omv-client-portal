import { BaseModel } from '../base-model';

export class UploadRequest extends BaseModel {
  id: number;
  createdBy: string;
  type: number;
  iconCss?: string;
  requester: string;
  requesterName: string;
  source: string;
  destination: string;
  ruleId: number;
  ruleName: string;
  isOCRAllowed: boolean;
  isSRAllowed: boolean;
  size: number;
  sizeDisplay: string;
  files: string;
  iP: string;
  status: string;
  estProcessTime: string;
  createdOn: Date;
  createdOnString?: string;
}
