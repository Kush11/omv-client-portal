import { BaseModel } from '../base-model';

export class WorkItem extends BaseModel {
	id?: number;
	workSetId: number;
	processId: number;
	processName?: string;
	data: string;
	sort?: any;
}


export class WorkFilesGroup extends BaseModel {
	name: string;
	workFile: WorkFile[];
	cycleStartDate: Date;
	cycleEndDate: Date;
	frequency: string;
}

export class WorkFile extends BaseModel {
	id: any;
	documentId: any;
	storageType: string;
	entityType: string;
	entityId: string;
	directoryId: number;
	type: string;
	documentTypeCode: string;
	documentUrl: string;
	metadata: string;
	metadataFieldSettingId: number;
	contentType: string;
	size?: number;
	containerId: string;
	thumbnailContainerUrl: string;
	isDeleted?: boolean;
	status: number;
	url: string;
	name: string;
	documentName: string;;
	requester: number;
	thumbnail: string;
	isFinding: boolean;
	documentDate: Date;
	processCode: string;
	findingName: string;
	metadataField: string;
	processName: string;
	isChecked?: boolean;
	fileType: number;
	workId: number;
}

export enum WorkFileType {
	Form = 1,
	Finding = 2
}

