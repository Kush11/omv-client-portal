import { MetadataField } from './metadata-field';
import { BaseModel } from '../base-model';
import { Pagination } from './pagination';
import { WorkFinding } from './work-finding';

export class Works extends BaseModel {
  pagination: Pagination;
  total: number;
  data: Work[];
  returned: number;
}

export class Work extends BaseModel {
  id: number;
  assetId: number;
  assetName: string;
  itemNo: string;
  itemName: string;
  processId: number;
  processCode: string;
  processName: string;
  findingsText: string;
  firstItemIdentifier: any;
  secondItemIdentifier: any;

  name: string;
  completeBy: Date;

  metadata: MetadataField[];

  currentStepId: number;
  currentStepName: string;
  cycleName: string;
  frequencyId: number;
  workSetCycleEndDate: Date;
  workSetCycleStartDate: Date;
  workSetCycleStatus: string;
  formData: string;
  findingData: string;
  advanceButton: string;
  advancedByGroup: number | null;
  canAdvance: boolean;
  disableAdvance: boolean;
  notifyByGroup: number | null;
  stepOrder: number | null;
  status: string;
  advanceButtonText: string;
  itemFields: MetadataField[];
  forms: MetadataField[];
  findings: WorkFinding[];
}