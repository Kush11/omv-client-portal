import { LiveStream } from './live-stream';
import { CameraRecording_GetAllOutputDTO } from '../../dtos/input/live-stream/StreamArchiveOutputDTO';

export class StreamingArchiveModel {
  dailyScheduleDate?: string;
  timeLineDate?: string;
  camera: StreamingArchiveCameraModel[];
}

export class StreamingArchiveCameraModel {
  cameraId: number;
  favoriteId: number | null;
  livestreamURL: string;
  name: string;
  thumbnailUrl: string;
  isFavorite: boolean;
  recordings: CameraRecording_GetAllOutputDTO[];
}

export class StreamingArchiveCameraRecordingModel {
  cameraRecordingId: number;
  cameraId: number;
  documentId: string;
  status: number;
  startDate: Date;
  endDate: Date;
  cameraRecordingStatus: number;
  documentName: string;
  documentUrl: string;
  thumbnailContainerUrl: string;
}

export class StreamingArchive {
  timelineDate: Date;
  date: Date;
  feeds: CameraFeed[];
}

export class CameraFeed {
  id: string;
  name: string;
  durations: CameraRecording[];
  url?: string;
  color?: string;
  labelColor?: string;
  isLoading?: boolean;
}

export class CameraRecording {
  startTime: Date;
  startTimeString?: string;
  endTimeString?: string;
  endTime: Date;
  offset?: number;
  width?: number;
  url?: string;
}