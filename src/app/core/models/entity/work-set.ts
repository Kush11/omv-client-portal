import { BaseModel } from '../base-model';
import { WorkItem } from './work-item';

export class WorkSetModel {
  data: WorkSet;
}

export class WorkSetData {
  id: number;
  name: string;
  asset: number;
  published: boolean;
  template: string;
  templateId: number;
  templateGroupId: number;
  metadataListId: number;
  comment: string;
  frequencyType: number;
  customDuration: number;
  customDurationName: string;
  customFrequency: number;
  frequencyStartDate: Date;
  frequencyEndDate: Date;
  repeatFrequency: number;
  repeatDuration: number;
  cycleName: string;
  completedDate: Date;
  workItems: WorkItem[];
  status: string;
  statusName: string;
  directoryId: number;
  directoryPath: string;
}

export class WorkSet extends BaseModel {
  id: number;
  name: string;
  asset: number;
  status: string;
  published: boolean;
  template: string;
  templateId: number;
  templateGroupId: number;
  metadataListId: number;
  schedule: WorkSetSchedule;
  workItems: WorkItem[];
  directoryId: number;
  directoryPath: string;
}

export class WorkSetSchedule {
  cycleName: string;
  frequencyType: number;
  startDate: Date;
  endDate: Date;
  completionDate: Date;
  customFrequency: number;
  customDuration: number;
  customDurationName: string;
  repeatFrequency: number;
  repeatDuration: number;
}

export class FrequencyType {
  name: string;
  id?: number;
}