import { BaseModel } from '../base-model';
import { MetadataField } from './metadata-field';
import { ListItem } from './list-item';
import { PaginationInfo } from '../../dtos/output/PaginationInfo';

export class WorkGroup {
  workTemplates: WorkTemplate[];
  workTemplateVersions: WorkTemplateVersion[];
  workTemplateHistories: WorkTemplateHistory[];
}

export class WorkTemplate extends BaseModel {
  id: number;
  groupId: number;
  name: string;
  version: number;
  workType: string;
  workItem: string;
  workItemParent: string;
  status: string;
  activeId?: number;
  metadataListId: number;
  fields: MetadataField[];
  processes: WorkTemplateProcess[];
  assetParent: string;
}

export class WorkTemplateFields {
  fields: MetadataField[];
  processes: WorkTemplateProcess[];
}

export class WorkTemplateField {
  name: string;
  isUnique: boolean;
}

export class WorkTemplateProcess extends BaseModel {
  id: number;
  name: string;
  code: string;
  steps: WorkTemplateStep[];
  templateId: number;
}

export class WorkTemplateStep extends BaseModel {
  id: number;
  code: string;
  name: string;
  notifyByGroup: number;
  templateCode: string;
  advanceButton: string;
  advancedByGroup: number;
  forms: MetadataField[];
  findings: MetadataField[];
  processId: number;
  order?: number;
}

export class WorkTemplateVersion extends BaseModel {
  templateId?: number;
  version: number;
  comment?: string;
  status?: string;
  publishedOn?: Date | string;
  publishedBy?: string;
  publishedOnString?: string;
}

export class WorkTemplateHistories  {
  data: WorkTemplateHistory[];
  pagination: PaginationInfo;
}

export class WorkTemplateHistory extends BaseModel {
  auditId: string;
  event: string;
  entityType: string;
  entityId: string;
  column: string;
  oldValue: string;
  newValue: string;
}