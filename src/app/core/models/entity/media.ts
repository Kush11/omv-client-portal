import { Pagination } from './pagination';
import { BaseModel } from '../base-model';
import { Filter } from './filter';
import { FieldConfiguration } from 'src/app/shared/dynamic-components/field-setting';

export class Media {
  document: any;
  pagination: Pagination;
  filters: Filter[];
  data: MediaItem[];
  doc: any;
}

export class Thumbnails {
  thumbnailUrl: string;
  time: number;
}

export class MediaItem extends BaseModel {
  custom?: any;
  id: any;
  documentId?: any;
  relatedDocumentId?: string; // documentid for a related item
  directoryId: number;
  directoryName: string;
  directoryParentId: number;
  directoryParentName: string;
  isDirectory: boolean;
  storageType: string;
  entityType: string;
  entityId: string;
  documentTypeCode?: string;
  name: string;

  nameWithoutExtension?: string;


  url: string;
  metadata?: any;
  fieldName?: string;
  contentType?: string;
  containerId?: string;
  size?: number;
  thumbnail: string;
  thumbnails: thumbnails[];
  thumbnailContainer?:string;
  thumbnailObject?: any;
  isDeleted?: boolean;
  status?: number;
  parentId?: number;
  hasChild?: boolean;
  isChecked?: boolean;
  longitude: number;
  latitude: number;
  icon?: string;
  isActive?: boolean;
  order?: number;
  isSRAllowed?: boolean;

  type: string;
  requester?: number;
  requestId?: number;
  requestType?: string;

  //favorite
  favoriteId: number;
  isFavorite?: boolean;
  isSearch?: boolean;

  // for treeview
  expanded?: boolean;
}

export class thumbnails {
  time: string;
  src: string;
}
