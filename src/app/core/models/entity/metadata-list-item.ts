export class MetadataListItem {
  id?: number;
  name?: string;
  listId?: number;
  fieldName?: string;
  isUnique?: boolean;
  metadataListItemId?: number;
  metadataListId?: number;
  value?: string;
  description?: string;
  sort: string;
  parentId: number;
  parentName?: string;
  parentItemValue?: string;
  status?: string;
}
