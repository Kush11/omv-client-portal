export class Widget {
    id: string;
    name: string;
    header: string;
    content: string;
    preview: string;
    widgetId: number;
}

export class UserWidget {
    id: string;
    widgetId: number;
    sizeX: number;
    sizeY: number;
    row: number;
    col: number;
    content: string;
    name: string;
    header: string;
}

