export class MenuItem {
  id?: any;
  text: string;
  subMenuItems?: MenuItem[];
  isSelected?: boolean;
  parentId?: any;
}