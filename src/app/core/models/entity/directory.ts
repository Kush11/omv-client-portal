import { BaseModel } from '../base-model';

export class Directory extends BaseModel {
  id: number;
  name: string;
  parentId: number | null;
  hasChild?: boolean;
  isChecked?: boolean;
  isDirectory?: boolean;
  icon?: string;
  isParent?: boolean;
  expanded?: boolean;
  hasChildRecords?: boolean;
}