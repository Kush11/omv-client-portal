import { BaseModel } from '../base-model';

export class EmailTemplate extends BaseModel {
    id: string;
    name: string;
    subject: string;
    body: string;
    isBodyHTML?: number;
    isSystem?: boolean;
    priority?: string;
  }