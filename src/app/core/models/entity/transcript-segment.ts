export class TranscriptSegment {
  timestamp: string;
  text: string;
  startTime?: number;
  endTime?: number;
  isCurrent?: boolean;
  index?: number;
}