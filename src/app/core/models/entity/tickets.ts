import {Document} from './document';
export class Ticket {
    id: number;
    subject: string;
    message: string;
    followUp: boolean;
    isFollowUp: string;
    availability: boolean[];
    startTime: boolean;
    endTime: boolean;
    documents: Document[];
}
