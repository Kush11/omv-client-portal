export class Filter {
  label: string;
  name: string;
  searchType: string;
  dataType: string;
  order: number;
  options: FilterDetail[];
  isOpen: boolean;
  value: any;
  values: any[];
  min: any;
  max: any;
}

export class FilterDetail {
  value: any;
  count: number;
  label: string;
  isChecked: boolean;
  key: string;
}

export enum FilterType {
  checkbox = 'checkbox',
  date = 'date',
  range = 'range'
}