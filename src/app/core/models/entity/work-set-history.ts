import { BaseModel } from '../base-model';
import { PaginationInfo } from '../../dtos/output/PaginationInfo';

export class WorkSetHistories {
  data: WorkSetHistory[];
  pagination: PaginationInfo;
}

export class WorkSetHistory extends BaseModel {
  auditId: string;
  event: string;
  entityType: string;
  entityId: string;
  column: string;
  oldValue: string;
  newValue: string;
}