import { MetadataField } from './metadata-field';

export class WorkFinding {
  id: number;
  workId: number;
  stepId: number;
  findingData: string;
  status: number;
  itemFields: MetadataField[];
  fields: MetadataField[];
}