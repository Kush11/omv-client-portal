import { BaseModel } from '../base-model';

export class Rule extends BaseModel {
  id: number;
  name: string;
  assemblyPath: string;
  file: any;
  fileName: string;
  class: string;
  approvalGroupId: number;
  approvalGroupName: string;
}