import { Pagination } from './pagination';
import { BaseModel } from '../base-model';

export class Users {
  pagination: Pagination;
  total: number;
  data: User[];
}

export class User extends BaseModel {
  id: number;
  userName: string;
  emailAddress: string;
  firstName: string;
  lastName: string;
  displayName: string;
  roleNames: string[];
  groups?: string;
  groupsDisplay?: string;
  status: number;
  statusName?: string;
  createdOn?: Date;
  createdBy?: string;
  modifiedOn?: Date;
  modifiedOnString?: string;
  modifiedBy?: string;
  folderPaths?: string[];
  permissions?: User_Permissions;
  collaborationRole?: string;
  avatarText?: string;
  avatarColor?: string;
  isParticipant?: boolean;
  isCurrent?: boolean;
  isHost?: boolean;
}

export class User_Permissions {
  menus: CustomerMenuPermission_GetByCustomerId[];
  UserPermissions: string[];
}

export class CustomerMenuPermission_GetByCustomerId {
  customerId: number;
  menuName: string;
  isPermitted: boolean;
}

export interface LoggedInUser {
  firstName: string;
  lastName: string;
  id: string;
  role: Array<UserRole>;
}

export enum UserRole {
  None = 0,
  Admin = 1
}
