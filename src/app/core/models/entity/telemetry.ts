import { MediaItem } from './media';

export class Telemetry {
	feeds: MediaItem[];
	files: TelemetryFile[];
}

export class TelemetryFile {
	name: string;
	nameWithoutExtension: string;
	tabs: string[];
	columns: string[];
	data: any[];
}


