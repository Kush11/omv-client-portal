export class Breadcrumb {
  name?: string;
  link?: any;
  text?: string;
  isFinal?: boolean;
  isLast?: boolean;
  isBack?: boolean;
  action?: EventListener;
}
