export class GridColumn {
  type?: string;
  headerText?: string;
  field?: string;
  width?: any;
  textAlign?: string = 'right';
  format?: string;
  showCheckbox?: boolean;
  template?: string;
  cssClass?: string;
  customAttributes?: object;
  formatOptions?: object;
  useAsLink?: boolean;
  ignoreFiltering?: boolean;
  ignoreSorting?: boolean;
  custom?: boolean;
  filterType?: string;

  //Custom Columns
  query?: any;
  isCustom?: boolean;
  visible?: boolean;
  isCollapse?: boolean;
  customText?: string;
  buttons?: GridColumnButton[];
  action?: EventListener;
}

export class GridColumnButton {
  type?= 'button';
  cssClass?= 'button-default';
  icon?: string;
  disabled?: boolean;
  hidden?: boolean;
  visible?: boolean;
  text?: string;
  override?: boolean;
  left?: number;
  right?: number;
  query?: any;
  imageSrc?: string;
  imageWidth?: number;
  action?: EventListener;
}