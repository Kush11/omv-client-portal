import { BaseModel } from './base-model';
import { MetadataListItem_GetById } from '../dtos/output/metadata/MetadataListItem_GetById';

export class WorkPlanningParentItem {
  name: string;
}

export class WorkPlanningAssets {
  data: WorkPlanningItem[];
}
export class WorkPlanningItem extends BaseModel {
  workId: number;
  assetId: number;
  asset: string;
  assetName: string;
  itemNo: string;
  itemName: string;
  processId: number;
  processCode: string;
  processName: string;
  findingsText: string;
  currentStep: string;
  currentStepId: number;
  cycleName: string;
  advanceButton: string;
  canAdvance: boolean;
  workSetCycleStartDate: Date;
  workSetCycleEndDate: Date;
  workSetCycleStatus: number;
  workSetCycleStatusName: string;
  frequencyId: number;
  formData: string;
  findingData: string;
  advancedByGroup?: number;
  notifyByGroup?: number;
  stepOrder?: number;
  status: number;
  statusName: string;
  createdOn: Date;
  createdBy: string;
  modifiedOn: Date;
  modifiedBy: string;
  form: WorkItemFormData[];
  findings: WorkItemFindings[];
}

export class WorkItemFormData {
  id: number;
  parentId: number;
  stepId: number;
  Label: string;
  FieldName: string;
  Value: string;
  FieldType: string;
  CanEdit: boolean;
  CanUpload?: boolean;
  IsRequired: boolean;
  isUnique: boolean;
  options: MetadataListItem_GetById[];
}


export class WorkPlanningWorkItemParent extends BaseModel {
  itemName: string;
  itemId: number;
  cycle1: string;
  cycle2: string;
  cycle3: string;
  cycle4: string;
  asset: string;
  cycles: WorkPlanningCycle[];
  // workSetPlanning: WorkSetPlanningWorkItemParent[];
}

export class WorkSetPlanningWorkItemParent extends BaseModel {
  cycleName: string;
  overall: number;
  group: number;
  workList: string;
}

export class WorkPlanningCycle extends BaseModel {
  cycle: string;
  data: WorkPlanningData[];
}
export class WorkPlanningData extends BaseModel {
  label: string;
  totalCompleted: number;
  total: number;
  days: number;
}
export class WorkItemFindings {
  form: WorkItemFormData[];
}
