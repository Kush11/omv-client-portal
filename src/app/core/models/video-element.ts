 export interface FsDocument extends HTMLDocument {
   webkitCancelFullScreen: any;
   fullScreenElement: any;
   cancelFullScreen();
   webkitExitFullscreen: any;
   webkitFullscreenElement: Element;
    mozFullScreenElement?: Element;
    msFullscreenElement?: Element;
    msExitFullscreen?: () => void;
    mozCancelFullScreen?: () => void;
  }
  
 
  
 export interface FsDocumentElement extends HTMLElement {
   webkitRequestFullscreen: any;
    msRequestFullscreen?: () => void;
    mozRequestFullScreen?: () => void;
  }
  
  
  
  