export class ApiError {
  error: string;
  message: string;
}