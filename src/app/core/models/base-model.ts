export class BaseModel {
  createdOn?: Date | null;
  createdOnString?: string;
  createdBy?: string;
  modifiedOn?: Date | null;
  modifiedOnString?: string;
  modifiedBy?: string;

  canActivate?: boolean;
  canDelete?: boolean;
  canDownload?: boolean;
  canEdit?: boolean;
  canOpen?: boolean;
  canPublish?: boolean;
  canRepublish?: boolean;
  completedBy?: Date | null;
  completedByString?: string;

  isSelected?: boolean;
  isChecked?: boolean;

  isDefault?: boolean;

  routeLink?: string;
  isVideo?: boolean;

  favoriteTemplate?: any;
  checkboxTemplate?: any;
  downloadTemplate?: any;
  editTemplate?: any;
}