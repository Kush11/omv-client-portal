export enum KeyType {
  // space bar = 32; up = 38; down = 40; left = 37; right = 39;
  SpaceBar = 32,
  Left = 37,
  Up = 38,
  Right = 39,
  Down = 40,
  F = 70,
  M = 77,
  N = 78,
  P = 80,
  R = 82,
  S = 83,
  T = 84
}