export enum SASTokenType {
  Read = 'read',
  Write = 'write'
}

export enum SASTokenModule {
  Media = 'media',
  WorkPlanning = 'workplanning',
  Core = 'core'
}