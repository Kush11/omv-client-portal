export enum Status {
  Open = 'Open',
  Draft = 'Draft',
  Closed = 'Closed',
  Pending = 'Pending',
  Approved = 'Approved',
  Completed = 'Completed',
  Rejected = 'Rejected',
  Cancelled = 'Cancelled',
  InProgress = 'In Progress',
  Processing = 'Processing',
  CompletedWithErrors = 'CompletedWithErrors',
  Dirty = 'Dirty',
  Valid = 'Valid',
  Pristine = 'Pristine'
}
