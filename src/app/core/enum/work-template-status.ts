export enum WorkTemplateStatus {
  Draft = 'Draft',
  Active = 'Active',
  Inactive = 'Inactive'
}

export enum WorkSetStatus {
  Draft = 'Draft',
  Active = 'Active',
  Inactive = 'Inactive'
}