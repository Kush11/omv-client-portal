// export enum permission {
//   VIEW_USERS = 'VIEW_USERS',
//   VIEW_USERS_EDIT = 'VIEW_USERS_EDIT',
//   VIEW_GROUP = 'VIEW_GROUP',
//   VIEW_GROUP_EDIT = 'VIEW_GROUP_EDIT',
//   VIEW_ADMIN_DASHBOARD = 'VIEW_ADMIN_DASHBOARD',

// }

export enum userType {
  active = 1,
  unassigned = 2,
  disabled = 3
}

export class Permission {
  id: string;
  name: string;
  status: number;
}


export enum menuNames {
  DASHBOARD = 'Dashboard',
  ADMIN_PORTAL = 'Admin Portal',
  MEDIA = 'Media',
  LIVE_STREAM = 'Live Stream',
  WORK_PLANNING = 'Work Planning',
  COLLABORATIONS = 'Collaborations',
}


export enum permissions {
  MEDIA_UPLOAD = 'MediaUpload',
  BULK_MEDIA_UPLOAD = 'BulkMediaUpload',
  BULK_METADATA_EDIT = 'BulkMetadataEdit',
  METADATA_EDIT = 'MetadataEdit',
  LIVE_STREAM_EDIT = 'LiveStreamEdit',
  COLLABORATION_EDIT = 'CollaborationEdit',
  CAN_VIEW_ADMIN_PORTAL = 'ViewAdmin'
}
