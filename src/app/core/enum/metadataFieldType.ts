export enum MetadataFieldType {
  Text = 'Text',
  Select = 'Select',
  Date = 'Date',
  MultiSelect = 'MultiSelect',
  DateRange = 'DateRange',
  Nested = 'Nested',
  Label = 'Label',
  Number = 'Number',
  Checkbox = 'Checkbox'
}

export enum MetadataFieldModule {
  Media = 'Media',
  WorkPlanning = 'Workplanning'
}