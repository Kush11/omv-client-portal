export enum WorkReportType {
  Export = 'export',
  Checklist = 'checklist',
  Findings = 'findings'
}