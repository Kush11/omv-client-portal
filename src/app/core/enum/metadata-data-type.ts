export enum MetadataDataType {
  Date = 'date',
  Double = 'double',
  String = 'string',
  Int32 = 'int32',
  Int64 = 'int64',
}