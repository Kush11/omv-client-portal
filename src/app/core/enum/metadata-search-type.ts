export enum MetadataSearchType {
  None = 'none',
  Checkbox = 'checkbox',
  Range = 'range',
  Date = 'date'
}