export enum ToastType {
  Success = 0,
  Error = 1,
  Info = 2,
  Warning = 3,
  Close = 4
}

export enum ToastAction {
  close = 0
}

export class Toast {
  title?: string;
  message?: string;
  type?: ToastType;
  action?: ToastAction;
  timeOut?: number;
  iconType?: ToastIcon;
}

export enum ToastIcon {
  Record = 0
}