export enum WorkSetStatus {
    Draft = 'Draft',
    Active = 'Active',
    Inactive = 'Inactive'
  }