export enum MediaType {
  Image = 'Image',
  Video = 'Video',
  Document = 'Document',
  Pdf = 'PDF',
  Playlist = 'Playlist',
  Telemetry = 'TELEMETRY',
  Others = 'Others'
}
