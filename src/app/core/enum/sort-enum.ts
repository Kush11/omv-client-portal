export enum SortType {
  Number = 'Number',
  String = 'String',
  Date = 'Date'
}

export enum SortDirection {
  Ascending = 'Ascending',
  Descending = 'Descending'
}