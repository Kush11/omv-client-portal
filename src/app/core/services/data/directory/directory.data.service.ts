import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { MetadataField } from 'src/app/core/models/entity/metadata-field';
import { Media } from 'src/app/core/models/entity/media';
import { Directory } from 'src/app/core/models/entity/directory';

@Injectable({
  providedIn: 'root'
})
export abstract class DirectoryDataService {

  constructor() { }

  abstract getMedia(directoryId: number, pageNumber?: number, limit?: number): Observable<Media>;
  abstract getFolders(parentId?: number): Observable<Directory[]>;
  abstract getFolder(id: number): Observable<Directory>;
  abstract getMetadataFields(directoryId: number): Observable<MetadataField[]>;
  abstract addMetadataField(folderId: number, payload: MetadataField);
  abstract updateMetadataField(folderId: number, payload: MetadataField);
  abstract deleteMetadataField(fieldSettingId: number);
  abstract createDirectory(payload: Directory);
  abstract updateDirectory(id: number, payload: Directory);
  abstract removeDirectory(id: number)
}
