import { DirectoryDataService } from './directory.data.service';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { MetadataField } from 'src/app/core/models/entity/metadata-field';
import { Media } from 'src/app/core/models/entity/media';

@Injectable({
  providedIn: 'root'
})
export class DirectoryMockDataService implements DirectoryDataService {
  getMedia(directoryId: number, pageNumber?: number, limit?: number): Observable<Media> {
    throw new Error("Method not implemented.");
  } getFolders(parentId?: number): Observable<any[]> {
    throw new Error("Method not implemented.");
  }
  getFolder(id: number): Observable<any> {
    throw new Error("Method not implemented.");
  }
  getMetadataFields(directoryId: number): Observable<MetadataField[]> {
    throw new Error("Method not implemented.");
  }
  addMetadataField(folderId: number, payload: MetadataField) {
    throw new Error("Method not implemented.");
  }
  updateMetadataField(folderId: number, payload: MetadataField) {
    throw new Error("Method not implemented.");
  }
  deleteMetadataField(fieldSettingId: number) {
    throw new Error("Method not implemented.");
  }
  createDirectory(payload: any) {
    throw new Error("Method not implemented.");
  }
  updateDirectory(id: number, payload: any) {
    throw new Error("Method not implemented.");
  }
  removeDirectory(id: number) {
    throw new Error("Method not implemented.");
  }

}