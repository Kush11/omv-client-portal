import { DirectoryDataService } from './directory.data.service';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { map } from 'rxjs/internal/operators/map';
import { MetadataSetting_GetAllOutputDTO } from 'src/app/core/dtos/output/metadata/MetadataSetting_GetAllOutputDTO';
import { environment } from 'src/environments/environment';
import * as automapper from 'automapper-ts';
import { Directory_GetAllOutputDTO } from 'src/app/core/dtos/output/directories/Directory_GetAllOutputDTO';
import { Directory_GetByIdOutputDTO } from 'src/app/core/dtos/output/directories/Directory_GetByIdOutputDTO';
import { MetadataField } from 'src/app/core/models/entity/metadata-field';
import { MetadataFieldSetting_InsertInputDTO } from 'src/app/core/dtos/input/metadata/MetadataFieldSetting_InsertInputDTO';
import { MetadataFieldSetting_UpdateInputDTO } from 'src/app/core/dtos/input/metadata/MetadataFieldSetting_UpdateInputDTO';
import { MetadataField_GetListItemByIdOutputDTO } from 'src/app/core/dtos/output/metadata/MetadataField_GetListItemByIdOutputDTO';
import { ListItem } from 'src/app/core/models/entity/list-item';
import { MetadataFieldSetting_GetAllOutputDTO } from 'src/app/core/dtos/output/metadata/MetadataFieldSetting_GetAllOutputDTO';
import { Directory_InsertInputDTO } from 'src/app/core/dtos/input/directory/Directory_InsertInputDTO';
import { Directory_UpdateInputDTO } from 'src/app/core/dtos/input/directory/Directory_UpdateInputDTO';
import { MediaItem, Media } from 'src/app/core/models/entity/media';
import { FieldsService } from '../../business/fields/fields.service';
import { Document_GetByIdOutputDTO } from 'src/app/core/dtos/output/documents/Document_GetByIdOutputDTO';
import { Document_GetByDirectoryIdOutputDTO } from 'src/app/core/dtos/output/documents/Document_GetByDirectoryIDOutputDTO';
import { Directory } from 'src/app/core/models/entity/directory';

@Injectable({
  providedIn: 'root'
})
export class DirectoryWebDataService implements DirectoryDataService {

  baseUrl = environment.api.baseUrl;

  constructor(private httpClient: HttpClient, private fieldsService: FieldsService) { }

  getMedia(directoryId: number, pageNumber: number, limit: number): Observable<Media> {
    var requestUri = environment.api.baseUrl + `/v1/directories/${directoryId}/documents`;
    let options = {
      params: new HttpParams()
    };
    if (directoryId) {
      options.params = options.params.set('pageNumber', pageNumber.toString());
      options.params = options.params.set('limit', limit.toString());
    }
    return this.httpClient.get<Document_GetByDirectoryIdOutputDTO>(requestUri, options)
      .pipe(
        map(
          response => {

            automapper
              .createMap(Document_GetByDirectoryIdOutputDTO, Media)
              .forMember('pagination', function (opts) { opts.mapFrom('pagination'); })
              .forMember('data', function (opts) { opts.mapFrom('data'); });
            let media: Media = automapper.map(Document_GetByDirectoryIdOutputDTO, Media, response);

            automapper
              .createMap(Document_GetByIdOutputDTO, MediaItem)
              .forMember('id', function (opts) { opts.mapFrom('documentId'); })
              .forMember('directoryId', function (opts) { opts.mapFrom('directoryId'); })
              .forMember('documentId', function (opts) { opts.mapFrom('documentId'); })
              .forMember('favoriteId', function (opts) { opts.mapFrom('favoriteId'); })
              .forMember('parentId', function (opts) { opts.mapFrom('directoryId'); })
              .forMember('directoryName', function (opts) { opts.mapFrom('DiirectoryName'); })
              .forMember('directoryParentId', function (opts) { opts.mapFrom('DirectoryParentId'); })
              .forMember('directoryParentName', function (opts) { opts.mapFrom('DirectoryParentName'); })
              .forMember('storageType', function (opts) { opts.mapFrom('StorageType'); })
              .forMember('entityType', function (opts) { opts.mapFrom('EntityType'); })
              .forMember('entityId', function (opts) { opts.mapFrom('EntityId'); })
              .forMember('documentTypeCode', function (opts) { opts.mapFrom('DocumentTypeCode'); })
              .forMember('name', function (opts) { opts.mapFrom('documentName'); })
              .forMember('url', function (opts) { opts.mapFrom('documentUrl'); })
              .forMember('metadata', function (opts) { opts.mapFrom('Metadata'); })
              .forMember('contentType', function (opts) { opts.mapFrom('ContentType'); })
              .forMember('containerId', function (opts) { opts.mapFrom('ContainerId'); })
              .forMember('size', function (opts) { opts.mapFrom('Size'); })
              .forMember('thumbnail', function (opts) { opts.mapFrom('thumbnailUrl'); })
              .forMember('thumbnails', function (opts) { opts.mapFrom('thumbnail'); })
              .forMember('isDeleted', function (opts) { opts.mapFrom('IsDeleted'); })
              .forMember('isFavorite', function (opts) { opts.mapFrom('isFavorite'); })
              .forMember('status', function (opts) { opts.mapFrom('Status'); })
              .forMember('modifiedOn', function (opts) { opts.mapFrom('modifiedOn'); })
              .forMember('hasChild', function () { return false; });

            let mediaItems: MediaItem[] = automapper.map(Document_GetByIdOutputDTO, MediaItem, media.data);
            mediaItems.forEach(item => {
              const docTypes = ['DOCX', 'CSV', 'DOC', 'TXT', 'XLS', 'XLSX', 'PDF', 'TELEMETRY'];
              const type = item.documentTypeCode ? item.documentTypeCode.toUpperCase() : '';
              if (docTypes.includes(type)) item.thumbnail = this.fieldsService.setPlaceholderByType(type);
              item.type = type;
              item.name = item.documentId ? item.name.toUpperCase() : item.name;
              item.nameWithoutExtension = item.name ? this.fieldsService.removeFileExtension(item.name) : 'UNKNOWN';
              item.parentId = item.parentId !== 0 ? item.parentId : null; // for treeview
            });
            media.data = mediaItems;
            return media;
          }
        )
      );
  }

  getFolders(parentId?: number): Observable<Directory[]> {
    let requestUri = this.baseUrl + `/v1/directories`;

    const options = {
      params: new HttpParams()
    };
    if (parentId) { options.params = options.params.set('parentId', parentId.toString()); }

    return this.httpClient.get<Directory_GetAllOutputDTO[]>(requestUri, options)
      .pipe(
        map(response => {
          automapper
            .createMap(Directory_GetAllOutputDTO, Directory)
            .forMember('id', function (opts) { opts.mapFrom('directoryId'); })
            .forMember('name', function (opts) { opts.mapFrom('directoryName'); })
            .forMember('parentId', function (opts) { opts.mapFrom('directoryParentId'); })
            .forMember('hasChild', function (opts) { opts.mapFrom('hasChild'); })
            .forMember('modifiedOn', function (opts) { opts.mapFrom('modifiedOn'); });

          let folders: Directory[] = automapper.map(Directory_GetAllOutputDTO, Directory, response);
          folders.forEach(item => item.parentId = item.parentId !== 0 ? item.parentId : null);
          return folders;
        })
      );
  }

  getFolder(id: number): Observable<Directory> {
    let requestUri = this.baseUrl + `/v1/directories/${id}`;
    return this.httpClient.get<Directory_GetByIdOutputDTO>(requestUri).pipe(
      map(response => {
        automapper
          .createMap(Directory_GetByIdOutputDTO, Directory)
          .forMember('id', function (opts) { opts.mapFrom('directoryId'); })
          .forMember('name', function (opts) { opts.mapFrom('directoryName'); })
          .forMember('parentId', function (opts) { opts.mapFrom('directoryParentId'); })
          .forMember('hasChild', function (opts) { opts.mapFrom('hasChild'); })
          .forMember('createdOn', function (opts) { opts.mapFrom('createdOn'); })
          .forMember('createdBy', function (opts) { opts.mapFrom('createdBy'); })
          .forMember('modifiedOn', function (opts) { opts.mapFrom('modifiedOn'); })
          .forMember('modifiedBy', function (opts) { opts.mapFrom('modifiedBy'); });

        let folder: Directory = automapper.map(Directory_GetByIdOutputDTO, Directory, response);
        return folder;
      })
    );
  }

  //#region Metadata Fields

  getMetadataFields(directoryId: number): Observable<MetadataField[]> {
    let requestUri = environment.api.baseUrl + `/v1/directories/${directoryId}/metadatafields`;

    return this.httpClient.get<MetadataSetting_GetAllOutputDTO[]>(requestUri).pipe(
      map(response => {
        automapper
          .createMap(MetadataFieldSetting_GetAllOutputDTO, MetadataField)
          .forMember('fieldId', function (opts) { opts.mapFrom('metadataFieldId'); })
          .forMember('parentId', function (opts) { opts.mapFrom('parentFieldId'); })
          .forMember('fieldName', function (opts) { opts.mapFrom('fieldName'); })
          .forMember('id', function (opts) { opts.mapFrom('metadataFieldId'); })
          .forMember('name', function (opts) { opts.mapFrom('fieldName'); })
          .forMember('label', function (opts) { opts.mapFrom('label'); })
          .forMember('type', function (opts) { opts.mapFrom('fieldTypeName'); })
          .forMember('dataType', function (opts) { opts.mapFrom('fieldDataType'); })
          .forMember('options', function (opts) { opts.mapFrom('options'); })
          .forMember('settingId', function (opts) { opts.mapFrom('metadataFieldSettingId'); })
          .forMember('metadataListName', function (opts) { opts.mapFrom('metadataListName'); })
          .forMember('fieldTypeName', function (opts) { opts.mapFrom('fieldTypeName'); })
          .forMember('entityId', function (opts) { opts.mapFrom('entityId'); })
          .forMember('entityName', function (opts) { opts.mapFrom('entityName'); })
          .forMember('isRequired', function (opts) { opts.mapFrom('isRequired'); })
          .forMember('order', function (opts) { opts.mapFrom('order'); })
          .forMember('value', function (opts) { opts.mapFrom('defaultValue'); })
          .forMember('status', function (opts) { opts.mapFrom('status'); });

        let metadataFields: MetadataField[] = automapper.map(MetadataFieldSetting_GetAllOutputDTO, MetadataField, response);
        metadataFields.forEach(field => {
          automapper
            .createMap(MetadataField_GetListItemByIdOutputDTO, ListItem)
            .forMember('id', function (opts) { opts.mapFrom('metadataListItemId'); })
            .forMember('parentId', function (opts) { opts.mapFrom('parentItemId'); })
            .forMember('value', function (opts) { opts.mapFrom('itemValue'); })
            .forMember('description', function (opts) { opts.mapFrom('itemDescription'); })
            .forMember('sort', function (opts) { opts.mapFrom('itemSort'); });

          let options: ListItem[] = automapper.map(MetadataField_GetListItemByIdOutputDTO, ListItem, field.options);
          field.options = options;
        });
        return metadataFields;
      })
    );
  }

  addMetadataField(folderId: number, payload: MetadataField) {
    const requestUri = environment.api.baseUrl + `/v1/metadatafieldsettings`;
    payload.entityId = folderId.toString();

    automapper
      .createMap(MetadataField, MetadataFieldSetting_InsertInputDTO)
      .forMember('metadataFieldId', function (opts) { opts.mapFrom('id'); })
      .forMember('entityId', function (opts) { opts.mapFrom('entityId'); })
      .forMember('isRequired', function (opts) { opts.mapFrom('isRequired'); })
      .forMember('order', function (opts) { opts.mapFrom('order'); })
      .forMember('value', function (opts) { opts.mapFrom('value'); });

    const request: MetadataFieldSetting_InsertInputDTO = automapper.map(MetadataField, MetadataFieldSetting_InsertInputDTO, payload);

    return this.httpClient.post<any>(requestUri, request);
  }

  updateMetadataField(folderId: number, payload: MetadataField) {
    const requestUri = environment.api.baseUrl + `/v1/metadatafieldsettings/${payload.settingId}`;

    automapper
      .createMap(MetadataField, MetadataFieldSetting_UpdateInputDTO)
      .forMember('metadataFieldId', function (opts) { opts.mapFrom('id'); })
      .forMember('entityId', function (opts) { opts.mapFrom('entityId'); })
      .forMember('isRequired', function (opts) { opts.mapFrom('isRequired'); })
      .forMember('order', function (opts) { opts.mapFrom('order'); })
      .forMember('value', function (opts) { opts.mapFrom('value'); });

    const request: MetadataFieldSetting_InsertInputDTO = automapper.map(MetadataField, MetadataFieldSetting_UpdateInputDTO, payload);

    return this.httpClient.put<any>(requestUri, request);
  }

  deleteMetadataField(settingId: number) {
    let requestUri = environment.api.baseUrl + `/v1/metadatafieldsettings/${settingId}`;
    return this.httpClient.delete(requestUri);
  }

  //#endregion

  createDirectory(payload: Directory) {
    const requestUri = environment.api.baseUrl + `/v1/directories`;
    automapper
      .createMap(Directory, Directory_InsertInputDTO)
      .forMember('directoryId', function (opts) { opts.mapFrom('id'); })
      .forMember('directoryName', function (opts) { opts.mapFrom('name'); })
      .forMember('directoryParentId', function (opts) { opts.mapFrom('parentId'); })

    const request = automapper.map(Directory, Directory_InsertInputDTO, payload);
    return this.httpClient.post<any>(requestUri, request)
      .pipe(
        map(
          response => {
            return response;
          })
      );
  }

  updateDirectory(id: number, payload: Directory) {
    const requestUri = environment.api.baseUrl + `/v1/directories/${id}`;

    automapper
      .createMap(Directory, Directory_UpdateInputDTO)
      .forMember('directoryId', function (opts) { opts.mapFrom('id'); })
      .forMember('directoryName', function (opts) { opts.mapFrom('name'); })
      .forMember('directoryParentId', function (opts) { opts.mapFrom('parentId'); })

    const request = automapper.map(Directory, Directory_UpdateInputDTO, payload);

    return this.httpClient.put(requestUri, request)
      .pipe(
        map(
          response => {
            automapper
              .createMap(Directory_UpdateInputDTO, Directory)
              .forMember('directoryId', (opts: AutoMapperJs.IMemberConfigurationOptions) => opts.mapFrom('id'))
              .forMember('directoryName', (opts: AutoMapperJs.IMemberConfigurationOptions) => opts.mapFrom('name'))
              .forMember('directoryParentId', (opts: AutoMapperJs.IMemberConfigurationOptions) => opts.mapFrom('parentId'));
            let _response = automapper.map(Directory_UpdateInputDTO, Directory, response);
            return _response;
          })
      );
  }

  removeDirectory(id: number) {
    const requestUri = environment.api.baseUrl + `/v1/directories/${id}`;
    return this.httpClient.delete(requestUri);
  }
}