import { Injectable } from '@angular/core';
import { LiveStreamDataService } from './live-stream-data.service';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { LiveStreams, LiveStream } from 'src/app/core/models/entity/live-stream';
import { Tag } from 'src/app/core/models/entity/tag';
import { Camera_OutputDTO, Camera_GetAllOutputDTO, Camera_GetByIdOutputDTO } from 'src/app/core/dtos/output/live-stream/live-stream';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';
import { Camera_GetByIdsInputDTO } from 'src/app/core/dtos/input/live-stream/Camera_GetByIdsInputDTO';
import { Favorite_InserInputDTO } from 'src/app/core/dtos/input/favorites/Favorite_InserInputDTO';

@Injectable({
  providedIn: 'root'
})
export class LiveStreamWebDataService implements LiveStreamDataService {
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    })
  };

  constructor(private httpClient: HttpClient) { }

  getLiveStreams(asset?: string, group?: string, pageNumber?: number, pageSize?: number, isTreeView?: boolean): Observable<LiveStreams> {
    let requestUri = environment.api.baseUrl + `/v1/livestreams`;
    const options = {
      params: new HttpParams()
    };
    if (asset) options.params = options.params.set('asset', asset);
    if (group) options.params = options.params.set('group', group);
    if (pageNumber) options.params = options.params.set('pageNumber', pageNumber.toString());
    if (pageSize) options.params = options.params.set('limit', pageSize.toString());

    return this.httpClient.get<Camera_OutputDTO>(requestUri, options)
      .pipe(
        map(
          response => {
            // Map response to liveStream.
            automapper
              .createMap(Camera_OutputDTO, LiveStreams)
              .forMember('pagination', function (opts) { opts.mapFrom('Pagination'); })
              .forMember('data', function (opts) { opts.mapFrom('Data'); });

            let liveStream: LiveStreams = automapper.map(Camera_OutputDTO, LiveStreams, response);
            console.log('LiveStreamWebDataService - getLiveStream : ', liveStream);

            // Map data from response to media items
            automapper
              .createMap(Camera_GetAllOutputDTO, LiveStream)
              .forMember('id', function (opts) { opts.mapFrom('cameraId'); })
              .forMember('url', function (opts) { opts.mapFrom('livestreamURL'); })
              .forMember('name', function (opts) { opts.mapFrom('name'); })
              .forMember('isFavorite', function (opts) { opts.mapFrom('isFavorite'); })
              .forMember('favoriteId', function (opts) { opts.mapFrom('favoriteId'); })
              .forMember('poster', function (opts) { opts.mapFrom('thumbnailUrl'); });
            let liveStreamItems: LiveStream[] = automapper.map(Camera_GetAllOutputDTO, LiveStream, liveStream.data);

            liveStreamItems.forEach(item => {
              const defaultImage = '../../../assets/images/photo-oceaneering.jpg';
              if (!item.poster) item.poster = defaultImage;


            });
            liveStream.data = liveStreamItems;
            return liveStream;
          })
      );
  }

  getLiveStream(id: number): Observable<LiveStream> {
    let requestUri = environment.api.baseUrl + `/v1/livestreams/${id}`;

    return this.httpClient.get<Camera_GetByIdOutputDTO>(requestUri).pipe(
      map(
        response => {
          automapper
            .createMap(Camera_GetByIdOutputDTO, LiveStream)
            .forMember('id', function (opts) { opts.mapFrom(' cameraId'); })
            .forMember('url', function (opts) { opts.mapFrom('secureUrl'); })
            .forMember('name', function (opts) { opts.mapFrom('name'); })
            .forMember('poster', function (opts) { opts.mapFrom('thumbnailUrl'); })
            .forMember('entityId', function (opts) { opts.mapFrom('EntityId'); });
          const liveStreamItem: LiveStream = automapper.map(Camera_GetByIdOutputDTO, LiveStream, response);
          return liveStreamItem;
        }
      )
    );;
  }

  getPlaylist(cameraIds: number[]): Observable<LiveStream[]> {
    let requestUri = environment.api.baseUrl + `/v1/livestreams/playlist`;
    const request: Camera_GetByIdsInputDTO = { cameraIds: cameraIds }
    return this.httpClient.post<Camera_GetByIdOutputDTO>(requestUri, request).pipe(
      map(
        response => {
          automapper
            .createMap(Camera_GetByIdOutputDTO, LiveStream)
            .forMember('id', function (opts) { opts.mapFrom(' cameraId'); })
            .forMember('url', function (opts) { opts.mapFrom('secureUrl'); })
            .forMember('name', function (opts) { opts.mapFrom('name'); })
            .forMember('poster', function (opts) { opts.mapFrom('thumbnailUrl'); });
          const liveStreamItem: LiveStream[] = automapper.map(Camera_GetByIdOutputDTO, LiveStream, response);
          return liveStreamItem;
        }
      )
    );;
  }

  applyFilters(filters: Tag[], pageNumber?: number, pageSize?: number): Observable<LiveStreams> {
    let requestUri = environment.api.baseUrl + `/v1/livestreams`;
    const options = {
      params: new HttpParams()
    };
    if (pageNumber) options.params = options.params.set('pageNumber', pageNumber.toString());
    if (pageSize) options.params = options.params.set('limit', pageSize.toString());

    filters.forEach(tag => {
      options.params = options.params.set(tag.name, tag.value);
    });

    return this.httpClient.get<Camera_OutputDTO>(requestUri, options)
      .pipe(
        map(
          response => {
            // Map response to liveStream.
            automapper
              .createMap(Camera_OutputDTO, LiveStreams)
              .forMember('pagination', function (opts) { opts.mapFrom('Pagination'); })
              .forMember('data', function (opts) { opts.mapFrom('Data'); });

            let liveStream: LiveStreams = automapper.map(Camera_OutputDTO, LiveStreams, response);
            console.log('LiveStreamWebDataService - getLiveStream : ', liveStream);

            // Map data from response to media items
            automapper
              .createMap(Camera_GetAllOutputDTO, LiveStream)
              .forMember('id', function (opts) { opts.mapFrom('cameraId'); })
              .forMember('url', function (opts) { opts.mapFrom('livestreamURL'); })
              .forMember('name', function (opts) { opts.mapFrom('name'); })
              .forMember('isFavorite', function (opts) { opts.mapFrom('isFavorite'); })
              .forMember('favoriteId', function (opts) { opts.mapFrom('favoriteId'); })
              .forMember('poster', function (opts) { opts.mapFrom('thumbnailUrl'); });
            let liveStreamItems: LiveStream[] = automapper.map(Camera_GetAllOutputDTO, LiveStream, liveStream.data);

            liveStreamItems.forEach(item => {
              const defaultImage = '../../../assets/images/photo-oceaneering.jpg';
              if (!item.poster) item.poster = defaultImage;
            });
            liveStream.data = liveStreamItems;
            return liveStream;
          })
      );
  }

  //#region Favorites

  getLiveStreamFavorites(pageNumber?: number, pageSize?: number): Observable<LiveStreams> {
    var requestUri = environment.api.baseUrl + `/v1/favorites/favoritelivestreams`;
    const options = {
      params: new HttpParams()
    };
    if (pageNumber) options.params = options.params.set('pageNumber', pageNumber.toString());
    if (pageSize) options.params = options.params.set('limit', pageSize.toString());

    return this.httpClient.get<Camera_OutputDTO>(requestUri, options).pipe(
      map(response => {
        // Map response to favorites
        automapper
          .createMap(Camera_OutputDTO, LiveStreams)
          .forMember('pagination', function (opts) { opts.mapFrom('Pagination'); })
          .forMember('data', function (opts) { opts.mapFrom('Data'); });

        let favorites: LiveStreams = automapper.map(Camera_OutputDTO, LiveStreams, response);

        // Map data from response to favorite items
        if (favorites.data.length > 0) {
          automapper
            .createMap(Camera_GetAllOutputDTO, LiveStream)
            .forMember('id', function (opts) { opts.mapFrom('cameraId'); })
            .forMember('favoriteId', function (opts) { opts.mapFrom('favoriteId'); })
            .forMember('url', function (opts) { opts.mapFrom('livestreamURL'); })
            .forMember('name', function (opts) { opts.mapFrom('name'); })
            .forMember('poster', function (opts) { opts.mapFrom('thumbnailUrl'); })
            .forMember('value', function (opts) { opts.mapFrom('value'); })
            .forMember('modifiedOn',function (opts) { opts.mapFrom('modifiedOn'); })
            .forMember('isFavorite', function () { return true; })

          let favoriteItems: LiveStream[] = automapper.map(Camera_GetAllOutputDTO, LiveStream, favorites.data);
          favoriteItems.forEach(item => {
            const defaultImage = './assets/images/photo-oceaneering.jpg';
            if (!item.poster) item.poster = defaultImage;
            if(item.value){
            item.favouriteIds =  JSON.parse(item.value);
            item.isMultiple = true;
            }
          });
          favorites.data = favoriteItems;
        }
        return favorites;
      })
    );
  }

  addLiveStreamFavorites(liveStreamItemId?: number, name?: string, livestreamIds?: number[]) {
    const requestUri = environment.api.baseUrl + `/v1/favorites`;
    const request: Favorite_InserInputDTO = {
      entityId: liveStreamItemId ? liveStreamItemId.toString() : null,
      entityType: 'Camera' ,
      filterName: name,
      value: livestreamIds ? JSON.stringify(livestreamIds) : null
    };

    console.log('LiveStreamWebDataService - addLiveStreamFavorite: ', request);

    return this.httpClient.post<any>(requestUri, request);
  }

  removeLiveStreamFavorites(favoriteId: number) {
    const requestUri = environment.api.baseUrl + `/v1/favorites/${favoriteId}`;
    return this.httpClient.delete<any>(requestUri);
  }
  //#endregion
}
