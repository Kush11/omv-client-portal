import { Injectable } from '@angular/core';
import { LiveStreamDataService } from './live-stream-data.service';
import { Observable } from 'rxjs';
import { LiveStreams, LiveStream } from 'src/app/core/models/entity/live-stream';
import { Tag } from 'src/app/core/models/entity/tag';

@Injectable({
  providedIn: 'root'
})
export class LiveStreamMockDataService implements LiveStreamDataService {

  constructor() { }

  getLiveStreams(asset?: string, group?: string, pageNumber?: number, pageSize?: number, isTreeView?: boolean): Observable<LiveStreams> {
    throw new Error("Method not implemented");
  }
  getLiveStream(payload: number): Observable<LiveStream> {
    throw new Error("Method not implemented");
  }
  getPlaylist(payload: number[]): Observable<LiveStream[]> {
    throw new Error("Method not implemented");
  }

  applyFilters(filters: Tag[], pageNumber?: number, pageSize?: number): Observable<LiveStreams> {
    throw new Error("Method not implemented");
  }

  //#region Favorites
  getLiveStreamFavorites(pageNumber?: number, pageSize?: number): Observable<LiveStreams> {
    throw new Error("Method not implemented")
  }
  addLiveStreamFavorites(liveStreamItemId?: number, name?: string, livestreamIds?: number[]) {
    throw new Error("Method not implemented")
  }
  removeLiveStreamFavorites(favoriteId: number) {
    throw new Error("Method not implemented")
  }
  //#endregion

}
