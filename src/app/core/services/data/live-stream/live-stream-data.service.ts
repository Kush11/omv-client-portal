import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { LiveStreams, LiveStream } from 'src/app/core/models/entity/live-stream';
import { Tag } from 'src/app/core/models/entity/tag';

@Injectable({
  providedIn: 'root'
})
export abstract class LiveStreamDataService {

  constructor() { }

  abstract getLiveStreams(asset?: string, group?: string, pageNumber?: number, pageSize?: number, isTreeView?: boolean): Observable<LiveStreams>;
  abstract getLiveStream(payload: number): Observable<LiveStream>;
  abstract getPlaylist(payload: number[]): Observable<LiveStream[]>;
  abstract applyFilters(filters: Tag[], pageNumber?: number, pageSize?: number): Observable<LiveStreams>;

  //#region Favorites

  abstract getLiveStreamFavorites(pageNumber?: number, pageSize?: number): Observable<LiveStreams>;
  abstract addLiveStreamFavorites(liveStreamItemId?: number, name?: string, livestreamIds?: number[]);
  abstract removeLiveStreamFavorites(favoriteId: number);

  //#endregion
}
