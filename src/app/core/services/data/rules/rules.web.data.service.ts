import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { map } from 'rxjs/internal/operators/map';
import { HttpClient } from '@angular/common/http';
import * as automapper from 'automapper-ts';
import { environment } from 'src/environments/environment';
import { Rule } from 'src/app/core/models/entity/rule';
import { RulesDataService } from './rules.data.service';
import { Rule_GetAllOutputDTO } from 'src/app/core/dtos/output/rules/Rule_GetAllOutputDTO';
import { Rule_GetByIdOutputDTO } from 'src/app/core/dtos/output/rules/Rule_GetByIdOutputDTO';
import { Rule_InsertInputDTO } from 'src/app/core/dtos/input/rules/Rule_InsertInputDTO';
import { Rule_UpdateInputDTO } from 'src/app/core/dtos/input/rules/Rule_UpdateInputDTO';
import { DateService } from '../../business/dates/date.service';

@Injectable({ providedIn: 'root' })
export class RulesWebDataService implements RulesDataService {

  baseUrl = environment.api.baseUrl;

  constructor(private httpClient: HttpClient, private dateService: DateService) { }

  getRules(): Observable<Rule[]> {
    const requestUri = `${this.baseUrl}/v1/rules`;
    return this.httpClient.get<Rule_GetAllOutputDTO>(requestUri)
      .pipe(
        map(response => {
          automapper
            .createMap(Rule_GetAllOutputDTO, Rule)
            .forMember('id', function (opts) { opts.mapFrom('ruleId'); })
            .forMember('name', function (opts) { opts.mapFrom('name'); })
            .forMember('assemblyName', function (opts) { opts.mapFrom('assemblyName'); })
            .forMember('assemblyPath', function (opts) { opts.mapFrom('assemblyPath'); })
            .forMember('class', function (opts) { opts.mapFrom('classNameWithNamespace'); })
            .forMember('approvalGroupId', function (opts) { opts.mapFrom('approvalGroupId'); })
            .forMember('approvalGroupName', function (opts) { opts.mapFrom('approvalGroupName'); })
            .forMember('createdOn', function (opts) { opts.mapFrom('createdOn'); })
            .forMember('createdBy', function (opts) { opts.mapFrom('createdBy'); })
            .forMember('modifiedOn', function (opts) { opts.mapFrom('modifiedOn'); })
            .forMember('modifiedBy', function (opts) { opts.mapFrom('modifiedBy'); });

          const rules: Rule[] = automapper.map(Rule_GetAllOutputDTO, Rule, response);
          rules.forEach(rule => {
            rule.modifiedOnString = this.dateService.formatToString(rule.modifiedOn);
          });
          return rules.sort((a, b) => new Date(b.modifiedOn).getTime() - new Date(a.modifiedOn).getTime());
        })
      );
  }

  getRule(id: number): Observable<Rule> {
    const requestUri = `${this.baseUrl}/v1/rules/${id}`;
    return this.httpClient.get<Rule_GetByIdOutputDTO>(requestUri)
      .pipe(
        map(response => {
          automapper
            .createMap(Rule_GetByIdOutputDTO, Rule)
            .forMember('id', function (opts) { opts.mapFrom('ruleId'); })
            .forMember('name', function (opts) { opts.mapFrom('name'); })
            .forMember('assemblyName', function (opts) { opts.mapFrom('assemblyName'); })
            .forMember('assemblyPath', function (opts) { opts.mapFrom('assemblyPath'); })
            .forMember('class', function (opts) { opts.mapFrom('classNameWithNamespace'); })
            .forMember('approvalGroupId', function (opts) { opts.mapFrom('approvalGroupId'); })
            .forMember('createdOn', function (opts) { opts.mapFrom('createdOn'); })
            .forMember('createdBy', function (opts) { opts.mapFrom('createdBy'); })
            .forMember('modifiedOn', function (opts) { opts.mapFrom('modifiedOn'); })
            .forMember('modifiedBy', function (opts) { opts.mapFrom('modifiedBy'); });

          const rule: Rule = automapper.map(Rule_GetByIdOutputDTO, Rule, response);
          return rule;
        })
      );
  }

  createRule(payload: Rule) {
    const requestUri = `${this.baseUrl}/v1/rules`;

    automapper
      .createMap(payload, Rule_InsertInputDTO)
      .forMember('name', function (opts) { opts.mapFrom('name'); })
      .forMember('assemblyName', function (opts) { opts.mapFrom('fileName'); })
      .forMember('assemblyPath', function () { return null; })
      .forMember('assemblyFile', function (opts) { opts.mapFrom('file'); })
      .forMember('classNameWithNamespace', function (opts) { opts.mapFrom('class'); })
      .forMember('approvalGroupId', function (opts) { opts.mapFrom('approvalGroupId'); });

    const request = automapper.map(payload, Rule_InsertInputDTO, payload);

    return this.httpClient.post(requestUri, request);
  }

  updateRule(id: number, payload: Rule) {
    const requestUri = `${this.baseUrl}/v1/rules/${id}`;

    automapper
      .createMap(payload, Rule_UpdateInputDTO)
      .forMember('name', function (opts) { opts.mapFrom('name'); })
      .forMember('assemblyName', function (opts) { opts.mapFrom('assemblyName'); })
      .forMember('assemblyPath', function () { return null; })
      .forMember('classNameWithNamespace', function (opts) { opts.mapFrom('class'); })
      .forMember('approvalGroupId', function (opts) { opts.mapFrom('approvalGroupId'); });

    const request = automapper.map(payload, Rule_UpdateInputDTO, payload);

    return this.httpClient.put(requestUri, request);
  }

  deleteRule(id: number) {
    const requestUri = `${this.baseUrl}/v1/rules/${id}`;
    return this.httpClient.delete(requestUri);
  }
}