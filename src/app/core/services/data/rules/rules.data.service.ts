import { Injectable } from '@angular/core';
import { Rule } from 'src/app/core/models/entity/rule';
import { Observable } from 'rxjs/internal/Observable';


@Injectable({
  providedIn: 'root'
})
export abstract class RulesDataService {
  
  abstract getRules(): Observable<Rule[]>;
  abstract getRule(id: number): Observable<Rule>;
  abstract createRule(payload: Rule);
  abstract updateRule(id: number, payload: Rule);
  abstract deleteRule(id: number);
}