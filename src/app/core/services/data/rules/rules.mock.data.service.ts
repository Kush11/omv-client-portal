import { Injectable } from '@angular/core';
import { RulesDataService } from './rules.data.service';
import { Rule } from 'src/app/core/models/entity/rule';
import { Observable } from 'rxjs/internal/Observable';

@Injectable({
  providedIn: 'root'
})
export class RulesMockDataService implements RulesDataService {
  getRules(): Observable<Rule[]> {
    throw new Error("Method not implemented.");
  }
  getRule(id: number): Observable<Rule> {
    throw new Error("Method not implemented.");
  }
  createRule(payload: Rule) {
    throw new Error("Method not implemented.");
  }
  updateRule(id: number, payload: Rule) {
    throw new Error("Method not implemented.");
  }
  deleteRule(id: number) {
    throw new Error("Method not implemented.");
  }
}