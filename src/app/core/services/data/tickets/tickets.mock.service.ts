import { Ticket } from 'src/app/core/models/entity/tickets';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { TicketsDataService } from './tickets.data.service';
import { HttpClient } from '@angular/common/http';
import { Document } from 'src/app/core/models/entity/document';


@Injectable({
    providedIn: 'root'
})

export class TicketsMockDataService implements TicketsDataService {

    constructor(private httpClient: HttpClient) { }

    createReportIssue(payload: Ticket, files?: Document[]): Observable<Ticket> {
        throw new Error("Method not implemented.");
    }
}