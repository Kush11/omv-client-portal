import { Ticket } from 'src/app/core/models/entity/tickets';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { Document } from 'src/app/core/models/entity/document';


@Injectable({
    providedIn: 'root'
})

export abstract class TicketsDataService {

    constructor() { }

    abstract createReportIssue(payload: Ticket, files?: Document[]): Observable<Ticket>;
}