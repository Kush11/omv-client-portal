import { Injectable } from '@angular/core';
import { TicketsDataService } from './tickets.data.service';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Ticket } from 'src/app/core/models/entity/tickets';
import { Observable } from 'rxjs';
import { Ticket_InsertInputDTO, DocumentDTO } from 'src/app/core/dtos/input/tickets/Ticket_InsertInputDTO';
import { map } from 'rxjs/operators';
import { Document } from 'src/app/core/models/entity/document';


@Injectable({
    providedIn: 'root'
})

export class TicketWebDataService implements TicketsDataService {

    baseUrl = environment.api.baseUrl;

    constructor(private httpClient: HttpClient) { }

    createReportIssue(payload: Ticket, files?: Document[]): Observable<Ticket> {
        const requestUri = environment.api.baseUrl + `/v1/tickets`;
        console.log('request', files);
        automapper
            .createMap(Document, DocumentDTO)
            .forMember('documentId', function (opts) { opts.mapFrom('id'); })
            .forMember('directoryId', function (opts) { opts.mapFrom('directoryId'); })
            .forMember('documentTypeCode', function (opts) { opts.mapFrom('documentTypeCode'); })
            .forMember('documentName', function (opts) { opts.mapFrom('name'); })
            .forMember('documentUrl', function (opts) { opts.mapFrom('documentUrl'); })
            .forMember('contentType', function (opts) { opts.mapFrom('contentType'); })
            .forMember('containerId', function (opts) { opts.mapFrom('containerId'); })
            .forMember('size', function (opts) { opts.mapFrom('size'); })
            .forMember('storageType', function () { return ''; })
            .forMember('entityType', function () { return ''; })
            .forMember('entityId', function () { return ''; })
            .forMember('isDeleted', function () { return false; })
            .forMember('status', function () { return 1; })
            .forMember('thumbnailContainerUrl', function (opts) { opts.mapFrom('thumbnailContainerUrl'); })
        const documents = automapper.map(Document, DocumentDTO, files);
        automapper
            .createMap(Ticket, Ticket_InsertInputDTO)
            .forMember('subject', function (opts) { opts.mapFrom('subject'); })
            .forMember('description', function (opts) { opts.mapFrom('message'); })
            .forMember('isFollowUp', function (opts) { opts.mapFrom('followUp'); })
            .forMember('availability', function (opts) { opts.mapFrom('availability '); })
            .forMember('startTime', function (opts) { opts.mapFrom('startTime'); })
            .forMember('endTime', function (opts) { opts.mapFrom('endTime'); })
            .forMember('documents', function (opts) { return documents || []; });
        const request = automapper.map(Ticket, Ticket_InsertInputDTO, payload);
        return this.httpClient.post(requestUri, request)
            .pipe(
                map((response: Ticket) => {
                    return response;
                })
            );
    }
}