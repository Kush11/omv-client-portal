import { Observable } from 'rxjs/internal/Observable';
import { FieldType } from 'src/app/core/models/entity/field-type';
import { FieldTypesDataService } from './field-types.data.service';
import { Injectable } from '@angular/core';
import { FieldType_GetAllOutputDTO } from 'src/app/core/dtos/output/metadata/FieldType_GetAllOutputDTO';
import { map } from 'rxjs/internal/operators/map';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class FieldTypesWebDataService implements FieldTypesDataService {

  baseUrl = environment.api.baseUrl;

  constructor(private httpClient: HttpClient) { }

  getFieldTypes(): Observable<FieldType[]> {
    var requestUri = this.baseUrl + `/v1/fieldtypes`;

    return this.httpClient.get<FieldType_GetAllOutputDTO[]>(requestUri)
      .pipe(
        map(response => {
          automapper
            .createMap(FieldType_GetAllOutputDTO, FieldType)
            .forMember('id', function (opts) { opts.mapFrom('fieldTypeId'); })
            .forMember('type', function (opts) { opts.mapFrom('type'); });

          let types: FieldType[] = automapper.map(FieldType_GetAllOutputDTO, FieldType, response);
          return types;
        })
      );
  }
}