import { Injectable } from '@angular/core';
import { FieldType } from 'src/app/core/models/entity/field-type';
import { Observable } from 'rxjs/internal/Observable';

@Injectable({
  providedIn: 'root'
})
export abstract class FieldTypesDataService {

  constructor() { }
  
  abstract getFieldTypes(): Observable<FieldType[]>;
}