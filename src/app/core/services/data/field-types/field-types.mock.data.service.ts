import { FieldType } from 'src/app/core/models/entity/field-type';
import { Observable } from 'rxjs/internal/Observable';
import { Injectable } from '@angular/core';
import { FieldTypesDataService } from './field-types.data.service';

@Injectable({
  providedIn: 'root'
})
export class FieldTypesMockDataService implements FieldTypesDataService {

  constructor() { }
    
  getFieldTypes(): Observable<FieldType[]> {
    throw new Error("Method not implemented.");
  }
}