import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { WorkTemplate, WorkTemplateProcess, WorkTemplateVersion, WorkTemplateStep, WorkTemplateHistory, WorkGroup, WorkTemplateField, WorkTemplateFields, WorkTemplateHistories } from 'src/app/core/models/entity/work-template';
import { MetadataField } from 'src/app/core/models/entity/metadata-field';
import { HttpEvent } from '@angular/common/http';
import { ListItem } from 'src/app/core/models/entity/list-item';

@Injectable({
  providedIn: 'root'
})
export abstract class WorkTemplatesDataService {

  //#region Work Templates

  abstract getGroups(status?: string): Observable<WorkTemplate[]>;
  abstract getGroup(id: number): Observable<WorkGroup>;

  //#endregion

  //#region Work Template

  abstract createTemplate(payload: WorkTemplate);
  abstract updateTemplate(id: number, payload: WorkTemplate);
  abstract activateTemplate(id: number, comment?: string);
  abstract downloadTemplate(id: number): Observable<HttpEvent<Blob>>;
  abstract importTemplate(id: number, file: string);

  //#endregion

  //#region Work Template Fields

  abstract getFields(templateId: number): Observable<WorkTemplateFields>;
  abstract updateFields(templateId: number, fields: WorkTemplateField[]);

  //#endregion

  //#region Work Template Process

  abstract createProcess(process: WorkTemplateProcess);
  abstract updateProcess(id: number, process: WorkTemplateProcess);
  abstract deleteProcess(id: number, workTemplateId: number);

  //#endregion

  //#region Work Template Step

  abstract createStep(step: WorkTemplateStep);
  abstract updateStep(step: WorkTemplateStep);
  abstract deleteStep(step: WorkTemplateStep);
  abstract sortSteps(processId: number, steps: WorkTemplateStep[]);

  //#endregion

  //#region Work Template Form

  abstract getForms(stepId: number): Observable<MetadataField[]>;
  abstract createForm(stepId: number, field: MetadataField);
  abstract updateForm(stepId: number, field: MetadataField);
  abstract deleteForm(stepId: number, field: MetadataField);

  //#endregion

  //#region Work Template Finding

  abstract getFindings(stepId: number): Observable<MetadataField[]>;
  abstract createFinding(stepId: number, field: MetadataField);
  abstract updateFinding(stepId: number, field: MetadataField);
  abstract deleteFinding(stepId: number, field: MetadataField);

  //#endregion

  //#region Work Template Version

  abstract getVersions(): Observable<WorkTemplateVersion[]>;
  abstract createVersion(version: WorkTemplateVersion);
  abstract updateVersion(id: number, version: WorkTemplateVersion);

  //#endregion

  //#region Work Template History

  abstract getHistory(id: number, pageNumber?: number, pageSize?: number): Observable<WorkTemplateHistories>;

  //#endregion

  //#region Work Template Metadata List Items

  abstract getMetadataListItems(templateId: number): Observable<ListItem[]>;

  //#endregion
}