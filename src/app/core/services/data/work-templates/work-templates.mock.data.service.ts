import { Injectable } from '@angular/core';
import { HttpClient, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs/internal/Observable';
import { WorkTemplate, WorkTemplateProcess, WorkTemplateHistory, WorkTemplateVersion, WorkTemplateStep, WorkGroup, WorkTemplateField, WorkTemplateFields, WorkTemplateHistories } from 'src/app/core/models/entity/work-template';
import { WorkTemplatesDataService } from './work-templates.data.service';
import { MetadataField } from 'src/app/core/models/entity/metadata-field';
import { ListItem } from 'src/app/core/models/entity/list-item';

@Injectable({
  providedIn: 'root'
})
export class WorkTemplatesMockDataService implements WorkTemplatesDataService {
  getHistory(id: number, pageNumber?: number, pageSize?: number): Observable<WorkTemplateHistories> {
    throw new Error("Method not implemented.");
  }
  updateFields(templateId: number, fields: WorkTemplateField[]) {
    throw new Error("Method not implemented.");
  }
  getFields(templateId: number): Observable<WorkTemplateFields> {
    throw new Error("Method not implemented.");
  }
  getMetadataListItems(templateId: number): Observable<ListItem[]> {
    throw new Error("Method not implemented.");
  }
  sortSteps(processId: number, steps: WorkTemplateStep[]) {
    throw new Error("Method not implemented.");
  }
  importTemplate(id: number, file: string) {
    throw new Error('Method not implemented.');
  }
  downloadTemplate(id: number): Observable<HttpEvent<Blob>> {
    throw new Error('Method not implemented.');
  }
  activateTemplate(id: number) {
    throw new Error('Method not implemented.');
  }
  getGroup(id: number): Observable<WorkGroup> {
    throw new Error('Method not implemented.');
  }
  getFindings(stepId: number): Observable<MetadataField[]> {
    throw new Error('Method not implemented.');
  }
  createFinding(stepId: number, field: MetadataField) {
    throw new Error('Method not implemented.');
  }
  updateFinding(stepId: number, field: MetadataField) {
    throw new Error('Method not implemented.');
  }
  deleteFinding(stepId: number, field: MetadataField) {
    throw new Error('Method not implemented.');
  }
  getForms(stepId: number): Observable<MetadataField[]> {
    throw new Error('Method not implemented.');
  }
  createForm(stepId: number, field: MetadataField) {
    throw new Error('Method not implemented.');
  }
  updateForm(stepId: number, field: MetadataField) {
    throw new Error('Method not implemented.');
  }
  deleteForm(stepId: number, field: MetadataField) {
    throw new Error('Method not implemented.');
  }

  getTemplate(groupId: any, version?: any): Observable<WorkTemplate> {
    throw new Error('Method not implemented.');
  }

  createStep(step: WorkTemplateStep) {
    throw new Error('Method not implemented.');
  }
  updateStep(step: WorkTemplateStep) {
    throw new Error('Method not implemented.');
  }
  createVersion(version: WorkTemplateVersion) {
    throw new Error('Method not implemented.');
  }
  updateVersion(id: number, version: WorkTemplateVersion) {
    throw new Error('Method not implemented.');
  }
  createProcess(process: WorkTemplateProcess) {
    throw new Error('Method not implemented.');
  }
  updateProcess(id: number, process: WorkTemplateProcess) {
    throw new Error('Method not implemented.');
  }

  constructor(private httpClient: HttpClient) { }

  createTemplate(payload: WorkTemplate) {
    throw new Error('Method not implemented.');
  }
  updateTemplate(id: number, payload: WorkTemplate) {
    throw new Error('Method not implemented.');
  }
  getGroups(): Observable<WorkTemplate[]> {
    throw new Error('Method not implemented.');
  }

  getWorkTemplatesProcesses(): Observable<WorkTemplateProcess[]> {
    throw new Error('Method not implemented.');
  }

  deleteProcess(id: number, workTemplateId: number) {
    throw new Error('Method not implemented.');
  }

  getVersions(): Observable<WorkTemplateVersion[]> {
    throw new Error('Method not implemented.');
  }

  deleteVersion(id: number) {
    throw new Error('Method not implemented.');
  }

  getWorkTemplatesHistory(): Observable<WorkTemplateHistory[]> {
    throw new Error('Method not implemented.');
  }


  getProcessType(): Observable<any[]> {
    throw new Error('Method not implemented.');
  }

  getWorkTemplatesSteps(): Observable<WorkTemplateStep[]> {
    throw new Error('Method not implemented.');
  }

  deleteStep(step: WorkTemplateStep) {
    throw new Error('Method not implemented.');
  }

  getStepType(): Observable<any[]> {
    throw new Error('Method not implemented.');
  }
}