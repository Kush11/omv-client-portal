import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { WorkTemplate, WorkGroup, WorkTemplateProcess, WorkTemplateVersion, WorkTemplateHistory, WorkTemplateStep, WorkTemplateField, WorkTemplateFields, WorkTemplateHistories } from 'src/app/core/models/entity/work-template';
import { WorkTemplatesDataService } from './work-templates.data.service';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpEvent, HttpRequest, HttpHeaders, HttpParams } from '@angular/common/http';
import { map } from 'rxjs/internal/operators/map';
import * as automapper from 'automapper-ts';
import { WorkTemplate_InsertInputDTO } from 'src/app/core/dtos/input/work-templates/WorkTemplate_InsertInputDTO';
import { WorkTemplate_UpdateInputDTO } from 'src/app/core/dtos/input/work-templates/WorkTemplate_UpdateInputDTO';
import { DateService } from '../../business/dates/date.service';
import {
	WorkTemplates_GetAllOutputDTO, WorkTemplateModel, WorkTemplateProcess_GetAllOutputDTO, WorkTemplateStep_GetAllOutputDTO, WorkTemplateVersion_GetAllOutputDTO,
	WorkTemplateVersionHistory_GetAllOutputDTO,
	WorkTemplateVersionHistory_GetAllOutputDTOData
} from 'src/app/core/dtos/output/work-templates/WorkTemplateModel';
import { WorkTemplateProcess_InsertInputDTO } from 'src/app/core/dtos/input/work-templates/WorkTemplateProcess_InsertInputDTO';
import { WorkTemplateProcess_UpdateInputDTO } from 'src/app/core/dtos/input/work-templates/WorkTemplateProcess_UpdateInputDTO';
import { MetadataFieldSetting_GetAllOutputDTO } from 'src/app/core/dtos/output/metadata/MetadataFieldSetting_GetAllOutputDTO';
import { MetadataField } from 'src/app/core/models/entity/metadata-field';
import { WorkTemplateStep_InsertInputDTO } from 'src/app/core/dtos/input/work-templates/WorkTemplateStep_InsertInputDTO';
import { WorkTemplateStep_UpdateInputDTO } from 'src/app/core/dtos/input/work-templates/WorkTemplateStep_UpdateInputDTO';
import { ListItem } from 'src/app/core/models/entity/list-item';
import { MetadataField_GetListItemByIdOutputDTO } from 'src/app/core/dtos/output/metadata/MetadataField_GetListItemByIdOutputDTO';
import { MetadataFieldSetting_InsertInputDTO } from 'src/app/core/dtos/input/metadata/MetadataFieldSetting_InsertInputDTO';
import { MetadataFieldSetting_UpdateInputDTO } from 'src/app/core/dtos/input/metadata/MetadataFieldSetting_UpdateInputDTO';
import { SortSteps_InputDTO } from 'src/app/core/dtos/input/work-templates/SortSteps_InputDTO';
import { WorkTemplateFieldOutputDTO } from 'src/app/core/dtos/output/work-templates/WorkTemplateFieldOutputDTO';
import { WorkItemFormData } from 'src/app/core/dtos/output/work-planning/WorkItemFormData';

@Injectable({
	providedIn: 'root'
})
export class WorkTemplatesWebDataService implements WorkTemplatesDataService {

	baseUrl = environment.api.baseUrl;

	constructor(private httpClient: HttpClient, private dateService: DateService) { }

	getGroups(status?: string): Observable<WorkTemplate[]> {
		const requestUri = `${this.baseUrl}/v1/worktemplategroups`;

		const options = { params: new HttpParams() }
		if (status) options.params = options.params.set('status', status);		

		return this.httpClient.get<WorkTemplates_GetAllOutputDTO[]>(requestUri, options)
			.pipe(
				map(response => {
					automapper
						.createMap(WorkTemplates_GetAllOutputDTO, WorkTemplate)
						.forMember('id', function (opts) { opts.mapFrom('workTemplateId'); })
						.forMember('groupId', function (opts) { opts.mapFrom('workTemplateGroupId'); })
						.forMember('name', function (opts) { opts.mapFrom('name'); })
						.forMember('activeId', function (opts) { opts.mapFrom('activeWorkTemplateId'); })
						.forMember('metadataListId', function (opts) { opts.mapFrom('metadataListId'); })
						.forMember('version', function (opts) { opts.mapFrom('version'); })
						.forMember('workType', function (opts) { opts.mapFrom('workType'); })
						.forMember('workItem', function (opts) { opts.mapFrom('workItem'); })
						.forMember('workItemParent', function (opts) { opts.mapFrom('workItemParent'); })
						.forMember('modifiedOn', function (opts) { opts.mapFrom('modifiedOn'); })
						.forMember('modifiedBy', function (opts) { opts.mapFrom('modifiedBy'); })
						.forMember('workTemplateProcess', function (opts) { opts.mapFrom('workTemplateProcess'); });

					let templates: WorkTemplate[] = automapper.map(WorkTemplates_GetAllOutputDTO, WorkTemplate, response);
					templates.forEach(template => template.modifiedOnString = this.dateService.formatToString(template.modifiedOn));
					return templates;
				})
			);
	}

	getGroup(id: number): Observable<WorkGroup> {
		const requestUri = `${this.baseUrl}/v1/worktemplategroups/${id}`;

		return this.httpClient.get<WorkTemplateModel>(requestUri)
			.pipe(
				map(response => {
					return this.mapGroup(response);
				})
			);
	}

	createTemplate(payload: WorkTemplate) {
		const requestUri = `${this.baseUrl}/v1/worktemplategroups`;

		automapper
			.createMap(WorkTemplate, WorkTemplate_InsertInputDTO)
			.forMember('id', function (opts) { opts.mapFrom('workTemplateId'); })
			.forMember('name', function (opts) { opts.mapFrom('name'); })
			.forMember('workType', function (opts) { opts.mapFrom('workType'); })
			.forMember('workItem', function (opts) { opts.mapFrom('workItem'); })
			.forMember('assetParent', function (opts) { opts.mapFrom('assetParent'); })
			.forMember('workItemParent', function (opts) { opts.mapFrom('workItemParent'); });

		const request = automapper.map(WorkTemplate, WorkTemplate_InsertInputDTO, payload);

		return this.httpClient.post(requestUri, request);
	}

	updateTemplate(id: number, payload: WorkTemplate) {
		const requestUri = `${this.baseUrl}/v1/worktemplategroups/${id}`;

		automapper
			.createMap(WorkTemplate, WorkTemplate_UpdateInputDTO)
			.forMember('id', function (opts) { opts.mapFrom('workTemplateId'); })
			.forMember('workType', function (opts) { opts.mapFrom('workType'); })
			.forMember('workItem', function (opts) { opts.mapFrom('workItem'); })
			.forMember('assetParent', function (opts) { opts.mapFrom('assetParent'); })
			.forMember('workItemParent', function (opts) { opts.mapFrom('workItemParent'); });

		const request = automapper.map(WorkTemplate, WorkTemplate_UpdateInputDTO, payload);

		return this.httpClient.put(requestUri, request);
	}

	activateTemplate(id: number, comment?: string) {
		const requestUri = `${this.baseUrl}/v1/worktemplates/${id}/activate`;
		return this.httpClient.post(requestUri, { field: comment });
	}

	downloadTemplate(id: number): Observable<HttpEvent<Blob>> {
		const request = new HttpRequest('GET', `${this.baseUrl}/v1/worktemplates/${id}/download`, null, {
			reportProgress: true,
			responseType: 'blob'
		});
		return this.httpClient.request(request);
	}

	importTemplate(id: number, file: string) {
		const requestUri = `${this.baseUrl}/v1/worktemplategroups/${id}/import`;
		return this.httpClient.post<WorkTemplateModel>(requestUri, { field: file })
			.pipe(
				map(response => {
					return this.mapGroup(response);
				})
			);
	}

	private mapGroup(group: WorkTemplateModel): WorkGroup {
		automapper
			.createMap(WorkTemplateModel, WorkGroup)
			.forMember('workTemplates', function (opts) { opts.mapFrom('workTemplates'); })
			.forMember('workTemplateVersions', function (opts) { opts.mapFrom('workTemplateVersions'); })
			.forMember('workTemplateHistories', function (opts) { opts.mapFrom('workTemplateVersionHistory'); });

		const workGroup: WorkGroup = automapper.map(WorkTemplateModel, WorkGroup, group);

		// Work Template Versions
		automapper
			.createMap(WorkTemplateVersion_GetAllOutputDTO, WorkTemplateVersion)
			.forMember('templateId', function (opts) { opts.mapFrom('workTemplateId'); })
			.forMember('version', function (opts) { opts.mapFrom('version'); })
			.forMember('status', function (opts) { opts.mapFrom('statusName'); })
			.forMember('comment', function (opts) { opts.mapFrom('comment'); })
			.forMember('publishedOn', function (opts) { opts.mapFrom('publishedOn'); })
			.forMember('publishedBy', function (opts) { opts.mapFrom('publishedBy'); });

		const versions: WorkTemplateVersion[] = automapper.map(WorkTemplateVersion_GetAllOutputDTO, WorkTemplateVersion, workGroup.workTemplateVersions);
		workGroup.workTemplateVersions = versions || [];

		// Work Templates
		automapper
			.createMap(WorkTemplates_GetAllOutputDTO, WorkTemplate)
			.forMember('id', function (opts) { opts.mapFrom('workTemplateId'); })
			.forMember('groupId', function (opts) { opts.mapFrom('workTemplateGroupId'); })
			.forMember('name', function (opts) { opts.mapFrom('name'); })
			.forMember('version', function (opts) { opts.mapFrom('version'); })
			.forMember('workType', function (opts) { opts.mapFrom('workType'); })
			.forMember('workItem', function (opts) { opts.mapFrom('workItem'); })
			.forMember('workItemParent', function (opts) { opts.mapFrom('workItemParent'); })
			.forMember('status', function (opts) { opts.mapFrom('statusName'); })
			.forMember('assetParent', function (opts) { opts.mapFrom('assetParent'); })
			.forMember('fields', function (opts) { opts.mapFrom('workTemplateFields'); })
			.forMember('processes', function (opts) { opts.mapFrom('workTemplateProcess'); });

		const workTemplates: WorkTemplate[] = automapper.map(WorkTemplates_GetAllOutputDTO, WorkTemplate, workGroup.workTemplates);
		workGroup.workTemplates = workTemplates || [];

		// Work Template Fields
		workGroup.workTemplates.forEach(template => {
			automapper
				.createMap(MetadataFieldSetting_GetAllOutputDTO, MetadataField)
				.forMember('id', function (opts) { opts.mapFrom('metadataFieldSettingId'); })
				.forMember('fieldId', function (opts) { opts.mapFrom('metadataFieldId'); })
				.forMember('parentId', function (opts) { opts.mapFrom('parentFieldId'); })
				.forMember('fieldName', function (opts) { opts.mapFrom('fieldName'); })
				.forMember('name', function (opts) { opts.mapFrom('fieldName'); })
				.forMember('label', function (opts) { opts.mapFrom('label'); })
				.forMember('type', function (opts) { opts.mapFrom('fieldType'); })
				.forMember('options', function (opts) { opts.mapFrom('options'); })
				.forMember('metadataListName', function (opts) { opts.mapFrom('metadataListName'); })
				.forMember('fieldTypeName', function (opts) { opts.mapFrom('fieldTypeName'); })
				.forMember('entityId', function (opts) { opts.mapFrom('entityId'); })
				.forMember('entityName', function (opts) { opts.mapFrom('entityName'); })
				.forMember('isRequired', function (opts) { opts.mapFrom('isRequired'); })
				.forMember('order', function (opts) { opts.mapFrom('order'); })
				.forMember('value', function (opts) { opts.mapFrom('defaultValue'); })
				.forMember('status', function (opts) { opts.mapFrom('status'); });

			const fields: MetadataField[] = automapper.map(MetadataFieldSetting_GetAllOutputDTO, MetadataField, template.fields);
			template.fields = fields || [];
		});

		// Work Templates Processes
		workGroup.workTemplates.forEach(template => {
			automapper
				.createMap(WorkTemplateProcess_GetAllOutputDTO, WorkTemplateProcess)
				.forMember('id', function (opts) { opts.mapFrom('workTemplateProcessId'); })
				.forMember('name', function (opts) { opts.mapFrom('name'); })
				.forMember('code', function (opts) { opts.mapFrom('code'); })
				.forMember('steps', function (opts) { opts.mapFrom('workTemplateStep'); })
				.forMember('templateId', function () { return template.id });

			const processes: WorkTemplateProcess[] = automapper.map(WorkTemplateProcess_GetAllOutputDTO, WorkTemplateProcess, template.processes);
			template.processes = processes || [];

			// Work Template Steps
			template.processes.forEach(process => {
				automapper
					.createMap(WorkTemplateStep_GetAllOutputDTO, WorkTemplateStep)
					.forMember('id', function (opts) { opts.mapFrom('workTemplateStepId'); })
					.forMember('name', function (opts) { opts.mapFrom('name'); })
					.forMember('code', function (opts) { opts.mapFrom('code'); })
					.forMember('notifyByGroup', function (opts) { opts.mapFrom('notifyByGroup'); })
					.forMember('templateCode', function (opts) { opts.mapFrom('templateCode'); })
					.forMember('advanceButton', function (opts) { opts.mapFrom('advanceButton'); })
					.forMember('advancedByGroup', function (opts) { opts.mapFrom('advancedByGroup'); })
					.forMember('forms', function (opts) { opts.mapFrom('workTemplateForm'); })
					.forMember('findings', function (opts) { opts.mapFrom('workTemplateFinding'); })
					.forMember('processId', function () { return process.id; });

				const steps: WorkTemplateStep[] = automapper.map(WorkTemplateStep_GetAllOutputDTO, WorkTemplateStep, process.steps);
				process.steps = steps || [];

				// Work Template Forms & Findings
				process.steps.forEach(step => {
					automapper
						.createMap(MetadataFieldSetting_GetAllOutputDTO, MetadataField)
						.forMember('id', function (opts) { opts.mapFrom('metadataFieldSettingId'); })
						.forMember('fieldId', function (opts) { opts.mapFrom('metadataFieldId'); })
						.forMember('parentId', function (opts) { opts.mapFrom('parentFieldId'); })
						.forMember('fieldName', function (opts) { opts.mapFrom('fieldName'); })
						.forMember('name', function (opts) { opts.mapFrom('fieldName'); })
						.forMember('label', function (opts) { opts.mapFrom('label'); })
						.forMember('type', function (opts) { opts.mapFrom('fieldTypeName'); })
						.forMember('options', function (opts) { opts.mapFrom('options'); })
						.forMember('metadataListName', function (opts) { opts.mapFrom('metadataListName'); })
						.forMember('fieldTypeName', function (opts) { opts.mapFrom('fieldTypeName'); })
						.forMember('entityId', function (opts) { opts.mapFrom('entityId'); })
						.forMember('entityName', function (opts) { opts.mapFrom('entityName'); })
						.forMember('isRequired', function (opts) { opts.mapFrom('isRequired'); })
						.forMember('order', function (opts) { opts.mapFrom('order'); })
						.forMember('value', function (opts) { opts.mapFrom('defaultValue'); })
						.forMember('status', function (opts) { opts.mapFrom('status'); });

					const forms: MetadataField[] = automapper.map(MetadataFieldSetting_GetAllOutputDTO, MetadataField, step.forms);
					const findings: MetadataField[] = automapper.map(MetadataFieldSetting_GetAllOutputDTO, MetadataField, step.findings);
					step.forms = forms || [];
					step.findings = findings || [];
				});
			});
		});
		return workGroup;
	}

	//#region Work Template Field

	getFields(templateId: number): Observable<WorkTemplateFields> {
		const requestUri = `${this.baseUrl}/v1/worktemplates/${templateId}/fields`;
		return this.httpClient.get<string>(requestUri)
			.pipe(
				map(response => {
					automapper
						.createMap(WorkTemplateFieldOutputDTO, WorkTemplateFields)
						.forMember('fields', function (opts) { opts.mapFrom('fields'); })
						.forMember('processes', function (opts) { opts.mapFrom('processes'); });

					let model: WorkTemplateFields = automapper.map(WorkTemplateFieldOutputDTO, WorkTemplateFields, response);

					// Mapping MetadataFields
					automapper
						.createMap(WorkItemFormData, MetadataField)
						.forMember('name', function (opts) { opts.mapFrom('fieldName'); })
						.forMember('label', function (opts) { opts.mapFrom('label'); })
						.forMember('value', function (opts) { opts.mapFrom('value'); })
						.forMember('type', function (opts) { opts.mapFrom('fieldType'); })
						.forMember('canEdit', function (opts) { opts.mapFrom('canEdit'); })
						.forMember('canUpload', function (opts) { opts.mapFrom('canUpload'); })
						.forMember('isUnique', function (opts) { opts.mapFrom('isUnique'); })
						.forMember('isRequired', function (opts) { opts.mapFrom('isRequired'); })
						.forMember('options', function (opts) { opts.mapFrom('options'); });

					let metadataFields: MetadataField[] = automapper.map(WorkItemFormData, MetadataField, model.fields);
					metadataFields.forEach(field => {
						automapper
							.createMap(MetadataField_GetListItemByIdOutputDTO, ListItem)
							.forMember('id', function (opts) { opts.mapFrom('metadataListItemId'); })
							.forMember('parentId', function (opts) { opts.mapFrom('parentItemId'); })
							.forMember('value', function (opts) { opts.mapFrom('itemValue'); })
							.forMember('description', function (opts) { opts.mapFrom('itemDescription'); })
							.forMember('sort', function (opts) { opts.mapFrom('itemSort'); });

						let options: ListItem[] = automapper.map(MetadataField_GetListItemByIdOutputDTO, ListItem, field.options);
						field.options = options;
					});
					model.fields = metadataFields;

					// Work Templates Processes
					automapper
						.createMap(WorkTemplateProcess_GetAllOutputDTO, WorkTemplateProcess)
						.forMember('id', function (opts) { opts.mapFrom('workTemplateProcessId'); })
						.forMember('name', function (opts) { opts.mapFrom('name'); })
						.forMember('code', function (opts) { opts.mapFrom('code'); })
						.forMember('steps', function (opts) { opts.mapFrom('workTemplateStep'); })
						.forMember('templateId', function (opts) { opts.mapFrom('workTemplateId'); });

					const processes: WorkTemplateProcess[] = automapper.map(WorkTemplateProcess_GetAllOutputDTO, WorkTemplateProcess, model.processes);

					// Work Template Steps
					processes.forEach(process => {
						automapper
							.createMap(WorkTemplateStep_GetAllOutputDTO, WorkTemplateStep)
							.forMember('id', function (opts) { opts.mapFrom('workTemplateStepId'); })
							.forMember('name', function (opts) { opts.mapFrom('name'); })
							.forMember('code', function (opts) { opts.mapFrom('code'); })
							.forMember('notifyByGroup', function (opts) { opts.mapFrom('notifyByGroup'); })
							.forMember('templateCode', function (opts) { opts.mapFrom('templateCode'); })
							.forMember('advanceButton', function (opts) { opts.mapFrom('advanceButton'); })
							.forMember('advancedByGroup', function (opts) { opts.mapFrom('advancedByGroup'); })
							.forMember('forms', function (opts) { opts.mapFrom('workTemplateForm'); })
							.forMember('findings', function (opts) { opts.mapFrom('workTemplateFinding'); })
							.forMember('processId', function () { return process.id; });

						const steps: WorkTemplateStep[] = automapper.map(WorkTemplateStep_GetAllOutputDTO, WorkTemplateStep, process.steps);
						process.steps = steps || [];

						// Work Template Forms & Findings
						process.steps.forEach(step => {
							automapper
								.createMap(MetadataFieldSetting_GetAllOutputDTO, MetadataField)
								.forMember('id', function (opts) { opts.mapFrom('metadataFieldSettingId'); })
								.forMember('fieldId', function (opts) { opts.mapFrom('metadataFieldId'); })
								.forMember('parentId', function (opts) { opts.mapFrom('parentFieldId'); })
								.forMember('fieldName', function (opts) { opts.mapFrom('fieldName'); })
								.forMember('name', function (opts) { opts.mapFrom('fieldName'); })
								.forMember('label', function (opts) { opts.mapFrom('label'); })
								.forMember('type', function (opts) { opts.mapFrom('fieldTypeName'); })
								.forMember('options', function (opts) { opts.mapFrom('options'); })
								.forMember('metadataListName', function (opts) { opts.mapFrom('metadataListName'); })
								.forMember('fieldTypeName', function (opts) { opts.mapFrom('fieldTypeName'); })
								.forMember('entityId', function (opts) { opts.mapFrom('entityId'); })
								.forMember('entityName', function (opts) { opts.mapFrom('entityName'); })
								.forMember('isRequired', function (opts) { opts.mapFrom('isRequired'); })
								.forMember('order', function (opts) { opts.mapFrom('order'); })
								.forMember('value', function (opts) { opts.mapFrom('defaultValue'); })
								.forMember('status', function (opts) { opts.mapFrom('status'); });

							const forms: MetadataField[] = automapper.map(MetadataFieldSetting_GetAllOutputDTO, MetadataField, step.forms);
							const findings: MetadataField[] = automapper.map(MetadataFieldSetting_GetAllOutputDTO, MetadataField, step.findings);
							step.forms = forms || [];
							step.findings = findings || [];
						});
					});
					model.processes = processes;

					return model;
				})
			);
	}


	updateFields(templateId: number, fields: WorkTemplateField[]) {
		const requestUri = `${this.baseUrl}/v1/worktemplates/${templateId}`;
		const requestBody = { fieldJson: JSON.stringify(fields) };

		return this.httpClient.put(requestUri, requestBody);
	}

	//#endregion

	//#region Work Template Process

	createProcess(process: WorkTemplateProcess) {
		const requestUri = `${this.baseUrl}/v1/worktemplates/${process.templateId}/processes`;

		automapper
			.createMap(WorkTemplateProcess, WorkTemplateProcess_InsertInputDTO)
			.forMember('name', function (opts) { opts.mapFrom('name'); })
			.forMember('code', function (opts) { opts.mapFrom('code'); })
			.forMember('workTemplateId', function (opts) { opts.mapFrom('templateId'); });

		const request = automapper.map(WorkTemplateProcess, WorkTemplateProcess_InsertInputDTO, process);
		console.log('WorkTemplateWebService createWorkTemplateProcess request: ', request);

		return this.httpClient.post(requestUri, request);
	}

	updateProcess(id: number, process: WorkTemplateProcess) {
		const requestUri = `${this.baseUrl}/v1/worktemplates/${process.templateId}/processes/${process.id}`;

		automapper
			.createMap(WorkTemplateProcess, WorkTemplateProcess_UpdateInputDTO)
			.forMember('workTemplatepProcessId', function (opts) { opts.mapFrom('id'); })
			.forMember('name', function (opts) { opts.mapFrom('name'); })
			.forMember('code', function (opts) { opts.mapFrom('code'); })
			.forMember('workTemplateId', function (opts) { opts.mapFrom('templateId'); });

		const request = automapper.map(WorkTemplateProcess, WorkTemplateProcess_UpdateInputDTO, process);
		console.log('WorkTemplateWebService updateWorkTemplateProcess request: ', request);

		return this.httpClient.put(requestUri, request);
	}

	deleteProcess(id: number, templateId: number) {
		const requestUri = `${this.baseUrl}/v1/worktemplates/${templateId}/processes/${id}`;
		return this.httpClient.delete(requestUri);
	}

	//#endregion

	//#region Work Template Step

	createStep(step: WorkTemplateStep) {
		const requestUri = `${this.baseUrl}/v1/worktemplateprocesses/${step.processId}/steps`;

		automapper
			.createMap(WorkTemplateStep, WorkTemplateStep_InsertInputDTO)
			.forMember('workTemplateStepId', function (opts) { opts.mapFrom('id'); })
			.forMember('workTemplateProcessId', function (opts) { opts.mapFrom('processId'); })
			.forMember('code', function (opts) { opts.mapFrom('code'); })
			.forMember('name', function (opts) { opts.mapFrom('name'); })
			.forMember('notifyGroup', function (opts) { opts.mapFrom('notifyByGroup'); })
			.forMember('emailTemplateCode', function (opts) { opts.mapFrom('templateCode'); })
			.forMember('advanceButton', function (opts) { opts.mapFrom('advanceButton'); })
			.forMember('advanceGroup', function (opts) { opts.mapFrom('advancedByGroup'); })
			.forMember('order', function (opts) { opts.mapFrom('order'); })
			.forMember('workTemplateId', function (opts) { opts.mapFrom('workTemplateId'); });

		const request = automapper.map(WorkTemplateStep, WorkTemplateStep_InsertInputDTO, step);
		console.log('WorkTemplateWebService steps request: ', request);

		return this.httpClient.post(requestUri, request);
	}

	updateStep(step: WorkTemplateStep) {
		const requestUri = `${this.baseUrl}/v1/worktemplateprocesses/${step.processId}/steps/${step.id}`;

		automapper
			.createMap(WorkTemplateStep, WorkTemplateStep_UpdateInputDTO)
			.forMember('workTemplateStepId', function (opts) { opts.mapFrom('id'); })
			.forMember('workTemplateProcessId', function (opts) { opts.mapFrom('processId'); })
			.forMember('code', function (opts) { opts.mapFrom('code'); })
			.forMember('name', function (opts) { opts.mapFrom('name'); })
			.forMember('notifyGroup', function (opts) { opts.mapFrom('notifyByGroup'); })
			.forMember('emailTemplateCode', function (opts) { opts.mapFrom('templateCode'); })
			.forMember('advanceButton', function (opts) { opts.mapFrom('advanceButton'); })
			.forMember('advanceGroup', function (opts) { opts.mapFrom('advancedByGroup'); })
			.forMember('order', function (opts) { opts.mapFrom('order'); })
			.forMember('workTemplateId', function (opts) { opts.mapFrom('workTemplateId'); });

		const request = automapper.map(WorkTemplateStep, WorkTemplateStep_UpdateInputDTO, step);

		return this.httpClient.put(requestUri, request);
	}

	deleteStep(step: WorkTemplateStep) {
		const requestUri = `${this.baseUrl}/v1/worktemplateprocesses/${step.processId}/steps/${step.id}`;
		return this.httpClient.delete(requestUri);
	}

	sortSteps(processId: number, steps: WorkTemplateStep[]) {
		const requestUri = `${this.baseUrl}/v1/worktemplateprocesses/${processId}/steps/sortsteps`;

		automapper
			.createMap(WorkTemplateStep, SortSteps_InputDTO)
			.forMember('stepId', function (opts) { opts.mapFrom('id'); })
			.forMember('order', function (opts) { opts.mapFrom('order'); });

		const request = automapper.map(WorkTemplateStep, SortSteps_InputDTO, steps);
		return this.httpClient.post(requestUri, request);
	}

	//#endregion

	//#region Work Template Form

	getForms(stepId: number): Observable<MetadataField[]> {
		const requestUri = `${this.baseUrl}/v1/worktemplatesteps/${stepId}/forms`;

		return this.httpClient.get<MetadataFieldSetting_GetAllOutputDTO[]>(requestUri)
			.pipe(
				map(response => {
					automapper
						.createMap(MetadataFieldSetting_GetAllOutputDTO, MetadataField)
						.forMember('settingId', function (opts) { opts.mapFrom('metadataFieldSettingId'); })
						.forMember('id', function (opts) { opts.mapFrom('metadataFieldId'); })
						.forMember('parentId', function (opts) { opts.mapFrom('parentFieldId'); })
						.forMember('fieldName', function (opts) { opts.mapFrom('fieldName'); })
						.forMember('name', function (opts) { opts.mapFrom('fieldName'); })
						.forMember('label', function (opts) { opts.mapFrom('label'); })
						.forMember('type', function (opts) { opts.mapFrom('fieldTypeName'); })
						.forMember('options', function (opts) { opts.mapFrom('options'); })
						.forMember('metadataListName', function (opts) { opts.mapFrom('metadataListName'); })
						.forMember('entityId', function (opts) { opts.mapFrom('entityId'); })
						.forMember('overrideLabel', function (opts) { opts.mapFrom('overrideLabel'); })
						.forMember('entityName', function (opts) { opts.mapFrom('entityName'); })
						.forMember('isRequired', function (opts) { opts.mapFrom('isRequired'); })
						.forMember('isEditable', function (opts) { opts.mapFrom('isEditable'); })
						.forMember('allowDocumentUpload', function (opts) { opts.mapFrom('allowDocumentUpload'); })
						.forMember('order', function (opts) { opts.mapFrom('order'); })
						.forMember('value', function (opts) { opts.mapFrom('defaultValue'); })
						.forMember('status', function (opts) { opts.mapFrom('status'); });

					let metadataFields: MetadataField[] = automapper.map(MetadataFieldSetting_GetAllOutputDTO, MetadataField, response);
					metadataFields.forEach(field => {
						automapper
							.createMap(MetadataField_GetListItemByIdOutputDTO, ListItem)
							.forMember('id', function (opts) { opts.mapFrom('metadataListItemId'); })
							.forMember('parentId', function (opts) { opts.mapFrom('parentItemId'); })
							.forMember('value', function (opts) { opts.mapFrom('itemValue'); })
							.forMember('description', function (opts) { opts.mapFrom('itemDescription'); })
							.forMember('sort', function (opts) { opts.mapFrom('itemSort'); });

						let options: ListItem[] = automapper.map(MetadataField_GetListItemByIdOutputDTO, ListItem, field.options);
						field.options = options;
					});
					return metadataFields;
				})
			);
	}

	createForm(stepId: number, field: MetadataField) {
		const requestUri = `${this.baseUrl}/v1/worktemplatesteps/${stepId}/forms`;

		automapper
			.createMap(MetadataField, MetadataFieldSetting_InsertInputDTO)
			.forMember('metadataFieldId', function (opts) { opts.mapFrom('id'); })
			.forMember('entityId', function (opts) { opts.mapFrom('entityId'); })
			.forMember('overrideLabel', function (opts) { opts.mapFrom('overrideLabel'); })
			.forMember('isRequired', function (opts) { opts.mapFrom('isRequired'); })
			.forMember('isEditable', function (opts) { opts.mapFrom('isEditable'); })
			.forMember('allowDocumentUpload', function (opts) { opts.mapFrom('allowDocumentUpload'); })
			.forMember('order', function (opts) { opts.mapFrom('order'); })
			.forMember('value', function (opts) { opts.mapFrom('value'); })
			.forMember('workTemplateId', function () { return 1; });

		const request: MetadataFieldSetting_InsertInputDTO = automapper.map(MetadataField, MetadataFieldSetting_InsertInputDTO, field);

		return this.httpClient.post<any>(requestUri, request);
	}

	updateForm(stepId: number, field: MetadataField) {
		const requestUri = `${this.baseUrl}/v1/worktemplatesteps/${stepId}/forms/${field.settingId}`;

		automapper
			.createMap(MetadataField, MetadataFieldSetting_UpdateInputDTO)
			.forMember('metadataFieldSettingId', function (opts) { opts.mapFrom('settingId'); })
			.forMember('metadataFieldId', function (opts) { opts.mapFrom('id'); })
			.forMember('entityId', function (opts) { opts.mapFrom('entityId'); })
			.forMember('overrideLabel', function (opts) { opts.mapFrom('overrideLabel'); })
			.forMember('isRequired', function (opts) { opts.mapFrom('isRequired'); })
			.forMember('isEditable', function (opts) { opts.mapFrom('isEditable'); })
			.forMember('allowDocumentUpload', function (opts) { opts.mapFrom('allowDocumentUpload'); })
			.forMember('order', function (opts) { opts.mapFrom('order'); })
			.forMember('value', function (opts) { opts.mapFrom('value'); })
			.forMember('workTemplateId', function () { return 1; });

		const request: MetadataFieldSetting_UpdateInputDTO = automapper.map(MetadataField, MetadataFieldSetting_UpdateInputDTO, field);

		return this.httpClient.put<any>(requestUri, request);
	}

	deleteForm(stepId: number, field: MetadataField) {
		const requestUri = `${this.baseUrl}/v1/worktemplatesteps/${stepId}/forms/${field.settingId}`;
		return this.httpClient.delete(requestUri);
	}

	//#endregion

	//#region Work Template Form

	getFindings(stepId: number): Observable<MetadataField[]> {
		const requestUri = `${this.baseUrl}/v1/worktemplatesteps/${stepId}/findings`;

		return this.httpClient.get<MetadataFieldSetting_GetAllOutputDTO[]>(requestUri)
			.pipe(
				map(response => {
					automapper
						.createMap(MetadataFieldSetting_GetAllOutputDTO, MetadataField)
						.forMember('settingId', function (opts) { opts.mapFrom('metadataFieldSettingId'); })
						.forMember('id', function (opts) { opts.mapFrom('metadataFieldId'); })
						.forMember('parentId', function (opts) { opts.mapFrom('parentFieldId'); })
						.forMember('fieldName', function (opts) { opts.mapFrom('fieldName'); })
						.forMember('name', function (opts) { opts.mapFrom('fieldName'); })
						.forMember('label', function (opts) { opts.mapFrom('label'); })
						.forMember('type', function (opts) { opts.mapFrom('fieldTypeName'); })
						.forMember('options', function (opts) { opts.mapFrom('options'); })
						.forMember('metadataListName', function (opts) { opts.mapFrom('metadataListName'); })
						.forMember('entityId', function (opts) { opts.mapFrom('entityId'); })
						.forMember('overrideLabel', function (opts) { opts.mapFrom('overrideLabel'); })
						.forMember('entityName', function (opts) { opts.mapFrom('entityName'); })
						.forMember('isRequired', function (opts) { opts.mapFrom('isRequired'); })
						.forMember('isEditable', function (opts) { opts.mapFrom('isEditable'); })
						.forMember('allowDocumentUpload', function (opts) { opts.mapFrom('allowDocumentUpload'); })
						.forMember('order', function (opts) { opts.mapFrom('order'); })
						.forMember('value', function (opts) { opts.mapFrom('defaultValue'); })
						.forMember('status', function (opts) { opts.mapFrom('status'); });

					let metadataFields: MetadataField[] = automapper.map(MetadataFieldSetting_GetAllOutputDTO, MetadataField, response);
					metadataFields.forEach(field => {
						automapper
							.createMap(MetadataField_GetListItemByIdOutputDTO, ListItem)
							.forMember('id', function (opts) { opts.mapFrom('metadataListItemId'); })
							.forMember('parentId', function (opts) { opts.mapFrom('parentItemId'); })
							.forMember('value', function (opts) { opts.mapFrom('itemValue'); })
							.forMember('description', function (opts) { opts.mapFrom('itemDescription'); })
							.forMember('sort', function (opts) { opts.mapFrom('itemSort'); });

						let options: ListItem[] = automapper.map(MetadataField_GetListItemByIdOutputDTO, ListItem, field.options);
						field.options = options;
					});
					return metadataFields;
				})
			);
	}

	createFinding(stepId: number, field: MetadataField) {
		const requestUri = `${this.baseUrl}/v1/worktemplatesteps/${stepId}/findings`;

		automapper
			.createMap(MetadataField, MetadataFieldSetting_InsertInputDTO)
			.forMember('metadataFieldId', function (opts) { opts.mapFrom('id'); })
			.forMember('entityId', function (opts) { opts.mapFrom('entityId'); })
			.forMember('overrideLabel', function (opts) { opts.mapFrom('overrideLabel'); })
			.forMember('isRequired', function (opts) { opts.mapFrom('isRequired'); })
			.forMember('isEditable', function (opts) { opts.mapFrom('isEditable'); })
			.forMember('allowDocumentUpload', function (opts) { opts.mapFrom('allowDocumentUpload'); })
			.forMember('order', function (opts) { opts.mapFrom('order'); })
			.forMember('value', function (opts) { opts.mapFrom('value'); })
			.forMember('workTemplateId', function () { return 1; });

		const request: MetadataFieldSetting_InsertInputDTO = automapper.map(MetadataField, MetadataFieldSetting_InsertInputDTO, field);

		return this.httpClient.post<any>(requestUri, request);
	}

	updateFinding(stepId: number, field: MetadataField) {
		const requestUri = `${this.baseUrl}/v1/worktemplatesteps/${stepId}/findings/${field.settingId}`;

		automapper
			.createMap(MetadataField, MetadataFieldSetting_UpdateInputDTO)
			.forMember('metadataFieldSettingId', function (opts) { opts.mapFrom('settingId'); })
			.forMember('metadataFieldId', function (opts) { opts.mapFrom('id'); })
			.forMember('entityId', function (opts) { opts.mapFrom('entityId'); })
			.forMember('overrideLabel', function (opts) { opts.mapFrom('overrideLabel'); })
			.forMember('isRequired', function (opts) { opts.mapFrom('isRequired'); })
			.forMember('isEditable', function (opts) { opts.mapFrom('isEditable'); })
			.forMember('allowDocumentUpload', function (opts) { opts.mapFrom('allowDocumentUpload'); })
			.forMember('order', function (opts) { opts.mapFrom('order'); })
			.forMember('value', function (opts) { opts.mapFrom('value'); })
			.forMember('workTemplateId', function () { return 1; });

		const request: MetadataFieldSetting_UpdateInputDTO = automapper.map(MetadataField, MetadataFieldSetting_UpdateInputDTO, field);

		return this.httpClient.put<any>(requestUri, request);
	}

	deleteFinding(stepId: number, field: MetadataField) {
		const requestUri = `${this.baseUrl}/v1/worktemplatesteps/${stepId}/findings/${field.settingId}`;
		return this.httpClient.delete(requestUri);
	}

	//#endregion

	createVersion(version: WorkTemplateVersion) {
		throw new Error("Method not implemented.");
	}

	updateVersion(id: number, version: WorkTemplateVersion) {
		throw new Error("Method not implemented.");
	}

	getVersions() {
		const url = `./assets/mock/admin-work-templates-version.json`;
		const data = this.httpClient.get<WorkTemplateVersion[]>(url);
		return data;
	}

	deleteVersion(id: number) {
		
	}

	getWorkTemplatesHistory() {
		const url = `./assets/mock/admin-work-templates-history.json`;
		const data = this.httpClient.get<WorkTemplateHistory[]>(url);
		return data;
	}

	getHistory(id: number, pageNumber?: number, pageSize?: number): Observable<WorkTemplateHistories> {
		const requestUri = `${this.baseUrl}/v1/worktemplates/${id}/history`;

		const options = { params: new HttpParams() };
		if (pageNumber) options.params = options.params.set('pageNumber', pageNumber.toString());
		if (pageSize) options.params = options.params.set('limit', pageSize.toString());

		return this.httpClient.get<WorkTemplateVersionHistory_GetAllOutputDTO>(requestUri, options)
			.pipe(
				map(response => {
					// Work Template Histories
					automapper
						.createMap(WorkTemplateVersionHistory_GetAllOutputDTO, WorkTemplateHistories)
						.forMember('data', function (opts) { opts.mapFrom('data'); })
						.forMember('pagination', function (opts) { opts.mapFrom('pagination'); });

					const model: WorkTemplateHistories = automapper.map(WorkTemplateVersionHistory_GetAllOutputDTO, WorkTemplateHistories, response);

					automapper
						.createMap(WorkTemplateVersionHistory_GetAllOutputDTOData, WorkTemplateHistory)
						.forMember('auditId', function (opts) { opts.mapFrom('auditId'); })
						.forMember('event', function (opts) { opts.mapFrom('eventName'); })
						.forMember('entityType', function (opts) { opts.mapFrom('entityType'); })
						.forMember('entityId', function (opts) { opts.mapFrom('entityId'); })
						.forMember('column', function (opts) { opts.mapFrom('columnName'); })
						.forMember('oldValue', function (opts) { opts.mapFrom('oldValue'); })
						.forMember('newValue', function (opts) { opts.mapFrom('newValue'); })
						.forMember('createdBy', function (opts) { opts.mapFrom('createdBy'); })
						.forMember('createdOn', function (opts) { opts.mapFrom('createdOn'); });

					const histories: WorkTemplateHistory[] = automapper.map(WorkTemplateVersionHistory_GetAllOutputDTOData, WorkTemplateHistory, model.data);
					model.data = histories || [];
					return model;
				})
			);
	}

	//#region Metadata List Items

	getMetadataListItems(templateId: number): Observable<ListItem[]> {
		const requestUri = `${this.baseUrl}/v1/worktemplates/${templateId}/metadatalist`;
		return this.httpClient.get<MetadataField_GetListItemByIdOutputDTO[]>(requestUri)
			.pipe(
				map(response => {
					automapper
						.createMap(MetadataField_GetListItemByIdOutputDTO, ListItem)
						.forMember('id', function (opts) { opts.mapFrom('metadataListItemId'); })
						.forMember('parentId', function (opts) { opts.mapFrom('parentItemId'); })
						.forMember('value', function (opts) { opts.mapFrom('itemValue'); })
						.forMember('description', function (opts) { opts.mapFrom('itemDescription'); })
						.forMember('sort', function (opts) { opts.mapFrom('itemSort'); });
					const items: ListItem[] = automapper.map(MetadataField_GetListItemByIdOutputDTO, ListItem, response);
					return items.sort((a, b) => (a.description > b.description) ? 1 : -1);
				})
			);
	}

	//#endregion
}