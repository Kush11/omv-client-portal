import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FieldsService } from '../../business/fields/fields.service';
import { UploadRequestsDataService } from './upload-requests.data.service';
import { Observable } from 'rxjs/internal/Observable';
import { UploadRequest } from 'src/app/core/models/entity/upload-request';

@Injectable({
  providedIn: 'root'
})
export class UploadRequestsMockDataService implements UploadRequestsDataService {
 

  constructor(private httpClient: HttpClient, private fieldsService: FieldsService) { }

  getUploadHistory(): Observable<UploadRequest[]> {
    throw new Error("Method not implemented.");
  }
  getUploadsInProgress(): Observable<UploadRequest[]> {
    throw new Error("Method not implemented.");
  }

  getNewUploads(): Observable<UploadRequest[]> {
    throw new Error("Method not implemented.");
  }

  getUpload(id: number): Observable<UploadRequest> {
    throw new Error("Method not implemented.");
  }

  approveUpload(id: number) {
    throw new Error("Method not implemented.");
  }
  
  rejectUpload(id: number) {
    throw new Error("Method not implemented.");
  }
  
  cancelUpload(id: number) {
    throw new Error("Method not implemented.");
  }
}