import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import * as automapper from 'automapper-ts';
import { Observable } from 'rxjs/internal/Observable';
import { FieldsService } from '../../business/fields/fields.service';
import { UploadRequestsDataService } from './upload-requests.data.service';
import { UploadRequest } from 'src/app/core/models/entity/upload-request';
import { UploadRequest_GetAllOutputDTO } from 'src/app/core/dtos/output/uploads/UploadRequest_GetAllOutputDTO';
import { environment } from 'src/environments/environment';
import { tap } from 'rxjs/internal/operators/tap';
import { map } from 'rxjs/internal/operators/map';
import { UploadRequest_GetByIdOutputDTO } from 'src/app/core/dtos/output/uploads/UploadRequest_GetByIdOutputDTO';

@Injectable({
  providedIn: 'root'
})
export class UploadRequestsWebDataService implements UploadRequestsDataService {
 

  baseUrl = environment.api.baseUrl;

  constructor(private httpClient: HttpClient, private fieldsService: FieldsService) { }
  getUploadsInProgress(): Observable<UploadRequest[]> {

    const requestUri = this.baseUrl + `/v1/uploadrequests/inprogress`;

    return this.httpClient.get<UploadRequest_GetAllOutputDTO[]>(requestUri)
      .pipe(
        map(response => {
          automapper
            .createMap(UploadRequest_GetAllOutputDTO, UploadRequest)
            .forMember('id', function (opts) { opts.mapFrom('uploadRequestId'); })
            .forMember('type', function (opts) { opts.mapFrom('uploadRequestType'); })
            .forMember('requester', function (opts) { opts.mapFrom('requester'); })
            .forMember('requesterName', function (opts) { opts.mapFrom('requesterName'); })
            .forMember('source', function (opts) { opts.mapFrom('source'); })
            .forMember('destination', function (opts) { opts.mapFrom('destination'); })
            .forMember('ruleId', function (opts) { opts.mapFrom('ruleId'); })
            .forMember('ruleName', function (opts) { opts.mapFrom('ruleName'); })
            .forMember('isOCRAllowed', function (opts) { opts.mapFrom('isOCRAllowed'); })
            .forMember('isSRAllowed', function (opts) { opts.mapFrom('isSRAllowed'); })
            .forMember('size', function (opts) { opts.mapFrom('size'); })
            .forMember('files', function (opts) { opts.mapFrom('files'); })
            .forMember('iP', function (opts) { opts.mapFrom('iP'); })
            .forMember('createdOn', function (opts) { opts.mapFrom('createdOn'); })
            .forMember('status', function (opts) { opts.mapFrom('statusName'); });

          let uploads: UploadRequest[] = automapper.map(UploadRequest_GetAllOutputDTO, UploadRequest, response);
          uploads.forEach(upload => {
            upload.size = upload.size ? upload.size : 0;
            upload.sizeDisplay = this.fieldsService.formatSize(upload.size);
          });
          return uploads;
        })
      );
  }
  getNewUploads(): Observable<UploadRequest[]> {
    const requestUri = this.baseUrl + `/v1/uploadrequests`;

    return this.httpClient.get<UploadRequest_GetAllOutputDTO[]>(requestUri)
      .pipe(
        map(response => {
          automapper
            .createMap(UploadRequest_GetAllOutputDTO, UploadRequest)
            .forMember('id', function (opts) { opts.mapFrom('uploadRequestId'); })
            .forMember('type', function (opts) { opts.mapFrom('uploadRequestType'); })
            .forMember('requester', function (opts) { opts.mapFrom('requester'); })
            .forMember('requesterName', function (opts) { opts.mapFrom('requesterName'); })
            .forMember('source', function (opts) { opts.mapFrom('source'); })
            .forMember('destination', function (opts) { opts.mapFrom('destination'); })
            .forMember('ruleId', function (opts) { opts.mapFrom('ruleId'); })
            .forMember('ruleName', function (opts) { opts.mapFrom('ruleName'); })
            .forMember('isOCRAllowed', function (opts) { opts.mapFrom('isOCRAllowed'); })
            .forMember('isSRAllowed', function (opts) { opts.mapFrom('isSRAllowed'); })
            .forMember('size', function (opts) { opts.mapFrom('size'); })
            .forMember('files', function (opts) { opts.mapFrom('files'); })
            .forMember('iP', function (opts) { opts.mapFrom('iP'); })
            .forMember('createdOn', function (opts) { opts.mapFrom('createdOn'); })
            .forMember('status', function (opts) { opts.mapFrom('statusName'); });

          let uploads: UploadRequest[] = automapper.map(UploadRequest_GetAllOutputDTO, UploadRequest, response);
          uploads.forEach(upload => {
            upload.size = upload.size ? upload.size : 0;
            upload.sizeDisplay = this.fieldsService.formatSize(upload.size);
          });
          return uploads;
        })
      );
  }

  getUploadHistory(): Observable<UploadRequest[]> {
    const requestUri = environment.api.baseUrl + `/v1/uploadrequests/history`;

    return this.httpClient.get<UploadRequest_GetAllOutputDTO[]>(requestUri).pipe(map(
      response => {
        automapper
          .createMap(UploadRequest_GetAllOutputDTO, UploadRequest)
          .forMember('id', function (opts) { opts.mapFrom('uploadRequestId'); })
          .forMember('createdBy', function (opts) { opts.mapFrom('createdBy'); })
          .forMember('type', function (opts) { opts.mapFrom('uploadRequestType'); })
          .forMember('requester', function (opts) { opts.mapFrom('requester'); })
          .forMember('requesterName', function (opts) { opts.mapFrom('requesterName'); })
          .forMember('source', function (opts) { opts.mapFrom('source'); })
          .forMember('destination', function (opts) { opts.mapFrom('destination'); })
          .forMember('ruleId', function (opts) { opts.mapFrom('ruleId'); })
          .forMember('ruleName', function (opts) { opts.mapFrom('ruleName'); })
          .forMember('isOCRAllowed', function (opts) { opts.mapFrom('isOCRAllowed'); })
          .forMember('isSRAllowed', function (opts) { opts.mapFrom('isSRAllowed'); })
          .forMember('size', function (opts) { opts.mapFrom('size'); })
          .forMember('files', function (opts) { opts.mapFrom('files'); })
          .forMember('iP', function (opts) { opts.mapFrom('iP'); })
          .forMember('createdOn', function (opts) { opts.mapFrom('createdOn'); })
          .forMember('status', function (opts) { opts.mapFrom('statusName'); });

        let histories: UploadRequest[] = automapper.map(UploadRequest_GetAllOutputDTO, UploadRequest, response);
        histories.forEach(history => {
          history.size = history.size ? history.size : 0;
          history.sizeDisplay = this.fieldsService.formatSize(history.size);
        });
        return histories;
      })
    );
  }

  getUpload(id: number): Observable<UploadRequest> {
    const requestUri = environment.api.baseUrl + `/v1/uploadrequests/${id}`;

    return this.httpClient.get<UploadRequest_GetAllOutputDTO[]>(requestUri).pipe(map(
      response => {
        automapper
          .createMap(UploadRequest_GetByIdOutputDTO, UploadRequest)
          .forMember('requesterName', function (opts) { opts.mapFrom('requesterName'); })
          .forMember('source', function (opts) { opts.mapFrom('source'); })
          .forMember('destination', function (opts) { opts.mapFrom('destination'); })
          .forMember('ruleId', function (opts) { opts.mapFrom('ruleId'); })
          .forMember('ruleName', function (opts) { opts.mapFrom('ruleName'); })
          .forMember('isOCRAllowed', function (opts) { opts.mapFrom('isOCRAllowed'); })
          .forMember('isSRAllowed', function (opts) { opts.mapFrom('isSRAllowed'); })
          .forMember('size', function (opts) { opts.mapFrom('size'); })
          .forMember('files', function (opts) { opts.mapFrom('files'); })
          .forMember('ip', function (opts) { opts.mapFrom('ip'); })
          .forMember('statusName', function (opts) { opts.mapFrom('statusName'); })
          .forMember('createdOn', function (opts) { opts.mapFrom('createdOn'); })
          .forMember('estProcessTime', function (opts) { opts.mapFrom('estProcessTime'); });

        let uploadRequest: UploadRequest = automapper.map(UploadRequest_GetByIdOutputDTO, UploadRequest, response);
        return uploadRequest;
      })
    );
  }

  approveUpload(id: number) {
    const requestUri = `${this.baseUrl}/v1/uploadrequests/${id}/approve`;
    return this.httpClient.put(requestUri, null);
  }

  rejectUpload(id: number) {
    const requestUri = `${this.baseUrl}/v1/uploadrequests/${id}/reject`;
    return this.httpClient.put(requestUri, null);
  }

  cancelUpload(id: number) {
    const requestUri = `${this.baseUrl}/v1/uploadrequests/${id}/cancel`;
    return this.httpClient.put(requestUri, null);
  }
}
