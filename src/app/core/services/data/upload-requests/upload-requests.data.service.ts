import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { UploadRequest } from 'src/app/core/models/entity/upload-request';

@Injectable({
  providedIn: 'root'
})
export abstract class UploadRequestsDataService {
  abstract getUploadHistory(): Observable<UploadRequest[]>;
  abstract getNewUploads(): Observable<UploadRequest[]>;
  abstract getUploadsInProgress(): Observable<UploadRequest[]>;
  abstract getUpload(id: number): Observable<UploadRequest>;
  abstract approveUpload(id: number);
  abstract rejectUpload(id: number);
  abstract cancelUpload(id: number);
}