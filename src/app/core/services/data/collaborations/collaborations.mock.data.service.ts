import { CollaborationsDataService } from './collaborations.data.service';
import { Observable } from 'rxjs/internal/Observable';
import { Collaboration, Participant, CollaborationItem, CollaborationRoom, CollaborationSession, CollaborationScreenshot } from 'src/app/core/models/entity/collaboration';
import { Injectable } from '@angular/core';
import { MediaItem } from 'src/app/core/models/entity/media';
import { User, Users } from 'src/app/core/models/entity/user';
import { LiveStreams } from 'src/app/core/models/entity/live-stream';

@Injectable({
    providedIn: 'root'
})

export class CollaborationsMockDataService implements CollaborationsDataService {
    deleteScreenshot(collaborationId: number, screenshotId: number) {
        throw new Error("Method not implemented.");
    }
    removeSession(id: number) {
        throw new Error("Method not implemented.");
    }
    updateSessionParticipants(sessionId: number, participantIds: number[]) {
        throw new Error("Method not implemented.");
    }
    createSession(session: CollaborationSession) {
        throw new Error("Method not implemented.");
    }
    updateSession(session: CollaborationSession) {
        throw new Error("Method not implemented.");
    }
    validateSessionName(name: string): Observable<boolean> {
        throw new Error("Method not implemented.");
    }
    leaveRoom(id: number) {
        throw new Error("Method not implemented.");
    }
    join(id: number): Observable<CollaborationRoom> {
        throw new Error("Method not implemented.");
    }

    constructor() { }

    getCollaborations(year: number, month: number): Observable<CollaborationSession[]> {
        throw new Error('Method not implemented.');
    }
    getCurrentCollaborations(year: number, month: number, day: number): Observable<CollaborationSession[]> {
        throw new Error('Method not implemented.');
    }
    getCollaborationSession(): Observable<CollaborationSession> {
        throw new Error('Method not implemented.');
    }
    getCollaborationItems(pageNumber?, pageSize?): Observable<LiveStreams> {
        throw new Error('Method not implemented.');
    }
    getParticipants(name: string, groupid: number, status?: number, pageNumber?: number, pageSize?: number): Observable<Users> {
        throw new Error('Method not implemented.');
    }

    createCollaborationSession(payload: Collaboration): Observable<Collaboration> {
        throw new Error('Method not implemented.');
    }
    updateCollaborationSession(id: number, payload: Collaboration) {
        throw new Error('Method not implemented.');
    }
    getCollaborationArchiveItems(): Observable<MediaItem[]> {
        throw new Error('Method not implemented.');
    }

    createCollaborationRoom(id: number, payload): Observable<CollaborationRoom> {
        throw new Error('Method not implemented.');
    }

    createScreenshot(collaborationId: number) {
        throw new Error('Method not implemented.');
    };
    getScreenshots(id: number): Observable<CollaborationScreenshot[]> {
        throw new Error('Method not implemented.');
    }
}
