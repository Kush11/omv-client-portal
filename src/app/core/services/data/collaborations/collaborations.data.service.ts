import { Observable } from 'rxjs/internal/Observable';
import { Collaboration, CollaborationRoom, CollaborationSession, CollaborationScreenshot } from 'src/app/core/models/entity/collaboration';
import { Injectable } from '@angular/core';
import { MediaItem } from 'src/app/core/models/entity/media';
import { User } from 'src/app/core/models/entity/user';
import { Users } from './../../../models/entity/user';
import { LiveStreams } from 'src/app/core/models/entity/live-stream';
import { Tag } from 'src/app/core/models/entity/tag';


@Injectable({
  providedIn: 'root'
})

export abstract class CollaborationsDataService {

  constructor() { }

  abstract getCollaborations(year: number, month: number, query?: string): Observable<CollaborationSession[]>;
  abstract getCurrentCollaborations(year: number, month: number, day: number, query?: string): Observable<CollaborationSession[]>;
  abstract getCollaborationSession(id: number): Observable<CollaborationSession>;
  abstract createSession(session: CollaborationSession);
  abstract updateSession(session: CollaborationSession);
  abstract removeSession(id: number);

  abstract getParticipants(name: string, groupid: number, status?: number, pageNumber?: number, pageSize?: number): Observable<Users>;
  abstract updateSessionParticipants(sessionId: number, participantIds: number[]);
  abstract getCollaborationItems(pageNumber?, pageSize?): Observable<LiveStreams>;

  abstract validateSessionName(name: string): Observable<boolean>;

  abstract getCollaborationArchiveItems(): Observable<MediaItem[]>;

  abstract createCollaborationRoom(id: number, payload: CollaborationRoom): Observable<CollaborationRoom>;

  abstract join(id: number): Observable<CollaborationRoom>;
  abstract leaveRoom(id: number);

  abstract getScreenshots(id: number): Observable<CollaborationScreenshot[]>;
  abstract createScreenshot(collaborationId: number, payload: MediaItem);
  abstract deleteScreenshot(collaborationId: number, screenshotId: number);
}