import { CollaborationsDataService } from './collaborations.data.service';
import { Observable } from 'rxjs/internal/Observable';
import { Collaboration, CollaborationRoom, CollaborationSession, CollaborationScreenshot } from 'src/app/core/models/entity/collaboration';
import {
	CollaborationItemDTO, AttendeesDTO,
	Collaboration_GetByIdOutputDTO
} from 'src/app/core/dtos/output/collaboration/collaboration-outputDTO';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { map } from 'rxjs/internal/operators/map';
import { Injectable } from '@angular/core';
import { DateService } from '../../business/dates/date.service';
import { Camera_GetAllOutputDTO } from 'src/app/core/dtos/input/live-stream/Camera_GetAllOutputDTO';
import { Collaboration_InsertInputDTO, CollaborationItem_InsertInputDTO } from 'src/app/core/dtos/input/collaboration/Collaboration_InsertInputDTO';
import { environment } from 'src/environments/environment';
import { FieldsService } from '../../business/fields/fields.service';
import { Document_GetByIdOutputDTO } from 'src/app/core/dtos/output/documents/Document_GetByIdOutputDTO';
import { MediaItem } from 'src/app/core/models/entity/media';
import { Collaboration_StartInputDTO } from './../../../dtos/input/collaboration/Collaboration_StartInputDTO';
import { Collaboration_StartOutputDTO } from 'src/app/core/dtos/output/collaboration/Collaboration_StartOutputDTO';
import { User_SearchOutputDTO, User_SearchOutputDTOData } from 'src/app/core/dtos/output/users/User_SearchOutputDTO';
import { User, Users } from 'src/app/core/models/entity/user';
import { Camera_OutputDTO } from 'src/app/core/dtos/output/live-stream/live-stream';
import { LiveStreams, LiveStream } from 'src/app/core/models/entity/live-stream';
import { Collaboration_JoinOutputDTO } from 'src/app/core/dtos/output/collaboration/Collaboration_JoinOutputDTO';
import { of } from 'rxjs/internal/observable/of';
import * as automapper from 'automapper-ts';
import { delay } from 'rxjs/internal/operators/delay';
import { Document_InsertInputDTO } from 'src/app/core/dtos/input/documents/Document_InsertInputDTO';
import { CollaborationScreenshot_OutputDTO } from 'src/app/core/dtos/output/collaboration/CollaborationItem_GetByCollaborationId';
import { Tag } from 'src/app/core/models/entity/tag';

@Injectable({
	providedIn: 'root'
})

export class CollaborationsWebDataService implements CollaborationsDataService {

	httpOptions = {
		headers: new HttpHeaders({
			'Content-Type': 'application/json',
		})
	};

	constructor(public httpClient: HttpClient, public dateService: DateService, public fieldsService: FieldsService) { }

	getCurrentCollaborations(year: number, month: number, day: number, query?: string): Observable<CollaborationSession[]> {
		const requestUri = environment.api.baseUrl + `/v1/collaborations/dailycollaborations`;
		const options = { params: new HttpParams() };
		options.params = options.params.set('year', year.toString());
		options.params = options.params.set('month', month.toString());
		options.params = options.params.set('day', day.toString());
		if (query) { options.params = options.params.set('query', query); }
		return this.httpClient.get<CollaborationItemDTO[]>(requestUri, options)
			.pipe(
				map(response => {
					automapper
						.createMap(CollaborationItemDTO, CollaborationSession)
						.forMember('id', function (opts) { opts.mapFrom('collaborationId'); })
						.forMember('title', function (opts) { opts.mapFrom('collaborationTitle'); })
						.forMember('date', function (opts) { opts.mapFrom('collaborationDate'); })
						.forMember('startTimeString', function (opts) { opts.mapFrom('startTime'); })
						.forMember('endTimeString', function (opts) { opts.mapFrom('endTime'); })
						.forMember('isHost', function (opts) { opts.mapFrom('isHost'); })
						.forMember('modifiedBy', function (opts) { opts.mapFrom('modifiedBy'); })
						.forMember('participants', function (opts) { opts.mapFrom('attendees'); })
						.forMember('status', function (opts) { opts.mapFrom('statusName'); });

					const items: CollaborationSession[] = automapper.map(CollaborationItemDTO, CollaborationSession, response);

					items.forEach(item => {
						automapper
							.createMap(AttendeesDTO, User)
							.forMember('id', function (opts) { opts.mapFrom('userId'); })
							.forMember('userName', function (opts) { opts.mapFrom('userName'); })
							.forMember('emailAddress', function (opts) { opts.mapFrom('emailAddress'); })
							.forMember('firstName', function (opts) { opts.mapFrom('firstName'); })
							.forMember('lastName', function (opts) { opts.mapFrom('lastName'); })
							.forMember('displayName', function (opts) { opts.mapFrom('displayName'); })
							.forMember('groups', function (opts) { opts.mapFrom('roleNames'); })
							.forMember('status', function (opts) { opts.mapFrom('status'); })
							.forMember('statusName', function (opts) { opts.mapFrom('statusName'); })
							.forMember('createdOn', function (opts) { opts.mapFrom('createdOn'); })
							.forMember('createdBy', function (opts) { opts.mapFrom('createdBy'); })
							.forMember('modifiedOn', function (opts) { opts.mapFrom('modifiedOn'); })
							.forMember('modifiedBy', function (opts) { opts.mapFrom('modifiedBy'); });

						const participants: User[] = automapper.map(AttendeesDTO, User, item.participants);
						item.participants = participants;
					});
					return items;
				})
			);
	}

	getCollaborations(year: number, month: number, query?: string): Observable<CollaborationSession[]> {
		const requestUri = environment.api.baseUrl + `/v1/collaborations`;
		const options = { params: new HttpParams() };
		options.params = options.params.set('year', year.toString());
		options.params = options.params.set('month', month.toString());
		if (query) { options.params = options.params.set('query', query); }
		return this.httpClient.get<CollaborationItemDTO[]>(requestUri, options)
			.pipe(
				map(response => {
					automapper
						.createMap(CollaborationItemDTO, CollaborationSession)
						.forMember('id', function (opts) { opts.mapFrom('collaborationId'); })
						.forMember('title', function (opts) { opts.mapFrom('collaborationTitle'); })
						.forMember('date', function (opts) { opts.mapFrom('collaborationDate'); })
						.forMember('startTimeString', function (opts) { opts.mapFrom('startTime'); })
						.forMember('endTimeString', function (opts) { opts.mapFrom('endTime'); })
						.forMember('isHost', function (opts) { opts.mapFrom('isHost'); })
						.forMember('modifiedBy', function (opts) { opts.mapFrom('modifiedBy'); })
						.forMember('participants', function (opts) { opts.mapFrom('attendees'); })
						.forMember('status', function (opts) { opts.mapFrom('statusName'); });

					const items: CollaborationSession[] = automapper.map(CollaborationItemDTO, CollaborationSession, response);

					items.forEach(item => {
						automapper
							.createMap(AttendeesDTO, User)
							.forMember('displayName', function (opts) { opts.mapFrom('displayName'); })
							.forMember('firstName', function (opts) { opts.mapFrom('firstName'); })
							.forMember('lastName', function (opts) { opts.mapFrom('lastName'); });

						const participants: User[] = automapper.map(AttendeesDTO, User, item.participants);
						item.participants = participants;
					});
					return items;
				})
			);
	}

	getCollaborationSession(id: number): Observable<CollaborationSession> {
		const baseUrl = environment.api.baseUrl + `/v1/collaborations/${id}`;
		return this.httpClient.get<Collaboration_GetByIdOutputDTO>(baseUrl)
			.pipe(
				map(response => {
					automapper
						.createMap(Collaboration_GetByIdOutputDTO, CollaborationSession)
						.forMember('id', function (opts) { opts.mapFrom('collaborationId'); })
						.forMember('title', function (opts) { opts.mapFrom('collaborationTitle'); })
						.forMember('date', function (opts) { opts.mapFrom('collaborationDate'); })
						.forMember('startTimeString', function (opts) { opts.mapFrom('startTime'); })
						.forMember('endTimeString', function (opts) { opts.mapFrom('endTime'); })
						.forMember('isHost', function (opts) { opts.mapFrom('isHost'); })
						.forMember('modifiedBy', function (opts) { opts.mapFrom('modifiedBy'); })
						.forMember('videos', function (opts) { opts.mapFrom('collaborationItem'); })
						.forMember('participants', function (opts) { opts.mapFrom('collaborationAttendees'); })
						.forMember('status', function (opts) { opts.mapFrom('statusName'); });

					const session: CollaborationSession = automapper.map(Collaboration_GetByIdOutputDTO, CollaborationSession, response);

					automapper
						.createMap(AttendeesDTO, User)
						.forMember('id', function (opts) { opts.mapFrom('userId'); })
						.forMember('displayName', function (opts) { opts.mapFrom('displayName'); })
						.forMember('firstName', function (opts) { opts.mapFrom('firstName'); })
						.forMember('lastName', function (opts) { opts.mapFrom('lastName'); });

					const participants: User[] = automapper.map(AttendeesDTO, User, session.participants);
					session.participants = participants;

					automapper
						.createMap(Camera_GetAllOutputDTO, LiveStream)
						.forMember('id', function (opts) { opts.mapFrom('cameraId'); })
						.forMember('url', function (opts) { opts.mapFrom('livestreamURL'); })
						.forMember('name', function (opts) { opts.mapFrom('name'); })
						.forMember('isFavorite', function (opts) { opts.mapFrom('isFavorite'); })
						.forMember('favoriteId', function (opts) { opts.mapFrom('favoriteId'); })
						.forMember('poster', function (opts) { opts.mapFrom('thumbnailUrl'); });
					const videos: LiveStream[] = automapper.map(Camera_GetAllOutputDTO, LiveStream, session.videos);
					session.videos = videos;
					return session;
				})
			);
	}

	createSession(session: CollaborationSession) {
		const requestUri = `${environment.api.baseUrl}/v1/collaborations`;

		automapper
			.createMap(CollaborationSession, Collaboration_InsertInputDTO)
			.forMember('collaborationTitle', function (opts) { opts.mapFrom('title'); })
			.forMember('collaborationDate', function (opts) { opts.mapFrom('date'); })
			.forMember('collaborationAttendees', function (opts) { opts.mapFrom('participantIds'); })
			.forMember('directoryId', function (opts) { opts.mapFrom('directoryId'); })
			.forMember('collaborationItems', function (opts) { opts.mapFrom('videos'); });
		const request: Collaboration_InsertInputDTO = automapper.map(CollaborationSession, Collaboration_InsertInputDTO, session);

		automapper
			.createMap(CollaborationSession, CollaborationItem_InsertInputDTO)
			.forMember('entityId', function (opts) { opts.mapFrom('id'); })
			.forMember('entityName', function () { return 'Camera'; });
		const collaborationItems: CollaborationItem_InsertInputDTO[] = automapper.map(CollaborationSession, CollaborationItem_InsertInputDTO, session.videos);
		request.collaborationItems = collaborationItems;
		request.startTime = session.startTimeString;
		request.endTime = session.endTimeString;

		return this.httpClient.post<any>(requestUri, request);
	}

	updateSession(session: CollaborationSession) {
		const requestUri = `${environment.api.baseUrl}/v1/collaborations/${session.id}`;

		automapper
			.createMap(CollaborationSession, Collaboration_InsertInputDTO)
			.forMember('collaborationTitle', function (opts) { opts.mapFrom('title'); })
			.forMember('collaborationDate', function (opts) { opts.mapFrom('date'); })
			.forMember('collaborationAttendees', function (opts) { opts.mapFrom('participantIds'); })
			.forMember('collaborationItems', function (opts) { opts.mapFrom('videos'); });
		const request: Collaboration_InsertInputDTO = automapper.map(CollaborationSession, Collaboration_InsertInputDTO, session);

		automapper
			.createMap(CollaborationSession, CollaborationItem_InsertInputDTO)
			.forMember('entityId', function (opts) { opts.mapFrom('id'); })
			.forMember('entityName', function () { return 'Camera'; });
		const collaborationItems: CollaborationItem_InsertInputDTO[] = automapper.map(CollaborationSession, CollaborationItem_InsertInputDTO, session.videos);
		request.collaborationItems = collaborationItems;
		request.startTime = session.startTimeString;
		request.endTime = session.endTimeString;

		return this.httpClient.put<any>(requestUri, request);
	}

	removeSession(id: number) {
		const requestUri = `${environment.api.baseUrl}/v1/collaborations/${id}`;
		return this.httpClient.delete(requestUri);
	}

	getCollaborationItems(pageNumber?, pageSize?): Observable<LiveStreams> {
		const requestUri = environment.api.baseUrl + `/v1/livestreams`;
		const options = {
			params: new HttpParams()
		};
		if (pageNumber) { options.params = options.params.set('pageNumber', pageNumber.toString()); }
		if (pageSize) { options.params = options.params.set('limit', pageSize.toString()); }
		return this.httpClient.get<Camera_OutputDTO[]>(requestUri)
			.pipe(
				map(
					response => { // Map response to liveStream.
						automapper
							.createMap(Camera_OutputDTO, LiveStreams)
							.forMember('pagination', function (opts) { opts.mapFrom('Pagination'); })
							.forMember('data', function (opts) { opts.mapFrom('Data'); });

						let liveStream: LiveStreams = automapper.map(Camera_OutputDTO, LiveStreams, response);
						console.log('LiveStreamWebDataService - getLiveStream : ', liveStream);

						// Map data from response to media items
						automapper
							.createMap(Camera_GetAllOutputDTO, LiveStream)
							.forMember('id', function (opts) { opts.mapFrom('cameraId'); })
							.forMember('url', function (opts) { opts.mapFrom('livestreamURL'); })
							.forMember('name', function (opts) { opts.mapFrom('name'); })
							.forMember('isFavorite', function (opts) { opts.mapFrom('isFavorite'); })
							.forMember('favoriteId', function (opts) { opts.mapFrom('favoriteId'); })
							.forMember('poster', function (opts) { opts.mapFrom('thumbnailUrl'); });
						let liveStreamItems: LiveStream[] = automapper.map(Camera_GetAllOutputDTO, LiveStream, liveStream.data);

						liveStreamItems.forEach(item => {
							const defaultImage = '../../../assets/images/photo-oceaneering.jpg';
							if (!item.poster) item.poster = defaultImage;
						});
						liveStream.data = liveStreamItems;
						return liveStream;
					}
				)
			);
	}

	validateSessionName(title: string): Observable<boolean> {
		const requestUri = environment.api.baseUrl + `/v1/collaborations/validate`;

		const options = { params: new HttpParams() };
		options.params = options.params.set('query', title);

		return this.httpClient.get<boolean>(requestUri, options);
	}

	getParticipants(name?: string, status: number = 1, pageNumber?: number, pageSize?: number): Observable<Users> {
		const requestUri = environment.api.baseUrl + `/v1/users`;
		const options = { params: new HttpParams() };
		if (name) { options.params = options.params.set('name', name); }
		options.params = options.params.set('status', status.toString());
		if (pageNumber) { options.params = options.params.set('pageNumber', pageNumber.toString()); }
		if (pageSize) { options.params = options.params.set('limit', pageSize.toString()); }

		return this.httpClient.get<User_SearchOutputDTO>(requestUri)
			.pipe(
				map(
					response => {
						let users = new Users();
						users.total = response.pagination.total;

						automapper
							.createMap(User_SearchOutputDTOData, User)
							.forMember('id', (opts) => opts.mapFrom('userId'))
							.forMember('firstName', (opts) => opts.mapFrom('firstName'))
							.forMember('lastName', (opts) => opts.mapFrom('lastName'))
							.forMember('groups', (opts) => opts.mapFrom('roleNames'))
							.forMember('displayName', (opts) => opts.mapFrom('displayName'))
							.forMember('imageUrl', (opts) => opts.mapFrom('imageUrl'));

						const participants: User[] = automapper.map(User_SearchOutputDTOData, User, response.data);
						users.data = participants;
						return users;
					}
				)
			);
	}

	updateSessionParticipants(sessionId: number, participantIds: number[]) {
		const requestUri = `${environment.api.baseUrl}/v1/collaborations/${sessionId}/participants`;

		const request = { collaborationAttendees: participantIds };

		return this.httpClient.put(requestUri, request);
	}

	createCollaborationSession(payload: Collaboration): Observable<Collaboration> {
		const requestUri = `${environment.api.baseUrl}/v1/collaborations`;

		automapper
			.createMap(Collaboration, Collaboration_InsertInputDTO)
			.forMember('collaborationTitle', function (opts) { opts.mapFrom('title'); })
			.forMember('collaborationDate', function (opts) { opts.mapFrom('date'); })
			.forMember('startTime', function (opts) { opts.mapFrom('startTime'); })
			.forMember('endTime', function (opts) { opts.mapFrom('endTime'); })
			.forMember('collaborationAttendees', function (opts) { opts.mapFrom('participantIds'); })
			.forMember('collaborationItems', function (opts) { opts.mapFrom('insertItems'); });

		const request = automapper.map(Collaboration, Collaboration_InsertInputDTO, payload);


		return this.httpClient.post<any>(requestUri, request);
	}

	updateCollaborationSession(id: number, payload: Collaboration) {
		const requestUri = `${environment.api.baseUrl}/v1/collaborations/${id}`;

		automapper
			.createMap(Collaboration, Collaboration_InsertInputDTO)
			.forMember('collaborationTitle', function (opts) { opts.mapFrom('title'); })
			.forMember('collaborationDate', function (opts) { opts.mapFrom('date'); })
			.forMember('startTime', function (opts) { opts.mapFrom('startTime'); })
			.forMember('endTime', function (opts) { opts.mapFrom('endTime'); })
			.forMember('collaborationAttendees', function (opts) { opts.mapFrom('ParticipantIds'); })
			.forMember('collaborationItems', function (opts) { opts.mapFrom('media'); });

		const request = automapper.map(Collaboration, Collaboration_InsertInputDTO, payload);


		return this.httpClient.put<any>(requestUri, request);
	}

	getCollaborationArchiveItems(): Observable<MediaItem[]> {
		const MockUrl = './assets/mock/collaborations-archive-listview.json';
		return this.httpClient.get<Document_GetByIdOutputDTO[]>(MockUrl)
			.pipe(
				map(
					response => {
						automapper
							.createMap(Document_GetByIdOutputDTO, MediaItem)
							.forMember('id', function (opts) { opts.mapFrom('documentId'); })
							.forMember('directoryId', function (opts) { opts.mapFrom('DirectoryId'); })
							.forMember('directoryName', function (opts) { opts.mapFrom('DiirectoryName'); })
							.forMember('directoryParentId', function (opts) { opts.mapFrom('DirectoryParentId'); })
							.forMember('directoryParentName', function (opts) { opts.mapFrom('DirectoryParentName'); })
							.forMember('storageType', function (opts) { opts.mapFrom('StorageType'); })
							.forMember('entityType', function (opts) { opts.mapFrom('EntityType'); })
							.forMember('entityId', function (opts) { opts.mapFrom('EntityId'); })
							.forMember('documentTypeCode', function (opts) { opts.mapFrom('DocumentTypeCode'); })
							.forMember('name', function (opts) { opts.mapFrom('documentName'); })
							.forMember('url', function (opts) { opts.mapFrom('documentUrl'); })
							.forMember('metadata', function (opts) { opts.mapFrom('Metadata'); })
							.forMember('contentType', function (opts) { opts.mapFrom('ContentType'); })
							.forMember('containerId', function (opts) { opts.mapFrom('ContainerId'); })
							.forMember('size', function (opts) { opts.mapFrom('Size'); })
							.forMember('thumbnail', function (opts) { opts.mapFrom('thumbnailUrl'); })
							.forMember('isDeleted', function (opts) { opts.mapFrom('IsDeleted'); })
							.forMember('status', function (opts) { opts.mapFrom('Status'); });

						const mediaItems: MediaItem[] = automapper.map(Document_GetByIdOutputDTO, MediaItem, response);

						mediaItems.forEach(item => {
							const docTypes = ['DOCX', 'CSV', 'DOC', 'TXT', 'XLS', 'XLSX', 'PDF', 'MP4', 'MKV', 'MOV', 'AVI', 'MPG', 'TELEMETRY',];
							const type = item ? item.documentTypeCode.toUpperCase() : '';
							if (docTypes.includes(type)) { item.thumbnail = this.fieldsService.setPlaceholderByType(type); }
							item.type = type;
							item.name = item.documentId ? item.name.toUpperCase() : item.name;
							item.nameWithoutExtension = item.name ? this.fieldsService.removeFileExtension(item.name) : 'UNKNOWN';
							item.modifiedOnString = this.dateService.formatToString(item.modifiedOn, 'MMM DD, YYYY hh:mm');
						});
						return mediaItems;
					}
				)
			);
	}

	createCollaborationRoom(id: number, payload: CollaborationRoom): Observable<CollaborationRoom> {

		const requestUri = `${environment.api.baseUrl}/v1/collaborations/${id}/start`;
		automapper
			.createMap(CollaborationRoom, Collaboration_StartInputDTO)
			.forMember('identity', (opts) => { opts.mapFrom('name'); })
			.forMember('title', (opts) => { opts.mapFrom('title'); });
		const itemsRequest = automapper.map(CollaborationRoom, Collaboration_StartInputDTO, payload);
		return this.httpClient.post<any>(requestUri, itemsRequest).pipe(
			map(
				response => {
					automapper.
						createMap(Collaboration_StartOutputDTO, CollaborationRoom)
						.forMember('title', (opts) => { opts.mapFrom('name'); })
						.forMember('token', (opts) => { opts.mapFrom('accessToken'); })
						.forMember('feeds', (opts) => { opts.mapFrom('collaborationItem'); })
						.forMember('isHost', (opts) => { opts.mapFrom('isHost'); });
					const collaborationRoom: CollaborationRoom = automapper.map(Collaboration_StartOutputDTO, CollaborationRoom, response);
					return collaborationRoom;
				}
			)
		);
	}

	//#region Collaboration Room

	join(id: number): Observable<CollaborationRoom> {
		const requestUri = `${environment.api.baseUrl}/v1/collaborations/${id}/join`;

		return this.httpClient.get<Collaboration_JoinOutputDTO>(requestUri)
			.pipe(
				map(
					response => {
						automapper.
							createMap(Collaboration_JoinOutputDTO, CollaborationRoom)
							.forMember('participantToken', (opts) => { opts.mapFrom('participantAccessToken'); })
							.forMember('channelToken', (opts) => { opts.mapFrom('chatAccessToken'); })
							.forMember('name', (opts) => { opts.mapFrom('name'); })
							.forMember('isHost', (opts) => { opts.mapFrom('isHost'); })
							.forMember('sessionTitle', (opts) => { opts.mapFrom('collaborationTitle'); })
							.forMember('hostIdentity', (opts) => { opts.mapFrom('hostIdentity'); })
							.forMember('connectedUserName', (opts) => { opts.mapFrom('identity'); });

						const session: CollaborationRoom = automapper.map(Collaboration_JoinOutputDTO, CollaborationRoom, response);
						automapper
							.createMap(Camera_GetAllOutputDTO, LiveStream)
							.forMember('id', function (opts) { opts.mapFrom('cameraId'); })
							.forMember('url', function (opts) { opts.mapFrom('livestreamURL'); })
							.forMember('name', function (opts) { opts.mapFrom('name'); })
							.forMember('isFavorite', function (opts) { opts.mapFrom('isFavorite'); })
							.forMember('favoriteId', function (opts) { opts.mapFrom('favoriteId'); })
							.forMember('poster', function (opts) { opts.mapFrom('thumbnailUrl'); });
						let liveStreamItems: LiveStream[] = automapper.map(Camera_GetAllOutputDTO, LiveStream, response.collaborationItems);
						session.feeds = liveStreamItems;

						automapper
							.createMap(User_SearchOutputDTOData, User)
							.forMember('id', (opts) => opts.mapFrom('userId'))
							.forMember('firstName', (opts) => opts.mapFrom('firstName'))
							.forMember('lastName', (opts) => opts.mapFrom('lastName'))
							.forMember('groups', (opts) => opts.mapFrom('roleNames'))
							.forMember('displayName', (opts) => opts.mapFrom('displayName'))
							.forMember('isHost', (opts) => opts.mapFrom('isHost'));

						let participants: User[] = automapper.map(User_SearchOutputDTOData, User, response.participants);
						session.participants = participants;
						return session;
					}
				)
			);
	}

	leaveRoom(id: number) {
		const requestUri = `${environment.api.baseUrl}/v1/collaborations/${id}/end`
		return this.httpClient.get<number>(requestUri);
	}

	createScreenshot(collaborationId: number, payload: MediaItem) {
		const requestUri = `${environment.api.baseUrl}/v1/collaborations/${collaborationId}/screenshots`;
		automapper
			.createMap(MediaItem, Document_InsertInputDTO)
			.forMember('directoryId', function (opts) { opts.mapFrom('directoryId'); })
			.forMember('documentTypeCode', function (opts) { opts.mapFrom('documentTypeCode'); })
			.forMember('documentName', function (opts) { opts.mapFrom('name'); })
			.forMember('documentUrl', function (opts) { opts.mapFrom('url'); })
			.forMember('metadata', function (opts) { opts.mapFrom('metadata'); })
			.forMember('contentType', function (opts) { opts.mapFrom('contentType'); })
			.forMember('size', function (opts) { opts.mapFrom('size'); })
			.forMember('isSRAllowed', function (opts) { opts.mapFrom('isSRAllowed'); });
		const request = automapper.map(MediaItem, Document_InsertInputDTO, payload);
		return this.httpClient.post<any>(requestUri, request);
	}

	getScreenshots(id: number): Observable<CollaborationScreenshot[]> {
		const requestUri = `${environment.api.baseUrl}/v1/collaborations/${id}/screenshots`;
		return this.httpClient.get<string[]>(requestUri).pipe(map(
			response => {
				automapper
					.createMap(CollaborationScreenshot_OutputDTO, CollaborationScreenshot)
					.forMember('id', function (opts) { opts.mapFrom('documentId'); })
					.forMember('name', function (opts) { opts.mapFrom('documentName'); })
					.forMember('url', function (opts) { opts.mapFrom('documentUrl'); });

				const screenshot: CollaborationScreenshot[] = automapper.map(CollaborationScreenshot_OutputDTO, CollaborationScreenshot, response);

				return screenshot;
			}
		));

	}

	deleteScreenshot(collaborationId: number, screenshotId: number) {
		const requestUri = `${environment.api.baseUrl}/v1/collaborations/${collaborationId}/screenshots/${screenshotId}`;
		return this.httpClient.delete(requestUri);
	}

	//#endregion
}
