import { HttpClient, HttpHeaders, HttpParams, HttpEvent, HttpRequest, HttpEventType, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { MediaDataService } from "./media.data.service";
import { map, tap, last, catchError } from 'rxjs/operators';
import * as automapper from 'automapper-ts';
import { MediaHistory, MediaHistories } from 'src/app/core/models/entity/media-history';
import { MediaItem, Media } from 'src/app/core/models/entity/media';
import { environment } from 'src/environments/environment';
import { Document_SearchOutputDTO, Document_AzureGetAllOutputDTO, FacetDTO, FacetResultDTO } from 'src/app/core/dtos/output/documents/Document_SearchOutputDTO';
import { Document_GetByIdOutputDTO, Thumbnail } from 'src/app/core/dtos/output/documents/Document_GetByIdOutputDTO';
import { Document_UpdateInputDTO } from 'src/app/core/dtos/input/documents/Document_UpdateInputDTO';
import { Document_GetAuditOutputDTO, Document_GetAuditOutputDTOData } from 'src/app/core/dtos/output/documents/Document_GetAuditOutputDTO';
import { Document_InsertInputDTO } from 'src/app/core/dtos/input/documents/Document_InsertInputDTO';
import { Favorite_OutputDTO, Favorite_OutputData } from 'src/app/core/dtos/output/favorites/Favorite_OutputDTO';
import { Favorite_InserInputDTO } from 'src/app/core/dtos/input/favorites/Favorite_InserInputDTO';
import { Tag } from 'src/app/core/models/entity/tag';
import { FieldsService } from '../../business/fields/fields.service';
import { Document_GetRelatedItemOutputDTO } from 'src/app/core/dtos/output/documents/Document_GetRelatedItemOutputDTO';
import { Document_InsertRelatedItemInputDTO } from 'src/app/core/dtos/input/documents/Document_InsertRelatedItemInputDTO';
import { DateService } from '../../business/dates/date.service';
import { Document_DeleteRelatedItemInputDTO } from 'src/app/core/dtos/input/documents/Document_DeleteRelatedItemInputDTO';
import { DocumentArray_InputDTO } from 'src/app/core/dtos/input/documents/DocumentArray_InputDTO';
import { throwError } from 'rxjs/internal/observable/throwError';
import { SurveyData } from 'src/app/core/dtos/output/telemetry/SurveyData';
import { Telemetry, TelemetryFile } from 'src/app/core/models/entity/telemetry';
import { TelemetryViewerOutputDTO } from 'src/app/core/dtos/output/telemetry/TelemetryViewerOutputDTO';
import { StreamArchiveOutputDTO, CameraRecording_GetAllOutputDTO } from 'src/app/core/dtos/input/live-stream/StreamArchiveOutputDTO';
import { StreamingArchive, CameraRecording, CameraFeed } from 'src/app/core/models/entity/streaming-archive';
import { Camera_GetAllOutputDTO } from 'src/app/core/dtos/output/live-stream/live-stream';
import { Filter, FilterDetail } from 'src/app/core/models/entity/filter';
import { Document_SearchInputDTO, AzureSearch_FilterDTO, Search_MapFilterDTO } from 'src/app/core/dtos/input/documents/Document_SearchInputDTO';
import { TranscriptSegment } from 'src/app/core/models/entity/transcript-segment';
import { thumbnails } from './../../../models/entity/media';
import { Transcript_GetAllOutputDTO } from 'src/app/core/dtos/output/transcribe/Transcript_GetAllOutputDTO';

@Injectable({
  providedIn: 'root'
})
export class MediaWebDataService implements MediaDataService {

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    })
  };

  constructor(private httpClient: HttpClient, private fieldsService: FieldsService, private dateService: DateService) { }

  getMedia(query: string, filters: Tag[], pageNumber?: number, pageSize?: number, isMapView?: boolean, mapCoordinates?: Search_MapFilterDTO[], directoryId?: number): Observable<Media> {

    const requestUri = environment.api.baseUrl + `/v1/documents/search`;

    automapper
      .createMap(Tag, AzureSearch_FilterDTO)
      .forMember('key', function (opts) { opts.mapFrom('name'); })
      .forMember('value', function (opts) { opts.mapFrom('value'); })
      .forMember('filterType', function (opts) { opts.mapFrom('searchType'); })
      .forMember('fieldDataType', function (opts) { opts.mapFrom('dataType'); })
      .forMember('rangeValue', function (opts) { opts.mapFrom('rangeValue'); });
    let requestFilters: AzureSearch_FilterDTO[] = automapper.map(Tag, AzureSearch_FilterDTO, filters);

    let payload = new Document_SearchInputDTO();
    payload.pageNumber = pageNumber;
    payload.isMapView = isMapView;
    payload.limit = pageSize;
    payload.query = query;
    payload.filters = requestFilters;
    payload.directoryId = directoryId;
    payload.coordinates = mapCoordinates;
    return this.httpClient.post<Document_SearchOutputDTO>(requestUri, payload)
      .pipe(
        map(
          response => {
            // Map response to media
            automapper
              .createMap(Document_SearchOutputDTO, Media)
              .forMember('pagination', function (opts) { opts.mapFrom('pagination'); })
              .forMember('data', function (opts) { opts.mapFrom('data'); })
              .forMember('doc', function (opts) { opts.mapFrom('data'); })
              .forMember('filters', function (opts) { opts.mapFrom('filters'); });

            let media: Media = automapper.map(Document_SearchOutputDTO, Media, response);
            // Media Items - Data
            automapper
              .createMap(Document_AzureGetAllOutputDTO, MediaItem)
              .forMember('id', function (opts) { opts.mapFrom('id'); })
              .forMember('documentId', function (opts) { opts.mapFrom('documentId'); })
              .forMember('favoriteId', function (opts) { opts.mapFrom('favoriteId'); })
              .forMember('entityType', function (opts) { opts.mapFrom('EntityType'); })
              .forMember('entityId', function (opts) { opts.mapFrom('EntityId'); })
              .forMember('isDirectory', function (opts) { opts.mapFrom('isDirectory'); })
              .forMember('directoryId', function (opts) { opts.mapFrom('directoryId'); })
              .forMember('documentTypeCode', function (opts) { opts.mapFrom('DocumentTypeCode'); })
              .forMember('name', function (opts) { opts.mapFrom('documentName'); })
              .forMember('documentName', function (opts) { opts.mapFrom('documentName'); })
              .forMember('url', function (opts) { opts.mapFrom('documentUrl'); })
              .forMember('metadata', function (opts) { opts.mapFrom('Metadata'); })
              .forMember('contentType', function (opts) { opts.mapFrom('ContentType'); })
              .forMember('containerId', function (opts) { opts.mapFrom('ContainerId'); })
              .forMember('hasChild', function (opts) { opts.mapFrom('hasChild'); })
              .forMember('size', function (opts) { opts.mapFrom('Size'); })
              .forMember('thumbnail', function (opts) { opts.mapFrom('thumbnailUrl'); })
              .forMember('thumbnails', function (opts) { opts.mapFrom('thumbnail'); })
              .forMember('isDeleted', function (opts) { opts.mapFrom('IsDeleted'); })
              .forMember('isFavorite', function (opts) { opts.mapFrom('isFavorite'); })
              .forMember('isChecked', function () { return false; })
              .forMember('status', function (opts) { opts.mapFrom('status'); })
              .forMember('parentId', function (opts) { opts.mapFrom('parentId'); })
              .forMember('createdOn', function (opts) { opts.mapFrom('createdOn'); })
              .forMember('createdBy', function (opts) { opts.mapFrom('createdBy'); })
              .forMember('modifiedOn', function (opts) { opts.mapFrom('modifiedOn'); })
              .forMember('latitude', function (opts) { opts.mapFrom('latitude'); })
              .forMember('longitude', function (opts) { opts.mapFrom('longitude'); })
              .forMember('modifiedBy', function (opts) { opts.mapFrom('modifiedBy'); });

            let mediaItems: MediaItem[] = automapper.map(Document_AzureGetAllOutputDTO, MediaItem, media.data);
            mediaItems.forEach(item => {
              const docTypes = ['DOCX', 'CSV', 'DOC', 'TXT', 'XLS', 'XLSX', 'PDF', 'TELEMETRY'];
              const type = item.documentTypeCode ? item.documentTypeCode.toUpperCase() : '';
              if (docTypes.includes(type)) item.thumbnail = this.fieldsService.setPlaceholderByType(type);
              item.type = type;
              item.name = item.documentId ? item.name.toUpperCase() : item.name;
              item.nameWithoutExtension = item.name ? this.fieldsService.removeFileExtension(item.name) : 'UNKNOWN';
              item.parentId = item.parentId !== 0 ? item.parentId : null; // for treeview
            });
            media.data = mediaItems;

            // Filters
            automapper
              .createMap(FacetDTO, Filter)
              .forMember('label', function (opts) { opts.mapFrom('label'); })
              .forMember('name', function (opts) { opts.mapFrom('fieldName'); })
              .forMember('searchType', function (opts) { opts.mapFrom('filterType'); })
              .forMember('dataType', function (opts) { opts.mapFrom('fieldDataType'); })
              .forMember('order', function (opts) { opts.mapFrom('sort'); })
              .forMember('options', function (opts) { opts.mapFrom('facetResults'); });

            let filters: Filter[] = automapper.map(FacetDTO, Filter, media.filters);
            media.filters = filters;

            // map options
            media.filters.forEach(filter => {
              automapper
                .createMap(FacetResultDTO, FilterDetail)
                .forMember('label', function (opts) { opts.mapFrom('value'); })
                .forMember('count', function (opts) { opts.mapFrom('count'); });

              let options: FilterDetail[] = automapper.map(FacetResultDTO, FilterDetail, filter.options);
              filter.options = options;
            });

            return media;
          })
      );
  }

  getMediaItem(id: any): Observable<MediaItem> {
    var requestUri = environment.api.baseUrl + `/v1/documents/${id}`;
    return this.httpClient.get<Document_GetByIdOutputDTO>(requestUri)
      .pipe(
        map(
          response => {
            automapper
              .createMap(response, MediaItem)
              .forMember('id', function (opts) { opts.mapFrom('documentId'); })
              .forMember('directoryId', function (opts) { opts.mapFrom('directoryId'); })
              .forMember('directoryName', function (opts) { opts.mapFrom('diirectoryName'); })
              .forMember('directoryParentId', function (opts) { opts.mapFrom('directoryParentId'); })
              .forMember('directoryParentName', function (opts) { opts.mapFrom('directoryParentName'); })
              .forMember('storageType', function (opts) { opts.mapFrom('storageType'); })
              .forMember('entityType', function (opts) { opts.mapFrom('entityType'); })
              .forMember('entityId', function (opts) { opts.mapFrom('entityId'); })
              .forMember('documentTypeCode', function (opts) { opts.mapFrom('documentTypeCode'); })
              .forMember('name', function (opts) { opts.mapFrom('documentName'); })
              .forMember('url', function (opts) { opts.mapFrom('documentUrl'); })
              .forMember('metadata', function (opts) { opts.mapFrom('metadata'); })
              .forMember('contentType', function (opts) { opts.mapFrom('contentType'); })
              .forMember('containerId', function (opts) { opts.mapFrom('containerId'); })
              .forMember('size', function (opts) { opts.mapFrom('size'); })
              .forMember('thumbnail', function (opts) { opts.mapFrom('thumbnailUrl'); })
              .forMember('thumbnails', function (opts) { opts.mapFrom('thumbnail'); })
              .forMember('thumbnailContainer', function (opts) { opts.mapFrom('thumbnailContainerUrl'); })
              .forMember('isDeleted', function (opts) { opts.mapFrom('IsDeleted'); })
              .forMember('status', function (opts) { opts.mapFrom('status'); });

            const mediaItem: MediaItem = automapper.map(response, MediaItem, response);

            const docTypes = ['DOCX', 'CSV', 'DOC', 'TXT', 'XLS', 'XLSX', 'PDF', 'TELEMETRY',];
            const type = mediaItem.documentTypeCode ? mediaItem.documentTypeCode.toUpperCase() : '';
            const metadata = JSON.parse(mediaItem.metadata);
            mediaItem.fieldName = metadata ? metadata.metadataField : '';
            mediaItem.metadata = mediaItem.metadata || '{}';
            if (docTypes.includes(type)) {
              mediaItem.thumbnail = this.fieldsService.setPlaceholderByType(type);


            }
            if (mediaItem.thumbnails.length > 0) {
              mediaItem.thumbnails.forEach(() => {
                automapper
                  .createMap(Thumbnail, thumbnails)
                  .forMember('time', (opts) => { opts.mapFrom('Time'); })
                  .forMember('src', (opts) => { opts.mapFrom('thumbnailUrl'); });
                let thumbnail: thumbnails[] = automapper.map(Thumbnail, thumbnails, mediaItem.thumbnails);
                mediaItem.thumbnails = thumbnail;
              })
            }
            mediaItem.type = type;
            mediaItem.nameWithoutExtension = this.fieldsService.removeFileExtension(mediaItem.name);
            return mediaItem;
          }
        )
      );
  }

  createMediaItem(payload: MediaItem): Observable<any> {
    const requestUri = environment.api.baseUrl + `/v1/documents`;

    automapper
      .createMap(MediaItem, Document_InsertInputDTO)
      .forMember('directoryId', function (opts) { opts.mapFrom('directoryId'); })
      .forMember('documentTypeCode', function (opts) { opts.mapFrom('documentTypeCode'); })
      .forMember('documentName', function (opts) { opts.mapFrom('name'); })
      .forMember('documentUrl', function (opts) { opts.mapFrom('url'); })
      .forMember('metadata', function (opts) { opts.mapFrom('metadata'); })
      .forMember('contentType', function (opts) { opts.mapFrom('contentType'); })
      .forMember('size', function (opts) { opts.mapFrom('size'); })
      .forMember('isSRAllowed', function (opts) { opts.mapFrom('isSRAllowed'); });
    const request = automapper.map(MediaItem, Document_InsertInputDTO, payload);
    return this.httpClient.post<any>(requestUri, request);
  }

  updateMediaItem(id: any, payload: MediaItem): Observable<any> {
    const requestUri = environment.api.baseUrl + `/v1/documents/${id}`;

    automapper
      .createMap(payload, Document_UpdateInputDTO)
      .forMember('storageType', function (opts) { opts.mapFrom('storageType'); })
      .forMember('entityType', function (opts) { opts.mapFrom('entityType'); })
      .forMember('entityId', function (opts) { opts.mapFrom('entityId'); })
      .forMember('documentTypeCode', function (opts) { opts.mapFrom('documentTypeCode'); })
      .forMember('documentName', function (opts) { opts.mapFrom('name'); })
      .forMember('documentUrl', function (opts) { opts.mapFrom('url'); })
      .forMember('metadata', function (opts) { opts.mapFrom('metadata'); })
      .forMember('contentType', function (opts) { opts.mapFrom('contentType'); })
      .forMember('containerId', function (opts) { opts.mapFrom('containerId'); })
      .forMember('size', function (opts) { opts.mapFrom('size'); })
      .forMember('thumbnailUrl', function (opts) { opts.mapFrom('thumbnail'); })
      .forMember('isDeleted', function (opts) { opts.mapFrom('isDeleted'); })
      .forMember('status', function (opts) { opts.mapFrom('status'); });

    const request = automapper.map(payload, Document_UpdateInputDTO, payload);

    return this.httpClient.put(requestUri, request);
  }

  getPlaylist(documentIds: string[]): Observable<MediaItem[]> {
    const requestUri = environment.api.baseUrl + `/v1/documents/playlist`;

    const request: DocumentArray_InputDTO = { Documents: documentIds }

    return this.httpClient.post<Document_GetByIdOutputDTO[]>(requestUri, request)
      .pipe(
        map(response => {
          automapper
            .createMap(Document_GetByIdOutputDTO, MediaItem)
            .forMember('id', function (opts) { opts.mapFrom('documentId'); })
            .forMember('directoryId', function (opts) { opts.mapFrom('DirectoryId'); })
            .forMember('directoryName', function (opts) { opts.mapFrom('DiirectoryName'); })
            .forMember('directoryParentId', function (opts) { opts.mapFrom('DirectoryParentId'); })
            .forMember('directoryParentName', function (opts) { opts.mapFrom('DirectoryParentName'); })
            .forMember('storageType', function (opts) { opts.mapFrom('StorageType'); })
            .forMember('entityType', function (opts) { opts.mapFrom('EntityType'); })
            .forMember('entityId', function (opts) { opts.mapFrom('EntityId'); })
            .forMember('documentTypeCode', function (opts) { opts.mapFrom('DocumentTypeCode'); })
            .forMember('name', function (opts) { opts.mapFrom('documentName'); })
            .forMember('url', function (opts) { opts.mapFrom('documentUrl'); })
            .forMember('metadata', function (opts) { opts.mapFrom('Metadata'); })
            .forMember('contentType', function (opts) { opts.mapFrom('ContentType'); })
            .forMember('containerId', function (opts) { opts.mapFrom('ContainerId'); })
            .forMember('size', function (opts) { opts.mapFrom('Size'); })
            .forMember('isFavorite', function (opts) { opts.mapFrom('isFavorite'); })
            .forMember('favoriteId', function (opts) { opts.mapFrom('favoriteId'); })
            .forMember('thumbnail', function (opts) { opts.mapFrom('thumbnailUrl'); })
            .forMember('thumbnails', function (opts) { opts.mapFrom('thumbnail'); })
            .forMember('isDeleted', function (opts) { opts.mapFrom('IsDeleted'); })
            .forMember('status', function (opts) { opts.mapFrom('Status'); });

          let mediaItems: MediaItem[] = automapper.map(Document_GetByIdOutputDTO, MediaItem, response);

          mediaItems.forEach(item => {
            const docTypes = ['DOCX', 'CSV', 'DOC', 'TXT', 'XLS', 'XLSX', 'PDF', 'TELEMETRY'];
            const type = item.documentTypeCode ? item.documentTypeCode.toUpperCase() : '';
            if (docTypes.includes(type)) {
              item.thumbnail = this.fieldsService.setPlaceholderByType(type);
              automapper
                .createMap(Thumbnail, thumbnails)
                .forMember('time', (opts) => { opts.mapFrom('time'); })
                .forMember('src', (opts) => { opts.mapFrom('thumbnailUrl'); });

            };
            item.type = type;
            item.name = item.documentId ? item.name.toUpperCase() : item.name;
            item.nameWithoutExtension = item.name ? this.fieldsService.removeFileExtension(item.name) : 'UNKNOWN';
            item.modifiedOnString = this.dateService.formatToString(item.modifiedOn, 'MMM DD, YYYY hh:mm');

          });
          return mediaItems;
        })
      );
  }

  getRelatedItems(id: string): Observable<MediaItem[]> {
    var requestUri = environment.api.baseUrl + `/v1/documents/${id}/relateditems`;

    return this.httpClient.get<Document_GetRelatedItemOutputDTO[]>(requestUri).pipe(
      map(
        response => {
          automapper
            .createMap(Document_GetRelatedItemOutputDTO, MediaItem)
            .forMember('documentId', function (opts) { opts.mapFrom('documentId'); })
            .forMember('relatedItemId', function (opts) { opts.mapFrom('relatedItemId'); })
            .forMember('relatedDocumentId', function (opts) { opts.mapFrom('relatedDocumentId'); })
            .forMember('directoryId', function (opts) { opts.mapFrom('DirectoryId'); })
            .forMember('entityType', function (opts) { opts.mapFrom('EntityType'); })
            .forMember('entityId', function (opts) { opts.mapFrom('EntityId'); })
            .forMember('documentTypeCode', function (opts) { opts.mapFrom('DocumentTypeCode'); })
            .forMember('name', function (opts) { opts.mapFrom('documentName'); })
            .forMember('url', function (opts) { opts.mapFrom('documentUrl'); })
            .forMember('metadata', function (opts) { opts.mapFrom('Metadata'); })
            .forMember('contentType', function (opts) { opts.mapFrom('ContentType'); })
            .forMember('size', function (opts) { opts.mapFrom('Size'); })
            .forMember('thumbnail', function (opts) { opts.mapFrom('thumbnailUrl'); })
            .forMember('thumbnails', function (opts) { opts.mapFrom('thumbnail'); })
            .forMember('status', function (opts) { opts.mapFrom('Status'); })
            .forMember('isFavorite', function (opts) { opts.mapFrom('isFavorite'); })
            .forMember('favoriteId', function (opts) { opts.mapFrom('favoriteId'); })
            .forMember('createdOn', function (opts) { opts.mapFrom('createdOn'); })
            .forMember('createdBy', function (opts) { opts.mapFrom('createdBy'); })
            .forMember('modifiedOn', function (opts) { opts.mapFrom('modifiedOn'); })
            .forMember('modifiedBy', function (opts) { opts.mapFrom('modifiedBy'); });

          let mediaItems: MediaItem[] = automapper.map(Document_GetRelatedItemOutputDTO, MediaItem, response);
          mediaItems.forEach(item => {
            let docTypes = ['DOCX', 'CSV', 'DOC', 'TXT', 'XLS', 'XLSX', 'PDF', 'MP4', 'MKV', 'MOV', 'AVI'];
            const type = item.documentTypeCode ? item.documentTypeCode.toUpperCase() : '';
            if (docTypes.includes(type)) item.thumbnail = this.fieldsService.setPlaceholderByType(type);
            item.type = type;
            item.nameWithoutExtension = item.name ? this.fieldsService.removeFileExtension(item.name).toUpperCase() : 'UNKNOWN';
            item.modifiedOnString = this.dateService.formatToString(item.modifiedOn, 'MMM DD, YYYY hh:mm');
          });
          return mediaItems;
        }
      )
    );
  }

  addRelatedItems(documentId: string, relatedDocumentIds: string[]) {
    const requestUri = environment.api.baseUrl + `/v1/documents/${documentId}/relateditems`;
    const request: Document_InsertRelatedItemInputDTO = { RelatedDocumentIds: relatedDocumentIds };
    return this.httpClient.post<any>(requestUri, request);
  }

  deleteRelatedItems(documentId: string, relatedDocumentIds: string[]) {
    const requestUri = environment.api.baseUrl + `/v1/documents/${documentId}/relateditems`;
    const request: Document_DeleteRelatedItemInputDTO = { RelatedDocumentIds: relatedDocumentIds };
    const options = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
      body: request
    };

    return this.httpClient.delete<any>(requestUri, options).pipe(map(
      response => {
        console.log('MediaWebDataService - deleteRelatedItems: ', response);
        return response;
      })
    );
  }

  deleteMediaItem(id: any) {
    const requestUri = environment.api.baseUrl + `/v1/documents/${id}`;
    return this.httpClient.delete(requestUri);
  }

  download(url: string): Observable<HttpEvent<Blob>> {
    const req = new HttpRequest('GET', url, null, {
      reportProgress: true,
      responseType: "blob"
    });
    return this.httpClient.request(req).pipe(
      map(event => this.getEventMessage(event)),
      tap(message => console.log('MediaComponent Service tap: ', (message))),
      last(), // return last (completed) message to caller
      catchError(err => this.handleError(err))
    );
  }

  updateMetadata(id: any, metadata: string) {
    const requestUri = environment.api.baseUrl + `/v1/documents/${id}/metadata`;
    let request: { metadata: string } = { metadata };
    return this.httpClient.put(requestUri, request);
  }

  getVideoTranscript(documentId: any): Observable<TranscriptSegment[]> {
    const requestUri = environment.api.baseUrl + `/v1/documents/${documentId}/transcribe`;

    return this.httpClient.get<any[]>(requestUri)
      .pipe(
        map(response => {
          automapper.
            createMap(Transcript_GetAllOutputDTO, TranscriptSegment)
            .forMember('startTime', function (opts) { opts.mapFrom('startTime'); })
            .forMember('endTime', function (opts) { opts.mapFrom('endTime'); })
            .forMember('text', function (opts) { opts.mapFrom('display'); });

          const segments: TranscriptSegment[] = automapper.map(Transcript_GetAllOutputDTO, TranscriptSegment, response);
          return segments;
        })
      );
  }

  private getEventMessage(event: HttpEvent<any>) {
    switch (event.type) {
      case HttpEventType.DownloadProgress:
        // Compute and show the % done:
        const percentDone = Math.round(100 * event.loaded / event.total);
        console.log('MediaComponent Service UploadProgress: ', percentDone);
        return event as HttpEvent<Blob>;

      case HttpEventType.Response:
        console.log('MediaComponent Service Response: ', event);
        return event as HttpEvent<Blob>;
      default:
        console.log('MediaComponent Service default: ', event);
        return event as HttpEvent<Blob>;
    }
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.error);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    // return an observable with a user-facing error message
    return throwError(
      'Something bad happened; please try again later.');
  };

  //#region Filters

  applyMediaFilters(): Observable<Media> {
    throw new Error("Method not implemented.");
  }

  //#endregion

  //#region Favorites

  getFavorites(pageNumber?: number, pageSize?: number, query?: string): Observable<Media> {
    var requestUri = environment.api.baseUrl + `/v1/favorites`;
    const options = {
      params: new HttpParams()
    };
    if (pageNumber) options.params = options.params.set('pageNumber', pageNumber.toString());
    if (pageSize) options.params = options.params.set('limit', pageSize.toString());
    if (query) options.params = options.params.set('query', query);
    return this.httpClient.get<Favorite_OutputDTO>(requestUri, options).pipe(
      map(
        response => {
          // Map response to favorites
          automapper
            .createMap(Favorite_OutputDTO, Media)
            .forMember('pagination', function (opts) { opts.mapFrom('Pagination'); })
            .forMember('data', function (opts) { opts.mapFrom('Data'); });

          let favorites: Media = automapper.map(Favorite_OutputDTO, Media, response);

          // Map data from response to favorite items
          if (favorites.data.length > 0) {
            automapper
              .createMap(Favorite_OutputData, MediaItem)
              .forMember('favoriteId', function (opts) { opts.mapFrom('favoriteId'); })
              .forMember('documentId', function (opts) { opts.mapFrom('documentId'); })
              .forMember('entityType', function (opts) { opts.mapFrom('EntityType'); })
              .forMember('entityId', function (opts) { opts.mapFrom('EntityId'); })
              .forMember('directoryId', function (opts) { opts.mapFrom('DirectoryId'); })
              .forMember('documentTypeCode', function (opts) { opts.mapFrom('DocumentTypeCode'); })
              .forMember('name', function (opts) { opts.mapFrom('name'); })
              .forMember('url', function (opts) { opts.mapFrom('documentUrl'); })
              .forMember('metadata', function (opts) { opts.mapFrom('Metadata'); })
              .forMember('contentType', function (opts) { opts.mapFrom('ContentType'); })
              .forMember('containerId', function (opts) { opts.mapFrom('ContainerId'); })
              .forMember('hasChild', function (opts) { opts.mapFrom('hasChild'); })
              .forMember('size', function (opts) { opts.mapFrom('Size'); })
              .forMember('thumbnail', function (opts) { opts.mapFrom('thumbnailUrl'); })
              .forMember('thumbnails', function (opts) { opts.mapFrom('thumbnail'); })
              .forMember('isDeleted', function (opts) { opts.mapFrom('IsDeleted'); })
              .forMember('status', function (opts) { opts.mapFrom('Status'); })
              .forMember('parentId', function (opts) { opts.mapFrom('parentId'); })
              .forMember('isFavorite', function () { return true; })
              .forMember('isSearch', function (opts) { opts.mapFrom('hasQueryFilter'); })
              .forMember('createdOn', function (opts) { opts.mapFrom('createdOn'); })
              .forMember('createdBy', function (opts) { opts.mapFrom('createdBy'); })
              .forMember('modifiedOn', function (opts) { opts.mapFrom('modifiedOn'); })
              .forMember('modifiedBy', function (opts) { opts.mapFrom('modifiedBy'); });

            let favoriteItems: MediaItem[] = automapper.map(Favorite_OutputData, MediaItem, favorites.data);

            favoriteItems.forEach(item => {
              const docTypes = ['DOCX', 'CSV', 'DOC', 'TXT', 'XLS', 'XLSX', 'PDF', 'TELEMETRY'];
              const type = item.documentTypeCode ? item.documentTypeCode.toUpperCase() : '';
              if (docTypes.includes(type)) item.thumbnail = this.fieldsService.setPlaceholderByType(type);
              item.type = type;
              item.nameWithoutExtension = this.fieldsService.removeFileExtension(item.name || 'Unknown').toUpperCase();
              if (item.isSearch) item.type = 'FILTER';
              item.modifiedOnString = this.dateService.formatToString(item.modifiedOn, 'MMM DD, YYYY hh:mm');
            });
            favoriteItems.sort((a, b) => new Date(b.modifiedOn).getTime() - new Date(a.modifiedOn).getTime());
            favorites.data = favoriteItems;
          }
          return favorites;
        })
    );
  }

  addFavorite(mediaItemId?: number, name?: string, tags?: Tag[]) {
    const requestUri = environment.api.baseUrl + `/v1/favorites`;

    const request: Favorite_InserInputDTO = {
      entityId: mediaItemId ? mediaItemId.toString() : null,
      entityType: mediaItemId ? 'Document' : null,
      filterName: name,
      value: tags ? JSON.stringify(tags) : null
    }

    return this.httpClient.post<any>(requestUri, request);
  }

  removeFavorite(favoriteId: number) {
    const requestUri = environment.api.baseUrl + `/v1/favorites/${favoriteId}`;
    return this.httpClient.delete<any>(requestUri);
  }

  //#endregion

  getHistory(id: string, pageNumber?: number, pageSize?: number): Observable<MediaHistories> {
    const requestUri = environment.api.baseUrl + `/v1/documents/${id}/audit`;

    const options = { params: new HttpParams() };
    if (pageNumber) options.params = options.params.set('pageNumber', pageNumber.toString());
    if (pageSize) options.params = options.params.set('limit', pageSize.toString());

    return this.httpClient.get<Document_GetAuditOutputDTO[]>(requestUri, options)
      .pipe(
        map(
          response => {
            automapper
              .createMap(Document_GetAuditOutputDTO, MediaHistories)
              .forMember('pagination', function (opts) { opts.mapFrom('Pagination'); })
              .forMember('data', function (opts) { opts.mapFrom('Data'); });

            let mediaHistories: MediaHistories = automapper.map(Document_GetAuditOutputDTO, MediaHistories, response);

            automapper
              .createMap(Document_GetAuditOutputDTOData, MediaHistory)
              .forMember('auditId', function (opts) { opts.mapFrom('auditId'); })
              .forMember('eventName', function (opts) { opts.mapFrom('eventName'); })
              .forMember('entityType', function (opts) { opts.mapFrom('entityType'); })
              .forMember('entityId', function (opts) { opts.mapFrom('entityId'); })
              .forMember('columnName', function (opts) { opts.mapFrom('columnName'); })
              .forMember('oldValue', function (opts) { opts.mapFrom('oldValue'); })
              .forMember('newValue', function (opts) { opts.mapFrom('newValue'); })
              .forMember('createdOn', function (opts) { opts.mapFrom('createdOn'); })
              .forMember('createdBy', function (opts) { opts.mapFrom('createdByName'); });

            const histories: MediaHistory[] = automapper.map(Document_GetAuditOutputDTOData, MediaHistory, mediaHistories.data);
            mediaHistories.data = histories;

            return mediaHistories;
          })
      );
  }

  //#region Telemetry

  getTelemetry(id: any): Observable<Telemetry> {
    let requestUri = environment.api.baseUrl + `/v1/documents/${id}/telemetry`;
    return this.httpClient.get<TelemetryViewerOutputDTO>(requestUri)
      .pipe(
        map(response => {
          automapper.
            createMap(TelemetryViewerOutputDTO, Telemetry)
            .forMember('feeds', function (opts) { opts.mapFrom('feeds'); })
            .forMember('files', function (opts) { opts.mapFrom('files'); });

          const telemetry: Telemetry = automapper.map(TelemetryViewerOutputDTO, Telemetry, response);

          automapper
            .createMap(response, MediaItem)
            .forMember('id', function (opts) { opts.mapFrom('documentId'); })
            .forMember('directoryId', function (opts) { opts.mapFrom('DirectoryId'); })
            .forMember('directoryName', function (opts) { opts.mapFrom('DiirectoryName'); })
            .forMember('directoryParentId', function (opts) { opts.mapFrom('DirectoryParentId'); })
            .forMember('directoryParentName', function (opts) { opts.mapFrom('DirectoryParentName'); })
            .forMember('storageType', function (opts) { opts.mapFrom('StorageType'); })
            .forMember('entityType', function (opts) { opts.mapFrom('EntityType'); })
            .forMember('entityId', function (opts) { opts.mapFrom('EntityId'); })
            .forMember('documentTypeCode', function (opts) { opts.mapFrom('DocumentTypeCode'); })
            .forMember('name', function (opts) { opts.mapFrom('documentName'); })
            .forMember('url', function (opts) { opts.mapFrom('documentUrl'); })
            .forMember('metadata', function (opts) { opts.mapFrom('Metadata'); })
            .forMember('contentType', function (opts) { opts.mapFrom('ContentType'); })
            .forMember('containerId', function (opts) { opts.mapFrom('ContainerId'); })
            .forMember('size', function (opts) { opts.mapFrom('Size'); })
            .forMember('thumbnail', function (opts) { opts.mapFrom('thumbnailUrl'); })
            .forMember('isDeleted', function (opts) { opts.mapFrom('IsDeleted'); })
            .forMember('status', function (opts) { opts.mapFrom('Status'); });

          const videoFeeds: MediaItem[] = automapper.map(response, MediaItem, telemetry.feeds);
          telemetry.feeds = videoFeeds || [];

          automapper
            .createMap(response, TelemetryFile)
            .forMember('name', function (opts) { opts.mapFrom('fileName'); })
            .forMember('data', function (opts) { opts.mapFrom('data'); });
          const files: TelemetryFile[] = automapper.map(response, TelemetryFile, telemetry.files);
          telemetry.files = files || [];

          return telemetry;
        })
      );


  }

  //#endregion

  //#region Streaming Archive

  getAvailableArchiveDays(asset: string, group: string): Observable<Date[]> {
    const requestUri = environment.api.baseUrl + `/v1/streamarchives/days`;

    const options = { params: new HttpParams() };
    options.params = options.params.set('asset', asset);
    options.params = options.params.set('group', group);

    return this.httpClient.get<Date[]>(requestUri, options)
      .pipe(
        map(response => {
          response.forEach(day => {
            day = new Date(day);
          });
          console.log("getAvailableArchiveDays: ", response);
          return response;
        })
      );
  }

  getArchiveVideos(asset: string, group: string, date: string, time?: string): Observable<StreamingArchive> {
    const requestUri = environment.api.baseUrl + `/v1/streamarchives`;
    const dateFilter = new Date(date);

    const options = { params: new HttpParams() };
    options.params = options.params.set('asset', asset);
    options.params = options.params.set('group', group);
    if (time) options.params = options.params.set('timelinefilter', time);
    else options.params = options.params.set('dateFilter', date);

    const mockUrl = './assets/mock/streaming-archive.json';

    return this.httpClient.get<StreamArchiveOutputDTO[]>(requestUri, options)
    // return this.httpClient.get<StreamArchiveOutputDTO[]>(mockUrl)
      .pipe(
        map(response => {
          automapper.
            createMap(StreamArchiveOutputDTO, StreamingArchive)
            .forMember('date', function (opts) { opts.mapFrom('dailyScheduleDate'); })
            .forMember('feeds', function (opts) { opts.mapFrom('feeds'); })
            .forMember('timelineDate', function (opts) { opts.mapFrom('timeLineDate'); });

          const streamArchive: StreamingArchive = automapper.map(StreamArchiveOutputDTO, StreamingArchive, response);

          automapper
            .createMap(Camera_GetAllOutputDTO, CameraFeed)
            .forMember('url', function () { return ''; })
            .forMember('durations', function (opts) { opts.mapFrom('recordings'); })
            .forMember('name', function (opts) { opts.mapFrom('name'); });
          const feeds: CameraFeed[] = automapper.map(Camera_GetAllOutputDTO, CameraFeed, streamArchive.feeds);
          streamArchive.feeds = feeds || [];

          feeds.forEach((feed) => {
            automapper
              .createMap(CameraRecording_GetAllOutputDTO, CameraRecording)
              .forMember('startTime', function (opts) { opts.mapFrom('startDate'); })
              .forMember('endTime', function (opts) { opts.mapFrom('endDate'); })
              .forMember('url', function (opts) { opts.mapFrom('documentUrl'); })
              .forMember('cameraRecordingStatus', function (opts) { opts.mapFrom('cameraRecordingStatus'); });

            const cameraRecordings: CameraRecording[] = automapper.map(CameraRecording_GetAllOutputDTO, CameraRecording, feed.durations);
            feed.durations = cameraRecordings || [];
          })
          return streamArchive;
        })
      )
  }

  //#endregion

}