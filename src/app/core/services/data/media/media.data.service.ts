import { Injectable } from '@angular/core';
import { MediaItem, Media } from 'src/app/core/models/entity/media';
import { MediaHistories } from 'src/app/core/models/entity/media-history';
import { Tag } from 'src/app/core/models/entity/tag';
import { Observable } from 'rxjs/internal/Observable';
import { Telemetry } from 'src/app/core/models/entity/telemetry';
import { HttpEvent } from '@angular/common/http';
import { StreamingArchive } from 'src/app/core/models/entity/streaming-archive';
import { TranscriptSegment } from 'src/app/core/models/entity/transcript-segment';
import { Search_MapFilterDTO } from 'src/app/core/dtos/input/documents/Document_SearchInputDTO';

@Injectable({
    providedIn: 'root'
})
export abstract class MediaDataService {

    constructor() { }

    abstract getMedia(query: string, filters: Tag[], pageNumber?: number, pageSize?: number, isMapView?: boolean, mapCoordinates?: Search_MapFilterDTO[], directoryId?: number): Observable<Media>;
    abstract getMediaItem(id: number): Observable<MediaItem>;
    abstract getPlaylist(documentIds: string[]): Observable<MediaItem[]>;
    abstract createMediaItem(payload: MediaItem): Observable<any>;
    abstract updateMediaItem(id: any, payload: MediaItem): Observable<any>;
    abstract getRelatedItems(id: string): Observable<MediaItem[]>;
    abstract addRelatedItems(documentId: string, relatedDocumentIds: string[]);
    abstract deleteRelatedItems(documentId: string, relatedDocumentIds: string[]);
    abstract getHistory(id: string, pageNumber?: number, pageSize?: number): Observable<MediaHistories>;
    abstract download(url: string): Observable<HttpEvent<Blob>>;
    abstract updateMetadata(id: any, metadata: string);
    abstract getVideoTranscript(documentId: any): Observable<TranscriptSegment[]>;
    abstract deleteMediaItem(id: any): Observable<any>;

    //#region Favorites

    abstract getFavorites(pageNumber?: number, pageSize?: number, query?: string): Observable<Media>;
    abstract addFavorite(mediaItemId?: number, name?: string, tags?: Tag[]);
    abstract removeFavorite(favoriteId: number);

    //#endregion

    //#region Telemetry view

    abstract getTelemetry(id: any): Observable<Telemetry>;

    //#endregion

    //#region Streaming Archive

    abstract getAvailableArchiveDays(asset: string, group: string): Observable<Date[]>;
    abstract getArchiveVideos(asset: string, group: string, date: string, time?: any): Observable<StreamingArchive>;

    //#endregion
}
