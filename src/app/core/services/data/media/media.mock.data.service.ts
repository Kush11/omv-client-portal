import { MediaItem, Media } from './../../../models/entity/media';
import { HttpClient, HttpEvent } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MediaDataService } from './media.data.service';
import { MediaHistory, MediaHistories } from 'src/app/core/models/entity/media-history';
import { map } from 'rxjs/operators';
import { MediaTreeGrid } from 'src/app/core/models/media-tree-grid';
import { Tag } from 'src/app/core/models/entity/tag';
import { Observable } from 'rxjs/internal/Observable';
import { of } from 'rxjs/internal/observable/of';
import { Telemetry } from 'src/app/core/models/entity/telemetry';
import { observable } from 'rxjs';
import { StreamingArchive, StreamingArchiveModel } from 'src/app/core/models/entity/streaming-archive';
import { TranscriptSegment } from 'src/app/core/models/entity/transcript-segment';
import { Search_MapFilterDTO } from 'src/app/core/dtos/input/documents/Document_SearchInputDTO';

@Injectable({
  providedIn: 'root'
})
export class MediaMockDataService implements MediaDataService {
  updateMetadata(id: any, metadata: string) {
    throw new Error("Method not implemented.");
  }
  getVideoTranscript(documentId: any): Observable<TranscriptSegment[]> {
    throw new Error("Method not implemented.");
  }
  getAvailableArchiveDays(asset: string, group: string): Observable<Date[]> {
    throw new Error("Method not implemented.");
  }
  applyMediaFilters(filters?: Tag[], pageNumber?: number, pageSize?: number, query?: string): Observable<Media> {
    throw new Error("Method not implemented.");
  }
  download(url: string): Observable<HttpEvent<Blob>> {
    throw new Error("Method not implemented.");
  }

  constructor(private httpClient: HttpClient) { }

  getMedia(query: string, filters: Tag[], pageNumber?: number, pageSize?: number, isMapView?: boolean, mapCoordinates?: Search_MapFilterDTO[], directoryId?: number): Observable<Media> {
    var url = `./assets/mock/media.json`;
    let data = this.httpClient.get<Media>(url).pipe(
      map(item => {
        // if (pageNumber === 1) {
        //   let retVal = item.splice(0,4);          
        //   console.log(retVal);
        //   return retVal;
        // } else if (pageNumber === 2) {
        //   let retVal = item.splice(4, item.length);          
        //   console.log(retVal);
        //   return retVal;
        // } else{
        //   return item;
        // }
        return item;
      }));
    return data;
  }

  getMediaItem(id: number): Observable<any> {
    var url = `./assets/mock/media.json`;
    let data = this.httpClient.get<any[]>(url).pipe(
      map(items => {
        return items.find(x => x.id === id.toString());
      })
    );
    return data;
  }

  getPlaylist(documentIds: string[]): Observable<MediaItem[]> {
    throw new Error("Method not implemented.");
  }

  createMediaItem(payload: MediaItem): Observable<any> {
    return of(1);
  }

  updateMediaItem(id: any, payload: MediaItem): Observable<any> {
    return of(1);
  }

  getRelatedItems(id: string): Observable<MediaItem[]> {
    throw new Error("Method not implemented.");
  }

  addRelatedItems(documentId: string, relatedDocumentIds: string[]) {
    throw new Error("Method not implemented.");
  }

  deleteRelatedItems(documentId: string, relatedDocumentIds: string[]) {
    throw new Error("Method not implemented.");
  }

  deleteMediaItem(id: any): Observable<any> {
    return of();
  }

  getHistory(id: string, pageNumber?: number, pageSize?: number): Observable<MediaHistories> {
    throw new Error("Method not implemented.");
  }

  getFavorites(pageNumber?: number, pageSize?: number, query?: string): Observable<Media> {
    var url = `./assets/mock/search-card.json`;
    let data = this.httpClient.get<Media>(url).pipe(
      map(item => {
        return item;
      }));
    return data;
  }

  addFavorite(mediaItemId?: number, name?: string, tags?: Tag[]) {
    throw new Error("Method not implemented.");
  }

  removeFavorite(favoriteId: number) {
    throw new Error("Method not implemented.");
  }

  getMediaTreeData(): Observable<MediaTreeGrid[]> {
    var url = `./assets/mock/media-treeview.json`;
    let data = this.httpClient.get<MediaTreeGrid[]>(url);
    return data;
  }

  getMetadata(id: number): Observable<any[]> {
    var url = `./assets/mock/metadata.json`;
    let data = this.httpClient.get<any[]>(url);
    return data;
  }

  getMetadataOptions(id: any): Observable<any[]> {
    var url = `./assets/mock/metadata-list.json`;
    let data = this.httpClient.get<any[]>(url).pipe(
      map(options => {
        return options.filter(x => x.optionsId === id);
      })
    );
    return data;
  }
  getSurvey(): Observable<any[]> {
    var url = `./assets/mock/metadata-list.json`;
    // let data = this.httpClient.get<any[]>(url).pipe(
    //   map(options => {
    //     return options.filter(x => x.optionsId === id);
    //   })
    // );
    // return data;
    throw new Error('Method have not been implemented');
  }
  getEvents(): Observable<any[]> {
    // var url = `./assets/mock/metadata-list.json`;
    // let data = this.httpClient.get<any[]>(url).pipe(
    //   map(options => {
    //     return options.filter(x => x.optionsId === id);
    //   })
    // );
    // return data;
    throw new Error('Method have not been implemented');
  }

  getTelemetry(id: any): Observable<Telemetry> {
    throw new Error('Method have not been implemented');
  }

  getArchiveVideos(asset: string, group: string, date: string, time?: string): Observable<StreamingArchive> {
    throw new Error('Method have not been implemented');
  }

}
