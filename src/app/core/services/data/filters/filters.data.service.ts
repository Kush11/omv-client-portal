import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { MetadataField } from 'src/app/core/models/entity/metadata-field';

@Injectable({
  providedIn: 'root'
})
export abstract class FiltersDataService {

  constructor() { }

  abstract getFilters(): Observable<MetadataField[]>;
}