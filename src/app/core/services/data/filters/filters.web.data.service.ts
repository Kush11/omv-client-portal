import { HttpClient } from '@angular/common/http';
import { FiltersDataService } from './filters.data.service';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import * as automapper from 'automapper-ts';
import { Filter_GetAllOutputDTO } from 'src/app/core/dtos/output/filters/Filter_GetAllOutputDTO';
import { MetadataField_GetListItemByIdOutputDTO } from 'src/app/core/dtos/output/metadata/MetadataField_GetListItemByIdOutputDTO';
import { ListItem } from 'src/app/core/models/entity/list-item';
import { Observable } from 'rxjs/internal/Observable';
import { map } from 'rxjs/internal/operators/map';
import { MetadataField } from 'src/app/core/models/entity/metadata-field';

@Injectable({
  providedIn: 'root'
})
export class FiltersWebDataService implements FiltersDataService {

  constructor(private httpClient: HttpClient) { }

  getFilters(): Observable<MetadataField[]> {
    var requestUri = environment.api.baseUrl + `/v1/filters`;

    return this.httpClient.get<Filter_GetAllOutputDTO[]>(requestUri).pipe(
      map(response => {
        // map filters
        automapper
          .createMap(Filter_GetAllOutputDTO, MetadataField)
          .forMember('name', function (opts) { opts.mapFrom('fieldName'); })
          .forMember('label', function (opts) { opts.mapFrom('label'); })
          .forMember('listName', function (opts) { opts.mapFrom('metadataListName'); })
          .forMember('type', function (opts) { opts.mapFrom('type'); })
          .forMember('isRequired', function (opts) { opts.mapFrom('isRequired'); })
          .forMember('order', function (opts) { opts.mapFrom('sort'); })
          .forMember('type', function (opts) { opts.mapFrom('type'); })
          .forMember('options', function (opts) { opts.mapFrom('options'); });

        let filters: MetadataField[] = automapper.map(Filter_GetAllOutputDTO, MetadataField, response);

        // map filter options
        filters.forEach(filter => {
          automapper
            .createMap(MetadataField_GetListItemByIdOutputDTO, ListItem)
            .forMember('value', function (opts) { opts.mapFrom('itemValue'); })
            .forMember('description', function (opts) { opts.mapFrom('itemDescription'); })
            .forMember('sort', function (opts) { opts.mapFrom('itemSort'); })
            .forMember('isSelected', function () { false });

          let options: ListItem[] = automapper.map(MetadataField_GetListItemByIdOutputDTO, ListItem, filter.options);
          filter.options = options;
        });
        return filters;
      })
    );
  }

}