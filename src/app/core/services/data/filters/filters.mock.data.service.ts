import { HttpClient } from '@angular/common/http';
import { FiltersDataService } from './filters.data.service';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { Media } from 'src/app/core/models/entity/media';
import { Tag } from 'src/app/core/models/entity/tag';
import { MetadataField } from 'src/app/core/models/entity/metadata-field';
import { Filter } from 'src/app/core/models/entity/filter';

@Injectable({
  providedIn: 'root'
})
export class FiltersMockDataService implements FiltersDataService {

  constructor(private httpClient: HttpClient) { }

  getFilters(): Observable<MetadataField[]> {
    var url = `./assets/mock/filter-metadata.json`;
    let data = this.httpClient.get<any[]>(url);
    return data;
  }

}