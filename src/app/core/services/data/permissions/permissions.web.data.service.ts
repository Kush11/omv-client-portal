import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { map } from 'rxjs/internal/operators/map';
import { PermissionsDataService } from './permissions.data.service';
import { Permission } from 'src/app/core/enum/permission';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Permission_GetAllOutputDTO } from 'src/app/core/dtos/output/permissions/Permission_GetAllOutputDTO';
import * as automapper from 'automapper-ts';

@Injectable({
	providedIn: 'root'
})
export class PermissionsWebService implements PermissionsDataService {

	constructor(private httpClient: HttpClient) { }

	getPermissions(): Observable<Permission[]> {
		const requestUri = environment.api.baseUrl + `/v1/permissions`;

		return this.httpClient.get<Permission_GetAllOutputDTO[]>(requestUri)
			.pipe(
				map(response => {
					automapper
						.createMap(response, Permission)
						.forMember('id', function (opts) { opts.mapFrom('permissionId'); })
						.forMember('name', function (opts) { opts.mapFrom('permissionDescription'); })
						.forMember('status', function (opts) { opts.mapFrom('status'); });
					const permissions: Permission[] = automapper.map(response, Permission, response);
					return permissions;
				})
			);
	}
}
