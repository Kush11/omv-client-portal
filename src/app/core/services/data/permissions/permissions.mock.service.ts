import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { PermissionsDataService } from './permissions.data.service';
import { Permission } from 'src/app/core/enum/permission';
import { HttpClient } from '@angular/common/http';


@Injectable({
    providedIn: 'root'
})
export  class PermissionsMockService implements PermissionsDataService {
    getPermissions(): Observable<Permission[]> {
        throw new Error("Method not implemented.");
    }

    mockUrl = `./assets/mock/permissions.json`;
    constructor(private httpClient: HttpClient) {}

    // getPermissions(): Observable<Group_permissionDTO[]> {
    //     return this.httpClient.get<Group_permissionDTO[]>(this.mockUrl);
    //   }

}
