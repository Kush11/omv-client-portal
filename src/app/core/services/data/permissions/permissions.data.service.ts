import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { Permission } from 'src/app/core/enum/permission';


@Injectable({
    providedIn: 'root'
})
export abstract class PermissionsDataService {

    abstract getPermissions(): Observable<Permission[]>;
}
