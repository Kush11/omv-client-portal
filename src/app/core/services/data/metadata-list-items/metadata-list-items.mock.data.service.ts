import { Injectable } from '@angular/core';
import { MetadataListItemsDataService } from './metadata-list-items.data.service';

@Injectable({
  providedIn: 'root'
})
export class MetadataListItemsMockDataService implements MetadataListItemsDataService {
  getMetadataListItem(id: number): import('rxjs').Observable<import('../../../models/entity/metadata-list-item').MetadataListItem> {
    throw new Error('Method not implemented.');
  }

  
  getMetadataListItems(id: number): import('rxjs').Observable<import('../../../models/entity/metadata-list-item').MetadataListItem[]> {
    throw new Error('Method not implemented.');
  } 
  createMetadataListItem(id: number, payload: import('../../../models/entity/metadata-list-item').MetadataListItem): import('rxjs').Observable<import('../../../models/entity/metadata-list-item').MetadataListItem> {
    throw new Error('Method not implemented.');
  }

  updateMetadataListItem(id: number, payload): import('rxjs').Observable<import('../../../models/entity/metadata-list-item').MetadataListItem> {
    throw new Error('Method not implemented.');
  }

  
  removeMetadataListItem(id: number, metadataListItemId: number) {
    throw new Error('Method not implemented.');
  }
}