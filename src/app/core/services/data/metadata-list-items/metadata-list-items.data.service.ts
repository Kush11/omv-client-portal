import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { MetadataListItem } from 'src/app/core/models/entity/metadata-list-item';

@Injectable({
  providedIn: 'root'
})
export abstract class MetadataListItemsDataService {
  abstract getMetadataListItem(id: number): Observable<MetadataListItem>;
  abstract getMetadataListItems(id: number): Observable<MetadataListItem[]>;
  abstract updateMetadataListItem(id: number, payload: MetadataListItem);
  abstract removeMetadataListItem(id: number, metadataListItemId: number);
}
