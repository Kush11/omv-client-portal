import { Injectable } from '@angular/core';
import { MetadataListItemsDataService } from './metadata-list-items.data.service';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs/internal/Observable';
import { MetadataListItem } from 'src/app/core/models/entity/metadata-list-item';
import { MetadataListItem_GetByIdOutputDTO } from 'src/app/core/dtos/output/metadata/MetadataListItem_GetByIdOutputDTO';
import { map } from 'rxjs/internal/operators/map';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class MetadataListItemsWebDataService implements MetadataListItemsDataService {

  baseUrl = environment.api.baseUrl;

  constructor(private httpClient: HttpClient) { }

  getMetadataListItems(id: number): Observable<MetadataListItem[]> {
    const requestUri = this.baseUrl + `/v1/metadatalists/${id}/listitems`;

    return this.httpClient.get<MetadataListItem_GetByIdOutputDTO[]>(requestUri)
      .pipe(
        map(response => {
          automapper
            .createMap(MetadataListItem_GetByIdOutputDTO, MetadataListItem)
            .forMember('id', function (opts) { opts.mapFrom('metadataListItemId'); })
            .forMember('listId', function (opts) { opts.mapFrom('metadataListId'); })
            .forMember('value', function (opts) { opts.mapFrom('itemValue'); })
            .forMember('description', function (opts) { opts.mapFrom('itemDescription'); })
            .forMember('sort', function (opts) { opts.mapFrom('itemSort'); })
            .forMember('parentId', function (opts) { opts.mapFrom('parentItemId'); })
            .forMember('parentName', function (opts) { opts.mapFrom('parentItemDescription'); })
            .forMember('status', function (opts) { opts.mapFrom('statusName'); });

          let items: MetadataListItem[] = automapper.map(MetadataListItem_GetByIdOutputDTO, MetadataListItem, response);
          return items;
        })
      );
  }

  getMetadataListItem(id: number): Observable<MetadataListItem> {
    const requestUri = this.baseUrl + `/v1/metadatalistitems/${id}`;

    return this.httpClient.get<MetadataListItem_GetByIdOutputDTO>(requestUri)
      .pipe(
        map(response => {
          automapper
            .createMap(MetadataListItem_GetByIdOutputDTO, MetadataListItem)
            .forMember('id', function (opts) { opts.mapFrom('metadataListItemId'); })
            .forMember('listId', function (opts) { opts.mapFrom('metadataListId'); })
            .forMember('value', function (opts) { opts.mapFrom('itemValue'); })
            .forMember('description', function (opts) { opts.mapFrom('itemDescription'); })
            .forMember('sort', function (opts) { opts.mapFrom('itemSort'); })
            .forMember('parentId', function (opts) { opts.mapFrom('parentItemId'); })
            .forMember('parentName', function (opts) { opts.mapFrom('parentItemDescription'); })
            .forMember('status', function (opts) { opts.mapFrom('statusName'); });

          let item: MetadataListItem = automapper.map(MetadataListItem_GetByIdOutputDTO, MetadataListItem, response);
          return item;
        })
      );
  }

  updateMetadataListItem(id: number, payload: MetadataListItem) {
    const requestUri = this.baseUrl + `/v1/metadatalistitems/${id}`;

    automapper
      .createMap(MetadataListItem, MetadataListItem_GetByIdOutputDTO)
      .forMember('metadataListItemId', function (opts) { opts.mapFrom('id'); })
      .forMember('metadataListId', function (opts) { opts.mapFrom('listId'); })
      .forMember('itemValue', function (opts) { opts.mapFrom('description '); })
      .forMember('itemDescription', function (opts) { opts.mapFrom('description'); })
      .forMember('itemSort', function (opts) { opts.mapFrom('sort'); })
      .forMember('parentItemId', function (opts) { opts.mapFrom('parentId'); })
      .forMember('parentItemDescription', function (opts) { opts.mapFrom('parentName'); })
      .forMember('statusName', function (opts) { opts.mapFrom('status'); });

    const request: MetadataListItem_GetByIdOutputDTO = automapper.map(MetadataListItem, MetadataListItem_GetByIdOutputDTO, payload);
    return this.httpClient.put<any>(requestUri, request);
  }

  removeMetadataListItem(id: number, metadataListItemId: number) {
    const requestUri = environment.api.baseUrl + `/v1/metadatalistitems/${metadataListItemId}`;
    return this.httpClient.delete(requestUri);
  }
}
