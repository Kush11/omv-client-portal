import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { map } from 'rxjs/internal/operators/map';
import * as automapper from 'automapper-ts';
import { Lookup } from 'src/app/core/models/entity/lookup';
import { LookupDataService } from './lookup.data.service';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Lookup_DTO } from 'src/app/core/dtos/output/lookup/Lookup';

@Injectable({
  providedIn: 'root'
})
export class LookupWebDataService implements LookupDataService {

  baseUrl = environment.api.baseUrl;

  constructor(private httpClient: HttpClient) { }

  getCustomWorkSetFrequencyTypes(): Observable<Lookup[]> {
    const requestUri = `${this.baseUrl}/v1/lookups?lookupType=CUSTOM_WORKSET_FREQUENCY_TYPE&isCore=false`;
    
    return this.httpClient.get<Lookup_DTO[]>(requestUri)
      .pipe(
        map(response => {
          let types = this.mapResponse(response);
          types.forEach(type => {
            type.value = Number(type.value);
          });
          return types;
        })
      );
  }

  getWorkSetFrequencyTypes(): Observable<Lookup[]> {
    const requestUri = `${this.baseUrl}/v1/lookups?LookupType=WORKSET_FREQUENCY_TYPE&isCore=false;`;
    return this.httpClient.get<Lookup_DTO[]>(requestUri)
      .pipe(
        map(response => {
          let types = this.mapResponse(response);
          types.forEach(type => {
            type.value = Number(type.value);
          });
          return types;
        })
      );
  }

  getMetadataModules(): Observable<Lookup[]> {
    const requestUri = `${this.baseUrl}/v1/lookups?lookupType=CUSTOM_WORKSET_FREQUENCY_TYPE&isCore=false`;
    const mockUrl = './assets/mock/metadata-modules-lookup.json';

    return this.httpClient.get<Lookup_DTO[]>(mockUrl)
      .pipe(
        map(response => {
          let types = this.mapResponse(response);
          types.forEach(type => {
            type.value = Number(type.value);
          });
          return types;
        })
      );
  }

  getLiveStreamAssets(): Observable<Lookup[]> {
    const requestUri = `${this.baseUrl}/v1/lookups?lookupType=LIVE_STREAM_ASSETS&isCore=false`;
    return this.httpClient.get<Lookup_DTO[]>(requestUri)
      .pipe(
        map(response => {
          let types = this.mapResponse(response);
          types.forEach(type => {
            type.value = Number(type.value);
          });
          return types;
        })
      );
  }

  getLiveStreamGroups(): Observable<Lookup[]> {
    const requestUri = `${this.baseUrl}/v1/lookups?lookupType=LIVE_STREAM_GROUPS&isCore=false`;
    return this.httpClient.get<Lookup_DTO[]>(requestUri)
      .pipe(
        map(response => {
          let types = this.mapResponse(response);
          types.forEach(type => {
            type.value = Number(type.value);
          });
          return types;
        })
      );
  }

  //#region Email Template
  getEmailPlaceholderLookup(): Observable<Lookup[]>  {
    const mockUrl = './assets/mock/email-placeholder-lookup.json';
    return this.httpClient.get<Lookup_DTO[]>(mockUrl)
      .pipe(
        map(response => {
          const types = this.mapResponse(response);
          types.forEach(type => {
            type.value = Number(type.value);
          });
          return types;
        })
      );
  }
  //#endregion

  private mapResponse(apiResponse: Lookup_DTO[]): Lookup[] {
    automapper
      .createMap(Lookup_DTO, Lookup)
      .forMember('type', function (opts) { opts.mapFrom('lookupType'); })
      .forMember('name', function (opts) { opts.mapFrom('lookupValue'); })
      .forMember('value', function (opts) { opts.mapFrom('lookupValue'); })
      .forMember('description', function (opts) { opts.mapFrom('lookupDescription'); })
      .forMember('sort', function (opts) { opts.mapFrom('lookupSort'); })
      .forMember('status', function (opts) { opts.mapFrom('status'); });

    const lookups: Lookup[] = automapper.map(Lookup_DTO, Lookup, apiResponse);
    // lookups.forEach(lookup => {
    //   lookup.value = `[${lookup.value}]`;
    // });
    console.log('lookup', lookups)
    return lookups;
  }
}