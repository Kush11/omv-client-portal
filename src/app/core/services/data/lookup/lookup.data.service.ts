import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Lookup } from 'src/app/core/models/entity/lookup';

@Injectable({
  providedIn: 'root'
})
export abstract class LookupDataService {

  abstract getCustomWorkSetFrequencyTypes(): Observable<Lookup[]>;
  abstract getWorkSetFrequencyTypes(): Observable<Lookup[]>;
  abstract getMetadataModules(): Observable<Lookup[]>;

  //#region Streaming Archive

  abstract getLiveStreamAssets(): Observable<Lookup[]>;
  abstract getLiveStreamGroups(): Observable<Lookup[]>;

  //#region Email Template
  abstract getEmailPlaceholderLookup(): Observable<Lookup[]>;

  //#endregion
}