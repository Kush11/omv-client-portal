import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Lookup } from 'src/app/core/models/entity/lookup';
import { LookupDataService } from './lookup.data.service';

@Injectable({
  providedIn: 'root'
})
export class LookupMockDataService implements LookupDataService {
  getLiveStreamAssets(): Observable<Lookup[]> {
    throw new Error("Method not implemented.");
  }
  getLiveStreamGroups(): Observable<Lookup[]> {
    throw new Error("Method not implemented.");
  }
  getMetadataModules(): Observable<Lookup[]> {
    throw new Error("Method not implemented.");
  }
  getCustomWorkSetFrequencyTypes(): Observable<Lookup[]> {
    throw new Error("Method not implemented.");
  }
  getWorkSetFrequencyTypes(): Observable<Lookup[]> {
    throw new Error("Method not implemented.");
  }

  //#region Email Template
  getEmailPlaceholderLookup(): Observable<Lookup[]> {
    throw new Error("Method not implemented.");
  }
  //#endregion

  constructor() { }  
}