import { WorkSetsDataService } from './work-sets.data.service';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpEvent, HttpRequest, HttpParams } from '@angular/common/http';
import { DateService } from '../../business/dates/date.service';
import { Observable } from 'rxjs';
import { WorkSet, WorkSetSchedule, WorkSetModel, WorkSetData } from 'src/app/core/models/entity/work-set';
import { WorkSet_GetAllOutputDTO } from 'src/app/core/dtos/output/work-sets/WorkSet_GetAllOutputDTO';
import { map } from 'rxjs/operators';
import { WorkSet_InsertInputDTO } from 'src/app/core/dtos/input/work-sets/WorkSet_InsertInputDTO';
import { WorkSet_GetByGroupIdOutputDTO } from 'src/app/core/dtos/output/work-sets/WorkSet_GetByGroupIdOutputDTO';
import { WorkAsset } from 'src/app/core/models/entity/work-set-asset';
import { WorkSetAsset_InputDTO } from 'src/app/core/dtos/input/work-sets/WorkSetAsset_InputDTO';
import { WorkSetSchedule_InputDTO } from 'src/app/core/dtos/input/work-sets/WorkSetSchedule_InputDTO';
import { WorkTemplateProcess } from 'src/app/core/models/entity/work-template';
import { WorkTemplateProcess_GetAllOutputDTO } from 'src/app/core/dtos/output/work-templates/WorkTemplateModel';
import { WorkItem_InsertInputDTO } from 'src/app/core/dtos/input/work-sets/WorkItem_InsertInputDTO';
import { WorkItem } from 'src/app/core/models/entity/work-item';
import { WorkItem_GetAllOutputDTO } from 'src/app/core/dtos/output/work-sets/WorkItem_GetAllOutputDTO';
import { WorkSetDTO, WorkSetHistory_GetAllOutputDTO, WorkSetHistory_GetAllOutputDTOData } from 'src/app/core/dtos/output/work-sets/WorkSetDTO';
import { WorkSetGroup_UpdateInputDTO } from 'src/app/core/dtos/input/work-sets/WorkSetGroup_UpdateInputDTO';
import { MetadataField } from 'src/app/core/models/entity/metadata-field';
import { MetadataField_GetAllOutputDTO } from 'src/app/core/dtos/output/metadata/MetadataField_GetAllOutputDTO';
import { MetadataField_GetListItemByIdOutputDTO } from 'src/app/core/dtos/output/metadata/MetadataField_GetListItemByIdOutputDTO';
import { ListItem } from 'src/app/core/models/entity/list-item';
import { WorkSetCycle, WorkSetCycles } from 'src/app/core/models/entity/work-set-cycle';
import { WorkSetCycle_GetAllOutputDTO, WorkSetCycle_GetAllOutputDTOData } from 'src/app/core/dtos/output/work-sets/WorkSetCycle_GetAllOutputDTO';
import { WorkSetCycle_UpdateInputDTO } from 'src/app/core/dtos/input/work-sets/WorkSetCycle_UpdateInputDTO';
import { WorkSetCycle_GetByIdOutputDTO } from 'src/app/core/dtos/output/work-sets/WorkSetCycle_GetByIdOutputDTO';
import { WorkSetHistories, WorkSetHistory } from 'src/app/core/models/entity/work-set-history';

@Injectable({
  providedIn: 'root'
})
export class WorkSetsWebDataService implements WorkSetsDataService {

  baseUrl = environment.api.baseUrl;

  constructor(private httpClient: HttpClient, private dateService: DateService) { }

  // #region Work Set	

  getWorkSets(): Observable<WorkSet[]> {
    const requestUri = `${this.baseUrl}/v1/worksets`;

    return this.httpClient.get<WorkSet_GetAllOutputDTO[]>(requestUri)
      .pipe(
        map(response => {
          automapper
            .createMap(WorkSet_GetAllOutputDTO, WorkSet)
            .forMember('id', function (opts) { opts.mapFrom('workSetId'); })
            .forMember('name', function (opts) { opts.mapFrom('name'); })
            .forMember('modifiedOn', function (opts) { opts.mapFrom('modifiedOn'); })
            .forMember('modifiedBy', function (opts) { opts.mapFrom('modifiedBy'); });

          const sets: WorkSet[] = automapper.map(WorkSet_GetAllOutputDTO, WorkSet, response);
          return sets;
        })
      );
  }

  getWorkSet(id: number): Observable<WorkSetModel> {
    const requestUri = `${this.baseUrl}/v1/worksets/${id}`;

    return this.httpClient.get<WorkSetDTO>(requestUri)
      .pipe(
        map(response => {
          return this.mapGroup(response);
        })
      );
  }

  createSet(payload: WorkSet) {
    const requestUri = `${this.baseUrl}/v1/worksets`;

    automapper
      .createMap(WorkSet, WorkSet_InsertInputDTO)
      .forMember('id', function (opts) { opts.mapFrom('workSetId'); })
      .forMember('workTemplateId', function (opts) { opts.mapFrom('templateId'); })
      .forMember('name', function (opts) { opts.mapFrom('name'); })
      .forMember('asset', function (opts) { opts.mapFrom('asset'); })
      .forMember('directoryId', function (opts) { opts.mapFrom('directoryId'); })
      .forMember('folderPath', function (opts) { opts.mapFrom('directoryPath'); });

    const request = automapper.map(WorkSet, WorkSet_InsertInputDTO, payload);
    return this.httpClient.post(requestUri, request);
  }

  updateSet(id: number, payload: WorkSet) {
    const requestUri = `${this.baseUrl}/v1/worksets/${id}`;
    automapper
      .createMap(WorkSet, WorkSetGroup_UpdateInputDTO)
      .forMember('workSetId', function (opts) { opts.mapFrom('id'); })
      .forMember('asset', function (opts) { opts.mapFrom('asset'); })
      .forMember('templateId', function (opts) { opts.mapFrom('templateId'); })
      .forMember('directoryId', function (opts) { opts.mapFrom('directoryId'); })
      .forMember('folderPath', function (opts) { opts.mapFrom('directoryPath'); });

    const request = automapper.map(WorkSet, WorkSetGroup_UpdateInputDTO, payload);
    return this.httpClient.put(requestUri, request);
  }

  private mapGroup(response: WorkSetDTO): WorkSetModel {
    automapper
      .createMap(WorkSetDTO, WorkSetModel)
      .forMember('data', function (opts) { opts.mapFrom('workSet_GetByGroupIdOutputDTOs'); })
      .forMember('history', function (opts) { opts.mapFrom('workSetHistories'); })

    const model: WorkSetModel = automapper.map(WorkSetDTO, WorkSetModel, response);

    // work sets
    automapper
      .createMap(WorkSet_GetByGroupIdOutputDTO, WorkSetData)
      .forMember('id', function (opts) { opts.mapFrom('workSetId'); })
      .forMember('name', function (opts) { opts.mapFrom('name'); })
      .forMember('asset', function (opts) { opts.mapFrom('asset'); })
      .forMember('status', function (opts) { opts.mapFrom('statusName'); })
      .forMember('templateId', function (opts) { opts.mapFrom('workTemplateId'); })
      .forMember('templateGroupId', function (opts) { opts.mapFrom('workTemplateGroupId'); })
      .forMember('metadataListId', function (opts) { opts.mapFrom('metadataListId'); })
      .forMember('version', function (opts) { opts.mapFrom('version'); })
      .forMember('modifiedOn', function (opts) { opts.mapFrom('modifiedOn'); })
      .forMember('modifiedBy', function (opts) { opts.mapFrom('modifiedBy'); })
      .forMember('workItems', function (opts) { opts.mapFrom('workItems'); })
      .forMember('cycleName', function (opts) { opts.mapFrom('cycleName'); })
      .forMember('frequencyType', function (opts) { opts.mapFrom('frequencyType'); })
      .forMember('customDuration', function (opts) { opts.mapFrom('customDuration'); })
      .forMember('customDurationName', function (opts) { opts.mapFrom('customDurationName'); })
      .forMember('customFrequency', function (opts) { opts.mapFrom('customFrequency'); })
      .forMember('repeatFrequency', function (opts) { opts.mapFrom('repeatFrequency'); })
      .forMember('repeatDuration', function (opts) { opts.mapFrom('repeatDuration'); })
      .forMember('frequencyStartDate', function (opts) { opts.mapFrom('frequencyStartDate'); })
      .forMember('frequencyEndDate', function (opts) { opts.mapFrom('frequencyEndDate'); })
      .forMember('completedDate', function (opts) { opts.mapFrom('completedDate'); })
      .forMember('directoryId', function (opts) { opts.mapFrom('directoryId'); })
      .forMember('directoryPath', function (opts) { opts.mapFrom('folderPath'); });

    let data: WorkSetData = automapper.map(WorkSet_GetByGroupIdOutputDTO, WorkSetData, model.data);
    const schedule: WorkSetSchedule = {
      cycleName: data.cycleName,
      startDate: new Date(data.frequencyStartDate),
      endDate: new Date(data.frequencyEndDate),
      completionDate: new Date(data.completedDate),
      frequencyType: data.frequencyType,
      customDuration: data.customDuration,
      customDurationName: data.customDurationName,
      customFrequency: data.customFrequency,
      repeatDuration: data.repeatDuration,
      repeatFrequency: data.repeatFrequency
    };
    const workSet: WorkSet = {
      asset: data.asset,
      schedule: schedule,
      name: data.name,
      templateId: data.templateId,
      id: data.id,
      template: data.template,
      templateGroupId: data.templateGroupId,
      metadataListId: data.metadataListId,
      published: data.published,
      workItems: data.workItems,
      status: data.status,
      directoryId: data.directoryId,
      directoryPath: data.directoryPath
    };
    model.data = workSet;

    automapper
      .createMap(WorkItem_GetAllOutputDTO, WorkItem)
      .forMember('id', function (opts) { opts.mapFrom('workItemId'); })
      .forMember('processId', function (opts) { opts.mapFrom('workTemplateProcessId'); })
      .forMember('sort', function (opts) { opts.mapFrom('sortOrder'); })
      .forMember('processName', function (opts) { opts.mapFrom('processDisplayText'); })
      .forMember('workSetId', function (opts) { opts.mapFrom('workSetId'); })
      .forMember('data', function (opts) { opts.mapFrom('data'); })
      .forMember('createdBy', function (opts) { opts.mapFrom('createdBy'); });

    let workItems: WorkItem[] = automapper.map(WorkItem_GetAllOutputDTO, WorkItem, model.data.workItems);
    model.data.workItems = workItems || [];
    return model;
  }

  //#endregion

  //#region Work Sets Asset

  createSetAsset(assetId: number, payload: WorkAsset) {
    const requestUri = `${this.baseUrl}/v1/worksets/${assetId}/assets`;

    automapper
      .createMap(WorkAsset, WorkSetAsset_InputDTO)
      .forMember('workSetId', function (opts) { opts.mapFrom('workSetId'); })
      .forMember('asset', function (opts) { opts.mapFrom('asset'); });

    const request = automapper.map(WorkAsset, WorkSetAsset_InputDTO, payload);
    return this.httpClient.post(requestUri, request);
  }

  updateSetAsset(id: number, payload: WorkAsset) {
    const requestUri = `${this.baseUrl}/v1/worksets/${id}/assets`;

    automapper
      .createMap(WorkAsset, WorkSetAsset_InputDTO)
      .forMember('workSetId', function (opts) { opts.mapFrom('workSetId'); })
      .forMember('asset', function (opts) { opts.mapFrom('asset'); });

    const request = automapper.map(WorkAsset, WorkSetAsset_InputDTO, payload);
    return this.httpClient.put(requestUri, request);
  }

  getSetAssets(id: number): Observable<MetadataField[]> {
    const requestUri = `${this.baseUrl}/v1/worktemplates/1/metadatalist`;
    return this.httpClient.get<MetadataField_GetAllOutputDTO[]>(requestUri)
      .pipe(
        map(response => {
          automapper
            .createMap(MetadataField_GetAllOutputDTO, MetadataField)
            .forMember('id', function (opts) { opts.mapFrom('metadataFieldId'); })
            .forMember('parentId', function (opts) { opts.mapFrom('parentFieldId'); })
            .forMember('settingId', function (opts) { opts.mapFrom('MetadataSettingId'); })
            .forMember('name', function (opts) { opts.mapFrom('fieldName'); })
            .forMember('label', function (opts) { opts.mapFrom('label'); })
            .forMember('type', function (opts) { opts.mapFrom('type'); })
            .forMember('typeId', function (opts) { opts.mapFrom('fieldTypeId'); })
            .forMember('entityName', function (opts) { opts.mapFrom('entityName'); })
            .forMember('listId', function (opts) { opts.mapFrom('metadataListId'); })
            .forMember('listName', function (opts) { opts.mapFrom('metadataListName'); })
            .forMember('isRequired', function (opts) { opts.mapFrom('isRequired'); })
            .forMember('status', function (opts) { opts.mapFrom('status'); })
            .forMember('options', function (opts) { opts.mapFrom('options'); });

          let metadataFields: MetadataField[] = automapper.map(MetadataField_GetAllOutputDTO, MetadataField, response);

          metadataFields.forEach(field => {
            automapper
              .createMap(MetadataField_GetListItemByIdOutputDTO, ListItem)
              .forMember('id', function (opts) { opts.mapFrom('metadataListItemId'); })
              .forMember('parentId', function (opts) { opts.mapFrom('parentItemId'); })
              .forMember('value', function (opts) { opts.mapFrom('itemValue'); })
              .forMember('description', function (opts) { opts.mapFrom('itemDescription'); })
              .forMember('sort', function (opts) { opts.mapFrom('itemSort'); });

            let options: ListItem[] = automapper.map(MetadataField_GetListItemByIdOutputDTO, ListItem, field.options);
            field.options = options;
          });
          return metadataFields.sort((a, b) => (a.name > b.name) ? 1 : -1);
        })
      );
  }

  //#endregion

  //#region Work Sets Schedule

  createSetSchedule(id: number, payload: WorkSetSchedule) {
    const requestUri = `${this.baseUrl}/v1/worksets/${id}/schedule`;
    automapper
      .createMap(WorkSetSchedule, WorkSetSchedule_InputDTO)
      .forMember('cycleName', function (opts) { opts.mapFrom('cycleName'); })
      .forMember('frequencyType', function (opts) { opts.mapFrom('frequencyType'); })
      .forMember('frequencyStartDate', function (opts) { opts.mapFrom('startDate'); })
      .forMember('frequencyEndDate', function (opts) { opts.mapFrom('endDate'); })
      .forMember('frequencyCompletionDate', function (opts) { opts.mapFrom('completionDate'); })
      .forMember('customFrequency', function (opts) { opts.mapFrom('customFrequency'); })
      .forMember('customDuration', function (opts) { opts.mapFrom('customDuration'); })
      .forMember('repeatFrequency', function (opts) { opts.mapFrom('repeatFrequency'); })
      .forMember('repeatDuration', function (opts) { opts.mapFrom('repeatDuration'); });

    const request = automapper.map(WorkSetSchedule, WorkSetSchedule_InputDTO, payload);
    return this.httpClient.post(requestUri, request);
  }

  updateSetSchedule(id: number, payload: WorkSetSchedule) {
    const requestUri = `${this.baseUrl}/v1/worksets/${id}/schedule`;

    automapper
      .createMap(WorkSetSchedule, WorkSetSchedule_InputDTO)
      .forMember('frequencyType', function (opts) { opts.mapFrom('frequencyType'); })
      .forMember('customFrequency', function (opts) { opts.mapFrom('customFrequency'); })
      .forMember('customDuration', function (opts) { opts.mapFrom('customDuration'); })
      .forMember('frequencyStartDate', function (opts) { opts.mapFrom('startDate'); })
      .forMember('frequencyEndDate', function (opts) { opts.mapFrom('endDate'); })
      .forMember('repeatFrequency', function (opts) { opts.mapFrom('repeatFrequency'); })
      .forMember('repeatDuration', function (opts) { opts.mapFrom('repeatDuration'); })
      .forMember('cycleName', function (opts) { opts.mapFrom('cycleName'); })
      .forMember('frequencyCompletionDate', function (opts) { opts.mapFrom('completedDate'); })

    const request = automapper.map(WorkSetSchedule, WorkSetSchedule_InputDTO, payload);
    return this.httpClient.put(requestUri, request);
  }

  //#endregion

  //#region Work Set Cycles

  getCycles(id: number, pageNumber?: number, pageSize?: number): Observable<WorkSetCycles> {
    const requestUri = `${this.baseUrl}/v1/worksets/${id}/worksetcycles`;

    const options = { params: new HttpParams() };
    if (pageNumber) options.params = options.params.set('pageNumber', pageNumber.toString());
    if (pageSize) options.params = options.params.set('limit', pageSize.toString());

    return this.httpClient.get<WorkSetCycle_GetAllOutputDTO[]>(requestUri, options)
      .pipe(
        map(response => {
          automapper
            .createMap(WorkSetCycle_GetAllOutputDTO, WorkSetCycles)
            .forMember('pagination', function (opts) { opts.mapFrom('Pagination'); })
            .forMember('data', function (opts) { opts.mapFrom('Data'); });

          let cycles: WorkSetCycles = automapper.map(WorkSetCycle_GetAllOutputDTO, WorkSetCycles, response);

          automapper
            .createMap(WorkSetCycle_GetAllOutputDTOData, WorkSetCycle)
            .forMember('id', function (opts) { opts.mapFrom('cycleId'); })
            .forMember('workSetId', function (opts) { opts.mapFrom('workSetId'); })
            .forMember('startDate', function (opts) { opts.mapFrom('startDate'); })
            .forMember('endDate', function (opts) { opts.mapFrom('endDate'); })
            .forMember('status', function (opts) { opts.mapFrom('status'); })
            .forMember('statusName', function (opts) { opts.mapFrom('statusName'); })
            .forMember('isPublished', function (opts) { opts.mapFrom('isPublished'); });

          const sets: WorkSetCycle[] = automapper.map(WorkSetCycle_GetAllOutputDTOData, WorkSetCycle, cycles.data);
          cycles.data = sets;
          return cycles;
        })
      );
  }

  getCycle(id: number, cycleId: number): Observable<WorkSetCycle> {
    const requestUri = `${this.baseUrl}/v1/worksets/${id}/worksetcycles/${cycleId}`;

    return this.httpClient.get<WorkSetCycle_UpdateInputDTO>(requestUri)
      .pipe(
        map(response => {

          automapper
            .createMap(WorkSetCycle_GetByIdOutputDTO, WorkSetCycle)
            .forMember('id', function (opts) { opts.mapFrom('cycleId'); })
            .forMember('workSetId', function (opts) { opts.mapFrom('workSetId'); })
            .forMember('startDate', function (opts) { opts.mapFrom('startDate'); })
            .forMember('endDate', function (opts) { opts.mapFrom('endDate'); })
            .forMember('status', function (opts) { opts.mapFrom('status'); })
            .forMember('statusName', function (opts) { opts.mapFrom('statusName'); })
            .forMember('isPublished', function (opts) { opts.mapFrom('isPublished'); });

          const set: WorkSetCycle = automapper.map(WorkSetCycle_GetByIdOutputDTO, WorkSetCycle, response);
          return set;
        })
      );
  }

  updateCycle(id: number, cycle: WorkSetCycle) {
    if (!cycle) return;
    const requestUri = `${this.baseUrl}/v1/worksets/${id}/worksetcycles/${cycle.id}`;

    automapper
      .createMap(WorkSetCycle, WorkSetSchedule_InputDTO)
      .forMember('cycleId', function (opts) { opts.mapFrom('id'); })
      .forMember('workSetId', function (opts) { opts.mapFrom('workSetId'); })
      .forMember('startDate', function (opts) { opts.mapFrom('startDate'); })
      .forMember('endDate', function (opts) { opts.mapFrom('endDate'); })
      .forMember('status', function (opts) { opts.mapFrom('status'); })
      .forMember('isPublished', function (opts) { opts.mapFrom('isPublished'); });

    const request = automapper.map(WorkSetCycle, WorkSetSchedule_InputDTO, cycle);
    return this.httpClient.put(requestUri, request);
  }

  deleteCycle(id: number, cycleId: number) {
    const requestUri = `${this.baseUrl}/v1/worksets/${id}/worksetcycles/${cycleId}`;
    return this.httpClient.delete(requestUri);
  }

  openCycle(id: number, cycleId: number) {
    const requestUri = `${this.baseUrl}/v1/worksets/${id}/worksetcycles/${cycleId}/open`;
    return this.httpClient.put(requestUri, null);
  }

  publishCycle(id: number, cycleId: number) {
    const requestUri = `${this.baseUrl}/v1/worksets/${id}/worksetcycles/${cycleId}/publish`;
    return this.httpClient.put(requestUri, null);
  }

  //#endregion

  getProcess(templateId: number): Observable<WorkTemplateProcess[]> {
    const requestUri = `${this.baseUrl}/v1/worktemplates/${templateId}/processes`;

    return this.httpClient.get<WorkTemplateProcess_GetAllOutputDTO>(requestUri)
      .pipe(
        map(response => {
          automapper
            .createMap(WorkTemplateProcess_GetAllOutputDTO, WorkTemplateProcess)
            .forMember('id', function (opts) { opts.mapFrom('workTemplateProcessId'); })
            .forMember('name', function (opts) { opts.mapFrom('Name'); })
            .forMember('code', function (opts) { opts.mapFrom('Code'); })
          let processs: WorkTemplateProcess[] = automapper.map(
            WorkTemplateProcess_GetAllOutputDTO, WorkTemplateProcess, response);

          return processs;
        })
      );
  }

  //#region Work Sets Items

  createSetItem(id: number, payload: WorkItem) {
    const requestUri = `${this.baseUrl}/v1/worksets/${id}/items`;
    automapper
      .createMap(WorkItem, WorkItem_InsertInputDTO)
      .forMember('workItemId', function () { return null; })
      .forMember('workSetId', function (opts) { opts.mapFrom('workSetId'); })
      .forMember('workTemplateProcessId', function (opts) { opts.mapFrom('processId'); })
      .forMember('data', function (opts) { opts.mapFrom('data'); })
      .forMember('sortOrder', function (opts) { opts.mapFrom('sort'); });

    const request = automapper.map(WorkItem, WorkItem_InsertInputDTO, payload);
    return this.httpClient.post(requestUri, request);
  }

  updateSetItem(id: number, payload: WorkItem) {
    const requestUri = `${this.baseUrl}/v1/worksets/${id}/items/${payload.id}`;
    automapper
      .createMap(WorkItem, WorkItem_InsertInputDTO)
      .forMember('workItemId', function (opts) { opts.mapFrom('id'); })
      .forMember('workSetId', function (opts) { opts.mapFrom('workSetId'); })
      .forMember('workTemplateProcessId', function (opts) { opts.mapFrom('processId'); })
      .forMember('data', function (opts) { opts.mapFrom('data'); })
      .forMember('sortOrder', function (opts) { opts.mapFrom('sort'); });

    const request = automapper.map(WorkItem, WorkItem_InsertInputDTO, payload);
    return this.httpClient.put(requestUri, request);
  }

  downloadWorkItem(id: number): Observable<HttpEvent<Blob>> {
    const request = new HttpRequest('GET', `${this.baseUrl}/v1/worksets/${id}/downloadWorkItem`, null, {
      reportProgress: true,
      responseType: 'blob'
    });
    return this.httpClient.request(request);
  }

  importWorkSetItem(id: number, file: string) {
    const requestUri = `${this.baseUrl}/v1/worksets/${id}/importWorkItem`;
    return this.httpClient.post<WorkSetDTO>(requestUri, { field: file })
      .pipe(
        map(response => {
          return response;
        })
      );
  }

  deleteSetItem(workSetId: number, itemId: number) {
    const requestUri = `${this.baseUrl}/v1/worksets/${workSetId}/items/${itemId}`;
    return this.httpClient.delete(requestUri);
  };

  //#endregion

  getHistory(id: number, pageNumber?: number, pageSize?: number): Observable<WorkSetHistories> {
    const requestUri = `${this.baseUrl}/v1/worksets/${id}/history`;

    const options = { params: new HttpParams() };
    if (pageNumber) options.params = options.params.set('pageNumber', pageNumber.toString());
    if (pageSize) options.params = options.params.set('limit', pageSize.toString());

    return this.httpClient.get<WorkSetHistory_GetAllOutputDTO>(requestUri, options)
      .pipe(
        map(response => {
          automapper
            .createMap(WorkSetHistory_GetAllOutputDTO, WorkSetHistories)
            .forMember('data', function (opts) { opts.mapFrom('data'); })
            .forMember('pagination', function (opts) { opts.mapFrom('pagination'); });

          const model: WorkSetHistories = automapper.map(WorkSetHistory_GetAllOutputDTO, WorkSetHistories, response);

          automapper
            .createMap(WorkSetHistory_GetAllOutputDTOData, WorkSetHistory)
            .forMember('entityType', function (opts) { opts.mapFrom('entityType'); })
            .forMember('entityId', function (opts) { opts.mapFrom('entityId'); })
            .forMember('column', function (opts) { opts.mapFrom('columnName'); })
            .forMember('oldValue', function (opts) { opts.mapFrom('oldValue'); })
            .forMember('newValue', function (opts) { opts.mapFrom('newValue'); })
            .forMember('event', function (opts) { opts.mapFrom('eventName'); })
            .forMember('createdBy', function (opts) { opts.mapFrom('createdBy'); })
            .forMember('createdOn', function (opts) { opts.mapFrom('createdOn'); });

          const histories: WorkSetHistory[] = automapper.map(WorkSetHistory_GetAllOutputDTOData, WorkSetHistory, model.data);
          model.data = histories || [];
          return model;
        })
      );
  }
}