import { WorkSetsDataService } from './work-sets.data.service';
import { Injectable } from '@angular/core';
import { WorkSet, WorkSetSchedule, WorkSetModel, FrequencyType } from 'src/app/core/models/entity/work-set';
import { Observable } from 'rxjs';
import { HttpClient, HttpEvent } from '@angular/common/http';
import { WorkTemplateProcess } from 'src/app/core/models/entity/work-template';
import { WorkItem } from 'src/app/core/models/entity/work-item';
import { WorkAsset } from 'src/app/core/models/entity/work-set-asset';
import { MetadataField } from 'src/app/core/models/entity/metadata-field';
import { WorkSetCycle, WorkSetCycles } from 'src/app/core/models/entity/work-set-cycle';
import { WorkSetHistories } from 'src/app/core/models/entity/work-set-history';

@Injectable({
    providedIn: 'root'
})
export class WorkSetsMockDataService implements WorkSetsDataService {
    deleteCycle(id: number, cycleId: number) {
        throw new Error("Method not implemented.");
    }
    getHistory(id: number, pageNumber?: number, pageSize?: number): Observable<WorkSetHistories> {
        throw new Error("Method not implemented.");
    }
    openCycle(id: number, cycleId: number) {
        throw new Error("Method not implemented.");
    }
    publishCycle(id: number, cycleId: number) {
        throw new Error("Method not implemented.");
    }

    updateCycle(id: number, cycle: WorkSetCycle) {
        throw new Error("Method not implemented.");
    }
    getCycle(id: number, cycleId: number): Observable<WorkSetCycle> {
        throw new Error("Method not implemented.");
    }
    getCycles(id: number, pageNumber?: number, pageSize?: number): Observable<WorkSetCycles> {
        throw new Error("Method not implemented.");
    }
    getWorkSet(id: number): Observable<WorkSetModel> {
        throw new Error("Method not implemented.");
    }

    constructor(private httpClient: HttpClient) { }

    //#region Work Set Group

    getWorkSets(): Observable<WorkSet[]> {
        var mockUrl = `./assets/mock/admin-work-sets.json`;
        return this.httpClient.get<WorkSet[]>(mockUrl);
    }

    createSet(payload: WorkSet) {
        throw new Error('Method not implemented.');
    }

    updateSet(id: number, payload: WorkSet) {
        throw new Error('Method not implemented.');
    }

    //#region Work Sets Asset
    createSetAsset(assetId: number, payload: WorkAsset) {
        throw new Error('Method not implemented.');
    };
    updateSetAsset(id: number, payload: WorkAsset) {
        throw new Error('Method not implemented.');
    }
    getSetAssets(id: number): Observable<MetadataField[]> {
        throw new Error('Method not implemented.');
    }
    //#endregion

    //#region Work Sets process
    getProcess(templateId: number): Observable<WorkTemplateProcess[]> {
        throw new Error('Method not implemented.');
    };

    //#endregion



    //#region Work Sets Schedule
    createSetSchedule(id: number, payload: WorkSetSchedule) {
        throw new Error('Method not implemented.');
    }
    updateSetSchedule(id: number, payload: WorkSetSchedule) {
        throw new Error('Method not implemented.');
    }

    getfrequency(): Observable<FrequencyType[]> {
        throw new Error('Method not implemented.');
    }

    //#endregion

    createSetItem(id: number, payload: WorkItem) {
        throw new Error('Method not implemented.');
    }

    updateSetItem(id: number, payload: WorkItem) {
        throw new Error('Method not implemented.');
    }
    deleteSetItem(workSetId: number, ItemId: number) {
        throw new Error('Method not implemented.');
    }
    downloadWorkItem(id: number): Observable<HttpEvent<Blob>> {
        throw new Error('Method not implemented.');
    }
    importWorkSetItem(id: number, file: string) {
        throw new Error('Method not implemented.');
    }
}