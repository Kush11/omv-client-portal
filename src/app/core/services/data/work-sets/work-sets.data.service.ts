import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { WorkSet, WorkSetSchedule, WorkSetModel } from 'src/app/core/models/entity/work-set';
import { WorkTemplateProcess } from 'src/app/core/models/entity/work-template';
import { WorkItem } from 'src/app/core/models/entity/work-item';
import { WorkAsset } from 'src/app/core/models/entity/work-set-asset';
import { HttpEvent } from '@angular/common/http';
import { MetadataField } from 'src/app/core/models/entity/metadata-field';
import { WorkSetCycle, WorkSetCycles } from 'src/app/core/models/entity/work-set-cycle';
import { WorkSetHistories } from 'src/app/core/models/entity/work-set-history';


@Injectable({
  providedIn: 'root'
})
export abstract class WorkSetsDataService {

  abstract getWorkSets(): Observable<WorkSet[]>;
  abstract getWorkSet(id: number): Observable<WorkSetModel>;

  //#region Work Set

  abstract createSet(payload: WorkSet);
  abstract updateSet(id: number, payload: WorkSet);

  //#endregion

  //#region Work Sets Asset

  abstract createSetAsset(assetId: number, payload: WorkAsset);
  abstract updateSetAsset(id: number, payload: WorkAsset);
  abstract getSetAssets(id: number): Observable<MetadataField[]>;

  //#endregion

  //#region Work Sets Schedule

  abstract createSetSchedule(id: number, payload: WorkSetSchedule);
  abstract updateSetSchedule(id: number, payload: WorkSetSchedule);

  //#endregion

  //#region Work Set Process

  abstract getProcess(templateId: number): Observable<WorkTemplateProcess[]>;

  //#endregion

  //#region Work Set Cycles

  abstract getCycles(id: number, pageNumber?: number, pageSize?: number): Observable<WorkSetCycles>;
  abstract getCycle(id: number, cycleId: number): Observable<WorkSetCycle>;
  abstract updateCycle(id: number, cycle: WorkSetCycle);
  abstract deleteCycle(id: number, cycleId: number);
  abstract openCycle(id: number, cycleId: number);
  abstract publishCycle(id: number, cycleId: number);

  //#endregion

  //#region Work Set Item

  abstract createSetItem(id: number, payload: WorkItem);
  abstract updateSetItem(id: number, payload: WorkItem);
  abstract deleteSetItem(workSetId: number, ItemId: number);
  abstract downloadWorkItem(id: number): Observable<HttpEvent<Blob>>;
  abstract importWorkSetItem(id: number, file: string);

  //#endregion

  //#region Work Set History

  abstract getHistory(id: number, pageNumber?: number, pageSize?: number): Observable<WorkSetHistories>;

  //#endregion
}

