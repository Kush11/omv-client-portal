import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { HttpClient } from '@angular/common/http';
import { MetadataFieldsDataService } from './metadata-fields.data.service';
import { MetadataField } from 'src/app/core/models/entity/metadata-field';

@Injectable({
  providedIn: 'root'
})
export class MetadataFieldsMockDataService implements MetadataFieldsDataService {
  sortFields(fields: MetadataField[]) {
    throw new Error("Method not implemented.");
  }

  constructor(private httpClient: HttpClient) { }

  getMetaDataFields(id?: number): Observable<MetadataField[]> {
    throw new Error("Method not implemented.");
  }
  createMetadataField(payload: MetadataField): Observable<MetadataField> {
    throw new Error("Method not implemented.");
  }
  updateMetaDataField(id: number, payload: MetadataField) {
    throw new Error("Method not implemented.");
  }
  removeMetadataField(id: number) {
    throw new Error("Method not implemented.");
  }
}
