import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { HttpClient, HttpParams } from '@angular/common/http';
import { MetadataFieldsDataService } from './metadata-fields.data.service';
import { environment } from 'src/environments/environment';
import * as automapper from 'automapper-ts';
import { MetadataField_GetListItemByIdOutputDTO } from 'src/app/core/dtos/output/metadata/MetadataField_GetListItemByIdOutputDTO';
import { ListItem } from 'src/app/core/models/entity/list-item';
import { MetadataField } from 'src/app/core/models/entity/metadata-field';
import { MetadataField_GetAllOutputDTO } from 'src/app/core/dtos/output/metadata/MetadataField_GetAllOutputDTO';
import { FieldsService } from '../../business/fields/fields.service';
import { MetadataField_InsertInputDTO } from 'src/app/core/dtos/input/metadata/MetadataField_InsertInputDTO';
import { MetadataField_UpdateInputDTO } from 'src/app/core/dtos/input/metadata/MetadataField_UpdateInputDTO';
import { map } from 'rxjs/internal/operators/map';
import { SortMetadataField_InputDTO } from 'src/app/core/dtos/input/metadata/SortMetadataField_InputDTO';

@Injectable({
  providedIn: 'root'
})
export class MetadataFieldsWebDataService implements MetadataFieldsDataService {

  constructor(private httpClient: HttpClient, private fieldsService: FieldsService) { }

  getMetaDataFields(module?: number): Observable<MetadataField[]> {
    const requestUri = environment.api.baseUrl + `/v1/metadatafields`;

    const options = { params: new HttpParams() };
    if (module) options.params = options.params.set('metadatafieldtype', module.toString());

    return this.httpClient.get<MetadataField_GetAllOutputDTO[]>(requestUri, options)
      .pipe(
        map(response => {
          automapper
            .createMap(MetadataField_GetAllOutputDTO, MetadataField)
            .forMember('id', function (opts) { opts.mapFrom('metadataFieldId'); })
            .forMember('parentId', function (opts) { opts.mapFrom('parentFieldId'); })
            .forMember('settingId', function (opts) { opts.mapFrom('MetadataSettingId'); })
            .forMember('name', function (opts) { opts.mapFrom('fieldName'); })
            .forMember('label', function (opts) { opts.mapFrom('label'); })
            .forMember('type', function (opts) { opts.mapFrom('type'); })
            .forMember('dataType', function (opts) { opts.mapFrom('dataType'); })
            .forMember('typeId', function (opts) { opts.mapFrom('fieldTypeId'); })
            .forMember('entityName', function (opts) { opts.mapFrom('entityName'); })
            .forMember('listId', function (opts) { opts.mapFrom('metadataListId'); })
            .forMember('listName', function (opts) { opts.mapFrom('metadataListName'); })
            .forMember('isRequired', function (opts) { opts.mapFrom('isRequired'); })
            .forMember('isFilterable', function (opts) { opts.mapFrom('isFilterable'); })
            .forMember('status', function (opts) { opts.mapFrom('status'); })
            .forMember('order', function (opts) { opts.mapFrom('sort'); })
            .forMember('dataType', function (opts) { opts.mapFrom('dataType'); })
            .forMember('searchType', function (opts) { opts.mapFrom('filterType'); })
            .forMember('options', function (opts) { opts.mapFrom('options'); })
            .forMember('isSystem', function (opts) { opts.mapFrom('isSystem'); });

          let metadataFields: MetadataField[] = automapper.map(MetadataField_GetAllOutputDTO, MetadataField, response);

          metadataFields.forEach(field => {
            automapper
              .createMap(MetadataField_GetListItemByIdOutputDTO, ListItem)
              .forMember('id', function (opts) { opts.mapFrom('metadataListItemId'); })
              .forMember('parentId', function (opts) { opts.mapFrom('parentItemId'); })
              .forMember('value', function (opts) { opts.mapFrom('itemValue'); })
              .forMember('description', function (opts) { opts.mapFrom('itemDescription'); })
              .forMember('sort', function (opts) { opts.mapFrom('itemSort'); });

            let options: ListItem[] = automapper.map(MetadataField_GetListItemByIdOutputDTO, ListItem, field.options);
            field.options = options;
          });
          return metadataFields;
        })
      );
  }

  createMetadataField(payload: MetadataField): Observable<MetadataField> {
    const requestUri = environment.api.baseUrl + `/v1/metadatafields`;

    automapper
      .createMap(MetadataField, MetadataField_InsertInputDTO)
      .forMember('metadataFieldId', function (opts) { opts.mapFrom('id'); })
      .forMember('fieldName', function (opts) { opts.mapFrom('name'); })
      .forMember('label', function (opts) { opts.mapFrom('label'); })
      .forMember('dataType', function (opts) { opts.mapFrom('dataType'); })
      .forMember('metadataListId', function (opts) { opts.mapFrom('listId'); })
      .forMember('parentFieldId', function (opts) { opts.mapFrom('parentId'); })
      .forMember('isFilterable', function (opts) { opts.mapFrom('isFilterable'); })
      .forMember('fieldTypeId', function (opts) { opts.mapFrom('typeId'); })
      .forMember('order', function (opts) { opts.mapFrom('order'); })
      .forMember('dataType', function (opts) { opts.mapFrom('dataType'); })
      .forMember('filterType', function (opts) { opts.mapFrom('searchType'); })
      .forMember('module', function (opts) { opts.mapFrom('module'); });

    const request = automapper.map(MetadataField, MetadataField_InsertInputDTO, payload);

    return this.httpClient.post(requestUri, request).pipe(map(
      response => {
        automapper
          .createMap(MetadataField_InsertInputDTO, MetadataField)
          .forMember('id', (opts: AutoMapperJs.IMemberConfigurationOptions) => opts.mapFrom('metadataFieldId'))
          .forMember('name', (opts: AutoMapperJs.IMemberConfigurationOptions) => opts.mapFrom('fieldName'))
          .forMember('label', function (opts) { opts.mapFrom('label'); })
          .forMember('listId', function (opts) { opts.mapFrom('metadataListId'); })
          .forMember('typeId', function (opts) { opts.mapFrom('fieldTypeId'); });

        let field: MetadataField = automapper.map(MetadataField_InsertInputDTO, response);
        return field;
      })
    );
  }

  updateMetaDataField(id: number, payload: MetadataField) {
    const requestUri = environment.api.baseUrl + `/v1/metadatafields/${id}`;

    automapper
      .createMap(MetadataField, MetadataField_UpdateInputDTO)
      .forMember('metadataFieldId', function (opts) { opts.mapFrom('id'); })
      .forMember('fieldName', function (opts) { opts.mapFrom('name'); })
      .forMember('label', function (opts) { opts.mapFrom('label'); })
      .forMember('dataType', function (opts) { opts.mapFrom('dataType'); })
      .forMember('metadataListId', function (opts) { opts.mapFrom('listId'); })
      .forMember('parentFieldId', function (opts) { opts.mapFrom('parentId'); })
      .forMember('fieldTypeId', function (opts) { opts.mapFrom('typeId'); })
      .forMember('order', function (opts) { opts.mapFrom('order'); })
      .forMember('dataType', function (opts) { opts.mapFrom('dataType'); })
      .forMember('filterType', function (opts) { opts.mapFrom('searchType'); })
      .forMember('isFilterable', function (opts) { opts.mapFrom('isFilterable'); });

    const request = automapper.map(MetadataField, MetadataField_UpdateInputDTO, payload);

    return this.httpClient.put(requestUri, request).pipe(map(
      response => {
        automapper
          .createMap(MetadataField_UpdateInputDTO, MetadataField)
          .forMember('id', (opts: AutoMapperJs.IMemberConfigurationOptions) => opts.mapFrom('metadataFieldId'))
          .forMember('name', (opts: AutoMapperJs.IMemberConfigurationOptions) => opts.mapFrom('fieldName'))
          .forMember('label', function (opts) { opts.mapFrom('label'); })
          .forMember('listId', function (opts) { opts.mapFrom('metadataListId') })
          .forMember('typeId', function (opts) { opts.mapFrom('fieldTypeId'); });

        let field = automapper.map(MetadataField_UpdateInputDTO, MetadataField, response);
        return field;
      })
    );
  }

  removeMetadataField(id: number) {
    const requestUri = environment.api.baseUrl + `/v1/metadatafields/${id}`;
    return this.httpClient.delete(requestUri);
  }

  sortFields(fields: MetadataField[]) {
    const requestUri = `${environment.api.baseUrl}/v1/metadatafields/sort`;

    automapper
      .createMap(MetadataField, SortMetadataField_InputDTO)
      .forMember('metadataFieldId', function (opts) { opts.mapFrom('id'); })
      .forMember('order', function (opts) { opts.mapFrom('order'); });

    const request = automapper.map(MetadataField, SortMetadataField_InputDTO, fields);
    return this.httpClient.post(requestUri, request);
  }
}
