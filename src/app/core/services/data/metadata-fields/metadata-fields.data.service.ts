import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { ListItem } from 'src/app/core/models/entity/list-item';
import { MetadataField } from 'src/app/core/models/entity/metadata-field';

@Injectable({
    providedIn: 'root'
})
export abstract class MetadataFieldsDataService {

    constructor() { }

    abstract getMetaDataFields(module?: number): Observable<MetadataField[]>;
    abstract createMetadataField(payload: MetadataField): Observable<MetadataField>;
    abstract updateMetaDataField(id: number, payload: MetadataField);
    abstract removeMetadataField(id: number);
    abstract sortFields(fields: MetadataField[]);
}
