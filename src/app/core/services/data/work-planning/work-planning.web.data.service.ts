import { Injectable } from '@angular/core';
import { WorkPlanningDataService } from './work-planning.data.service';
import { Observable } from 'rxjs/internal/Observable';
import { WorkPlanningParentItem, WorkPlanningWorkItemParent, WorkSetPlanningWorkItemParent, WorkPlanningCycle, WorkPlanningData } from 'src/app/core/models/work-planning';
import { HttpClient, HttpParams, HttpEvent, HttpRequest } from '@angular/common/http';
import { DateService } from '../../business/dates/date.service';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { Work, Works } from 'src/app/core/models/entity/work';
import { MetadataField } from 'src/app/core/models/entity/metadata-field';
import { ListItem } from 'src/app/core/models/entity/list-item';
import { WorkPlanning_GetWorksOutputDTO, WorkPlanning_GetWorksOutputDataDTO } from 'src/app/core/dtos/output/work-planning/WorkPlanning_GetWorksOutputDTO';
import { MetadataListItem_GetById } from 'src/app/core/dtos/output/metadata/MetadataListItem_GetById';
import { WorkPlanning_GetAllOutputDTO, WorkPlanning_GetCycles } from 'src/app/core/dtos/output/work-planning/WorkItemPlanning_GetAllOutputDTO';
import { WorkFile, WorkFilesGroup } from 'src/app/core/models/entity/work-item';
import { Document_InsertInputDTO } from 'src/app/core/dtos/input/documents/Document_InsertInputDTO';
import { DocumentArray_InputDTO } from 'src/app/core/dtos/input/documents/DocumentArray_InputDTO';
import { WorkDocument_GetAllOutputDTO, WorkDocument_GetAllDTO } from 'src/app/core/dtos/output/work-planning/WorkDocumentDTO';
import { WorkItemFindingData } from 'src/app/core/dtos/output/work-planning/WorkItemFindingData';
import { WorkItemFormData_UpdateDTO } from 'src/app/core/dtos/input/work-planning/WorkItemFormData_UpdateDTO';
import { WorkItemFormData } from 'src/app/core/dtos/output/work-planning/WorkItemFormData';
import { MetadataField_GetAllOutputDTO } from 'src/app/core/dtos/output/metadata/MetadataField_GetAllOutputDTO';
import { WorkFinding } from 'src/app/core/models/entity/work-finding';
import { WorkItemFindingData_InsertDTO } from 'src/app/core/dtos/input/work-planning/WorkItemFindingData_InsertDTO';
import { FieldsService } from '../../business/fields/fields.service';

@Injectable({
  providedIn: 'root'
})
export class WorkPlanningWebDataService implements WorkPlanningDataService {

  baseUrl = environment.api.baseUrl;
  constructor(public httpClient: HttpClient, public dateService: DateService, public fieldsService: FieldsService) { }

  getParentItem(): Observable<WorkPlanningParentItem[]> {
    const requestUri = `${this.baseUrl}/v1/workplanning/workparents`;
    return this.httpClient.get<WorkPlanningParentItem[]>(requestUri);
  }

  getWorkItemGroup(name: string): Observable<WorkPlanningWorkItemParent[]> {
    const parentName = `?parent=${name}`;
    const requestUri = `${this.baseUrl}/v1/workplanning/workassets${parentName}`;
    return this.httpClient.get<WorkPlanning_GetAllOutputDTO[]>(requestUri)
      .pipe(
        map(response => {
          automapper
            .createMap(WorkPlanning_GetAllOutputDTO, WorkPlanningWorkItemParent)
            .forMember('asset', function (opts) { opts.mapFrom('Asset'); })
            .forMember('cycles', function (opts) { opts.mapFrom('Cycles'); });


          let items: WorkPlanningWorkItemParent[] =
            automapper.map(WorkPlanning_GetAllOutputDTO, WorkPlanningWorkItemParent, response);
          items.forEach(item => {
            automapper
              .createMap(WorkPlanning_GetCycles, WorkPlanningCycle)
              .forMember('cycle', function (opts) { opts.mapFrom('Cycle'); })
              .forMember('data', function (opts) { opts.mapFrom('Data'); });
            let workItems: WorkPlanningCycle[] =
              automapper.map(WorkPlanning_GetCycles, WorkPlanningCycle, item.cycles);
            item.cycles = workItems;
          });

          // items.forEach(item => {
          //   item.workSetPlanning = item.workSetPlanning.slice(0, 4);
          // })
          return items;
        })
      );
  }

  //#region Works

  getWorks(assetId: number, dateFilter?: string, actionList?: boolean, cycle?: string, pageNumber?: number, pageSize?: number): Observable<Work[]> {
    const requestUri = `${this.baseUrl}/v1/workassets/${assetId}/works`;
    const options = { params: new HttpParams() };
    if (dateFilter) options.params = options.params.set('dateFilter', dateFilter);
    if (actionList) options.params = options.params.set('actionList', actionList.toString());
    if (cycle) options.params = options.params.set('cycle', cycle);
    if (pageNumber) options.params = options.params.set('pageNumber', pageNumber.toString());
    if (pageSize) options.params = options.params.set('limit', pageSize.toString());

    return this.httpClient.get<WorkPlanning_GetWorksOutputDTO>(requestUri, options)
      .pipe(
        map(response => {
          automapper
            .createMap(WorkPlanning_GetWorksOutputDataDTO, Work)
            .forMember('id', function (opts) { opts.mapFrom('workId'); })
            .forMember('assetId', function (opts) { opts.mapFrom('AssetId'); })
            .forMember('firstItemIdentifier', function (opts) { opts.mapFrom('firstItemIdentifier'); })
            .forMember('secondItemIdentifier', function (opts) { opts.mapFrom('secondItemIdentifier'); })
            .forMember('processCode', function (opts) { opts.mapFrom('ProcessCode'); })
            .forMember('findingsText', function (opts) { opts.mapFrom('FindingsText'); })
            .forMember('advanceButton', function (opts) { opts.mapFrom('AdvanceButton'); })
            .forMember('currentStep', function (opts) { opts.mapFrom('CurrentStep'); })
            .forMember('cycleName', function (opts) { opts.mapFrom('CycleName'); })
            .forMember('advanceButton', function (opts) { opts.mapFrom('advanceButton'); })
            .forMember('canAdvance', function (opts) { opts.mapFrom('canAdvance'); })
            .forMember('modifiedOn', function (opts) { opts.mapFrom('ModifiedOn'); })
            .forMember('completeBy', function (opts) { opts.mapFrom('workSetCycleEndDate'); })
            .forMember('workSetCycleEndDate', function (opts) { opts.mapFrom('workSetCycleEndDate'); })
            .forMember('forms', function (opts) { opts.mapFrom('form'); });

          let works: Work[] = automapper.map(WorkPlanning_GetWorksOutputDataDTO, Work, response);
          works.forEach(item => {
            automapper
              .createMap(WorkItemFormData, MetadataField)
              .forMember('label', function (opts) { opts.mapFrom('label'); })
              .forMember('isRequired', function (opts) { opts.mapFrom('isRequired'); })
              .forMember('value', function (opts) { opts.mapFrom('value'); });
            let workItemFormData: MetadataField[] = automapper.map(WorkItemFormData, MetadataField, item.forms);
            item.forms = workItemFormData;
          });
          return works;
        })
      );
  }

  downloadReport(assetId: number, queryType: string, dateFilter?: string, cycle?: string): Observable<HttpEvent<Blob>> {
    let options = new HttpParams();
    options = options.set('queryType', queryType);
    if (dateFilter) options = options.set('dateFilter', dateFilter);
    if (cycle) options = options.set('cycle', cycle);

    const request = new HttpRequest('GET', `${this.baseUrl}/v1/workassets/${assetId}/reports`, null, {
      reportProgress: true,
      responseType: 'blob',
      params: options
    });
    return this.httpClient.request(request);
  }

  //#endregion

  //#region Forms

  getForms(workId: number): Observable<Work[]> {
    const requestUri = `${this.baseUrl}/v1/works/${workId}/form`;
    return this.httpClient.get<WorkPlanning_GetWorksOutputDTO[]>(requestUri)
      .pipe(
        map(response => {
          automapper
            .createMap(WorkPlanning_GetWorksOutputDTO, Work)
            .forMember('id', function (opts) { opts.mapFrom('workId'); })
            .forMember('assetId', function (opts) { opts.mapFrom('assetId'); })
            .forMember('processId', function (opts) { opts.mapFrom('processId'); })
            .forMember('processCode', function (opts) { opts.mapFrom('processCode'); })
            .forMember('processName', function (opts) { opts.mapFrom('processName'); })
            .forMember('findingsText', function (opts) { opts.mapFrom('findingsText'); })
            .forMember('currentStepId', function (opts) { opts.mapFrom('currentStepId'); })
            .forMember('currentStepName', function (opts) { opts.mapFrom('currentStep'); })
            .forMember('cycleName', function (opts) { opts.mapFrom('cycleName'); })
            .forMember('firstItemIdentifier', function (opts) { opts.mapFrom('firstItemIdentifier'); })
            .forMember('secondItemIdentifier', function (opts) { opts.mapFrom('secondItemIdentifier'); })
            .forMember('frequencyId', function (opts) { opts.mapFrom('frequencyId'); })
            .forMember('workSetCycleStartDate', function (opts) { opts.mapFrom('workSetCycleStartDate'); })
            .forMember('workSetCycleEndDate', function (opts) { opts.mapFrom('workSetCycleEndDate'); })
            .forMember('workSetCycleStatus', function (opts) { opts.mapFrom('workSetCycleStatus'); })
            .forMember('formData', function (opts) { opts.mapFrom('formData'); })
            .forMember('findingData', function (opts) { opts.mapFrom('findingData'); })
            .forMember('advanceButtonText', function (opts) { opts.mapFrom('advanceButton'); })
            .forMember('advancedByGroup', function (opts) { opts.mapFrom('advancedByGroup'); })
            .forMember('canAdvance', function (opts) { opts.mapFrom('canAdvance'); })
            .forMember('notifyByGroup', function (opts) { opts.mapFrom('notifyByGroup'); })
            .forMember('stepOrder', function (opts) { opts.mapFrom('stepOrder'); })
            .forMember('status', function (opts) { opts.mapFrom('statusName'); })
            .forMember('createdOn', function (opts) { opts.mapFrom('createdOn'); })
            .forMember('createdBy', function (opts) { opts.mapFrom('createdBy'); })
            .forMember('modifiedOn', function (opts) { opts.mapFrom('modifiedOn'); })
            .forMember('modifiedBy', function (opts) { opts.mapFrom('modifiedBy'); })
            .forMember('itemFields', function (opts) { opts.mapFrom('itemFields'); })
            .forMember('forms', function (opts) { opts.mapFrom('form'); })
            .forMember('findings', function (opts) { opts.mapFrom('findings'); });

          const items: Work[] = automapper.map(WorkPlanning_GetWorksOutputDTO, Work, response);

          items.forEach(item => {
            automapper
              .createMap(WorkItemFormData, MetadataField)
              .forMember('id', function (opts) { opts.mapFrom('id'); })
              .forMember('parentId', function (opts) { opts.mapFrom('parentId'); })
              .forMember('label', function (opts) { opts.mapFrom('label'); })
              .forMember('name', function (opts) { opts.mapFrom('fieldName'); })
              .forMember('value', function (opts) { opts.mapFrom('value'); })
              .forMember('type', function (opts) { opts.mapFrom('fieldType'); })
              .forMember('isRequired', function (opts) { opts.mapFrom('isRequired'); })
              .forMember('canEdit', function (opts) { opts.mapFrom('canEdit'); })
              .forMember('canUpload', function (opts) { opts.mapFrom('canUpload'); })
              .forMember('options', function (opts) { opts.mapFrom('options'); });

            const metadataFields: MetadataField[] = automapper.map(WorkItemFormData, MetadataField, item.forms);
            const itemFields: MetadataField[] = automapper.map(WorkItemFormData, MetadataField, item.itemFields);
            metadataFields.forEach(field => {
              automapper
                .createMap(MetadataListItem_GetById, ListItem)
                .forMember('id', function (opts) { opts.mapFrom('metadataListItemId'); })
                .forMember('parentId', function (opts) { opts.mapFrom('parentItemId'); })
                .forMember('value', function (opts) { opts.mapFrom('itemValue'); })
                .forMember('description', function (opts) { opts.mapFrom('itemDescription'); })
                .forMember('sort', function (opts) { opts.mapFrom('itemSort'); });

              const options: ListItem[] = automapper.map(MetadataListItem_GetById, ListItem, field.options);
              field.options = options;
            });
            item.forms = metadataFields;
            itemFields.forEach(field => {
              automapper
                .createMap(MetadataListItem_GetById, ListItem)
                .forMember('id', function (opts) { opts.mapFrom('metadataListItemId'); })
                .forMember('parentId', function (opts) { opts.mapFrom('parentItemId'); })
                .forMember('value', function (opts) { opts.mapFrom('itemValue'); })
                .forMember('description', function (opts) { opts.mapFrom('itemDescription'); })
                .forMember('sort', function (opts) { opts.mapFrom('itemSort'); });

              const options: ListItem[] = automapper.map(MetadataListItem_GetById, ListItem, field.options);
              field.options = options;
            });
            item.itemFields = itemFields || [];
          });
          return items;
        })
      );
  }

  updateForm(workId: number, formData: string) {
    const requestUri = `${this.baseUrl}/v1/works/${workId}/form`;
    const request: WorkItemFormData_UpdateDTO = { workId: workId, formData: formData };
    return this.httpClient.put(requestUri, request);
  }

  signOff(workId: number) {
    const requestUri = `${this.baseUrl}/v1/works/${workId}/signoff`;
    return this.httpClient.post(requestUri, null);
  }

  //#endregion

  createWorkFile(payload: WorkFile): Observable<any> {
    const requestUri = `${this.baseUrl}/v1/works/${payload.workId}/upload?documenttype=${payload.fileType}`;

    payload.id = this.newGuid();

    automapper
      .createMap(WorkFile, Document_InsertInputDTO)
      .forMember('documentId', function (opts) { opts.mapFrom('id'); })
      .forMember('documentTypeCode', function (opts) { opts.mapFrom('type'); })
      .forMember('documentName', function (opts) { opts.mapFrom('name'); })
      .forMember('documentUrl', function (opts) { opts.mapFrom('url'); })
      .forMember('metadata', function (opts) { opts.mapFrom('metadata'); })
      .forMember('contentType', function (opts) { opts.mapFrom('contentType'); })
      .forMember('containerId', function (opts) { opts.mapFrom('containerId'); })
      .forMember('size', function (opts) { opts.mapFrom('size'); })
      .forMember('storageType', function () { return ''; })
      .forMember('entityType', function () { return ''; })
      .forMember('entityId', function () { return ''; })
      .forMember('isDeleted', function () { return false; })
      .forMember('status', function () { return 1; })
      .forMember('thumbnailContainerUrl', function (opts) { opts.mapFrom('thumbnail'); })

    const request = automapper.map(WorkFile, Document_InsertInputDTO, payload);
    return this.httpClient.post<any>(requestUri, request);
  }

  newGuid() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
      var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
      return v.toString(16);
    });
  }

  getWorkFile(workId: number): Observable<WorkFilesGroup[]> {
    const requestUri = `${this.baseUrl}/v1/works/${workId}/documents`;
    return this.httpClient.get<WorkDocument_GetAllOutputDTO>(requestUri)
      .pipe(
        map(response => {
          automapper
            .createMap(WorkDocument_GetAllOutputDTO, WorkFilesGroup)
            .forMember('name', function (opts) { opts.mapFrom('cycleName'); })
            .forMember('cycleStartDate', function (opts) { opts.mapFrom('cycleStartDate'); })
            .forMember('cycleEndDate', function (opts) { opts.mapFrom('cycleEndDate'); })
            .forMember('workFile', function (opts) { opts.mapFrom('workDocument'); })

          let workFiles: WorkFilesGroup[] = automapper.map(WorkDocument_GetAllOutputDTO, WorkFilesGroup, response);

          workFiles.forEach(workFile => {
            let cycleStartDateString = this.dateService.formatToString(workFile.cycleStartDate, 'MM/DD/YYYY');
            let cycleEndDateString = this.dateService.formatToString(workFile.cycleEndDate, 'MM/DD/YYYY');
            workFile.frequency = `(${cycleStartDateString} to ${cycleEndDateString})`;
          })

          workFiles.forEach(workFile => {
            automapper
              .createMap(WorkDocument_GetAllDTO, WorkFile)
              .forMember('id', function (opts) { opts.mapFrom('documentId'); })
              .forMember('type', function (opts) { opts.mapFrom('documentTypeCode'); })
              .forMember('name', function (opts) { opts.mapFrom('documentName'); })
              .forMember('url', function (opts) { opts.mapFrom('documentUrl'); })
              .forMember('metadata', function (opts) { opts.mapFrom('metadata'); })
              .forMember('metadataFieldSettingId', function (opts) { opts.mapFrom('metadataFieldSettingId'); })
              .forMember('contentType', function (opts) { opts.mapFrom('contentType'); })
              .forMember('containerId', function (opts) { opts.mapFrom('containerId'); })
              .forMember('size', function (opts) { opts.mapFrom('size'); })
              .forMember('isDeleted', function (opts) { opts.mapFrom('isDeleted'); })
              .forMember('isFinding', function (opts) { opts.mapFrom('isFinding'); })
              .forMember('status', function (opts) { opts.mapFrom('status'); })
              .forMember('thumbnail', function (opts) { opts.mapFrom('thumbnailContainerUrl'); })
              .forMember('documentDate', function (opts) { opts.mapFrom('documentDate'); })

            let files: WorkFile[] = automapper.map(WorkDocument_GetAllDTO, WorkFile, workFile.workFile);
            files.forEach(file => {
              const docTypes = ['DOCX', 'CSV', 'DOC', 'TXT', 'XLS', 'XLSX', 'PDF', 'MP4', 'MKV', 'MOV', 'AVI', 'MPG', 'TELEMETRY',];
              const type = file.type ? file.type.toUpperCase() : '';
              const metadata = JSON.parse(file.metadata) || {};
              file.processCode = metadata.processCode || '';
              file.processName = metadata.processName || '';
              file.metadataField = metadata.metadataField || '';
              file.findingName = metadata.findingName || 'N/A';
              if (docTypes.includes(type)) file.thumbnail = this.fieldsService.setPlaceholderByType(type);

              file.modifiedOnString = this.dateService.formatToString(file.documentDate, 'MMM DD, YYYY hh:mm');

            });
            workFile.workFile = files;
          });
          return workFiles;
        })
      )
  }

  //#region Findings

  getFindingFields(workId: number): Observable<MetadataField[]> {
    const requestUri = `${this.baseUrl}/v1/works/${workId}/fieldsettings`;

    return this.httpClient.get<WorkItemFormData[]>(requestUri)
      .pipe(
        map(response => {
          automapper
            .createMap(WorkItemFormData, MetadataField)
            .forMember('id', function (opts) { opts.mapFrom('id'); })
            .forMember('parentId', function (opts) { opts.mapFrom('parentId'); })
            .forMember('label', function (opts) { opts.mapFrom('label'); })
            .forMember('name', function (opts) { opts.mapFrom('fieldName'); })
            .forMember('value', function (opts) { opts.mapFrom('value'); })
            .forMember('type', function (opts) { opts.mapFrom('fieldType'); })
            .forMember('isRequired', function (opts) { opts.mapFrom('isRequired'); })
            .forMember('canEdit', function (opts) { opts.mapFrom('canEdit'); })
            .forMember('canUpload', function (opts) { opts.mapFrom('canUpload'); })
            .forMember('options', function (opts) { opts.mapFrom('options'); });

          const metadataFields: MetadataField[] = automapper.map(WorkItemFormData, MetadataField, response);
          metadataFields.forEach(field => {
            automapper
              .createMap(MetadataListItem_GetById, ListItem)
              .forMember('id', function (opts) { opts.mapFrom('metadataListItemId'); })
              .forMember('parentId', function (opts) { opts.mapFrom('parentItemId'); })
              .forMember('value', function (opts) { opts.mapFrom('itemValue'); })
              .forMember('description', function (opts) { opts.mapFrom('itemDescription'); })
              .forMember('sort', function (opts) { opts.mapFrom('itemSort'); });

            const options: ListItem[] = automapper.map(MetadataListItem_GetById, ListItem, field.options);
            field.options = options;
          });
          return metadataFields.sort((a, b) => (a.label > b.label) ? 1 : -1);
        })
      );
  }

  getFindings(workId: number): Observable<Work[]> {
    const requestUri = `${this.baseUrl}/v1/works/${workId}/findings`;
    return this.httpClient.get<WorkPlanning_GetWorksOutputDTO[]>(requestUri)
      .pipe(
        map(response => {
          automapper
            .createMap(WorkPlanning_GetWorksOutputDTO, Work)
            .forMember('id', function (opts) { opts.mapFrom('workId'); })
            .forMember('assetId', function (opts) { opts.mapFrom('assetId'); })
            .forMember('processId', function (opts) { opts.mapFrom('processId'); })
            .forMember('processCode', function (opts) { opts.mapFrom('processCode'); })
            .forMember('processName', function (opts) { opts.mapFrom('processName'); })
            .forMember('findingsText', function (opts) { opts.mapFrom('findingsText'); })
            .forMember('currentStepId', function (opts) { opts.mapFrom('currentStepId'); })
            .forMember('currentStepName', function (opts) { opts.mapFrom('currentStep'); })
            .forMember('cycleName', function (opts) { opts.mapFrom('cycleName'); })
            .forMember('firstItemIdentifier', function (opts) { opts.mapFrom('firstItemIdentifier'); })
            .forMember('secondItemIdentifier', function (opts) { opts.mapFrom('secondItemIdentifier'); })
            .forMember('frequencyId', function (opts) { opts.mapFrom('frequencyId'); })
            .forMember('workSetCycleStartDate', function (opts) { opts.mapFrom('workSetCycleStartDate'); })
            .forMember('workSetCycleEndDate', function (opts) { opts.mapFrom('workSetCycleEndDate'); })
            .forMember('workSetCycleStatus', function (opts) { opts.mapFrom('workSetCycleStatus'); })
            .forMember('formData', function (opts) { opts.mapFrom('formData'); })
            .forMember('findingData', function (opts) { opts.mapFrom('findingData'); })
            .forMember('advanceButtonText', function (opts) { opts.mapFrom('advanceButton'); })
            .forMember('advancedByGroup', function (opts) { opts.mapFrom('advancedByGroup'); })
            .forMember('canAdvance', function (opts) { opts.mapFrom('canAdvance'); })
            .forMember('notifyByGroup', function (opts) { opts.mapFrom('notifyByGroup'); })
            .forMember('stepOrder', function (opts) { opts.mapFrom('stepOrder'); })
            .forMember('status', function (opts) { opts.mapFrom('statusName'); })
            .forMember('createdOn', function (opts) { opts.mapFrom('createdOn'); })
            .forMember('createdBy', function (opts) { opts.mapFrom('createdBy'); })
            .forMember('modifiedOn', function (opts) { opts.mapFrom('modifiedOn'); })
            .forMember('modifiedBy', function (opts) { opts.mapFrom('modifiedBy'); })
            .forMember('itemFields', function (opts) { opts.mapFrom('itemFields'); })
            .forMember('forms', function (opts) { opts.mapFrom('form'); })
            .forMember('findings', function (opts) { opts.mapFrom('findings'); });

          const works: Work[] = automapper.map(WorkPlanning_GetWorksOutputDTO, Work, response);

          works.forEach(work => {
            automapper
              .createMap(WorkItemFormData, MetadataField)
              .forMember('id', function (opts) { opts.mapFrom('id'); })
              .forMember('parentId', function (opts) { opts.mapFrom('parentId'); })
              .forMember('label', function (opts) { opts.mapFrom('label'); })
              .forMember('name', function (opts) { opts.mapFrom('fieldName'); })
              .forMember('value', function (opts) { opts.mapFrom('value'); })
              .forMember('type', function (opts) { opts.mapFrom('fieldType'); })
              .forMember('isRequired', function (opts) { opts.mapFrom('isRequired'); })
              .forMember('canEdit', function (opts) { opts.mapFrom('canEdit'); })
              .forMember('canUpload', function (opts) { opts.mapFrom('canUpload'); })
              .forMember('options', function (opts) { opts.mapFrom('options'); });

            const itemFields: MetadataField[] = automapper.map(WorkItemFormData, MetadataField, work.itemFields);
            itemFields.forEach(field => {
              automapper
                .createMap(MetadataListItem_GetById, ListItem)
                .forMember('id', function (opts) { opts.mapFrom('metadataListItemId'); })
                .forMember('parentId', function (opts) { opts.mapFrom('parentItemId'); })
                .forMember('value', function (opts) { opts.mapFrom('itemValue'); })
                .forMember('description', function (opts) { opts.mapFrom('itemDescription'); })
                .forMember('sort', function (opts) { opts.mapFrom('itemSort'); });

              const options: ListItem[] = automapper.map(MetadataListItem_GetById, ListItem, field.options);
              field.options = options;
            });
            work.itemFields = itemFields || [];

            automapper
              .createMap(WorkItemFindingData, WorkFinding)
              .forMember('id', function (opts) { opts.mapFrom('workFindingId'); })
              .forMember('workId', function (opts) { opts.mapFrom('workId'); })
              .forMember('stepId', function (opts) { opts.mapFrom('stepId'); })
              .forMember('findingData', function (opts) { opts.mapFrom('findingData'); })
              .forMember('status', function (opts) { opts.mapFrom('status'); })
              .forMember('fields', function (opts) { opts.mapFrom('fields'); });

            const findings: WorkFinding[] = automapper.map(WorkItemFindingData, WorkFinding, work.findings);
            findings.forEach(finding => {
              automapper
                .createMap(WorkItemFormData, MetadataField)
                .forMember('id', function (opts) { opts.mapFrom('id'); })
                .forMember('parentId', function (opts) { opts.mapFrom('parentId'); })
                .forMember('label', function (opts) { opts.mapFrom('label'); })
                .forMember('name', function (opts) { opts.mapFrom('fieldName'); })
                .forMember('value', function (opts) { opts.mapFrom('value'); })
                .forMember('type', function (opts) { opts.mapFrom('fieldType'); })
                .forMember('isRequired', function (opts) { opts.mapFrom('isRequired'); })
                .forMember('canEdit', function (opts) { opts.mapFrom('canEdit'); })
                .forMember('canUpload', function (opts) { opts.mapFrom('canUpload'); })
                .forMember('options', function (opts) { opts.mapFrom('options'); });

              const metadataFields: MetadataField[] = automapper.map(WorkItemFormData, MetadataField, finding.fields);

              metadataFields.forEach(field => {
                automapper
                  .createMap(MetadataListItem_GetById, ListItem)
                  .forMember('id', function (opts) { opts.mapFrom('metadataListItemId'); })
                  .forMember('parentId', function (opts) { opts.mapFrom('parentItemId'); })
                  .forMember('value', function (opts) { opts.mapFrom('itemValue'); })
                  .forMember('description', function (opts) { opts.mapFrom('itemDescription'); })
                  .forMember('sort', function (opts) { opts.mapFrom('itemSort'); });

                const options: ListItem[] = automapper.map(MetadataListItem_GetById, ListItem, field.options);
                field.options = options;
              });
              finding.fields = metadataFields;
            });
            work.findings = findings;
          });
          return works;
        })
      );
  }

  createFinding(workId: number, work: Work) {
    const requestUri = `${this.baseUrl}/v1/works/${workId}/findings`;

    automapper
      .createMap(Work, WorkItemFindingData_InsertDTO)
      .forMember('workFindingId', function () { return 0; })
      .forMember('workId', function (opts) { opts.mapFrom('id'); })
      .forMember('stepId', function (opts) { opts.mapFrom('currentStepId'); })
      .forMember('findingData', function (opts) { opts.mapFrom('findingData'); });

    const request: WorkItemFindingData_InsertDTO = automapper.map(Work, WorkItemFindingData_InsertDTO, work);

    return this.httpClient.post(requestUri, request);
  }

  updateFinding(findingId: number, data: string) {
    const requestUri = `${this.baseUrl}/v1/workfindings/${findingId}`;
    const request = { findingData: data };

    return this.httpClient.put(requestUri, request);
  }


  downloadFinding(findingId: number): Observable<HttpEvent<Blob>> {
    const request = new HttpRequest('GET', `${this.baseUrl}/v1/workfindings/${findingId}/download`, null, {
      reportProgress: true,
      responseType: 'blob'
    });
    return this.httpClient.request(request);
  }

  //#endregion
}

