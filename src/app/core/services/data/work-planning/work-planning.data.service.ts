import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { WorkPlanningParentItem, WorkPlanningWorkItemParent } from 'src/app/core/models/work-planning';
import { Work } from 'src/app/core/models/entity/work';
import { WorkFile, WorkFilesGroup } from 'src/app/core/models/entity/work-item';
import { MetadataField } from 'src/app/core/models/entity/metadata-field';
import { HttpEvent } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export abstract class WorkPlanningDataService {

  abstract getParentItem(): Observable<WorkPlanningParentItem[]>;

  //#region Works

  abstract getWorks(assetId: number, dateFilter: string, actionList?: boolean, cycle?: string, pageNumber?: number, pageSize?: number): Observable<Work[]>;
  abstract downloadReport(assetId: number, queryType: string, dateFitler?: string, cycle?: string): Observable<HttpEvent<Blob>>;

  //endregion

  abstract getWorkItemGroup(name: string): Observable<WorkPlanningWorkItemParent[]>;

  //#region Forms

  abstract getForms(workId: number): Observable<Work[]>;
  abstract updateForm(workId: number, formData: string);
  abstract signOff(workId: number);

  //endregion

  //#region Findings

  abstract getFindingFields(workId: number): Observable<MetadataField[]>;
  abstract getFindings(workId: number): Observable<Work[]>;
  abstract createFinding(workId: number, work: Work);
  abstract updateFinding(findingId: number, data: string);
  abstract downloadFinding(findingId: number): Observable<HttpEvent<Blob>>;

  //endregion

  //#region Workfile

  abstract createWorkFile(payload: WorkFile): Observable<any>;
  abstract getWorkFile(workId: number): Observable<WorkFilesGroup[]>;

  //endregion
}