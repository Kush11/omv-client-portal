import { Injectable } from '@angular/core';
import { WorkPlanningDataService } from './work-planning.data.service';
import { Observable } from 'rxjs/internal/Observable';
import { WorkPlanningWorkItemParent } from 'src/app/core/models/work-planning';
import { Work } from 'src/app/core/models/entity/work';
import { WorkFile, WorkFilesGroup } from 'src/app/core/models/entity/work-item';
import { MetadataField } from 'src/app/core/models/entity/metadata-field';
import { HttpEvent } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class WorkPlanningMockDataService implements WorkPlanningDataService {
  downloadReport(assetId: number, queryType: string, dateFitler?: string, cycle?: string): Observable<HttpEvent<Blob>> {
    throw new Error("Method not implemented.");
  }
  getParentItem(): Observable<import("../../../models/work-planning").WorkPlanningParentItem[]> {
    throw new Error("Method not implemented.");
  }
  getWorks(assetId: number, dateFilter: string, actionList?: boolean): Observable<Work[]> {
    throw new Error("Method not implemented.");
  }
  getWorkItemGroup(name: string): Observable<WorkPlanningWorkItemParent[]> {
    throw new Error("Method not implemented.");
  }
  getForms(workId: number): Observable<Work[]> {
    throw new Error("Method not implemented.");
  }
  updateForm(workId: number, formData: string) {
    throw new Error("Method not implemented.");
  }
  signOff(workId: number) {
    throw new Error("Method not implemented.");
  }
  getFindingFields(workId: number): Observable<MetadataField[]> {
    throw new Error("Method not implemented.");
  }
  getFindings(workId: number): Observable<Work[]> {
    throw new Error("Method not implemented.");
  }
  createFinding(workId: number, work: Work) {
    throw new Error("Method not implemented.");
  }
  updateFinding(findingId: number, data: string) {
    throw new Error("Method not implemented.");
  }
  downloadFinding(findingId: number): Observable<HttpEvent<Blob>> {
    throw new Error("Method not implemented.");
  }
  createWorkFile(payload: WorkFile): Observable<any> {
    throw new Error("Method not implemented.");
  }
  getWorkFile(workId: number): Observable<WorkFilesGroup[]> {
    throw new Error("Method not implemented.");
  }


}