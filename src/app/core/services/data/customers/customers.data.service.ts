import { Observable } from 'rxjs/internal/Observable';
import { Customer } from 'src/app/core/models/entity/customer';
import { SASTokenModule, SASTokenType } from 'src/app/core/enum/azure-sas-token';

export abstract class CustomersDataService {

  constructor() { }

  abstract getSettings(): Observable<any[]>;
  abstract getAzureSASToken(type: SASTokenType, module: SASTokenModule): Observable<string>;
  abstract getWriteSASToken(): Observable<string>;
  abstract getByHostHeader(header: string): Observable<Customer>;
  abstract migrateDatabase(): Promise<any>;
}


