import { CustomersDataService } from './customers.data.service';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Injectable } from '@angular/core';
import { Customer } from 'src/app/core/models/entity/customer';
import { CustomerHostHeaderDTO } from 'src/app/core/dtos/output/customers/CustomerHostHeaderDTO';
import { Observable } from 'rxjs/internal/Observable';
import { map } from 'rxjs/internal/operators/map';
import { SASTokenType, SASTokenModule } from 'src/app/core/enum/azure-sas-token';

@Injectable({
  providedIn: 'root'
})
export class CustomersWebDataService implements CustomersDataService {

  constructor(private httpClient: HttpClient) { }

  getAzureSASToken(type: SASTokenType, module: SASTokenModule): Observable<string> {
    const options = { params: new HttpParams() };
    if (type) options.params = options.params.set('permission', type);
    if (module === SASTokenModule.Core) {
      options.params = options.params.set('containername', environment.issuesContainer);
      options.params = options.params.set('isCore', "true");
    } else if (module) options.params = options.params.set('type', module);

    const requestUri = environment.api.baseUrl + `/v1/documents/sastoken`;
    return this.httpClient.get<string>(requestUri, options);
  }

  getAzureReadSASToken(): Observable<string> {
    const requestUri = environment.api.baseUrl + `/v1/documents/sastoken?permission=read&type=media`;
    return this.httpClient.get<string>(requestUri);
  }
  getWriteSASToken(): Observable<string> {
    const requestUri = environment.api.baseUrl + `/v1/documents/sastoken?permission=write`;
    return this.httpClient.get<string>(requestUri);
  }



  getSettings(): Observable<any[]> {
    var requestUri = environment.api.baseUrl + `/v1/customers/settings`;
    return this.httpClient.get<any>(requestUri);
  }

  getByHostHeader(header: string): Observable<Customer> {
    var requestUri = environment.api.baseUrl + `/v1/customers`;

    const options = { params: new HttpParams() };
    options.params = options.params.set('header', header);

    return this.httpClient.get<CustomerHostHeaderDTO>(requestUri, options)
      .pipe(
        map(response => {
          automapper
            .createMap(CustomerHostHeaderDTO, Customer)
            .forMember('id', function (opts) { opts.mapFrom('customerId'); })
            .forMember('name', function (opts) { opts.mapFrom('name'); })
            .forMember('authServerId', function (opts) { opts.mapFrom('authServerId'); })
            .forMember('issuerUrl', function (opts) { opts.mapFrom('issuerUrl'); })
            .forMember('clientSecret', function (opts) { opts.mapFrom('clientSecret'); })
            .forMember('clientId', function (opts) { opts.mapFrom('clientId'); })
            .forMember('customerLogo', function (opts) { opts.mapFrom('imageUrl'); });

          var user = automapper.map(CustomerHostHeaderDTO, Customer, response);
          console.log('CustomersWebDataService - getHostHeader: ', user);
          return user;
        })
      );
  }

  migrateDatabase(): Promise<any> {
    console.log("Migrate Database");
    const requestUri = environment.api.baseUrl + `/v1/migrations/initialize`;
    return this.httpClient.post(requestUri, null).toPromise();
  }

}
