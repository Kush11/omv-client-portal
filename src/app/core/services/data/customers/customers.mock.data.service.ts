import { CustomersDataService } from './customers.data.service';
import { Observable } from 'rxjs/internal/Observable';
import { HttpClient } from '@angular/common/http';
import { Customer } from 'src/app/core/models/entity/customer';
import { SASTokenType, SASTokenModule } from 'src/app/core/enum/azure-sas-token';

export class CustomersMockDataService implements CustomersDataService {


  constructor(private httpClient: HttpClient) { }

  getSettings(): Observable<any[]> {
    throw new Error('Method not implemented.');
  }

  getAzureReadSASToken(): Observable<string> {
    throw new Error('Method not implemented.');
  }

  getWriteSASToken(): Observable<string> {
    throw new Error('Method not implemented.');
  }
  getAzureSASToken(type: SASTokenType, module: SASTokenModule): Observable<string> {
    throw new Error('Method not implemented.');
  }

  getByHostHeader(header: string): Observable<Customer> {
    throw new Error('Method not implemented.');
  }

  migrateDatabase(): Promise<any> {
    throw new Error('Method not implemented.');
  }
}
