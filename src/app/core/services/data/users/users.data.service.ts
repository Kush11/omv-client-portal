import { Observable } from 'rxjs/internal/Observable';
import { User, Users } from 'src/app/core/models/entity/user';
import { Permission } from 'src/app/core/enum/permission';
import { Injectable } from '@angular/core';
import { Widget, UserWidget } from 'src/app/core/models/entity/widget';

@Injectable({
  providedIn: 'root'
})
export abstract class UsersDataService {

  constructor() { }

  abstract getUsers(name: string, groupid: number, status?: number, pageNumber?: number, pageSize?: number): Observable<Users>;
  abstract getUser(id: number): Observable<User>;
  abstract createUser(payload: User);
  abstract updateUser(id: number, payload: User): Observable<User>;

  abstract getGroups(userid: number);
  abstract updateGroups(userid: number, payload: number[], isAddRoles: boolean);

  // abstract getPermissions(userid: number): Observable<Permission[]>;

  abstract getLoggedInUser(): Observable<User>;

  abstract getUserDashboardWidgets(): Observable<UserWidget[]>;
  abstract updateUserDashboardWidgets(panels: UserWidget[]): Observable<any>;
  // abstract getPFCData(period: string): Observable<any[]>;
  // abstract getUploadRequestsData(period: string): Observable<any[]>;
  // abstract getLiveStreamHistory(): Observable<any[]>;
  abstract getAllWidgets(): Observable<Widget[]>;
}
