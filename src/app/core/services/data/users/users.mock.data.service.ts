import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { map } from 'rxjs/internal/operators/map';
import { User, Users } from 'src/app/core/models/entity/user';
import { UsersDataService } from './users.data.service';
import { HttpClient } from '@angular/common/http';
import { Widget, UserWidget } from 'src/app/core/models/entity/widget';
// import { Permission } from 'src/app/core/enum/permission';

@Injectable({
  providedIn: 'root'
})
export class UsersMockDataService implements UsersDataService {


  constructor(private httpClient: HttpClient) { }

  getUsers(name: string, groupid: number, status?: number, pageNumber?: number, pageSize?: number): Observable<Users> {
    var mockUrl = `./assets/mock/admin-users.json`;
    var data = this.httpClient.get<Users>(mockUrl);

    return data;
  }

  getUser(id: number): Observable<User> {
    var mockUrl = `./assets/mock/admin-users.json`;
    return this.httpClient.get<User[]>(mockUrl).pipe(map(users => {
      return users.find(user => user.id === id);
    }));
  }

  createUser(payload: User) {
    var mockUrl = `./assets/mock/admin-users.json`;
    return this.httpClient.get<User>(mockUrl).pipe(map(user => {
      var _user = new User();
      _user.id = 3;
      return _user;
    }));
  }

  updateUser(id: number, payload: User): Observable<User> {
    var mockUrl = `./assets/mock/admin-users.json`;
    return this.httpClient.get<User[]>(mockUrl).pipe(map(users => {
      return users.find(user => user.id === id);
    }));
  }

  updateGroups(userid: number, payload: number[]) {
    var mockUrl = `./assets/mock/admin-users.json`;
    return this.httpClient.put<any>(mockUrl, payload);
  }

  getGroups(userid: number) {
    var mockUrl = `./assets/mock/admin-users.json`;
    var data = this.httpClient.get<User>(mockUrl);

    return data;
  }

  // getPermissions(userid: number): Observable<Permission[]> {
  //   throw new Error("Method not implemented.");
  // }

  getLoggedInUser(): Observable<User> {
    return null;
  }

  getUserDashboardWidgets(): Observable<UserWidget[]> {
    throw new Error('Method not implemented.');
  }

  updateUserDashboardWidgets(panels: UserWidget[]): Observable<any> {
    throw new Error('Method not implemented.');
  }

  // getPFCData(period: string): Observable<any[]> {
  //   throw new Error('Method not implemented.');
  // }

  // getUploadRequestsData(period: string): Observable<any[]> {
  //   throw new Error('Method not implemented.');
  // }

  // getLiveStreamHistory(): Observable<any[]> {
  //   throw new Error('Method not implemented.');
  // }

  getAllWidgets(): Observable<Widget[]> {
    throw new Error('Method not implemented.');
  }


}
