import { Injectable } from "@angular/core";
import { Observable } from 'rxjs/internal/Observable';
import { of } from 'rxjs/internal/observable/of';
import { User, Users } from 'src/app/core/models/entity/user';
import { UsersDataService } from './users.data.service';
import { environment } from 'src/environments/environment';
import * as automapper from 'automapper-ts';
import { HttpClient, HttpParams } from '@angular/common/http';
import { User_GetByIdOutputDTO } from 'src/app/core/dtos/output/users/User_GetByIdOutputDTO';
import { User_SearchOutputDTO, User_SearchOutputDTOData } from 'src/app/core/dtos/output/users/User_SearchOutputDTO';
import { User_InsertInputDTO } from 'src/app/core/dtos/input/users/User_InsertInputDTO';
import { User_UpdateInputDTO } from 'src/app/core/dtos/input/users/User_UpdateInputDTO';
import { User_InsertRoleInputDTO } from 'src/app/core/dtos/input/users/User_InsertRoleInputDTO';
import { User_GetRolesByIdOutputDTO } from 'src/app/core/dtos/output/users/User_GetRolesByIdOutputDTO';
import { Group } from 'src/app/core/models/entity/group';
import { map } from 'rxjs/internal/operators/map';
import { DateService } from '../../business/dates/date.service';
import { Widget, UserWidget } from 'src/app/core/models/entity/widget';
import { Widget_GetAllOutputDTO } from 'src/app/core/dtos/output/widgets/Widget_GetAllOutputDTO';

@Injectable({
  providedIn: 'root'
})
export class UsersWebDataService implements UsersDataService {


  constructor(private httpClient: HttpClient, private dateService: DateService) { }

  //#region Users

  getUsers(name: string, groupid: number, status?: number, pageNumber?: number, pageSize?: number): Observable<Users> {
    const requestUri = environment.api.baseUrl + `/v1/users`;
    const options = { params: new HttpParams() };
    if (name) options.params = options.params.set('name', name);
    options.params = options.params.set('status', status.toString());
    if (groupid) options.params = options.params.set('roleId', groupid.toString());
    if (pageNumber) options.params = options.params.set('pageNumber', pageNumber.toString());
    if (pageSize) options.params = options.params.set('limit', pageSize.toString());

    return this.httpClient.get<User_SearchOutputDTO>(requestUri, options)
      .pipe(
        map(
          response => {
            let users = new Users();
            users.total = response.pagination.total;

            automapper
              .createMap(User_SearchOutputDTOData, User)
              .forMember('id', function (opts) { opts.mapFrom('userId'); })
              .forMember('userName', function (opts) { opts.mapFrom('userName'); })
              .forMember('emailAddress', function (opts) { opts.mapFrom('emailAddress'); })
              .forMember('firstName', function (opts) { opts.mapFrom('firstName'); })
              .forMember('lastName', function (opts) { opts.mapFrom('lastName'); })
              .forMember('displayName', function (opts) { opts.mapFrom('displayName'); })
              .forMember('groups', function (opts) { opts.mapFrom('roleNames'); })
              .forMember('status', function (opts) { opts.mapFrom('status'); })
              .forMember('statusName', function (opts) { opts.mapFrom('statusName'); })
              .forMember('createdOn', function (opts) { opts.mapFrom('createdOn'); })
              .forMember('createdBy', function (opts) { opts.mapFrom('createdBy'); })
              .forMember('modifiedOn', function (opts) { opts.mapFrom('modifiedOn'); })
              .forMember('modifiedBy', function (opts) { opts.mapFrom('modifiedBy'); });

            let usersData: User[] = automapper.map(User_SearchOutputDTOData, User, response.data);
            users.data = usersData;
            return users;
          })
      );
  }

  getUser(id: number): Observable<User> {
    const requestUri = environment.api.baseUrl + `/v1/users/${id}`;

    return this.httpClient.get<User_GetByIdOutputDTO[]>(requestUri)
      .pipe(
        map(response => {
          automapper
            .createMap(User_GetByIdOutputDTO, User)
            .forMember('id', function (opts) { opts.mapFrom('userId'); })
            .forMember('userName', function (opts) { opts.mapFrom('userName'); })
            .forMember('emailAddress', function (opts) { opts.mapFrom('emailAddress'); })
            .forMember('firstName', function (opts) { opts.mapFrom('firstName'); })
            .forMember('lastName', function (opts) { opts.mapFrom('lastName'); })
            .forMember('displayName', function (opts) { opts.mapFrom('displayName'); })
            .forMember('roleNames', function (opts) { opts.mapFrom('roleNames'); })
            .forMember('status', function (opts) { opts.mapFrom('status'); })
            .forMember('statusName', function (opts) { opts.mapFrom('statusName'); })
            .forMember('createdOn', function (opts) { opts.mapFrom('createdOn'); })
            .forMember('createdBy', function (opts) { opts.mapFrom('createdBy'); })
            .forMember('modifiedOn', function (opts) { opts.mapFrom('modifiedOn'); })
            .forMember('modifiedBy', function (opts) { opts.mapFrom('modifiedBy'); })
          const user: User = automapper.map(User_GetByIdOutputDTO, User, response);
          return user;
        })
      );
  }

  createUser(payload: User) {
    const requestUri = environment.api.baseUrl + `/v1/users`;

    automapper
      .createMap(payload, User_InsertInputDTO)
      .forMember('userName', function (opts) { opts.mapFrom('UserName'); })
      .forMember('emailAddress', function (opts) { opts.mapFrom('EmailAddress'); })
      .forMember('firstName', function (opts) { opts.mapFrom('FirstName'); })
      .forMember('lastName', function (opts) { opts.mapFrom('LastName'); })
      .forMember('userId', function (opts) { opts.mapFrom('id'); })
      .forMember('displayName', function (opts) { opts.mapFrom('DisplayName'); })

    const request = automapper.map(payload, User_InsertInputDTO, payload);

    return this.httpClient.post(requestUri, request)
      .pipe(
        map(response => {
          automapper
            .createMap(response, User)
            .forMember('id', function (opts) { opts.mapFrom('userId'); })
            .forMember('userName', function (opts) { opts.mapFrom('userName'); })
            .forMember('emailAddress', function (opts) { opts.mapFrom('emailAddress'); })
            .forMember('firstName', function (opts) { opts.mapFrom('firstName'); })
            .forMember('lastName', function (opts) { opts.mapFrom('lastName'); })
            .forMember('displayName', function (opts) { opts.mapFrom('displayName'); })
            .forMember('roleNames', function (opts) { opts.mapFrom('roleNames'); })
            .forMember('status', function (opts) { opts.mapFrom('status'); })
            .forMember('statusName', function (opts) { opts.mapFrom('statusName'); })
            .forMember('createdOn', function (opts) { opts.mapFrom('createdOn'); })
            .forMember('createdBy', function (opts) { opts.mapFrom('createdBy'); })
            .forMember('modifiedOn', function (opts) { opts.mapFrom('modifiedOn'); })
            .forMember('modifiedBy', function (opts) { opts.mapFrom('modifiedBy'); })

          const user: User = automapper.map(response, User, response);
          return user;
        })
      );
  }

  updateUser(id: number, payload: User): Observable<User> {
    const requestUri = environment.api.baseUrl + `/v1/users/${id}`;

    const request = new User_UpdateInputDTO();
    request.UserId = payload.id;
    request.UserName = payload.userName;
    request.EmailAddress = payload.emailAddress;
    request.FirstName = payload.firstName;
    request.LastName = payload.lastName;
    request.Status = payload.status;

    return this.httpClient.put<User_UpdateInputDTO>(requestUri, request)
      .pipe(
        map(
          response => {
            automapper
              .createMap(User_UpdateInputDTO, User)
              .forMember('id', function (opts) { opts.mapFrom('userId'); })
              .forMember('userName', function (opts) { opts.mapFrom('userName'); })
              .forMember('emailAddress', function (opts) { opts.mapFrom('emailAddress'); })
              .forMember('firstName', function (opts) { opts.mapFrom('firstName'); })
              .forMember('lastName', function (opts) { opts.mapFrom('lastName'); })
              .forMember('displayName', function (opts) { opts.mapFrom('displayName'); })
              .forMember('roleNames', function (opts) { opts.mapFrom('roleNames'); })
              .forMember('status', function (opts) { opts.mapFrom('status'); })
              .forMember('statusName', function (opts) { opts.mapFrom('statusName'); })
              .forMember('createdOn', function (opts) { opts.mapFrom('createdOn'); })
              .forMember('createdBy', function (opts) { opts.mapFrom('createdBy'); })
              .forMember('modifiedOn', function (opts) { opts.mapFrom('modifiedOn'); })
              .forMember('modifiedBy', function (opts) { opts.mapFrom('modifiedBy'); })

            const user: User = automapper.map(User_UpdateInputDTO, User, response);
            return user;
          })
      );
  }

  //#endregion

  //#region Groups

  updateGroups(userid: number, payload: number[], isAddRoles: boolean) {
    const requestUri = environment.api.baseUrl + `/v1/users/${userid}/roles?isAddRoles=${isAddRoles}`;

    const request = new User_InsertRoleInputDTO();
    request.RoleIds = payload;

    return this.httpClient.post(requestUri, request);
  }

  getGroups(userid: number): Observable<Group[]> {
    var requestUri = environment.api.baseUrl + `/v1/users/${userid}/roles`;

    return this.httpClient.get<User_GetRolesByIdOutputDTO>(requestUri)
      .pipe(
        map(response => {
          automapper
            .createMap(User_GetRolesByIdOutputDTO, Group)
            .forMember('id', function (opts) { opts.mapFrom('roleId'); })
            .forMember('name', function (opts) { opts.mapFrom('roleName'); })

          const groups: Group[] = automapper.map(User_GetRolesByIdOutputDTO, Group, response);
          return groups;
        })
      );
  }


  getLoggedInUser(): Observable<User> {
    var requestUri = environment.api.baseUrl + `/v1/users/username`;
    return this.httpClient.get<User_GetByIdOutputDTO>(requestUri).pipe(
      map(response => {
        automapper
          .createMap(User_GetByIdOutputDTO, User)
          .forMember('id', function (opts) { opts.mapFrom('userId'); })
          .forMember('userName', function (opts) { opts.mapFrom('userName'); })
          .forMember('emailAddress', function (opts) { opts.mapFrom('emailAddress'); })
          .forMember('firstName', function (opts) { opts.mapFrom('firstName'); })
          .forMember('lastName', function (opts) { opts.mapFrom('lastName'); })
          .forMember('displayName', function (opts) { opts.mapFrom('displayName'); })
          .forMember('roleNames', function (opts) { opts.mapFrom('roleNames'); })
          .forMember('status', function (opts) { opts.mapFrom('status'); })
          .forMember('statusName', function (opts) { opts.mapFrom('statusName'); })
          .forMember('createdOn', function (opts) { opts.mapFrom('createdOn'); })
          .forMember('createdBy', function (opts) { opts.mapFrom('createdBy'); })
          .forMember('modifiedOn', function (opts) { opts.mapFrom('modifiedOn'); })
          .forMember('modifiedBy', function (opts) { opts.mapFrom('modifiedBy'); })
          .forMember('folderPaths', function (opts) { opts.mapFrom('folderPaths'); })
          .forMember('permissions', function (opts) { opts.mapFrom('permissions'); });
        const user: User = automapper.map(User_GetByIdOutputDTO, User, response);
        return user;
      })
    );
  }

  //#endregion


  //#region Dashboard

  getUserDashboardWidgets(): Observable<UserWidget[]> {
    var requestUri = environment.api.baseUrl + `/v1/userwidgets`;
    // var requestUri = `./assets/mock/dashboard-templates.json`;
    return this.httpClient.get<Widget_GetAllOutputDTO[]>(requestUri)
      .pipe(
        map(response => {
          automapper
            .createMap(Widget_GetAllOutputDTO, UserWidget)
            .forMember('widgetId', function (opts) { opts.mapFrom('widgetId'); })
            .forMember('sizeX', function (opts) { opts.mapFrom('sizeX'); })
            .forMember('sizeY', function (opts) { opts.mapFrom('sizeY'); })
            .forMember('row', function (opts) { opts.mapFrom('row'); })
            .forMember('col', function (opts) { opts.mapFrom('col'); })
            .forMember('content', function (opts) { opts.mapFrom('content'); })
            .forMember('name', function (opts) { opts.mapFrom('name'); })
            .forMember('header', function (opts) { opts.mapFrom('header'); })
            .forMember('data', function (opts) { opts.mapFrom('data'); });
          const userWidget: UserWidget[] = automapper.map(Widget_GetAllOutputDTO, UserWidget, response);
          return userWidget;
        })
      );
  }

  updateUserDashboardWidgets(panels: UserWidget[]): Observable<any> {
    const requestUri = environment.api.baseUrl + `/v1/userwidgets`;

    automapper
      .createMap(panels, Widget_GetAllOutputDTO)
      .forMember('widgetId', function (opts) { opts.mapFrom('widgetId'); })
      .forMember('sizeX', function (opts) { opts.mapFrom('sizeX'); })
      .forMember('sizeY', function (opts) { opts.mapFrom('sizeY'); })
      .forMember('row', function (opts) { opts.mapFrom('row'); })
      .forMember('col', function (opts) { opts.mapFrom('col'); })
      .forMember('data', function (opts) { opts.mapFrom('data'); });

    const request = automapper.map(panels, Widget_GetAllOutputDTO, panels);

    return this.httpClient.post(requestUri, request);
  }

  getAllWidgets(): Observable<Widget[]> {
    const requestUri = environment.api.baseUrl + `/v1/widgets`;
    const data = this.httpClient.get<Widget[]>(requestUri);
    return data;
  }

  //#endregion
}
