import { Role_InsertInputDTO } from '../../../dtos/input/roles/Role_InsertInputDTO';
import { Role_GetAllOutputDTO } from '../../../dtos/output/roles/Role_GetAllOutputDTO';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable, of } from "rxjs";
import { User } from "src/app/core/models/entity/user";
import { GroupsDataService } from "./groups.data.service";
import { Group } from "src/app/core/models/entity/group";
import { environment } from "src/environments/environment";
import { map } from "rxjs/operators";
import { Permission } from 'src/app/core/enum/permission';
import * as automapper from 'automapper-ts';
import { Role_UpdateInputDTO } from 'src/app/core/dtos/input/roles/Role_UpdateInputDTO';
import { Role_GetPermissionsByIdOutputDTO } from 'src/app/core/dtos/output/roles/Role_GetPermissionsByIdOutputDTO';
import { Role_InsertPermissionInputDTO } from 'src/app/core/dtos/input/roles/Role_InsertPermissionInputDTO';
import { Role_GetMembersByIdOutputDTO } from 'src/app/core/dtos/output/roles/Role_GetMembersByIdOutputDTO';
import { Role_InsertMembersInputDTO } from 'src/app/core/dtos/input/roles/Role_InsertMembersInputDTO';
import { Directory } from 'src/app/core/models/entity/directory';
import { Role_InsertMediaAccessInputDTO } from 'src/app/core/dtos/input/roles/Role_InsertMediaAccessInputDTO';
import { Role_GetDirectoriesByIdOutputDTO } from 'src/app/core/dtos/output/roles/Role_GetDirectoriesByIdOutputDTO';
import { DateService } from '../../business/dates/date.service';

@Injectable({
  providedIn: "root"
})
export class GroupsWebDataService implements GroupsDataService {
  private requestUri = environment.api.baseUrl + `/v1/roles`;

  httpOptions = {
    headers: new HttpHeaders({
      "Cache-Control": "no-cache",
      Pragma: "no-cache",
      Expires: "-1"
    })
  };

  constructor(private httpClient: HttpClient, private dateService: DateService) { }

  getGroups(): Observable<Group[]> {
    const requestUri = environment.api.baseUrl + `/v1/roles`;

    return this.httpClient.get<Role_GetAllOutputDTO[]>(requestUri).pipe(map(
      response => {
        automapper
          .createMap(Role_GetAllOutputDTO, Group)
          .forMember('id', (opts: AutoMapperJs.IMemberConfigurationOptions) => opts.mapFrom('roleId'))
          .forMember('name', (opts: AutoMapperJs.IMemberConfigurationOptions) => opts.mapFrom('roleName'))
          .forMember('nameWithBadge', (opts: AutoMapperJs.IMemberConfigurationOptions) => opts.mapFrom('roleNameWithBadge'))
          .forMember('memberCount', (opts: AutoMapperJs.IMemberConfigurationOptions) => opts.mapFrom('memberCount'))
          .forMember('isSystem', function (opts) { opts.mapFrom('isSystem'); })
          .forMember('description', function (opts) { opts.mapFrom('description'); })
          .forMember('status', function (opts) { opts.mapFrom('status'); })
          .forMember('createdOn', function (opts) { opts.mapFrom('createdOn'); })
          .forMember('createdBy', function (opts) { opts.mapFrom('createdBy'); })
          .forMember('modifiedOn', function (opts) { opts.mapFrom('modifiedOn'); })
          .forMember('modifiedBy', function (opts) { opts.mapFrom('modifiedBy'); });

        let groups: Group[] = automapper.map(Role_GetAllOutputDTO, Group, response);
        groups.map(group => group.modifiedOnString = this.dateService.formatToString(group.modifiedOn));
        return groups;
      })
    );
  }

  getGroup(id: number): Observable<Group> {
    const requestUri = environment.api.baseUrl + `/v1/roles/${id}`;

    return this.httpClient.get<Role_GetAllOutputDTO[]>(requestUri).pipe(map(
      response => {
        automapper
          .createMap(Role_GetAllOutputDTO, Group)
          .forMember('id', (opts: AutoMapperJs.IMemberConfigurationOptions) => opts.mapFrom('roleId'))
          .forMember('name', (opts: AutoMapperJs.IMemberConfigurationOptions) => opts.mapFrom('roleName'))
          .forMember('memberCount', (opts: AutoMapperJs.IMemberConfigurationOptions) => opts.mapFrom('memberCount'))
          .forMember('isSystem', function (opts) { opts.mapFrom('isSystem'); })
          .forMember('status', function (opts) { opts.mapFrom('status'); })
          .forMember('description', function (opts) { opts.mapFrom('description'); })

          .forMember('createdOn', function (opts) { opts.mapFrom('createdOn'); })
          .forMember('createdBy', function (opts) { opts.mapFrom('createdBy'); })
          .forMember('modifiedOn', function (opts) { opts.mapFrom('modifiedOn'); })
          .forMember('modifiedBy', function (opts) { opts.mapFrom('modifiedBy'); })

        let group: Group = automapper.map(Role_GetAllOutputDTO, Group, response);
        return group;
      })
    );
  }

  createGroup(payload: Group): Observable<Group> {
    const requestUri = environment.api.baseUrl + `/v1/roles`;

    automapper
      .createMap(payload, Role_InsertInputDTO)
      .forMember('roleId', function (opts) { opts.mapFrom('id'); })
      .forMember('rolename', function (opts) { opts.mapFrom('name'); })
      .forMember('isSystem', function (opts) { opts.mapFrom('isSystem'); })
      .forMember('status', function (opts) { opts.mapFrom('status'); })
      .forMember('description', function (opts) { opts.mapFrom('description'); });

    const request = automapper.map(payload, Role_InsertInputDTO, payload);

    return this.httpClient.post(requestUri, request).pipe(map(
      response => {
        automapper
          .createMap(Role_GetAllOutputDTO, Group)
          .forMember('id', (opts: AutoMapperJs.IMemberConfigurationOptions) => opts.mapFrom('roleId'))
          .forMember('name', (opts: AutoMapperJs.IMemberConfigurationOptions) => opts.mapFrom('roleName'))
          .forMember('memberCount', (opts: AutoMapperJs.IMemberConfigurationOptions) => opts.mapFrom('memberCount'))
          .forMember('isSystem', function (opts) { opts.mapFrom('isSystem'); })
          .forMember('status', function (opts) { opts.mapFrom('status'); })
          .forMember('description', function (opts) { opts.mapFrom('description'); })
          .forMember('createdOn', function (opts) { opts.mapFrom('createdOn'); })
          .forMember('createdBy', function (opts) { opts.mapFrom('createdBy'); })
          .forMember('modifiedOn', function (opts) { opts.mapFrom('modifiedOn'); })
          .forMember('modifiedBy', function (opts) { opts.mapFrom('modifiedBy'); })

        let _response = automapper.map(Role_GetAllOutputDTO, Group, response);
        return _response;
      })
    );
  }

  updateGroup(id: number, payload: Group) {
    const requestUri = environment.api.baseUrl + `/v1/roles/${id}`;

    automapper
      .createMap(payload, Role_UpdateInputDTO)
      .forMember('roleName', function (opts) { opts.mapFrom('name'); })
      .forMember('isSystem', function (opts) { opts.mapFrom('isSystem'); })
      .forMember('status', function (opts) { opts.mapFrom('status'); })
      .forMember('description', function (opts) { opts.mapFrom('description'); })

    const request = automapper.map(payload, Role_UpdateInputDTO, payload);
    request.isSystem = request.isSystem.toString();
    request.roleName = request.roleName ? request.roleName.toString() : '';

    return this.httpClient.put(requestUri, request).pipe(map(
      response => {
        automapper
          .createMap(Role_GetAllOutputDTO, Group)
          .forMember('id', (opts: AutoMapperJs.IMemberConfigurationOptions) => opts.mapFrom('roleId'))
          .forMember('name', (opts: AutoMapperJs.IMemberConfigurationOptions) => opts.mapFrom('roleName'))
          .forMember('memberCount', (opts: AutoMapperJs.IMemberConfigurationOptions) => opts.mapFrom('memberCount'))
          .forMember('isSystem', function (opts) { opts.mapFrom('isSystem'); })
          .forMember('status', function (opts) { opts.mapFrom('status'); })
          .forMember('description', function (opts) { opts.mapFrom('description'); })
          .forMember('createdOn', function (opts) { opts.mapFrom('createdOn'); })
          .forMember('createdBy', function (opts) { opts.mapFrom('createdBy'); })
          .forMember('modifiedOn', function (opts) { opts.mapFrom('modifiedOn'); })
          .forMember('modifiedBy', function (opts) { opts.mapFrom('modifiedBy'); })

        let _response = automapper.map(Role_GetAllOutputDTO, Group, response);
        return _response;
      })
    );
  }

  //#region Permissions

  getPermissions(groupId: number): Observable<Permission[]> {
    const requestUri = environment.api.baseUrl + `/v1/roles/${groupId}/permissions`;

    return this.httpClient.get<Role_GetPermissionsByIdOutputDTO[]>(requestUri).pipe(map(
      response => {
        automapper
          .createMap(Role_GetPermissionsByIdOutputDTO, Permission)
          .forMember('id', (opts: AutoMapperJs.IMemberConfigurationOptions) => opts.mapFrom('permissionId'))
          .forMember('name', (opts: AutoMapperJs.IMemberConfigurationOptions) => opts.mapFrom('permissionDescription'))
          .forMember('status', function (opts) { opts.mapFrom('status'); })
          .forMember('createdOn', function (opts) { opts.mapFrom('createdOn'); })
          .forMember('createdBy', function (opts) { opts.mapFrom('createdBy'); })
          .forMember('modifiedOn', function (opts) { opts.mapFrom('modifiedOn'); })
          .forMember('modifiedBy', function (opts) { opts.mapFrom('modifiedBy'); })

        let permissions: Permission[] = automapper.map(Role_GetPermissionsByIdOutputDTO, Permission, response);
        return permissions;
      })
    );
  }

  updatePermissions(groupId: number, payload: string[]) {
    const requestUri = environment.api.baseUrl + `/v1/roles/${groupId}/permissions`;
    const request = new Role_InsertPermissionInputDTO();
    request.PermissionIds = payload;

    return this.httpClient.post(requestUri, request);
  }

  //#endregion

  //#region Members

  getMembers(groupId: number): Observable<User[]> {
    const requestUri = environment.api.baseUrl + `/v1/roles/${groupId}/members`;

    return this.httpClient.get<Role_GetMembersByIdOutputDTO[]>(requestUri).pipe(map(
      response => {
        automapper
          .createMap(Role_GetMembersByIdOutputDTO, User)
          .forMember('id', function (opts) { opts.mapFrom('userId'); })
          .forMember('userName', function (opts) { opts.mapFrom('userName'); })
          .forMember('emailAddress', function (opts) { opts.mapFrom('emailAddress'); })
          .forMember('firstName', function (opts) { opts.mapFrom('firstName'); })
          .forMember('lastName', function (opts) { opts.mapFrom('lastName'); })
          .forMember('displayName', function (opts) { opts.mapFrom('displayName'); })
          .forMember('roleNames', function (opts) { opts.mapFrom('roleNames'); })
          .forMember('status', function (opts) { opts.mapFrom('status'); })
          .forMember('statusName', function (opts) { opts.mapFrom('statusName'); })
          .forMember('createdOn', function (opts) { opts.mapFrom('createdOn'); })
          .forMember('createdBy', function (opts) { opts.mapFrom('createdBy'); })
          .forMember('modifiedOn', function (opts) { opts.mapFrom('modifiedOn'); })
          .forMember('modifiedBy', function (opts) { opts.mapFrom('modifiedBy'); })


        let _response = automapper.map(Role_GetMembersByIdOutputDTO, User, response);

        return _response;

      })
    );
  }

  addMembers(groupId: number, payload: number[]) {
    const requestUri = environment.api.baseUrl + `/v1/roles/${groupId}/members`;
    const request = new Role_InsertMembersInputDTO();
    request.UserIds = payload;

    return this.httpClient.post(requestUri, request);
  }

  removeMembers(groupId: number, payload: number[]) {
    const requestUri = environment.api.baseUrl + `/v1/roles/${groupId}/members`;
    const request = new Role_InsertMembersInputDTO();
    request.UserIds = payload;
    const httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
      body: request
    };

    return this.httpClient.delete(requestUri, httpOptions);
  }

  //#endregion

  //#region Folders

  getFolders(groupid: number): Observable<Directory[]> {
    var requestUri = environment.api.baseUrl + `/v1/roles/${groupid}/directories`;
    return this.httpClient.get<Role_GetDirectoriesByIdOutputDTO[]>(requestUri)
      .pipe(
        map(response => {
          automapper
            .createMap(Role_GetDirectoriesByIdOutputDTO, Directory)
            .forMember('id', function (opts) { opts.mapFrom('directoryId'); })
            .forMember('name', function (opts) { opts.mapFrom('directoryName'); })
            .forMember('parentId', function (opts) { opts.mapFrom('directoryParentId'); })
            .forMember('hasChild', function (opts) { opts.mapFrom('hasChild'); })

          let folders: Directory[] = automapper.map(Role_GetDirectoriesByIdOutputDTO, Directory, response);
          return folders;
        })
      );
  }

  updateFolders(groupid: number, payload: number[]) {
    const requestUri = environment.api.baseUrl + `/v1/roles/${groupid}/directories`;
    const request = new Role_InsertMediaAccessInputDTO();
    request.DirectoryIds = payload;

    return this.httpClient.post(requestUri, request);
  }

  //#endregion
}
