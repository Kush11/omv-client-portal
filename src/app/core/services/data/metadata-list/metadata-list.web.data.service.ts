import { Injectable } from "@angular/core";
import { Observable } from 'rxjs/internal/Observable';
import { MetadataListDataService } from './metadata-list.data.service';
import { HttpClient } from '@angular/common/http';
import { MetadataList } from 'src/app/core/models/entity/metadata-list';
import { environment } from 'src/environments/environment';
import { MetadataList_GetAllOutputDTO } from 'src/app/core/dtos/output/metadata/MetadataList_GetAllOutputDTO';
import { map } from 'rxjs/internal/operators/map';
import { MetadataList_GetByIdOutputDTO } from 'src/app/core/dtos/output/metadata/MetadataList_GetByIdOutputDTO';
import { MetadataList_InsertInputDTO } from 'src/app/core/dtos/input/metadata/MetadataList_InsertInputDTO';
import { MetadataListInputDTO } from 'src/app/core/dtos/input/metadata/MetadataListInputDTO';
import { MetadataListItem } from 'src/app/core/models/entity/metadata-list-item';
import { MetadataListItemInputDTO } from 'src/app/core/dtos/input/metadata/MetadataListItemInputDTO';
import { MetadataListItem_GetByIdOutputDTO } from 'src/app/core/dtos/output/metadata/MetadataListItem_GetByIdOutputDTO';

@Injectable({
  providedIn: 'root'
})
export class MetadataListWebDataService implements MetadataListDataService {

  baseUrl = environment.api.baseUrl;

  constructor(private httpClient: HttpClient) { }

  getMetadataLists(): Observable<MetadataList[]> {
    const requestUri = environment.api.baseUrl + `/v1/metadatalists`;

    return this.httpClient.get<MetadataList_GetAllOutputDTO[]>(requestUri)
      .pipe(
        map(
          response => {
            automapper
              .createMap(MetadataList_GetAllOutputDTO, MetadataList)
              .forMember('id', function (opts) { opts.mapFrom('metadataListId'); })
              .forMember('name', function (opts) { opts.mapFrom('metadataListName'); })
              .forMember('status', function (opts) { opts.mapFrom('status'); })
              .forMember('parentListId', function (opts) { opts.mapFrom('parentListId'); })
              .forMember('statusName', function (opts) { opts.mapFrom('statusName'); });
            let metadataLists: MetadataList[] = automapper.map(MetadataList_GetAllOutputDTO, MetadataList, response);
            return metadataLists;
          })
      );
  }

  getMetadataList(id: number): Observable<MetadataList> {
    const requestUri = environment.api.baseUrl + `/v1/metadatalists/${id}`;

    return this.httpClient.get<MetadataList_GetByIdOutputDTO>(requestUri).pipe(
      map(response => {
        automapper
          .createMap(MetadataList_GetByIdOutputDTO, MetadataList)
          .forMember('id', function (opts) { opts.mapFrom('metadataListId'); })
          .forMember('name', function (opts) { opts.mapFrom('metadataListName'); })
          .forMember('status', function (opts) { opts.mapFrom('status'); })
          .forMember('statusName', function (opts) { opts.mapFrom('statusName'); })
          .forMember('parentListId', function (opts) { opts.mapFrom('parentListId'); })
          .forMember('parentListName', function (opts) { opts.mapFrom('parentListName'); });
        let metadataList: MetadataList = automapper.map(MetadataList_GetByIdOutputDTO, MetadataList, response);
        return metadataList;
      })
    );
  }

  createMetadataList(payload: MetadataList): Observable<MetadataList> {
    const requestUri = environment.api.baseUrl + `/v1/metadatalists`;

    automapper
      .createMap(MetadataList, MetadataList_InsertInputDTO)
      .forMember('metadataListId', function (opts) { opts.mapFrom('id'); })
      .forMember('metadataListName', function (opts) { opts.mapFrom('fieldName'); })
      .forMember('parentListId', function (opts) { opts.mapFrom('parentListId'); })
      .forMember('status', function (opts) { opts.mapFrom('status'); });

    const request = automapper.map(MetadataList, MetadataList_InsertInputDTO, payload);

    return this.httpClient.post(requestUri, request)
      .pipe(
        map(
          response => {
            automapper
              .createMap(MetadataList_GetAllOutputDTO, MetadataList)
              .forMember('id', (opts: AutoMapperJs.IMemberConfigurationOptions) => opts.mapFrom('metadataListId'))
              .forMember('name', (opts: AutoMapperJs.IMemberConfigurationOptions) => opts.mapFrom('metadataListName'))
              .forMember('status', function (opts) { opts.mapFrom('status'); })
              .forMember('statusName', function (opts) { opts.mapFrom('statusName'); });

            let _response = automapper.map(MetadataList_GetAllOutputDTO, MetadataList, response);
            return _response;
          })
      );
  }

  updateMetadataList(id: number, payload: MetadataList) {
    const requestUri = environment.api.baseUrl + `/v1/metadatalists/${id}`;

    automapper
      .createMap(MetadataList, MetadataListInputDTO)
      .forMember('metadataListId', function (opts) { opts.mapFrom('id'); })
      .forMember('metadataListName', function (opts) { opts.mapFrom('name'); })
      .forMember('status', function (opts) { opts.mapFrom('status'); });

    const request = automapper.map(MetadataList, MetadataListInputDTO, payload);
    console.log('AdminMediaWebDataService - updateMetadataList: ', request);

    return this.httpClient.put(requestUri, request).pipe(map(
      response => {
        automapper
          .createMap(MetadataListInputDTO, MetadataList)
          .forMember('id', (opts: AutoMapperJs.IMemberConfigurationOptions) => opts.mapFrom('MetadataListId'))
          .forMember('name', (opts: AutoMapperJs.IMemberConfigurationOptions) => opts.mapFrom('MetadataListName'))
          .forMember('status', function (opts) { opts.mapFrom('status'); });

        let list = automapper.map(MetadataListInputDTO, MetadataList, response);
        return list;
      }
    ));
  }

  removeMetadataList(id: number) {
    const requestUri = environment.api.baseUrl + `/v1/metadatalists/${id}`;
    return this.httpClient.delete(requestUri);
  }

  //#region Metadata List Items


  getMetadataListItems(id: number): Observable<MetadataListItem[]> {
    const requestUri = this.baseUrl + `/v1/metadatalists/${id}/listitems`;

    return this.httpClient.get<MetadataListItem_GetByIdOutputDTO[]>(requestUri)
      .pipe(
        map(response => {
          automapper
            .createMap(MetadataListItem_GetByIdOutputDTO, MetadataListItem)
            .forMember('id', function (opts) { opts.mapFrom('metadataListItemId'); })
            .forMember('listId', function (opts) { opts.mapFrom('metadataListId'); })
            .forMember('value', function (opts) { opts.mapFrom('itemValue'); })
            .forMember('description', function (opts) { opts.mapFrom('itemDescription'); })
            .forMember('sort', function (opts) { opts.mapFrom('itemSort'); })
            .forMember('parentId', function (opts) { opts.mapFrom('parentItemId'); })
            .forMember('parentName', function (opts) { opts.mapFrom('parentItemDescription'); })
            .forMember('status', function (opts) { opts.mapFrom('statusName'); });

          let items: MetadataListItem[] = automapper.map(MetadataListItem_GetByIdOutputDTO, MetadataListItem, response);
          return items;
        })
      );
  }

  createMetadataListItem(id: number, payload: MetadataListItem): Observable<MetadataListItem> {
    var requestUri = environment.api.baseUrl + `/v1/metadatalists/${id}/listitems`;

    automapper
      .createMap(MetadataListItem, MetadataListItemInputDTO)
      .forMember('metadataListItemId', function (opts) { opts.mapFrom('id'); })
      .forMember('metadataListId', function (opts) { opts.mapFrom('listId'); })
      .forMember('itemDescription', function (opts) { opts.mapFrom('description'); })
      .forMember('itemValue', function (opts) { opts.mapFrom('value') })
      .forMember('itemSort', function (opts) { opts.mapFrom('sort') })
      .forMember('parentItemId', function (opts) { opts.mapFrom('parentId') })
      .forMember('status', function (opts) { opts.mapFrom('status') });

    const request = automapper.map(MetadataListItem, MetadataListItemInputDTO, payload);

    return this.httpClient.post(requestUri, request)
      .pipe(
        map(response => {
          automapper
            .createMap(MetadataListItemInputDTO, MetadataListItem)
            .forMember('metadataListItemId', (opts: AutoMapperJs.IMemberConfigurationOptions) => opts.mapFrom('metadataListItemId'))
            .forMember('metadataListId', (opts: AutoMapperJs.IMemberConfigurationOptions) => opts.mapFrom('metadataListId'))
            .forMember('itemDescription', function (opts) { opts.mapFrom('itemDescription'); })
            .forMember('itemValue', function (opts) { opts.mapFrom('itemValue'); })
            .forMember('itemSort', function (opts) { opts.mapFrom('itemSort') })
            .forMember('parentId', function (opts) { opts.mapFrom('parentItemValue') })
            .forMember('status', function (opts) { opts.mapFrom('status') })
            .forMember('statusName', function (opts) { opts.mapFrom('statusName') });

          let item = automapper.map(MetadataListItemInputDTO, MetadataListItem, response);
          return item;
        })
      );
  }

  //#endregion
}