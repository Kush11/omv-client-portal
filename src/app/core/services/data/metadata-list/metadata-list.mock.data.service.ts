import { Injectable } from "@angular/core";
import { MetadataListDataService } from './metadata-list.data.service';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { MetadataList } from 'src/app/core/models/entity/metadata-list';
import { Observable } from 'rxjs/internal/Observable';
import { MetadataListItem } from 'src/app/core/models/entity/metadata-list-item';

@Injectable({
  providedIn: 'root'
})
export class MetadataListMockDataService implements MetadataListDataService {
  getMetadataListItems(id: number): Observable<MetadataListItem[]> {
    throw new Error("Method not implemented.");
  }

  constructor(private httpClient: HttpClient) { }
  
  getMetadataLists(): Observable<MetadataList[]> {
    throw new Error("Method not implemented.");
  }
  getMetadataList(id: number): Observable<MetadataList> {
    throw new Error("Method not implemented.");
  }
  createMetadataList(payload: MetadataList): Observable<MetadataList> {
    throw new Error("Method not implemented.");
  }
  updateMetadataList(id: number, payload: MetadataList) {
    throw new Error("Method not implemented.");
  }
  removeMetadataList(id: number) {
    throw new Error("Method not implemented.");
  }
  
  createMetadataListItem(id: number, payload: MetadataListItem): Observable<MetadataListItem> {
    throw new Error("Method not implemented.");
  }
}