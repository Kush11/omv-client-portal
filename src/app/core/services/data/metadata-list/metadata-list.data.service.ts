import { Injectable } from "@angular/core";
import { Observable } from 'rxjs/internal/Observable';
import { MetadataList } from 'src/app/core/models/entity/metadata-list';
import { MetadataListItem } from 'src/app/core/models/entity/metadata-list-item';

@Injectable({
  providedIn: 'root'
})
export abstract class MetadataListDataService {

  abstract getMetadataLists(): Observable<MetadataList[]>;
  abstract getMetadataList(id: number): Observable<MetadataList>;
  abstract createMetadataList(payload: MetadataList): Observable<MetadataList>;
  abstract updateMetadataList(id: number, payload: MetadataList);
  abstract removeMetadataList(id: number);
  
  abstract getMetadataListItems(id: number): Observable<MetadataListItem[]>;
  abstract createMetadataListItem(id: number, payload: MetadataListItem): Observable<MetadataListItem>;
}