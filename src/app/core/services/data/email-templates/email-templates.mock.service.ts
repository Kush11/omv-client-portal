import { Injectable } from '@angular/core';
import { EmailTemplatesDataService } from './email-templates.data.service';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/internal/Observable';
import { EmailTemplate } from 'src/app/core/models/entity/email-templates';

@Injectable({
  providedIn: 'root'
})

export class EmailTemplatesMockDataService implements EmailTemplatesDataService {

  constructor(private httpClient: HttpClient) { }

  getEmailTemplates(): Observable<EmailTemplate[]> {
    throw new Error("Method not implemented.");
  }
  getEmailTemplate(id: string): Observable<EmailTemplate> {
    throw new Error("Method not implemented.");
  }
  createEmailTemplate(payload: EmailTemplate): Observable<string> {
    throw new Error("Method not implemented.");
  }
  updateEmailTemplate(id: string, payload: EmailTemplate) {
    throw new Error("Method not implemented.");
  }
  removeEmailTemplate(id: string) {
    throw new Error("Method not implemented.");
  }
}