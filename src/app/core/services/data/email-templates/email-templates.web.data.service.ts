import { Injectable } from '@angular/core';
import { EmailTemplatesDataService } from './email-templates.data.service';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/internal/Observable';
import { EmailTemplate } from 'src/app/core/models/entity/email-templates';
import { environment } from 'src/environments/environment';
import { MailTemplate_GetAllOutputDTO } from 'src/app/core/dtos/output/email/MailTemplate_GetAllOutputDTO';
import { MailTemplate_InsertInputDTO } from 'src/app/core/dtos/input/email/MailTemplate_InsertInputDTO';
import { MailTemplate_GetByIdOutputDTO } from 'src/app/core/dtos/output/email/MailTemplate_GetByIdOutputDTO';
import { MailTemplate_UpdateInputDTO } from 'src/app/core/dtos/input/email/MailTemplate_UpdateInputDTO';
import { map } from 'rxjs/internal/operators/map';
import { DateService } from '../../business/dates/date.service';

@Injectable({
	providedIn: 'root'
})
export class EmailTemplatesWebDataService implements EmailTemplatesDataService {

	baseUrl = environment.api.baseUrl;

	constructor(private httpClient: HttpClient, private dateService: DateService) { }

	getEmailTemplates(): Observable<EmailTemplate[]> {
		const requestUri = environment.api.baseUrl + `/v1/mailtemplates`;

		return this.httpClient.get<MailTemplate_GetAllOutputDTO[]>(requestUri)
			.pipe(
				map(response => {
					automapper
						.createMap(MailTemplate_GetAllOutputDTO, EmailTemplate)
						.forMember('id', function (opts) { opts.mapFrom('templateCode'); })
						.forMember('name', function (opts) { opts.mapFrom('templateName'); })
						.forMember('subject', function (opts) { opts.mapFrom('templateSubject'); })
						.forMember('body', function (opts) { opts.mapFrom('templateBody'); })
						.forMember('isBodyHTML', function (opts) { opts.mapFrom('isBodyHTML'); })
						.forMember('isSystem', function (opts) { opts.mapFrom('isSystem'); })
						.forMember('priority', function (opts) { opts.mapFrom('priority'); })
						.forMember('createdOn', function (opts) { opts.mapFrom('createdOn'); })
						.forMember('createdBy', function (opts) { opts.mapFrom('createdBy'); })
						.forMember('modifiedOn', function (opts) { opts.mapFrom('modifiedOn'); })
						.forMember('modifiedBy', function (opts) { opts.mapFrom('modifiedBy'); });
					let emailTemplates: EmailTemplate[] = automapper.map(MailTemplate_GetAllOutputDTO, EmailTemplate, response);
					emailTemplates.forEach(template => template.modifiedOnString = this.dateService.formatToString(template.modifiedOn));
					emailTemplates.sort((a, b) => new Date(b.modifiedOn).getTime() - new Date(a.modifiedOn).getTime());
					return emailTemplates;
				})
			);
	}

	getEmailTemplate(id: string): Observable<EmailTemplate> {
		const requestUri = environment.api.baseUrl + `/v1/mailtemplates/${id}`;

		return this.httpClient.get<EmailTemplate>(requestUri)
			.pipe(
				map(response => {
					automapper
						.createMap(MailTemplate_GetByIdOutputDTO, EmailTemplate)
						.forMember('id', function (opts) { opts.mapFrom('templateCode'); })
						.forMember('name', function (opts) { opts.mapFrom('templateName'); })
						.forMember('subject', function (opts) { opts.mapFrom('templateSubject'); })
						.forMember('body', function (opts) { opts.mapFrom('templateBody'); })
						.forMember('isBodyHTML', function (opts) { opts.mapFrom('isBodyHTML'); })
						.forMember('isSystem', function (opts) { opts.mapFrom('isSystem'); })
						.forMember('priority', function (opts) { opts.mapFrom('priority'); })
						.forMember('createdOn', function (opts) { opts.mapFrom('createdOn'); })
						.forMember('createdBy', function (opts) { opts.mapFrom('createdBy'); })
						.forMember('modifiedOn', function (opts) { opts.mapFrom('modifiedOn'); })
						.forMember('modifiedBy', function (opts) { opts.mapFrom('modifiedBy'); });
					let emailTemplates: EmailTemplate = automapper.map(MailTemplate_GetByIdOutputDTO, EmailTemplate, response);
					return emailTemplates;
				})
			);
	}

	createEmailTemplate(payload: EmailTemplate): Observable<string> {

		const requestUri = environment.api.baseUrl + `/v1/mailtemplates`;

		automapper
			.createMap(EmailTemplate, MailTemplate_InsertInputDTO)
			.forMember('templateCode', function (opts) { opts.mapFrom('id'); })
			.forMember('templateName', function (opts) { opts.mapFrom('name'); })
			.forMember('templateSubject', function (opts) { opts.mapFrom('subject'); })
			.forMember('templateBody', function (opts) { opts.mapFrom('body'); });
		const request = automapper.map(EmailTemplate, MailTemplate_InsertInputDTO, payload);


		return this.httpClient.post(requestUri, request)
			.pipe(
				map((response: string) => {
					return response;
				})
			);
	}

	updateEmailTemplate(id: string, payload: EmailTemplate) {
		const requestUri = environment.api.baseUrl + `/v1/mailtemplates/${id}`;

		automapper
			.createMap(EmailTemplate, MailTemplate_UpdateInputDTO)
			.forMember('templateCode', function (opts) { opts.mapFrom('id'); })
			.forMember('templateName', function (opts) { opts.mapFrom('name'); })
			.forMember('templateSubject', function (opts) { opts.mapFrom('subject'); })
			.forMember('templateBody', function (opts) { opts.mapFrom('body'); });
		const request = automapper.map(EmailTemplate, MailTemplate_UpdateInputDTO, payload);

		return this.httpClient.put(requestUri, request);
	}

	removeEmailTemplate(id: string) {
		const requestUri = environment.api.baseUrl + `/v1/mailtemplates/${id}`;
		return this.httpClient.delete(requestUri);
	}
}