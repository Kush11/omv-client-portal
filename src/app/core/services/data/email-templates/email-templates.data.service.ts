import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { EmailTemplate } from 'src/app/core/models/entity/email-templates';

@Injectable({
    providedIn: 'root'
})
export abstract class EmailTemplatesDataService {

    constructor() { }

    abstract getEmailTemplates(): Observable<EmailTemplate[]>;
    abstract getEmailTemplate(id: string): Observable<EmailTemplate>;
    abstract createEmailTemplate(payload: EmailTemplate): Observable<string>;
    abstract updateEmailTemplate(id: string, payload: EmailTemplate);
    abstract removeEmailTemplate(id: string);
}