import { Injectable } from "@angular/core";

@Injectable({
  providedIn: 'root'
})
export abstract class OktaDataService {

  constructor() { }

  abstract logout(): Promise<void>;
}