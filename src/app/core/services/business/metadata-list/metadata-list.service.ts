import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { MetadataListDataService } from '../../data/metadata-list/metadata-list.data.service';
import { MetadataList } from 'src/app/core/models/entity/metadata-list';
import { MetadataListItem } from 'src/app/core/models/entity/metadata-list-item';

@Injectable({
  providedIn: 'root'
})
export class MetadataListService {

  constructor(private metadataListDataService: MetadataListDataService) { }

  getMetadataLists(): Observable<MetadataList[]> {
    return this.metadataListDataService.getMetadataLists();
  }

  getMetadataList(id: number): Observable<MetadataList> {
    return this.metadataListDataService.getMetadataList(id);
  }

  createMetaDataList(payload: MetadataList): Observable<MetadataList> {
    return this.metadataListDataService.createMetadataList(payload);
  }

  updateMetadataList(id: number, payload: MetadataList) {
    return this.metadataListDataService.updateMetadataList(id, payload);
  }

  enableMetadataList(list: MetadataList) {
    list = { ...list, status: 1 };
    return this.updateMetadataList(list.id, list).toPromise();
  }

  disableMetadataList(list: MetadataList) {
    list = { ...list, status: 0 };
    return this.updateMetadataList(list.id, list).toPromise();
  }

  removeMetadataList(id: number) {
    return this.metadataListDataService.removeMetadataList(id);
  }

  //#region Metadata List Items

  getMetadataListItems(id: number): Observable<MetadataListItem[]> {
    return this.metadataListDataService.getMetadataListItems(id);
  }

  createMetaDataListItem(id: number, payload: MetadataListItem) {
    return this.metadataListDataService.createMetadataListItem(id, payload)
  }

  //#endregion
}
