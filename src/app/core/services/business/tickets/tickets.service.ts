import { Injectable } from '@angular/core';
import { Ticket } from 'src/app/core/models/entity/tickets';
import { Observable } from 'rxjs';
import { TicketsDataService } from '../../data/tickets/tickets.data.service';
import { Document } from 'src/app/core/models/entity/document';

@Injectable({
  providedIn: 'root'
})
export class TicketsService {

  constructor(private ticketsDataService: TicketsDataService) { }

  createReportIssue(payload: Ticket, files?: Document[]): Observable<Ticket> {
    return this.ticketsDataService.createReportIssue(payload, files);
  }
}
