import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { EmailTemplatesDataService } from '../../data/email-templates/email-templates.data.service';
import { EmailTemplate } from 'src/app/core/models/entity/email-templates';
import { tap } from 'rxjs/internal/operators/tap';

@Injectable({
  providedIn: 'root'
})
export class EmailTemplatesService {

  constructor(private emailTemplatesDataService: EmailTemplatesDataService) { }

  getEmailTemplates(): Observable<EmailTemplate[]> {
    return this.emailTemplatesDataService.getEmailTemplates()
      .pipe(
        tap(templates => {
          templates.forEach(template => {
            template.canDelete = !template.isSystem;
          });
        })
      );
  }

  getEmailTemplate(id: string): Observable<EmailTemplate> {
    return this.emailTemplatesDataService.getEmailTemplate(id);
  }

  createEmailTemplate(payload: EmailTemplate): Observable<string> {
    return this.emailTemplatesDataService.createEmailTemplate(payload);
  }

  updateEmailTemplate(id: string, payload: EmailTemplate) {
    return this.emailTemplatesDataService.updateEmailTemplate(id, payload);
  }

  removeEmailTemplate(id: string) {
    return this.emailTemplatesDataService.removeEmailTemplate(id);
  }
}
