import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { User, Users } from 'src/app/core/models/entity/user';
import { UsersDataService } from '../../data/users/users.data.service';
import { Group } from 'src/app/core/models/entity/group';
import { map } from 'rxjs/operators';
import { DateService } from '../dates/date.service';
import { Widget, UserWidget } from 'src/app/core/models/entity/widget';
import { HttpClient } from '@angular/common/http';
import templates from '../../../../../assets/mock/dashboard-templates.json';
import { Store } from '@ngxs/store';
import {
  GetAllWidgets
} from 'src/app/state/app.actions';

const COLORS = ['#0B2545', '#8E5572', '#EFCB68', '#725AC1', '#EF233C', '#DE6B48', '#8DA9C4', '#EF8275', '#0CCA4A',
  '#F17300', '#054A91', '#3E5641', '#A24936', '#D36135', '#DD9AC2', '#8DA9C4', '#732C2C', '#7E1F86', '#54428E',
  '#75755D'

  // '#ABD978', '#f8e71c', '#EAA9FB', '#A2AFFE', '#97DFFC', '#b8e986', '#D5BBB1', '#F79BA6', '#52BFFF', '#57E2E5',
  // '#ffba53', '#9CF739', '#ccb5d5', '#F7A581', '#9FCEE2', '#FDC527'
];

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  public allTemplates = templates;
  public dataTemplates = [];


  constructor(private usersDataService: UsersDataService, private dateService: DateService,
    private httpClient: HttpClient, protected store: Store) { }

  getUsers(name: string, groupId: number, status?: number, pageNumber?: number, pageSize?: number): Observable<Users> {
    return this.usersDataService.getUsers(name, groupId, status, pageNumber, pageSize)
      .pipe(
        map(response => {
          response.data.forEach(user => {
            if (user.groups) {
              var groups = user.groups.split(",");
              const length = groups.length;
              user.groups = length > 1 ? `${groups[0]} +${length - 1}` : groups[0];
            }
            user.modifiedOnString = this.dateService.formatToString(user.modifiedOn);
          });
          return response;
        })
      );
  }

  getActiveUsers(name: string, groupId: number, pageNumber?: number, pageSize?: number): Observable<Users> {
    return this.usersDataService.getUsers(name, groupId, 1, pageNumber, pageSize)
      .pipe(
        map(response => {
          response.data.forEach(user => {
            if (user.groups) {
              var groups = user.groups.split(",");
              const length = groups.length;
              user.groups = length > 1 ? `${groups[0]} +${length - 1}` : groups[0];
            }
            const randomNumber = Math.floor(Math.random() * (COLORS.length - 1) + 1);
            user.avatarText = `${user.firstName ? user.firstName[0] : ''}${user.lastName ? user.lastName[0] : ''}`.toUpperCase();
            user.avatarColor = COLORS[randomNumber];
            user.modifiedOnString = this.dateService.formatToString(user.modifiedOn);
          });
          return response;
        })
      );
  }

  getUsersForExport(name: string, groupId: number, status?: number, pageNumber?: number, pageSize?: number): Observable<Users> {
    return this.usersDataService.getUsers(name, groupId, status, pageNumber, pageSize)
      .pipe(
        map(response => {
          response.data.forEach(user => {
            user.modifiedOnString = this.dateService.formatToString(user.modifiedOn);
          });
          return response;
        })
      );
  }

  getUser(id: number): Observable<User> {
    return this.usersDataService.getUser(id);
  }

  createUser(payload: User) {
    return this.usersDataService.createUser(payload);
  }

  enableUser(user: User): Promise<User> {
    user = {
      ...user,
      status: 1
    };
    return this.usersDataService.updateUser(user.id, user).toPromise();
  }

  disableUser(user: User): Promise<User> {
    user = {
      ...user,
      status: 0
    };
    return this.usersDataService.updateUser(user.id, user).toPromise();
  }

  updateUser(id: number, payload: User): Observable<User> {
    return this.usersDataService.updateUser(id, payload);
  }

  updateGroups(userid: number, payload: number[], isAddRoles = false) {
    return this.usersDataService.updateGroups(userid, payload, isAddRoles).toPromise();
  }

  getGroups(userid: number): Observable<Group[]> {
    return this.usersDataService.getGroups(userid);
  }

  getLoggedInUser(): Observable<User> {
    return this.usersDataService.getLoggedInUser();
  }

  //#region Dashboard
  getUserDashboardWidgets(): Observable<UserWidget[]> {
    return this.usersDataService.getUserDashboardWidgets()
      .pipe(
        map(sections => {
          sections.forEach(section => {
            section.id = 'Layout_' + section.widgetId;
            this.getUserAvailableWidgetData(section);
          });
          return sections;
        })
      );
  }

  updateUserDashboardWigets(panels: UserWidget[]): Observable<UserWidget[]> {
    return this.usersDataService.updateUserDashboardWidgets(panels)
      .pipe(
        map(sections => {
          sections.forEach(section => {
            section.id = 'Layout_' + section.widgetId;
            this.getUserAvailableWidgetData(section);
          });
          return sections;
        })
      );
  }

  private replaceUsersWidgetContent(section) {
    const contentString = section.content;
    const mapObj = {
      UNQUSERS: section.data == null || Object.keys(section.data).length == 0 ? 'N/A' : section.data.NumberofActiveUsers == null ? 'N/A' : section.data.NumberofActiveUsers,
      MOSTACTIVEUSER: section.data == null || Object.keys(section.data).length == 0 ? 'N/A' : section.data.MostFrequentUser == null ? 'N/A' : section.data.MostFrequentUser
    };
    section.content = contentString.replace(/UNQUSERS|MOSTACTIVEUSER/gi, (matched) => {
      return mapObj[matched];
    });
  }

  private replaceLiveStreamCameraWidgetContent(section) {
    const contentString = section.content;
    const mapObj = {
      No_Of_Streams: section.data == null || Object.keys(section.data).length == 0 ? 'N/A' : section.data.streamCount == null ? 'N/A' : section.data.streamCount,
      No_Of_Active_Streams: section.data == null || Object.keys(section.data).length == 0 ? 'N/A' : section.data.averageStreamCount == null ? 'N/A' : section.data.averageStreamCount
    };
    section.content = contentString.replace(/No_Of_Streams|No_Of_Active_Streams/gi, (matched) => {
      return mapObj[matched];
    }
    );
  }

  private getUserAvailableWidgetData(section) {
    switch (section.name) {
      case 'Live-Streams : Camera':
        this.replaceLiveStreamCameraWidgetContent(section);
        break;
      case 'User-Stats : All Time':
      case 'User-Stats : Current Month':
      case 'User-Stats : Last Month':
      case 'User-Stats : Current Year':
      case 'User-Stats : Last Year':
        this.replaceUsersWidgetContent(section);
        break;
      default:
        break;
    }
  }

  getAllWidgets(): Observable<Widget[]> {
    return this.usersDataService.getAllWidgets()
      .pipe(
        map(sections => {
          sections.forEach(section => {
            section.id = 'Layout_' + section.widgetId;
          });
          return sections;
        })
      );
  }
}
