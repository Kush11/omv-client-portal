import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { CollaborationRoom, CollaborationSession } from 'src/app/core/models/entity/collaboration';
import { CollaborationsDataService } from '../../data/collaborations/collaborations.data.service';
import { map } from 'rxjs/internal/operators/map';
import { MediaItem } from 'src/app/core/models/entity/media';
import { Users } from 'src/app/core/models/entity/user';
import { LiveStreams } from 'src/app/core/models/entity/live-stream';
import { tap } from 'rxjs/internal/operators/tap';
import { DateService } from '../dates/date.service';
import { Status } from 'src/app/core/enum/status.enum';

@Injectable({
  providedIn: 'root'
})
export class CollaborationsService {

  constructor(private collaborationDataService: CollaborationsDataService, private dateService: DateService) { }

  getCollaborationDays(dateFilter: Date, query?: string): Observable<{ days: number[], upcomingSessionsCount: number }> {
    const year = dateFilter.getFullYear();
    const month = dateFilter.getMonth() + 1;
    return this.collaborationDataService.getCollaborations(year, month, query)
      .pipe(
        map(sessions => {
          let retVal: { days: number[], upcomingSessionsCount: number } = { days: [], upcomingSessionsCount: 0 };
          const today = this.dateService.fromDateToUTCDate(new Date());
          sessions.forEach(d => {
            const date = this.dateService.fromDateToUTCDate(d.date);
            retVal.days = [...retVal.days, date.getDate()];
            d.date = this.dateService.mergeDateAndTime(date, d.startTimeString);
          });
          retVal.upcomingSessionsCount = sessions.filter(d => d.date >= today &&
            d.date.getMonth() === today.getMonth() &&
            d.status === Status.Pending).length;
          return retVal;
        })
      );
  }

  getCollaborationSessions(dateFilter: Date, query?: string): Observable<CollaborationSession[]> {
    const year = dateFilter.getFullYear();
    const month = dateFilter.getMonth() + 1;
    const day = dateFilter.getDate();
    return this.collaborationDataService.getCurrentCollaborations(year, month, day, query)
      .pipe(
        tap(sessions => {
          sessions.forEach(session => {
            const now = this.dateService.fromDateToUTCDate(new Date());
            let startTime = this.dateService.mergeDateAndTime(dateFilter, session.startTimeString, true);
            let endTime = this.dateService.mergeDateAndTime(dateFilter, session.endTimeString, true);
            startTime = this.dateService.fromDateToUTCDate(this.dateService.addMinutes(startTime, -15));
            endTime = this.dateService.fromDateToUTCDate(this.dateService.addMinutes(endTime, 15));
            // if start time - 15 minutes is greater than now and end time - 15 minutes is less than now;
            session.canJoin = startTime <= now && endTime >= now && (session.status === Status.InProgress || session.status === Status.Pending);
            session.canEdit = session.status === Status.InProgress || session.status === Status.Pending;
            session.isPast = endTime <= now;
            session.participantNames = session.participants.map(x => x.displayName).join(', ');
            session.day = day;
            session.month = month;
            session.participants.forEach(participant => {
              if (participant.groups) {
                const groups: string[] = [...participant.groups];
                const length = groups.length;
                participant.groups = length > 1 ? `${groups[0]} +${length - 1}` : groups[0];
              }
            })
          });
          return sessions;
        })
      );
  }

  getCollaborationSession(id: number): Observable<CollaborationSession> {
    return this.collaborationDataService.getCollaborationSession(id)
      .pipe(
        tap(session => {
          session.startTime = this.dateService.toDate(session.startTimeString, 'hh:mm a');
          session.endTime = this.dateService.toDate(session.endTimeString, 'hh:mm a');
          return session;
        })
      );
  }

  async isSessionEnded(id: number): Promise<boolean> {
    const session = await this.collaborationDataService.getCollaborationSession(id).toPromise();
    return session.status === Status.Processing || session.status === Status.Completed;
  }

  removeSession(id: number) {
    return this.collaborationDataService.removeSession(id).toPromise();
  }

  getCollaborationItems(): Observable<LiveStreams> {
    return this.collaborationDataService.getCollaborationItems();
  }

  validateSessionName(name: string): Promise<boolean> {
    return this.collaborationDataService.validateSessionName(name).toPromise();
  }

  getParticipants(name: string, groupid: number, status?: number, pageNumber?: number, pageSize?: number): Observable<Users> {
    return this.collaborationDataService.getParticipants(name, groupid, status, pageNumber, pageSize)
      .pipe(
        map(response => {
          response.data.forEach(user => {
            const groups = user.groups.split(',');
            user.roleNames = groups;
          });
          return response;
        })
      );
  }

  updateSessionParticipants(sessionId: number, participantIds: number[]) {
    return this.collaborationDataService.updateSessionParticipants(sessionId, participantIds);
  }

  createSession(session: CollaborationSession) {
    return this.collaborationDataService.createSession(session);
  }

  updateSession(session: CollaborationSession) {
    return this.collaborationDataService.updateSession(session);
  }

  getCollaborationArchiveItems(): Observable<MediaItem[]> {
    return this.collaborationDataService.getCollaborationArchiveItems();
  }

  join(id: number): Observable<CollaborationRoom> {
    return this.collaborationDataService.join(id);
  }

  leaveRoom(id: number) {
    return this.collaborationDataService.leaveRoom(id);
  }

  createScreenshot(id: number, payload: MediaItem) {
    return this.collaborationDataService.createScreenshot(id, payload).toPromise();
  }

  getScreenshots(id: number) {
    return this.collaborationDataService.getScreenshots(id).toPromise();
  }

  deleteScreenshot(collaborationId: number, id: number) {
    return this.collaborationDataService.deleteScreenshot(collaborationId, id).toPromise();
  }
}
