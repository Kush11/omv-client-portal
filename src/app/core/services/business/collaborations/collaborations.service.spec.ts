import { TestBed } from '@angular/core/testing';

import { CollaborationsService } from './collaborations.service';

describe('CollaborationsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CollaborationsService = TestBed.get(CollaborationsService);
    expect(service).toBeTruthy();
  });
});
