import { Group } from '../../../models/entity/group';
import { Injectable } from '@angular/core';
import { GroupsDataService } from '../../data/groups/groups.data.service';
import { Observable } from 'rxjs/internal/Observable';
import { Permission } from 'src/app/core/enum/permission';
import { User } from 'src/app/core/models/entity/user';

@Injectable({
  providedIn: 'root'
})
export class GroupsService {

  constructor(private groupsDataService: GroupsDataService) { }

  //#region Groups
  
  getGroups(status?: number, pageNumber?: number, pageSize?: number): Observable<Group[]> {
    return this.groupsDataService.getGroups();
  }

  getGroup(id: number): Observable<Group> {
    return this.groupsDataService.getGroup(id);
  }

  createGroup(payload: Group): Observable<Group> {
    return this.groupsDataService.createGroup(payload);
  }

  updatePermissions(groupId:number, payload:string[]){
    return this.groupsDataService.updatePermissions(groupId,payload).toPromise();
  }

  updateGroup(id: number, payload: Group) {
    return this.groupsDataService.updateGroup(id, payload);
  }

  enableGroup(group: Group): Promise<Group> {
    group = {
      ...group,
      status: 1
    };
    return this.groupsDataService.updateGroup(group.id, group).toPromise();
  }

  disableGroup(group: Group): Promise<Group> {
    group = {
      ...group,
      status: 0
    };
    return this.groupsDataService.updateGroup(group.id, group).toPromise();
  }

  //#endregion

  //#region Permissions

  getGroupPermissions(groupId: number): Observable<Permission[]> {
    return this.groupsDataService.getPermissions(groupId);
  }

  updateGroupPermissions(groupId: number, payload: string[]) {
    return this.groupsDataService.updatePermissions(groupId, payload);
  }

  //#endregion

  //#region Members

  getGroupMembers(groupId: number): Observable<User[]> {
    return this.groupsDataService.getMembers(groupId);
  }

  addGroupMembers(groupId: number, payload: number[]) {
    return this.groupsDataService.addMembers(groupId, payload);
  }

  removeGroupMembers(groupId: number, payload: number[]) {
    return this.groupsDataService.removeMembers(groupId, payload);
  }

  //#endregion

  //#region Folders

  getFolders(id: number) {
    return this.groupsDataService.getFolders(id);
  }

  updateFolders(id: number, payload: number[]) {
    return this.groupsDataService.updateFolders(id, payload);
  }

  //#endregion
}
