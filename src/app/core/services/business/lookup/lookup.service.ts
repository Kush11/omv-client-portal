import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Lookup } from 'src/app/core/models/entity/lookup';
import { LookupDataService } from '../../data/lookup/lookup.data.service';

@Injectable({
  providedIn: 'root'
})
export class LookupService {

  constructor(private lookupDataService: LookupDataService) { }

  getCustomWorkSetFrequencyTypes(): Observable<Lookup[]> {
    return this.lookupDataService.getCustomWorkSetFrequencyTypes();
  }

  getWorkSetFrequencyTypes(): Observable<Lookup[]> {
    return this.lookupDataService.getWorkSetFrequencyTypes();
  }

  getMetadataModules(): Observable<Lookup[]> {
    return this.lookupDataService.getMetadataModules();
  }

  getLiveStreamAssets(): Observable<Lookup[]> {
    return this.lookupDataService.getLiveStreamAssets();
  }

  getLiveStreamGroups(): Observable<Lookup[]> {
    return this.lookupDataService.getLiveStreamGroups();
  }
  getEmailPlaceholderLookup(): Observable<Lookup[]> {
    return this.lookupDataService.getEmailPlaceholderLookup();
  }
}
