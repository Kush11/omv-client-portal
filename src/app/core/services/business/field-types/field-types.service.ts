import { Injectable } from '@angular/core';
import { FieldTypesDataService } from '../../data/field-types/field-types.data.service';
import { Observable } from 'rxjs/internal/Observable';
import { FieldType } from 'src/app/core/models/entity/field-type';
import { tap } from 'rxjs/internal/operators/tap';
import { MetadataFieldType } from 'src/app/core/enum/metadataFieldType';

@Injectable({
  providedIn: 'root'
})
export class FieldTypesService {

  constructor(private fieldTypesDataService: FieldTypesDataService) { }

  getFieldTypes(): Observable<FieldType[]> {
    return this.fieldTypesDataService.getFieldTypes()
      .pipe(
        tap(types => {
          types.forEach(fieldType => {
            switch (fieldType.type) {
              case MetadataFieldType.Text:
                fieldType.description = "Text";
                break;
              case MetadataFieldType.Date:
                fieldType.description = "Date Picker";
                break;
              case MetadataFieldType.Select:
                fieldType.description = "Dropdown";
                break;
              case MetadataFieldType.Nested:
                fieldType.description = "Nested Dropdown";
                break;
            }
          });
        })
      );
  }
}
