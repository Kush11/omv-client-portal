import { Injectable } from '@angular/core';
import { Lookup } from 'src/app/core/models/entity/lookup';
import * as automapper from 'automapper-ts';
import { ListItem } from 'src/app/core/models/entity/list-item';
import { SortType, SortDirection } from 'src/app/core/enum/sort-enum';

@Injectable({
  providedIn: 'root'
})
export class FieldsService {

  constructor() { }

  encode(data: string): string {
    return data.split(' ').join('_');
  }

  decode(data: string): string {
    return data.split('_').join(' ');
  }

  removeFileExtension(name: string): string {
    if (name) {
      var index = name.lastIndexOf('.');
      return name.slice(0, index);
    } else {
      return name;
    }
  }

  formatSize(size: number, decimals: number = 0): any {
    if (!size) return '0 KB';

    const k = 1024;
    const dm = decimals < 0 ? 0 : decimals;
    const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];

    const i = Math.floor(Math.log(size) / Math.log(k));

    return parseFloat((size / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
  }

  setPlaceholderByType(fileType: string): string {
    let placeholder: string;
    switch (fileType) {
      case 'PDF':
        placeholder = 'https://haywardgordon.com/wp-content/themes/HaywardGordon/assets/pdf-icon.jpg';
        break;
      case 'DOC':
      case 'DOCX':
      case 'XLS':
      case 'XLSX':
      case 'TXT':
      case 'CSV':
      case 'LOG':
        placeholder = 'https://vacanegra.com/wp-content/plugins/widgetkit/assets/images/file.svg';
        break;
      case 'TELEMETRY':
        placeholder = 'https://d2fni493fitngs.cloudfront.net/blog/wp-content/uploads/2017/04/28150617/multi-carousel-blog-planoly-cover1.png';
        break;
    }

    return placeholder;
  }

  sort(list: any[], prop: string, type: SortType = SortType.String, direction: SortDirection = SortDirection.Descending): any[] {
    let retVal: any[] = [];
    switch (type) {
      case SortType.Number:
        retVal = list.sort((a, b) => direction === SortDirection.Descending ? b[prop] - a[prop] : a[prop] - b[prop]);
        break;
      case SortType.String:
        retVal = list.sort((a, b) => (a.label.localeCompare(b.label)));
        break;
      case SortType.Date:
        retVal = list.sort((a, b) => new Date(b[prop]).getTime() - new Date(a[prop]).getTime())
        break;
      default:
        retVal = list.sort((a, b) => new Date(b[prop]).getTime() - new Date(a[prop]).getTime());
        break;
    }
    return retVal;
  }

  sortAscending(list: any[], prop: string) {
    return list.sort((a, b) => new Date(a[prop]).getTime() - new Date(b[prop]).getTime());
  }

  compareNumberArrays(firstArray: number[], secondArray: number[]): boolean {
    if (firstArray.length !== secondArray.length)
      return false;
    firstArray.sort((a, b) => (a - b));
    secondArray.sort((a, b) => (a - b));
    for (var i = firstArray.length; i--;) {
      if (firstArray[i] !== secondArray[i])
        return false;
    }
    return true;
  }

  compareStringArrays(firstArray: string[], secondArray: string[]): boolean {
    if (firstArray.length !== secondArray.length)
      return false;
    firstArray.sort((a, b) => (a.localeCompare(b)));
    secondArray.sort((a, b) => (a.localeCompare(b)));
    for (var i = firstArray.length; i--;) {
      if (firstArray[i] !== secondArray[i])
        return false;
    }
    return true;
  }

  convertLookupsToListItems(lookups: Lookup[]): ListItem[] {
    automapper
      .createMap(Lookup, ListItem)
      .forMember('value', function (opts) { opts.mapFrom('value'); })
      .forMember('description', function (opts) { opts.mapFrom('description'); })
      .forMember('sort', function (opts) { opts.mapFrom('sort'); });

    let listItems: ListItem[] = automapper.map(Lookup, ListItem, lookups);
    return listItems;
  }

  isValidJson(data: string) {
    try {
      JSON.parse(data);
    } catch (e) {
      return false;
    }
    return true;
  }

  getInitialsFromFullname(fullname: string) {
    const array = fullname.split(' ');
    return `${array[0][0]}${array[1][0]}`.toUpperCase();
  }
}
