import { Injectable } from '@angular/core';
import * as OktaAuth from '@okta/okta-auth-js';
import { Router } from '@angular/router';
import { CustomersDataService } from '../data/customers/customers.data.service';
import { OktaDataService } from '../data/okta/okta.service';
import { Store } from '@ngxs/store';
import { GetLoggedInUser, SaveCompanyLogo } from 'src/app/state/app.actions';

const OktaTokenStorageKey = 'okta-token-storage';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private _auth: OktaAuth = null;

  constructor(private router: Router, private customersDataService: CustomersDataService, private oktaDataService: OktaDataService, private store: Store) { }

  async isAuthenticated() {
    //ensure auth initialized
    if (!this._auth) {
      await this.load();
    }

    // Checks if there is a current accessToken in the TokenManger.
    let retVal = !!(await this._auth.tokenManager.get('accessToken'));
    return retVal;
  }

  async getAccessToken() {
    //ensure auth initialized
    if (!this._auth)
      return false;
    // Checks if there is a current accessToken in the TokenManger.
    let retVal = await this._auth.tokenManager.get('accessToken');

    if (retVal)
      return retVal.accessToken;
    else
      return null;
  }

  async login() {
    //ensure auth initialized
    if (!this._auth) {
      await this.load();
    }

    if (localStorage.getItem(OktaTokenStorageKey)) {
      localStorage.removeItem(OktaTokenStorageKey);
    }

    // Launches the login redirect.
    this._auth.token.getWithRedirect({
      responseType: ['id_token', 'token'],
      scopes: ['openid', 'email', 'profile']
    });
  }

  async handleAuthentication() {
    //ensure auth initialized
    if (!this._auth) {
      await this.load();
    }

    await this._auth.token.parseFromUrl()
      .then(tokens => {
        tokens.forEach(token => {
          if (token.idToken) {
            localStorage.setItem('id_Token', token.idToken);
            this._auth.tokenManager.add('idToken', token);
          }
          if (token.accessToken) {
            this._auth.tokenManager.add('accessToken', token);
          }
        });
      }, error => {
        console.log(error);
      });

    await this.isAuthenticated()
      .then(isAuthenticated => {
        if (isAuthenticated) {
          this.store.dispatch(new GetLoggedInUser()).toPromise()
            .then(() => {
              this.customersDataService.migrateDatabase();
              this.router.navigateByUrl('/authorize-check');
            });
        }
      });
  }

  async logout() {
    //ensure auth initialized
    if (!this._auth) {
      await this.load();
    }

    if (localStorage.getItem(OktaTokenStorageKey)) {
      localStorage.removeItem(OktaTokenStorageKey);
    }
    localStorage.removeItem('@@STATE');

    return await this.oktaDataService.logout();
  }

  async load(): Promise<OktaAuth> {
    const host = window.location.host.toLowerCase(); //this includes host and port - eg localhost:4200 or bp.omv.com
    let okta_security_config: any;

    if (!localStorage.getItem(host)) {
      let customer = await this.customersDataService.getByHostHeader(host).toPromise();
      okta_security_config = {
        issuerUrl: customer.issuerUrl,
        clientId: customer.clientId,
        authServerId: customer.authServerId
      };

      this.store.dispatch(new SaveCompanyLogo(customer.customerLogo));
      localStorage.setItem(host, JSON.stringify(okta_security_config));
    } else {
      okta_security_config = JSON.parse(localStorage.getItem(host));
    }

    this._auth = new OktaAuth({
      url: okta_security_config.issuerUrl,
      clientId: okta_security_config.clientId,
      issuer: `${okta_security_config.issuerUrl}/oauth2/${okta_security_config.authServerId}`,
      redirectUri: `${window.location.protocol}//${window.location.host.toLowerCase()}/implicit/callback`
    });
  }
}

