import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { MetadataField } from 'src/app/core/models/entity/metadata-field';
import { FiltersDataService } from '../../data/filters/filters.data.service';

@Injectable({
  providedIn: 'root'
})
export class FiltersService {

  constructor(private filtersDataService: FiltersDataService) { }

  getFilters(): Observable<MetadataField[]> {
    return this.filtersDataService.getFilters();
  }
}
