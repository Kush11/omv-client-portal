import { Injectable } from '@angular/core';
import { LiveStreamDataService } from '../../data/live-stream/live-stream-data.service';
import { LiveStreams, LiveStream } from 'src/app/core/models/entity/live-stream';
import { Observable } from 'rxjs';
import { Tag } from 'src/app/core/models/entity/tag';
import { map } from 'rxjs/internal/operators/map';

@Injectable({
  providedIn: 'root'
})
export class LiveStreamService {

  constructor(private liveStreamDataService: LiveStreamDataService) { }

  getLiveStreams(asset?: string, group?: string, pageNumber?: number, pageSize?: number, isTreeView?: boolean): Observable<LiveStreams> {
    return this.liveStreamDataService.getLiveStreams(asset, group, pageNumber, pageSize, isTreeView);
  }

  getLiveStream(payload: number): Observable<LiveStream> {
    return this.liveStreamDataService.getLiveStream(payload);
  }

  getPlaylist(payload: number[]): Observable<LiveStream[]> {
    return this.liveStreamDataService.getPlaylist(payload);
  }

  applyFilters(filters: Tag[], pageNumber?: number, pageSize?: number): Observable<LiveStreams> {
    return this.liveStreamDataService.applyFilters(filters, pageNumber, pageSize);
  }

  //#region Live Stream Favorites

  getLiveStreamFavorites(pageNumber?: number, pageSize?: number): Observable<LiveStreams> {
    return this.liveStreamDataService.getLiveStreamFavorites(pageNumber, pageSize);
  }

  addLiveStreamFavorite(liveStreamItemId: number, name?: string, livestreamIds?: number[]) {
    return this.liveStreamDataService.addLiveStreamFavorites(liveStreamItemId, name, livestreamIds);
  }

  removeLiveStreamFavorite(liveStreamItemId: number) {
    return this.liveStreamDataService.removeLiveStreamFavorites(liveStreamItemId);
  }


  //#endregion
}
