import { Injectable } from '@angular/core';
import { MetadataFieldsDataService } from '../../data/metadata-fields/metadata-fields.data.service';
import { Observable } from 'rxjs/internal/Observable';
import { MetadataField } from 'src/app/core/models/entity/metadata-field';
import { tap } from 'rxjs/internal/operators/tap';
import { MetadataDataType } from 'src/app/core/enum/metadata-data-type';
import { MetadataSearchType } from 'src/app/core/enum/metadata-search-type';
import { MetadataSearchTypes, MetadataDataTypes, Type, MetadataControlTypes } from 'src/app/admin/admin-metadata-fields/admin-metadata-fields.data';

@Injectable({
  providedIn: 'root'
})
export class MetadataFieldsService {

  constructor(private metadataFieldsDataService: MetadataFieldsDataService) { }

  getMetadataFields(module?: number): Observable<MetadataField[]> {
    return this.metadataFieldsDataService.getMetaDataFields(module)
      .pipe(
        tap(fields => {
          fields.sort((a, b) => (a.order - b.order));
          return fields;
        })
      );
  }

  getMediaMetadataFields(): Observable<MetadataField[]> {
    return this.metadataFieldsDataService.getMetaDataFields(1)
      .pipe(
        tap(fields => {
          fields.forEach(field => {
            const dataType = MetadataDataTypes.find(x => x.value === field.dataType);
            let searchType = MetadataSearchTypes.find(x => x.value === field.searchType);
            const controlType = MetadataControlTypes.find(x => x.value === field.type);
            if (dataType && searchType) {
              field.typeDescription = `${dataType.text} | ${searchType.text}`;
            } else if (dataType && !searchType) {
              field.typeDescription = dataType.text;
            }
            field.controlType = controlType ? controlType.text : '';
          });
          return fields.sort((a, b) => (a.order - b.order));;
        })
      );
  }

  getWorkPlanningMetadataFields(): Observable<MetadataField[]> {
    return this.metadataFieldsDataService.getMetaDataFields(2)
      .pipe(
        tap(fields => {
          fields.forEach(field => {
            const dataType = MetadataDataTypes.find(x => x.value === field.dataType);
            const controlType = MetadataControlTypes.find(x => x.value === field.type);
            field.typeDescription = dataType ? dataType.text : '';
            field.controlType = controlType ? controlType.text : '';
          });
          return fields.sort((a, b) => (a.order - b.order));;
        })
      );
  }

  createMetaDataField(payload: MetadataField) {
    return this.metadataFieldsDataService.createMetadataField(payload);
  }

  updateMetaDataField(id: number, payload: MetadataField) {
    return this.metadataFieldsDataService.updateMetaDataField(id, payload);
  }

  removeMetadataField(id: number): Observable<MetadataField[]> {
    return this.metadataFieldsDataService.removeMetadataField(id);
  }

  sortFields(payload: MetadataField[]) {
    return this.metadataFieldsDataService.sortFields(payload).toPromise();
  }
}
