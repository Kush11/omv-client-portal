import { Injectable } from '@angular/core';
import { RulesDataService } from '../../data/rules/rules.data.service';
import { Observable } from 'rxjs/internal/Observable';
import { Rule } from 'src/app/core/models/entity/rule';


@Injectable({
  providedIn: 'root'
})
export class RulesService {

  constructor(private rulesDataService: RulesDataService) { }

  getRules(): Observable<Rule[]> {
    return this.rulesDataService.getRules();
  }

  getRule(id: number): Observable<Rule> {
    return this.rulesDataService.getRule(id);
  }

  createRule(payload: Rule): Observable<number> {
    return this.rulesDataService.createRule(payload);
  }

  updateRule(id: number, payload: Rule) {
    return this.rulesDataService.updateRule(id, payload);
  }

  deleteRule(id: number) {
    return this.rulesDataService.deleteRule(id);
  }
}
