import { TestBed } from '@angular/core/testing';

import { WorkPlanningService } from './work-planning.service';

describe('WorkPlanningService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: WorkPlanningService = TestBed.get(WorkPlanningService);
    expect(service).toBeTruthy();
  });
});
