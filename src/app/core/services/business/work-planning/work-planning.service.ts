import { Injectable } from '@angular/core';
import { WorkPlanningDataService } from '../../data/work-planning/work-planning.data.service';
import { Observable } from 'rxjs/internal/Observable';
import { WorkPlanningParentItem, WorkPlanningWorkItemParent } from 'src/app/core/models/work-planning';
import { Work, Works } from 'src/app/core/models/entity/work';
import { tap } from 'rxjs/internal/operators/tap';
import { DateService } from '../dates/date.service';
import { WorkFilesGroup, WorkFile } from 'src/app/core/models/entity/work-item';
import { MetadataField } from 'src/app/core/models/entity/metadata-field';
import { map } from 'rxjs/internal/operators/map';
import { HttpEventType } from '@angular/common/http';
import { MetadataFieldType } from 'src/app/core/enum/metadataFieldType';
import { MetadataDataType } from 'src/app/core/enum/metadata-data-type';

@Injectable({
  providedIn: 'root'
})
export class WorkPlanningService {

  constructor(private dataService: WorkPlanningDataService, private dateService: DateService) { }

  getParentItems(): Observable<WorkPlanningParentItem[]> {
    return this.dataService.getParentItem();
  }

  //#region Works

  getWorks(assetId: number, dateFilter: Date, actionList?: boolean, cycle?: string, pageNumber?: number, pageSize?: number): Observable<Work[]> {
    const _dateFilter = this.dateService.toUTC(dateFilter);
    return this.dataService.getWorks(assetId, _dateFilter, actionList, cycle, pageNumber, pageSize)
      .pipe(
        tap(works => {
          works.forEach(work => {
            work.name = work.secondItemIdentifier ? `${work.firstItemIdentifier} - ${work.secondItemIdentifier}` : `${work.firstItemIdentifier || ''}`;
            work.routeLink = `${work.id}/form`;
            work.forms.forEach(form => {
              if (!form.value && form.isRequired) {
                work.disableAdvance = true;
              }
            });
          });
        })
      );
  }

  downloadReport(assetId: number, queryType: string, reportName: string, dateFilter?: Date, cycle?: string) {
    const date = this.dateService.toUTC(dateFilter);
    return this.dataService.downloadReport(assetId, queryType, date, cycle)
      .pipe(
        map(data => {
          switch(data.type) {
            case HttpEventType.DownloadProgress:
              break;
            case HttpEventType.Response:
              const downloadedFile = new Blob([data.body], { type: data.body.type });
              const a = document.createElement('a');
              a.setAttribute('style', 'display:none;');
              document.body.appendChild(a);
              a.download = `${reportName}`;
              a.href = URL.createObjectURL(downloadedFile);
              a.target = '_blank';
              a.click();
              document.body.removeChild(a);
              break;
          }
        })
      )
  }

  //#endregion

  //#region Work Planning Item

  getWorkItemGroup(name: string): Observable<WorkPlanningWorkItemParent[]> {
    return this.dataService.getWorkItemGroup(name)
      .pipe(
        tap(parent => {
          // parent.map(item => {
          //   item.routeLink = `${item.itemId}/works`;
          //   item.workSetPlanning.map((itm, i) => {
          //     item[`cycle${i}`] = item.workSetPlanning[i].workList;
          //   });
          // });
        })
      );
  }

  //#endregion

  //#region Forms

  getForms(workId: any): Observable<Work[]> {
    return this.dataService.getForms(workId)
      .pipe(
        tap(works => {
          works.forEach(work => {
            work.name = work.secondItemIdentifier ? `${work.firstItemIdentifier} - ${work.secondItemIdentifier}` : `${work.firstItemIdentifier || ''}`;
            const step: MetadataField = { name: 'step', label: 'Current Step', value: work.currentStepName };
            const cycle: MetadataField = { name: 'cycle', label: 'Cycle', value: work.cycleName };
            const cycleStartDate: MetadataField = {
              name: 'cycleStartDate', label: 'Cycle Start Date', type: MetadataFieldType.Date,
              value: work.workSetCycleStartDate
            };
            const cycleEndDate: MetadataField = {
              name: 'cycleEndDate', label: 'Cycle End Date', type: MetadataFieldType.Date,
              value: work.workSetCycleEndDate
            };
            work.metadata = [step, cycle, cycleStartDate, cycleEndDate];
            work.itemFields.forEach(field => {
              const itemField: MetadataField = {
                name: field.name, label: field.name,
                value: field.type === MetadataDataType.Date ? (field.value ? new Date(field.value) : null) : field.value
              };
              work.metadata = [...work.metadata, itemField];
            });
          });
        })
      );
  }

  updateForm(workId: number, formData: string) {
    return this.dataService.updateForm(workId, formData);
  }

  signOff(workId: number) {
    return this.dataService.signOff(workId).toPromise();
  }

  //#endregion

  createWorkFile(payload: WorkFile): Observable<any> {
    return this.dataService.createWorkFile(payload);
  }

  getWorkFile(workId: any): Observable<WorkFilesGroup[]> {
    return this.dataService.getWorkFile(workId);
  }

  //#region Findings

  getFindingFields(workId: number): Promise<MetadataField[]> {
    return this.dataService.getFindingFields(workId).toPromise();
  }

  getFindings(workId: number): Promise<Work[]> {
    return this.dataService.getFindings(workId)
      .pipe(
        tap(works => {
          works.forEach(work => {
            work.name = work.secondItemIdentifier ? `${work.firstItemIdentifier} - ${work.secondItemIdentifier} ` : `${work.firstItemIdentifier || ''} `;
            const step: MetadataField = { name: 'step', label: 'Current Step', value: work.currentStepName };
            const cycle: MetadataField = { name: 'cycle', label: 'Cycle', value: work.cycleName };
            const cycleStartDate: MetadataField = {
              name: 'cycleStartDate', label: 'Cycle Start Date', type: MetadataFieldType.Date,
              value: work.workSetCycleStartDate
            };
            const cycleEndDate: MetadataField = {
              name: 'cycleEndDate', label: 'Cycle End Date', type: MetadataFieldType.Date,
              value: work.workSetCycleEndDate
            };
            work.metadata = [step, cycle, cycleStartDate, cycleEndDate];
            work.itemFields.forEach(field => {
              const itemField: MetadataField = {
                name: field.name, label: field.name,
                value: field.type === MetadataDataType.Date ? (field.value ? new Date(field.value) : null) : field.value
              };
              work.metadata = [...work.metadata, itemField];
            });
          });
        })
      ).toPromise();
  }

  createFinding(workId: number, work: Work) {
    return this.dataService.createFinding(workId, work).toPromise();
  }

  updateFinding(findingId: number, data: string) {
    return this.dataService.updateFinding(findingId, data).toPromise();
  }

  downloadFinding(findingId: number, name: string) {
    return this.dataService.downloadFinding(findingId)
      .pipe(
        map(data => {
          switch (data.type) {
            case HttpEventType.DownloadProgress:
              break;
            case HttpEventType.Response:
              const downloadedFile = new Blob([data.body], { type: data.body.type });
              const a = document.createElement('a');
              a.setAttribute('style', 'display:none;');
              document.body.appendChild(a);
              a.download = `${name}.doc`;
              a.href = URL.createObjectURL(downloadedFile);
              a.target = '_blank';
              a.click();
              document.body.removeChild(a);
              break;
          }
        })
      );
  }

  //#endregion
}
