import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { DirectoryDataService } from '../../data/directory/directory.data.service';
import { MetadataField } from 'src/app/core/models/entity/metadata-field';
import { tap } from 'rxjs/internal/operators/tap';
import { MediaItem, Media } from 'src/app/core/models/entity/media';
import { Tag } from 'src/app/core/models/entity/tag';
import { MediaDataService } from '../../data/media/media.data.service';
import { DateService } from '../dates/date.service';
import { Directory } from 'src/app/core/models/entity/directory';

@Injectable({
  providedIn: 'root'
})
export class DirectoryService {

  constructor(private directoryDataService: DirectoryDataService, private mediaDataService: MediaDataService, private dateService: DateService) { }

  getMedia(directoryId: number, pageNumber?: number, limit?: number): Observable<Media> {
    return this.directoryDataService.getMedia(directoryId, pageNumber, limit)
      .pipe(
        tap(items => {
          items.data.forEach(item => {
            if (item.type) { item.isVideo = item.type.toLowerCase() === 'mp4'; }
            item.routeLink = `/media/${item.id}/details`;
          });
        })
      );
  }

  getTreeViewMedia(query: string, filters: Tag[], directoryId?: number): Promise<MediaItem[]> {
    return this.mediaDataService.getMedia(query, filters, 0, 0, false, null, directoryId).toPromise()
      .then(response => {
        if (!response) { return; }
        let items = response.data;
        items.forEach(item => {
          if (item.type) { item.isVideo = item.type.toLowerCase() === 'mp4'; }
          item.routeLink = `/media/${item.documentId}/details`;
          item.modifiedOnString = this.dateService.formatToFullDate(item.modifiedOn);
          item.parentId = item.directoryId;
          item.id = item.documentId;
        });
        return items;
      });
  }

  getFolders(parentId?: number): Observable<Directory[]> {
    return this.directoryDataService.getFolders(parentId)
      .pipe(
        tap(folders => {
          folders.forEach(folder => {
            folder.isDirectory = true;
            folder.modifiedOnString = this.dateService.formatToFullDate(folder.modifiedOn);
            folder.icon = 'folder';
            if (parentId) {
              folder.hasChildRecords = folder.hasChild;
            }
          })
        })
      );
  }

  getFolder(id: number): Observable<Directory> {
    return this.directoryDataService.getFolder(id);
  }

  getMetadataFields(directoryId: number): Promise<MetadataField[]> {
    return this.directoryDataService.getMetadataFields(directoryId).toPromise();
  }

  addMetadataField(folderId: number, payload: MetadataField) {
    return this.directoryDataService.addMetadataField(folderId, payload);
  }

  updateMetadataField(folderId: number, payload: MetadataField) {
    return this.directoryDataService.updateMetadataField(folderId, payload);
  }

  deleteMetadataField(fieldSettingId: number) {
    return this.directoryDataService.deleteMetadataField(fieldSettingId);
  }
}
