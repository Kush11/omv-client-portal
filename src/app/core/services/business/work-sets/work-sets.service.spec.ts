import { TestBed } from '@angular/core/testing';

import { WorkSetsService } from './work-sets.service';

describe('WorkSetsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: WorkSetsService = TestBed.get(WorkSetsService);
    expect(service).toBeTruthy();
  });
});
