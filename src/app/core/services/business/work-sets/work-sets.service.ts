import { Injectable } from '@angular/core';
import { WorkSetsDataService } from '../../data/work-sets/work-sets.data.service';
import { WorkSet, WorkSetSchedule, WorkSetModel } from 'src/app/core/models/entity/work-set';
import { Observable } from 'rxjs';
import { WorkTemplateProcess } from 'src/app/core/models/entity/work-template';
import { WorkItem } from 'src/app/core/models/entity/work-item';
import { WorkAsset } from 'src/app/core/models/entity/work-set-asset';
import { DateService } from '../dates/date.service';
import { HttpEventType } from '@angular/common/http';
import { map, tap } from 'rxjs/operators';
import { WorkSetCycle, WorkSetCycles } from 'src/app/core/models/entity/work-set-cycle';
import { Status } from 'src/app/core/enum/status.enum';
import { FieldsService } from '../fields/fields.service';

@Injectable({
  providedIn: 'root'
})
export class WorkSetsService {

  constructor(private workSetsDataService: WorkSetsDataService, private dateService: DateService, private fieldsService: FieldsService) { }

  //#region Work Sets

  getSets(): Observable<WorkSet[]> {
    return this.workSetsDataService.getWorkSets()
      .pipe(
        tap(sets => {
          sets.forEach(set => {
            set.modifiedOnString = this.dateService.formatToFullDate(set.modifiedOn);
            set.routeLink = `/admin/work-sets/${set.id}/general-info`;
          });
        })
      );
  }

  getSet(id: number): Observable<WorkSetModel> {
    return this.workSetsDataService.getWorkSet(id);
  }

  createSet(set: WorkSet): Observable<number> {
    return this.workSetsDataService.createSet(set);
  }

  updateSet(id: number, payload: WorkSet) {
    return this.workSetsDataService.updateSet(id, payload);
  }

  //#endregion

  //#region Work Sets Asset

  getSetAssets(id: number) {
    return this.workSetsDataService.getSetAssets(id);
  }

  createSetAsset(assetId: number, payload: WorkAsset) {
    return this.workSetsDataService.createSetAsset(assetId, payload);
  }

  updateSetAsset(id: number, payload: WorkAsset) {
    return this.workSetsDataService.updateSetAsset(id, payload);
  }

  //#endregion

  //#region Work Sets Schedule

  createSetSchedule(id: number, payload: WorkSetSchedule) {
    return this.workSetsDataService.createSetSchedule(id, payload);
  }

  updateSetSchedule(id: number, payload: WorkSetSchedule) {
    return this.workSetsDataService.updateSetSchedule(id, payload);
  }

  //#endregion

  //#region Work Cycles

  getCycles(id: number, pageNumber: number, pageSize: number): Promise<WorkSetCycles> {
    return this.workSetsDataService.getCycles(id, pageNumber, pageSize)
      .pipe(
        tap(model => {
          model.data.forEach(c => {
            c.startDateString = this.dateService.formatToShortdate(new Date(c.startDate));
            c.endDateString = this.dateService.formatToShortdate(new Date(c.endDate));
            c.canEdit = c.statusName !== Status.Closed;
            c.canOpen = c.statusName === Status.Pending;
            c.canDelete = c.statusName !== Status.Closed;
            c.canPublish = c.statusName === Status.Open;
            c.statusDisplay = c.isPublished ? `${c.statusName} <span class='publish-badge'>P</span>` : c.statusName;
          });
        })
      ).toPromise();
  }

  getCycle(id: number, cycleId: number): Promise<WorkSetCycle> {
    return this.workSetsDataService.getCycle(id, cycleId).toPromise();
  }

  updateCycle(id: number, cycle: WorkSetCycle) {
    return this.workSetsDataService.updateCycle(id, cycle).toPromise();
  }

  deleteCycle(id: number, cycleId: number) {
    return this.workSetsDataService.deleteCycle(id, cycleId).toPromise();
  }

  openCycle(id: number, cycle: WorkSetCycle) {
    cycle = {
      ...cycle,
      status: 2
    };
    return this.workSetsDataService.updateCycle(id, cycle).toPromise();
  }

  publishCycle(id: number, cycleId: number) {
    return this.workSetsDataService.publishCycle(id, cycleId).toPromise();
  }

  //#endregion

  //#region Work Sets Item

  getprocess(templateId: number): Observable<WorkTemplateProcess[]> {
    return this.workSetsDataService.getProcess(templateId);
  }

  createSetItem(id: number, payload: WorkItem) {
    return this.workSetsDataService.createSetItem(id, payload);
  }

  updateSetItem(id: number, payload: WorkItem) {
    return this.workSetsDataService.updateSetItem(id, payload);
  }

  deleteSetItem(workSetId: number, ItemId: number) {
    return this.workSetsDataService.deleteSetItem(workSetId, ItemId);
  }

  downloadSetItem(workset: WorkSet) {
    return this.workSetsDataService.downloadWorkItem(workset.id)
      .pipe(
        map(data => {
          switch (data.type) {
            case HttpEventType.DownloadProgress:
              console.log('DownloadWorkSetItem DownloadProgress: ', data);
              break;
            case HttpEventType.Response:
              console.log('DownloadWorkSet complete: ', data);
              const downloadedFile = new Blob([data.body], { type: data.body.type });
              const a = document.createElement('a');
              a.setAttribute('style', 'display:none;');
              document.body.appendChild(a);
              a.download = `${workset.name}_Items.csv`;
              a.href = URL.createObjectURL(downloadedFile);
              a.target = '_blank';
              a.click();
              document.body.removeChild(a);
              break;
          }
        })
      );
  }

  importWorkSetItem(id: number, file: string) {
    return this.workSetsDataService.importWorkSetItem(id, file);
  }

  //#endregion

  //#region Work Sets Item

  getHistory(id: number, pageNumber?: number, pageSize?: number) {
    return this.workSetsDataService.getHistory(id, pageNumber, pageSize).toPromise()
      .then(response => {
        response.data.forEach(h => h.createdOnString = h.createdOn ? this.dateService.formatToFullDate(h.createdOn) : '');
        response.data.sort((a, b) => new Date(b.createdOn).getTime() - new Date(a.createdOn).getTime());
        return response;
      });
  }

  //#endregion
}
