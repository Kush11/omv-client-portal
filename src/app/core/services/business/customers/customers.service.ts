import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { CustomersDataService } from '../../data/customers/customers.data.service';
import { Customer } from 'src/app/core/models/entity/customer';
import { SASTokenType, SASTokenModule } from 'src/app/core/enum/azure-sas-token';

@Injectable({
  providedIn: 'root'
})
export class CustomersService {

  constructor(private customersDataService: CustomersDataService) { }

  getSettings(): Promise<any[]> {
    return this.customersDataService.getSettings().toPromise();
  }

  getAzureSASToken(type = SASTokenType.Read, module = SASTokenModule.Media): Promise<string> {
    return this.customersDataService.getAzureSASToken(type, module).toPromise();
  }

  getByHostHeader(header: string): Observable<Customer> {
    return this.customersDataService.getByHostHeader(header);
  }

  migrateDatabase() {
    return this.customersDataService.migrateDatabase();
  }
}
