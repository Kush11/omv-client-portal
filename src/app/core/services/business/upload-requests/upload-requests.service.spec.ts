import { TestBed } from '@angular/core/testing';

import { UploadRequestsService } from './upload-requests.service';

describe('UploadRequestsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: UploadRequestsService = TestBed.get(UploadRequestsService);
    expect(service).toBeTruthy();
  });
});
