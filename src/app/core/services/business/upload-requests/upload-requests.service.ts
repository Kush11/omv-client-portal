import { Injectable } from '@angular/core';
import { UploadRequestsDataService } from '../../data/upload-requests/upload-requests.data.service';
import { Observable } from 'rxjs/internal/Observable';
import { UploadRequest } from 'src/app/core/models/entity/upload-request';
import { tap } from 'rxjs/internal/operators/tap';
import { Status } from 'src/app/core/enum/status.enum';

@Injectable({
  providedIn: 'root'
})
export class UploadRequestsService {

  constructor(private uploadRequestsDataService: UploadRequestsDataService) { }

  getUploadHistory(): Observable<UploadRequest[]> {
    return this.uploadRequestsDataService.getUploadHistory()
      .pipe(
        tap(histories => {
          histories.forEach(history => {
            console.log('uploads history', history);
            history.routeLink = `/admin/media/uploads/${history.id}/details`;
            switch (history.status) {
              case Status.Completed:
                history.iconCss = 'c-icon-success';
                break;
              case Status.Rejected:
                history.iconCss = 'c-icon-warning';
                break;
              case Status.CompletedWithErrors:
                history.iconCss = 'c-icon-danger';
                break;
            }
          });
        })
      );
  }

  getNewUploads(): Observable<UploadRequest[]> {
    return this.uploadRequestsDataService.getNewUploads()
      .pipe(
        tap(uploads => {
          uploads.forEach(upload => {
            upload.routeLink = `/admin/media/uploads/${upload.id}/details`;
          });
        })
      );
  }

  getUploadsInProgress(): Observable<UploadRequest[]> {
    return this.uploadRequestsDataService.getUploadsInProgress()
    .pipe(
      tap(histories => {
        histories.forEach(progress => {
          progress.routeLink = `/admin/media/uploads/${progress.id}/details`;
          switch (progress.status) {
            case Status.Processing:
              progress.iconCss = 'c-icon-pending';
              break;
          }
        });
      })
    );
  }

  getUpload(id: number): Observable<UploadRequest> {
    return this.uploadRequestsDataService.getUpload(id);
  }

  approveUpload(id: number) {
    return this.uploadRequestsDataService.approveUpload(id).toPromise();
  }

  rejectUpload(id: number) {
    return this.uploadRequestsDataService.rejectUpload(id).toPromise();
  }

  cancelUpload(id: number) {
    return this.uploadRequestsDataService.cancelUpload(id).toPromise();
  }
}
