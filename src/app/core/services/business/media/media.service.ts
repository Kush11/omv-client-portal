import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
import { MediaDataService } from '../../data/media/media.data.service';
import { MediaHistories } from 'src/app/core/models/entity/media-history';
import { MediaItem, Media } from 'src/app/core/models/entity/media';
import { Tag } from 'src/app/core/models/entity/tag';
import { map } from 'rxjs/internal/operators/map';
import { HttpEventType } from '@angular/common/http';
import { DateService } from '../dates/date.service';
import { StreamingArchive } from 'src/app/core/models/entity/streaming-archive';
import { tap } from 'rxjs/internal/operators/tap';
import { TranscriptSegment } from 'src/app/core/models/entity/transcript-segment';
import { FieldsService } from '../fields/fields.service';
import { SortType, SortDirection } from 'src/app/core/enum/sort-enum';
import { Telemetry } from 'src/app/core/models/entity/telemetry';
import { MediaType } from 'src/app/core/enum/media-type';
import { Search_MapFilterDTO } from 'src/app/core/dtos/input/documents/Document_SearchInputDTO';

@Injectable({
  providedIn: 'root'
})
export class MediaService {

  constructor(private mediaDataService: MediaDataService, private dateService: DateService, private fieldsService: FieldsService) { }

  getMedia(query: string, filters: Tag[], pageNumber?: number, pageSize?: number, isTreeView?: boolean, mapCoordinates?: Search_MapFilterDTO[]): Observable<Media> {
    return this.mediaDataService.getMedia(query, filters, pageNumber, pageSize, isTreeView, mapCoordinates)
      .pipe(
        map(response => {
          response.data.forEach(item => {
            if (item.type) item.isVideo = item.type.toLowerCase() === 'mp4';
            item.routeLink = `/media/${item.documentId}/details`;
            if (!item.directoryId) {
              item.favoriteTemplate = `<input class="c-checkbox-star" type="checkbox" title="" [checked]="data.isFavorite" (click)="toggleFavoriteEvent(data)">`;
              item.checkboxTemplate = `<input class="c-checkbox-default" type="checkbox" title="" [checked]="data.isChecked" (click)="toggleFavoriteEvent(data)">`;
              item.downloadTemplate = `<button class="button-no-outline button-grid-link" (click)="downloadEvent(data)"> Download </button>`;
              item.editTemplate = `<img src="./assets/images/icon-pencil.svg" width="18" (click)="editEvent(data)">`;
            }
          });
          if (response.filters) {
            // get filters without order
            let filters = response.filters.filter(f => !f.order && f.order != 0);
            if (filters) {
              filters.sort((a, b) => (a.label.localeCompare(b.label)));
              filters.forEach((filter, index) => {
                filter.order = index - filters.length;
              });
            }
            response.filters.forEach(filter => {
              if (filter.options) {
                filter.options.forEach(option => {
                  if (typeof option.label === 'string') {
                    option.label = option.label.trim();
                  }
                });
                filter.options = filter.options.filter(o => o.label);
              }
              if (!filter.order) filter.order
            });
            response.filters.sort((a, b) => (a.order - b.order));
          }
          return response;
        })
      );
  }

  getTreeViewMedia(): Promise<Media> {
    return this.mediaDataService.getMedia(null, null, null, null, true)
      .pipe(
        map(response => {
          response.data.forEach(item => {
            item.routeLink = item.type !== MediaType.Telemetry ? `/media/${item.documentId}/details` : `/media/${item.documentId}/telemetry`;
          });
          return response;
        })
      ).toPromise();
  }

  getMediaItem(id: number): Observable<MediaItem> {
    return this.mediaDataService.getMediaItem(id)
      .pipe(
        tap(item => {
          if (item.type) item.isVideo = item.type.toLowerCase() === 'mp4';
          item.thumbnails.sort();
          const obj = {}
          for (const key of item.thumbnails) {
            obj[key.time] = {
              src: key.src,
              style: {
                left: '-62px',
                width: '135px',
                height: '94px',
              }
            };
          }

          item.thumbnailObject = obj;
        })
      );
  }

  updateMediaItem(id: any, payload: MediaItem): Observable<any> {
    return this.mediaDataService.updateMediaItem(id, payload);
  }

  deleteMediaItem(id: any): Observable<any> {
    return this.mediaDataService.deleteMediaItem(id);
  }

  createMediaItem(payload: MediaItem): Observable<any> {
    return this.mediaDataService.createMediaItem(payload);
  }

  download(name: string, url: string): Promise<boolean> {
    return this.mediaDataService.download(url)
      .pipe(
        map(data => {
          switch (data.type) {
            case HttpEventType.Response:
              const downloadedFile = new Blob([data.body], { type: data.body.type });
              const a = document.createElement('a');
              a.setAttribute('style', 'display:none;');
              document.body.appendChild(a);
              a.download = name;
              a.href = URL.createObjectURL(downloadedFile);
              a.target = '_blank';
              a.click();
              document.body.removeChild(a);
              break;
          }
          return true;
        })
      ).toPromise();
  }

  updateMetadata(id: any, metadata: string) {
    return this.mediaDataService.updateMetadata(id, metadata);
  }

  getVideoTranscript(documentId: any): Promise<TranscriptSegment[]> {
    return this.mediaDataService.getVideoTranscript(documentId)
      .pipe(
        tap(transcript => {
          if (transcript) {
            transcript = this.fieldsService.sort(transcript, 'startTime', SortType.Number, SortDirection.Ascending);
            const lastSegment = transcript[transcript.length - 1];
            transcript.forEach((segment) => {
              segment.startTime = Number(segment.startTime);
              segment.endTime = Number(segment.endTime);
              segment.timestamp = lastSegment.endTime < 3600
                ? this.dateService.convertSecondsToTimeString(segment.startTime, 'mm:ss')
                : this.dateService.convertSecondsToTimeString(segment.startTime, 'HH:mm:ss');
            });
          }
        })
      ).toPromise();
  }

  //#region Related Items

  getRelatedItems(id: string): Observable<MediaItem[]> {
    return this.mediaDataService.getRelatedItems(id)
      .pipe(
        tap(items => {
          items.forEach(item => {
            item.routeLink = `/media/${item.relatedDocumentId}/details`;
          });
        })
      );
  }

  addRelatedItems(documentId: string, relatedDocumentIds: string[]) {
    return this.mediaDataService.addRelatedItems(documentId, relatedDocumentIds);
  }

  deleteRelatedItems(documentId: string, relatedDocumentIds: string[]) {
    return this.mediaDataService.deleteRelatedItems(documentId, relatedDocumentIds);
  }

  //#endregion

  //#region History

  getHistory(id: string, pageNumber: number, pageSize: number): Observable<MediaHistories> {
    return this.mediaDataService.getHistory(id, pageNumber, pageSize)
      .pipe(
        map(audit => {
          audit.data.forEach(item => {
            item.createdOnString = this.dateService.formatToFullDate(item.createdOn)
          });
          return audit;
        })
      );
  }

  //#endregion

  //#region Favorites

  getFavorites(pageNumber?: number, pageSize?: number, query?: string): Observable<Media> {
    return this.mediaDataService.getFavorites(pageNumber, pageSize, query)
      .pipe(
        map(response => {
          response.data.forEach(item => {
            if (item.type) item.isVideo = item.type.toLowerCase() === 'mp4';
            item.routeLink = item.type !== MediaType.Telemetry ? item.type === 'FILTER' ? '' : `/media/${item.documentId}/details` : `/media/${item.documentId}/telemetry`;
          });
          return response;
        })
      );
  }

  addFavorite(mediaItemId: number, name?: string, tags?: Tag[]) {
    return this.mediaDataService.addFavorite(mediaItemId, name, tags);
  }

  removeFavorite(favoriteItemId: number) {
    return this.mediaDataService.removeFavorite(favoriteItemId);
  }

  //#endregion

  //#region Telemetry

  getTelemetry(id: any): Observable<Telemetry> {
    return this.mediaDataService.getTelemetry(id);
  }

  //#endregion

  //#region Streaming Archive

  getAvailableArchiveDays(asset: string, group: string): Observable<string[]> {
    return this.mediaDataService.getAvailableArchiveDays(asset, group)
      .pipe(
        map(response => {
          let dates: string[] = [];
          response.forEach(day => {
            const formattedDate = this.dateService.formatToString(new Date(day), 'MM-DD-YYYY');
            dates = [
              ...dates,
              formattedDate
            ];
          });
          return dates;
        })
      );
  }

  getArchiveVideos(asset: string, group: string, date: string, time?: string): Observable<StreamingArchive> {
    const queryDate = new Date(date + ' ' + time);
    const timeFilter = this.dateService.fromDateToUTCDate(queryDate);
    const utcTime1 = this.dateService.formatToString(timeFilter, 'MM-DD-YYYY hh:mm a');
    return this.mediaDataService.getArchiveVideos(asset, group, date, time ? utcTime1 : null)
      .pipe(
        map(response => {
          let retVal: StreamingArchive = new StreamingArchive();
          retVal.date = response.date;
          const firstHour = new Date(new Date(date).setHours(0, 0, 0, 0));
          const lastHour = this.dateService.addHours(firstHour, 24);
          const firstMinute = new Date(new Date(queryDate).setMinutes(0, 0, 0));
          const lastMinute = this.dateService.addHours(firstMinute, 1);
          response.feeds.forEach((feed, index) => {
            if (!time) { // for daily Videos
              feed.durations.forEach((duration) => {
                const serverStart = duration.startTime;
                const serverEnd = duration.endTime;
                duration.startTime = new Date(duration.startTime);
                duration.endTime = new Date(duration.endTime);
                if (duration.startTime >= lastHour || duration.endTime <= firstHour || duration.startTime > duration.endTime) return;

                duration.startTime = duration.startTime < firstHour ? firstHour : duration.startTime;
                duration.endTime = duration.endTime > lastHour ? lastHour : duration.endTime;

                console.log(`Start Time: ${this.dateService.formatToFullDate(duration.startTime)} - End Time: ${this.dateService.formatToFullDate(duration.endTime)} | Server Time => Start Time: ${serverStart} - End Time: ${serverEnd}`);

                const dateDifference = this.dateService.difference(duration.startTime, firstHour, 'm');
                const recordingTimeDifference = this.dateService.difference(duration.endTime, duration.startTime, 'm');

                // STEP 1: Find the percentage of the difference between the first hour (12am) and this recording's start time with respect to 24 hours (1440 minutes)
                duration.offset = (dateDifference / 1440) * 100;

                // STEP 2: Find the percentage of the difference between this recording's start and end times with respect to 24 hours(1440 minutes)
                duration.width = (recordingTimeDifference / 1440) * 100;
              });
            } else { // for hourly Videos
              feed.durations.forEach((duration) => {
                duration.startTime = new Date(duration.startTime);
                duration.endTime = new Date(duration.endTime);
                if (duration.startTime > lastMinute || duration.endTime < firstMinute) return;
                duration.startTime = duration.startTime < queryDate ? queryDate : duration.startTime;
                duration.endTime = duration.endTime > lastMinute ? lastMinute : duration.endTime;

                console.log(`Start Time: ${this.dateService.formatToTimeOnly(duration.startTime)} - End Time: ${this.dateService.formatToTimeOnly(duration.endTime)}`);

                const dateDifference = this.dateService.difference(duration.startTime, firstMinute, 's');
                const recordingTimeDifference = this.dateService.difference(duration.endTime, duration.startTime, 's');
                duration.offset = (dateDifference / 3600) * 100;
                duration.width = (recordingTimeDifference / 3600) * 100;
              });
            }
            feed.color = index < 16 ? this.getColor(index) : this.getRandomColor();
            feed.id = index.toString();
          });
          // response = this.getResponse(response, 2);
          return response;
        })
      );
  }


  private getColor(index: number) {
    const colors = ['#ABD978', '#f8e71c', '#EAA9FB', '#A2AFFE', '#97DFFC', '#b8e986', '#D5BBB1', '#F79BA6', '#52BFFF', '#57E2E5',
      '#ffba53', '#9CF739', '#ccb5d5', '#F7A581', '#9FCEE2', '#FDC527'];
    return colors[index];
  }

  private getRandomColor(): string {
    var letters = '0123456789ABCDEF';
    var color = '#';
    for (var i = 0; i < 6; i++) {
      color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
  }

  //#endregion
}
