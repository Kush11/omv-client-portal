import { TestBed } from '@angular/core/testing';

import { MetadataListItemsService } from './metadata-list-items.service';

describe('MetadataListItemsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MetadataListItemsService = TestBed.get(MetadataListItemsService);
    expect(service).toBeTruthy();
  });
});
