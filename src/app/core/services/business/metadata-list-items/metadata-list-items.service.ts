import { Injectable } from '@angular/core';
import { MetadataListItemsDataService } from '../../data/metadata-list-items/metadata-list-items.data.service';
import { Observable } from 'rxjs/internal/Observable';
import { MetadataListItem } from 'src/app/core/models/entity/metadata-list-item';

@Injectable({
  providedIn: 'root'
})
export class MetadataListItemsService {

  constructor(private metadataListItemsDataService: MetadataListItemsDataService) { }

  getMetadataListItems(id: number): Observable<MetadataListItem[]> {
    return this.metadataListItemsDataService.getMetadataListItems(id);
  }

  getMetadataListItem(id: number): Observable<MetadataListItem> {
    return this.metadataListItemsDataService.getMetadataListItem(id);
  }

  updateMetadataListItem(id: number, payload: MetadataListItem): Observable<MetadataListItem> {
    return this.metadataListItemsDataService.updateMetadataListItem(id, payload);
  }

  removeMetadataListItem(id: number, metadataListItemId: number) {
    return this.metadataListItemsDataService.removeMetadataListItem(id, metadataListItemId);
  }
}
