import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { Permission } from 'src/app/core/enum/permission';
import { PermissionsDataService } from '../../data/permissions/permissions.data.service';


@Injectable({
  providedIn: 'root'
})
export class PermissionsService {

  constructor(private permissionsDataService: PermissionsDataService) { }

  getPermissions(): Observable<Permission[]> {
    return this.permissionsDataService.getPermissions();
  }
}
