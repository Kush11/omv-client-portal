import { Injectable } from '@angular/core';
import { WorkTemplate, WorkTemplateProcess, WorkTemplateStep, WorkGroup, WorkTemplateField, WorkTemplateFields } from 'src/app/core/models/entity/work-template';
import { Observable } from 'rxjs/internal/Observable';
import { WorkTemplatesDataService } from '../../data/work-templates/work-templates.data.service';
import { MetadataField } from 'src/app/core/models/entity/metadata-field';
import { map } from 'rxjs/internal/operators/map';
import { WorkTemplateStatus } from 'src/app/core/enum/work-template-status';
import { tap } from 'rxjs/internal/operators/tap';
import { DateService } from '../dates/date.service';
import { HttpEventType } from '@angular/common/http';
import { ListItem } from 'src/app/core/models/entity/list-item';

@Injectable({
  providedIn: 'root'
})
export class WorkTemplatesService {

  constructor(private workTemplatesDataService: WorkTemplatesDataService, private dateService: DateService) { }

  //#region Work Templates

  getGroups(status?: string): Observable<WorkTemplate[]> {
    return this.workTemplatesDataService.getGroups(status)
      .pipe(
        tap(response => {
          const templates = response;
          templates.forEach(t => {
            t.modifiedOnString = t.modifiedOn ? this.dateService.formatToFullDate(t.modifiedOn) : '';
            t.modifiedOn = new Date(t.modifiedOn);
          });
          templates.sort((a, b) => new Date(b.modifiedOn).getTime() - new Date(a.modifiedOn).getTime());
          return templates;
        })
      );
  }

  getGroup(id: number): Observable<WorkGroup> {
    return this.workTemplatesDataService.getGroup(id)
      .pipe(
        tap(response => {
          return this.processWorkGroup(response);
        })
      );
  }

  processWorkGroup(group: WorkGroup) {
    group.workTemplateVersions.forEach(v => v.publishedOnString = v.publishedOn ? this.dateService.formatToFullDate(new Date(v.publishedOn)) : '');
    group.workTemplateVersions.sort((a, b) => b.version - a.version);
    group.workTemplates.forEach(t => {
      t.fields.forEach((f, i) => {
        f.order = i;
        f.isDefault = true;
      });
    });
    if (group.workTemplateVersions) {
      group.workTemplateVersions.forEach(v => {
        v.canDownload = v.status !== WorkTemplateStatus.Draft;
        v.canActivate = v.status === WorkTemplateStatus.Draft;
      });
    }
    return group;
  }

  //#endregion

  //#region Work Template

  getTemplate(groupId: number, version: number): Observable<WorkTemplate> {
    return this.getGroup(groupId)
      .pipe(
        map(group => {
          console.log('WorkTemplatesService getTemplate: ', group);
          let template: WorkTemplate;
          if (version) {
            template = group.workTemplates.find(t => t.version === version);
          } else {
            template = group.workTemplates.find(t => t.status === WorkTemplateStatus.Draft);
          }
          return template;
        })
      );
  }

  createTemplate(template: WorkTemplate): Observable<number> {
    return this.workTemplatesDataService.createTemplate(template);
  }

  importTemplate(id: number, file: string) {
    return this.workTemplatesDataService.importTemplate(id, file);
  }

  activateTemplate(id: number, comment?: string) {
    return this.workTemplatesDataService.activateTemplate(id, comment);
  }

  downloadTemplate(id: number, template: WorkTemplate) {
    return this.workTemplatesDataService.downloadTemplate(id)
      .pipe(
        map(data => {
          switch (data.type) {
            case HttpEventType.DownloadProgress:
              break;
            case HttpEventType.Response:
              const downloadedFile = new Blob([data.body], { type: data.body.type });
              const a = document.createElement('a');
              a.setAttribute('style', 'display:none;');
              document.body.appendChild(a);
              a.download = `${template.name}v${template.version}`;
              a.href = URL.createObjectURL(downloadedFile);
              a.target = '_blank';
              a.click();
              document.body.removeChild(a);
              break;
          }
        })
      );
  }

  //#endregion

  //#region Work Template Fields

  getFields(templateId: number): Promise<WorkTemplateFields> {
    return this.workTemplatesDataService.getFields(templateId).toPromise();
  }

  updateFields(templateId: number, fields: WorkTemplateField[]) {
    return this.workTemplatesDataService.updateFields(templateId, fields);
  }

  //#endregion

  //#region Work Template Process

  createProcess(process: WorkTemplateProcess) {
    return this.workTemplatesDataService.createProcess(process).toPromise();
  }

  updateProcess(id: number, process: WorkTemplateProcess) {
    return this.workTemplatesDataService.updateProcess(id, process).toPromise();
  }

  deleteProcess(id: number, templateId: number) {
    return this.workTemplatesDataService.deleteProcess(id, templateId).toPromise();
  }

  //#endregion

  //#region Work Template Step

  createStep(step: WorkTemplateStep) {
    return this.workTemplatesDataService.createStep(step).toPromise();
  }

  updateStep(step: WorkTemplateStep) {
    return this.workTemplatesDataService.updateStep(step).toPromise();
  }

  deleteStep(step: WorkTemplateStep) {
    return this.workTemplatesDataService.deleteStep(step).toPromise();
  }

  sortSteps(processId: number, steps: any[]) {
    return this.workTemplatesDataService.sortSteps(processId, steps).toPromise();
  }

  //#endregion

  //#region Work Template Form

  getForms(stepId: number): Promise<MetadataField[]> {
    return this.workTemplatesDataService.getForms(stepId).toPromise();
  }

  createForm(stepId: number, field: MetadataField) {
    return this.workTemplatesDataService.createForm(stepId, field).toPromise();
  }

  updateForm(stepId: number, field: MetadataField) {
    return this.workTemplatesDataService.updateForm(stepId, field).toPromise();
  }

  deleteForm(stepId: number, field: MetadataField) {
    return this.workTemplatesDataService.deleteForm(stepId, field).toPromise();
  }

  //#endregion

  //#region Work Template Finding

  getFindings(stepId: number): Promise<MetadataField[]> {
    return this.workTemplatesDataService.getFindings(stepId).toPromise();
  }

  createFinding(stepId: number, field: MetadataField) {
    return this.workTemplatesDataService.createFinding(stepId, field).toPromise();
  }

  updateFinding(stepId: number, field: MetadataField) {
    return this.workTemplatesDataService.updateFinding(stepId, field).toPromise();
  }

  deleteFinding(stepId: number, field: MetadataField) {
    return this.workTemplatesDataService.deleteFinding(stepId, field).toPromise();
  }

  //#endregion

  //#region Work Template Metadata List Items

  getMetadataListItems(templateId: number): Observable<ListItem[]> {
    return this.workTemplatesDataService.getMetadataListItems(templateId);
  }

  //#endregion

  //#region Work Template Item

  getHistory(id: number, pageNumber?: number, pageSize?: number) {
    return this.workTemplatesDataService.getHistory(id, pageNumber, pageSize).toPromise()
      .then(response => {
        response.data.forEach(h => h.createdOnString = h.createdOn ? this.dateService.formatToFullDate(h.createdOn) : '');
        response.data.sort((a, b) => new Date(b.createdOn).getTime() - new Date(a.createdOn).getTime());
        return response;
      });
  }

  //#endregion

}
