import { TestBed } from '@angular/core/testing';

import { WorkTemplatesService } from './work-templates.service';

describe('WorkTemplatesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: WorkTemplatesService = TestBed.get(WorkTemplatesService);
    expect(service).toBeTruthy();
  });
});
