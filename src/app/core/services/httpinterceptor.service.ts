import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpHandler, HttpEvent, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs/internal/Observable';
import { AuthService } from './business/auth.service';
import { from } from 'rxjs/internal/observable/from';


@Injectable({
  providedIn: 'root'
})
export class HttpInterceptorService implements HttpInterceptor {

  constructor(private auth: AuthService) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return from(this.processInterceptor(req, next));
  }

  private async processInterceptor(req: HttpRequest<any>, next: HttpHandler): Promise<HttpEvent<any>> {
    const token = await this.auth.getAccessToken();

    const exlude = 'blob.core.windows.net';

    let changedRequest: HttpRequest<any> = req;

    if (token && (req.url.search(exlude) === -1)) {
      if (req.method === 'GET') { // for IE
        changedRequest = req.clone({
          headers: req.headers.set('Cache-Control', 'no-cache')
            .set('Pragma', 'no-cache')
            .set('Content-Type', 'application/json')
            .set('Authorization', `Bearer ${token}`)
        });
      } else {
        changedRequest = req.clone({
          headers: req.headers.set('Content-Type', 'application/json')
            .set('Authorization', `Bearer ${token}`)
        });
      }



    }



    return next.handle(changedRequest).toPromise();
  }
}
