export class MailTemplate_UpdateInputDTO {
    TemplateCode: string;
    TemplateName: string;
    TemplateSubject: string;
    TemplateBody: string;
}