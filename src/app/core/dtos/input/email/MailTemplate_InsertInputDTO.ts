export class MailTemplate_InsertInputDTO {
    TemplateName: string;
    TemplateSubject: string;
    TemplateBody: string;
}