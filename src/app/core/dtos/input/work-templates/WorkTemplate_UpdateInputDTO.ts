export class WorkTemplate_UpdateInputDTO {
   WorkTemplateId: number;
   WorkType: string;
   WorkItem: string;
   WorkItemParent: string;
}