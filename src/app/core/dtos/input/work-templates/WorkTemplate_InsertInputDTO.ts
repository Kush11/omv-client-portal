export class WorkTemplate_InsertInputDTO {
  WorkTemplateId: number | null;
  Name: string;
  WorkType: string;
  WorkItem: string;
  WorkItemParent: string;
  AssetParent : number;
}