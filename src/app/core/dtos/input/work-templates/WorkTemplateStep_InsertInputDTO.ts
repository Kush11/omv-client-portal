export class WorkTemplateStep_InsertInputDTO {
  workTemplateStepId: number;
  workTemplateProcessId: number;
  code: string;
  name: string;
  order: number;
  notifyGroup: number;
  emailTemplateCode: string;
  advanceButton: string;
  advanceGroup: number;
  workTemplateId: number;
  createdBy: string;
}