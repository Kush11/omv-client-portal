export class WorkTemplateStep_UpdateInputDTO {
  workTemplateStepId: number;
  code: string;
  name: string;
  order: number;
  notifyGroup: number;
  emailTemplateCode: string;
  advanceButton: string;
  advanceGroup: number;
  workTemplateId: number;
  createdBy: string;
}