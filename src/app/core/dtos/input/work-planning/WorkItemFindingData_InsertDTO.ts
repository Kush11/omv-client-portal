export class WorkItemFindingData_InsertDTO {
  workFindingId: number;
  workId: number;
  stepId: number;
  findingData: string;
}