export class WorkItem_InsertInputDTO {
    workItemId: number | null;
    workSetId: number;
    workTemplateProcessId: number;
    data: string;
}