export class WorkSetCycle_UpdateInputDTO {
  cycleId: number;
  workSetId: number;
  startDate: Date | string;
  endDate: Date | string;
  status: number;
  isPublished: boolean;
}