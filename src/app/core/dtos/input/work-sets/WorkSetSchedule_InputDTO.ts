export class WorkSetSchedule_InputDTO {
	workSetId: number;
	cycleName: string;
	frequencyType: number;
	frequencyStartDate: Date;
	frequencyEndDate: Date;
	frequencyCompletionDate: Date;
	customFrequency: number;
	customDuration: number;
	repeatFrequency: number;
	repeatDuration: number;
}