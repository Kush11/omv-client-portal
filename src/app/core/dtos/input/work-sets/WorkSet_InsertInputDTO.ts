export class WorkSet_InsertInputDTO {
	workSetId: number | null;
	workTemplateId: number;
	name: string;
	asset: number;
}