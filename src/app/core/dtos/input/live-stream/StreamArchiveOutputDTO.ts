import { Camera_GetAllOutputDTO } from './Camera_GetAllOutputDTO';

export class CameraRecording_GetAllOutputDTO {
  cameraRecordingId: number;
  cameraId: number;
  documentId: string;
  status: number;
  startDate: Date;
  endDate: Date;
  cameraRecordingStatus: number;
  documentName: string;
  documentUrl: string;
  thumbnailContainerUrl: string;
}

export class StreamArchiveOutputDTO {
  dailyScheduleDate?: string;
  timeLineDate?: string;
  feeds: Camera_GetAllOutputDTO[];
}