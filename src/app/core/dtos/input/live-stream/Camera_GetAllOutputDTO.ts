import { CameraRecording_GetAllOutputDTO } from './StreamArchiveOutputDTO';

export class Camera_GetAllOutputDTO {
    cameraId: number;
    favoriteId?: number;
    livestreamURL: string;
    name: string;
    thumbnailUrl: string;
    isFavorite: boolean;
    asset: number;
    group: number;
    recordings: CameraRecording_GetAllOutputDTO[];
}