export class Document_SearchInputDTO {
  query: string;
  isMapView: boolean;
  pageNumber: number;
  limit: number;
  filters: AzureSearch_FilterDTO[];
  directoryId: number;
  coordinates: Search_MapFilterDTO[];
}

export class AzureSearch_FilterDTO {
  key: string;
  value: string;
  rangeValue: number[];
  fieldDataType: string;
  filterType: string;
}

export class Search_MapFilterDTO {
  latitude: number;
  longitude: number;
}
