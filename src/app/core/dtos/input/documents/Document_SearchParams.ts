export class Document_SearchParams {
  fieldName: string;
  Value: any;
  IsMetaColumn: boolean;
  key?: string;
  value?: string;
  rangeValue?: number[];
  type?: string;
}
