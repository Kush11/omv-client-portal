import { Document_SearchParams } from './Document_SearchParams';

export class Document_SearchParamsInputDTO {
  PageNumber: number;
  Limit: number;
  Filter: Document_SearchParams[];
  Query: string;
  isTreeView: boolean;
}