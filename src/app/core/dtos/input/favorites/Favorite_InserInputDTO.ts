export class Favorite_InserInputDTO {
  entityId: string;
  entityType: string;
  value: string;
  filterName: string;
}