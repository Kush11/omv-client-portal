export class Rule_InsertInputDTO {
  Name: string;
  AssemblyName: string;
  AssemblyPath: string;
  AssemblyFile: any;
  ClassNameWithNamespace: string;
  ApprovalGroupId: number;
}