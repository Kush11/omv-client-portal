export class Rule_UpdateInputDTO {
  Name: string;
  AssemblyName: string;
  AssemblyPath: string;
  AssemblyFile: any;
  ClassNameWithNamespace: string;
  ApprovalGroupId: number;
}