export class MetadataField_InsertInputDTO {
    MetadataFieldId?: number;
    FieldName: string;
    label: string;
    MetadataListId?: number;
    FieldTypeId: number;
    module: number;
    ParentFieldId: number | null;
    isFilterable: boolean;
    DataType: string;
}
