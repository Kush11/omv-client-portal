export class MetadataFieldSetting_UpdateInputDTO {
  metadataFieldId: number;
  entityId: number;
  isRequired: boolean;
  order: number | null;
  value: string;
}