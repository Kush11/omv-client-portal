export class SortMetadataField_InputDTO {
  metadataFieldId: number;
  order: number;
}