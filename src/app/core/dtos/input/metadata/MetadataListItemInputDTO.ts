export class MetadataListItemInputDTO {
    MetadataListItemId?: number;
    MetadataListId: number;
    ItemValue: string;
    ItemDescription: string;
    ItemSort: string;
    ParentItemId: number;
    status: number;
    statusName: string;

}