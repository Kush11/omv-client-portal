export class MetadataFieldSetting_InsertInputDTO {
  metadataFieldId: number;
  entityId: number;
  overrideLabel: string;
  isRequired: boolean;
  isEditable: boolean;
  allowDocumentUpload: boolean;
  order: number | null;
  value: string;
  isNested: boolean;
}