export class MetadataField_UpdateInputDTO {
    FieldName: string;
    label: string;
    MetadataListId: number | null;
    FieldTypeId: number;
    IsFilterable: boolean | null;
    ParentFieldId: number | null;
    DataType : string;
}