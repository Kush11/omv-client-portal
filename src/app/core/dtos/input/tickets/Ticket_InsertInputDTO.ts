export class Ticket_InsertInputDTO {
    TicketId: number;
    TicketSubject: string;
    TicketMessage: string;
    IsFollowUp: boolean;
    Availability: boolean;
    StartTime: boolean;
    EndTime: boolean;
    documents: DocumentDTO[];
}

export class DocumentDTO {
    storageType: string;
    entityType: string;
    entityId: string;
    documentTypeCode: string;
    documentName: string;
    documentUrl: string;
    metadata: string;
    contentType: string;
    size: number;
    containerId: string;
    thumbnailContainerUrl: string;
    isDeleted: boolean;
    status: number;
}
