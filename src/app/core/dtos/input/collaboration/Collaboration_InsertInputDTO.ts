export class Collaboration_InsertInputDTO {
    collaborationTitle: string;
    collaborationDate: Date;
    startTime: string;
    endTime: string;
    directoryId : number;
    collaborationAttendees: number[];
    collaborationItems: CollaborationItem_InsertInputDTO[];
}
export class CollaborationItem_InsertInputDTO {
    entityId: number;
    entityName: string;
}