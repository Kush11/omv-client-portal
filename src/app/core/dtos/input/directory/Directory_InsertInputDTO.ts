export class Directory_InsertInputDTO {
    DirectoryId: number;
    DirectoryName: string;
    DirectoryParentId: number;
}