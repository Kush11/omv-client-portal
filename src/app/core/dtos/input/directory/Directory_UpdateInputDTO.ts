export class Directory_UpdateInputDTO
   {
       DirectoryId: number;
       DirectoryName: string;
       DirectoryParentId: number;
       Status?: number | null;
   }