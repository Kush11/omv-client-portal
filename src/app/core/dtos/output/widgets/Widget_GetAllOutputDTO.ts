export class Widget_GetAllOutputDTO {
    widgetId: string;
    sizeX: number;
    sizeY: number;
    row: number;
    col: number;
    content: object;
    name: string;
    header: string;
}


