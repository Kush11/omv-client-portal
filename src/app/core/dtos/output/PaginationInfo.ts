export class PaginationInfo {
  Limit: number;
  PageNumber: number;
  returned: number;
  total: number;
}