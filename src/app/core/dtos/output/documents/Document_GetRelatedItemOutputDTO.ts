export class Document_GetRelatedItemOutputDTO {
  RelatedItemId: number;
  DocumentId: string;
  RelatedDocumentId: string;
  DirectoryId: number | null;
  EntityType: string;
  EntityId: string;
  DocumentTypeCode: string;
  DocumentName: string;
  DocumentUrl: string;
  Metadata: string;
  ContentType: string;
  isFavorite: boolean;
  favoriteId: number;
  Size: number | null;
  ThumbnailContainerUrl: string;
  Status: number;
  CreatedOn: Date | string;
  CreatedBy: string;
  ModifiedOn: Date | string;
  ModifiedBy: string;
}
