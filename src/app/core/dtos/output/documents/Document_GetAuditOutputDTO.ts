import { PaginationInfo } from '../PaginationInfo';

export class Document_GetAuditOutputDTO {
    data: Document_GetAuditOutputDTOData[];
    pagination: PaginationInfo;
}

export class Document_GetAuditOutputDTOData {
    AuditId: string;
    EventName: string;
    EntityType: string;
    EntityId: string;
    ColumnName: string;
    OldValue: string;
    NewValue: string;
    CreatedOn: Date;
    CreatedBy: string;
}