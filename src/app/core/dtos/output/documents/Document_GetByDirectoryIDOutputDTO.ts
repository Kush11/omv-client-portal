import { Document_GetByIdOutputDTO } from './Document_GetByIdOutputDTO';
import { PaginationInfo } from '../PaginationInfo';

export class Document_GetByDirectoryIdOutputDTO
   {
    pagination: PaginationInfo;
    data: Document_GetByIdOutputDTO;
}

