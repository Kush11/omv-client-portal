import { PaginationInfo } from '../PaginationInfo';

export class Document_SearchOutputDTO {
  pagination: PaginationInfo;
  data: Document_AzureGetAllOutputDTO[];
  filters: FacetDTO[];
}

export class Document_AzureGetAllOutputDTO {
  id: number;
  documentId: string;
  workDocumentId: number | null;
  directoryId: number | null;
  directoryName: string;
  name: string;
  parentId: number | null;
  entityType: string;
  entityId: string;
  documentTypeCode: string;
  documentName: string;
  documentUrl: string;
  hasChild: boolean | null;
  metadata: string;
  contentType: string;
  size: number | null;
  thumbnail: string;
  thumbnails: [];
  longitude: string;
  latitude: string;
  favoriteId: number | null;
  isFavorite: boolean;
  status: number | null;
  createdOn: Date | string;
  createdBy: string;
  modifiedOn: Date | string;
  modifiedBy: string;
  mediaType: number | null;
  mediaTypeName: string;
  storageType: string;
  metadataField: { [key: string]: any };
}

export class FacetDTO {
  label: string;
  fieldName: string;
  filterType: string;
  fieldDataType: string;
  facetResults: FacetResultDTO[];
}

export class FacetResultDTO {
  value: any;
  count: number;
}

// export class DataDTO {
//   document: Document_SearchOutputData[];
// }

// export class Document_SearchOutputData extends BaseDTO {
//   Id: number;
//   Name: string;
//   ParentId?: number;
//   HasChild?: boolean;
//   DirectoryId?: number;
//   DocumentId?: any;
//   FavoriteId?: number;
//   EntityType: string;
//   EntityId: string;
//   DocumentTypeCode: string;
//   DocumentName: string;
//   DocumentUrl: string;
//   Metadata: string;
//   ContentType: string;
//   Size?: number;
//   ThumbnailContainerUrl: string;
//   Status?: number;
//   Longitude: number;
//   Latitude: number;
//   IsFavorite: boolean;

//   IsSearch: boolean;
// }


