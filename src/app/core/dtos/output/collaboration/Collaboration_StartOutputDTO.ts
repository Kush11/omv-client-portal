import { CollaborationItem_GetByCollaborationId } from './CollaborationItem_GetByCollaborationId';

// tslint:disable-next-line: class-name
export class Collaboration_StartOutputDTO {
    name: string;
    accessToken: string;
    roomId: string;
    roomUrl: string;
    collaborationItem: CollaborationItem_GetByCollaborationId[];
    isHost: boolean;
 }
