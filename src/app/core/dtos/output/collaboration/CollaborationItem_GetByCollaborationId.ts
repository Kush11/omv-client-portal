export interface CollaborationItem_GetByCollaborationId {
    livestreamURL: string;
    name: string;
}

export class CollaborationScreenshot_OutputDTO {
    Id: number;
    DocumentId: string;
    DocumentUrl: string;
}