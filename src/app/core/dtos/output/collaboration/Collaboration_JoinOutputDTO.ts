import { CollaborationItem_GetByCollaborationId } from './CollaborationItem_GetByCollaborationId';

export class Collaboration_JoinOutputDTO {
    name: string;
    identity: string;
    collaborationAccessToken: string;
    chatAccessToken: string;
    collaborationItems: CollaborationItem_GetByCollaborationId[];
    isHost: boolean;
	participants: any[];
}