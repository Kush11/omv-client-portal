import { BaseDTO } from '../../BaseDTO';
import { Camera_GetAllOutputDTO } from '../../input/live-stream/Camera_GetAllOutputDTO';
import { User_SearchOutputDTO } from '../users/User_SearchOutputDTO';

export class CollaborationItemDTO extends BaseDTO {
    collaborationId: number;
    collaborationTitle: string;
    startTime: string;
    endTime: string;
    collaborationDate: Date;
    isProgress: boolean;
    attendees: AttendeesDTO[];
    media: Camera_GetAllOutputDTO[];
    isHost: boolean;
    isChecked: boolean;
}

export class AttendeesDTO {
    id: number;
    firstName: string;
    lastName: string;
    displayName: string;
    role: string;
    imageUrl: string;
}


export class Collaboration_GetByIdOutputDTO {
    collaborationId: number;
    collaborationTitle: string;
    collaborationDate?: Date;
    createdOn: Date;
    createdBy: string;
    modifiedOn: Date;
    modifiedBy: string;
    startTime: string;
    endTime: string;
    collaborationAttendees: User_SearchOutputDTO[];
    collaborationItem: Camera_GetAllOutputDTO[];
}