import { BaseDTO } from '../../BaseDTO';

export class WorkSet_GetAllOutputDTO extends BaseDTO {
	workSetId: number;
	name: string;
}