import { BaseDTO } from '../../BaseDTO';
import { WorkItem_GetAllOutputDTO } from './WorkItem_GetAllOutputDTO';

export class WorkSet_GetByIdOutputDTO extends BaseDTO {
  workSetId: number;
  workTemplateId: number;
  workTemplateGroupId: number;
  metadataListId: number;
  comment: string;
  templateName: string;
  name: string;
  asset: number;
  cycleName: string;
  frequencyType: number;
  frequencyTypeName: string;
  frequencyStartDate: Date;
  frequencyEndDate: Date;
  completedDate: Date;
  customFrequency: number;
  customDuration: number;
  customDurationName: string;
  status: number;
  statusName: string;
  version: number;
  workItems: WorkItem_GetAllOutputDTO[];
}