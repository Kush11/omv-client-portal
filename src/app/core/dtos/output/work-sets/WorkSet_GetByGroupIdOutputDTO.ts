import { WorkItem_GetAllOutputDTO } from './WorkItem_GetAllOutputDTO';

export class WorkSet_GetByGroupIdOutputDTO {
    workSetId: number;
    workSetGroupId: number;
    workTemplateId: number;
    workTemplateGroupId: number;
    metadataListId: number;
    comment: string;
    name: string;
    asset: number;
    cycleName: string;
    frequencyType: string;
    customDuration: number;
    customDurationName: string;
    customFrequency: number;
    frequencyStartDate: Date;
    frequencyEndDate: Date;
    completedDate: Date;
    version: number;
    statusName: number;
    workItems: WorkItem_GetAllOutputDTO[];
}
