export class WorkSetProcess_GetAllOutputDTO {
    workTemplateProcessId: number;
    workTemplateId: number;
    name: string;
    code: string;
}