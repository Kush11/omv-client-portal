import { PaginationInfo } from '../PaginationInfo';

export class WorkSetCycle_GetAllOutputDTO {
  data: WorkSetCycle_GetAllOutputDTOData[];
  pagination: PaginationInfo;
}

export class WorkSetCycle_GetAllOutputDTOData {
  cycleId: number;
  workSetId: number;
  startDate: Date | string;
  endDate: Date | string;
  status: number;
  statusName: string;
  isPublished: boolean;
}