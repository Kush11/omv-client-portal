export class WorkSetCycle_GetByIdOutputDTO {
  cycleId: number;
  workSetId: number;
  startDate: Date | string;
  endDate: Date | string;
  status: number;
  isPublished: boolean;
}