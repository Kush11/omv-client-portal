import { WorkSet_GetByGroupIdOutputDTO } from './WorkSet_GetByGroupIdOutputDTO';
import { PaginationInfo } from '../PaginationInfo';

export class WorkSetDTO {
	workSet_GetByGroupIdOutputDTOs: WorkSet_GetByGroupIdOutputDTO;
	workSetHistories: WorkSetHistory_GetAllOutputDTO[];
}

export class WorkSetVersion_GetAllOutputDTO {
	WorkSetId: number;
	Version: number;
	Status: number;
	StatusName: string;
	Comment: string;
	PublishedOn: Date | string;
	PublishedBy: string;
}

export class WorkSetHistory_GetAllOutputDTO {
	data: WorkSetHistory_GetAllOutputDTOData[];
	pagination: PaginationInfo[];
}

export class WorkSetHistory_GetAllOutputDTOData {
	AuditId: string;
	EventName: string;
	EntityType: string;
	EntityId: string;
	ColumnName: string;
	OldValue: string;
	NewValue: string;
	CreatedOn: Date | string;
	CreatedBy: string;
}
