import { BaseDTO } from '../../BaseDTO';

export class WorkItem_GetAllOutputDTO extends BaseDTO {
	workItemId: number;
	workSetId: number;
	workTemplateProcessId: number;
	processDisplayText: string;
	processName: string;
	data: string;
	sortOrder: any;
}