import { PaginationInfo } from '../PaginationInfo';

export class Favorite_OutputDTO {
  Pagination: PaginationInfo;
  Data: Favorite_OutputData[];
}

export class Favorite_OutputData {
  FavoriteId: number;
  Name: string;
  ParentId: number | null;
  DocumentId: string | null;
  EntityType: string;
  EntityId: string;
  DocumentTypeCode: string;
  DocumentName: string;
  DocumentUrl: string;
  Metadata: string;
  ContentType: string;
  Size: number | null;
  ThumbnailContainerUrl: string;
  HasQueryFilter: boolean;
  Status: number | null;
  CreatedOn: Date | string;
  CreatedBy: string;
  ModifiedOn: Date | string;
  ModifiedBy: string;
}