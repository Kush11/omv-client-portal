import { MetadataField_GetListItemByIdOutputDTO } from './MetadataField_GetListItemByIdOutputDTO';

export class MetadataFieldSetting_GetAllOutputDTO {
  MetadataFieldSettingId: number;
  MetadataFieldId: number;
  DefaultValue: string;
  FieldName: string;
  MetadataListId: number;
  MetadataListName: string;
  FieldTypeId: number;
  fieldTypeName: string;
  label: string;
  EntityId: string;
  EntityName: string;
  IsRequired: boolean;
  isEditable: boolean;
  allowDocumentUpload: boolean;
  Order: number;
  Status: number;
  StatusName: string;
  parentFieldId: number;
  Options: MetadataField_GetListItemByIdOutputDTO[];
}