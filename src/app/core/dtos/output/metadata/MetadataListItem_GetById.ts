export class MetadataListItem_GetById {
  MetadataListItemId: number;
  MetadataListId: number;
  ItemValue: string;
  ItemDescription: string;
  ItemSort: string;
  ParentItemId: number;
  ParentItemDescription: string;
  Status: number;
  CreatedOn: Date | string;
  CreatedBy: string;
  ModifiedOn: Date | string;
  ModifiedBy: string;
}