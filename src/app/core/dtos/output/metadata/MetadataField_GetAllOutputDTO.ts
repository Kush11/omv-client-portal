import { MetadataField_GetListItemByIdOutputDTO } from './MetadataField_GetListItemByIdOutputDTO';

export class MetadataField_GetAllOutputDTO {
    MetadataFieldId: number;
    parentFieldId: number;
    EntityId: string;
    EntityName: string;
    FieldName: string;
    label: string;
    MetadataListId: number;
    MetadataListName: string;
    IsRequired: boolean;
    Type: string;
    fieldTypeId: number;
    isFilterable: boolean;
    IsDeleted: boolean;
    RelatedField: number;
    DataType: string;
    Sort: number;
    Status: number;
    options: MetadataField_GetListItemByIdOutputDTO[];
    isSystem: boolean;
}