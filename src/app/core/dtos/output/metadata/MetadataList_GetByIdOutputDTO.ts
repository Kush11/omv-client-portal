import { BaseDTO } from '../../BaseDTO';

export class MetadataList_GetByIdOutputDTO extends BaseDTO {
	MetadataListId: number;
	MetadataListName: string;
	status: number;
	StatusName: string;
	parentListId : number;
	ParentListName? : string;
}