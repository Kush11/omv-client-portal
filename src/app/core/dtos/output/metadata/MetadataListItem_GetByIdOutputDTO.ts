export class MetadataListItem_GetByIdOutputDTO {
    MetadataListItemId: number;
    MetadataListId: number;
    ItemValue: string;
    ItemDescription: string;
    ItemSort: string;
    ParentItemValue: string;
    parentItemId: number;
    Status: number;
    StatusName: string;
    ParentItemDescription: string;
}
