import { PaginationInfo } from '../PaginationInfo';
import { CameraRecording_GetAllOutputDTO } from '../../input/live-stream/StreamArchiveOutputDTO';
import { BaseDTO } from '../../BaseDTO';

export class Camera_OutputDTO {
  Pagination: PaginationInfo;
  Data: Camera_GetAllOutputDTO[];
}


export class Camera_GetAllOutputDTO extends BaseDTO{
  cameraId: number;
  favoriteId?: number;
  livestreamURL: string;
  name: string;
  thumbnailUrl: string;
  isFavorite: boolean;
  asset: number;
  group: number;
  recordings: CameraRecording_GetAllOutputDTO[];
  value : string;
}


export class Camera_GetByIdOutputDTO extends BaseDTO{
  cameraId: number;
  livestreamURL: string;
  secureUrl: string;
  name: string;
  thumbnailUrl: string;
}

