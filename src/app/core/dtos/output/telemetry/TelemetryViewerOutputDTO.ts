import { Document_GetByIdOutputDTO } from '../documents/Document_GetByIdOutputDTO';
import { SurveyData } from './SurveyData';
import { EventsData } from './EventsData';



export class TelemetryViewerOutputDTO
  {
      Feeds: Document_GetByIdOutputDTO[];
      Files: FileData[];
  }
export class FileData
  {
      FileName: string;
      Data: any[];
  }