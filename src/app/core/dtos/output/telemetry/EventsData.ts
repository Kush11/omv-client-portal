 export class EventsData {
    vWTimestamp: string;
    kP: string;
    time: string;
    dCC: string;
    eventType: string;
    dimension: string;
    location: string;
    comments: string;
    eventCode: string;
}