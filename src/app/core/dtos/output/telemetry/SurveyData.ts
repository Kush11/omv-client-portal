export class SurveyData {
    aSFIndex: string;
    date: string;
    time: string;
    kP: string;
    dCC: string;
    easting: string;
    northing: string;
    depth: string;
    altitude: string;
    gyro: string;
    pitch: string;
    roll: string;
    client: string;
    task: string;
    rOV: string;
    vessel: string;
}