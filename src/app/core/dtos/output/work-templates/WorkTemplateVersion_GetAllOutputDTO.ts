export class WorkTemplateVersion_GetAllOutputDTO {
    workTemplateVersionId: number;
    version: number;
    status: number;
    statusName: string;
    publishedOn: string;
    publishedBy: string;
}