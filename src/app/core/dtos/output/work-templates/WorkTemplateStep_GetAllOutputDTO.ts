import { MetadataFieldSetting_GetAllOutputDTO } from '../metadata/MetadataFieldSetting_GetAllOutputDTO';

export class WorkTemplateStep_GetAllOutputDTO {
    workTemplateStepId: number;
    code: string;
    name: string;
    notifyByGroup: number;
    templateCode: string;
    advanceButton: string;
    advancedByGroup: number;
    workTemplateForm: MetadataFieldSetting_GetAllOutputDTO[];
    workTemplateFinding: MetadataFieldSetting_GetAllOutputDTO[];
}
