import { MetadataFieldSetting_GetAllOutputDTO } from '../metadata/MetadataFieldSetting_GetAllOutputDTO';
import { PaginationInfo } from '../PaginationInfo';

export class WorkTemplateModel {
  WorkTemplates: WorkTemplates_GetAllOutputDTO[];
  WorkTemplateVersions: WorkTemplateVersion_GetAllOutputDTO[];
  WorkTemplateVersionHistory: WorkTemplateVersionHistory_GetAllOutputDTO[];
}

export class WorkTemplates_GetAllOutputDTO {
  WorkTemplateId: number;
  WorkTemplateGroupId: number;
  ActiveWorkTemplateId:number;
  metadataListId: number;
  Name: string;
  Version: number | null;
  WorkType: string;
  WorkItem: string;
  WorkItemParent: string;
  workTemplateFields: MetadataFieldSetting_GetAllOutputDTO[];
  WorkTemplateProcess: WorkTemplateProcess_GetAllOutputDTO[];
  ModifiedOn: Date | string;
  ModifiedBy: string;
  AssetParent : string;
}

export class WorkTemplateProcess_GetAllOutputDTO {
  WorkTemplateProcessId: number;
  Name: string;
  Code: string;
  WorkTemplateStep: WorkTemplateStep_GetAllOutputDTO[];
}

export class WorkTemplateStep_GetAllOutputDTO {
  WorkTemplateStepId: number;
  Code: string;
  Name: string;
  NotifyByGroup: number;
  TemplateCode: string;
  AdvanceButton: string;
  AdvancedByGroup: number;
  WorkTemplateForm: MetadataFieldSetting_GetAllOutputDTO[];
  WorkTemplateFinding: MetadataFieldSetting_GetAllOutputDTO[];
}

export class WorkTemplateVersion_GetAllOutputDTO {
  workTemplateId: number;
  Version: number;
  Status: number;
  StatusName: string;
  Comment: string;
  PublishedOn: Date | string;
  PublishedBy: string;
}

export class WorkTemplateVersionHistory_GetAllOutputDTO {
  data: WorkTemplateVersionHistory_GetAllOutputDTOData;
  pagination: PaginationInfo;
}

export class WorkTemplateVersionHistory_GetAllOutputDTOData {
  AuditId: string;
  EventName: string;
  EntityType: string;
  EntityId: string;
  ColumnName: string;
  OldValue: string;
  NewValue: string;
  CreatedOn: Date | string;
  CreatedBy: string;
}
