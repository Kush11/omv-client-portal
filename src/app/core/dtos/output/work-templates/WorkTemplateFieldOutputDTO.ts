import { WorkItemFormData } from '../work-planning/WorkItemFormData';
import { WorkTemplateProcess_GetAllOutputDTO } from './WorkTemplateModel';

export class WorkTemplateFieldOutputDTO {
  fields: WorkItemFormData[];
  processes: WorkTemplateProcess_GetAllOutputDTO[];
}