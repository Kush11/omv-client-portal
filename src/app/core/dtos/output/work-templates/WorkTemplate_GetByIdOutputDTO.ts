import { WorkTemplate_GetAllOutputDTO } from './WorkTemplate_GetAllOutputDTO';
import { WorkTemplateVersionHistory_GetAllOutputDTO } from './WorkTemplateVersionHistory_GetAllOutputDTO';
import { WorkTemplateVersion_GetAllOutputDTO } from './WorkTemplateVersion_GetAllOutputDTO';



export class WorkTemplate_GetByIdOutputDTO{
    workTemplates: WorkTemplate_GetAllOutputDTO[];
    workTemplateVersions: WorkTemplateVersion_GetAllOutputDTO[];
    workTemplateVersionHistory: WorkTemplateVersionHistory_GetAllOutputDTO[];
}








