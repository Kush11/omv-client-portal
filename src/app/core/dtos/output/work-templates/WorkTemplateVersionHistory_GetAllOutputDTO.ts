import { BaseDTO } from '../../BaseDTO';

export class WorkTemplateVersionHistory_GetAllOutputDTO extends BaseDTO {
    auditId: string;
    eventName: string;
    entityType: string;
    entityId: string;
    columnName: string;
    oldValue: string;
    newValue: string;
    
}