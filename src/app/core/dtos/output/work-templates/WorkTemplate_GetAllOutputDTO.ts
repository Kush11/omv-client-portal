import { BaseDTO } from '../../BaseDTO';
import { WorkTemplateProcess_GetAllOutputDTO } from './WorkTemplateModel';

export class WorkTemplate_GetAllOutputDTO extends BaseDTO {
    workTemplateId: number;
    workTemplateGroupId: number;
    name: string;
    version?: number;
    workType: string;
    workItem: string;
    workItemParent: string;
    workTemplateProcess: WorkTemplateProcess_GetAllOutputDTO[];
}