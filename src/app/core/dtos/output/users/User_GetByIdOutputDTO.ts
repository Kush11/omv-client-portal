import { BaseDTO } from '../../BaseDTO';

export class User_GetByIdOutputDTO extends BaseDTO {
  UserId: number;
  UserName: string;
  EmailAddress: string;
  FirstName: string;
  LastName: string;
  DisplayName: string;
  RoleName: string;
  Status: number;
  StatusName: string;
  FolderPaths: string[];
  Permissions: User_PermissionsOutputDTO;
}


export class User_PermissionsOutputDTO {
  Menus: CustomerMenuPermission_GetByCustomerId[];
  UserPermissions: string[];
}

export class CustomerMenuPermission_GetByCustomerId {
  CustomerId: number;
  MenuName: string;
  IsPermitted: boolean;
}


export class TopLevelMenuPermission {
  isPermittedDashboard: boolean;
  isPermittedMedia: boolean;
  isPermittedLiveStream: boolean;
  isPermitedWorkPlanning: boolean;
  isPermittedCollaborations: boolean;
}


export class UserPermission {
  isClientAdmin: boolean;
  isOIIAdmin: boolean;
  canUploadMedia: boolean;
  canUploadBulkMedia: boolean;
  canEditBulkMetadata: boolean;
  canEditMetadata: boolean;
  canEditCollaboration: boolean;
  canViewAdminPortal: boolean;
}
