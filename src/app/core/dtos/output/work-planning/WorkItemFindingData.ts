import { WorkItemFormData } from './WorkItemFormData';
import { BaseDTO } from '../../BaseDTO';

export class WorkItemFindingData extends BaseDTO {
  workFindingId: number;
  workId: number;
  stepId: number;
  findingData: string;
  status: number;
  itemFields: WorkItemFormData[];
  form: WorkItemFormData[];
}