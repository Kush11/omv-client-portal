import { MetadataField_GetListItemByIdOutputDTO } from '../metadata/MetadataField_GetListItemByIdOutputDTO';

export class WorkItemFormData {
  id: number;
  parentId: number;
  label: string;
  fieldName: string;
  value: string;
  fieldType: string;
  canEdit: boolean;
  canUpload: boolean | null;
  isRequired: boolean;
  isUnique: boolean | null;
  stepId: number | null;
  options: MetadataField_GetListItemByIdOutputDTO[];
}