import { WorkItemFindingData } from './WorkItemFindingData';
import { WorkItemFormData } from './WorkItemFormData';
import { BaseDTO } from '../../BaseDTO';
import { PaginationInfo } from '../PaginationInfo';

export class WorkPlanning_GetWorksOutputDTO extends BaseDTO {
  data: WorkPlanning_GetWorksOutputDataDTO[];
  pagination: PaginationInfo;
}

export class WorkPlanning_GetWorksOutputDataDTO extends BaseDTO {
  workId: number;
  assetId: number;
  assetName: string;
  itemId: number;
  firstItemIdentifier: string;
  secondItemIdentifier: string;
  processId: number;
  processCode: string;
  processName: string;
  findingsText: string;
  currentStepId: number;
  currentStep: string;
  cycleName: string;
  cycleId: number;
  workSetCycleStartDate: Date;
  workSetCycleEndDate: Date;
  workSetCycleStatus: number;
  workSetCycleStatusName: string;
  formData: string;
  findingData: string;
  advanceButton: string;
  advancedByGroup: number;
  canAdvance: boolean;
  notifyByGroup: number;
  stepOrder: number;
  status: number;
  statusName: string;
  createdOn: Date;
  createdBy: string;
  modifiedOn: Date;
  modifiedBy: string;
  itemFields: WorkItemFormData[];
  form: WorkItemFormData[];
  findings: WorkItemFindingData[];
}