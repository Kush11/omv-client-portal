import { MetadataListItem_GetByIdOutputDTO } from '../metadata/MetadataListItem_GetByIdOutputDTO';
import { MetadataListItem_GetById } from '../metadata/MetadataListItem_GetById';

export class WorkItemFormData_DTO {
    Label: string;
    FieldName: string;
    Value: string;
    FieldType: string;
    CanEdit: boolean;
    CanUpload?: boolean;
    IsRequired: boolean;
    Options: MetadataListItem_GetById[];
}