export class WorkFinding {
  WorkFindingId: number;
  WorkId: number;
  StepId: number;
  FindingData: string;
  Status: number;
  CreatedOn: Date | string;
  CreatedBy: string;
  ModifiedOn: Date | string;
  ModifiedBy: string;
}