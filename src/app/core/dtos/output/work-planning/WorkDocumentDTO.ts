
export class WorkDocument_GetAllOutputDTO {
    cycleName: string;
    workDocument: WorkDocument_GetAllDTO[];
    cycleStartDate: Date;
    cycleEndDate: Date;
}

export class WorkDocument_GetAllDTO {
    metadataFieldSettingId: number;
    workId: number;
    isFinding: boolean;
    documentId: string;
    documentTypeCode: string;
    documentName: string;
    documentUrl: string;
    metadata: string;
    contentType: string;
    size?: number;
    containerId: string;
    thumbnailContainerUrl: string;
    isDeleted?: boolean;
    status?: number;
    documentDate: Date;
    processCode: string;
    processName: string;
}