import { WorkItemFindingDataDto } from './WorkItemFindingDataDto';
import { WorkItemFormData_DTO } from './WorkItemFormDataDto';
import { PaginationInfo } from '../PaginationInfo';

export class WorkItemPlanning_GetAllOutputDTO {
  pagination: PaginationInfo;
  data: WorkItemPlanning_GetAllOutputDTOData[];
}

export class WorkItemPlanning_GetAllOutputDTOData {
  WorkId: number;
  AssetId: number;
  ItemNo: string;
  ItemName: string;
  ProcessId: number;
  ProcessCode: string;
  ProcessName: string;
  FindingsText: string;
  CurrentStepId: number;
  CurrentStep: string;
  CycleName: string;
  FrequencyId: number;
  workSetCycleEndDate: Date;
  workSetCycleStartDate: Date;
  workSetCycleStatus: number;
  workSetCycleStatusName: string;
  FormData: string;
  FindingData: string;
  AdvanceButton: string;
  AdvancedByGroup?: number;
  CanAdvance: boolean;
  NotifyByGroup?: number;
  StepOrder?: number;
  Status: number;
  StatusName: string;
  CreatedOn: Date;
  CreatedBy: string;
  ModifiedOn: Date;
  ModifiedBy: string;
  Form: WorkItemFormData_DTO[];
  Findings: WorkItemFindingDataDto;
}

export class WorkPlanning_GetAllOutputDTO {
  Asset: string;
  Cycles: WorkPlanning_GetCycles[];
}

export class WorkPlanning_GetCycles {
  Cycle: string;
  Data: WorkPlanning_Data[];
}
export class WorkPlanning_Data {
  Label: string;
  TotalCompleted: number;
  Total: number;
  Days: number;

}
