import { BaseDTO } from '../../BaseDTO';

export class Rule_GetByIdOutputDTO extends BaseDTO {
  RuleId: number;
  Name: string;
  AssemblyName: string;
  AssemblyPath: string;
  ClassNameWithNamespace: string;
  ApprovalGroupId: number;
  ApprovalGroupName: string;
}