export class Lookup_DTO {
  lookupType: string;
  lookupValue: string;
  lookupDescription: string;
  lookupSort: string;
  status: number;
  createdOn: Date | string;
  createdBy: string;
  modifiedOn: Date | string;
  modifiedBy: string;
}