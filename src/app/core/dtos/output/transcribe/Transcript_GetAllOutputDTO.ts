export class Transcript_GetAllOutputDTO {
  confidence: string;
  display: string;
  duration: string;
  offSet: string;
  startTime: number;
  endTime: number;
}