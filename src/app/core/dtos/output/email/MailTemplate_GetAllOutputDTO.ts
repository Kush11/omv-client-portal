import { BaseDTO } from '../../BaseDTO';

export class MailTemplate_GetAllOutputDTO extends BaseDTO {
    TemplateCode: string;
    TemplateName: string;
    TemplateSubject: string;
    TemplateBody: string;
    isBodyHTML: number;
    isSystem: boolean;
    priority: string;
}