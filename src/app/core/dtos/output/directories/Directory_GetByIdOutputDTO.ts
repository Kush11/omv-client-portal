export class Directory_GetByIdOutputDTO {
  DirectoryId: number;
  DirectoryName: string;
  DirectoryParentId: number;
  HasChild: boolean;
  CreatedOn: Date | string;
  CreatedBy: string;
  ModifiedOn: Date | string;
  ModifiedBy: string;
}