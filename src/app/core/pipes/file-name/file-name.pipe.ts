import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'fileName'
})
export class FileNamePipe implements PipeTransform {

  transform(value: any, args?: any): any {
    var index = value.lastIndexOf('.');
    return value.slice(0, index);
  }
}
