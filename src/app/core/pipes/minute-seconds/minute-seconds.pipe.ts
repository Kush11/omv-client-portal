import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'minuteSeconds'
})
export class MinuteSecondsPipe implements PipeTransform {

  transform(value: number, args?: any): any {
    let hours   =  Math.floor(value / 3600).toString().padStart(2, '0');
    let minutes = Math.floor(value / 60).toString().padStart(2, '0');
    let seconds = Math.round(value % 60).toString().padStart(2, '0');
    return "" + hours + ":"+ minutes + ":" + seconds;
  }

}
