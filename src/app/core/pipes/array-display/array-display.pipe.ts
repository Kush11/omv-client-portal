import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'arrayDisplay'
})
export class ArrayDisplayPipe implements PipeTransform {

  transform(values: any, args?: any): any {
    let retVal = values;
    if (args === 'date') {
      const min = Number(values[0]);
      const max = Number(values[1]);
      let custom = { year: "numeric", month: "short", day: "numeric" };
      retVal = `${new Date(min).toLocaleDateString("en-us", custom)} - ${new Date(max).toLocaleDateString("en-us", custom)}`;
    } else {
      if (typeof values !== 'string') {
        const min = values[0];
        const max = values[1];
        retVal = `${min} - ${max}`;
      }
    }
    return retVal;
  }

}
