import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'avatar'
})
export class AvatarPipe implements PipeTransform {

  colors = ['#0B2545', '#8E5572', '#EFCB68', '#725AC1', '#EF233C', '#DE6B48', '#8DA9C4', '#EF8275', '#0CCA4A', '#F17300',
    '#054A91', '#3E5641', '#A24936', '#D36135', '#DD9AC2', '#8DA9C4', '#732C2C', '#7E1F86', '#54428E', '#75755D'];

  transform(value: any, args?: any): any {
    if (args === 'color') {
      const randomNumber = Math.floor(Math.random() * (this.colors.length - 1) + 1);
      const index = value ? value % this.colors.length : randomNumber;
      return this.colors[index];
    }
    const array = value.split(' ');
    const result = `${array[0][0]}${array[1][0]}`.toUpperCase();
    return result;
  }

}
