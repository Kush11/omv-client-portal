export const environment = {
  production: true,
  useMocks: false,
  storageAccount: "{env-storageAccount}",
  issuesContainer: "{env-issuesContainer}",
  api: {
    baseUrl: '{env-baseUrl}'
  }
 
};

