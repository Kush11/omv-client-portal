// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  useMocks: false,
  storageAccount: "ocean33r1ngm3d1avault",
  issuesContainer: "issues",
  api: {
      baseUrl: 'https://chai-omv-d-cp-api.azurewebsites.net/api'
  },
  Twilio_Account: {
    ACCOUNT_SID: 'AC734c583b928ac1f6ad5c5380ab6e84d2',
    API_KEY_SID: 'SKdbee41c86dc4aabde7939b2ba131ce75',
    API_KEY_SECRET: '1LfGzozUxCSyJSUdBwoen1Ur9iRUxGGp'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// 'https://omv2dcpsvr.azurewebsites.net/api', 'https://omv2dcpsvr.azurewebsites.net/bulk-uploader'
// 'http://omvapi.test.eminenttechnology.com/api', 'http://uploader.test.eminenttechnology.com'
  import 'zone.js/dist/zone-error'; // Included with Angular CLI.