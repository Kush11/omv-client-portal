export const environment = {
  production: false,
  useMocks: true,
  storageAccount: "ocean33r1ngm3d1avault",
  issuesContainer: "issues",
  api: {
    baseUrl: 'http://omv.test.eminenttechnology.com/OMV.Api/api1'
  }
};
